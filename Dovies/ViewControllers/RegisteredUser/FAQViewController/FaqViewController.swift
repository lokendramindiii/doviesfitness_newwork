//
//  FaqViewController.swift
//  Dovies
//
//  Created by HB-PC on 25/04/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class FaqViewController: BaseViewController {

    @IBOutlet var IBtblFAQ: UITableView!
    @IBOutlet var IBlblNoData: UILabel!
    var arrFaq = [FaqModel]()
    var cellCount = 10
    var expandedIndex = -999
    
    //MARK: ViewLifeCycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "FAQ"
        self.navigationItem.hidesBackButton  = true
        //self.addNavigationBackButton()
        self.addNavigationWhiteBackButton()
        
        IBtblFAQ.register(UINib(nibName: "FaqCell", bundle: nil), forCellReuseIdentifier: "FaqCell")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if arrFaq.count == 0{
            cellCount = 10
            self.IBtblFAQ.showLoader()
            self.callFaqListAPI()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension FaqViewController : UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard arrFaq.count > indexPath.row else{return}
        let prevIndex = expandedIndex
        if indexPath.row == expandedIndex{
            expandedIndex = -999
        }else{
            expandedIndex = indexPath.row
        }
        tableView.reloadRows(at: [indexPath,IndexPath.init(row: prevIndex, section: 0)], with: .automatic)
    }
}

extension FaqViewController : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrFaq.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "FaqCell") as! FaqCell
        cell.set(content: arrFaq[indexPath.row], shouldExpand: indexPath.row == expandedIndex)
        return cell
    }

}

//API Calls
extension FaqViewController
{
    /**
     This method is used fetch the list of frequently asking question and update the tableview.
     */
    func callFaqListAPI(){
        DoviesWSCalls.callGetFaqsAPI(dicParam: nil, onSuccess: { [weak self](success, faqList, message, hasNextPage) in
            guard let weakSelf = self else {return}
            if success, let list = faqList{
                weakSelf.arrFaq = list
            }
            weakSelf.IBlblNoData.isHidden = weakSelf.arrFaq.count > 0
            weakSelf.cellCount = 0
            weakSelf.IBtblFAQ.hideLoader()
            weakSelf.IBtblFAQ.reloadData()
        }) {[weak self] (error) in
            guard let weakSelf = self else {return}
            weakSelf.IBtblFAQ.hideLoader()
            weakSelf.IBtblFAQ.reloadData()
        }
        
    }
    
}
