//
//  FeaturesVerticallTableCell.swift
//  Dovies
//
//  Created by hb on 08/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class FeaturesVerticallTableCell: UITableViewCell {

    @IBOutlet var imgBackgroud: UIImageView!
    @IBOutlet weak var btnMore: UIButton!
	@IBOutlet var IBlblName: UILabel!
	@IBOutlet var IBlblWorkOut: UILabel!
    @IBOutlet var btnLock: UIButton!
    var moreTapped:((IndexPath?)->())?
    var indexPath : IndexPath?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
	func setFeaturedCell(model: ModelWorkoutList){
	//	imgBackgroud.sd_setImage(with: URL(string: model.workoutImage), placeholderImage: AppInfo.placeholderImage)
        
        imgBackgroud.sd_setImage(with: URL(string: model.workoutImage), placeholderImage: nil)

		IBlblName.text = model.workoutName
        btnLock.isHidden = (model.workoutAccessLevel.uppercased() == "OPEN".uppercased() || model.workoutAccessLevel.isEmpty)
		if model.workoutExerciseCount == "0" || model.workoutExerciseCount == "1"{
			IBlblWorkOut.text = "\(model.workoutExerciseCount!) Workout - \(model.workoutLevel!)"
		}
		else{
			IBlblWorkOut.text = "\(model.workoutExerciseCount!) Workouts - \(model.workoutLevel!)"
		}
	}
	
    func setFeaturedDownloadCell(model: Workout, iPath : IndexPath){
        btnMore.isHidden = false
		//imgBackgroud.sd_setImage(with: URL(string: model.workoutImage!), placeholderImage: AppInfo.placeholderImage)
        
        imgBackgroud.sd_setImage(with: URL(string: model.workoutImage!), placeholderImage: nil)

        
		IBlblName.text = model.workoutName
		if model.workoutExerciseCount == "0" || model.workoutExerciseCount == "1"{
			IBlblWorkOut.text = "\(model.workoutExerciseCount!) Workout - \(model.workoutLevel!)"
		}
		else{
			IBlblWorkOut.text = "\(model.workoutExerciseCount!) Workouts - \(model.workoutLevel!)"
		}
        self.indexPath = iPath
	}
    
    @IBAction func btnMoreTapped(_ sender : UIButton){
        moreTapped?(indexPath)
    }
}
