//
//  ZPassGlobalUtility.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit
import Foundation
import AutoScrollLabel
import Branch
import NVActivityIndicatorView

let urlfolderPathForAttachedVideo = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0].appendingPathComponent("Videos")

class DoviesGlobalUtility: NSObject {
    
    static var isSubscribed = false
    static var isAdminUserName = ""

    static var subscriptionDaysLeft = ""
    static var currentUser : User?
    static var  arrFilterData = [FilterGroup]()
    static var isNetworkAvailable : Bool = false
    static var isMusicAdded = false
    static var showSubscriptionPopupOnRegistration = false


    enum DefaultType {
        case iPhone4s
        case iPhone5
        case iPhone6
        case iPhone6p
        case iPhoneX
    }
    
    static var deviceType:DefaultType {
        let screenheight:CGFloat = UIScreen.main.bounds.size.height
        switch screenheight {
        case 568:
            return DefaultType.iPhone5
        case 667:
            return DefaultType.iPhone6
        case 736:
            return DefaultType.iPhone6p
        case 812:
            return DefaultType.iPhoneX
        default:
            return DefaultType.iPhone4s
        }
    }
    
    class func getNavigationTitleView(with text : String) -> CBAutoScrollLabel{
        var navigationBarScrollLabel : CBAutoScrollLabel!
        navigationBarScrollLabel = CBAutoScrollLabel(frame: CGRect(x: 0, y: 0, width: ScreenSize.width - 100, height: 44))
        navigationBarScrollLabel.text = text
        navigationBarScrollLabel.pauseInterval = 3.0
        navigationBarScrollLabel.font = UIFont.robotoBold(size: 16.0)
        navigationBarScrollLabel.textColor = UIColor.black
        navigationBarScrollLabel.textAlignment = .center
        return navigationBarScrollLabel
    }
    
    class func showShareSheet(on viewController : UIViewController, shareText: String){
        let activitySheet = UIActivityViewController(activityItems: [shareText], applicationActivities: nil)
        viewController.present(activitySheet, animated: true, completion: nil)
    }
    
    class func showSimpleAlert(viewController: UIViewController, message: String? = "", completion: (()->())? = nil) {
        DispatchQueue.main.async {
            UIAlertController.showAlert(in: viewController, withTitle: AlertTitle.appName, message: message, cancelButtonTitle: AlertButtonTitle.ok, destructiveButtonTitle: nil, otherButtonTitles: nil) { (alertController, action, buttonIndex) in
                completion?()
            }
        }
    }
    
    class func getBackgroundImageBasedOnDevice(imageName:String) -> String{
        switch DoviesGlobalUtility.deviceType
        {
        case .iPhone5:
            return "\(imageName)" + "-568h"
        case .iPhone6:
            return "\(imageName)" + "-667h"
        case .iPhone6p:
            return "\(imageName)" + "-736h"
        case .iPhoneX:
            return "\(imageName)" + "-812h"
        default:
            return ""
        }
    }
	
	class func pathForVideoZip(fileName: String, downloadUrl: String) -> URL {
		let songUrl = URL(string: downloadUrl)
		let sSongExtension = (songUrl!.pathExtension != "") ? songUrl!.pathExtension : "zip"
		let pathComponent = String(fileName) + ((songUrl != nil) ? ("." + sSongExtension) : "")
		return urlfolderPathForAttachedVideo.appendingPathComponent(fileName).appendingPathComponent(pathComponent)
	}
	
	class func pathForVideoFile(videoFileName: String) -> URL {
		return urlfolderPathForAttachedVideo.appendingPathComponent(videoFileName)
	}
	
	class func tempZipPath(fileName: String) -> String {
		var path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
		path += "/Videos/\(fileName)/\(fileName).zip"
		return path
	}
	
	class func tempUnzipPath(fileName: String) -> String {
		var path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
		path += "/Videos/\(fileName)"
		let url = URL(fileURLWithPath: path)
		
		return url.path
	}
    
    /*
     Convenience method for delaying a block from executing
     */
    public class func delay(delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(deadline: .now() + delay) {
            closure()
        }
    }
    
   class func showDefaultActionSheet(on vc:UIViewController, actionData:[alertActionData],cancelTitle:String,completion: @escaping ( _ clickedIndex: Int ,  _ isCancel: Bool ) -> Void) {
        
        let title = ""
        let message = ""
        let style = AlertControllerStyle(rawValue: 0)!
        let alert = AlertController(title: title, message: message, preferredStyle: style)
        
        alert.add(AlertAction(title: cancelTitle, style: .preferred))
        
        for btnData in actionData {
            let index = actionData.index(of: btnData)
            let image = UIImage(named: btnData.imageName!)
            let highlightedImage = UIImage(named: btnData.hightlightedImageName!)
            let imgAttachment = NSTextAttachment()
            imgAttachment.image = image
            let font = UIFont.robotoMedium(size: 16.0)
            let mid = (font.descender) + (font.capHeight)
            
           imgAttachment.bounds = CGRect(x: 0, y: (font.descender) - (image?.size.height)! / 2 + mid + 5, width: (image?.size.width)!, height: (image?.size.height)!).integral
            
            let imgString = NSAttributedString(attachment: imgAttachment)
            let msgString = NSMutableAttributedString(attributedString: imgString)
            let style = NSMutableParagraphStyle()
            style.alignment = NSTextAlignment.right
            let txtString = NSAttributedString(string: "    (btnData.title!)", attributes: [ NSAttributedStringKey.paragraphStyle: style, NSAttributedStringKey.foregroundColor : UIColor.black])
            msgString.append(txtString)
            
            let alertAlert = AlertAction(title : btnData.title!, image : image, highlightedImage : highlightedImage, style: .normal,handler: { (action) in
                completion(index!,false)
            })
            alert.add(alertAlert)
        }
        
        alert.view.tintColor = UIColor.black
        alert.actionLayout = ActionLayout(rawValue: 0)!
        alert.present()
    }
    
    
    class func showDefaultActionSheetModified(on vc:UIViewController, actionData:[alertActionData],cancelTitle:String,completion: @escaping ( _ clickedIndex: Int ,  _ isCancel: Bool, _ strTitle:String ) -> Void) {
        
        let title = ""
        let message = ""
        let style = AlertControllerStyle(rawValue: 0)!
        let alert = AlertController(title: title, message: message, preferredStyle: style)
        
        alert.add(AlertAction(title: cancelTitle, style: .preferred))
        
        for btnData in actionData {
            let index = actionData.index(of: btnData)
            let image = UIImage(named: btnData.imageName!)
            let highlightedImage = UIImage(named: btnData.hightlightedImageName!)
            let imgAttachment = NSTextAttachment()
            imgAttachment.image = image
            let font = UIFont.robotoMedium(size: 16.0)
            let mid = (font.descender) + (font.capHeight)
            
            imgAttachment.bounds = CGRect(x: 0, y: (font.descender) - (image?.size.height)! / 2 + mid + 5, width: (image?.size.width)!, height: (image?.size.height)!).integral
            
            let imgString = NSAttributedString(attachment: imgAttachment)
            let msgString = NSMutableAttributedString(attributedString: imgString)
            let style = NSMutableParagraphStyle()
            style.alignment = NSTextAlignment.right
            let txtString = NSAttributedString(string: "    (btnData.title!)", attributes: [ NSAttributedStringKey.paragraphStyle: style, NSAttributedStringKey.foregroundColor : UIColor.black])
            msgString.append(txtString)
            
            let alertAlert = AlertAction(title : btnData.title!, image : image, highlightedImage : highlightedImage, style: .normal,handler: { (action) in
                completion(index!,false, btnData.title!)
            })
            alert.add(alertAlert)
        }
        
        alert.view.tintColor = UIColor.black
        alert.actionLayout = ActionLayout(rawValue: 0)!
        alert.present()
    }
    
    class func checkWorkOutDownloadedOrNot(folderName: String) -> Bool{
        let fileManager = FileManager.default
        if fileManager.fileExists(atPath: "\(DoviesGlobalUtility.tempUnzipPath(fileName: folderName))") {
            return true
        }
        
        return false
    }
	
	class func deleteVideoFolder(folderName: String){
		do {
			let fileManager = FileManager.default
			// Check if file exists
			if fileManager.fileExists(atPath: "\(DoviesGlobalUtility.tempUnzipPath(fileName: folderName))") {
				// Delete file
				try fileManager.removeItem(atPath: "\(DoviesGlobalUtility.tempUnzipPath(fileName: folderName))")
			} else {
				print("File does not exist")
			}
			
		}
		catch let error as NSError {
			print("An error took place: \(error)")
		}
	}
    
    public class func loadingFooter() -> UIView {
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        let v = UIView(frame: CGRect(x: 0, y: 0, width: ScreenSize.width, height: 40))
        v.addSubview(spinner)
        spinner.center = v.center
        spinner.startAnimating()
        
        return v
    }
	
	/// This method sets no data found label in given tableview.
	///
	/// - Parameters:
	///   - tblView: table view to set the label
	///   - text: text to set in table
	class func setNoLabelData(tblView: UITableView, text: String = "No data found.") {
		let lbl = UILabel(frame: tblView.frame)
		lbl.text = text
		lbl.textAlignment = .center
		tblView.backgroundView = lbl
	}
    
    /// This method used to call feed listing WS
    class func setFilterData(){
        if let date = GlobalUtility.getPref(key: UserDefaultsKey.filterDate) as? Date{
            let components = Calendar.current.dateComponents([.day], from: date, to: Date())
            if  let days = components.day , days > 0{
                DoviesGlobalUtility.callFilterAPI()
            }else{
                if let arr = GlobalUtility.getPref(key: UserDefaultsKey.filterData) as? [[String : Any]] {
                    arrFilterData = [FilterGroup]()
                    GlobalUtility.setPref(value: arr, key: UserDefaultsKey.filterData)
                    for dic in arr{
                        arrFilterData.append(FilterGroup(fromDictionary: dic))
                    }
                }
            }
            
        }else{
            DoviesGlobalUtility.callFilterAPI()
        }
        
    }
    
    fileprivate class func setSubscriptionStatus(vc : UIViewController){
        guard let cUser = DoviesGlobalUtility.currentUser else {return}
        var dic = [String: Any]()
        dic[WsParam.authCustomerId] = cUser.customerAuthToken
        DoviesWSCalls.callSubscriptionStatusAPI(dicParam: dic, onSuccess: { (success, response, message) in
            if success, let subscriptionDict = response{
                if let subStatus = (subscriptionDict[WsParam.isSubscribed] as? String)?.toBool(){
                    DoviesGlobalUtility.isSubscribed = subStatus
                }
                if let daysLeft = (subscriptionDict[WsParam.title] as? String){
                    DoviesGlobalUtility.subscriptionDaysLeft = daysLeft
                }
                
            }
        }) { (error) in
            
        }
    }
    
    fileprivate class func callFilterAPI(){
        guard let cUser = DoviesGlobalUtility.currentUser else {return}
        var dic = [String: Any]()
        dic[WsParam.authCustomerId] = cUser.customerAuthToken
        dic["module_type"] = ""
       // GlobalUtility.setPref(value: Date(), key: UserDefaultsKey.filterDate)
        DoviesWSCalls.callGetModelFilterDataAPI(dicParam: dic, onSuccess: { (sucess, filterArr, strMessage) in
            if sucess, let filterInfo = filterArr{
                DoviesGlobalUtility.arrFilterData = filterInfo
            }
            
        }) { (error) in
            
        }
    }
    
    class func checkNetworkStatus(){
        let net = NetworkReachabilityManager()
        net?.startListening()
        
        net?.listener = { status in
            if net?.isReachable ?? false {
                switch status {
                case .reachable(.ethernetOrWiFi):
                    print("The network is reachable over the WiFi connection")
                    isNetworkAvailable = true
                case .reachable(.wwan):
                    print("The network is reachable over the WWAN connection")
                    isNetworkAvailable = true
                case .notReachable:
                    print("The network is not reachable")
                    isNetworkAvailable = false
                case .unknown :
                    isNetworkAvailable = false
                    print("It is unknown whether the network is reachable")
                    
                }
                if let window = UIApplication.shared.keyWindow{
                    if let vc = window.getVisibleViewController(), let sideVC = vc.sideMenuController?.sideViewController as? SideMenuViewController{
                        sideVC.tblMenuView.reloadData()
                        if !isNetworkAvailable{
                        DoviesGlobalUtility.showSimpleAlert(viewController: vc, message: AlertMessages.noNetwrok, completion: nil)
                        }
                    }
                }
            }else{
                isNetworkAvailable = false
                if let window = UIApplication.shared.keyWindow{
                    if let vc = window.getVisibleViewController(), let sideVC = vc.sideMenuController?.sideViewController as? SideMenuViewController{
                        sideVC.tblMenuView.reloadData()
                        if !isNetworkAvailable{
                            DoviesGlobalUtility.showSimpleAlert(viewController: vc, message: AlertMessages.noNetwrok, completion: nil)
                        }
                    }
                }
            }
        }
    }
    
    class func generateBranchIoUrl(with moduleName : String, moduleId : String, completionHandler: @escaping (_ url : String)->()){
        
        let branchUniversalObject: BranchUniversalObject = BranchUniversalObject(canonicalIdentifier: "item/12345")
        branchUniversalObject.title = "Dovies"
        branchUniversalObject.contentDescription = "Share \(moduleName)"
        branchUniversalObject.imageUrl = ""
       
    branchUniversalObject.contentMetadata.customMetadata[WsParam.moduleName] = moduleName
        
        let lp: BranchLinkProperties = BranchLinkProperties()
        lp.channel = "SMS"
        lp.feature = "invite"
        lp.addControlParam(WsParam.moduleId, withValue: moduleId)
        
        branchUniversalObject.getShortUrl(with: lp) { (url, error) in
            print("Url is \(url ?? "")")
            completionHandler(url ?? "")
        }
        
    }
    
    class func deleteCasheForNewUser(){
        UserDefaults.standard.removeObject(forKey: UserDefaultsKey.userData)
        UserDefaults.standard.synchronize()
        Workout.deleteAllDataOfWorkout()
        Exercise.deleteAllDataOfExercise()
        SettingViewController.clearTempFolder()
    }
    
}

class alertActionData:NSObject{
    
    var title:String?
    var imageName:String?
    var hightlightedImageName : String?
    var isSeleted:Bool?
   
    init(title:String?,imageName:String?, highlighedImageName : String?) {
        self.title = title
        self.imageName = imageName
        self.hightlightedImageName = highlighedImageName
       
        //self.isSeleted = isSeleted
        
    }
    
}

