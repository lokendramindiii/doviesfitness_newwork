//
//	WorkoutExerciseList.swift
//
//	Create by hb on 12/3/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ModelWorkoutExerciseList : NSObject, NSCoding {
	
	var isLiked : String!
	var workoutExerciseBodyParts : String!
	var workoutExerciseBreakTime : String!
	var workoutExerciseDescription : String!
	var workoutExerciseDetail : String!
	var workoutExerciseEquipments : String!
	var workoutExerciseImage : String!
	var workoutExerciseIsFavourite : Bool!
	var workoutExerciseLevel : String!
	var workoutExerciseName : String!
	var workoutExercisesId : String!
    var workoutAccessLevel : String!
    var workoutOption: String = "Time"
    var workoutExercisesTime: String = ""
    var workoutRepeatText: String = ""
    var workoutBreakTime: String = ""
    var workoutTotalTime: String = ""
	var workoutExerciseVideo: String = ""
	var workoutExerciseType: String = ""
	var isBreakTimeThere = false
	var fBreakTime: Int!
    var workoutShareUrl : String!

    var workoutExerciseSequenceSequenceNumber : String!
	/**
	* Instantiate the instance using the passed dictionary values to set the properties values
	*/
	init(fromDictionary dictionary: [String:Any]){

		isLiked = dictionary["is_liked"] as? String ?? ""
        
        workoutShareUrl = dictionary["exercise_share_url"] as? String ?? ""

        
		workoutExerciseBodyParts = dictionary["workout_exercise_body_parts"] as? String ?? ""
		workoutExerciseBreakTime = dictionary["workout_exercise_break_time"] as? String ?? ""
		workoutExerciseDescription = dictionary["workout_exercise_description"] as? String ?? ""
		workoutExerciseDetail = dictionary["workout_exercise_detail"] as? String ?? ""
		workoutExerciseEquipments = dictionary["workout_exercise_equipments"] as? String ?? ""
		workoutExerciseImage = dictionary["workout_exercise_image"] as? String ?? ""
		workoutExerciseLevel = dictionary["workout_exercise_level"] as? String ?? ""
		workoutExerciseName = dictionary["workout_exercise_name"] as? String ?? ""
		workoutExercisesId = dictionary["workout_exercises_id"] as? String ?? ""
		workoutExerciseVideo = dictionary["workout_exercise_video"] as? String ?? ""
        
        workoutExerciseSequenceSequenceNumber = dictionary["iSequenceNumber"] as? String ?? ""

        workoutExercisesTime = dictionary["workout_exercise_time"] as? String ?? ""
		workoutRepeatText = dictionary["workout_repeat_text"] as? String ?? ""
		workoutExerciseType = dictionary["workout_exercise_type"] as? String ?? ""
		workoutBreakTime = dictionary["workout_exercise_break_time"] as? String ?? ""
        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        {
            workoutAccessLevel = "OPEN"
        }
        else
        {
            workoutAccessLevel = dictionary["exercise_access_level"] as? String ?? ""

        }
		if let likeStatus = dictionary["workout_exercise_is_favourite"] as? String , likeStatus == "1"{
			workoutExerciseIsFavourite = true
		}else if  let bLikeStatus = dictionary["workout_exercise_is_favourite"] as? Bool{
			workoutExerciseIsFavourite = bLikeStatus
		}else{
			workoutExerciseIsFavourite = false
		}
        var secondsToInterval = 0
        var timeArray = workoutExerciseBreakTime.components(separatedBy: ":")
        secondsToInterval = 0
        if timeArray.count > 0{
            if timeArray.count > 1{
                secondsToInterval = secondsToInterval + (Int(timeArray.first!)! * 3600)
            }
            if timeArray.count > 2{
                secondsToInterval = secondsToInterval + (Int(timeArray[1])! * 60)
            }
            if timeArray.count == 3{
                secondsToInterval = secondsToInterval + Int(timeArray.last!)!
            }
        }
        fBreakTime = secondsToInterval
        if secondsToInterval > 0{
            isBreakTimeThere = true
        }
        
	}
	
	/**
	* Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	*/
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if isLiked != nil{
			dictionary["is_liked"] = isLiked
		}
        
        if workoutShareUrl != nil{
            dictionary["exercise_share_url"] = workoutShareUrl
        }
        
		if workoutExerciseBodyParts != nil{
			dictionary["workout_exercise_body_parts"] = workoutExerciseBodyParts
		}
		if workoutExerciseBreakTime != nil{
			dictionary["workout_exercise_break_time"] = workoutExerciseBreakTime
		}
		if workoutExerciseDescription != nil{
			dictionary["workout_exercise_description"] = workoutExerciseDescription
		}
        if workoutExerciseSequenceSequenceNumber != nil{
            dictionary["iSequenceNumber"] = workoutExerciseSequenceSequenceNumber
        }
        
		if workoutExerciseDetail != nil{
			dictionary["workout_exercise_detail"] = workoutExerciseDetail
		}
		if workoutExerciseEquipments != nil{
			dictionary["workout_exercise_equipments"] = workoutExerciseEquipments
		}
		if workoutExerciseImage != nil{
			dictionary["workout_exercise_image"] = workoutExerciseImage
		}
		if workoutExerciseIsFavourite != nil{
			dictionary["workout_exercise_is_favourite"] = workoutExerciseIsFavourite
		}
		if workoutExerciseLevel != nil{
			dictionary["workout_exercise_level"] = workoutExerciseLevel
		}
		if workoutExerciseName != nil{
			dictionary["workout_exercise_name"] = workoutExerciseName
		}
		if workoutExercisesId != nil{
			dictionary["workout_exercises_id"] = workoutExercisesId
		}
        if workoutOption != nil{
            dictionary["Time"] = workoutOption
        }
        if workoutExercisesTime != nil{
            dictionary["time_value"] = workoutExercisesTime
        }
        if workoutRepeatText != nil{
            dictionary["reps_value"] = workoutRepeatText
        }
		if workoutBreakTime != nil{
			dictionary["break_time_value"] = workoutBreakTime
        }
		if workoutBreakTime != nil{
            dictionary["total_time_value"] = workoutBreakTime
        }
        if workoutAccessLevel != nil{
            dictionary["workout_access_level"] = workoutAccessLevel
        }
		return dictionary
	}
	
	init(modelExercise: Exercise){
        
        workoutShareUrl = modelExercise.workoutShareUrl
		workoutExerciseBodyParts = modelExercise.workoutExerciseBodyParts
		workoutExerciseBreakTime = modelExercise.workoutExerciseBreakTime
		workoutExerciseDescription = modelExercise.workoutExerciseDescription
		workoutExerciseDetail = modelExercise.workoutExerciseDetail
		workoutExerciseEquipments = modelExercise.workoutExerciseEquipments
		workoutExerciseImage = modelExercise.workoutExerciseImage
		workoutExerciseLevel = modelExercise.workoutExerciseLevel
		workoutExerciseName = modelExercise.workoutExerciseName
		workoutExercisesId = modelExercise.workoutExercisesId
		workoutExerciseVideo = modelExercise.workoutExerciseVideo!
		
		workoutExercisesTime = modelExercise.workoutExerciseTime!
		workoutRepeatText = modelExercise.workoutRepeatText!
		workoutExerciseType = modelExercise.workoutExerciseType!
		workoutAccessLevel = modelExercise.workoutAccessLevel!
        
        workoutExerciseSequenceSequenceNumber = modelExercise.workoutExerciseSequenceSequenceNumber!

        var secondsToInterval = 0
        var timeArray = workoutExerciseBreakTime.components(separatedBy: ":")
        secondsToInterval = 0
        if timeArray.count > 0{
            if timeArray.count > 1{
                secondsToInterval = secondsToInterval + (Int(timeArray.first!)! * 3600)
            }
            if timeArray.count > 2{
                secondsToInterval = secondsToInterval + (Int(timeArray[1])! * 60)
            }
            if timeArray.count == 3{
                secondsToInterval = secondsToInterval + Int(timeArray.last!)!
            }
        }
        fBreakTime = secondsToInterval
        if secondsToInterval > 0{
            isBreakTimeThere = true
        }
	}
	
	/**
	* NSCoding required initializer.
	* Fills the data from the passed decoder
	*/
	@objc required init(coder aDecoder: NSCoder)
	{
		isLiked = aDecoder.decodeObject(forKey: "is_liked") as? String
        
        workoutShareUrl = aDecoder.decodeObject(forKey: "exercise_share_url") as? String

		workoutExerciseBodyParts = aDecoder.decodeObject(forKey: "workout_exercise_body_parts") as? String
		workoutExerciseBreakTime = aDecoder.decodeObject(forKey: "workout_exercise_break_time") as? String
		workoutExerciseDescription = aDecoder.decodeObject(forKey: "workout_exercise_description") as? String
		workoutExerciseDetail = aDecoder.decodeObject(forKey: "workout_exercise_detail") as? String
		workoutExerciseEquipments = aDecoder.decodeObject(forKey: "workout_exercise_equipments") as? String
		workoutExerciseImage = aDecoder.decodeObject(forKey: "workout_exercise_image") as? String
		workoutExerciseIsFavourite = aDecoder.decodeObject(forKey: "workout_exercise_is_favourite") as? Bool
		workoutExerciseLevel = aDecoder.decodeObject(forKey: "workout_exercise_level") as? String
		workoutExerciseName = aDecoder.decodeObject(forKey: "workout_exercise_name") as? String
		workoutExercisesId = aDecoder.decodeObject(forKey: "workout_exercises_id") as? String
        workoutExerciseSequenceSequenceNumber = aDecoder.decodeObject(forKey: "iSequenceNumber") as? String

	}
	
	/**
	* NSCoding required method.
	* Encodes mode properties into the decoder
	*/
	@objc func encode(with aCoder: NSCoder)
	{
		if isLiked != nil{
			aCoder.encode(isLiked, forKey: "is_liked")
		}
        
        if workoutShareUrl != nil{
            aCoder.encode(workoutShareUrl, forKey: "exercise_share_url")
        }

		if workoutExerciseBodyParts != nil{
			aCoder.encode(workoutExerciseBodyParts, forKey: "workout_exercise_body_parts")
		}
		if workoutExerciseBreakTime != nil{
			aCoder.encode(workoutExerciseBreakTime, forKey: "workout_exercise_break_time")
		}
		if workoutExerciseDescription != nil{
			aCoder.encode(workoutExerciseDescription, forKey: "workout_exercise_description")
		}
		if workoutExerciseDetail != nil{
			aCoder.encode(workoutExerciseDetail, forKey: "workout_exercise_detail")
		}
		if workoutExerciseEquipments != nil{
			aCoder.encode(workoutExerciseEquipments, forKey: "workout_exercise_equipments")
		}
		if workoutExerciseImage != nil{
			aCoder.encode(workoutExerciseImage, forKey: "workout_exercise_image")
		}
		if workoutExerciseIsFavourite != nil{
			aCoder.encode(workoutExerciseIsFavourite, forKey: "workout_exercise_is_favourite")
		}
		if workoutExerciseLevel != nil{
			aCoder.encode(workoutExerciseLevel, forKey: "workout_exercise_level")
		}
		if workoutExerciseName != nil{
			aCoder.encode(workoutExerciseName, forKey: "workout_exercise_name")
		}
		if workoutExercisesId != nil{
			aCoder.encode(workoutExercisesId, forKey: "workout_exercises_id")
		}
        if workoutOption != nil{
            aCoder.encode(workoutOption, forKey: "Time")
        }
        if workoutExercisesTime != nil{
            aCoder.encode(workoutExercisesTime, forKey: "time_value")
        }
        if workoutRepeatText != nil{
            aCoder.encode(workoutRepeatText, forKey: "reps_value")
        }
        if workoutBreakTime != nil{
            aCoder.encode(workoutBreakTime, forKey: "break_time_value")
        }
        if workoutTotalTime != nil{
            aCoder.encode(workoutTotalTime, forKey: "total_time_value")
        }
        if workoutExerciseSequenceSequenceNumber != nil{
            aCoder.encode(workoutExerciseSequenceSequenceNumber, forKey: "iSequenceNumber")
        }

	}
    
}
