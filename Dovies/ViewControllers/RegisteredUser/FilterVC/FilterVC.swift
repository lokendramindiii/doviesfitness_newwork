//
//  FilterVC.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit
import AlignedCollectionViewFlowLayout

//This view controller used to show filter options for the user
class FilterVC: BaseViewController {
    
    @IBOutlet var filterCollView: UICollectionView!
    @IBOutlet var collFooterView: UIView!
    
    static let cellReuseIdentifier = "FilterCollectionCell"
    static let headerIdentifier = "FilterReusableCollView"
    static let bottomIdentifier = "FilterBottomView"
    static let blankIdentifier = "BlankReusableView"
	var isFromCreateWorkout = false
    var isFromWorkoutExeVC = false
    let cellHeight = CGFloat(30.0)//CGFloat(30.0/568.0) * ScreenSize.height
    
    var  arrFilterData: [FilterGroup] = DoviesGlobalUtility.arrFilterData
    var arrSelctedModels = [[String : [String]]]()
    
    //MARK: - View Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Filter".uppercased()
        
        navigationItem.hidesBackButton  = true
        addNavigationWhiteBackButton()
        addRightNavigationButton()
        registerCollectionView()
        self.removeExerciseListFromArray()
    }
    
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
         
    //     self.navigationController?.navigationBar.barTintColor = UIColor.appNavColor()
         
         self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,
                                                                         NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //UIApplication.shared.statusBarStyle = .default
      //  self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor : UIColor.colorWithRGB(r: 34.0, g: 34.0, b: 34.0),
            NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]
    }
    
    func addRightNavigationButton(){
        let saveButton = UIButton(type: .custom)
        saveButton.setTitle("APPLY", for: .normal)
        saveButton.titleLabel?.font  = UIFont.tamilBold(size: 16)
        saveButton.setTitleColor(#colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), for: .normal)
        // saveButton.setImage(UIImage(named:"btn_save"), for: .normal)
        saveButton.addTarget(self, action: #selector(self.didApplyButton), for: .touchUpInside)
        saveButton.frame = CGRect(x: 0, y: 0, width: 30, height: 25)
        let saveBarItem = UIBarButtonItem(customView: saveButton)
        navigationItem.rightBarButtonItems = [saveBarItem]
    }
    @objc func didApplyButton(sender: AnyObject){
        
        if let viewControllers = navigationController?.viewControllers{
            for viewController in viewControllers{
                if let workoutDetailsVC = viewController as? WorkoutDetailsVC{
                    workoutDetailsVC.reloadOnFilter?(arrSelctedModels)
                    navigationController?.popToViewController(workoutDetailsVC, animated: true)
                    return
                }
            }
        }
        if let workoutDetailsVC = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "WorkoutDetailsVC") as? WorkoutDetailsVC{
            workoutDetailsVC.arrFilterInfo = arrSelctedModels
            workoutDetailsVC.isFromCreateWorkout = self.isFromCreateWorkout
            workoutDetailsVC.isFromWorkoutExeVC = self.isFromWorkoutExeVC
            self.navigationController?.pushViewController(workoutDetailsVC, animated: true)
        }

    }
    
    //MARK: - custom functions
    
    ///This function used to register collection view nib files
    func registerCollectionView(){
        if arrSelctedModels.count == 0{
            for filterGroup in arrFilterData{
                arrSelctedModels.append([filterGroup.groupKey : [String]()])
            }
        }
        
        filterCollView.showsVerticalScrollIndicator = false
        filterCollView.register(UINib(nibName: FilterVC.cellReuseIdentifier, bundle: nil), forCellWithReuseIdentifier: FilterVC.cellReuseIdentifier)
        filterCollView.register(UINib(nibName: FilterVC.headerIdentifier, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: FilterVC.headerIdentifier)
    //    filterCollView.register(UINib(nibName: FilterVC.bottomIdentifier, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: FilterVC.bottomIdentifier)
        filterCollView.register(UINib(nibName: FilterVC.blankIdentifier, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: FilterVC.blankIdentifier)
		
		let alignedFlowLayout = filterCollView?.collectionViewLayout as? AlignedCollectionViewFlowLayout
		alignedFlowLayout?.horizontalAlignment = .left
		alignedFlowLayout?.minimumInteritemSpacing = 8
		alignedFlowLayout?.minimumLineSpacing = 0
    }
    func removeExerciseListFromArray(){
        if let indexPath = self.arrFilterData.index(where: {$0.groupName == "Exercises"}){
            arrFilterData.remove(at: indexPath)
        }
        if let indexPath = self.arrFilterData.index(where: {$0.groupKey == "filter_good_for"}){
            arrFilterData.remove(at: indexPath)
        }
    }
    //MARK: - didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


extension FilterVC:UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return arrFilterData[section].list.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FilterCollectionCell", for: indexPath) as? FilterCollectionCell else{return UICollectionViewCell()}
        cell.delegate = self
        let strItems = arrFilterData[indexPath.section].list[indexPath.row]
        var isSelelected = false
        if let arr = arrSelctedModels[indexPath.section][arrFilterData[indexPath.section].groupKey]{
            isSelelected = arr.contains(arrFilterData[indexPath.section].list[indexPath.row].gmGoogforMasterId)
        }
        cell.setData(strItems, at: indexPath, isSelected: isSelelected)
        return cell
    }
}

extension FilterVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "gotoNewsFeedMedia", sender: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let strList = arrFilterData[indexPath.section].list[indexPath.row].gmDisplayName
//        let width =  strList?.widthOfString(usingFont:UIFont.robotoRegular(size: 14.0))
        return CGSize(width: (ScreenSize.width-51)/2,height: cellHeight)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
		return arrFilterData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView{
        switch kind{
			case UICollectionElementKindSectionHeader:
				let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: FilterVC.headerIdentifier, for: indexPath) as! FilterReusableCollView
				var strTitle = (arrFilterData[indexPath.section].groupName)
				strTitle?.capitalizeFirstLetter()
				header.lblTitle.text = strTitle
				return header
            // loks comment code and add top header button apply
           case UICollectionElementKindSectionFooter:
         //     if indexPath.section == arrFilterData.count - 1{
//                    let bottomView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: FilterVC.bottomIdentifier, for: indexPath) as! FilterBottomView
//                    bottomView.filterTapped = { [weak self] in
//                        guard let weakSelf = self else{return}
//                        if let viewControllers = weakSelf.navigationController?.viewControllers{
//                            for viewController in viewControllers{
//                                if let workoutDetailsVC = viewController as? WorkoutDetailsVC{
//                                    workoutDetailsVC.reloadOnFilter?(weakSelf.arrSelctedModels)
//                                    weakSelf.navigationController?.popToViewController(workoutDetailsVC, animated: true)
//                                    return
//
//                                }
//                            }
//                        }
//                        if let workoutDetailsVC = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "WorkoutDetailsVC") as? WorkoutDetailsVC{
//                            workoutDetailsVC.arrFilterInfo = weakSelf.arrSelctedModels
//                            workoutDetailsVC.isFromCreateWorkout = (self?.isFromCreateWorkout)!
//                            self?.navigationController?.pushViewController(workoutDetailsVC, animated: true)
//                        }
//                    }
//                    return bottomView
//
            //    }else{
                    let blankView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionFooter, withReuseIdentifier: FilterVC.blankIdentifier, for: indexPath) as! BlankReusableView
                    return blankView

             // }
			default:
				return UIView() as! UICollectionReusableView
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        let size = CGSize(width:view.bounds.size.width, height:30)
        return size
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        let size = (section == arrFilterData.count - 1) ? CGSize(width: view.bounds.size.width, height: 90) : CGSize(width: view.bounds.size.width, height: 20)
        return size
        
    }
    
}

extension FilterVC : FilterCollectionCellDelegate{
    func buttonTogglePressed(obj: FilterDataModel?, selected: Bool, indexPath: IndexPath) {
        guard let model = obj else{return}
        
        if selected == true, let model = obj{
            var arr = arrSelctedModels[indexPath.section][arrFilterData[indexPath.section].groupKey]
            arr?.append(model.gmGoogforMasterId)
            arrSelctedModels[indexPath.section][arrFilterData[indexPath.section].groupKey] = arr
        }else{
            var arr = arrSelctedModels[indexPath.section][arrFilterData[indexPath.section].groupKey]
            if let index = arr?.index(of: model.gmGoogforMasterId){
                arr?.remove(at: index)
            }
            arrSelctedModels[indexPath.section][arrFilterData[indexPath.section].groupKey] = arr
        }
        print(arrSelctedModels)
    }

}



