//
//  ViewController.swift
//  CountryList-Swift
//
//  Created by Sai Ram Kotha on 29/01/16.
//  
//


//import Foundation
import UIKit

struct Country {
  let country_code : String
  let dial_code: String
  let country_name : String
}

protocol CountrySelectedDelegate {
  func SRcountrySelected(countrySelected country: Country) -> Void
}

class SRCountryPickerController: UIViewController {
    
  @IBOutlet weak var constraintTopSearchBar: NSLayoutConstraint!

  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var searchBar: UISearchBar!
    var countries = [[String: String]]()
  var countryDelegate: CountrySelectedDelegate!
  var countriesFiltered = [Country]()
  var countriesModel = [Country]()
	static var dicTexts:[String: String] = ["title":"COUNTRIES","cancel_btn_title":"Cancel","search_bar_placeholder":"Search Country"]
  
  override func viewDidLoad() {
    super.viewDidLoad()
    jsonSerial()
    collectCountries()
    self.title = SRCountryPickerController.dicTexts["title"]
    searchBar.delegate = self
    tableView.delegate = self
    tableView.dataSource = self
    tableView.allowsMultipleSelection = false
	tableView.register(UINib(nibName: "CountryTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
    
    if self.navigationController?.navigationBar.isTranslucent == true
    {
    
    let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
        (self.navigationController?.navigationBar.frame.height ?? 0.0)
    print("topBarHeighttopBarHeight\(topBarHeight)")
        
        if IS_IPHONE_X
        {
            constraintTopSearchBar.constant = 88.0
        }
        else
        {
            constraintTopSearchBar.constant = 64.0
        }
    }
    
	self.searchBar.placeholder = SRCountryPickerController.dicTexts["search_bar_placeholder"]
	let barBtnItemLeft: UIBarButtonItem = UIBarButtonItem(title: SRCountryPickerController.dicTexts["cancel_btn_title"], style: .plain, target: self, action: #selector(self.btnCancelTapped))
	self.navigationItem.leftBarButtonItem = barBtnItemLeft
   }

	@objc func btnCancelTapped() {
		self.dismiss(animated: true, completion: nil)
	}

  func jsonSerial() {
    let data = try? Data(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "countries", ofType: "json")!))
     do {
     let parsedObject = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
      countries = parsedObject as! [[String : String]]
//      print("country list \(countries)")
     }catch{
      print("not able to parse")
    }
  }
  
  func collectCountries() {
    for country in countries  {
      let code = country["code"] ?? ""
      let name = country["name"] ?? ""
      let dailcode = country["dial_code"] ?? ""
      countriesModel.append(Country(country_code:code,dial_code:dailcode, country_name:name))
    }
  }
  
  func filtercountry(_ searchText: String) {
    countriesFiltered = countriesModel.filter({(country ) -> Bool in
     let value = country.country_name.lowercased().contains(searchText.lowercased()) || country.country_code.lowercased().contains(searchText.lowercased())
      return value
    })
    tableView.reloadData()
  }
  
  func checkSearchBarActive() -> Bool {
    if searchBar.text != "" {
      return true
    } else {
      return false
    }
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
	
	class func getDialCode(countryCode: String) -> String? {
		let data = try? Data(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "countries", ofType: "json")!))
		do {
			let parsedObject = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
			var countries = [[String: String]]()
			countries = parsedObject as! [[String : String]]
			for dic in countries {
				if dic["code"] == countryCode {
					return dic["dial_code"]
				}
			}
			return nil
		}catch{
			print("not able to parse")
			return nil
		}
	}
    
    class func getCountryName(countryCode: String) -> String? {
        let data = try? Data(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "countries", ofType: "json")!))
        do {
            let parsedObject = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
            var countries = [[String: String]]()
            countries = parsedObject as! [[String : String]]
            for dic in countries {
                if dic["code"] == countryCode {
                    return dic["name"]
                }
            }
            return nil
        }catch{
            print("not able to parse")
            return nil
        }
    }
    
    class func getCountryInfo(countryCode: String) -> Country? {
        let data = try? Data(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "countries", ofType: "json")!))
        do {
            let parsedObject = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
            var countries = [[String: String]]()
            countries = parsedObject as! [[String : String]]
            for dic in countries {
                if dic["code"] == countryCode {
                    return Country(country_code: dic["code"]!, dial_code: dic["dial_code"]!, country_name: dic["name"]!)
                }
            }
            return nil
        }catch{
            print("not able to parse")
            return nil
        }
    }
	
	class func getCountryInfo(dialCode: String) -> Country? {
		let data = try? Data(contentsOf: URL(fileURLWithPath: Bundle.main.path(forResource: "countries", ofType: "json")!))
		do {
			let parsedObject = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.allowFragments)
			var countries = [[String: String]]()
			countries = parsedObject as! [[String : String]]
			for dic in countries {
				if dic["code"] == dialCode {
					return Country(country_code: dic["code"]!, dial_code: dic["dial_code"]!, country_name: dic["name"]!)
				}
			}
			return nil
		}catch{
			print("not able to parse")
			return nil
		}
	}
    
    
}

extension SRCountryPickerController: UISearchBarDelegate {
  
  func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
    self.filtercountry(searchText)
  }
  
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    searchBar.resignFirstResponder()
  }
  
  func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
    searchBar.resignFirstResponder()
  }
  
}


extension SRCountryPickerController: UITableViewDataSource {
  
  func numberOfSections(in tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if checkSearchBarActive() {
      return countriesFiltered.count
    }
    return countries.count
  }

  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if checkSearchBarActive() {
      countryDelegate.SRcountrySelected(countrySelected: countriesFiltered[indexPath.row])
    }else {
      countryDelegate.SRcountrySelected(countrySelected: countriesModel[indexPath.row])
    }
    self.dismiss(animated: true, completion: nil)
  }
  
}

extension SRCountryPickerController : UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CountryTableViewCell
    let contry: Country
    if checkSearchBarActive() {
      contry = countriesFiltered[indexPath.row]
    }else{
      contry = countriesModel[indexPath.row]
    }
	cell.lblCountryName.text = contry.country_name
	cell.lblPhoneCode.text = contry.dial_code
	let imagestring = contry.country_code
    let imagePath = "CountryPicker.bundle/\(imagestring).png"
	cell.imgViewFlag.image = UIImage(named: imagePath)
    return cell
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 50
  }
  
}
