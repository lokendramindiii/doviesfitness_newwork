//
//  FavoriteWorkoutVC.swift
//  Dovies
//
//  Created by Ananda on 16/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class FavoriteWorkoutVC: FavoriteBaseVC {

	@IBOutlet weak var IBviewExploreWorkout: UIView!
	@IBOutlet weak var IBbottomCon: NSLayoutConstraint!
	@IBOutlet weak var colGroups: UICollectionView!
	
	//let cellSizeValue : CGFloat  = (ScreenSize.width - (ProgramsViewController.cellPadding))/2
//    let cellSizeValue : CGFloat  = (ScreenSize.width - 1)/2
    let cellSizeValue : CGFloat  = (ScreenSize.width - 62 )/2

    override func viewDidLoad() {
        super.viewDidLoad()
		if IS_IPHONE_X{
			IBbottomCon.constant = 74
		}
		colGroups.register(UINib(nibName: "GroupDetailsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "GroupDetailsCollectionCell")
    }
    
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		if FavoriteVC.currentSelectedSegment == 1{
			self.callGetCustomerFavouritesAPI(apiCallingType: FavouriteApiType.Workout.rawValue, completion: {[weak self] (success) in
				guard let weakSelf = self else { return }
				if success {
					weakSelf.colGroups.reloadData()
				}
				if weakSelf.favoriteLists?.workoutFavourites == nil || weakSelf.favoriteLists?.workoutFavourites.count == 0{
					weakSelf.IBviewExploreWorkout.isHidden = false
				}
				else{
					weakSelf.IBviewExploreWorkout.isHidden = true
				}
			})
		}
	}
	
	@IBAction func redirectToWorkout(){
		self.redirectToItsTab(tabIndex: 1)
		
		let viewCont = GlobalUtility.topViewController(withRootViewController: (self.tabBarController?.selectedViewController)!)
		if viewCont.isKind(of: WorkoutExeVC.self){
			let workoutExeVC = viewCont as! WorkoutExeVC
			workoutExeVC.selectIndexForFavourite = 1
			workoutExeVC.segmentController.setSelectedSegmentAt(1, animated: false)
		}
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension FavoriteWorkoutVC : UICollectionViewDataSource{
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return self.favoriteLists?.workoutFavourites.count ?? 0
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GroupDetailsCollectionCell", for: indexPath) as? GroupDetailsCollectionCell else{return UICollectionViewCell()}
		if let arrWorkout = self.favoriteLists?.workoutFavourites {
			cell.setCellForFav(with: arrWorkout[indexPath.row])
		}
		return cell
	}
}

extension FavoriteWorkoutVC : UICollectionViewDelegate{
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		if let arrWorkout = self.favoriteLists?.workoutFavourites {
			let featuredWorkOutDetailViewControlle = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "FeaturedWorkOutDetailViewController") as! FeaturedWorkOutDetailViewController
			featuredWorkOutDetailViewControlle.modelWorkoutList = arrWorkout[indexPath.row]
			featuredWorkOutDetailViewControlle.navigationItem.title = arrWorkout[indexPath.row].workoutName
			self.navigationController?.pushViewController(featuredWorkOutDetailViewControlle, animated: false)
		}
	}
}

extension FavoriteWorkoutVC: UICollectionViewDelegateFlowLayout{
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
	//	return CGSize(width: cellSizeValue, height: cellSizeValue + 76)
        return CGSize(width: cellSizeValue, height: cellSizeValue)
	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
		return UIEdgeInsets(top: 0, left: 0, bottom: DietPlansListVC.cellPadding, right: 0)
	}
}
