//
//  WeekPlanViewController.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit
import SJSegmentedScrollView
import NVActivityIndicatorView

class WeekPlanViewController: BaseViewController {
    @IBOutlet weak var tblWorkouts      : UITableView!
    
    let buildPlanWorkoutCellIdentifier  : String = "BuildWorkoutPlanCell"
    var buildPlanModel      : BuildPlanModel!
    var weekIndex           : Int = 0
    var isFromMyPlan        : Bool = false
    var isFromEditAdmin     : Bool = false

    var editIndexPath       : IndexPath?
    var arrWorkouts         : [WorkoutDayModel]!

    var reloadAfterEdit:((_ workouts: [WorkoutDayModel], _ wIndex : Int)->())?
    var reloadAfterAddedWorkout:((_ workout: ModelWorkoutList)->())?
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBOutlet weak var activityIndicatorView : NVActivityIndicatorView!{
        didSet{
            activityIndicatorView.padding = 20
            activityIndicatorView.color = UIColor.lightGray
            activityIndicatorView.type = .circleStrokeSpin
        }
    }
    
    //MARK: ViewLifeCycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if isFromEditAdmin
        {
            self.view.backgroundColor = UIColor.black
            self.tblWorkouts.backgroundColor = UIColor.black
        }
        inititalUISetUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        activityIndicatorNew.stopAnimating()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        activityIndicatorNew.stopAnimating()
    }
    
    
    //MARK: Instance methods
    
    override func inititalUISetUp() {
        arrWorkouts = buildPlanModel.programWorkouts.weeksData[weekIndex]
        tblWorkouts.register(UINib(nibName: buildPlanWorkoutCellIdentifier, bundle: nil), forCellReuseIdentifier: buildPlanWorkoutCellIdentifier)

        reloadAfterAddedWorkout = { (workout) in
            if let indexPath = self.editIndexPath{
                BuildPlanViewController.isDataChanged = true
                var dict = [String : Any]()
                dict["program_day_number"] = "\(indexPath.row)"
                dict["program_week_number"] = "\(self.weekIndex + 1)"
                dict["program_workout_flag"] = "1"
                dict["program_workout_good_for"] = workout.workoutCategory
                dict["program_workout_id"] = workout.workoutId
                dict["program_workout_image"] = workout.workoutImage
                dict["program_workout_status"] = ""
                dict["program_workout_name"] = workout.workoutName
                dict["program_workout_time"] = workout.workoutTime
                dict["workout_id"] = workout.workoutId
                let objWorkoutDay = WorkoutDayModel(fromDictionary: dict)
                self.arrWorkouts[indexPath.row] = objWorkoutDay
                self.buildPlanModel.programWorkouts.weeksData[self.weekIndex] = self.arrWorkouts
                self.reloadAfterEdit?(self.arrWorkouts,self.weekIndex)
                self.tblWorkouts.reloadData()
            }
        }
    }
}

extension WeekPlanViewController : UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrWorkouts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: buildPlanWorkoutCellIdentifier, for: indexPath) as! BuildWorkoutPlanCell
        cell.isFromViewPlan = isFromMyPlan
        cell.isFromEditAdmin = isFromEditAdmin
        if isFromEditAdmin
        {
            cell.backgroundColor = UIColor.black
        }
        cell.setCellWith(workOut: arrWorkouts[indexPath.row], for: indexPath)
        cell.editTapped = { (iPath) in
            self.editIndexPath = iPath
            let buildListVc = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "BuildWorkOutListVC") as! BuildWorkOutListVC
            buildListVc.weekViewVC = self
            buildListVc.planID = self.buildPlanModel.customerProgramDetail[0].programId
            buildListVc.workoutDayModel = self.arrWorkouts[iPath.row]
            self.navigationController?.pushViewController(buildListVc, animated: true)
        }
        cell.viewWorkoutTapped = { (iPath) in
        }
        
        cell.workoutStatusTapped = { (iPath) in
            self.updateWorkoutStatus(indexPath: iPath)
        }
        
        cell.deleteTapped = { (iPath) in
            BuildPlanViewController.isDataChanged = true
            let workoutDay = self.arrWorkouts[indexPath.row]
            workoutDay.programWorkoutFlag = "0"
            workoutDay.programWorkoutGoodFor = ""
            workoutDay.programWorkoutId = "0"
            workoutDay.programWorkoutImage = ""
            workoutDay.programWorkoutName = "Rest Day"
            workoutDay.programWorkoutStatus = "0"
            workoutDay.programWorkoutTime = "0:"
            workoutDay.workoutId = "0"
            self.arrWorkouts[indexPath.row] = workoutDay
            self.tblWorkouts.reloadRows(at: [iPath], with: .automatic)
            self.reloadAfterEdit?(self.arrWorkouts,self.weekIndex)
     
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75.0
    }
    
    func deleteWorkoutAt(indexPath : IndexPath){
        guard let user = DoviesGlobalUtility.currentUser else {return}
        
        UIAlertController.showAlert(in: self, withTitle: AlertTitle.appName, message: AlertMessages.workoutDeleteWarning, cancelButtonTitle: "No", destructiveButtonTitle: "Delete", otherButtonTitles: nil) { (alertControler, action, actionButtonIndex) in
            if actionButtonIndex == 1{
                let weekModel = self.arrWorkouts[indexPath.row]
                var params = [String: Any]()
                params[WsParam.authCustomerId] = user.customerAuthToken
                params[WsParam.programId] = self.buildPlanModel.customerProgramDetail[0].programId
                params[WsParam.programWorkoutId] = weekModel.programWorkoutId
                params[WsParam.type] = "Delete"
                params[WsParam.workoutId] = weekModel.workoutId
                self.callDelateProgramAPI(params: params , deletedAt: indexPath)
            }
        }
    }
    
    func updateWorkoutStatus(indexPath : IndexPath){
        guard let user = DoviesGlobalUtility.currentUser else {return}
        let weekModel = arrWorkouts[indexPath.row]
        var params = [String: Any]()
        params[WsParam.authCustomerId] = user.customerAuthToken
        params[WsParam.programId] = self.buildPlanModel.customerProgramDetail[0].programId
        params[WsParam.programWorkoutId] = weekModel.programWorkoutId
        callUpdateWorkoutStatusAPI(params: params, updateAt: indexPath)

    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isFromMyPlan, arrWorkouts[indexPath.row].programWorkoutFlag.toBool()!{
            redirectToDetailScreen(indexPath: indexPath)
        }
    }
    
    func redirectToDetailScreen(indexPath: IndexPath){
        let featuredWorkOutDetailViewControlle = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "FeaturedWorkOutDetailViewController") as! FeaturedWorkOutDetailViewController
        featuredWorkOutDetailViewControlle.onWorkoutLogSuccess = {
            if self.arrWorkouts[indexPath.row].programWorkoutStatus == "0"{
                self.updateWorkoutStatus(indexPath : indexPath)
            }
        }
		if buildPlanModel.customerProgramDetail.count > 0{
			featuredWorkOutDetailViewControlle.programId = buildPlanModel.customerProgramDetail[0].programId
		}
        featuredWorkOutDetailViewControlle.navigationItem.title = self.arrWorkouts[indexPath.row].programWorkoutName.uppercased()
        featuredWorkOutDetailViewControlle.workoutId = self.arrWorkouts[indexPath.row].workoutId
        self.navigationController?.pushViewController(featuredWorkOutDetailViewControlle, animated: true)
        
    }

}

extension WeekPlanViewController : UITableViewDelegate{
    
}

//API Calls
extension WeekPlanViewController{
    /**
     This method deletes the workout in particualr week plan.
     - Parameter params: is used to remove the week plan information
     - Parameter index : is used to remove the week plan from the table after successful deletion from server.
     */
    
    func callDelateProgramAPI(params : [String : Any], deletedAt index : IndexPath){
        DoviesWSCalls.callEditProgramWorkoutAPI(dicParam: params, onSuccess: { [weak self](success, workoutModel, message) in
            guard let weakSelf = self else{return}
            if let model = workoutModel{
            weakSelf.arrWorkouts[index.row] = model
                weakSelf.tblWorkouts.reloadRows(at: [index], with: UITableViewRowAnimation.automatic)
            }
        }) { (error) in

        }
    }
    
    /**
     This method updates the workout in the week plan.
     - Parameter params: is used to update the week plan information
     - Parameter index : is used to update the week plan from the table after successful deletion from server.
     */
    func callUpdateWorkoutStatusAPI(params : [String : Any] , updateAt index : IndexPath){
        DoviesWSCalls.callUpdateProgramWorkoutStatusAPI(dicParam: params, onSuccess: { [weak self](success, response, message) in
            guard let weakSelf = self else{return}
            if success{
                weakSelf.arrWorkouts[index.row].programWorkoutStatus = weakSelf.arrWorkouts[index.row].programWorkoutStatus == "1" ? "0" : "1"
                weakSelf.tblWorkouts.reloadRows(at: [index], with: UITableViewRowAnimation.automatic)
            }
        }) { (error) in
            
        }
    }
}
