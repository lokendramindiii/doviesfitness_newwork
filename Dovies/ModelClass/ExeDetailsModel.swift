//
//	RootClass.swift
//
//	Create by hb on 13/3/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class ExerciseDetails : NSObject, NSCoding, NSCopying{
	var exerciseAccessLevel : String!
	var exerciseAmount : String!
	var exerciseAmountDisplay : String!
	var exerciseCategory : String!
	var exerciseDescription : String!
	var exerciseImage : String!
	var exerciseIsFavourite : Bool!
	var exerciseIsFeatured : String!
	var exerciseLevel : String!
	var exerciseName : String!
	var exerciseShareUrl : String!
	var exerciseVideo : String!
	var exerciseVideoDuration : String!

	var exerciseBodyParts : String!
	var exerciseEquipments : String!
	var exerciseId : String!
	var exerciseTags : String!
	var isLiked : Bool!

    var excerciseOption: String! = "Time"
    
    var timeValue: String! = UserDefaults.standard.string(forKey: "allTimerExcercises") ?? ""
    var repsValue: String! = UserDefaults.standard.string(forKey: "numberOfRepas") ?? ""
    var breakTimeValue : String! = ""
    var totalTimeValue : String! = UserDefaults.standard.string(forKey: "timeFinishEachExcercise") ?? ""
    var dictParams: [String:Any]?

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
        self.dictParams = dictionary
        
        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        {
            exerciseAccessLevel = "OPEN"
        }
        else
        {
            exerciseAccessLevel = dictionary["exercise_access_level"] as? String ?? ""
        }
        
		exerciseAmount = dictionary["exercise_amount"] as? String ?? ""
		exerciseAmountDisplay = dictionary["exercise_amount_display"] as? String ?? ""
		exerciseCategory = dictionary["exercise_category"] as? String ?? ""
		exerciseDescription = dictionary["exercise_description"] as? String ?? ""
		exerciseImage = dictionary["exercise_image"] as? String ?? ""
		exerciseIsFeatured = dictionary["exercise_is_featured"] as? String ?? ""
		exerciseLevel = dictionary["exercise_level"] as? String ?? ""
		exerciseName = dictionary["exercise_name"] as? String ?? ""
		exerciseShareUrl = dictionary["exercise_share_url"] as? String ?? ""
		exerciseVideo = dictionary["exercise_video"] as? String ?? ""
		exerciseVideoDuration = dictionary["exercise_video_duration"] as? String ?? ""
		
		exerciseBodyParts = dictionary["exercise_body_parts"] as? String ?? ""
		exerciseEquipments = dictionary["exercise_equipments"] as? String ?? ""
		exerciseId = dictionary["exercise_id"] as? String ?? ""
		exerciseTags = dictionary["exercise_tags"] as? String ?? ""

		if let likeStatus = dictionary["exercise_is_favourite"] as? String , likeStatus == "1"{
			exerciseIsFavourite = true
		}else if  let bLikeStatus = dictionary["exercise_is_favourite"] as? Bool{
			exerciseIsFavourite = bLikeStatus
		}else{
			exerciseIsFavourite = false
		}
		
		if let likeStatus = dictionary["is_liked"] as? String , likeStatus == "1"{
			isLiked = true
		}else if  let bLikeStatus = dictionary["is_liked"] as? Bool{
			isLiked = bLikeStatus
		}else{
			isLiked = false
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if exerciseAccessLevel != nil{
			dictionary["exercise_access_level"] = exerciseAccessLevel
		}
		if exerciseAmount != nil{
			dictionary["exercise_amount"] = exerciseAmount
		}
		if exerciseAmountDisplay != nil{
			dictionary["exercise_amount_display"] = exerciseAmountDisplay
		}
		if exerciseBodyParts != nil{
			dictionary["exercise_body_part"] = exerciseBodyParts
		}
		if exerciseCategory != nil{
			dictionary["exercise_category"] = exerciseCategory
		}
		if exerciseDescription != nil{
			dictionary["exercise_description"] = exerciseDescription
		}
		if exerciseImage != nil{
			dictionary["exercise_image"] = exerciseImage
		}
		if exerciseIsFavourite != nil{
			dictionary["exercise_is_favourite"] = exerciseIsFavourite
		}
		if exerciseIsFeatured != nil{
			dictionary["exercise_is_featured"] = exerciseIsFeatured
		}
		if exerciseLevel != nil{
			dictionary["exercise_level"] = exerciseLevel
		}
		if exerciseName != nil{
			dictionary["exercise_name"] = exerciseName
		}
		if exerciseShareUrl != nil{
			dictionary["exercise_share_url"] = exerciseShareUrl
		}
		if exerciseTags != nil{
			dictionary["exercise_tag"] = exerciseTags
		}
		if exerciseVideo != nil{
			dictionary["exercise_video"] = exerciseVideo
		}
		if exerciseVideoDuration != nil{
			dictionary["exercise_video_duration"] = exerciseVideoDuration
		}
        if excerciseOption != nil{
            dictionary["time"] = excerciseOption
        }
        if timeValue != nil{
            dictionary["time_value"] = timeValue
        }
        if repsValue != nil{
            dictionary["reps_value"] = repsValue
        }
        if breakTimeValue != nil{
            dictionary["break_time_value"] = breakTimeValue
        }
        if totalTimeValue != nil{
            dictionary["total_time_value"] = totalTimeValue
        }
        
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
        
        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        {
            exerciseAccessLevel = "OPEN"
        }
        else
        {
            exerciseAccessLevel = aDecoder.decodeObject(forKey: "exercise_access_level") as? String
        }
        
         exerciseAmount = aDecoder.decodeObject(forKey: "exercise_amount") as? String
         exerciseAmountDisplay = aDecoder.decodeObject(forKey: "exercise_amount_display") as? String
         exerciseBodyParts = aDecoder.decodeObject(forKey: "exercise_body_part") as? String
         exerciseCategory = aDecoder.decodeObject(forKey: "exercise_category") as? String
         exerciseDescription = aDecoder.decodeObject(forKey: "exercise_description") as? String
         exerciseImage = aDecoder.decodeObject(forKey: "exercise_image") as? String
         exerciseIsFavourite = aDecoder.decodeObject(forKey: "exercise_is_favourite") as? Bool
         exerciseIsFeatured = aDecoder.decodeObject(forKey: "exercise_is_featured") as? String
         exerciseLevel = aDecoder.decodeObject(forKey: "exercise_level") as? String
         exerciseName = aDecoder.decodeObject(forKey: "exercise_name") as? String
         exerciseShareUrl = aDecoder.decodeObject(forKey: "exercise_share_url") as? String
         exerciseTags = aDecoder.decodeObject(forKey: "exercise_tag") as? String
         exerciseVideo = aDecoder.decodeObject(forKey: "exercise_video") as? String
         exerciseVideoDuration = aDecoder.decodeObject(forKey: "exercise_video_duration") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if exerciseAccessLevel != nil{
			aCoder.encode(exerciseAccessLevel, forKey: "exercise_access_level")
		}
		if exerciseAmount != nil{
			aCoder.encode(exerciseAmount, forKey: "exercise_amount")
		}
		if exerciseAmountDisplay != nil{
			aCoder.encode(exerciseAmountDisplay, forKey: "exercise_amount_display")
		}
		if exerciseBodyParts != nil{
			aCoder.encode(exerciseBodyParts, forKey: "exercise_body_part")
		}
		if exerciseCategory != nil{
			aCoder.encode(exerciseCategory, forKey: "exercise_category")
		}
		if exerciseDescription != nil{
			aCoder.encode(exerciseDescription, forKey: "exercise_description")
		}
		if exerciseImage != nil{
			aCoder.encode(exerciseImage, forKey: "exercise_image")
		}
		if exerciseIsFavourite != nil{
			aCoder.encode(exerciseIsFavourite, forKey: "exercise_is_favourite")
		}
		if exerciseIsFeatured != nil{
			aCoder.encode(exerciseIsFeatured, forKey: "exercise_is_featured")
		}
		if exerciseLevel != nil{
			aCoder.encode(exerciseLevel, forKey: "exercise_level")
		}
		if exerciseName != nil{
			aCoder.encode(exerciseName, forKey: "exercise_name")
		}
		if exerciseShareUrl != nil{
			aCoder.encode(exerciseShareUrl, forKey: "exercise_share_url")
		}
		if exerciseTags != nil{
			aCoder.encode(exerciseTags, forKey: "exercise_tag")
		}
		if exerciseVideo != nil{
			aCoder.encode(exerciseVideo, forKey: "exercise_video")
		}
		if exerciseVideoDuration != nil{
			aCoder.encode(exerciseVideoDuration, forKey: "exercise_video_duration")
		}
        if excerciseOption != nil{
            aCoder.encode(excerciseOption, forKey: "time")
        }
        if timeValue != nil{
            aCoder.encode(timeValue, forKey: "time_value")
        }
        if repsValue != nil{
            aCoder.encode(repsValue, forKey: "reps_value")
        }
        if breakTimeValue != nil{
            aCoder.encode(breakTimeValue, forKey: "break_time_value")
        }
        if totalTimeValue != nil{
            aCoder.encode(totalTimeValue, forKey: "total_time_value")
        }

	}

    func copy(with zone: NSZone? = nil) -> Any
    {
        let exerciseDetails = ExerciseDetails(fromDictionary: dictParams!)
        return exerciseDetails
    }
}
