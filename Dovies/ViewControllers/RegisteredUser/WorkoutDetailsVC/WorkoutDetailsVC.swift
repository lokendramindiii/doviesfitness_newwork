//
//  WorkoutDetailsVC.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import CoreData
//This view controller used to show workout details
class WorkoutDetailsVC: FavoriteBaseVC,UISearchBarDelegate {
    
    enum ScreenType {
        case workOutList
        case workOutDetailList
        case favoriteWorkoutList
    }

    @IBOutlet var topLayoutConstraint: NSLayoutConstraint!
    @IBOutlet var constraintClearFileterH: NSLayoutConstraint!
    @IBOutlet var filterHeaderView: UIView!
    @IBOutlet var btnfilter: UIButton!

    @IBOutlet var IBDetailsTblView: UITableView!
    @IBOutlet var IBviewAddFavorite: UIView!
    @IBOutlet weak var tblFilter : UITableView!
    @IBOutlet weak var lblNoRecord: UILabel!
    @IBOutlet weak var emptyWorkoutsView: UIView!
    @IBOutlet weak var IBbtnExploreExercise: UIButton!
    @IBOutlet weak var IBbtnAddfavorite: UIButton!

    var searchButton: UIButton!
    // loks add flage for upcoming favrouite exercise screen
    // IBbtnExploreExercise hidden and IBbtnAddfavorite text change
    var Str_UpcomingFavrouite =  ""
    var IsVideoOpen = ""

    var filterSearchData = [FilterDataModel]()
    var arrSeachedFilter = [FilterDataModel]()
    var searchBar : UISearchBar!
    
    var arrSelectedExercises : [ExerciseDetails] = [ExerciseDetails]()
    var exerciseId: String!
    
    var  arrExeDetails: [ExerciseDetails] = [ExerciseDetails]()

    var  arrFilterData: [FilterGroup] = DoviesGlobalUtility.arrFilterData

    var arrFilterInfo : [[String : [String]]]?
    
    var reloadOnFilter:((_ filterData: [[String : [String]]])->())?
	
	var selectedRow = -1
	var	modelExerciseLibrary: ModelExerciseLibrary!
	
	var visibleCellsDuringAnimation = [UITableViewCell]()
	var screenType = ScreenType.workOutList
	
    var isFromCreateWorkout = false
    // validation for handling navigation vc  isFromWorkoutExeVC 
	var isFromWorkoutExeVC = false
    var searchString : String?
    var searchFilterData : FilterDataModel?
    
	let cellHeight:CGFloat = 108
	let cellVideoHeight:CGFloat = 120 + (185 * CGRect.widthRatio)
    
    //var playerHeight = 190 * CGRect.widthRatio
     var playerHeight = 179.5 * CGRect.widthRatio

    var currentPageIndex = 1
    var isLoadData = true
	
    var shouldLoadMore = true {
        didSet {
            if shouldLoadMore == false {
                IBDetailsTblView.tableFooterView = UIView()
            } else {
                IBDetailsTblView.tableFooterView = DoviesGlobalUtility.loadingFooter()
            }
        }
    }
	
	@IBOutlet weak var IBbottomCon: NSLayoutConstraint!
	
    @IBOutlet weak var activityIndicatorView : NVActivityIndicatorView!{
        didSet{
            activityIndicatorView.padding = 20
            activityIndicatorView.color = UIColor.lightGray
            activityIndicatorView.type = .circleStrokeSpin
            activityIndicatorView.startAnimating()

        }
    }
    
    override func viewDidLoad() {
       //  DoviesGlobalUtility.setFilterData()
        super.viewDidLoad()
		if IS_IPHONE_X{
			IBbottomCon.constant = 74
		}
		
        if Str_UpcomingFavrouite == "Favorite Exercises"
        {
           self.IBbtnExploreExercise.isHidden = true
           self.IBbtnAddfavorite.setTitle("No Favorite Exercise", for: .normal)
        }
        
        addNavigationWhiteBackButton()
        IBDetailsTblView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        IBDetailsTblView.register(UINib(nibName: "ExerciseDetailTableviewCell", bundle: nil), forCellReuseIdentifier: "ExerciseDetailTableviewCell")
        IBDetailsTblView.separatorStyle = .none
        updateUI()
        addRightNavigationButton2()
        
		let notificationCenter = NotificationCenter.default
		notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: Notification.Name.UIApplicationWillResignActive, object: nil)
        view.bringSubview(toFront: tblFilter)
        for model in DoviesGlobalUtility.arrFilterData{
            filterSearchData.append(contentsOf: model.list)
        }
        print(filterSearchData)
        tblFilter.register(UINib(nibName: "SearchTableCell", bundle: nil), forCellReuseIdentifier: "SearchTableCell")
        tblFilter.contentInset = UIEdgeInsetsMake(0, 0, 176, 0)
        if let screenTitle = self.navigationItem.title, !screenTitle.isEmpty{
            self.title = screenTitle.uppercased()
        }else{
            self.title = "Exercise library"
        }
        
        navigationController?.navigationBar.backgroundColor = UIColor.black
    }
	
	@objc func appMovedToBackground() {
		if selectedRow > -1{
			closeVideoPlayForCell()
		}
	}
    
    func updateUI(){
        if screenType == .workOutList {
            var params = [String: Any]()
            params[WsParam.filterCategory] = modelExerciseLibrary == nil ? exerciseId : modelExerciseLibrary.exerciseCategoryId
            if let strSearch = searchString{
                params[WsParam.filterKeyword] = strSearch
            }
            if let searchModel = searchFilterData{
                params[searchModel.groupKey] = searchModel.gmGoogforMasterId
            }
            if let filterInfo = arrFilterInfo, filterInfo.count > 0{
                for data in filterInfo{
                    var dictKeys = Array(data.keys)
                    if let arrStrings = data[dictKeys[0]]{
                        params[dictKeys[0]] = arrStrings.joined(separator: ",")
                    }
                }
            }
            IBDetailsTblView.showLoader()
            callExerciseListingAPI(params: params)
            addRightNavigationButton()
            
        } else {
            filterHeaderView.isHidden = true
            btnfilter.isHidden = true
            topLayoutConstraint.constant = -30
            
            if screenType == .workOutDetailList {
                IBDetailsTblView.showLoader()
                callExerciseDetailListingWS()
            }
        }
        setFilterReload()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
      //  self.navigationController?.navigationBar.barTintColor = UIColor.appNavColor()
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor : UIColor.white ,
            NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]

        
        if self.screenType == .favoriteWorkoutList && FavoriteVC.currentSelectedSegment == 0{
            self.callGetCustomerFavouritesAPI(apiCallingType: FavouriteApiType.Exercise.rawValue, completion: {[weak self] (success) in
                guard let weakSelf = self else {return}
                if success, let arr = self?.favoriteLists?.exercise {
                    weakSelf.arrExeDetails = arr
                    weakSelf.IBDetailsTblView.reloadData()
                }
                if weakSelf.arrExeDetails.count > 0
                {
                    weakSelf.IBviewAddFavorite.isHidden = true
                }
                else
                {
                    weakSelf.IBviewAddFavorite.isHidden = false
                }
                self?.activityIndicatorView.stopAnimating()
            })
           
        }
        if let filterInfo = self.arrFilterInfo{
            btnfilter.isHidden = false
            filterHeaderView.isHidden =  !(filterInfo.count > 0)
            topLayoutConstraint.constant = filterInfo.count > 0 ? 0 : -30

        }else{
            filterHeaderView.isHidden = true
            btnfilter.isHidden = true
            topLayoutConstraint.constant = -30
        }
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let filterInfo = self.arrFilterInfo{
            
            if IS_IPHONE_X
            {
                self.IBDetailsTblView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 70, right: 0)
            }
            else{
                self.IBDetailsTblView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 70, right: 0)
            }
        }
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if selectedRow > -1{
            let selectedCell = IBDetailsTblView.cellForRow(at: IndexPath(row: selectedRow, section: 0)) as! ExerciseDetailTableviewCell
            selectedCell.stopVideo()
        }
    }
    
    deinit{
		NotificationCenter.default.removeObserver(self, name: Notification.Name.UIApplicationWillResignActive, object: nil)
        if selectedRow > -1{
            let selectedCell = IBDetailsTblView.cellForRow(at: IndexPath(row: selectedRow, section: 0)) as! ExerciseDetailTableviewCell
            selectedCell.removePlayerFromView()
        }
    }
    
    //MARK: IBAction methods
    @IBAction func redirectToWorkOutTab(){
        self.redirectToItsTab(tabIndex: 1)
        
        let viewCont = GlobalUtility.topViewController(withRootViewController: (self.tabBarController?.selectedViewController)!)
        if viewCont.isKind(of: WorkoutExeVC.self){
            let workoutExeVC = viewCont as! WorkoutExeVC
            workoutExeVC.selectIndexForFavourite = 0
            workoutExeVC.segmentController.setSelectedSegmentAt(0, animated: false)
        }
    }
    
    @objc func didTapDetailsButton(){
        if let viewControllers = self.navigationController?.viewControllers{
            for viewController in viewControllers{
                if let filterVc = viewController as? FilterVC{
                    if let filterInfo = arrFilterInfo{
                        filterVc.arrSelctedModels = filterInfo
                    }
                    filterVc.filterCollView.reloadData()
                 self.navigationController?.popToViewController(filterVc, animated: true)
                    return
                }
            }
        }
        
        let filterVc = self.storyboard?.instantiateViewController(withIdentifier: "FilterVC")as! FilterVC
        if let filterInfo = arrFilterInfo{
            filterVc.arrSelctedModels = filterInfo
        }
        self.navigationController?.pushViewController(filterVc, animated: true)
    }
    
    @objc func buttonDetailTap(sender: UIButton) {
      /*
         loks comment code
         let detailsVC = self.storyboard?.instantiateViewController(withIdentifier: "WorkOutExerciseDescriptionDetail")as! WorkOutExerciseDescriptionDetail
        detailsVC.exerciseDetails = arrExeDetails[sender.tag]
        detailsVC.modalTransitionStyle = .crossDissolve
        detailsVC.modalPresentationStyle = .overCurrentContext
        self.present(detailsVC, animated: true, completion: nil)
        
      */
        let detailsVC = self.storyboard?.instantiateViewController(withIdentifier: "WorkOutExerciseDescription_Detail")as! WorkOutExerciseDescription_Detail
        detailsVC.exerciseDetails = arrExeDetails[sender.tag]
        detailsVC.modalTransitionStyle = .crossDissolve
        detailsVC.modalPresentationStyle = .overCurrentContext
        self.present(detailsVC, animated: true, completion: nil)
        
    }
    
    @IBAction func btnClearAllFilterTapped(_ sender: UIButton){
        if screenType == .workOutList {
            searchButton.isHidden = true
            btnfilter.isHidden = true
            topLayoutConstraint.constant = -30
            currentPageIndex = 1
            shouldLoadMore = false
            var params = [String: Any]()
            params[WsParam.filterCategory] = modelExerciseLibrary == nil ? exerciseId : modelExerciseLibrary.exerciseCategoryId
            arrFilterInfo = [[String : [String]]]()
            for filterGroup in arrFilterData{
                arrFilterInfo?.append([filterGroup.groupKey : [String]()])
            }
            callExerciseListingAPI(params: params)
            
            if isFromWorkoutExeVC == true{
                
                for controller in self.navigationController!.viewControllers as Array {
                    if controller.isKind(of: WorkoutExeVC.self) {
                        self.navigationController!.popToViewController(controller, animated: true)
                        break
                    }
                    else
                    {
                        print("No object workoutexevc")
                    }
                }
            }
        }
    }

    //MARK: - custom functions
    func addRightNavigationButton2(){
        searchButton = UIButton(type: .custom)
        searchButton.setImage(UIImage(named:"btn_search"), for: .normal)
        searchButton.setImage(UIImage(named:"btn_search"), for: .highlighted)

        searchButton.addTarget(self, action: #selector(self.didTapSearchButton), for: .touchUpInside)
        searchButton.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
        searchButton.isHidden = true
        let searchBarItem = UIBarButtonItem(customView: searchButton)
        if let barItems = navigationItem.rightBarButtonItems{
            var items = [UIBarButtonItem]()
            items.append(contentsOf: barItems)
            items.append(searchBarItem)
            navigationItem.rightBarButtonItems = items
        }
    }
    
    @objc func didTapSearchButton(){
        arrSeachedFilter = [FilterDataModel]()
        tblFilter.reloadData()
        navigationItem.leftBarButtonItems = nil;
        navigationItem.rightBarButtonItem = nil;
        navigationItem.titleView?.isHidden = true
        searchBar = UISearchBar(frame:CGRect(origin: CGPoint(x: 40,y :0), size: CGSize(width: self.view.frame.size.width - 40, height: 44)))
        searchBar.backgroundColor = UIColor.clear
        searchBar.placeholder = "Search"
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        if let txfSearchField = searchBar.value(forKey: "_searchField") as? UITextField {
            txfSearchField.backgroundColor = UIColor.colorWithRGB(r: 222, g: 222, b: 222)
        }
        let leftNavBarButton = UIBarButtonItem(customView:searchBar)
        self.navigationItem.rightBarButtonItem = leftNavBarButton
        self.navigationItem.leftBarButtonItem = nil;
        searchBar.becomeFirstResponder()
        
        view.bringSubview(toFront: self.lblNoRecord)
    }
    
    func setFilterReload(){
        reloadOnFilter = { (filterData) in
            self.arrFilterInfo = filterData
            var params = [String: Any]()
            params[WsParam.filterCategory] = self.modelExerciseLibrary == nil ? self.exerciseId : self.modelExerciseLibrary.exerciseCategoryId
            for data in filterData{
                var dictKeys = Array(data.keys)
                if let arrStrings = data[dictKeys[0]]{
                    params[dictKeys[0]] = arrStrings.joined(separator: ",")
                }
            }
            self.currentPageIndex = 1
            self.callExerciseListingAPI(params: params)
        }
    }
    
    func addRightNavigationButton(){
        let detailsButton = UIButton(type: .custom)
        detailsButton.setImage(UIImage(named:"btn_filterNew"), for: .normal)
        detailsButton.setImage(UIImage(named:"btn_filterNew"), for: .highlighted)

        detailsButton.addTarget(self, action: #selector(self.didTapDetailsButton), for: .touchUpInside)
        detailsButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        let detailsBarItem = UIBarButtonItem(customView: detailsButton)
        if isFromCreateWorkout{
            let rightButton = GlobalUtility.getNavigationButtonItemWhiteAndRedWith(target: self, selector: #selector(self.saveTapped), title: "Done")
            
            navigationItem.rightBarButtonItems = [detailsBarItem, rightButton]
            return
        }
        navigationItem.rightBarButtonItems = [detailsBarItem]
        
    }
    
    @objc func saveTapped(){
        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        {
            for obj in CreateWorkoutAdminVC.arrTempExercises{
                CreateWorkoutAdminVC.arrExercise.append(obj)
                if let viewControllers = self.navigationController?.viewControllers{
                    for controller in viewControllers{
                        if controller is CreateWorkoutAdminVC{
                            
                            CreateWorkoutAdminVC.arrTempExercises = [ExerciseDetails]()
                            self.navigationController?.popToViewController(controller, animated: true)
                        }
                    }
                }
            }
        }
        else
        {
            for obj in CreateWorkoutVC.arrTempExercises{
                CreateWorkoutVC.arrExercise.append(obj)
                if let viewControllers = self.navigationController?.viewControllers{
                    for controller in viewControllers{
                        if controller is CreateWorkoutVC{
                            CreateWorkoutVC.arrTempExercises = [ExerciseDetails]()
                            self.navigationController?.popToViewController(controller, animated: true)
                        }
                    }
                }
            }
        }
    }
    
    //MARK: - didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
	
    @objc func didTapVideoPlayButton(sender : UIButton!) {
        
        let idxPath : IndexPath = IndexPath(row: sender.tag - 100, section: 0)
        self.openVideoUIForTableCell(indexPath: idxPath )
    }
    
	func openVideoUIForTableCell(indexPath: IndexPath)
	{
		if selectedRow == -1 {
			selectedRow = indexPath.row
            IsVideoOpen = "OPEN"
			IBDetailsTblView.isScrollEnabled = false
			let selectedCell = IBDetailsTblView.cellForRow(at: indexPath) as! ExerciseDetailTableviewCell
            selectedCell.btnvideo.setImage(UIImage(named: "icon_arrow_dropdUp"), for: .normal)
            
			//selectedCell.contentView.backgroundColor = UIColor.colorWithRGB(r: 244, g: 244, b: 244)
			self.visibleCellsDuringAnimation = IBDetailsTblView.visibleCells
			selectedCell.setUpPlayerInCell(urlString: arrExeDetails[indexPath.row].exerciseVideo)
            selectedCell.constPlayerHeight.constant = playerHeight
			self.IBDetailsTblView.beginUpdates()
			self.IBDetailsTblView.endUpdates()
			
			IBDetailsTblView.scrollToRow(at: indexPath, at: .none, animated: true)
		}
        else
        {
            self.closeVideoPlayForCell()
        }
	}
	
	func closeVideoPlayForCell(){
        guard let selectedCell = IBDetailsTblView.cellForRow(at: IndexPath(row: selectedRow, section: 0)) as? ExerciseDetailTableviewCell else{return}
		//selectedCell.contentView.backgroundColor = UIColor.white
        selectedCell.btnvideo.setImage(UIImage(named: "icon_downarrow"), for: .normal)

		selectedRow = -1
		self.IBDetailsTblView.beginUpdates()
		self.IBDetailsTblView.endUpdates()
		
		for cell in self.self.visibleCellsDuringAnimation {
			if let theCell = cell as? ExerciseDetailTableviewCell {
				//theCell.viewUpperLayer.isHidden = true
				//theCell.contentView.backgroundColor = UIColor.white
			}
		}
        selectedCell.constPlayerHeight.constant = 0
        //selectedCell.viewUpperLayer.isHidden = true
		selectedCell.stopVideo()
		selectedCell.removePlayerFromView()
		IBDetailsTblView.isScrollEnabled = true
        IsVideoOpen = "CLOSED"

	}
	
	func createWorkoutArrayContainsExercise(exerciseObj: ExerciseDetails) -> Bool {
		let arrContainsSelectedObject = CreateWorkoutVC.arrExercise.contains(where: { (obj) -> Bool in
			if let theObj = obj as? ExerciseDetails {
				if theObj.exerciseId == exerciseObj.exerciseId {
					return true
				}
			}
			return false
		})
		return arrContainsSelectedObject
	}
	
	func indexOfObjectInCreateWorkoutArray(exerciseObj: ExerciseDetails) -> Int? {
		if let indexOfObj = CreateWorkoutVC.arrExercise.index(where: { (obj) -> Bool in
			if let theObj = obj as? ExerciseDetails {
				if theObj.exerciseId == exerciseObj.exerciseId {
					return true
				}
			}
			return false
		}) {
			return indexOfObj
		}
		return nil
	}
    
    //MARK: -  Searchbar Delegate Methods
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // Do some search stuff
        DispatchQueue.main.async {
            self.arrSeachedFilter = self.filterSearchData.filter({$0.gmDisplayName.lowercased().contains(searchText.lowercased())})
            self.tblFilter.reloadData()
        }
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        tblFilter.isHidden = false
       
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        tblFilter.isHidden = true
        searchBar.resignFirstResponder()
        searchString = searchBar.text
        updateUI()
    }
    
    internal func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // Stop doing the search stuff
        // and clear the text in the search bar
        searchBar.isHidden = true
        tblFilter.isHidden = true
        searchBar.text = ""
        // Hide the cancel button
        searchBar.showsCancelButton = false
        
        addRightNavigationButton()
        addRightNavigationButton2()
        searchButton.isHidden = true
        addNavigationBackButton()
        self.lblNoRecord.isHidden = true
    }
    
}

extension WorkoutDetailsVC: UITableViewDataSource{
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == tblFilter{
            if self.arrSeachedFilter.count == 0,searchBar != nil , searchBar.text != nil {
                self.lblNoRecord.isHidden = false
            }else{
                self.lblNoRecord.isHidden = true
            }
            return arrSeachedFilter.count
        }else{
            return arrExeDetails.count
        }
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == tblFilter{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableCell") as! SearchTableCell
            if indexPath.row < arrSeachedFilter.count{
                cell.lblTitle.text = arrSeachedFilter[indexPath.row].gmDisplayName
                cell.lblGroupName.text = arrSeachedFilter[indexPath.row].groupName
            }
            return cell
        }
		let cell = tableView.dequeueReusableCell(withIdentifier: "ExerciseDetailTableviewCell") as? ExerciseDetailTableviewCell
		
		let exerciseObj = arrExeDetails[indexPath.row]
		
		if self.isFromCreateWorkout {
			cell?.setupForCreateWorkout()
			//cell?.constLockFavTop.priority = UILayoutPriority(rawValue: 750)
			//cell?.constlockMiddleTop.priority = UILayoutPriority(rawValue: 250)
		}
		else{
		//	cell?.constLockFavTop.priority = UILayoutPriority(rawValue: 250)
		//	cell?.constlockMiddleTop.priority = UILayoutPriority(rawValue: 750)
		}

        cell?.setData(exerciseObj, iPath: indexPath)
        
		cell?.clipsToBounds = true
       // cell?.btnDetails.tag = indexPath.row
       // cell?.btnDetails.addTarget(self, action: #selector(self.buttonDetailTap(sender:)), for: .touchUpInside)
        //cell?.btnDetails.tag = indexPath.row
		cell?.selectionStyle = .none
        
        cell?.btnvideo.tag = indexPath.row + 100
        cell?.btnvideo.addTarget(self, action: #selector(self.didTapVideoPlayButton), for: .touchUpInside)

        cell?.imgViewCheckBox.isHighlighted = arrSelectedExercises.contains(exerciseObj)
		cell?.likeTapped = { () in
            if self.IsVideoOpen == "OPEN"
            {
                self.closeVideoPlayForCell()
                return
            }
            
			if self.screenType != .favoriteWorkoutList{
				cell?.IBbtnFavo.isSelected = !(cell?.IBbtnFavo.isSelected)!
				self.arrExeDetails[indexPath.row].exerciseIsFavourite = !self.arrExeDetails[indexPath.row].exerciseIsFavourite
				self.callExerciseFavouriteAPI(with: self.arrExeDetails[indexPath.row])
			}
			else{
				self.callExerciseFavouriteAPI(with: self.arrExeDetails[indexPath.row])
				self.arrExeDetails.remove(at: indexPath.row)
				tableView.reloadData()
				if self.arrExeDetails.count == 0{
					self.IBviewAddFavorite.isHidden = false
                    self.activityIndicatorView.startAnimating()

				}
			}
		}
		
		cell?.shareTapped = { () in
            
            if self.IsVideoOpen == "OPEN"
            {
                self.closeVideoPlayForCell()
                return
            }
            
            DoviesGlobalUtility.showShareSheet(on: self, shareText: self.arrExeDetails[indexPath.row].exerciseShareUrl)

		}
        
		cell?.imageTapped = { (iPath) in
            if self.IsVideoOpen == "OPEN"
            {
                self.closeVideoPlayForCell()
                return
            }
            if iPath.row < self.arrExeDetails.count{
                let exeDetails = self.arrExeDetails[iPath.row]
                if exeDetails.exerciseAccessLevel.uppercased() == "OPEN".uppercased() {
                    self.openVideoUIForTableCell(indexPath: iPath)
                }else{
                    if let upgradeVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "UpgradeToPremiumViewController") as? UpgradeToPremiumViewController{
                        upgradeVC.onPurchaseSuccess = {
                            self.updateUI()
                        }
                    self.navigationController?.pushViewController(upgradeVC, animated: true)
                    }
                }
            }
		}
		
		cell?.videoCloseBtnTap = { ()
			self.closeVideoPlayForCell()
		}
        
        cell?.videoUpperLayerTapped = {
            self.closeVideoPlayForCell()
        }
        
        cell?.selectionTappedAt = { (selIndexPath) in
            if self.IsVideoOpen == "OPEN"
            {
                self.closeVideoPlayForCell()
                return
            }
            
            if self.isFromCreateWorkout{
                if indexPath.row < self.arrExeDetails.count{
                    let exeDetails = self.arrExeDetails[indexPath.row]
                    if exeDetails.exerciseAccessLevel.uppercased() == "OPEN".uppercased()
                    {
                        let selectedObj = self.arrExeDetails[indexPath.row]
                        let cell = tableView.cellForRow(at: indexPath) as! ExerciseDetailTableviewCell
                        cell.imgViewCheckBox.isHighlighted = !cell.imgViewCheckBox.isHighlighted
                        if cell.imgViewCheckBox.isHighlighted == true{
                            self.arrSelectedExercises.append(selectedObj)
                          
                            if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
                            {
                                CreateWorkoutAdminVC.arrTempExercises.append(selectedObj)
                            }
                            else
                            {
                                CreateWorkoutVC.arrTempExercises.append(selectedObj)
                            }

                        }else{
                            if let indexOfObj = self.arrSelectedExercises.index(of: selectedObj){
                                self.arrSelectedExercises.remove(at: indexOfObj)
                            }
                            
                            if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
                            {
                                if let indexOfObj = CreateWorkoutAdminVC.arrTempExercises.index(of: selectedObj){
                                    CreateWorkoutAdminVC.arrTempExercises.remove(at: indexOfObj)
                                }
                            }
                            else
                            {
                                if let indexOfObj = CreateWorkoutVC.arrTempExercises.index(of: selectedObj){
                                    CreateWorkoutVC.arrTempExercises.remove(at: indexOfObj)
                                }
                            }
                        }
                    }else{
                        if let upgradeVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "UpgradeToPremiumViewController") as? UpgradeToPremiumViewController{
                            upgradeVC.onPurchaseSuccess = {
                                self.updateUI()
                            }
                            self.navigationController?.pushViewController(upgradeVC, animated: true)
                        }
                    }
                }
            }
        }
		
		return cell!
	}
}

extension WorkoutDetailsVC: UITableViewDelegate{
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if tableView == tblFilter{
            return 60
        }else{
            var extraheight : CGFloat = 0
            let exerciseObj = arrExeDetails[indexPath.row]
            if let name : String = exerciseObj.exerciseName{
                let height = name.heightWithConstrainedWidth(width: ScreenSize.width - 148, font: UIFont.tamilRegular(size: 14.0), lineSpace: 5.0)
                extraheight = height > 30 ? 20 : 0
            }
            print(extraheight)
            if self.selectedRow == indexPath.row{
                return cellVideoHeight - 18
            }else{
                return cellHeight 
            }
        }
        
        
//        if tableView == tblFilter{
//            return 60
//        }else{
//            var extraheight : CGFloat = 0
//            let exerciseObj = arrExeDetails[indexPath.row]
//            if let name : String = exerciseObj.exerciseName{
//                let height = name.heightWithConstrainedWidth(width: ScreenSize.width - 148, font: UIFont.tamilRegular(size: 14.0), lineSpace: 5.0)
//                extraheight = height > 30 ? 20 : 0
//            }
//            print(extraheight)
//            if self.selectedRow == indexPath.row{
//                return cellVideoHeight + extraheight
//            }else{
//                return cellHeight + extraheight
//            }
//        }
	}
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblFilter{
            return 60
        }else{
            return cellHeight
        }
    }
    	
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == tblFilter{
            guard  indexPath.row < arrSeachedFilter.count else{return}
            self.searchFilterData  = arrSeachedFilter[indexPath.row]
            tblFilter.isHidden = true
            searchBar.resignFirstResponder()
            updateUI()
            return
        }
		if indexPath.row < self.arrExeDetails.count{
			let exeDetails = self.arrExeDetails[indexPath.row]
			if exeDetails.exerciseAccessLevel.uppercased() == "OPEN".uppercased()
            {
                if selectedRow == -1
                {
                    self.openVideoUIForTableCell(indexPath: indexPath)
                }
                else
                {
                    self.closeVideoPlayForCell()
                }
                
			}else{
				if let upgradeVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "UpgradeToPremiumViewController") as? UpgradeToPremiumViewController{
                    
                    upgradeVC.onPurchaseSuccess =
                        {
                            self.updateUI()
                    }
                    if self.screenType == .favoriteWorkoutList{
                        upgradeVC.isFromFavExercise = true
                        self.navigationController?.pushViewController(upgradeVC, animated: false)
                    }
                    else
                    {
                        self.navigationController?.pushViewController(upgradeVC, animated: true)
                    }

				}
			}
		}
	}
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == tblFilter{return}
        if indexPath.row == (arrExeDetails.count - 1) && shouldLoadMore == true {
            if screenType == .workOutDetailList {
                callExerciseDetailListingWS()
            }else if screenType == .workOutList{
                var params = [String: Any]()
                params[WsParam.filterCategory] = modelExerciseLibrary == nil ? exerciseId : modelExerciseLibrary.exerciseCategoryId
                if let strSearch = searchString{
                    params[WsParam.filterKeyword] = strSearch
                }
                if let searchModel = searchFilterData{
                    params[searchModel.groupKey] = searchModel.gmGoogforMasterId
                }
                if let filterInfo = arrFilterInfo, filterInfo.count > 0{
                    for data in filterInfo{
                        var dictKeys = Array(data.keys)
                        if let arrStrings = data[dictKeys[0]]{
                            params[dictKeys[0]] = arrStrings.joined(separator: ",")
                        }
                    }
                }
                callExerciseListingAPI(params: params)
            }
        }
    }
}

//MARK: - Exercise Details APIs
extension WorkoutDetailsVC{
	
    /**
     This method used to call feed listing WS
     */
	func callExerciseDetailListingWS(){
		guard let cUser = DoviesGlobalUtility.currentUser else {return}
		var dic = [String: Any]()
        dic[WsParam.authCustomerId] = cUser.customerAuthToken
		dic[WsParam.exerciseId] = exerciseId
		dic[WsParam.pageIndex] = "\(currentPageIndex)"
		DoviesWSCalls.callGetExerciseDetailsAPI(dicParam: dic, onSuccess: { (sucess, arrExercise, strMessage, hasNextPage) in
			GlobalUtility.hideActivityIndi(viewContView: self.view)
            self.shouldLoadMore = hasNextPage
            self.IBDetailsTblView.hideLoader()
			if sucess{
                if self.currentPageIndex == 1, let exercises = arrExercise{
                    self.arrExeDetails = exercises   
                }else if let exercises = arrExercise{
                    self.arrExeDetails += exercises
                }
				self.IBDetailsTblView.reloadData()
                self.activityIndicatorView.stopAnimating()
			}
            else
            {
                self.activityIndicatorView.stopAnimating()
                GlobalUtility.showToastMessage(msg: "No longer available.")
            }

            if hasNextPage{
                self.currentPageIndex += 1
            }
		}) { (error) in
            self.activityIndicatorView.stopAnimating()
		}
	}
	
    /**
     This method used to call excercise list and update the tableview.
     */
	func callExerciseListingAPI(params : [String: Any]){
        var apiParams = params
        apiParams[WsParam.pageIndex] = "\(currentPageIndex)"
        apiParams[WsParam.authCustomerSubscription] = DoviesGlobalUtility.currentUser?.customerUserType ?? ""
		DoviesWSCalls.callExerciseListingAPI(dicParam: apiParams, onSuccess: { (sucess, arrExercise, strMessage, hasNextPage) in
			GlobalUtility.hideActivityIndi(viewContView: self.view)
            self.shouldLoadMore = hasNextPage
            self.IBDetailsTblView.hideLoader()
			if sucess, let arr = arrExercise{
                
                if self.currentPageIndex == 1{
                    
                 //Exercise_List.saveContactInDBExerciseListEntity(ExerciseArray:arr)
                    
                    self.arrExeDetails = arr
                    self.emptyWorkoutsView.isHidden = arr.count > 0
                }else{
                    
                //Exercise_List.saveContactInDBExerciseListEntity(ExerciseArray:arr)

                    self.arrExeDetails += arr
                }
                if hasNextPage{
                    self.currentPageIndex += 1
                }
            }else{
                if self.currentPageIndex == 1{
                    self.emptyWorkoutsView.isHidden = false
                    self.arrExeDetails = [ExerciseDetails]()
                }
            }
            self.activityIndicatorView.stopAnimating()

            self.IBDetailsTblView.reloadData()
		}) { (error) in
            
            self.activityIndicatorView.stopAnimating()
		}
	}
	
    /**
     This method used to call favourite exercise api and update the tableview.
     */
	func callExerciseFavouriteAPI(with exerciseDetails : ExerciseDetails){
		guard let cUser = DoviesGlobalUtility.currentUser else {return}
		var dic = [String: Any]()
		dic[WsParam.authCustomerId] = cUser.customerAuthToken
		dic[WsParam.moduleId] = exerciseDetails.exerciseId
		dic[WsParam.moduleName] = ModuleName.exercise
		
		DoviesWSCalls.callFavouriteAPI(dicParam: dic, onSuccess: { (success, message, responseDict) in
            self.IBDetailsTblView.hideLoader()
            self.activityIndicatorView.stopAnimating()

			if !success{
				if self.screenType != .favoriteWorkoutList{
					exerciseDetails.exerciseIsFavourite = !exerciseDetails.exerciseIsFavourite
				}
			}
            self.activityIndicatorView.stopAnimating()

		}) { (error) in
            self.activityIndicatorView.stopAnimating()
		}
	}
}


