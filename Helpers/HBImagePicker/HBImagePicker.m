//
//  HBImagePicker.m
//  HBImagePicker
//
//  Created by Venky on 3/12/15.
//  Copyright (c) 2015 Hidden Brains. All rights reserved.
//

#import "HBImagePicker.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <UIKit/UIKit.h>
@interface HBImagePicker () <UIImagePickerControllerDelegate, UINavigationControllerDelegate,UIActionSheetDelegate>
@end
@implementation HBImagePicker{
    ImagePickerCompletionHandler _completionHandler;
    CFStringRef _media;
    
    BOOL isEditing;
}

+(HBImagePicker*) sharedInstance{
    static dispatch_once_t once;
    static HBImagePicker * sharedInstance;
    dispatch_once(&once, ^{
                sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

- (void)openImagePickerWithMediaType:(enum mediaType)media actionSheetTitle:(NSString *)title cancelButtonTitle:(NSString *)cancelTitle cameraButtonTitle:(NSString *)cameraTitle  galleryButtonTitle:(NSString *)galleryTitle canEdit:(BOOL)canEdit withCompletion:(ImagePickerCompletionHandler)completionHandler{
    
    
    isEditing =canEdit;
    
    if (media == MediaTypeImage) {
        _media = kUTTypeImage;
    }else {
       _media = kUTTypeMovie;
    }
    _completionHandler = [completionHandler copy];
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:title delegate:self cancelButtonTitle:cancelTitle destructiveButtonTitle:nil otherButtonTitles:
                           cameraTitle,galleryTitle,nil];
    popup.tag = 1;
    [popup showInView:[UIApplication sharedApplication].keyWindow];

}

- (void)openCameraEdit:(BOOL)canEdit completion:(ImagePickerCompletionHandler)completionHandler{
    
        _media = kUTTypeImage;
        isEditing = canEdit;
        _completionHandler = [completionHandler copy];
        [self openCamera];
    
}

- (void)openPhotoVideo:(BOOL)canEdit completion:(ImagePickerCompletionHandler)completionHandler{
    isEditing = canEdit;
    _completionHandler = [completionHandler copy];
    [self openCameraForPhotoVideo];
}




- (void)openVideoImagePickerWithactionSheetTitle:(NSString *)title cancelButtonTitle:(NSString *)cancelTitle captureImageTitle:(NSString *)captureImageTitle  pickImageTitle:(NSString *)pickImageTitle captureVideoTitle:(NSString *)captureVideoTitle  pickVideoTitle:(NSString *)pickVideoTitle canEdit:(BOOL)canEdit withCompletion:(ImagePickerCompletionHandler)completionHandler{
    
        isEditing =canEdit;
    _completionHandler = [completionHandler copy];
    
    UIActionSheet *popup = [[UIActionSheet alloc] initWithTitle:title delegate:self cancelButtonTitle:cancelTitle destructiveButtonTitle:nil otherButtonTitles:
                            captureImageTitle,pickImageTitle,captureVideoTitle,pickVideoTitle,nil];
    popup.tag = 2;
    [popup showInView:[UIApplication sharedApplication].keyWindow];
    
    
}



- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if(popup.tag==1){
        
        switch (buttonIndex) {
            case 0:
                [self openCamera];
                break;
            case 1:
                [self openGallery];
                break;
            default:
                break;
        }
    }
    else if(popup.tag==2){
        
        switch (buttonIndex) {
            case 0:
                _media = kUTTypeImage;
                [self openCamera];
                break;
            case 1:
                _media = kUTTypeImage;
                [self openGallery];
                break;
            case 2:
                _media = kUTTypeVideo;
                [self openCameraForVideo];
                break;
            case 3:
                _media = kUTTypeVideo;
                [self openGalleryForVideo];
                break;
            default:
                break;
        }
    }
}

- (void)openGallery {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    

    picker.delegate = self;
    picker.allowsEditing = isEditing;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.mediaTypes = [[NSArray alloc] initWithObjects:(__bridge NSString *)_media, nil];

    UIViewController *controller = [[UIApplication sharedApplication].delegate window].rootViewController;
    [controller presentViewController:picker animated:YES completion:nil];

}

- (void)openCameraForPhotoVideo {
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        _completionHandler(NO, nil );
        _completionHandler = nil;
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Device has no camera" delegate:nil
                                                    cancelButtonTitle:@"OK" otherButtonTitles : nil];
        [myAlertView show];
    } else {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = isEditing;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        
        picker.videoQuality = UIImagePickerControllerQualityTypeMedium;
        
        picker.showsCameraControls=YES;
        picker.videoMaximumDuration=30;
        
        
        //picker.mediaTypes = [[NSArray alloc] initWithObjects:(__bridge NSString *)_media, nil];
        picker.mediaTypes = [[NSArray alloc] initWithObjects:(__bridge NSString *)kUTTypeImage, kUTTypeMovie, nil];
        UIViewController *controller = [[UIApplication sharedApplication].delegate window].rootViewController;
        [controller presentViewController:picker animated:YES completion:nil];
    }
}

- (void)openCamera {
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        _completionHandler(NO, nil );
        _completionHandler = nil;
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Device has no camera" delegate:nil
                                                    cancelButtonTitle:@"OK" otherButtonTitles : nil];
        [myAlertView show];
    } else {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = isEditing;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.mediaTypes = [[NSArray alloc] initWithObjects:(__bridge NSString *)_media, nil];
        UIViewController *controller = [[UIApplication sharedApplication].delegate window].rootViewController;
        [controller presentViewController:picker animated:YES completion:nil];
    }
}
- (void)openGalleryForVideo {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        
    picker.delegate = self;
    picker.allowsEditing = isEditing;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.videoMaximumDuration =40;
    picker.videoQuality = UIImagePickerControllerQualityTypeMedium;
    picker.mediaTypes = @[(NSString*)kUTTypeMovie, (NSString*)kUTTypeAVIMovie, (NSString*)kUTTypeVideo, (NSString*)kUTTypeMPEG4];
    
    UIViewController *controller = [[UIApplication sharedApplication].delegate window].rootViewController;
    [controller presentViewController:picker animated:YES completion:nil];
    
}

- (void)openCameraForVideo {
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        _completionHandler(NO, nil );
        _completionHandler = nil;
        UIAlertView *myAlertView = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Device has no camera" delegate:nil
                                                    cancelButtonTitle:@"OK" otherButtonTitles : nil];
        [myAlertView show];
    } else {
        
        
        
        UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
        
        mediaUI.sourceType = UIImagePickerControllerSourceTypeCamera;
        mediaUI.mediaTypes = [[NSArray alloc] initWithObjects: (NSString *) kUTTypeMovie, nil];
        mediaUI.videoQuality = UIImagePickerControllerQualityTypeMedium;
        // Hides the controls for moving & scaling pictures, or for
        // trimming movies. To instead show the controls, use YES.
        mediaUI.allowsEditing = isEditing;
        mediaUI.showsCameraControls=YES;
        mediaUI.videoMaximumDuration=40;
        mediaUI.delegate = self;
        
       /*
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = isEditing;
        picker.videoMaximumDuration =30;
        picker.showsCameraControls=YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        NSArray *mediaTypes = [[NSArray alloc]initWithObjects:(__bridge NSString *)_media, nil];
        picker.mediaTypes = mediaTypes;
        */
        
        
        UIViewController *controller = [[UIApplication sharedApplication].delegate window].rootViewController;
        [controller presentViewController:mediaUI animated:YES completion:nil];
    }
}

#pragma image picker delegate methods

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    
    
     NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    //Handle picture
    if (CFStringCompare ((CFStringRef) mediaType, kUTTypeImage, 0) == kCFCompareEqualTo) {
        if(isEditing){
            [dict setObject:info[UIImagePickerControllerEditedImage] forKey:@"image"];
        }else{
            [dict setObject:info[UIImagePickerControllerOriginalImage] forKey:@"image"];
        }
    }
    // Handle a movie capture
    else if (CFStringCompare ((CFStringRef) mediaType, kUTTypeMovie, 0) == kCFCompareEqualTo) {
        if([info objectForKey:UIImagePickerControllerMediaURL]){
            [dict setObject:[[info objectForKey: UIImagePickerControllerMediaURL] path] forKey:@"video"];
        }else{
            [dict setObject:[[info objectForKey: UIImagePickerControllerReferenceURL] path] forKey:@"video"];
        }
    }
	
    [picker dismissViewControllerAnimated:YES completion:^{
        _completionHandler(YES, dict);
        _completionHandler = nil;
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:^{
        _completionHandler(NO, nil);
        _completionHandler = nil;
    }];
}

#pragma mark- Image resizing

-(UIImage *)reduceImageSize:(UIImage *)img squareImgWidth:(float)imgWidth{
    
    img = [self rotateImage:img];
    float retinaPX=[[UIScreen mainScreen] scale];
    CGSize newSize;
    
    
    CGFloat maxWidth=imgWidth;
    //CGFloat maxHeight=400;
    
    newSize =CGSizeMake(img.size.width,img.size.height);
    if(img.size.width>maxWidth){
        
        float height =0;
        float width =maxWidth;
        //height =(width/img.size.width)*img.size.height;
        height = width;
        retinaPX=1;
        
        newSize=CGSizeMake(width/retinaPX,height/retinaPX);
        
        
    }
    
    newSize =CGSizeMake(imgWidth,imgWidth);
    
    
    UIImage * temp = [self makeResizedImage:newSize quality:kCGInterpolationMedium withImage:img isAspectFit:NO];
    
    if(temp == nil){
        temp = img;
    }
    return temp;
    
}



-(UIImage *)reduceImageSize:(UIImage *)img imgWidth:(float)imgWidth{
    
    img = [self rotateImage:img];
    float retinaPX=[[UIScreen mainScreen] scale];
    CGSize newSize;
    
    
    CGFloat maxWidth=imgWidth;
    //    CGFloat maxHeight=400;
    
    newSize =CGSizeMake(img.size.width,img.size.height);
    
    if(img.size.width>maxWidth){
        
        float height =0;
        float width =maxWidth;
        height =(width/img.size.width)*img.size.height;
        retinaPX=1;
		
		newSize=CGSizeMake(width/retinaPX,height/retinaPX);
		
        
    }
    
    UIImage * temp = [self makeResizedImage:newSize quality:kCGInterpolationMedium withImage:img isAspectFit:YES];
    
    if(temp == nil){
        temp = img;
    }
    return temp;
    
}

-(UIImage *)compressImage:(UIImage *)img {
	
	img = [self rotateImage:img];
	float retinaPX=[[UIScreen mainScreen] scale];
	CGSize newSize;
	
	
	
		newSize =CGSizeMake(img.size.width,img.size.height);
		retinaPX =1;
		float height =0;
		float width = 700;
		if(img.size.width<width){
			width = img.size.width;
		}
		height =(width/img.size.width)*img.size.height;
		newSize=CGSizeMake(width/retinaPX,height/retinaPX);
	
	
	UIImage *temp =  [self makeResizedImage:newSize quality:kCGInterpolationMedium withImage:img isAspectFit:YES];
	if(temp == nil){
		return img;
	}
	
	return temp;
	
}




-(UIImage *)makeResizedImage:(CGSize)newSize quality:(CGInterpolationQuality)interpolationQuality withImage:(UIImage*)oldImage isAspectFit:(BOOL)isAspectFit {
    CGRect newRect = CGRectIntegral(CGRectMake(0, 0, newSize.width, newSize.height));
    //oldImage = [self rotateImage:oldImage];
    CGImageRef imageRef = [oldImage CGImage];
    CGSize mySize = [oldImage size];
    CGFloat imageWidth = mySize.width;
    CGFloat imageHeight = mySize.height;
    CGRect tempRect;
    if(isAspectFit){
        tempRect = [self aspectFittedRect:CGRectMake(0, 0,imageWidth, imageHeight) max:newRect];
        newRect = tempRect;
    }
    size_t bytesPerRow = CGImageGetBitsPerPixel(imageRef) / CGImageGetBitsPerComponent(imageRef) * newRect.size.width;
    bytesPerRow = (bytesPerRow + 15) & ~15;
    CGContextRef bitmap = CGBitmapContextCreate(NULL,
                                                newRect.size.width,
                                                newRect.size.height,
                                                CGImageGetBitsPerComponent(imageRef),
                                                bytesPerRow,
                                                CGImageGetColorSpace(imageRef),
                                                CGImageGetBitmapInfo(imageRef));
    
    CGContextSetInterpolationQuality(bitmap, interpolationQuality);
    CGContextDrawImage(bitmap, newRect, imageRef);
    CGImageRef resizedImageRef = CGBitmapContextCreateImage(bitmap);
    //UIImage *resizedImage = [UIImage imageWithCGImage:resizedImageRef];
    UIImage *resizedImage = [UIImage imageWithCGImage:resizedImageRef scale:oldImage.scale orientation:oldImage.imageOrientation];
    
    CGContextRelease(bitmap);
    
    CGImageRelease(resizedImageRef);
    
    return resizedImage;
}
- (CGRect)aspectFittedRect:(CGRect)inRect max:(CGRect)maxRect {
    float currentHeight = CGRectGetHeight(inRect);
    float currentWidth = CGRectGetWidth(inRect);
    float liChange ;
    CGSize newSize ;
    if (currentWidth == currentHeight) { // image is square
        liChange = CGRectGetHeight(maxRect) / currentHeight;
        newSize.height = currentHeight * liChange;
        newSize.width = currentWidth * liChange;
    } else if (currentHeight > currentWidth) { // image is landscape
        liChange  = CGRectGetWidth(maxRect) / currentWidth;
        newSize.height = currentHeight * liChange;
        newSize.width = CGRectGetWidth(maxRect);
    } else     {                           // image is Portrait
        liChange =CGRectGetHeight(maxRect) / currentHeight;
        newSize.height= CGRectGetHeight(maxRect);
        newSize.width = currentWidth * liChange;
    }
    return CGRectMake(0, 0,floorf(newSize.width), floorf(newSize.height));
}
-(UIImage *)rotateImage:(UIImage*)image {
    UIImageOrientation orient = image.imageOrientation;
    // No-op if the orientation is already correct
    if (orient == UIImageOrientationUp) return image;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (orient) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, image.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, image.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationUpMirrored:
            break;
    }
    
    switch (orient) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, image.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        case UIImageOrientationUp:
        case UIImageOrientationDown:
        case UIImageOrientationLeft:
        case UIImageOrientationRight:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, image.size.width, image.size.height,
                                             CGImageGetBitsPerComponent(image.CGImage), 0,
                                             CGImageGetColorSpace(image.CGImage),
                                             CGImageGetBitmapInfo(image.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (orient) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.height,image.size.width), image.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,image.size.width,image.size.height), image.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}


@end
