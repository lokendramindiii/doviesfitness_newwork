//
//  FontHelper.swift
//
//  Copyright © 2017 hb. All rights reserved.
//

import Foundation

extension UILabel {
	override open func awakeFromNib() {
		super.awakeFromNib()
		if self.tag == 9999 {
			self.font = UIFont(name: (self.font.fontName), size: (self.font.pointSize) * CGRect.widthRatio)
		}
	}
}

open class CustomLabel : UILabel {
	@IBInspectable open var characterSpacing:CGFloat = 1 {
		didSet {
			let attributedString = NSMutableAttributedString(string: self.text!)
//			attributedString.addAttribute(NSAttributedStringKey.kern, value: self.characterSpacing, range: NSRange(location: 0, length: attributedString.length))
			let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 1.5
//            paragraphStyle.lineHeightMultiple = 1.5
			attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
			self.attributedText = attributedString
		}
		
	}
}

extension UITextView {
	
	override open func awakeFromNib() {
		super.awakeFromNib()
//		if self.tag != 9999 {
//			self.font = UIFont(name: (self.font?.fontName)!, size: (self.font?.pointSize)! * CGRect.widthRatio)
//		}
	}
}

extension UIButton {
	
	override open func awakeFromNib() {
		super.awakeFromNib()
		if self.tag == 9999 {
			self.titleLabel!.font = UIFont(name: (self.titleLabel!.font.fontName), size: (self.titleLabel!.font.pointSize) * CGRect.widthRatio)
		}
	}
}
