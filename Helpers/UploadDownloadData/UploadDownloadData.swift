//
//  UploadDownloadData.swift

//
//  Copyright © 2017 HiddenBrains. All rights reserved.
//

import UIKit

//NSURLSessionDelegate, NSURLSessionTaskDelegate, NSURLSessionDownloadDelegate

enum DownloadType: String {
    case image = "IMAGE"
    case audio = "AUDIO"
    case video = "VIDEO"
    case doc = "DOC"
    case zip = "ZIP"
    case rar = "RAR"
}

struct FilePath {
    static let audios = "Audios"
    static let videos = "Videos"
    static let images = "Images"
    static let docs = "Docs"
    static let zips = "Zips"
    static let rars = "Rars"
}

class UploadDownloadData: NSObject ,URLSessionDelegate,URLSessionTaskDelegate,URLSessionDataDelegate,URLSessionDownloadDelegate{
    
    static let session:URLSession = UploadDownloadData.shared.backgroundSession()
    var downloadDictTasks:NSMutableDictionary = NSMutableDictionary()
    
    static let shared: UploadDownloadData = {
        let instance = UploadDownloadData()
        // setup code
        return instance
    }()

    func backgroundSession() ->URLSession {
        var session:URLSession? = nil
        var configuration:URLSessionConfiguration!
        
        configuration = URLSessionConfiguration.background(withIdentifier: "com.simpletense.pr")
        configuration.sessionSendsLaunchEvents = true
        configuration.shouldUseExtendedBackgroundIdleMode = true
        configuration.httpMaximumConnectionsPerHost = 10
        configuration.timeoutIntervalForRequest = 600000000000
        
        session = URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
        
        return session!
    }
    
    func downloadData(_ type: DownloadType,path:String,withName:String, downloadProgress: ((_ progress:Float) -> Void)?, _ completion: @escaping (_ success: Bool, _ localURL: String? ) -> Void) {
		
        let fdi:FileDownloadInfo = FileDownloadInfo()
        fdi.isDownloading = false
        fdi.downloadComplete = false
        fdi.taskIdentifier = -1
        fdi.downloadProgress = 0.0
        fdi.downloadTask = nil
        fdi.downloadSource = path
        
        if(!fdi.isDownloading){
            if(fdi.taskIdentifier == -1){
                
                fdi.downloadTask = UploadDownloadData.session.downloadTask(with: URL(string: path)!)
            }else{
                fdi.downloadTask = UploadDownloadData.session.downloadTask(withResumeData: fdi.taskResume as! Data)
            }
        }
        
        let obj:blocksData = blocksData()
        obj.completionBlock = completion
        obj.progressBlock =  downloadProgress
        obj.type = type.rawValue
        obj.isUpload = false
        
        fdi.taskIdentifier = fdi.downloadTask?.taskIdentifier
        
        let str:String = String(describing: fdi.taskIdentifier!)
        fdi.downloadTask?.resume()
        fdi.isDownloading = true
        fdi.downloadSource = path
        obj.fdi = fdi
        downloadDictTasks[str] = obj
        
        downloadProgress!(0)
    }
    

	func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
		
		let str:String = String(downloadTask.taskIdentifier)
		
		if(( downloadDictTasks[str]) == nil){
			downloadTask.cancel()
			return
		}
		let obj = downloadDictTasks[str] as! blocksData
		let tt:URL = URL(string: (obj.fdi?.downloadSource)!)!
		let fileManager:FileManager = FileManager.default
		
		let URLs = fileManager.urls(for: .documentDirectory, in: .userDomainMask)
		
		let documentsDirectory = URLs[0] as URL
        
        
        var fileName = (obj.fdi?.downloadSource)!
        fileName = fileName.MD5!
        
       
        
        var folder = FilePath.images
        if(obj.type == "IMAGE"){
            folder = FilePath.images
        }else if(obj.type == "AUDIO"){
            folder = FilePath.audios
        }else if(obj.type == "VIDEO"){
            folder = FilePath.videos
        }
        else if(obj.type == "DOC"){
            folder = FilePath.docs
        }else if(obj.type == "ZIP"){
            folder = FilePath.zips
        }
        else if(obj.type == "RAR"){
            folder = FilePath.rars
        }
        
        let dataPath = documentsDirectory.appendingPathComponent(folder)
        
        do {
			try FileManager.default.createDirectory(atPath: dataPath.path, withIntermediateDirectories: true, attributes: nil)
		} catch let error as NSError {
			print("Error creating directory: \(error.localizedDescription)")
		}
		
		let destinationURL = dataPath.appendingPathComponent(fileName+"."+tt.pathExtension)
		
		do {
			try fileManager.removeItem(at: destinationURL)
		} catch let error as NSError {
			print(error)
		}
		
		do {
			try fileManager.moveItem(at: location, to: destinationURL)
			
			obj.completionBlock?(true,destinationURL.absoluteString)
		}
		catch let error as NSError {
			print(error)
			
			obj.completionBlock?(false,nil)
			
			
		}
		
		downloadDictTasks.removeObject(forKey: str)
	}
	
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
		
        let progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
        let percentage = progress*100
        
        let str:String = String(downloadTask.taskIdentifier)
        
        if(( downloadDictTasks[str]) == nil){
            downloadTask.cancel()
        }else{
            let obj = downloadDictTasks[str] as! blocksData
            obj.fdi?.downloadProgress  = percentage
            obj.progressBlock?(percentage)
        }
    }
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didResumeAtOffset fileOffset: Int64, expectedTotalBytes: Int64){
        
    }
    
    //MARK:- Session Delegates
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?){
        
        //print(task.response)
        
        if((error) != nil){
            
            let str:String = String(task.taskIdentifier)
            if(( downloadDictTasks[str]) != nil){
                let obj = downloadDictTasks[str] as! blocksData
                obj.completionBlock?(false,nil)
                downloadDictTasks.removeObject(forKey: str)
                
            }
            print("didCompleteWithError")
            
        }else{
            print("didCompleteWithNoError")
        }
        
    }
    
    func convertStringToDictionary(_ text: String) -> [String:AnyObject]? {
        
        if let data = text.data(using: String.Encoding.utf8, allowLossyConversion: false) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: [.allowFragments]) as? [String:AnyObject]
            } catch let error as NSError {
                print(error)
            }
        }
        return nil
    }
    
}

class blocksData:NSObject{
    var completionBlock :((_ success:Bool, _ items: String?)->Void)?
    var progressBlock :((_ progress:Float) -> Void)?
    var type:String = ""
    var isUpload:Bool = false
    var fdi:FileDownloadInfo?
}

class FileDownloadInfo:NSObject{
    var fileTitle:String?
    var downloadSource:String?
    var downloadTask:URLSessionDownloadTask?
    var taskResume:NSData?
    var downloadProgress:Float?
    var isDownloading:Bool = false
    var downloadComplete:Bool = false
    var taskIdentifier:Int?
}

extension NSMutableData {
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}


