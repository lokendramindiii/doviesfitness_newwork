//
//  ModelDoviesContact.swift
//  Dovies
//
//  Created by Neel Shah on 24/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class ModelDoviesContact: NSObject , NSCoding{
	
	var companyAddress : String!
	var companyContactNumber : String!
	var companyEmail : String!
	
	
	/**
	* Instantiate the instance using the passed dictionary values to set the properties values
	*/
	init(fromDictionary dictionary: [String:Any]){
		companyAddress = dictionary["company_address"] as? String ?? ""
		companyContactNumber = dictionary["company_contact_number"] as? String ?? ""
		companyEmail = dictionary["company_email"] as? String ?? ""
	}
	
	/**
	* Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	*/
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if companyAddress != nil{
			dictionary["company_address"] = companyAddress
		}
		if companyContactNumber != nil{
			dictionary["company_contact_number"] = companyContactNumber
		}
		if companyEmail != nil{
			dictionary["company_email"] = companyEmail
		}
		return dictionary
	}
	
	/**
	* NSCoding required initializer.
	* Fills the data from the passed decoder
	*/
	@objc required init(coder aDecoder: NSCoder)
	{
		companyAddress = aDecoder.decodeObject(forKey: "company_address") as? String
		companyContactNumber = aDecoder.decodeObject(forKey: "company_contact_number") as? String
		companyEmail = aDecoder.decodeObject(forKey: "company_email") as? String
		
	}
	
	/**
	* NSCoding required method.
	* Encodes mode properties into the decoder
	*/
	@objc func encode(with aCoder: NSCoder)
	{
		if companyAddress != nil{
			aCoder.encode(companyAddress, forKey: "company_address")
		}
		if companyContactNumber != nil{
			aCoder.encode(companyContactNumber, forKey: "company_contact_number")
		}
		if companyEmail != nil{
			aCoder.encode(companyEmail, forKey: "company_email")
		}
		
	}
	
}
