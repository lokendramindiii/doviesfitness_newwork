

import Foundation
import UIKit

let fieldBorderColor = UIColor(r: 136, g: 136, b: 136, alpha: 1)
let fieldBackgroundColor = UIColor(r: 250, g: 250, b: 250, alpha: 1)
let fieldTextColor = UIColor(r: 136, g: 136, b: 136, alpha: 1)

class MyCustomTextField: UITextField {
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        let borderColor : UIColor = fieldBorderColor
        self.layer.cornerRadius = 3.0;
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = 0.3
        self.backgroundColor = fieldBackgroundColor
        self.textColor = fieldTextColor
        self.font = UIFont.tamilRegular(size: 12)
        self.setValue(fieldTextColor, forKeyPath: "_placeholderLabel.textColor")

        self.layer.masksToBounds = true
    }
    override func textRect(forBounds bounds: CGRect) -> CGRect {
             return bounds.insetBy(dx: 12, dy: 0)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.insetBy(dx: 12    , dy: 0)
    }
}
