//
//  BlankReusableView.swift
//  Dovies
//
//  Created by hb on 10/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class BlankReusableView: UICollectionReusableView {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
