//
//  ResetViewController.swift
//  Dovies
//
//  Created by Mindiii on 12/12/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import TTTAttributedLabel

class ResetViewController: UIViewController {
    

    fileprivate var returnKeyHandler : IQKeyboardReturnKeyHandler!

    @IBOutlet weak var txtEmail: DTTextField!
    @IBOutlet weak var lblErrorEmail: UILabel!
    @IBOutlet weak var lbl_Suggest: UILabel!
    @IBOutlet weak var lbl_RetruntoLogin: TTTAttributedLabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let style = NSMutableParagraphStyle()
        style.alignment = .center
        style.lineSpacing = 8.0
        
        let att = [NSAttributedStringKey.font : UIFont.tamilRegular(size: 15.0), NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.5411764706, green: 0.5411764706, blue: 0.5411764706, alpha: 1), NSAttributedStringKey.paragraphStyle : style]
        let attStr = NSMutableAttributedString(string: "Enter your email to receive instructions on how to reset your password.", attributes: att)
        lbl_Suggest.attributedText = attStr
        returnKeyHandler = IQKeyboardReturnKeyHandler(controller: self)
        returnKeyHandler.lastTextFieldReturnKeyType = UIReturnKeyType.done
        
        
        lbl_RetruntoLogin.isUserInteractionEnabled = true
        
        let text = "Or return to Log In."
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5
        paragraphStyle.alignment = .center
        
        let defaultAttriString = NSMutableAttributedString(string: text, attributes: [NSAttributedStringKey.font: UIFont.tamilRegular(size: 14.0), NSAttributedStringKey.foregroundColor: fieldTextColor, NSAttributedStringKey.paragraphStyle: paragraphStyle, NSAttributedStringKey.kern : 0.5])
        
        self.lbl_RetruntoLogin.delegate = self
        self.lbl_RetruntoLogin.setText(defaultAttriString)
        
        let subscriptionNoticeLinkAttributes = [
            NSAttributedStringKey.font: UIFont.tamilBold(size: 13.0),
            NSAttributedStringKey.foregroundColor: fieldTextColor,
            NSAttributedStringKey.underlineStyle: NSNumber(value:true),
            ]
        lbl_RetruntoLogin.linkAttributes = subscriptionNoticeLinkAttributes
        
        let subscriptionNoticeActiveLinkAttributes = [
            NSAttributedStringKey.foregroundColor: UIColor.colorWithRGB(r: 153, g: 153, b: 153, alpha: 0.3),
            NSAttributedStringKey.backgroundColor: UIColor.lightGray,
            NSAttributedStringKey.underlineStyle: NSNumber(value:false),
            ]
        
        lbl_RetruntoLogin.activeLinkAttributes = subscriptionNoticeActiveLinkAttributes
        
        let range1 = (text as NSString).range(of: "Log In.")
        self.lbl_RetruntoLogin.addLink(to: NSURL(string: "login") as URL?, with:range1)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isStatusBarHidden = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        UIApplication.shared.isStatusBarHidden = false
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    
    @IBAction func btnResetTapped(_ sedner : UIButton){
        self.view.endEditing(true)
        
        
        self.hideAllError()
        
        guard let email = txtEmail.text, email.count > 0 else {
            self.lblErrorEmail.text = "Please enter email address"
            self.lblErrorEmail.isHidden = false
            layout()
            return
        }
        
        guard self.isValidateEmail(strEmail: email) else {
            self.lblErrorEmail.text = "Email address is invalid"
            self.lblErrorEmail.isHidden = false
            layout()
            return
        }
        
        let dict = [WsParam.email : txtEmail.text ?? ""]
            self.callForgotPasswordApi(with: dict)
            //WS call done here
        }
    
    func isValidateEmail(strEmail: String) -> Bool {
        let REGEX: String
        REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: strEmail)
    }
    
    
    func hideAllError(){
        self.lblErrorEmail.text = "Please enter email address"
        self.lblErrorEmail.isHidden = true
        self.layout()
    }
    
    func layout()  {
        DispatchQueue.main.async {
            self.view.layoutIfNeeded()
            self.view.updateConstraintsIfNeeded()
        }
    }
    @IBAction func btnCloseTapped(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
  }
    

//API calls
extension ResetViewController{
    
    func callForgotPasswordApi(with parameters:[String: Any]){
        if NetworkReachabilityManager()?.isReachable == true {
            GlobalUtility.showActivityIndi(viewContView: self.view)
        }
        DoviesWSCalls.callForgotPasswordAPI(dicParam: parameters, onSuccess: {[weak self] (isSuccess, message, response) in
            guard let weakSelf = self else{return}
            
            if isSuccess{
          
            GlobalUtility.hideActivityIndi(viewContView: self!.view)
            let storyBoard : UIStoryboard = UIStoryboard(name: "Login", bundle:nil)
            let PasswordReset = storyBoard.instantiateViewController(withIdentifier: "PasswordResetVC") as! PasswordResetVC
            PasswordReset.Str_Email = self!.txtEmail.text!
            self?.navigationController?.pushViewController(PasswordReset, animated: true)
            }
            else{
                GlobalUtility.showToastMessage(msg: message!)
            }


        }) {(error) in
            GlobalUtility.hideActivityIndi(viewContView: self.view)

        }
    }
    
}
extension ResetViewController: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let dtTextField = textField as? UITextField else{return}
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
     
     }

}
extension ResetViewController: TTTAttributedLabelDelegate {
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        self.navigationController?.popViewController(animated: true)
    }
}
