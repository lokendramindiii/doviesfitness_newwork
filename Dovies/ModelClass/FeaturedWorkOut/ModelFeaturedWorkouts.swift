//
//  ModelFeaturedWorkouts.swift
//  Dovies
//
//  Created by Neel Shah on 12/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class ModelFeaturedWorkouts: NSObject, NSCoding{
	
	var workoutGroupDescription : String!
	var workoutGroupId : String!
	var workoutGroupName : String!
	var workoutList : [ModelWorkoutList]!
	
    var groupWorkoutCount : String!
    var workoutGroupImage : String!
    var workoutGroupLevel : String!

	
	/**
	* Instantiate the instance using the passed dictionary values to set the properties values
	*/
	init(fromDictionary dictionary: [String:Any]){
        groupWorkoutCount = dictionary["group_workout_count"] as? String ?? ""
        workoutGroupImage = dictionary["workout_group_image"] as? String ?? ""
        workoutGroupLevel = dictionary["workout_group_level"] as? String ?? ""
		if workoutGroupLevel.uppercased() == "ALL"{
			workoutGroupLevel = "All Levels"
		}
		workoutGroupDescription = dictionary["workout_group_description"] as? String ?? ""
		workoutGroupId = dictionary["workout_group_id"] as? String ?? ""
		workoutGroupName = dictionary["workout_group_name"] as? String ?? ""
		workoutList = [ModelWorkoutList]()
		if let workoutListArray = dictionary["workout_list"] as? [[String:Any]]{
			for dic in workoutListArray{
				let value = ModelWorkoutList(fromDictionary: dic)
				workoutList.append(value)
			}
		}
	}
	
	/**
	* Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	*/
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if workoutGroupDescription != nil{
			dictionary["workout_group_description"] = workoutGroupDescription
		}
		if workoutGroupId != nil{
			dictionary["workout_group_id"] = workoutGroupId
		}
		if workoutGroupName != nil{
			dictionary["workout_group_name"] = workoutGroupName
		}
		if workoutList != nil{
			var dictionaryElements = [[String:Any]]()
			for workoutListElement in workoutList {
				dictionaryElements.append(workoutListElement.toDictionary())
			}
			dictionary["workout_list"] = dictionaryElements
		}
		return dictionary
	}
	
	/**
	* NSCoding required initializer.
	* Fills the data from the passed decoder
	*/
	@objc required init(coder aDecoder: NSCoder)
	{
		workoutGroupDescription = aDecoder.decodeObject(forKey: "workout_group_description") as? String
		workoutGroupId = aDecoder.decodeObject(forKey: "workout_group_id") as? String
		workoutGroupName = aDecoder.decodeObject(forKey: "workout_group_name") as? String
		workoutList = aDecoder.decodeObject(forKey :"workout_list") as? [ModelWorkoutList]
		
	}
	
	/**
	* NSCoding required method.
	* Encodes mode properties into the decoder
	*/
	@objc func encode(with aCoder: NSCoder)
	{
		if workoutGroupDescription != nil{
			aCoder.encode(workoutGroupDescription, forKey: "workout_group_description")
		}
		if workoutGroupId != nil{
			aCoder.encode(workoutGroupId, forKey: "workout_group_id")
		}
		if workoutGroupName != nil{
			aCoder.encode(workoutGroupName, forKey: "workout_group_name")
		}
		if workoutList != nil{
			aCoder.encode(workoutList, forKey: "workout_list")
		}
		
	}	
}
