//
//  BuildPlan_Detail.swift
//  Dovies
//
//  Created by Mindiii on 12/25/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

protocol BuildPlan_DetailDelegate
{
    func actionBack_andActionInfo(sender:Int)
    func GetHeightImage(Height:CGFloat)
}
class BuildPlan_Detail: BaseViewController {
    
    var programName                      : String?
    var ProgrameImage                    : String?
    
    var isFavrouitePlanVC  = false

    var delegate:BuildPlan_DetailDelegate?
    
    @IBOutlet weak var IB_ImgDetailPlan   : UIImageView!
    @IBOutlet weak var IB_lblPlanTitle    : UILabel!
    @IBOutlet weak var IB_HeaderView      : UIView!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if isFavrouitePlanVC == true
        {
          //  IB_HeaderView.isHidden = true
        }
        
        IB_lblPlanTitle.text = programName?.uppercased()
        
       // IB_ImgDetailPlan.sd_setImage(with: URL(string:ProgrameImage ?? ""), placeholderImage: AppInfo.placeholderImage)
        
         IB_ImgDetailPlan.sd_setImage(with: URL(string:ProgrameImage ?? ""), placeholderImage: nil)
        
        var viewbound = self.view.bounds.width
        let calculatewidth =  (9.2/7.2)
        viewbound = (viewbound) * CGFloat(calculatewidth)
        print(viewbound)
        delegate?.GetHeightImage(Height:viewbound)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    // Action navigation top header
    @IBAction func Action_Header(sender:UIButton)
    {
        if sender.tag == 1
        {
        delegate?.actionBack_andActionInfo(sender: 1)
        }
        else if sender.tag == 2
        {
        delegate?.actionBack_andActionInfo(sender: 2)
        }
    }
}
