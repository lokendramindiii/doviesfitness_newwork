//
//  DietPlanCell.swift
//  Dovies
//
//  Created by Mindiii on 1/14/19.
//  Copyright © 2019 Hidden Brains. All rights reserved.
//

import UIKit

class DietPlanCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDisplayname : UILabel!
    @IBOutlet weak var imgBackground : UIImageView!
    @IBOutlet weak var lblDietPlanCategoryName : UILabel!
    @IBOutlet weak var lblProgramPlanCategoryName : UILabel!
    @IBOutlet weak var lblDietPlanWeekCount : UILabel!
    @IBOutlet weak var imgFadeBackground : UIImageView!
    @IBOutlet weak var viewBackground : UIView!
    var isFromEditAdmin             : Bool = false
    var cellIndexPath : IndexPath?
    
    @IBOutlet weak var btnMore : UIButton!
    @IBOutlet weak var imgLock : UIImageView!
    @IBOutlet weak var constTitleHeight  : NSLayoutConstraint!
    @IBOutlet weak var constOptionTop  : NSLayoutConstraint!
    
    var moreButtonAction:((DietPlan?)->())?
    var moreButtonMyPlanAction:((ModelMyPlan?)->())?
    var moreButtonDeleteAction:(()->())?
    var dietPlan : DietPlan?
    var myPlan : ModelMyPlan?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        viewBackground.backgroundColor = UIColor.appBlackColorNew()
        self.imgBackground.backgroundColor = UIColor.appBlackColorNew()

    }
    
    //Diet plan cell
    func setCellUIPlan(with category : DietPlanCategory ){


        imgBackground.sd_setImage(with:  URL(string: category.dietPlanCategoryImage), placeholderImage: nil)
        
        lblDietPlanCategoryName.isHidden = false
        lblDietPlanCategoryName.text = category.dietPlanCategoryName.uppercased()
    }
    
    func setCellUIProgram(with program : Program){
        
        imgBackground.sd_setImage(with:  URL(string: program.programImage), placeholderImage: nil)
        if program.programAccessLevel == "LOCK" {
            
            if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
            {
                imgLock.isHidden = true
            }
            else
            {
                imgLock.isHidden = false
            }
        }
        else{
            imgLock.isHidden = true
        }
        
        
        switch program.programWeekCount {
        case "0":
            lblDietPlanWeekCount.text = "Coming soon"
            lblDietPlanWeekCount.isHidden = false
            
        case "1":
            lblDietPlanWeekCount.text = program.programWeekCount.uppercased() + " Week plan"
            lblDietPlanWeekCount.isHidden = false
            
        case "":
            lblDietPlanWeekCount.isHidden = true
        default:
            lblDietPlanWeekCount.text = program.programWeekCount.uppercased() + " Weeks plan"
            lblDietPlanWeekCount.isHidden = false
        }
        
        
        lblProgramPlanCategoryName.text = program.programName.uppercased()
        lblProgramPlanCategoryName.isHidden = false
    }
    // My plan cell
    func setMyPlanUI(modelMyPlan: ModelMyPlan){
        

        imgBackground.sd_setImage(with:  URL(string: modelMyPlan.programImage), placeholderImage: nil)
        
        lblProgramPlanCategoryName.text = modelMyPlan.programName.uppercased()
        lblProgramPlanCategoryName.isHidden = false
        
        imgLock.isHidden = modelMyPlan.programAccessLevel.uppercased() == "OPEN".uppercased() || DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        if imgLock.isHidden == false{
            self.constOptionTop.constant = 45
        }
        else{
            self.constOptionTop.constant = 10
        }
        
        
        switch modelMyPlan.programWeekCount {
        case "0":
            lblDietPlanWeekCount.text = "Coming soon"
            lblDietPlanWeekCount.isHidden = false
            
        case "1":
            lblDietPlanWeekCount.text = modelMyPlan.programWeekCount.uppercased() + " Week plan"
            lblDietPlanWeekCount.isHidden = false
        case "":
            lblDietPlanWeekCount.isHidden = true
        default:
            lblDietPlanWeekCount.text = modelMyPlan.programWeekCount.uppercased() + " Weeks plan"
            lblDietPlanWeekCount.isHidden = false
        }
        
        //  constTitleHeight.constant = 0
        contentView.backgroundColor = UIColor.white
        myPlan = modelMyPlan
    }
    @IBAction func btnMoreTapped(_ sender : UIButton){
        if let plan = dietPlan{
            moreButtonAction?(plan)
        }
        if let mPlan = myPlan{
            moreButtonMyPlanAction?(mPlan)
        }
        if moreButtonDeleteAction != nil{
            moreButtonDeleteAction?()
        }
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
extension UIView{
    func addGradientBackground(firstColor: UIColor, secondColor: UIColor){
        clipsToBounds = true
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [firstColor.cgColor, secondColor.cgColor]
        gradientLayer.frame = self.bounds
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 0, y: 1)
        print(gradientLayer.frame)
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}
