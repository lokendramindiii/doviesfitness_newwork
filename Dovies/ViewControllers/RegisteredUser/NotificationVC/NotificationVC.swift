//
//  NotificationVC.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.


import UIKit
import NVActivityIndicatorView


struct notificationVC {
    
    static var readUnreadStatus = ""
}

//This view controller used to show user notifications.
class NotificationVC: BaseViewController,UITableViewDelegate,UITableViewDataSource{
    
    var activityIndicatorNew : NVActivityIndicatorView!

    @IBOutlet var IBtblNotification: UITableView!
    @IBOutlet var IBlblNoData: UILabel!
    var arrNotifications = [NotificationModel]()
    var cellCount = 0
    var pullToRefreshCtrl:UIRefreshControl!

    var currentPageIndex = 1
    var Ispulltorefresh = false
    var LoadMore = true
    

//    var shouldLoadMore = false {
//        didSet {
//            if shouldLoadMore == false {
//                IBtblNotification.tableFooterView = UIView()
//            } else {
//                IBtblNotification.tableFooterView = DoviesGlobalUtility.loadingFooter()
//            }
//        }
//    }
//
    
    var shouldLoadMore = true {
        didSet {
            if shouldLoadMore == false {
                IBtblNotification.tableFooterView = UIView()
            } else {
                IBtblNotification.tableFooterView = DoviesGlobalUtility.loadingFooter()
            }
        }
    }
    
    
    
    //MARK: - View Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let frame = CGRect(x: self.view.center.x-22, y: self.view.center.y-22, width: 45, height: 45)
        activityIndicatorNew = NVActivityIndicatorView(frame: frame)
        activityIndicatorNew.center = self.view.center
        activityIndicatorNew.type = . circleStrokeSpin // add your type
        activityIndicatorNew.color = UIColor.lightGray // add your color
        APP_DELEGATE.window?.addSubview(activityIndicatorNew)
        activityIndicatorNew.startAnimating()
        
        IBtblNotification.estimatedRowHeight = 120.0
        IBtblNotification.rowHeight = UITableViewAutomaticDimension
        
        self.navigationItem.title = "inbox".uppercased()
        self.navigationItem.hidesBackButton  = true
        
        self.navigationItem.leftBarButtonItems = GlobalUtility.getNavigationButtonItems(target: self, selector: #selector(self.btnBackTapped(sender:)), imageName: "btn_top_backWhite")
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.black


        self.setPullToRefresh()
        
        self.callNotificationListAPI()

    }
    
    @objc override func btnBackTapped(sender: UIButton) {
        
        if arrNotifications.count != 0
        {
            for obj in arrNotifications
            {
                if obj.notificationStatus == "Unread"
                {
                    notificationVC.readUnreadStatus = "Unread"
                }
            }
        }
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getUserProfileWSCall()
        self.Ispulltorefresh = false
//        if arrNotifications.count == 0{
        
          //  cellCount = 10
          //  self.IBtblNotification.showLoader()
          //  self.callNotificationListAPI()
        
      //  }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        activityIndicatorNew.stopAnimating()
    }
    
    func getUserProfileWSCall(){
        DoviesWSCalls.callGetUserInfoAPI(dicParam: nil, onSuccess: {[weak self] (isSuccess, message, response) in
            guard let weakSelf = self else{return}
            if isSuccess, let dic = response {
                UserDefaults.standard.set(dic, forKey: UserDefaultsKey.userData)
                DoviesGlobalUtility.currentUser = User.init(fromDictionary: dic)
                
                if DoviesGlobalUtility.currentUser?.notifyCount == "0"
                {
                    notificationVC.readUnreadStatus = "Read"
                }
                else
                {
                    notificationVC.readUnreadStatus = "Unread"
                }
            }
        }) { (error) in
        }
    }
    
    
    func setPullToRefresh(){
        
        pullToRefreshCtrl = UIRefreshControl()

        pullToRefreshCtrl.addTarget(self, action: #selector(self.pullToRefreshClick(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            IBtblNotification.refreshControl  = pullToRefreshCtrl
        } else {
            IBtblNotification.addSubview(pullToRefreshCtrl)
        }
    }
    
    @objc func pullToRefreshClick(sender:UIRefreshControl) {
        self.pullToRefreshCtrl?.endRefreshing()

        if  Ispulltorefresh == false
        {
            currentPageIndex = 1
            shouldLoadMore = false
            self.callNotificationListAPI()
            self.pullToRefreshCtrl?.endRefreshing()
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        //IBTableview.collectionViewLayout.invalidateLayout()
        self.navigationController?.isNavigationBarHidden = false
    }


    
    //MARK: - didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
  
    
    // MARK: UITableview DataSource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
      return arrNotifications.count > 0 ? arrNotifications.count : cellCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       let cell = tableView.dequeueReusableCell(withIdentifier: "Notification_Cell") as! Notification_Cell
            
            if indexPath.row < arrNotifications.count{
                cell.setCellWith(notification: arrNotifications[indexPath.row])
            }
            
            return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (arrNotifications.count - 1) && shouldLoadMore == true {
            //shouldLoadMore = false
            
            if LoadMore == true
            {
                LoadMore = false
                callNotificationListAPI()
            }
        }
    }
    
    // MARK: UITableview Delegate Methods
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = arrNotifications [indexPath.row]
        if arrNotifications[indexPath.row].notificationStatus == "Unread"
        {
            obj.notificationStatus = "Read"
            IBtblNotification.reloadData()
        }
        
        if arrNotifications[indexPath.row].notificationCode == "custom" || arrNotifications[indexPath.row].notificationCode.uppercased() == "WELCOME"
        {
            let Custom = self.storyboard?.instantiateViewController(withIdentifier: "NotificationDetailVC")as! NotificationDetailVC
            
            Custom.arrobj = [arrNotifications[indexPath.row]]
            
           // Custom.arrayCommentList = arrNotifications[indexPath.row].commentList
            self.navigationController?.pushViewController(Custom, animated: true)
        }
        else if arrNotifications[indexPath.row].notificationCode == "News_feed"
        {
            
            let newsFeedDetailsVC = Storyboard.NewsFeed.storyboard().instantiateViewController(withIdentifier: "NewsFeedMediaDetailsVC") as! NewsFeedMediaDetailsVC
            newsFeedDetailsVC.newsFeedId = arrNotifications[indexPath.row].notificationConnectionId
            newsFeedDetailsVC.isFromShare = true
            if arrNotifications[indexPath.row].notificationType == "Image"
            {
                newsFeedDetailsVC.title = "PHOTO"
            }
            else if arrNotifications[indexPath.row].notificationType == "Video"
            {
                newsFeedDetailsVC.title = "VIDEO"
            }
        self.navigationController?.pushViewController(newsFeedDetailsVC, animated: true)
        }
        else if arrNotifications[indexPath.row].notificationCode == "Program_plan"
        {
            let programDetailsVC = Storyboard.Programs.instantiate(ProgramDetail_VC.self)
            programDetailsVC.programId =  arrNotifications[indexPath.row].notificationConnectionId
            self.navigationController?.pushViewController(programDetailsVC, animated: true)
            
        }
        else if arrNotifications[indexPath.row].notificationCode.uppercased() == "WORKOUT"
        {
            let featuredWorkOutDetailViewControlle = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "FeaturedWorkOutDetailViewController") as! FeaturedWorkOutDetailViewController
            featuredWorkOutDetailViewControlle.workoutId = arrNotifications[indexPath.row].notificationConnectionId
            featuredWorkOutDetailViewControlle.workoutScreenType = .sharedWorkout
            featuredWorkOutDetailViewControlle.navigationItem.title = ""
            self.navigationController?.pushViewController(featuredWorkOutDetailViewControlle, animated: true)
            
        }
            
        else if arrNotifications[indexPath.row].notificationCode.uppercased() == "EXERCISE"
        {
            let wVC = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "WorkoutDetailsVC") as! WorkoutDetailsVC
            wVC.exerciseId = arrNotifications[indexPath.row].notificationConnectionId
            wVC.screenType = .workOutDetailList
            self.navigationController?.pushViewController(wVC, animated: true)
        }
        else if arrNotifications[indexPath.row].notificationCode == ""
        {
            
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            if let wId = arrNotifications[indexPath.row].notificationId{
                UIAlertController.showAlert(in: self, withTitle: AlertTitle.appName, message: AlertMessages.deleteNotification, cancelButtonTitle: "No", destructiveButtonTitle: "Delete", otherButtonTitles: nil) { (alertControl, action, index) in
                    if index == 1{
                        self.callDeleteComment(at: wId)
                        self.arrNotifications.remove(at: indexPath.row)
                        self.IBtblNotification.reloadData()
                    }
                }
            }
        }
    }

    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

//API Calls
extension NotificationVC{
    
    
    /**
     This method used to delete particular Notification  and update the tableview.
     */
    func callDeleteComment(at NotificaitonId : String){
        guard let cUser = DoviesGlobalUtility.currentUser else {return}
        var params = [String: Any]()
        params[WsParam.authCustomerId] = cUser.customerAuthToken
        params[WsParam.notificaitonId] = NotificaitonId
        
        DoviesWSCalls.callDeleteNotificationAPI(dicParam: params, onSuccess: { (success, message) in
            if success{
                
            }
            
        }) { (error) in
        }
    }
    
    
    /**
     This method used to fetch the current user notifications and update the tableview.
     */
    func callNotificationListAPI(){
        
        self.Ispulltorefresh = true
        guard let user = DoviesGlobalUtility.currentUser else{return}
        
        var params = [String : Any]()
        params[WsParam.authCustomerId] = user.customerAuthToken
        params[WsParam.pageIndex] = "\(currentPageIndex)"
        DoviesWSCalls.callGetNotificationsAPI(dicParam: params, onSuccess: { [weak self](success, notifications, message,hasNextPage) in
            
            guard let weakSelf = self else {return}
            weakSelf.shouldLoadMore = hasNextPage
            weakSelf.LoadMore = hasNextPage
            if success, let notificationList = notifications{
                if self?.currentPageIndex == 1, let notificationList = notifications{

                    weakSelf.arrNotifications = notificationList
                }else if let notificationList = notifications{
                    weakSelf.arrNotifications += notificationList
                }
            }
            
            if hasNextPage{
                weakSelf.currentPageIndex += 1
            }
            
            weakSelf.IBlblNoData.isHidden = weakSelf.arrNotifications.count > 0
            
            weakSelf.cellCount = 0
            
       //   weakSelf.IBtblNotification.hideLoader()
            weakSelf.IBtblNotification.reloadData()
        
            self!.Ispulltorefresh = false
            self!.activityIndicatorNew.stopAnimating()


        }) { [weak self](error) in
            guard let weakSelf = self else {return}
            self!.activityIndicatorNew.stopAnimating()
            self!.Ispulltorefresh = false
           // weakSelf.IBtblNotification.hideLoader()
            weakSelf.IBtblNotification.reloadData()
        }
    }
}
