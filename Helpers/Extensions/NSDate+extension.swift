//
//  NSDate+extension.swift
//
//  Copyright © 2017 hb. All rights reserved.
//

import Foundation

extension Date {
	func dayOfWeek() -> String? {
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = "EEEE"
		return dateFormatter.string(from: self).capitalized
		// or use capitalized(with: locale) if you want
	}
}

extension NSDate {
	
	/**
	Method which convert string date to timeAgo formate
	
	- parameter PostDate:    date on which you want to perform
	- parameter dateFormate: date foramte like(yyyy-MM-DD hh:mm:ss, ect)
	
	- returns: time ago string
	*/
	class func timeAgoString(PostDate: String, dateFormate: String) -> String {
		
		let dateFormat = DateFormatter()
		dateFormat.dateFormat = dateFormate
		dateFormat.isLenient = false
		dateFormat.timeZone = NSTimeZone(abbreviation: "GMT") as TimeZone!
		dateFormat.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
		let ExpDate = dateFormat.date(from: PostDate)!
		let calendar = NSCalendar.current
		let components = (calendar as NSCalendar).components([.weekOfYear, .second, .minute, .hour, .day, .month, .year], from: ExpDate, to: Date(), options: [])
		var time: String = ""
		if components.year != nil && components.year! > 0 {
			if components.year == 1 {
				time = "\(Int(components.year!)) year"
			}
			else {
				time = "\(Int(components.year!)) years"
			}
		}
		else if components.month != nil && components.month! > 0 {
			if components.month == 1 {
				time = "\(Int(components.month!)) month"
			}
			else {
				time = "\(Int(components.month!)) months"
			}
		}
		else if components.weekOfYear != nil && components.weekOfYear! > 0{
			if components.weekOfYear == 1{
				time = "\(Int(components.weekOfYear!)) week"
			}else{
				time = "\(Int(components.weekOfYear!)) weeks"
			}
		}
		else if components.day != nil && components.day! > 0 {
			if components.day == 1 {
				time = "\(Int(components.day!)) day"
			}
			else {
				time = "\(Int(components.day!)) days"
			}
		}
		else if components.hour != nil && components.hour! > 0 {
			if components.hour == 1 {
				time = "\(Int(components.hour!)) hour"
			}
			else {
				time = "\(Int(components.hour!)) hours"
			}
		}
		else if components.minute != nil && components.minute! > 0 {
			if components.minute == 1 {
				time = "\(Int(components.minute!)) min"
			}
			else {
				time = "\(Int(components.minute!)) mins"
			}
		}
		else if components.second != nil {
			if components.second! >= 0 {
				if components.second == 0 {
					time = "1 sec"
				}
				else {
					time = "\(Int(components.second!)) secs"
				}
			} else if components.second! < 0 {
				return "just now"
			}
		}
		return "\(time) ago"
	}
	
	
	class func stringToDate(strDate:String?, format:String) -> Date {
		//        print("StrDAte :: \(strDate)")
		if let date = strDate {
			let dateFormat = DateFormatter()
			dateFormat.dateFormat = format
			dateFormat.timeZone = NSTimeZone(abbreviation: "GMT") as TimeZone!
			dateFormat.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
			//            print("ConvertDAte :: \(dateFormat.date(from: date)!)")
            return dateFormat.date(from: date)!
		} else {
			return Date()
		}
	}
//    class func stringToTime (strTime:String?, format:String)-> Date{
//
//        if let date = strTime {
//            let inFormatter = DateFormatter()
////            inFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone!
////            inFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
//
//            //lokend
//            inFormatter.dateFormat = "hh:mm:a"
//            return inFormatter.date(from: date)!
//        }else {
//            return Date()
//        }
//    }
    
    
    class func stringToTime (strTime:String?, format:String)-> Date{
        
        if let date = strTime {
            let inFormatter = DateFormatter()
            inFormatter.timeZone = NSTimeZone(abbreviation: "UTC") as TimeZone!
         //   inFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
            
            //lokend change code after lounch app 1.5
         //  inFormatter.dateFormat = "hh:mm:a"
            inFormatter.dateFormat = "HH:mm"

            
            return inFormatter.date(from: date)!
        }else {
            return Date()
        }
    }
    
	class func dateToString(date:Date?, format:String) -> String {
		if let dateToConvert = date {
			let dateFormat = DateFormatter()
			dateFormat.dateFormat = format
//            dateFormat.timeZone = NSTimeZone(abbreviation: "GMT") as TimeZone!
//            dateFormat.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
			return dateFormat.string(from: dateToConvert)
		} else {
			return ""
		}
	}
	
	class func stringToString(strSourceDate:String?, stcFormat: String, destFormat: String) -> String {
		if let strToConvert = strSourceDate {
			let dateFormat = DateFormatter()
			dateFormat.dateFormat = stcFormat
			dateFormat.timeZone = NSTimeZone(abbreviation: "GMT") as TimeZone!
			dateFormat.locale = NSLocale(localeIdentifier: "en_US_POSIX") as Locale!
			if let date = dateFormat.date(from: strToConvert) {
				dateFormat.dateFormat = destFormat
				return dateFormat.string(from: date)
			} else {
				return ""
			}
		} else {
			return ""
		}
	}
}
