//
//  PasswordResetVC.swift
//  Dovies
//
//  Created by Mindiii on 12/12/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class PasswordResetVC: UIViewController {
    
    @IBOutlet weak var lbl_Suggest: UILabel!
    @IBOutlet weak var lbl_EmailAddress: UILabel!
    
    var Str_Email = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let style = NSMutableParagraphStyle()
        style.alignment = .center
        style.lineSpacing = 8.0
        lbl_EmailAddress.text = Str_Email
        let att = [NSAttributedStringKey.font : UIFont.tamilRegular(size: 15.0), NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.5411764706, green: 0.5411764706, blue: 0.5411764706, alpha: 1), NSAttributedStringKey.paragraphStyle : style]
        let attStr = NSMutableAttributedString(string: "You should receive a link a few moments. Please open that link to reset your password.", attributes: att)
        lbl_Suggest.attributedText = attStr
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isStatusBarHidden = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        UIApplication.shared.isStatusBarHidden = false
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func btnBackToLogin(_ sedner : UIButton)
    {
        if let viewControllers = self.navigationController?.viewControllers{
            for viewController in viewControllers{
                if let LoginVC = viewController as? LoginViewController{
                    self.navigationController?.popToViewController(LoginVC, animated: true)
                    return
                }
            }
        }
    }
    
    @IBAction func btnCloseTapped(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }


}
