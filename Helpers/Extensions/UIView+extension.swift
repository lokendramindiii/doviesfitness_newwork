//
//  UIView+extension.swift
//
//  Copyright © 2017 hb. All rights reserved.
//

import Foundation

extension UIView {
	
    // Turns a view into a circle
    func circle() {
        self.layer.cornerRadius = bounds.height / 2
        self.layer.masksToBounds = true
    }
    
	func setRoundCorner(radius:CGFloat) {
		self.layer.cornerRadius = radius
		self.layer.masksToBounds = true
	}
	
	func setBorder(color:UIColor = UIColor.clear, size:CGFloat = 1) {
		self.layer.borderColor = color.cgColor
		self.layer.borderWidth = size
	}
	
	func setRoundBorder(radius:CGFloat, color:UIColor = UIColor.clear, size:CGFloat = 1) {
		self.setRoundCorner(radius: radius)
		self.setBorder(color: color, size: size)
	}
	
	func addTapGesture(target:AnyObject?, action:Selector) {
		let tapGesture = UITapGestureRecognizer(target: target, action: action)
		tapGesture.numberOfTapsRequired = 1
		tapGesture.numberOfTouchesRequired = 1
		self.addGestureRecognizer(tapGesture)
	}
	
	func addDoubleTapGesture(target:AnyObject?, actionDouble:Selector, actionSingle:Selector) {
		let tapGestureSingle = UITapGestureRecognizer(target: target, action: actionSingle)
		tapGestureSingle.numberOfTapsRequired = 1
		self.addGestureRecognizer(tapGestureSingle)
		
		let tapGesture = UITapGestureRecognizer(target: target, action: actionDouble)
		tapGesture.numberOfTapsRequired = 2
		self.addGestureRecognizer(tapGesture)
		
		tapGestureSingle.require(toFail: tapGesture)
	}
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func addSeparator(with color : UIColor, size : CGFloat, padding: CGFloat = 0,isAtTop : Bool = false){
        let separator = UIView()
        separator.translatesAutoresizingMaskIntoConstraints = false
         self.addSubview(separator)
        separator.backgroundColor = color
        if isAtTop{
            separator.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        }else{
            separator.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        }
        separator.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: padding).isActive = true
        separator.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: padding).isActive = true
        separator.heightAnchor.constraint(equalToConstant: size).isActive = true
       self.bringSubview(toFront: separator)
    }
    
    
    // Adds a view as a subview of another view with anchors at all sides
    func addToView(view: UIView) {
        self.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(self)
        
        self.topAnchor.constraint(equalTo:    view.topAnchor).isActive = true
        self.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        self.leftAnchor.constraint(equalTo:   view.leftAnchor).isActive = true
        self.rightAnchor.constraint(equalTo:  view.rightAnchor).isActive = true
    }
    
}
