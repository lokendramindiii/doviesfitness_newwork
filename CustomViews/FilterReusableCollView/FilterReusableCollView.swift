//
//  FilterReusableCollView.swift
//  Dovies
//
//  Created by hb on 10/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class FilterReusableCollView: UICollectionReusableView {
    @IBOutlet var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.backgroundColor = .black
    }
    
}
