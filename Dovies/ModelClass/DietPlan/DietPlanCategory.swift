//
//  DietPlanCategory.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

struct DietPlanCategory {
    
    var dietPlanCategoryDescription : String!
    var dietPlanCategoryId : String!
    var dietPlanCategoryImage : String!
    var dietPlanCategoryName : String!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        dietPlanCategoryDescription = dictionary["diet_plan_category_description"] as? String ?? ""
        dietPlanCategoryId = dictionary["diet_plan_category_id"] as? String ?? ""
        dietPlanCategoryImage = dictionary["diet_plan_category_image"] as? String ?? ""
        dietPlanCategoryName = dictionary["diet_plan_category_name"] as? String ?? ""
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if dietPlanCategoryDescription != nil{
            dictionary["diet_plan_category_description"] = dietPlanCategoryDescription
        }
        if dietPlanCategoryId != nil{
            dictionary["diet_plan_category_id"] = dietPlanCategoryId
        }
        if dietPlanCategoryImage != nil{
            dictionary["diet_plan_category_image"] = dietPlanCategoryImage
        }
        if dietPlanCategoryName != nil{
            dictionary["diet_plan_category_name"] = dietPlanCategoryName
        }
        return dictionary
    }
    
}
