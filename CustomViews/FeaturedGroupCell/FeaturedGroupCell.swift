//
//  FeaturedGroupCell.swift
//  Dovies
//
//  Created by HB-PC on 12/06/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class FeaturedGroupCell: UITableViewCell {

    @IBOutlet weak var imgGroup : UIImageView!

    @IBOutlet weak var lblGroupName : UILabel!
    @IBOutlet weak var lblGroupDetails : UILabel!
    @IBOutlet weak var viewBackground : UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
    
        self.imgGroup.backgroundColor = UIColor.appBlackColorNew()
    }
    
    func setCellFor(model : ModelFeaturedWorkouts){
    
        
        imgGroup.sd_setImage(with: URL(string: model.workoutGroupImage), placeholderImage: nil)

        lblGroupName.text = model.workoutGroupName.uppercased()
       // lblGroupDetails.text = model.groupWorkoutCount + " Workouts - " + model.workoutGroupLevel.capitalized
        
        if model.groupWorkoutCount == "0"
        {
        lblGroupDetails.text = model.workoutGroupLevel.capitalized + " - " + "Coming soon"
        }
        else
        {
            if model.groupWorkoutCount == "1" || model.groupWorkoutCount == "0"
            {
                lblGroupDetails.text = model.workoutGroupLevel.capitalized + " - " + model.groupWorkoutCount + " Workout"
            }
            else
            {
                lblGroupDetails.text = model.workoutGroupLevel.capitalized + " - " + model.groupWorkoutCount + " Workouts"
            }
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
