//
//  Exercise+CoreDataProperties.swift
//  
//
//  Created by Neel Shah on 01/05/18.
//
//

import Foundation
import CoreData


extension Exercise {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Exercise> {
        return NSFetchRequest<Exercise>(entityName: "Exercise")
    }

    @NSManaged public var workoutExerciseBodyParts: String?
    @NSManaged public var workoutExerciseBreakTime: String?
    @NSManaged public var workoutExerciseDescription: String?
    @NSManaged public var workoutExerciseDetail: String?
    @NSManaged public var workoutExerciseEquipments: String?
    @NSManaged public var workoutExerciseImage: String?
    @NSManaged public var workoutExerciseIsFavourite: Bool
    @NSManaged public var workoutExerciseLevel: String?
    @NSManaged public var workoutExerciseName: String?
    @NSManaged public var workoutExerciseTime: String?
    @NSManaged public var workoutExerciseType: String?
    @NSManaged public var workoutExerciseVideo: String?
    @NSManaged public var workoutExercisesId: String?
    @NSManaged public var workoutRepeatText: String?
	@NSManaged public var exerciseParentId: String?
	@NSManaged public var workoutAccessLevel: String?
    @NSManaged public var workoutExerciseSequenceSequenceNumber: String?
    @NSManaged public var workoutShareUrl: String?
    @NSManaged public var workoutExerciseBinaryimage: String?

}
