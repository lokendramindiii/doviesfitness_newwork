//
//  StaticPagesViewController.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

class StaticPagesViewController: BaseViewController, UIWebViewDelegate {
    
    enum StaticPageType{
        case aboutUs
        case termsAndConditions
        case aboutApplication
        case aboutDeveloper
        case privacyPolicy
        var title : String{
            switch self {
            case .aboutUs:
                return "About Us".uppercased()
            case .aboutDeveloper:
                return "About Developer".uppercased()
            case .aboutApplication:
                return "About Application".uppercased()
            case .termsAndConditions:
                return "Terms And Conditions".uppercased()
            case .privacyPolicy:
                return "Privacy Policy".uppercased()
            }
        }
        
        var pageCode : String{
            switch self {
            case .aboutUs:
                return "aboutus"
            case .aboutDeveloper:
                return "aboutdeveloper"
            case .aboutApplication:
                return "aboutapplication"
            case .termsAndConditions:
                return "termsconditions"
            case .privacyPolicy:
                return "privacypolicy"
            }
        }
        
    }

    @IBOutlet weak var webView : UIWebView!
    @IBOutlet weak var loader : UIActivityIndicatorView!
    var pageType : StaticPageType = .aboutUs

    var strUrl : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = pageType.title
      //  addNavigationBackButton()
        addNavigationWhiteBackButton()
        
      //    let BASE_URL = "https://www.doviesfitness.com/WS/"
        if pageType.title == "TERMS AND CONDITIONS"
        {
         self.strUrl = "https://doviesfitness.com/terms-conditions.html"

        }
        else if pageType.title == "PRIVACY POLICY"
        {
            self.strUrl = "https://doviesfitness.com/privacy-policy.html"
        
        }
        else if pageType.title == "ABOUT APPLICATION"{

           // self.strUrl = "https://doviesfitness.com/dovies/about-application.html"
            self.strUrl = "https://doviesfitness.com/about-application.html"

        }else {
            
        }
         self.loadWebView()
        
       // callAboutUsApi()
    }
    
    override func btnBackTapped(sender: UIButton) {
        super.btnBackTapped(sender: sender)
        self.dismiss(animated: true, completion: nil)
    }

    func loadWebView(){
        guard let url = strUrl ,let finalUrl = URL(string: url) else{return}
        loader.startAnimating()
        webView.loadRequest(URLRequest(url: finalUrl))
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func webViewDidFinishLoad(_ webView : UIWebView) {
        loader.stopAnimating()
    }

    //MARK: Webservice methods
    
    /**
     This method used to fetch about us information
     */
    
    func callAboutUsApi(){
        var params = [String : Any]()
        params[WsParam.pageCode] = pageType.pageCode
        DoviesWSCalls.callStaticAPI(dicParam: params, onSuccess: { (success, modelStaticPage, msg) in
            if success, let staticInfo = modelStaticPage{
                self.strUrl = staticInfo.staticPageUrl
            }
            self.loadWebView()
        }) { (error) in
            
        }
    }
    
}
