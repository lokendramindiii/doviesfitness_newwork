//
//  FeaturedWorkOutDetailViewController.swift
//  Dovies
//
//  Created by Neel Shah on 16/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.


import UIKit
import AutoScrollLabel
import MediaPlayer
import NVActivityIndicatorView

var mDicDownloading = [String : Any]()

class FeaturedWorkOutDetailViewController: BaseViewController {
    
    enum WorkOutScreenType{
        case featured
        case buildPlan
        case myWorkouts
        case downloads
        case sharedWorkout
    }
    
    var  constant: CGFloat = 0
    var activityIndicatorNew : NVActivityIndicatorView!

	@IBOutlet weak var IBrpCircularProgress: RPCircularProgress!
	@IBOutlet weak var IBbtnDownload: UIButton!
    @IBOutlet weak var IBlblTimeUnits: UILabel!
    @IBOutlet weak var IBbtnOverView: UIButton!
    @IBOutlet weak var IBbtnShare: UIButton!

    @IBOutlet weak var btnFavourite: UIButton!
    @IBOutlet weak var btnHeaderEditWorkout: UIButton!

    @IBOutlet weak var IBimgBg : UIImageView!
	@IBOutlet weak var IBlblWorkOutLevel: UILabel!
	@IBOutlet weak var IBlblWorkOutCount: UILabel!
	@IBOutlet weak var IBlblAvgMinutes: UILabel!
	@IBOutlet weak var IBlblWorkOutName: UILabel!
	@IBOutlet weak var IBlblGoodFor: UILabel!
	@IBOutlet weak var IBlblQuiptment: UILabel!
	@IBOutlet weak var IBimgWorkoutCreator: UIImageView!
	@IBOutlet weak var IBlblCreatorName: UILabel!
	var modelWorkoutList: ModelWorkoutList!
	var modelWorkoutDetail: ModelWorkoutDetail!
    var workoutId : String?

	var visibleCellsDuringAnimation = [UITableViewCell]()
	@IBOutlet weak var IBtblHeader: UIView!

	@IBOutlet weak var IBtblView: UITableView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var stackViewContainer: UIView!
    
    @IBOutlet weak var viewUpgraedeToPlay: UIView!
    @IBOutlet weak var Const_tablebottom: NSLayoutConstraint!

    var isForDownload = false
    var isFromMyDownload = false
    var isFromFeaturesVC = false


    var isFromShare = false
    var isWorkoutLocked = false
    var isSubscriptionReuired = false
    // IsvideoOpen str flage
    var IsVideoOpen = ""
	var selectedRow = -1
    var workoutScreenType : WorkOutScreenType = .featured
    
	var deleteWorkout:(() -> ())?
    var onWorkoutLogSuccess:(() -> ())?
    var onUpdateFavStatus:((Bool)->())?
    var updateUiCloser :((_ isupdate:Bool) ->())?
    
    var playerHeight = 179.5 * CGRect.widthRatio

    let cellHeight:CGFloat = 108 + 38
    
    let cellLastHeight:CGFloat = 108
    let cellVideoHeight:CGFloat = 120 + (185 * CGRect.widthRatio)

	var programId = ""
	var isFromEdit = false
	
	var progress:CGFloat = 0.0
	var isWorkOutVideoDownloaded: Bool!{
		didSet{
            self.IBbtnDownload.backgroundColor = UIColor.colorWithRGB(r: 252, g: 82, b: 41, alpha: 1.0)
            
            if isWorkoutLocked{
                self.viewUpgraedeToPlay.isHidden = false
                //self.IBbtnDownload.backgroundColor = UIColor.colorWithRGB(r: 255, g: 255, b: 255, alpha: 0.2)
                self.IBbtnDownload.setTitle("", for: .normal)
                self.IBbtnDownload.setImage(UIImage(named: "ic_lock_big")?.withRenderingMode(.alwaysOriginal), for: .normal)
                self.IBrpCircularProgress.isHidden = true
                return
            }
            
			if isWorkOutVideoDownloaded == true{
                self.IBbtnDownload.setImage(UIImage(named: "btn_play_white")?.withRenderingMode(.alwaysOriginal), for: .normal)
                self.IBbtnDownload.setImage(UIImage(named: "btn_play_white")?.withRenderingMode(.alwaysOriginal), for: .highlighted)
                self.IBbtnDownload.setTitle("", for: .normal)
				self.IBrpCircularProgress.isHidden = true
			}
			else{
				if isForDownload == true{
                    self.IBbtnDownload.setImage(nil, for: .normal)
					self.IBbtnDownload.setTitle("Update", for: .normal)
                    //self.IBbtnDownload.backgroundColor = UIColor.colorWithRGB(r: 255, g: 255, b: 255, alpha: 0.2)
				}
				else{
                    self.IBbtnDownload.setImage(UIImage(named: "btn_download")?.withRenderingMode(.alwaysOriginal), for: .normal)
                    self.IBbtnDownload.setImage(UIImage(named: "btn_download_h")?.withRenderingMode(.alwaysOriginal), for: .highlighted)
                    self.IBbtnDownload.setTitle("", for: .normal)
				}
                self.IBrpCircularProgress.isHidden = true
			}
		}
	}
	
	var isFromCreateWorkout = false
    var isFromMyworkoutlog = false

    var musicPlayer: MPMusicPlayerController!

    
    //---Narendra for Go Timer
    var workoutStartTimer: Timer!
    var secondsForWorkoutStartTime = 6
    var isWorkoutStratTimerRunning = false
    @IBOutlet weak var lblWorkoutStartTimer: UILabel!
    @IBOutlet weak var viewWorkoutStartTimer: UIView!
    @IBOutlet weak var imageViewWorkoutStartTimer: UIImageView!

    @IBOutlet weak var consTimerBottomSpace: NSLayoutConstraint!
    
    //---Narendra for Go Timer

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.appBlackColorNew()
        IBtblView.backgroundColor = UIColor.appBlackColorNew()
        addNavigationBackButton()
    
            
        let frame = CGRect(x: self.view.center.x-22, y: self.view.center.y-22, width: 45, height: 45)
        activityIndicatorNew = NVActivityIndicatorView(frame: frame)
        activityIndicatorNew.center = self.view.center
        activityIndicatorNew.type = . circleStrokeSpin // add your type
        activityIndicatorNew.color = UIColor.lightGray // add your color
        APP_DELEGATE.window?.addSubview(activityIndicatorNew)
        activityIndicatorNew.startAnimating()

        btnHeaderEditWorkout.isUserInteractionEnabled = false
        IBbtnShare.isUserInteractionEnabled = false
        self.consTimerBottomSpace.constant = 200

		IBrpCircularProgress.roundedCorners = false
		IBrpCircularProgress.thicknessRatio = 0.08
		IBrpCircularProgress.trackTintColor = UIColor.init(red: 255 / 255, green: 255 / 255, blue: 255 / 255, alpha: 0.3)
		IBrpCircularProgress.progressTintColor = UIColor.white
		IBrpCircularProgress.updateProgress(progress)
        //IBbtnOverView.setRoundCorner(radius: 5)
		self.view.layoutIfNeeded()
		IBbtnDownload.layer.cornerRadius = IBbtnDownload.bounds.width / 2
		
		if isForDownload == false{
			callWorkOutDetailAPI()
		}
		else
        {
            IBbtnShare.isUserInteractionEnabled = true
            self.fillTheViewWithDetailData()
            self.checkVideosDownloaded()
            print(modelWorkoutDetail.workoutExerciseList.count)
            
            var arraysequencid = [Int]()
            for  i  in  0..<(modelWorkoutDetail.workoutExerciseList.count)
            {
                arraysequencid.append(Int (modelWorkoutDetail.workoutExerciseList[i].workoutExerciseSequenceSequenceNumber) ?? 0 )
            }
            arraysequencid = arraysequencid.sorted() { $0 < $1 }
            print(arraysequencid)
            
            var workoutExerciseNew = [ModelWorkoutExerciseList]()

            for i in 0..<(modelWorkoutDetail.workoutExerciseList.count)
            {
                let Id:Int = arraysequencid[i]
                let sequencId:String = String(Id)
                
            let filteredArray = modelWorkoutDetail.workoutExerciseList.filter(){ $0.workoutExerciseSequenceSequenceNumber.contains(sequencId) }
                
                for obj in filteredArray
                {
                    if obj.workoutExerciseSequenceSequenceNumber == sequencId{
                        workoutExerciseNew.append(obj)
                    }
                }
            }
            
            modelWorkoutDetail.workoutExerciseList.removeAll()
            modelWorkoutDetail.workoutExerciseList = workoutExerciseNew
            
            self.IBtblView.reloadData()
        }
		
		IBtblView.register(UINib(nibName: "FeaturedDetailListTableCell", bundle: nil), forCellReuseIdentifier: "FeaturedDetailListTableCell")
		IBtblView.delegate = self
		IBtblView.dataSource = self
		
		
        let view = UIView(frame: CGRect(x: 0, y: 0, width: ScreenSize.width, height: 20))
        IBtblView.tableFooterView = view

		let notificationCenter = NotificationCenter.default
		notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: Notification.Name.UIApplicationWillResignActive, object: nil)
        
        musicPlayer = MPMusicPlayerController.systemMusicPlayer
        self.stackViewContainer.layer.masksToBounds = true
        self.stackViewContainer.layer.cornerRadius = self.stackViewContainer.frame.size.height/2
        
    }
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//        
//        var topSafeArea: CGFloat
//        var bottomSafeArea: CGFloat
//        
//        if #available(iOS 11.0, *) {
//            topSafeArea = view.safeAreaInsets.top
//            bottomSafeArea = view.safeAreaInsets.bottom
//        } else {
//            topSafeArea = topLayoutGuide.length
//            bottomSafeArea = bottomLayoutGuide.length
//        }
//        
//        // safe area values are now available to use
//        
//    }

	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
        APP_DELEGATE.isRotateAllow = false
        self.navigationController?.isNavigationBarHidden = true

   }
    


    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.isNavigationBarHidden = false

    }
	
	@objc func appMovedToBackground() {
		if selectedRow > -1{
			closeVideoPlayForCell()
		}
	}
    
    @IBAction func actionBack() {
        
        self.updateUiCloser?(false)
        if isFromFeaturesVC
        {
            self.navigationController?.popViewController(animated: false)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
            
        }
    }
    
    @IBAction func actionActionSheet() {
       self.showActionSheetToPerformOnWorkout()
    }
    
    @IBAction func actionFav() {
        self.callToMakeWorkOutFavourite()
    }
    
    @IBAction func actionUpgradeToPlay(sender : UIButton){
        self.pushToSubscriptionScreen()
    }
    
    @IBAction func actionMusic() {
        MPMediaLibrary.requestAuthorization { (status) in
            if status == .authorized {
                DispatchQueue.main.async {
                    let mpMediaPickerController = MPMediaPickerController()
                    mpMediaPickerController.allowsPickingMultipleItems = true
                    mpMediaPickerController.delegate = self
                   // self.present(mpMediaPickerController, animated: true)
                    
                    self.present(mpMediaPickerController, animated: true, completion: {
                        self.navigationController?.isNavigationBarHidden = true

                    })
                }
            }
            else
            {
                self.displayMediaLibraryError()
            }
        }
    }
    func displayMediaLibraryError() {
        var error: String
        switch MPMediaLibrary.authorizationStatus() {
        case .restricted:
            error = "Media library access restricted by corporate or parental settings"
        case .denied:
            error = "Media library access denied by user"
        default:
            error = "Unknown error"
        }
        
        let controller = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(controller, animated: true, completion: nil)
    }
    
    @IBAction func actionShare() {
        
        DoviesGlobalUtility.showShareSheet(on: self, shareText: self.modelWorkoutDetail.workoutShareUrl)
    }
	
   
	override func viewDidDisappear(_ animated: Bool) {
        
		super.viewDidDisappear(animated)
        activityIndicatorNew.stopAnimating()
		if IBrpCircularProgress.isHidden == false && self.isWorkOutVideoDownloaded == false{
			APP_DELEGATE.downloadService.showProgress = nil
			APP_DELEGATE.downloadService.updateLabel = nil
			APP_DELEGATE.downloadService.workoutDownloaded = nil
            self.isWorkOutVideoDownloaded = false
            DispatchQueue.main.async {
                self.IBbtnDownload.setTitle("Download", for: .normal)
                self.IBrpCircularProgress.isHidden = true
               self.IBrpCircularProgress.updateProgress(0.0)
            }
		}
        else{
            self.IBbtnDownload.backgroundColor = UIColor.colorWithRGB(r: 252, g: 82, b: 41, alpha: 1.0)
        }
		if selectedRow > -1{
			let selectedCell = IBtblView.cellForRow(at: IndexPath(row: selectedRow, section: 0)) as! FeaturedDetailListTableCell
 			selectedCell.stopVideo()
		}
	}
	
	deinit{
		NotificationCenter.default.removeObserver(self, name: Notification.Name.UIApplicationWillResignActive, object: nil)
		if selectedRow > -1{
			let selectedCell = IBtblView.cellForRow(at: IndexPath(row: selectedRow, section: 0)) as! FeaturedDetailListTableCell
			selectedCell.removePlayerFromView()
		}
	}
	
	override var prefersStatusBarHidden: Bool {
		return false
	}
	
    func checkVideosDownloaded(isFromCell: Bool = false){
		let videoUrls = self.checkAllVideoDownloaded(model: self.modelWorkoutDetail)
		if videoUrls.count == 0{
			self.isWorkOutVideoDownloaded = true
		}
		else{
            if isFromCell == false{
                self.isWorkOutVideoDownloaded = false
            }
		}
	}
	
	func setUpProgressWhichIsDownloading(){
		APP_DELEGATE.downloadService.showProgress = { [weak self](progress, id) in
			DispatchQueue.main.async {
				guard let weak = self else{return}
				if id == weak.modelWorkoutDetail.workoutId{
                weak.IBrpCircularProgress.updateProgress(progress)
				}
			}
		}
		APP_DELEGATE.downloadService.updateLabel = { [weak self](sLabelValue, id) in
			DispatchQueue.main.async {
				guard let weak = self else{return}
				if id == weak.modelWorkoutDetail.workoutId{
                    weak.IBbtnDownload.setImage(nil, for: .normal)
                    weak.IBbtnDownload.setImage(nil, for: .highlighted)
//					weak.IBbtnDownload.setTitle("\(sLabelValue)", for: .normal)
					weak.IBbtnDownload.setTitle("Downloading.", for: .normal)
				}
			}
		}
		APP_DELEGATE.downloadService.workoutDownloaded = { [weak self] (id) in
			DispatchQueue.main.async {
				guard let weak = self else{return}
				if id == weak.modelWorkoutDetail.workoutId{
					weak.isWorkOutVideoDownloaded = true
				}
			}
		}
	}
	
	@IBAction func btnOverViewTap(){
        
        let detailsVC = self.storyboard?.instantiateViewController(withIdentifier: "WorkOutExerciseDescription_Detail")as! WorkOutExerciseDescription_Detail
        detailsVC.modelWorkoutDetail = modelWorkoutDetail
        detailsVC.modalTransitionStyle = .crossDissolve
        detailsVC.modalPresentationStyle = .overCurrentContext
        self.present(detailsVC, animated: true, completion: nil)
        
	}
	
	@IBAction func downloadVideoFiles(sender : UIButton)
	{
        if isWorkoutLocked{
            pushToSubscriptionScreen()
            return
        }
        
        guard let exeCount = modelWorkoutDetail?.workoutExerciseList?.count, exeCount > 0 else {
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyExercises, completion: nil)
            return}
        if let obj = self.modelWorkoutDetail {
            if isWorkOutVideoDownloaded == false{
                if IBrpCircularProgress.isHidden == true{
                    IBrpCircularProgress.isHidden = false
					let videoUrls = self.checkAllVideoDownloaded(model: obj)
					if videoUrls.count > 0{
						self.setUpProgressWhichIsDownloading()
						APP_DELEGATE.downloadService.startWorkoutDownload(obj)
					}
					else{
						isWorkOutVideoDownloaded = true
						Workout.saveContactInDB(modelWorkoutDetail: self.modelWorkoutDetail)
//                        redirectToWorkoutVideoPlay()
					}
                }
            }
            else
            {
				//redirectToWorkoutVideoPlay()
                self.startWorkoutTimer()
            }
        }
	}
	
    
    func startWorkoutTimer() {
        self.isWorkoutStratTimerRunning = true
        self.viewWorkoutStartTimer.isHidden = false
        
        if IS_IPHONE_X
        {
          self.consTimerBottomSpace.constant = -34
        }
        else
        {
        self.consTimerBottomSpace.constant = 0
        }
        UIView.animate(withDuration: 0.8, animations: {
            self.view.layoutIfNeeded()

        })
        
        
        self.lblWorkoutStartTimer.font = UIFont(name:"Futura-Bold", size: 35.0)

        self.lblWorkoutStartTimer.text = "GET READY IN"
        DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
        
            self.secondsForWorkoutStartTime = 4
            self.workoutStartTimer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(self.updateWorkoutSratTimer)), userInfo: nil, repeats: true)
        })
    }
    
    @objc func updateWorkoutSratTimer() {
        
        self.lblWorkoutStartTimer.font = UIFont(name:"Futura-Bold", size: 98.0)

        if secondsForWorkoutStartTime == 0{
            self.workoutStartTimer.invalidate()
            self.redirectToWorkoutVideoPlay()
            //APP_DELEGATE.isRotateAllow = false
        }
        else if secondsForWorkoutStartTime == 1{
            secondsForWorkoutStartTime -= 1
            self.lblWorkoutStartTimer.text = "GO"
        }
        else{
            secondsForWorkoutStartTime -= 1
            self.lblWorkoutStartTimer.text = String(secondsForWorkoutStartTime)
        }
        
    }
    
    func redirectToWorkoutVideoPlay(){
        
        if self.isWorkoutLocked{
            pushToSubscriptionScreen()
            return
        }
        
        let featuredWorkOutVideoPlayViewController = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "FeaturedWorkOutVideoPlayViewController") as! FeaturedWorkOutVideoPlayViewController
        featuredWorkOutVideoPlayViewController.modelWorkoutDetail = modelWorkoutDetail
        featuredWorkOutVideoPlayViewController.isForDownload = self.isForDownload
        let navigation = UINavigationController(rootViewController: featuredWorkOutVideoPlayViewController)
        featuredWorkOutVideoPlayViewController.workOutId = modelWorkoutList != nil ? modelWorkoutList.workoutId : workoutId!
        featuredWorkOutVideoPlayViewController.onFeedBackSuccess = { ()
            self.onWorkoutLogSuccess?()
            APP_DELEGATE.isRotateAllow = false
            self.navigationController?.isNavigationBarHidden = true

        }
        featuredWorkOutVideoPlayViewController.onNaviationColor = { ()
            APP_DELEGATE.isRotateAllow = false
            self.navigationController?.isNavigationBarHidden = true
        }
        
        featuredWorkOutVideoPlayViewController.likeTapped = { (index) in
            if index < (self.modelWorkoutDetail?.workoutExerciseList?.count)!{
                self.modelWorkoutDetail?.workoutExerciseList[index].workoutExerciseIsFavourite = !(self.modelWorkoutDetail?.workoutExerciseList[index].workoutExerciseIsFavourite)!
            }
        }
        
        DispatchQueue.main.async {
            self.present(navigation, animated: false, completion: {
                self.viewWorkoutStartTimer.isHidden = true
                self.isWorkoutStratTimerRunning = false
                self.consTimerBottomSpace.constant = 200
            })
        }
        
    }
	
	func checkAllVideoDownloaded(model: ModelWorkoutDetail) -> [String]{
		var mArrVideoUrl = [String]()
		for list in model.workoutExerciseList{
			let theFileName = (list.workoutExerciseVideo as NSString).lastPathComponent
			if theFileName.isEmpty == false{
				let videoThere = DoviesGlobalUtility.checkWorkOutDownloadedOrNot(folderName: theFileName)
				if videoThere == false{
					mArrVideoUrl.append(list.workoutExerciseVideo)
				}
			}
		}
		return mArrVideoUrl
	}
	
    
	func callWorkOutDetailAPI()
	{
        IBtblView.showLoader()
        let workoutId = modelWorkoutList != nil ? modelWorkoutList.workoutId : self.workoutId!
        
		DoviesWSCalls.callWorkOutDetailAPI(dicParam: [WsParam.workoutId : workoutId ?? "", "program_id" : programId], onSuccess: { (success, msg, modelWorkOutDetail) in
            
			if success, let model = modelWorkOutDetail{
                self.IBtblView.hideLoader()
                self.isWorkoutLocked = model.workoutAccessLevel.uppercased() == "OPEN".uppercased() ? false : true
            
                if model.workoutName == ""
                {
                    GlobalUtility.showToastMessage(msg: "No longer available.")
                    self.navigationController?.popViewController(animated: true)
                }
                
                if model.workoutAccessLevel.uppercased() == "OPEN".uppercased(){
                    if self.isFromShare{
                        UIAlertController.showAlert(in: self, withTitle: nil, message: "Do you want to add this to your Workouts?", cancelButtonTitle: "No", destructiveButtonTitle: "Yes", otherButtonTitles: nil, tap: { (alertController, action, selectedIndex) in
                            if selectedIndex == 1{
                                self.callAddToMyWorkOutApi(with: model.workoutId)
                            }
                        })
                    }
                }
                
                if self.isFromCreateWorkout{
                    self.IBtblView.tableHeaderView = nil
                }else{
                    if self.isForDownload == false{
                        self.addRightNavigationButton()
                    }
                }
                
				self.modelWorkoutDetail = model
                self.isSubscriptionReuired = (self.isWorkoutLocked == true || model.isWorkoutContainPremiumExercise == true) ? true : false
                
				self.fillTheViewWithDetailData()
                
				let predicate = NSPredicate(format: "workoutId == '\(self.modelWorkoutDetail.workoutId!)'")
				let videoUrls = self.checkAllVideoDownloaded(model: self.modelWorkoutDetail)
				if let arrMusicTrack = Workout.getWorkoutFromContext(params: predicate) , arrMusicTrack.count > 0 {
					if videoUrls.count == 0{
						self.isWorkOutVideoDownloaded = true
					}
					else{
						self.isWorkOutVideoDownloaded = false
					}
				}
				else{
					self.isWorkOutVideoDownloaded = false
				}
				if let dic = APP_DELEGATE.downloadService.activeWorkoutDownload[self.modelWorkoutDetail.workoutId]{
					let model = dic as! ModelWorkoutDetail
                    self.IBbtnDownload.setImage(nil, for: .normal)
                    self.IBbtnDownload.setImage(nil, for: .highlighted)
                    //self.IBbtnDownload.backgroundColor = UIColor.colorWithRGB(r: 255, g: 255, b: 255, alpha: 0.2)
//					self.IBbtnDownload.setTitle("\(model.sDownloadLabel!)", for: .normal)
					self.IBbtnDownload.setTitle("Downloading.", for: .normal)
					self.setUpProgressWhichIsDownloading()
					self.IBrpCircularProgress.isHidden = false
				}
                
				self.IBtblView.reloadData()
				if self.isFromEdit == true{
					Workout.saveContactInDB(modelWorkoutDetail: self.modelWorkoutDetail, isForEdit: true)
				}
				self.isFromEdit = false
            }else if let strMessage = msg{
                DoviesGlobalUtility.showSimpleAlert(viewController: self, message: strMessage, completion: nil)
            }
            self.btnHeaderEditWorkout.isUserInteractionEnabled = true
            self.IBbtnShare.isUserInteractionEnabled = true
            self.activityIndicatorNew.stopAnimating()

			self.view.isUserInteractionEnabled = true
            self.Const_tablebottom.constant = 0

		}) { (error) in
			self.isFromEdit = false
            self.activityIndicatorNew.stopAnimating()

			self.view.isUserInteractionEnabled = true
            self.btnHeaderEditWorkout.isUserInteractionEnabled = true
            self.IBbtnShare.isUserInteractionEnabled = true

		}
	}
	
	func callToMakeWorkOutFavourite(){
		self.modelWorkoutList.workoutFavStatus = !self.modelWorkoutList.workoutFavStatus
		guard let cUser = DoviesGlobalUtility.currentUser else {return}
		var dic = [String: Any]()
		dic[WsParam.authCustomerId] = cUser.customerAuthToken
		dic[WsParam.moduleId] = modelWorkoutList.workoutId
		dic[WsParam.moduleName] = ModuleName.workout
		
		DoviesWSCalls.callFavouriteAPI(dicParam: dic, onSuccess: { (success, message, responseDict) in
			if !success{
				self.modelWorkoutList.workoutFavStatus = !self.modelWorkoutList.workoutFavStatus
            }else{
                self.onUpdateFavStatus?(self.modelWorkoutList.workoutFavStatus)
                self.updateFavButton()
            }
		}) { (error) in
		}
	}
	
	func callDeleteWorkOutWs(){
		UIAlertController.showAlert(in: self, withTitle: "", message: AlertMessages.deleteWorkoutConfirmation, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Delete", otherButtonTitles: nil) { (alert, action, index) in
			if index == 1{
				guard DoviesGlobalUtility.currentUser != nil else {return}
				var dic = [String: Any]()
				dic[WsParam.workoutId] = self.modelWorkoutList.workoutId
				
				DoviesWSCalls.cellDeleteWorkoutAPI(dicParam: dic, onSuccess: { (success, msg) in
					if success{
						Workout.deleteWorkoutWhichIsEdit(workoutId: self.modelWorkoutList != nil ? self.modelWorkoutList.workoutId : self.workoutId!)
					    	self.deleteWorkout?()
						self.navigationController?.popViewController(animated: true)
					}
					else{
                        DoviesGlobalUtility.showSimpleAlert(viewController: self, message: msg, completion: nil)
					}
				}) { (error) in
					
				}
			}
		}
	}
	
	func callToMakeExcerciseFavourite(indexPath: IndexPath, cell: FeaturedDetailListTableCell){
		self.modelWorkoutDetail.workoutExerciseList[indexPath.row].workoutExerciseIsFavourite = !self.modelWorkoutDetail.workoutExerciseList[indexPath.row].workoutExerciseIsFavourite
		
		guard let cUser = DoviesGlobalUtility.currentUser else {return}
		var dic = [String: Any]()
		dic[WsParam.authCustomerId] = cUser.customerAuthToken
		dic[WsParam.moduleId] = modelWorkoutDetail?.workoutExerciseList[indexPath.row].workoutExercisesId
		dic[WsParam.moduleName] = ModuleName.exercise
		
		DoviesWSCalls.callFavouriteAPI(dicParam: dic, onSuccess: { (success, message, responseDict) in
			if !success{
				self.modelWorkoutDetail.workoutExerciseList[indexPath.row].workoutExerciseIsFavourite = !self.modelWorkoutDetail.workoutExerciseList[indexPath.row].workoutExerciseIsFavourite
				cell.btnLike.isSelected = !cell.btnLike.isSelected
			}
		}) { (error) in
		}
	}
	
    func fillTheViewWithDetailData(){
        if isFromCreateWorkout {
            return
        }
        self.navigationItem.title = modelWorkoutDetail.workoutName
        print("modelWorkoutDetail=\(modelWorkoutDetail.workoutCreatedBy)")
        
        
      //  IBimgBg.sd_setImage(with:  URL(string: modelWorkoutDetail.workoutImage), placeholderImage: AppInfo.placeholderImage)
        
        IBimgBg.sd_setImage(with:  URL(string: modelWorkoutDetail.workoutImage), placeholderImage: nil)
        
      //  imageViewWorkoutStartTimer.sd_setImage(with:  URL(string: modelWorkoutDetail.workoutImage), placeholderImage: AppInfo.placeholderImage)
        
        imageViewWorkoutStartTimer.sd_setImage(with:  URL(string: modelWorkoutDetail.workoutImage), placeholderImage: nil)

        
        if modelWorkoutDetail.workoutLevel == "All"
        {
            IBlblWorkOutLevel.text = "ALL"
        }
        else
        {
            IBlblWorkOutLevel.text = modelWorkoutDetail.workoutLevel.uppercased()
        }
        IBlblWorkOutLevel.minimumScaleFactor = 0.5
        IBlblWorkOutLevel.adjustsFontSizeToFitWidth = true

        
        IBlblWorkOutCount.text = modelWorkoutDetail.workoutExerciseCount
        let totalTimeArr = modelWorkoutDetail.workoutTotalTime.components(separatedBy: " ")
        if totalTimeArr.count == 2{
            IBlblAvgMinutes.text = totalTimeArr[0]
            IBlblTimeUnits.text = totalTimeArr[1].capitalizingFirstLetter()
        }else{
            IBlblAvgMinutes.text = modelWorkoutDetail.workoutTotalTime
        }
        IBlblWorkOutName.text = modelWorkoutDetail.workoutName.uppercased()
        
        var heightGoodFor = modelWorkoutDetail.workoutGoodFor.heightForWithFont(font: IBlblGoodFor.font, width: IBlblGoodFor.bounds.width, insets: UIEdgeInsets(top: 0, left: 0, bottom:0, right: 0))
        
        if heightGoodFor == 0 {
            heightGoodFor = 15
        }
        
        var heightEquipment = modelWorkoutDetail.workoutEquipment.heightForWithFont(font: IBlblQuiptment.font, width: IBlblQuiptment.bounds.width, insets: UIEdgeInsets(top: 0, left: 0, bottom:0, right: 0))
        
        if heightEquipment == 0 {
            heightEquipment = 15
        }
        
        var txtGoodFor = modelWorkoutDetail.workoutGoodFor ?? ""
        txtGoodFor = txtGoodFor.replacingOccurrences(of: "|", with: ",", options: .literal, range: nil)
        
        print(txtGoodFor)
        IBlblGoodFor.text = txtGoodFor
        
        var txtEquip = modelWorkoutDetail.workoutEquipment ?? ""
        txtEquip = txtEquip.replacingOccurrences(of: "|", with: ",", options: .literal, range: nil)
        
        IBlblQuiptment.text = txtEquip
        
      //  IBimgWorkoutCreator.sd_setImage(with:  URL(string: modelWorkoutDetail.creatorProfileImage), placeholderImage: AppInfo.userPlaceHolder)
        
        IBimgWorkoutCreator.sd_setImage(with:  URL(string: modelWorkoutDetail.creatorProfileImage), placeholderImage: nil)
        
        IBlblCreatorName.text = modelWorkoutDetail.creatorName
        
        let hViewCreatedBy:CGFloat = (IS_IPHONE_6_PLUS ? 65 : 70)
        
        
        IBtblHeader.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: (806 * CGRect.widthRatio) + 44  + heightEquipment + 60 + heightGoodFor + 44 + (5 * CGRect.widthRatio) + (heightGoodFor == 0.0 ? 1 : 0) + (heightEquipment == 0.0 ? 1 : 0) + 30.0 + hViewCreatedBy )
        print("Header Height get \(IBtblHeader.frame.size.height)")
        
        self.updateFavButton()
        self.viewUpgraedeToPlay.isHidden = !self.isWorkoutLocked
        
        
        Const_tablebottom.constant = 0
        
    }
	
    
    func updateFavButton()  {
        if workoutId != nil{
            self.btnFavourite.isHidden = true
        }
        else{
            self.btnFavourite.isHidden = false
            let favImageName = modelWorkoutList.workoutFavStatus == false ? "btn_fav_s21a" : "favorite_icon_s34_h"
            self.btnFavourite.setImage(UIImage(named: favImageName), for: .normal)
        }
    }
    
	func addRightNavigationButton(){
		let detailsButton = UIButton(type: .custom)
		detailsButton.setImage(UIImage(named:"btn_more"), for: .normal)
		detailsButton.addTarget(self, action: #selector(self.showActionSheetToPerformOnWorkout), for: .touchUpInside)
		detailsButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
		let detailsBarItem = UIBarButtonItem(customView: detailsButton)
		navigationItem.rightBarButtonItems = [detailsBarItem]
	}
	
    func pushToSubscriptionScreen(){
        if let upgradeVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "UpgradeToPremiumViewController") as? UpgradeToPremiumViewController{
            upgradeVC.onPurchaseSuccess = {
                self.callWorkOutDetailAPI()
            }
            self.navigationController?.pushViewController(upgradeVC, animated: true)
        }
    }
    
    /**
     This method opens the actionsheet to select the options where user can edit, delete, share or add to my workouts based on the workout *'creatorID'*.
     */
	@objc func showActionSheetToPerformOnWorkout(){
		var arrData: [alertActionData]!
        
        if workoutId != nil{
            arrData = [alertActionData(title: "Edit workout", imageName: "btn_edit_s21a", highlighedImageName : "btn_edit_s21a")]
            arrData .append(alertActionData(title: "Save to my workout", imageName: "ico_workout_tab_h", highlighedImageName : "ico_workout_tab_h"))
            DoviesGlobalUtility.showDefaultActionSheetModified(on:self,actionData: arrData,cancelTitle: "Cancel") { (clickedIndex:Int, isCancel:Bool, title: String) in
                
                if clickedIndex == 0{
                    
                    if self.isFromMyDownload == true
                    {
                        GlobalUtility.showToastMessage(msg: "Unable to Edit workout.")
                    }
                    else{
                        self.navigateToEditWorkout(w_id: self.workoutId!)
                    }
                    
                }else if clickedIndex == 1{
					if self.isWorkoutLocked || self.modelWorkoutDetail.isWorkoutContainPremiumExercise == true{
						self.pushToSubscriptionScreen()
						return
					}
                    self.callAddToMyWorkOutApi(with: self.workoutId!)
                }
            }
            return
        }
        
        
		if modelWorkoutDetail != nil, modelWorkoutDetail.creatorId == DoviesGlobalUtility.currentUser?.customerId{
			arrData = [
				alertActionData(title: "Edit workout", imageName: "btn_edit_s21a", highlighedImageName : "btn_edit_s21a"),

				alertActionData(title: "Delete", imageName: "btn_del_s21a", highlighedImageName : "btn_del_s21a ")
			]
		}
		else{
			arrData = [
				alertActionData(title: "Edit workout", imageName: "btn_edit_s21a", highlighedImageName : "btn_edit_s21a"),
                
                 alertActionData(title: "Save to my workout", imageName: "ico_workout_tab_h", highlighedImageName : "ico_workout_tab_h"),
			]
		}
		
        DoviesGlobalUtility.showDefaultActionSheetModified(on:self,actionData: arrData,cancelTitle: "Cancel") { (clickedIndex:Int, isCancel:Bool, action_title: String) in
            
			if clickedIndex == 0{
                self.navigateToEditWorkout(w_id: self.modelWorkoutList.workoutId)

			}

			else if clickedIndex == 1{
                
                if action_title == "Delete"{
                    self.callDeleteWorkOutWs()
                }
                else{
                    if self.isWorkoutLocked || self.modelWorkoutDetail.isWorkoutContainPremiumExercise == true{
                        self.pushToSubscriptionScreen()
                        return
                    }
                    
                    self.callAddToMyWorkOutApi(with: self.modelWorkoutDetail.workoutId)
                }
			}
            
		}
	}
    
    func navigateToEditWorkout(w_id: String)  {
        
        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        {
            if self.isWorkoutLocked || self.modelWorkoutDetail.isWorkoutContainPremiumExercise == true {
                self.pushToSubscriptionScreen()
                return
            }
            let createWorkoutadmin = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "CreateWorkoutAdminVC") as? CreateWorkoutAdminVC
            
            createWorkoutadmin?.isEditSelected = true
            createWorkoutadmin?.IsEditedByAdmin = true

            if self.modelWorkoutDetail.workoutCreatedBy.uppercased() == "ADMIN"{
                createWorkoutadmin?.isCreatedByYou = false
            }
            else{
                createWorkoutadmin?.isCreatedByYou = true
            }
            
            CreateWorkoutAdminVC.arrExercise = [Any]()
            createWorkoutadmin?.parentExeID = w_id
            createWorkoutadmin?.modelWorkoutDetailData = self.modelWorkoutDetail
            createWorkoutadmin?.isForEditWork = { () in
                self.isFromEdit = true
                self.isWorkOutVideoDownloaded = false
                self.view.isUserInteractionEnabled = false
                self.callWorkOutDetailAPI()
            }
            self.navigationController?.pushViewController(createWorkoutadmin!, animated: true)
        }
        else
        {
            if self.isWorkoutLocked || self.modelWorkoutDetail.isWorkoutContainPremiumExercise == true {
                self.pushToSubscriptionScreen()
                return
            }
            let createWorkout = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "CreateWorkoutVC") as? CreateWorkoutVC
            
            createWorkout?.isEditSelected = true
            createWorkout?.isCreatedMyworkoutLog = isFromMyworkoutlog
            
            if self.modelWorkoutDetail.workoutCreatedBy.uppercased() == "ADMIN"{
                createWorkout?.isCreatedByYou = false
                createWorkout?.isCreatedWorkoutAdmin = true
            }
            else{
                createWorkout?.isCreatedByYou = true
            }
            CreateWorkoutVC.arrExercise = [Any]()
            createWorkout?.parentExeID = w_id
            createWorkout?.modelWorkoutDetailData = self.modelWorkoutDetail
            createWorkout?.isForEditWork = { () in
                self.isFromEdit = true
                self.isWorkOutVideoDownloaded = false
                self.view.isUserInteractionEnabled = false
                self.callWorkOutDetailAPI()
            }
            
            self.navigationController?.pushViewController(createWorkout!, animated: true)
        }
    }
	
    /**
     This method is used to add the current workout into my workouts list.
     - Parameter wId : is a workoutID
     */
    func callAddToMyWorkOutApi(with wId : String){
        guard DoviesGlobalUtility.currentUser != nil else {return}
        var dic = [String: Any]()
        
        dic[WsParam.workoutId] = wId
       
        DoviesWSCalls.callAddToMyWorkoutsAPI(dicParam: dic, onSuccess: { (success, messgae) in
            
            GlobalUtility.showToastMessage(msg: "Done")
//            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: messgae, completion: nil)
            if success{
                if let myWorkoutsVC = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "MyWorkoutsVC") as? MyWorkoutsVC{
                    self.navigationController?.pushViewController(myWorkoutsVC, animated: true)
                }
            }
        }) { (error) in
            
        }
    }
    
    func isExerciseLocked(indexPath: IndexPath) -> Bool {
        var isLock = false
        if let workoutExerciseList = modelWorkoutDetail?.workoutExerciseList {
            let selectedObj = workoutExerciseList[indexPath.row]
            isLock = selectedObj.workoutAccessLevel == "LOCK" ? true : false
        }
        return isLock
    }
    
    @objc func didTapVideoPlayButton(sender : UIButton!) {
        
        let idxPath : IndexPath = IndexPath(row: sender.tag - 100, section: 0)
        self.openVideoUIForTableCell(indexPath: idxPath )
    }
    /**
     This method is used to open the Video view for particular workout.
     - Parameter indexPath : is a the index to open the video view and play the workout.
     */
	func openVideoUIForTableCell(indexPath: IndexPath)
	{
        if isWorkoutLocked{
            pushToSubscriptionScreen()
            return
        }
        
        if self.isExerciseLocked(indexPath: indexPath){
            pushToSubscriptionScreen()
            return
        }
        
		if selectedRow == -1 {
			
			selectedRow = indexPath.row
			IBtblView.isScrollEnabled = false
            IsVideoOpen = "OPEN"
			let selectedCell = IBtblView.cellForRow(at: indexPath) as! FeaturedDetailListTableCell
            selectedCell.btnvideo.setImage(UIImage(named: "icon_arrow_dropdUp"), for: .normal)
            selectedCell.constPlayerHeight.constant = playerHeight
			self.visibleCellsDuringAnimation = IBtblView.visibleCells
			self.IBtblView.beginUpdates()
			self.IBtblView.endUpdates()
			
			selectedCell.setUpPlayerInCell(urlString: (modelWorkoutDetail?.workoutExerciseList[indexPath.row].workoutExerciseVideo)!)
			IBtblView.scrollToRow(at: indexPath, at: .none, animated: true)
		}
        else
        {
           self.closeVideoPlayForCell()
        }
    }
	
    /**
     This method is used to close the workout video.
     */
	func closeVideoPlayForCell(){
        guard let selectedCell = IBtblView.cellForRow(at: IndexPath(row: selectedRow, section: 0)) as? FeaturedDetailListTableCell else{return}
        selectedCell.btnvideo.setImage(UIImage(named: "icon_downarrow"), for: .normal)

        selectedCell.constPlayerHeight.constant = 0
		self.IBtblView.beginUpdates()
		selectedRow = -1
		self.IBtblView.endUpdates()
		for cell in self.self.visibleCellsDuringAnimation {
			if let theCell = cell as? FeaturedDetailListTableCell {
				
			}
		}
		selectedCell.stopVideo()
		selectedCell.removePlayerFromView()
		IBtblView.isScrollEnabled = true
        IsVideoOpen = "CLOSED"

	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension FeaturedWorkOutDetailViewController: UITableViewDataSource{
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return modelWorkoutDetail?.workoutExerciseList?.count ?? 0
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "FeaturedDetailListTableCell") as! FeaturedDetailListTableCell

		if self.isForDownload == true{
			cell.btnLike.isHidden = true
		}
		//cell.viewVideoPlayer.isHidden = true
        
        cell.btnvideo.tag = indexPath.row + 100
        cell.btnvideo.addTarget(self, action: #selector(self.didTapVideoPlayButton), for: .touchUpInside)

		if let workoutExerciseList = modelWorkoutDetail?.workoutExerciseList
		{
			if workoutExerciseList.count == (indexPath.row + 1) {
				cell.stackViewBreakTime.isHidden = true
				cell.constBreakTimeHeight.constant = 0
			} else {
				cell.stackViewBreakTime.isHidden = false
				cell.constBreakTimeHeight.constant = 38
			}
			
			cell.favouriteTap = { () in
                
                if self.IsVideoOpen == "OPEN"
                {
                    self.closeVideoPlayForCell()
                    return
                }
                
                if self.isWorkoutLocked{
                    self.pushToSubscriptionScreen()
                    return
                }
				if workoutExerciseList[indexPath.row].workoutAccessLevel.uppercased() == "LOCK"{
					self.pushToSubscriptionScreen()
					return
				}
				cell.btnLike.isSelected = !cell.btnLike.isSelected
				self.callToMakeExcerciseFavourite(indexPath: indexPath, cell: cell)
			}
            cell.shareTappedbtn = { () in
            
            if self.IsVideoOpen == "OPEN"
            {
                self.closeVideoPlayForCell()
                return
            }

//                DoviesGlobalUtility.generateBranchIoUrl(with: ModuleName.exercise, moduleId: workoutExerciseList[indexPath.row].workoutExercisesId, completionHandler: { (urlString) in
//                    DoviesGlobalUtility.showShareSheet(on: self, shareText: urlString)
//                })
                
   DoviesGlobalUtility.showShareSheet(on: self, shareText: workoutExerciseList[indexPath.row].workoutShareUrl)

            }
			cell.btnDetailTap = { () in
                
             
                let detailsVC = self.storyboard?.instantiateViewController(withIdentifier: "WorkOutExerciseDescription_Detail")as! WorkOutExerciseDescription_Detail
                detailsVC.workoutExerciseList = workoutExerciseList[indexPath.row]
                detailsVC.modalTransitionStyle = .crossDissolve
                detailsVC.modalPresentationStyle = .overCurrentContext
                self.present(detailsVC, animated: true, completion: nil)
                
			}
			
			let workoutExercise = workoutExerciseList[indexPath.row]
            cell.setUpFeaturedDetailData(workoutExercise: workoutExercise, isLastCell : (indexPath.row == modelWorkoutDetail!.workoutExerciseList.count - 1))
		
			if self.isFromCreateWorkout {
				cell.btnCheck.isHidden = false
				cell.btnLike.isHidden = true
				cell.btnCheck.isSelected = CreateWorkoutVC.createWorkoutArrayContainsExercise(exerciseObj: workoutExercise)
			}
			
			
			cell.videoCloseBtnTap = { ()
				self.closeVideoPlayForCell()
			}
//            cell.videoUpperLayerTapped = {
//                self.closeVideoPlayForCell()
//            }
			
			cell.videoDownloaded = {
				self.checkVideosDownloaded(isFromCell: true)
				if self.isWorkOutVideoDownloaded == true{
					Workout.saveContactInDB(modelWorkoutDetail: self.modelWorkoutDetail)
				}
			}
		}
		
		return cell
	}
}

extension FeaturedWorkOutDetailViewController: UITableViewDelegate {
    
 
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var extraheight : CGFloat = 0
        if let list = modelWorkoutDetail?.workoutExerciseList{
            let exerciseObj = list[indexPath.row]
            if let name : String = exerciseObj.workoutExerciseName{
                let height = name.heightWithConstrainedWidth(width: ScreenSize.width - 163, font: UIFont.tamilRegular(size: 14.0), lineSpace: 5.0)
                extraheight =  height > 30 ? 20 : 0
            }
        }
        if self.selectedRow == indexPath.row{
            if indexPath.row == (modelWorkoutDetail?.workoutExerciseList.count)! - 1{
               // return cellVideoHeight-38
                //change 27-12-18  - 20
                return cellVideoHeight - 20

            }
            if let workoutExerciseList = modelWorkoutDetail?.workoutExerciseList{
                let workout = workoutExerciseList[indexPath.row]
                if workout.isBreakTimeThere{
                    return cellVideoHeight + 25
                }else{
                    //return cellVideoHeight-38
                    //change 27-12-18  - 20
                    return cellVideoHeight - 20

                }
            }else{
                return cellVideoHeight
            }
            
        }else{
            
            if indexPath.row == (modelWorkoutDetail?.workoutExerciseList.count)! - 1{
                if extraheight > 0{
                    return cellLastHeight
                }else{
                    return cellLastHeight
                }
                
            }
            if let workoutExerciseList = modelWorkoutDetail?.workoutExerciseList
            {
                let workout = workoutExerciseList[indexPath.row]
                if workout.isBreakTimeThere{
                    return cellHeight
                }else{
                    return cellHeight-38
                }
            }else{
                return cellHeight
            }
        }
      
	}
	
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
         return cellHeight
    }
    
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if isWorkoutLocked{
            pushToSubscriptionScreen()
            return
        }
        
		if let workoutExerciseList = modelWorkoutDetail?.workoutExerciseList {
			let selectedObj = workoutExerciseList[indexPath.row]
			if CreateWorkoutVC.createWorkoutArrayContainsExercise(exerciseObj: selectedObj) {
				if let indexOfObj = CreateWorkoutVC.indexOfObjectInCreateWorkoutArray(exerciseObj: selectedObj) {
					CreateWorkoutVC.arrExercise.remove(at: indexOfObj)
				}
			} else {
				CreateWorkoutVC.arrExercise.append(selectedObj)
			}
			let cell = tableView.cellForRow(at: indexPath) as! FeaturedDetailListTableCell
			cell.btnCheck.isSelected = !cell.btnCheck.isSelected
            if selectedRow == -1
            {
             self.openVideoUIForTableCell(indexPath: indexPath)
            }
            else
            {
                self.closeVideoPlayForCell()
            }
		}
	}
}

extension String {
    var removingNewlines: String {
        return components(separatedBy: .newlines).joined()
    }
}

extension FeaturedWorkOutDetailViewController: MPMediaPickerControllerDelegate{
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        if mediaItemCollection.items.count > 0 {
            musicPlayer.setQueue(with: mediaItemCollection)
            DoviesGlobalUtility.isMusicAdded = true
            print("Music added to Queue successfully..")
        }
        
        mediaPicker.dismiss(animated: true) {
            
        }
    }
    
    func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
        mediaPicker.dismiss(animated: true) {
            
        }
    }
}

extension FeaturedWorkOutDetailViewController:UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        self.stackViewContainer.isHidden = offset.y > 400 ? true : false
    }
}
