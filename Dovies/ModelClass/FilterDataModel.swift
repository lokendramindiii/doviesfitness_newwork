//
//	List.swift
//
//	Create by hb on 12/3/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FilterDataModel: NSObject, NSCoding{

	var gmDisplayName : String!
	var gmGoogforMasterId : String!
    var groupName : String!
    var groupKey : String!

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		gmDisplayName = dictionary["display_name"] as? String ?? ""
		gmGoogforMasterId = dictionary["id"] as? String ?? ""
        groupName = dictionary["group_name"] as? String ?? ""
        groupKey = dictionary["group_key"] as? String ?? ""
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if gmDisplayName != nil{
			dictionary["display_name"] = gmDisplayName
		}
		if gmGoogforMasterId != nil{
			dictionary["id"] = gmGoogforMasterId
		}
        if groupName != nil{
            dictionary["group_name"] = groupName
        }
        if groupKey != nil{
            dictionary["group_key"] = groupKey
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
        gmDisplayName = aDecoder.decodeObject(forKey: "display_name") as? String
         gmGoogforMasterId = aDecoder.decodeObject(forKey: "id") as? String
        groupName = aDecoder.decodeObject(forKey: "group_name") as? String
        groupKey = aDecoder.decodeObject(forKey: "group_key") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if gmDisplayName != nil{
			aCoder.encode(gmDisplayName, forKey: "display_name")
		}
		if gmGoogforMasterId != nil{
			aCoder.encode(gmGoogforMasterId, forKey: "id")
		}
        if groupName != nil{
            aCoder.encode(groupName, forKey: "group_name")
        }
        if groupKey != nil{
            aCoder.encode(groupKey, forKey: "group_key")
        }

	}

}
