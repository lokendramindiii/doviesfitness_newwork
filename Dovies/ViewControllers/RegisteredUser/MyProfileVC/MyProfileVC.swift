//
//  MyProfileVC.swift
//  Dovies
//
//  Created by hb on 19/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit



class MyProfileVC: BaseViewController  {
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var lblUserName: UILabel!
    @IBOutlet var imgViewProfile: UIImageView!
    @IBOutlet var imgViewRedDot: UIImageView!
    
    @IBOutlet var constViewBlackTop: NSLayoutConstraint!
    
    let cellWidth = 80 * CGRect.widthRatio
    //let cellHeight = 40 * CGRect.widthRatio
    
    var cellHeight : CGFloat = 0
    let arrTitle = ["My Workout", "My Diet", "Workout Log","My Plan","Favorites","Notification"]
    // Purchase History
    let arrOptionImage = ["myProfileMyworkoutImg", "myProfileDiet", "myProfileWorkoutlogImg","myProfileMyPlan","myProfileFavourite","Notification_IconHeaderNew"]
    // purchase_history
    
    let arrSegues = ["myProfileToMyWorkouts", "myProfileToMyDietPlan", "MyWorkOutLogViewController","myProfileToAddPlan","myProfileToFavorite","SubOrPurHistoryVC"]
    
    //MARK: ViewLifeCycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DoviesGlobalUtility.setFilterData()
        
        self.imgViewRedDot.isHidden = true

        self.navigationItem.title = "My Profile".uppercased()
        //self.addNavigationSettingButton()
        let h = self.view.frame.size.height - (self.view.frame.size.width + self.view.frame.size.width/2)
        cellHeight = h/2
        self.setDetails()
        APP_DELEGATE.profileNavController = self.navigationController
        
    }

    
    func getUserProfileWSCall(){
        DoviesWSCalls.callGetUserInfoAPI(dicParam: nil, onSuccess: {[weak self] (isSuccess, message, response) in
            guard let weakSelf = self else{return}
            if isSuccess, let dic = response {
                UserDefaults.standard.set(dic, forKey: UserDefaultsKey.userData)
                DoviesGlobalUtility.currentUser = User.init(fromDictionary: dic)
                // self?.setDetails()
                weakSelf.updateSidepanelProfileImage()
                
                if DoviesGlobalUtility.currentUser?.notifyCount == "0"
                {
                    self!.imgViewRedDot.isHidden = true
                    notificationVC.readUnreadStatus = "Read"
                }
                else
                {
                    self!.imgViewRedDot.isHidden = false
                    notificationVC.readUnreadStatus = "Unread"
                }
                
            }
        }) { (error) in
        }
    }
    
    func updateSidepanelProfileImage(){
        if let viewController = sideMenuController?.sideViewController as? SideMenuViewController{
            viewController.setUserPhotoAndName()
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
   
        // Navigation--Color  after add
     //   self.navigationController?.navigationBar.barTintColor = UIColor.appNavColor()
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor : UIColor.white,
            NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]

        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        
        
        if notificationVC.readUnreadStatus == "Read"
        {
            self.imgViewRedDot.isHidden = true
        }
        else if notificationVC.readUnreadStatus == "Unread"
        {
            self.imgViewRedDot.isHidden = false
        }
        
        getUserProfileWSCall()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewWillDisappear(animated)
    }
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        //self.imgViewProfile.setRoundBorder(radius: self.imgViewProfile.frame.size.width / 2, color: UIColor.darkGray, size: 1.0)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Instance methods
    
    func addNavigationSettingButton() {
        self.navigationItem.leftBarButtonItems = self.getSettingButtonItems(target: self, selector: #selector(self.btnSettingTapped(sender:)), imageName: "setting_icon_top_s24_h")
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.black
    }
    
    
    //    @IBAction func btnSettingTapped() {
    //
    //        let Setting = self.storyboard?.instantiateViewController(withIdentifier: "SettingViewController")as! SettingViewController
    //        Setting.profileUpdated = {
    //            self.setDetails()
    //        }
    //        self.navigationController?.pushViewController(Setting, animated: true)
    //    }
    
    @objc @IBAction func btnSettingTapped(sender: UIButton) {
        
        let Setting = self.storyboard?.instantiateViewController(withIdentifier: "SettingViewController")as! SettingViewController
        Setting.profileUpdated = {
            self.setDetails()
        }
        self.navigationController?.pushViewController(Setting, animated: true)
    }
    
    func getSettingButtonItems(target: AnyObject, selector: Selector, imageName: String) -> [UIBarButtonItem]? {
        if let image = UIImage(named: imageName) {
            let itemWidth = 30
            let viewMain: UIView = UIView(frame: CGRect(x: 0, y: 0, width: itemWidth, height: itemWidth))
            let btnLeft: UIButton = UIButton(type: .custom)
            btnLeft.frame = CGRect(x: 0, y: 0, width: itemWidth, height: itemWidth)
            btnLeft.setImage(image, for: .normal)
            btnLeft.addTarget(target, action: selector, for: .touchUpInside)
            viewMain.addSubview(btnLeft)
            let barBtnItem: UIBarButtonItem = UIBarButtonItem(customView: viewMain)
            let frontSpacer: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
            frontSpacer.width = -10
            let arrBarButton = [frontSpacer, barBtnItem]
            return arrBarButton
        }
        return nil
    }
    
    func setDetails() {
        guard let userData = DoviesGlobalUtility.currentUser else {
            return
        }
        self.lblName.text = userData.customerFullName
        self.lblUserName.text = "@\(userData.customerUserName.lowercased() ?? "")"
        //  self.imgViewProfile.sd_setImage(with: URL(string: userData.customerProfileImage), placeholderImage: AppInfo.placeholderImage)
        
        self.imgViewProfile.sd_setImage(with: URL(string: userData.customerProfileImage), placeholderImage: nil)
        
        
    }
    
    func addRightNavigationBarItems() {
        let notificationButton = GlobalUtility.getNavigationRightButtonItem(target: self, selector: #selector(self.btnNotificationTapped(sender:)), imageName: "notification_icon-top_s24_h")
        if  let editButton = GlobalUtility.getNavigationRightButtonItem(target: self, selector: #selector(self.btnEditTapped(sender:)), imageName: "edit_icon_top_s24_h").0, let navNotifButton = notificationButton.0 {
            self.navigationItem.rightBarButtonItems = [ navNotifButton, editButton]
        }
    }
    
    // MARK: IBAction methods
    
    @objc @IBAction func btnEditTapped(sender: UIButton) {
        let AboutYou = self.storyboard?.instantiateViewController(withIdentifier: "AboutYouVC")as! AboutYouVC
        AboutYou.profileUpdated = {
            self.setDetails()
        }
        self.navigationController?.pushViewController(AboutYou, animated: true)
    }
    
    @IBAction func btnhistoryAction(sender: UIButton) {
        //        let storyboard = UIStoryboard.init(name: "SidePanel", bundle: nil)
        //
        //        let AboutYou = storyboard.instantiateViewController(withIdentifier: "SubOrPurHistoryVC")as! SubOrPurHistoryVC
        //
        //        self.navigationController?.pushViewController(AboutYou, animated: true)
        
        let Notification = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC")as! NotificationVC
        self.navigationController?.pushViewController(Notification, animated: true)
    }
    
    @IBAction func btnEditTapped() {
        
        let arrData:[alertActionData] =
            [alertActionData(title: "Edit Profile", imageName: "edit_icon_top_s24_h", highlighedImageName : "edit_icon_top_s24_h")
                
                //  , alertActionData(title: "Notification", imageName: "notification_icon-top_s24_h", highlighedImageName : "notification_icon-top_s24_h")
        ]
        DoviesGlobalUtility.showDefaultActionSheet(on:self,actionData: arrData,cancelTitle: "CANCEL") { (clickedIndex:Int, isCancel:Bool) in
            if clickedIndex == 0{
                
                let AboutYou = self.storyboard?.instantiateViewController(withIdentifier: "AboutYouVC")as! AboutYouVC
                AboutYou.profileUpdated = {
                    self.setDetails()
                }
                self.navigationController?.pushViewController(AboutYou, animated: true)
            }
            else{
                
                //                let Notification = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC")as! NotificationVC
                //                self.navigationController?.pushViewController(Notification, animated: true)
                
            }
        }
    }
    
    @objc func btnNotificationTapped(sender: UIButton) {
        
        let Notification = self.storyboard?.instantiateViewController(withIdentifier: "NotificationVC")as! NotificationVC
        self.navigationController?.pushViewController(Notification, animated: true)
    }
    
    
    
    @IBAction func btnAddTapped(sender: UIButton) {
        let arrData:[alertActionData] =
            [alertActionData(title: "Create a Plan", imageName: "calender_icon_s24a_h", highlighedImageName : "calender_icon_s24a"),
             alertActionData(title: "Create a Workout", imageName: "myworkout_icon", highlighedImageName : "myworkout_icon")]
        DoviesGlobalUtility.showDefaultActionSheet(on:self,actionData: arrData,cancelTitle: "CANCEL") { (clickedIndex:Int, isCancel:Bool) in
            if clickedIndex == 0{
                let PlanOverview = self.storyboard?.instantiateViewController(withIdentifier: "PlanOverviewVC")as! PlanOverviewVC
                self.navigationController?.pushViewController(PlanOverview, animated: true)
            }
            else{
                if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
                {
                    let CreateWorkout = self.storyboard?.instantiateViewController(withIdentifier: "CreateWorkoutAdminVC")as! CreateWorkoutAdminVC
                    CreateWorkoutAdminVC.arrExercise = [Any]()
                    CreateWorkout.IsEditedByAdmin = false
                    self.navigationController?.pushViewController(CreateWorkout, animated: true)
                }
                else
                {
                    let CreateWorkout = self.storyboard?.instantiateViewController(withIdentifier: "CreateWorkoutVC")as! CreateWorkoutVC
                    CreateWorkoutVC.arrExercise = [Any]()
                    CreateWorkout.isFromMyProfileCreate = true
                    self.navigationController?.pushViewController(CreateWorkout, animated: true)
                }
            }
        }
    }
}


extension MyProfileVC : UICollectionViewDataSource {
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return 6
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "optionCell", for: indexPath) as! OptionCell
		
		cell.label.text = self.arrTitle[indexPath.row]
      //  cell.label.font = UIFont.tamilRegular(size: 14.0)
		cell.imageViewOption.image = UIImage(named: self.arrOptionImage[indexPath.row])
		
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		if arrSegues[indexPath.item].isEmpty == false
		{
			if indexPath.item == arrSegues.count - 1
			{
				let sidePanel = Storyboard.SidePanel.instantiate(SubOrPurHistoryVC.self)
				if let tab = sideMenuController?.centerViewController as? TabBarViewController
				{
					let selectedTab = tab.viewControllers![tab.selectedIndex] as! UINavigationController
                    
					selectedTab.pushViewController(sidePanel, animated: true)
				}
				return
			}
			self.performSegue(withIdentifier: arrSegues[indexPath.item], sender: nil)
		}
	}
	
	func collectionView(_ collectionView: UICollectionView, didHighlightItemAt indexPath: IndexPath) {
		let cell = collectionView.cellForItem(at: indexPath) as! OptionCell
		cell.imageViewOption.image = UIImage(named: self.arrOptionImage[indexPath.row])
	}
	
	func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
		let cell = collectionView.cellForItem(at: indexPath) as! OptionCell
		cell.imageViewOption.image = UIImage(named: self.arrOptionImage[indexPath.row])
	}
}

extension MyProfileVC: UICollectionViewDelegateFlowLayout {
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return CGSize(width: cellWidth, height: cellHeight)
	}
}

class OptionCell: UICollectionViewCell {
	@IBOutlet weak var imageViewOption: UIImageView!
	@IBOutlet weak var label: UILabel!
}

