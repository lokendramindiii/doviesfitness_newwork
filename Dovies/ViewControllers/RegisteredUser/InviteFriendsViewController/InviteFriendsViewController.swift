//
//  InviteFriendsViewController.swift
//  Dovies
//
//  Created by Neel Shah on 14/04/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class InviteFriendsViewController: BaseViewController {
    @IBOutlet var lbl_Invite       : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        lbl_Invite.text = """
        SHARE THE LOVE WITH YOUR
        FRIENDS AND FAMILY.
        INSPIRE THEM TO START
        WORKING OUT.
"""
        //        self.navigationItem.title = "Invite a Friend".uppercased()
        //        self.addNavigationBackButton()
        

    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
	
    @IBAction func Action_Back()
    {
        self.navigationController?.popViewController(animated: true)
    }
	@IBAction func inviteFriendsTap(){
		//let activitySheet = UIActivityViewController(activityItems: ["Come join me. Install Doviesfitness app and enjoy lots of workout and more with me.  Install the Doviesfitness app here: https://itunes.apple.com/us/app/doviesfitness/id1370793122?ls=1&mt=8 #beinspiredalways"], applicationActivities: nil)
       
        // dovies change 13-04-2019
        
        let activitySheet = UIActivityViewController(activityItems: ["Come and join me. Download the Doviesfitness app and let's workout together. Install the Doviesfitness app here: https://itunes.apple.com/us/app/doviesfitness/id1370793122?ls=1&mt=8 #beinspiredalways"], applicationActivities: nil)
        
		self.present(activitySheet, animated: true, completion: nil)
	}
}
