//
//  CollectionFullImageCell.swift
//  CollectionViewPhotoGellery
//
//  Created by Narendra Goojer on 30/09/18.
//  Copyright © 2018 Narendra Goojer. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class CollectionFullImageCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lbl_Weight: UILabel!
    @IBOutlet weak var View_DateWeight: UIView!
    @IBOutlet weak var ContentView: UIView!
    @IBOutlet weak var heightcollectionConstraint  : NSLayoutConstraint!
    
    @IBOutlet weak var activityIndicatorView : NVActivityIndicatorView!{
        didSet{
            activityIndicatorView.padding = 20
            activityIndicatorView.color = UIColor.lightGray
            activityIndicatorView.type = .circleStrokeSpin
            activityIndicatorView.startAnimating()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let path = UIBezierPath(roundedRect:View_DateWeight.bounds,
                                byRoundingCorners:[.topLeft, .topRight],
                                cornerRadii: CGSize(width: 8.0, height:  8.0))
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        View_DateWeight.layer.mask = maskLayer
    }
    
    func loadCellUI(model: ModelWorkoutLogListImage) {
        
        if IS_IPHONE_XSMAX_XR
        {
            heightcollectionConstraint.constant = 95
        }
        
        self.lblDate.text = model.logDate.strrigWithFormat(format: "d MMM, YYYY")
        self.lbl_Weight.text = model.CustomerWeight
        
        image.sd_setImage(with: URL(string: model.logImageURL )) { (image, error, cache, url) in
            self.activityIndicatorView.stopAnimating()
        }
    }
}
