//
//  ProgramsCollectionCell.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

class ProgramsCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imgLock: UIImageView!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var imgBackground : UIImageView!
    @IBOutlet weak var constTitleHeight : NSLayoutConstraint!
    @IBOutlet weak var lblProgrameName : UILabel!
    @IBOutlet weak var imgFadeBackground : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        lblProgrameName.isHidden = true
        imgFadeBackground.isHidden = true
    }

    func setCellUI(with program : Program){
       //  imgBackground.sd_setImage(with:  URL(string: program.programImage), placeholderImage: AppInfo.placeholderImage)
        
        imgBackground.sd_setImage(with:  URL(string: program.programImage), placeholderImage: nil)

        //imgBackground.image =  UIImage(named: "2ND_PAGE_OF_APP.png")
        
        constTitleHeight.constant = 0
        if program.programAccessLevel == "LOCK" {
    
            if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
            {
                imgLock.isHidden = true
            }
            else
            {
                imgLock.isHidden = false
            }
        }
        else{
            imgLock.isHidden = true
        }
        lblProgrameName.text = program.programName.uppercased()
        lblProgrameName.isHidden = false
        imgFadeBackground.isHidden = false
    }
    
    func setFavCellUI(with program : Program){
       // imgBackground.sd_setImage(with:  URL(string: program.programImage), placeholderImage: AppInfo.placeholderImage)
        
        imgBackground.sd_setImage(with:  URL(string: program.programImage), placeholderImage: nil)

        //constTitleHeight.constant = 20
        if program.programAccessLevel == "LOCK" {            
            if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
            {
                imgLock.isHidden = true
            }
            else
            {
                imgLock.isHidden = false
            }
        }
        else{
            imgLock.isHidden = true
        }
        //lblTitle.text = program.programName
        
        // loks add code lblProgrameName
        constTitleHeight.constant = 0
        lblProgrameName.text = program.programName.uppercased()
        lblProgrameName.isHidden = false
        imgFadeBackground.isHidden = false


    }
    
}
