//
//  UserListVC.swift
//  Dovies
//
//  Created by Mindiii on 12/5/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class UserListVC: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var UserTblview: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var btnPublishDone: UIButton!
    @IBOutlet weak var searchbar_heightconstraint: NSLayoutConstraint!
    
    var Str_Title = ""
    var WorkoutId = ""
    var shouldLoadMore = false
    var isloadData = false
    
    var currentPageIndex = 1
    var activityIndicatorNew : NVActivityIndicatorView!

    var arrWorkoutGroupListing = [ModelWorkoutCollection]()
    var arrCreatedByListing = [ModelCreatedBy]()
    
    var FilterUserlist  = [ModelUserlist]()
    var arrUserListing = [ModelUserlist]()
    var isSearch : Bool!
    var selected: [String] = [String]()
    var arrIdData = [String]()
    var completion : ((_ selected: [String]?,_ arrUserID: [String]?) -> ())?
    
    
    var workoutName = String()
    var SelectWorkoutCollectionId = String()
    var completionWorkoutGroupBy : ((_ selected: String?,_ WorkoutId: String?) -> ())?
    
    var guestName = String()
    var SelectGuestId = String()
    var completionCreatedBy : ((_ selected: String?,_ GuestId: String?) -> ())?
    
    
    //MARK: ViewLifeCycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        arrUserListing.removeAll()
        
        searchBar.delegate = self
        self.searchBar.placeholder = "Search"
        
        self.navigationItem.title = Str_Title.uppercased()
        self.navigationItem.hidesBackButton  = true
     //   self.addNavigationBackButton()
        self.addNavigationWhiteBackButton()

        self.UserTblview.tableFooterView = UIView()
        isSearch = false
        
        if Str_Title == "Workout Group"
        {
            searchbar_heightconstraint.constant = 0
            btnPublishDone .setTitle("PUBLISH", for: .normal)
        }
        else if Str_Title == "Created By"{
            btnPublishDone .setTitle("DONE", for: .normal)
            searchbar_heightconstraint.constant = 0
        }
        else
        {
            print(arrIdData)
            print(selected)
            print(arrIdData.count)
            print(selected.count)

            let frame = CGRect(x: self.view.center.x-22, y: self.view.center.y-22, width: 45, height: 45)
            activityIndicatorNew = NVActivityIndicatorView(frame: frame)
            activityIndicatorNew.center = self.view.center
            activityIndicatorNew.type = . circleStrokeSpin // add your type
            activityIndicatorNew.color = UIColor.lightGray // add your color
            APP_DELEGATE.window?.addSubview(activityIndicatorNew)
            activityIndicatorNew.startAnimating()

            
            self.isloadData = true
            self.callGetcustomerUserListingNewWS(text: self.searchBar.text ?? "")
            
            btnPublishDone .setTitle("DONE", for: .normal)
        }
    }
    
    func checkSearchBarActive() -> Bool {
        if searchBar.text != "" {
            return true
        } else {
            return false
        }
    }
    

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: UITableviewDatasource Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if Str_Title == "Workout Group"
        {
            return arrWorkoutGroupListing.count
        }
        else if Str_Title == "Created By"{
            
            return arrCreatedByListing.count
        }
            // User list for else
        else
        {
            return arrUserListing.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserListCell") as! UserListCell
        cell.btnSelection.tag = indexPath.row
        // title workout group
        if Str_Title == "Workout Group"
        {
            if SelectWorkoutCollectionId == arrWorkoutGroupListing[indexPath.row].groupId{
                cell.btnSelection.isSelected = true
            }else{
                cell.btnSelection.isSelected = false
            }
            cell.setDataModelWorkoutCollection(arrWorkoutGroupListing[indexPath.row], at: indexPath)
        }
            // title Created By
        else if Str_Title == "Created By"{
            if SelectGuestId == arrCreatedByListing[indexPath.row].guestId{
                cell.btnSelection.isSelected = true
            }else{
                cell.btnSelection.isSelected = false
            }
            cell.setDataModelCreatedBy(arrCreatedByListing[indexPath.row], at: indexPath)
        }
            // title user
        else
        {
            if let fetchtresult = arrIdData.first(where: { $0 == arrUserListing[indexPath.row].userId })  {
                cell.btnSelection.isSelected = true
                print(fetchtresult)
            }
            else
            {
                cell.btnSelection.isSelected = false
            }
            
           //  cell.btnSelection.isSelected = arrIdData.contains(arrUserListing[indexPath.row].userId)
          
             cell.setDataUserlist(arrUserListing[indexPath.row], at: indexPath)
        }
        
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    
    // MARK: UITableviewDelegate Methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if Str_Title == "Workout Group"
        {
        }
        else if Str_Title == "Created By"{
        }
        else
        {
            if indexPath.item == (arrUserListing.count - 1) && shouldLoadMore == true {
                shouldLoadMore = false
                if self.isloadData == true
                {
                    self.isloadData = false
                    self.callGetcustomerUserListingNewWS(text: self.searchBar.text ?? "")
               }
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // title workout group
        if Str_Title == "Workout Group"
        {
            if SelectWorkoutCollectionId.contains(arrWorkoutGroupListing[indexPath.row].groupId){
                self.workoutName = ""
                self.SelectWorkoutCollectionId = ""
                
            }else{
                self.SelectWorkoutCollectionId = arrWorkoutGroupListing[indexPath.row].groupId
                self.workoutName = arrWorkoutGroupListing[indexPath.row].groupName
            }
            UserTblview.reloadData()
        }
            // title Created By
            
        else if Str_Title == "Created By"{
            if SelectGuestId.contains(arrCreatedByListing[indexPath.row].guestId){
                self.guestName = ""
                self.SelectGuestId = ""
                
            }else{
                self.SelectGuestId = arrCreatedByListing[indexPath.row].guestId
                self.guestName = arrCreatedByListing[indexPath.row].guestName
            }
            UserTblview.reloadData()
        }
            
            // title userlist
        else
        {
            if arrIdData.contains(arrUserListing[indexPath.row].userId){
                
                if let index = self.arrIdData.index(where: {$0 == arrUserListing[indexPath.row].userId}){
                    self.arrIdData.remove(at: index)
                    self.selected.remove(at: index)
                }
                else
                {
                    print("llllllllllll")
                }
            }else{
                self.arrIdData.append(arrUserListing[indexPath.row].userId)
                self.selected.append(arrUserListing[indexPath.row].name)
            }
            UserTblview.reloadRows(at: [indexPath], with: .none)
        }
    }
    
    @objc func buttonAction(_ sender: UIButton!) {
        print("Button tapped")
    }
    
    @IBAction func btnDoneClicked(_ sender: Any) {
        if Str_Title == "Workout Group"
        {
            if SelectWorkoutCollectionId == "" || SelectWorkoutCollectionId == "0"
            {
            }
            else
            {
                self.callworkoutsPublishApi(WorkoutId: WorkoutId, workout_collectionId: SelectWorkoutCollectionId)
            }
        }
            
        else if Str_Title == "Created By"{
            self.completionCreatedBy?(self.guestName, self.SelectGuestId)
        }
        else
        {
            self.completion?(self.selected, self.arrIdData)
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func getSearchArrayContains(_ text : String) {
        
        print(text)
        
        if isloadData == true
        {
            isloadData = false
            self.arrUserListing.removeAll()
            currentPageIndex = 1
            self.callGetcustomerUserListingNewWS(text: self.searchBar.text ?? "")
        }
        else
        {

            let texttrim = text.trimmingCharacters(in: .whitespacesAndNewlines)
            if texttrim == ""
            {
                isloadData = false
                self.arrUserListing.removeAll()
                currentPageIndex = 1
                self.callGetcustomerUserListingNewWS(text: texttrim)
            }
        }
    }
}

extension UserListVC: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        getSearchArrayContains(searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearch = false
        searchBar.resignFirstResponder()
    }
}

extension UserListVC
{
    func callworkoutsPublishApi(WorkoutId:String , workout_collectionId: String){
        var dic = [String: Any]()
        
        dic[WsParam.workout_collectionId] = workout_collectionId
        dic[WsParam.workoutId] = WorkoutId
        
        DoviesWSCalls.callGetpublishWorkoutAPI(dicParam: dic, onSuccess: {[weak self]  (isSuccess, response, message) in
            guard let weakSelf = self else{return}
            if isSuccess{
                GlobalUtility.showToastMessage(msg: message!)
            }
        }) { (error) in
            
        }
    }
    
    func callGetcustomerUserListingNewWS(text :String){
        
        if currentPageIndex == 1
        {
            self.arrUserListing.removeAll()
        }
        var dic = [String: Any]()
        
        dic[WsParam.searchQuery] = text
        dic[WsParam.pageIndex] = currentPageIndex
        dic[WsParam.allowedUserId] = self.arrIdData.joined(separator:  ",")
        DoviesWSCalls.callGetModelGetuserListDataSearchAPI(dicParam: dic, onSuccess: {  [weak self](sucess, arrgetuserlist,arrCreatedBy,arrWorkoutGroup, strMessage ,hasNextPage) in
            
            guard let weakSelf = self else{return}
            weakSelf.shouldLoadMore = hasNextPage
            GlobalUtility.hideActivityIndi(viewContView: self!.view)
            
            if sucess
            {
                
//                for obj in arrgetuserlist!
//                {
//                    if let firstSuchElement = weakSelf.arrUserListing.first(where: { $0.userId == obj.userId }) {
//
//                        print(firstSuchElement.userId)
//                    }
//                    else
//                    {
//                        weakSelf.arrUserListing.append(obj)
//                    }
//                }
                
                if weakSelf.currentPageIndex == 1{
                    weakSelf.arrUserListing = arrgetuserlist!

                }else{
                    weakSelf.arrUserListing += arrgetuserlist!
                }
                
                if hasNextPage{
                    weakSelf.currentPageIndex += 1
                }
                self?.UserTblview.reloadData()
            }
            self?.activityIndicatorNew.stopAnimating()
            self?.isloadData = true
        }) { (error) in
           
            self.activityIndicatorNew.stopAnimating()
            self.isloadData = true
        }
    }
}




