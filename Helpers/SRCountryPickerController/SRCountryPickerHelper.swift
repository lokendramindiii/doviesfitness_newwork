//
//  SRCountryPickerHelper.swift
//  BaseProj
//
//  Copyright © 2017 Company Name. All rights reserved.
//

import UIKit

class SRCountryPickerHelper: NSObject {
	static let shared = SRCountryPickerHelper()
	
	var countrySelected: ((Country)->())!
	
	func setStaticTextsForPicker(navigationTitle: String = "COUNTRIES", cancelButtonTitle: String = "Cancel", searchBarPlaceholder: String = "Search Country") {
		SRCountryPickerController.dicTexts = ["title":navigationTitle,"cancel_btn_title":cancelButtonTitle,"search_bar_placeholder":searchBarPlaceholder]
	}
	
	func showCountryPicker(_ completion: @escaping ((Country)->())) {
		self.countrySelected = completion
		let sRCountryPickerController = SRCountryPickerController(nibName: "SRCountryPickerController", bundle: nil)
		sRCountryPickerController.countryDelegate = self
		let nvController = UINavigationController(rootViewController: sRCountryPickerController)
		let viewController = (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController
		viewController?.present(nvController, animated: true, completion: nil)
	}
}

// MARK: - CountrySelectedDelegate
extension SRCountryPickerHelper: CountrySelectedDelegate {
	func SRcountrySelected(countrySelected country: Country) {
		self.countrySelected(country)
	}
}
