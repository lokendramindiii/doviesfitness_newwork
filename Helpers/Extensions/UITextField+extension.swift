//
//  UITextField+extension.swift
//
//  Copyright © 2017 hb. All rights reserved.
//

import Foundation

extension UITextField {
	
	func placehoderTextColor(color:UIColor) {
		self.attributedPlaceholder = NSAttributedString(string:self.placeholder!,
		                                                attributes:[NSAttributedStringKey.foregroundColor: color])
	}
	
	//Method which helps to add left padding to textfield.
	func leftViewPadding(imageName: String = "", isBorder: Bool = false, paddingWidth: CGFloat = 30) {
		let viewFrame = UIView(frame: CGRect(x: 0, y: 0, width: paddingWidth, height: self.frame.size.height))
		viewFrame.backgroundColor = UIColor.clear
		
		if imageName.isEmpty == false{
			let imageView = UIImageView(image: UIImage(named: imageName))
			imageView.frame = CGRect(x: 3, y: 0, width: paddingWidth, height: self.frame.size.height)
			imageView.contentMode = UIViewContentMode.center
			viewFrame.addSubview(imageView)
		}
		
		if isBorder == true{
			let lblLine = UILabel(frame: CGRect(x: 32, y: 5, width: 1, height: self.frame.size.height - 10))
			lblLine.backgroundColor = UIColor(red: 227.0/255.0, green: 227.0/255.0, blue: 227.0/255.0, alpha: 1.0)
			
			viewFrame.addSubview(lblLine)
			
			self.layer.borderWidth = 1.0
			self.layer.borderColor = UIColor(red: 227.0/255.0, green: 227.0/255.0, blue: 227.0/255.0, alpha: 1.0).cgColor
		}
		
		self.leftView = viewFrame
		self.leftViewMode = UITextFieldViewMode.always
	}
	
	func leftViewTextPadding(sName: String = ""){
		let viewFrame = UIView(frame: CGRect(x: 0, y: 0, width: 20 * CGRect.widthRatio, height: self.frame.size.height * CGRect.heightRatio))
		viewFrame.backgroundColor = UIColor.clear
		
		if sName.isEmpty == false{
			let imageView = UILabel()
			let yOfLlb:CGFloat = -1.0
			imageView.frame = CGRect(x: 0, y: yOfLlb, width: 20 * CGRect.widthRatio, height: self.frame.size.height * CGRect.heightRatio)
			imageView.text = sName
			imageView.backgroundColor = UIColor.clear
			imageView.textColor = UIColor.white
			imageView.textAlignment = NSTextAlignment.right
//			imageView.font = UIFont.init(fontRokkittRegularFontSize: 14.0)
			viewFrame.addSubview(imageView)
		}
		
		self.leftView = viewFrame
		self.leftViewMode = UITextFieldViewMode.always
	}
}
