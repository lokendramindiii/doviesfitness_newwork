//
//  UpgradeToPremiumViewController.swift
//  Dovies
//
//  Created by Neel Shah on 12/04/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

struct UpgradeToPremiumVC {
    
    static var structArrayModelUpgradePremium:ModelUpgradePremium?
}

class UpgradeToPremiumViewController: BaseViewController  {
    
    var arrModelUpgradePremium: ModelUpgradePremium!

    static let cellPadding : CGFloat = 5.0
    static let cellWidth : CGFloat  = (ScreenSize.width - UpgradeToPremiumViewController.cellPadding)/2
    let collectionCellHeight: CGFloat = cellWidth + 33
    static let tableElementsPadding : CGFloat = 55
    let tableCellHeight = cellWidth + tableElementsPadding
	@IBOutlet weak var IBtblPackageList: UITableView!
	var pullToRefreshCtrl:UIRefreshControl!
    var updateTableCells:(()->())?
    var selectedIndexPath : IndexPath!
    
    var onPurchaseSuccess:(()->())?

    var isPresent = false
    var isFromFavExercise = false

    
    @IBOutlet weak var activityIndicatorView : NVActivityIndicatorView!{
        didSet{
            activityIndicatorView.padding = 20
            activityIndicatorView.color = UIColor.lightGray
            activityIndicatorView.type = .circleStrokeSpin
        }
    }
    @IBOutlet weak var btnFooter : UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.barTintColor = UIColor.black

		self.navigationItem.title = "Upgrade to Premium".uppercased()
        if isPresent{
            if self.navigationController?.navigationBar.barTintColor == UIColor.white
            {
                self.addNavigationCloseButton()
            }
            else
            {
                self.addNavigationCloseButton()
                self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white,
                                                                                NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]
            }
        }
        else
        {
            if self.navigationController?.navigationBar.barTintColor == UIColor.white
            {
                self.addNavigationBackButton()
            }
            else
            {
                self.addNavigationWhiteBackButton()
                self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white,
                                                                                NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]
            }
        }
        
		self.callPackageListAPI()
        self.setUpViewNeeds()
    }
    
    func addNavigationCloseButton() {
        self.navigationItem.rightBarButtonItems = GlobalUtility.getNavigationButtonItems(target: self, selector: #selector(self.btnCloseTapped(sender:)), imageName: "ico_closeWhite")
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.black
    }
    
    func addNavigationCloseButtonWhite() {
        self.navigationItem.rightBarButtonItems = GlobalUtility.getNavigationButtonItems(target: self, selector: #selector(self.btnCloseTapped(sender:)), imageName: "ico_close")
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.black
    }

    @objc func btnCloseTapped(sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        activityIndicatorView.stopAnimating()
        GlobalUtility.hideActivityIndi(viewContView: self.view)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    func setUpViewNeeds(){
        IBtblPackageList.register(UINib(nibName: "UpgradeHorizontalTableCell", bundle: nil), forCellReuseIdentifier: "UpgradeHorizontalTableCell")
        IBtblPackageList.register(UINib(nibName: "UpgradeToPremiumCell", bundle: nil), forCellReuseIdentifier: "UpgradeToPremiumCell")
        
        //update cells after like or favourite status changed in details screen
        updateTableCells = { [weak self] in
            guard let weakSelf = self, let inedexPath = weakSelf.selectedIndexPath else{return}
            weakSelf.IBtblPackageList.reloadRows(at: [inedexPath], with: .none)
        }
        
        let window = UIApplication.shared.keyWindow
        if #available(iOS 11.0, *) {
            let topPadding = window!.safeAreaInsets.top
            self.IBtblPackageList.contentInset = UIEdgeInsets(top: self.navigationController!.navigationBar.frame.size.height + topPadding + 10 , left: 0, bottom: 0, right: 0)

        } else {
            self.IBtblPackageList.contentInset = UIEdgeInsets(top: self.navigationController!.navigationBar.frame.size.height + 10 , left: 0, bottom: 0, right: 0)
        }
        
      // for extra spacing
        if self.navigationController?.navigationBar.isTranslucent == false  &&  isFromFavExercise == false
        {
            self.IBtblPackageList.contentInset = UIEdgeInsets(top: 10 , left: 0, bottom: 0, right: 0)
        }
       

    }
    
	func callPackageListAPI(){
         self.activityIndicatorView.startAnimating()

		DoviesWSCalls.callGetPackageListAPI(dicParam: nil, onSuccess: { (success, arrModelPackage, msg) in
            self.activityIndicatorView.stopAnimating()
			if success{
                self.arrModelUpgradePremium = arrModelPackage!
                
             UpgradeToPremiumVC.structArrayModelUpgradePremium = arrModelPackage!
                
                self.IBtblPackageList.reloadData()
			}else{
                DoviesGlobalUtility.showSimpleAlert(viewController: self, message: msg, completion: nil)
			}
		}) { (error) in
            self.activityIndicatorView.stopAnimating()
		}
	}
	
    func callCompletePaymentAPI(purchaseDetails: [String:String], modelPackageId : String, encodedString : String ){
        
        
        let pucrhaseDetailAgain = purchaseDetails
        let modelPackageAgain = modelPackageId
        let encodedStringAgain = encodedString
        
        var tansactionDetail_Info = [String: Any]()
        tansactionDetail_Info["expires_date"] = purchaseDetails["expires_date"] ?? ""
        tansactionDetail_Info["expires_date_ms"] = purchaseDetails["expires_date_ms"] ?? ""
        tansactionDetail_Info["expires_date_pst"] = purchaseDetails["expires_date_pst"] ?? ""
        tansactionDetail_Info["is_in_intro_offer_period"] = purchaseDetails["is_in_intro_offer_period"] ?? ""
        tansactionDetail_Info["is_trial_period"] = purchaseDetails["is_trial_period"] ?? ""
        tansactionDetail_Info["original_purchase_date"] = purchaseDetails["original_purchase_date"] ?? ""
        tansactionDetail_Info["original_purchase_date_ms"] = purchaseDetails["original_purchase_date_ms"] ?? ""
        tansactionDetail_Info["original_purchase_date_pst"] = purchaseDetails["original_purchase_date_pst"] ?? ""
        tansactionDetail_Info["original_transaction_id"] = purchaseDetails["original_transaction_id"] ?? ""
        tansactionDetail_Info["product_id"] = purchaseDetails["product_id"] ?? ""
        tansactionDetail_Info["purchase_date"] = purchaseDetails["purchase_date"] ?? ""
        tansactionDetail_Info["purchase_date_ms"] = purchaseDetails["purchase_date_ms"] ?? ""
        tansactionDetail_Info["purchase_date_pst"] = purchaseDetails["purchase_date_pst"] ?? ""
        tansactionDetail_Info["quantity"] = purchaseDetails["quantity"] ?? ""
        tansactionDetail_Info["transaction_id"] = purchaseDetails["transaction_id"] ?? ""
        tansactionDetail_Info["web_order_line_item_id"] = purchaseDetails["web_order_line_item_id"] ?? ""
        tansactionDetail_Info["latest_receipt"] = encodedString
       
      
        let dataTransactionDetail = try! JSONSerialization.data(withJSONObject:tansactionDetail_Info, options: [ ])

        let jsonStringTransactionDetail = String(data: dataTransactionDetail, encoding: .utf8)
        
	    var tansactionDetailsDict = [String: Any]()
        tansactionDetailsDict[WsParam.productId] =  purchaseDetails["product_id"]
        tansactionDetailsDict[WsParam.quantity] = purchaseDetails["quantity"]
        tansactionDetailsDict[WsParam.encodedKey] = encodedString
      

        if let tID = purchaseDetails["transaction_id"] {
            tansactionDetailsDict[WsParam.transactionId] = tID
        }
        let data = try! JSONSerialization.data(withJSONObject:tansactionDetailsDict, options: [ ])
        guard let cUser = DoviesGlobalUtility.currentUser, let jsonString = String(data: data, encoding: .utf8)
            else{
                return
        }
        
        var dic = [String: Any]()
        dic["package_id"] = modelPackageId
        dic[WsParam.authCustomerId] =  cUser.customerAuthToken
        
        if encodedString != ""
        {
          dic["transaction_detail"] = jsonString
          dic["parsed_receipt_detail"] = jsonStringTransactionDetail
        }
        else
        {
            dic["transaction_detail"] = ""
            dic["parsed_receipt_detail"] = ""
        }
        
		DoviesWSCalls.callCompletePaymentAPI(dicParam: dic, onSuccess: { (success, msg) in
            GlobalUtility.hideActivityIndi(viewContView: self.view)
            if let sideMenu = self.sideMenuController?.sideViewController as? SideMenuViewController{
                sideMenu.setSubscriptionStatus()
            }
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: msg, completion: {
                
                let controller:UIViewController? = UIApplication.shared.keyWindow?.rootViewController
                
                if let present =  controller?.presentedViewController
                {
                    self.dismiss(animated: true, completion: nil)
                    print("dismisviewcontroller")
                }
                else
                {
                self.navigationController?.popViewController(animated: true)
                    print("Popviewcontroller")
                }
                
                self.onPurchaseSuccess?()

            })
		}) { (error) in
			GlobalUtility.hideActivityIndi(viewContView: self.view)
            
            self.callCompletePaymentAPI(purchaseDetails: pucrhaseDetailAgain, modelPackageId: modelPackageAgain, encodedString: encodedStringAgain)
		}
	}

    func showAlert(_ alert: UIAlertController) {
		guard self.presentedViewController != nil else {
			self.present(alert, animated: true, completion: nil)
			return
		}
	}
	
    func payForPackage(packageId: String, localPackage : ModelGetAllPackage){
        
        if packageId.isEmpty {
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyPackageId)
            return
        }
        
        GlobalUtility.showActivityIndi(viewContView: self.view)

        PurchaseHelper.payForPackage(packageId: packageId ,packageMasterId: localPackage.packageMasterId, onSuccss: {[weak self] (purchaseDetials, encodedString , receipt_Data) in
            
            UserDefaults.standard.set(localPackage.packageMasterId, forKey: UserDefaultsKey.packageMasterId)
       
            UserDefaults.standard.set(DoviesGlobalUtility.currentUser?.customerId, forKey: UserDefaultsKey.customerIdbyPayment)
            
            guard let weakSelf = self else{return}
            
        // uplod new build on app store with recipt and transaction info
            weakSelf.upload(receipt: receipt_Data, packageMaster_Id: localPackage.packageMasterId)
            
        //    weakSelf.callCompletePaymentAPI(purchaseDetails: purchaseDetials, modelPackage: localPackage, encodedString: encodedString)
            
        }) { (errorAlert) in
            GlobalUtility.hideActivityIndi(viewContView: self.view)
            self.showAlert(errorAlert)
        }
	}
    
    @IBAction func btnTermsAndConditionsTapped(_ sender : UIButton){
        //redirect to privacy policy
        let staticPageVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "StaticPagesViewController")as! StaticPagesViewController
        staticPageVC.pageType = .termsAndConditions
        self.navigationController?.pushViewController(staticPageVC, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension UpgradeToPremiumViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if arrModelUpgradePremium != nil {
            if section == 0{
                
                return 1
            }
            return arrModelUpgradePremium.getStaticContent.count
        }
        return 0
	}
    
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		if indexPath.section == 0{
			guard let cell = tableView.dequeueReusableCell(withIdentifier: "UpgradeHorizontalTableCell") as? UpgradeHorizontalTableCell else{return UITableViewCell()}
			cell.recommendedCollection.register(UINib(nibName: "UpgradeToPremiumCollCell", bundle: nil), forCellWithReuseIdentifier: "UpgradeToPremiumCollCell")
			cell.recommendedCollection.dataSource = self
			cell.recommendedCollection.delegate = self
			cell.recommendedCollection.reloadData()
			return cell
		}
		else{
			guard let cell = tableView.dequeueReusableCell(withIdentifier: "UpgradeToPremiumCell") as? UpgradeToPremiumCell else {return UITableViewCell()}
           cell.delegate = self
           cell.setData(getStaticContent: arrModelUpgradePremium.getStaticContent[indexPath.row])
            
			return cell
		}
	}
}



extension UpgradeToPremiumViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 0{
            //return (ScreenSize.width - 20) / 3.0
            //return (self.view.frame.size.width+50) / 3.0
            
            return 142
        }
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
}

extension UpgradeToPremiumViewController:UICollectionViewDataSource{
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
		return arrModelUpgradePremium.getAllPackages.count
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "UpgradeToPremiumCollCell", for: indexPath) as? UpgradeToPremiumCollCell else{return UICollectionViewCell()}
        //cell.layer.cornerRadius = 1
       cell.setData(PackageList: arrModelUpgradePremium.getAllPackages[indexPath.row])
        
        cell.backgroundColor = hexStringToUIColor(hex: arrModelUpgradePremium.getAllPackages[indexPath.row].colorcode)
        
//        if indexPath.row == 0 {
//          //  cell.backgroundColor = UIColor.colorWithRGB(r: 185.0, g: 57.0, b: 27.0, alpha: 1.0)
//         
//        }
//        else if indexPath.row == 1 {
//            cell.backgroundColor = UIColor.colorWithRGB(r: 85.0, g: 137.0, b: 125.0, alpha: 1.0)
//        }
//        else if indexPath.row == 2 {
//            cell.backgroundColor = UIColor.colorWithRGB(r: 94.0, g: 105.0, b: 88.0, alpha: 1.0)
//        }
		return cell
	}
}

func hexStringToUIColor (hex:String) -> UIColor {
    var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
    
    if (cString.hasPrefix("#")) {
        cString.remove(at: cString.startIndex)
    }
    
    if ((cString.count) != 6) {
        return UIColor.gray
    }
    
    var rgbValue:UInt32 = 0
    Scanner(string: cString).scanHexInt32(&rgbValue)
    
    return UIColor(
        red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
        green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
        blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
        alpha: CGFloat(1.0)
    )
}

// MARK: UPLOAD recipt from server  AND Complete payment api call
extension UpgradeToPremiumViewController
{
    
    func upload(receipt data: Data , packageMaster_Id : String) {
        
        let receiptdataBase64 = data.base64EncodedString()
        let body = [
            "receipt-data":receiptdataBase64 ,
            "password": WsParam.itunesPassword
        ]
        let bodyData = try! JSONSerialization.data(withJSONObject: body, options: [])
        
        let url = URL(string:  WsUrl.verifyReceiptUrl)!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = bodyData
        
        let task = URLSession.shared.dataTask(with: request) { (responseData, response, error) in
            
            DispatchQueue.main.async {
                
                if let responseData = responseData {
                    let json = try! JSONSerialization.jsonObject(with: responseData, options: []) as! Dictionary<String, Any>
                    
                    if let receiptInfo: NSArray = json["latest_receipt_info"] as? NSArray {
                      //  print("latestReceiptInfoList----\(receiptInfo)")
                        //LatestRecipt
                        let lastReceipt = receiptInfo.lastObject as! NSDictionary
                        
                        
                    self.callCompletePaymentAPI(purchaseDetails: lastReceipt as! [String : String], modelPackageId: packageMaster_Id , encodedString: receiptdataBase64)
                    }
                }
            }
        }
        task.resume()
    }
    
    
}


extension UpgradeToPremiumViewController : UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if arrModelUpgradePremium.getAllPackages.count > indexPath.item{
            payForPackage(packageId: arrModelUpgradePremium.getAllPackages[indexPath.row].iosPackageId, localPackage: arrModelUpgradePremium.getAllPackages[indexPath.row])
        }
        
    }
}

extension UpgradeToPremiumViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var collectionViewSize = collectionView.frame.size
        collectionViewSize.width = (collectionViewSize.width - 20)/3.0

        //collectionViewSize.height = collectionViewSize.height+20
        return collectionViewSize
    }
}

extension UpgradeToPremiumViewController: UpgradeToPremium_RedirectPrivacyPolicy {
    func RedirectToPrivacyPolicy(_ PrivacyPolicy: String) {
        
        if PrivacyPolicy == "Terms And Conditions" {
            let staticPageVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "StaticPagesViewController")as! StaticPagesViewController
            staticPageVC.pageType = .termsAndConditions
            let navi = UINavigationController(rootViewController: staticPageVC)
            self.present(navi, animated: true, completion: nil)
            
        } else  if  PrivacyPolicy == "Privacy Policy"{
            let staticPageVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "StaticPagesViewController")as! StaticPagesViewController
            staticPageVC.pageType = .privacyPolicy
            let navi = UINavigationController(rootViewController: staticPageVC)
            self.present(navi, animated: true, completion: nil)
        }
    }
}

