//
//  CreateWorkoutVC.swift
//  Dovies
//
//  Created by hb on 23/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//
import UIKit

let cell_Tag = 100000
let timeTxtfld_Tag = 10000
let repsTxtfld_Tag = 1000
let totalTimeTxtfld_Tag = 100
let breakTimeTxtfld_Tag = 10


class CreateWorkoutVC: BaseViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UITextViewDelegate , UIScrollViewDelegate {

    
    var Level : String = "Level"
    var Str_SlectLevel : String = ""

    var isEditSelected : Bool = false
    var isCreatedByYou: Bool = false
    var isCreatedMyworkoutLog: Bool = false
    var isCreatedWorkoutAdmin: Bool = false

    
    var timeMinute : String = "Min"
    var timeSecond : String = "Sec"
    var parentExeID : String!
    var  arrLevel: [FilterGroup]!
    var btnSelected : UIButton = UIButton()
    var btnTimeSelected = true
    static var arrExercise = [Any]()
    static var arrTempExercises = [ExerciseDetails]()
    var exeTime : String = ""
    var exeReps : String = ""
    var exeBreakTime : String = ""
    @IBOutlet var workoutTblView: UITableView!
    var defaultsetting: UserDefaults!
    var headerView :AddWorkoutHeaderView!
    var dragger: TableViewDragger!
    var imageUpdated = false
    let validator = Validator()
    var arrLevelListing = [FilterDataModel]()
    var goodForListing:GoodForListingVC?
    var equipmentList : EquipmentListVC?
    var  arrFilterData: [FilterGroup] = DoviesGlobalUtility.arrFilterData
    var modelWorkoutDetailData: ModelWorkoutDetail!
    var ExerciseDetailsData : ExerciseDetails?
    var arExerciseDetailsData : [ModelWorkoutExerciseList]?
    var arrSelectedGoodfor = [String]()
    var arrGoodforId = [String]()
    var arrSelectedequips = [String]()
    var arrEquipsId = [String]()
    var arrSelectedEquipsID = [String]()
    var arrSelectedEquipsList = [String]()
    
    var isForEditWork:(() -> ())?
    var updateUiFromCreateWorkout :((_ isupdate:Bool) ->())?

	var isFromMyProfileCreate = false

// Video section
    var ValidationRow = -1
    var IsVideoOpen = ""

    var selectedRow = -1
    var visibleCellsDuringAnimation = [UITableViewCell]()
    var playerHeight = 179.5 * CGRect.widthRatio
    let cellVideoHeight:CGFloat = 120 + (185 * CGRect.widthRatio)
    
    
    var strGuestId = String()
    var isNewworkout = false

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        defaultsetting = UserDefaults.standard
        
        if  defaultsetting.object(forKey: "allTimerExcercises") != nil || defaultsetting.object(forKey: "numberOfRepas") != nil || defaultsetting.object(forKey: "timeFinishEachExcercise") != nil
        {
        }
        else{
            defaultsetting.set("00:30", forKey: "allTimerExcercises")
            defaultsetting.set("10", forKey: "numberOfRepas")
            defaultsetting.set("00:30", forKey: "timeFinishEachExcercise")
        }

        
        if self.navigationController?.navigationBar.barTintColor == UIColor.white
        {
        }
        else
        {
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white,
                                                                            NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]
        }
         DoviesGlobalUtility.setFilterData()
        
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: Notification.Name.UIApplicationWillResignActive, object: nil)
        
        self.navigationItem.title = "Create workout".uppercased()
        self.navigationItem.leftBarButtonItem = GlobalUtility.getNavigationButtonItemWith(target: self, selector: #selector(self.cancelButtonTapped), title: "Cancel")
        self.setDetail()
        self.getArrayDetails()
        self.workoutTblView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 5, right: 0)
        self.mapExerciseData()
    }
    
    @objc func appMovedToBackground() {
        if selectedRow > -1{
            let idxPath : IndexPath = IndexPath(row: selectedRow, section: 0)
            closeVideoPlayForCell(indexPath: idxPath)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        if selectedRow > -1{
            let selectedCell = workoutTblView.cellForRow(at: IndexPath(row: selectedRow, section: 0)) as! CreateWorkoutCell
            selectedCell.stopVideo()
        }
    }
    
    deinit{
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIApplicationWillResignActive, object: nil)
        if selectedRow > -1{
            let selectedCell = workoutTblView.cellForRow(at: IndexPath(row: selectedRow, section: 0)) as! CreateWorkoutCell
            selectedCell.removePlayerFromView()
        }
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.headerView.btnGoodFor.addTarget(self, action: #selector(self.didTapGoodforButton), for: .touchUpInside)
        print("callExerciseListingAPI\(CreateWorkoutVC.arrExercise)")
        workoutTblView.reloadData()
        workoutTblView.layoutIfNeeded()

    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        sizeHeaderToFit()
        if IS_IPHONE_X
        {
            self.workoutTblView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 40, right: 0)
        }
        else{
            self.workoutTblView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 75, right: 0)
        }
    }
    
    @objc func cancelButtonTapped(_ sender: UIButton){
		self.setCancelButtonValidation()
    }
    
    func mapExerciseData(){
        if modelWorkoutDetailData != nil {
            let objModelWorkoutExercise: [ModelWorkoutExerciseList] = modelWorkoutDetailData.workoutExerciseList
            if objModelWorkoutExercise.count > 0 {
                for i in 0...objModelWorkoutExercise.count - 1 {
                    let objExerInfo = ExerciseDetails(fromDictionary: [String : Any]())
                    objExerInfo.exerciseVideo = objModelWorkoutExercise[i].workoutExerciseVideo
                    objExerInfo.exerciseAccessLevel = objModelWorkoutExercise[i].workoutAccessLevel
                    objExerInfo.exerciseId = objModelWorkoutExercise[i].workoutExercisesId
                    objExerInfo.exerciseImage = objModelWorkoutExercise[i].workoutExerciseImage
                    objExerInfo.exerciseName = objModelWorkoutExercise[i].workoutExerciseName
                    objExerInfo.breakTimeValue = String(objModelWorkoutExercise[i].workoutExerciseBreakTime.dropFirst(3))
                    objExerInfo.timeValue = String(objModelWorkoutExercise[i].workoutExercisesTime.dropFirst(3))
					objExerInfo.excerciseOption = objModelWorkoutExercise[i].workoutExerciseType
					objExerInfo.repsValue = objModelWorkoutExercise[i].workoutRepeatText
					objExerInfo.totalTimeValue = String(objModelWorkoutExercise[i].workoutExercisesTime.dropFirst(3))
                    CreateWorkoutVC.arrExercise.append(objExerInfo)
                }
                
            }
            print(CreateWorkoutVC.arrExercise)
        }
        if isEditSelected == true {
            
            arrGoodforId = modelWorkoutDetailData.workoutGoodForIds.components(separatedBy: ",")
            arrSelectedGoodfor = modelWorkoutDetailData.workoutGoodFor.components(separatedBy:" | ")
            arrEquipsId = modelWorkoutDetailData.workoutEquipmentIds.components(separatedBy: ",")
            arrSelectedequips = modelWorkoutDetailData.workoutEquipment.components(separatedBy:" | ")
            
            self.strGuestId = modelWorkoutDetailData.creatorId

        }
    }

    func sizeHeaderToFit(){
        let headerView = workoutTblView.tableHeaderView!
        
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        
        let height = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
        var frame = headerView.frame
        frame.size.height = height
        headerView.frame = frame
        
        workoutTblView.tableHeaderView = headerView
    }
    
    func setDetail() {
        headerView = MyClass.instanceFromNib() as! AddWorkoutHeaderView
       // headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 600)
        
        headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 523 )

        headerView.imgAddPhoto.layer.borderWidth = 1.0
        headerView.imgAddPhoto.layer.borderColor = UIColor(red:231/255, green:231/255, blue:231/255, alpha: 1).cgColor
        
        headerView.btn_allTimerExcercise.layer.borderWidth = 1.0
        headerView.btn_allTimerExcercise.layer.borderColor =  UIColor(red:231/255, green:231/255, blue:231/255, alpha: 1).cgColor
        headerView.btn_allTimerExcercise.setTitle(defaultsetting.string(forKey: "allTimerExcercises") ?? "", for: .normal)
        
        headerView.btn_TimeFinish_reps.layer.borderWidth = 1.0
        headerView.btn_TimeFinish_reps.layer.borderColor =  UIColor(red:231/255, green:231/255, blue:231/255, alpha: 1).cgColor
        headerView.btn_TimeFinish_reps.setTitle(defaultsetting.string(forKey: "timeFinishEachExcercise") ?? "", for: .normal)

        headerView.btnNoOfReps.layer.borderWidth = 1.0
        headerView.btnNoOfReps.layer.borderColor =  UIColor(red:231/255, green:231/255, blue:231/255, alpha: 1).cgColor
        headerView.btnNoOfReps.setTitle(defaultsetting.string(forKey: "numberOfRepas") ?? "", for: .normal)
        
        workoutTblView.tableHeaderView = headerView
        headerView.isUserInteractionEnabled = true
        headerView.btnClickPhoto.layer.cornerRadius = 20
        headerView.btnClickPhoto.clipsToBounds = true
        headerView.txtView.delegate = self
        headerView.txtView.text = "Overview"
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 1.5
        
        headerView.txtView.typingAttributes = [NSAttributedStringKey.paragraphStyle.rawValue : style]
        headerView.txtView.textColor = UIColor.init(r: 199.0, g: 199.0, b: 199.0, alpha: 1.0)
        headerView.btnLevel.addTarget(self, action: #selector(self.didTapLevelButton), for: .touchUpInside)
        headerView.btnGoodFor.addTarget(self, action: #selector(self.didTapGoodforButton), for: .touchUpInside)
        headerView.btnEquipments.addTarget(self, action: #selector(self.didTapEquipmentsButton), for: .touchUpInside)
        headerView.btnLevelImage.addTarget(self, action: #selector(self.didTapLevelButton), for: .touchUpInside)
        headerView.btnGoodForImage.addTarget(self, action: #selector(self.didTapGoodforButton), for: .touchUpInside)
        headerView.btnEquipImage.addTarget(self, action: #selector(self.didTapEquipmentsButton), for: .touchUpInside)
        
        headerView.btnDefaultViewHideShow.addTarget(self, action: #selector(self.didTapDefaultViewHideshow), for: .touchUpInside)
        headerView.btn_TimeFinish_reps.addTarget(self, action: #selector(self.didTapTimeSelection_Button(sender:)), for: .touchUpInside)
        headerView.btn_allTimerExcercise.addTarget(self, action: #selector(self.didTapTimeSelection_Button(sender:)), for: .touchUpInside)
        headerView.btnNoOfReps.addTarget(self, action: #selector(self.didTapNoofRepsSelection_Button(sender:)), for: .touchUpInside)
        
        headerView.txtfldTitle.attributedPlaceholder = fun_attributedstring_forcolor(main_string: "Workout Name *", string_to_color: "*")
     
        headerView.btnLevel.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "Level ", String2: "*"), for: .normal)
        
        headerView.btnGoodFor.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "Good For ", String2: "*"), for: .normal)
     headerView.btnEquipments.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "Equipments ", String2: "*"), for: .normal)

        if isEditSelected == true{
            headerView.btnClickPhoto.isHidden = true
         //   headerView.imgAddPhoto.sd_setImage(with:  URL(string: modelWorkoutDetailData.workoutImage), placeholderImage: AppInfo.placeholderImage)
            
            headerView.imgAddPhoto.sd_setImage(with:  URL(string: modelWorkoutDetailData.workoutImage), placeholderImage: nil)

            
            headerView.img_StarPhoto.image = nil
            headerView.txtfldTitle.text = modelWorkoutDetailData.workoutName
            headerView.lblLevel.text = modelWorkoutDetailData.workoutLevel
            
            if var textlevel = headerView.lblLevel.text, !textlevel.isEmpty{
                if textlevel == "All"
                {
                    textlevel = "All Levels"
                }
                headerView.lblLevel.attributedText = fun_attributedstringNextLine1(StringTitle: " Level", SelectString: textlevel)
            }
            
            Str_SlectLevel = modelWorkoutDetailData.workoutLevel
            
            Level = modelWorkoutDetailData.workoutLevel
             self.headerView.btnLevel.setImage(nil, for: .normal)
            headerView.txtView.text = modelWorkoutDetailData.workoutDescription
            headerView.txtView.textColor = UIColor.colorWithRGB(r: 40.0, g: 40.0, b: 40.0)
        headerView.btnLevel.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "", String2: ""), for: .normal)

        headerView.btnGoodFor.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "", String2: ""), for: .normal)
       headerView.btnEquipments.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "", String2: ""), for: .normal)

            headerView.lblGoodFor.text = modelWorkoutDetailData.workoutGoodFor
            
            if let text = headerView.lblGoodFor.text, !text.isEmpty{
                
                headerView.lblGoodFor.attributedText = fun_attributedstringNextLine1(StringTitle: " Good for", SelectString: text)
            }
            headerView.lblEquipments.text = modelWorkoutDetailData.workoutEquipment
            
            if let text =  headerView.lblEquipments.text, !text.isEmpty{
                
                headerView.lblEquipments.attributedText = fun_attributedstringNextLine1(StringTitle: " Equipments", SelectString: text)
            }
        }
        workoutTblView.register(UINib(nibName: "CreateWorkoutCell", bundle: nil), forCellReuseIdentifier: "CreateWorkoutCell")
        self.workoutTblView.tableFooterView = UIView()
        self.addRightNavigationButton()
        if #available(iOS 11.0, *) {
            self.workoutTblView.dragDelegate = true as? UITableViewDragDelegate
            self.workoutTblView.dragInteractionEnabled = true
            
        } else {
            
        }
        dragger = TableViewDragger(tableView: workoutTblView)
        dragger.dataSource = self
        dragger.delegate = self
        dragger.alphaForCell = 1.0
        dragger.zoomScaleForCell = 1.0
        headerView.btnProfile.addTarget(self, action: #selector(self.didAddworkoutPhoto), for: .touchUpInside)
        headerView.btnClickPhoto.addTarget(self, action: #selector(self.didAddworkoutPhoto), for: .touchUpInside)

        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        
	}
    
    func getArrayDetails(){
        
        let arrValue = arrFilterData.filter {$0.groupName == "Level"}
        if arrValue.count > 0{
            self.arrLevelListing = arrValue[0].list
        }
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.init(r: 199.0, g: 199.0, b: 199.0, alpha: 1.0) {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Overview"
            textView.textColor = UIColor.init(r: 199.0, g: 199.0, b: 199.0, alpha: 1.0)
        }
    }
	
	func setCancelButtonValidation(){
		if let text = headerView.txtfldTitle.text?.removeWhiteSpace(), text.count > 0 || self.headerView.imgAddPhoto.image != nil || headerView.lblLevel.text?.count != 0 || headerView.lblGoodFor.text?.count != 0 || headerView.lblEquipments.text?.count != 0 || CreateWorkoutVC.arrExercise.count != 0
		{
			UIAlertController.showAlert(in: (APP_DELEGATE.window?.rootViewController)!, withTitle: "", message: AlertMessages.cancleCreateWorkout, cancelButtonTitle: "No", destructiveButtonTitle: "Yes", otherButtonTitles: nil, tap: { (alertCont, action, index) in
			 	if index == 1{
					self.navigationController?.popViewController(animated: true)
					CreateWorkoutVC.arrExercise = [Any]()
				}
			})
		}
		else
		{
            self.updateUiFromCreateWorkout?(false)
			self.navigationController?.popViewController(animated: true)
			CreateWorkoutVC.arrExercise = [Any]()
		}
	}
    
    func setFormValidators()-> Bool{
        if let text = headerView.txtfldTitle.text, text.isEmpty{
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyWorkoutTitle, completion: nil)
            return false
        }
        else if (self.headerView.imgAddPhoto.image == nil){
            self.headerView.img_StarPhoto.image = UIImage(named: "img_star")
            
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyWorkoutPhoto, completion: nil)
            return false
        }
            
        else if let text = headerView.lblLevel.text, text.isEmpty || Str_SlectLevel == "" {
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyWorkoutLevel, completion: nil)
            return false
        }
        else if let text = headerView.lblGoodFor.text, text.isEmpty{
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyWorkoutGoodFor, completion: nil)
            return false
        }
        else if let text = headerView.lblEquipments.text, text.isEmpty{
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyWorkoutEquipments, completion: nil)
            return false
        }
        else if CreateWorkoutVC.arrExercise.count == 0 {
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyWorkoutExercise, completion: nil)
            return false
        }
        if (CreateWorkoutVC.arrExercise.count > 0)
        {
            var iCanProceed:Bool = true
            if CreateWorkoutVC.arrExercise.count > 0{
                for i in 0...CreateWorkoutVC.arrExercise.count-1{
                    let model = CreateWorkoutVC.arrExercise[i] as? ExerciseDetails
                    
                    if model?.excerciseOption == "Time"
                    {
                        if model?.breakTimeValue == "" || model?.breakTimeValue == "00:00"
                        {
                            if model?.timeValue == "" || model?.timeValue == "00:00"{
                                ValidationRow = i
                                self.workoutTblView.reloadData()
                                DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyTime, completion: nil)
                                iCanProceed = false
                                break
                            }
                        }
                            // loks add else part
                        else
                        {
                            if model?.timeValue == "" || model?.timeValue == "00:00"{
                                ValidationRow = i
                                DispatchQueue.main.async {
                                    self.workoutTblView.reloadData()
                                }
                                
                                DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyTime, completion: nil)
                                iCanProceed = false
                                break
                            }
                        }
                    }
                    else
                    {
                        if model?.breakTimeValue == "" || model?.breakTimeValue == "00:00"
                        {
                            if model?.totalTimeValue == "" || model?.totalTimeValue == "00:00"
                            {
                                ValidationRow = i
                                DispatchQueue.main.async {
                                    self.workoutTblView.reloadData()
                                }
                                DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyTotalTime, completion: nil)
                                iCanProceed = false
                                break
                            }
                            if model?.repsValue == "" || model?.repsValue == "00"
                            {
                                ValidationRow = i
                                DispatchQueue.main.async {
                                    self.workoutTblView.reloadData()
                                }
                                DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyReps, completion: nil)
                                iCanProceed = false
                                break
                            }
                        }
                            //       loks add else part
                        else
                        {
                            if model?.totalTimeValue == "" || model?.totalTimeValue == "00:00"
                            {
                                ValidationRow = i
                                DispatchQueue.main.async {
                                    self.workoutTblView.reloadData()
                                }
                                DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyTotalTime, completion: nil)
                                iCanProceed = false
                                break
                            }
                            if model?.repsValue == "" || model?.repsValue == "00"
                            {
                                ValidationRow = i
                                DispatchQueue.main.async {
                                    self.workoutTblView.reloadData()
                                }
                                DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyReps, completion: nil)
                                iCanProceed = false
                                break
                            }
                        }
                    }
                }
            }
            return iCanProceed
        } else {
            return false
        }
    }
    
    
    func addRightNavigationButton(){
        let saveButton = UIButton(type: .custom)
        saveButton.setTitle("DONE", for: .normal)
        saveButton.titleLabel?.font  = UIFont.tamilBold(size: 16)
        saveButton.setTitleColor(#colorLiteral(red: 0.9882352941, green: 0.3215686275, blue: 0.1607843137, alpha: 1), for: .normal)
       // saveButton.setImage(UIImage(named:"btn_save"), for: .normal)
        saveButton.addTarget(self, action: #selector(self.didSaveButton), for: .touchUpInside)
        saveButton.frame = CGRect(x: 0, y: 0, width: 30, height: 25)
        let saveBarItem = UIBarButtonItem(customView: saveButton)
        navigationItem.rightBarButtonItems = [saveBarItem]
    }
    
    @objc func didSaveButton(sender: AnyObject){
        
//        //            Request to call an API begins here...
//        if self.setFormValidators() == true{
//            self.createWorkoutWSCall()
//        }
//        if self.setFormValidators() == true{
//            self.createWorkoutWSCall()
//        }
        
        self.view.endEditing(true)

        if isCreatedMyworkoutLog == true
        {
            if self.setFormValidators() == true{
                self.isEditSelected = false
                self.isCreatedByYou = true
                self.isFromMyProfileCreate = true
                self.createWorkoutWSCall()
            }
            return
        }
        
        if self.isEditSelected == true
        {
            if self.isCreatedWorkoutAdmin == true
            {
                if self.setFormValidators() == true{
                  self.createWorkoutWSCall()
                }
            }
            else
            {
                
                // Note:- self.strGuestId == DoviesGlobalUtility.currentUser?.customerId-  fix by time issue
                
                if  self.strGuestId == DoviesGlobalUtility.currentUser?.customerId{
                   
                    if self.setFormValidators() == true
                    {
                        let arrData:[alertActionData] =
                            [alertActionData(title: "Save as new workout", imageName: "ico_workout_tab_NewBlack", highlighedImageName : "ico_workout_tab_NewBlack"),
                             alertActionData(title: "Overwrite current workout", imageName: "btn_edit_s21a", highlighedImageName : "btn_edit_s21a")
                        ]
                        DoviesGlobalUtility.showDefaultActionSheet(on:self,actionData: arrData,cancelTitle: "CANCEL") { (clickedIndex:Int, isCancel:Bool) in
                            if clickedIndex == 0{
                                
                                self.isEditSelected = false
                                self.isCreatedByYou = true
                                self.isFromMyProfileCreate = true
                                if self.setFormValidators() == true{
                                    self.createWorkoutWSCall()
                                }
                            }
                            else{
                                self.isEditSelected = true
                                if self.setFormValidators() == true{
                                    self.createWorkoutWSCall()
                                }
                            }
                        }
                    }
                }
                else
                {
                    self.isNewworkout = true
                    if self.setFormValidators() == true{
                        self.createWorkoutWSCall()
                    }
                }
            }
        }
        else
        {
            if self.setFormValidators() == true{
                self.createWorkoutWSCall()
            }
        }
    }
    
    @objc func didAddworkoutPhoto(sender: AnyObject){
        self.UpdateUserButtonTapped()
        
    }
    
    @objc func UpdateUserButtonTapped ()
    {
        HBImagePicker.sharedInstance().open(with: MediaTypeImage, actionSheetTitle: "Choose Image", cancelButtonTitle: "Cancel", cameraButtonTitle: "Use Camera", galleryButtonTitle: "Choose existing", canEdit: true) {[weak self] (success, dict) in
            guard let weakself = self else { return }
            if success {
                if let img = dict?["image"] as? UIImage {
                    weakself.imageUpdated = true
                    weakself.headerView.imgAddPhoto.image = HBImagePicker.sharedInstance().compressImage(img)
                    weakself.headerView.btnClickPhoto.isHidden = true
                    weakself.headerView.img_StarPhoto.image = nil
                }
            }
        }
    }
  
    @objc func didTapLevelButton(){
        self.view.endEditing(true)
        let arrList = arrLevelListing.filter {!$0.gmGoogforMasterId.isEmpty}
        let mcPicker = McPicker(data: [(arrList.map{$0.gmDisplayName})], selectedTitle: [Level])
        mcPicker.show { [weak self] (selections: [Int : String]) -> Void in
            guard let weakSelf = self else{return}
            if let level = selections[0]{
                let arrList = weakSelf.arrLevelListing.filter {$0.gmDisplayName == level}
                
            weakSelf.headerView.btnLevel.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "", String2: ""), for: .normal)
                weakSelf.headerView.lblLevel.text = level
                
                if level == "All Levels" || level == "All Level"
                {
                    self?.Str_SlectLevel = "All"
                }
                else
                {
                    self?.Str_SlectLevel  = level
                }
                
                weakSelf.headerView.lblLevel.attributedText = fun_attributedstringNextLine1(StringTitle: " Level", SelectString: level)
                
                self?.headerView.btnLevel.setImage(nil, for: .normal)

                if arrList.count > 0{
                    weakSelf.Level = arrList[0].gmGoogforMasterId
                }
            }
        }
    }
    
    @objc func didTapExerciseButton() {
        self.performSegue(withIdentifier: "createWorkoutToWorkoutExe", sender: nil)
    }
    
    @IBAction func ExerciseButton()
    {
        if selectedRow > -1{
            let idxPath : IndexPath = IndexPath(row: selectedRow, section: 0)
            self.closeVideoPlayForCell(indexPath:idxPath )
        }
        self.performSegue(withIdentifier: "createWorkoutToWorkoutExe", sender: nil)
    }
    @IBAction func WorkoutInfo()
    {
        let detailsVC = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "WorkOutExerciseDescription_Detail")as! WorkOutExerciseDescription_Detail
        detailsVC.Str_CreateWorkoutFlage = "CreateWorkout"
        detailsVC.modalTransitionStyle = .crossDissolve
        detailsVC.modalPresentationStyle = .overCurrentContext
        self.present(detailsVC, animated: true, completion: nil)
    }
    
    
    
    @IBAction func WorkoutTime()
    {
        
        if CreateWorkoutVC.arrExercise.count == 0
        {
            let totalworkoutTime = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "TotalWorkouttimeVC")as! TotalWorkouttimeVC
            totalworkoutTime.TotalWorkoutMinutes = ""
            totalworkoutTime.modalTransitionStyle = .crossDissolve
            totalworkoutTime.modalPresentationStyle = .overCurrentContext
            self.present(totalworkoutTime, animated: true, completion: nil)
            return
        }
        else
        {
            var TotalMinutesofSecond:Int = 0
            var Totalsecond:Int = 0
            
            for i in 0...CreateWorkoutVC.arrExercise.count-1{
                if let model = CreateWorkoutVC.arrExercise[i] as? ExerciseDetails {
                    
                    var Timevalue:String = "00:00"
                    var TotalTimeValue:String = "00:00"
                    
                    if model.excerciseOption == "Time"{
                        Timevalue  =  model.timeValue
                        let time = Timevalue.components(separatedBy: ":")
                        let min:Int    = Int(time[0]) ?? 0
                        let sec:Int =  Int(time[1]) ?? 0
                        TotalMinutesofSecond += (min * 60)
                        Totalsecond += sec
                    }
                    else {
                        TotalTimeValue = model.totalTimeValue
                        let totalTimeArr = TotalTimeValue.components(separatedBy: ":")
                        let min:Int    = Int(totalTimeArr[0]) ?? 0
                        let sec:Int =  Int(totalTimeArr[1]) ?? 0
                        TotalMinutesofSecond += (min * 60)
                        Totalsecond += sec
                    }
                    
                    
                    var  breakTime:String = "00:00"
                    if model.breakTimeValue == "00:00"{
                        breakTime = "00:00"
                    }else if model.breakTimeValue == ""{
                        breakTime = "00:00"
                    }else{
                        breakTime =   model.breakTimeValue
                    }
                    let  breakTimeArr = breakTime.components(separatedBy: ":")
                    let bmin:Int    = Int(breakTimeArr[0]) ?? 0
                    let bsec:Int =  Int(breakTimeArr[1]) ?? 0
                    TotalMinutesofSecond += (bmin * 60)
                    Totalsecond += bsec
                }
            }
            
            var SendMinutes = ""
            var SendSecond = ""
            
            TotalMinutesofSecond = TotalMinutesofSecond + Totalsecond
            
            //  let minutes = Int(TotalMinutesofSecond) / 60 % 60
            
            let minutesa = Int(TotalMinutesofSecond) / 60
            
            print("\(minutesa)")
            
            SendMinutes = "\(minutesa)"
            let sec = Int((TotalMinutesofSecond % 3600) % 60)
            if sec >= 30
            {
                SendMinutes = "\(minutesa + 1)"
            }
            if 59  >= TotalMinutesofSecond
            {
                SendMinutes = "\(TotalMinutesofSecond)"
                SendSecond = SendMinutes
            }
            let totalworkoutTime = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "TotalWorkouttimeVC")as! TotalWorkouttimeVC
            totalworkoutTime.TotalWorkoutMinutes = SendMinutes
            totalworkoutTime.TotlaWorkoutSecond = SendSecond
            totalworkoutTime.modalTransitionStyle = .crossDissolve
            totalworkoutTime.modalPresentationStyle = .overCurrentContext
            self.present(totalworkoutTime, animated: true, completion: nil)
        }
     /*
        {

            var TotalMinutesofSecond:Int = 0
            var Totalsecond:Int = 0

            for i in 0...CreateWorkoutVC.arrExercise.count-1{
                if let model = CreateWorkoutVC.arrExercise[i] as? ExerciseDetails {

                    var Timevalue:String = "00:00"
                    var TotalTimeValue:String = "00:00"

                    if model.excerciseOption == "Time"{
                        Timevalue  =  model.timeValue
                        let time = Timevalue.components(separatedBy: ":")
                        let min:Int    = Int(time[0]) ?? 0
                        let sec:Int =  Int(time[1]) ?? 0
                        TotalMinutesofSecond += (min * 60)
                        Totalsecond += sec
                    }
                    else {
                        TotalTimeValue = model.totalTimeValue
                        let totalTimeArr = TotalTimeValue.components(separatedBy: ":")
                        let min:Int    = Int(totalTimeArr[0]) ?? 0
                        let sec:Int =  Int(totalTimeArr[1]) ?? 0
                        TotalMinutesofSecond += (min * 60)
                        Totalsecond += sec
                    }


                    var  breakTime:String = "00:00"
                    if model.breakTimeValue == "00:00"{
                        breakTime = "00:00"
                    }else if model.breakTimeValue == ""{
                        breakTime = "00:00"
                    }else{
                        breakTime =   model.breakTimeValue
                    }
                    let  breakTimeArr = breakTime.components(separatedBy: ":")
                    let bmin:Int    = Int(breakTimeArr[0]) ?? 0
                    let bsec:Int =  Int(breakTimeArr[1]) ?? 0
                    TotalMinutesofSecond += (bmin * 60)
                    Totalsecond += bsec
                }

            }

            var SendMinutes = ""
            var SendSecond = ""

            TotalMinutesofSecond = TotalMinutesofSecond + Totalsecond


            let minutes = Int(TotalMinutesofSecond) / 60 % 60
            SendMinutes = "\(minutes)"
            let sec = Int((TotalMinutesofSecond % 3600) % 60)
            if sec >= 30
            {
                SendMinutes = "\(minutes + 1)"
            }
            if 59  >= TotalMinutesofSecond
            {
                SendMinutes = "\(TotalMinutesofSecond)"
                SendSecond = SendMinutes
            }
            let totalworkoutTime = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "TotalWorkouttimeVC")as! TotalWorkouttimeVC
            totalworkoutTime.TotalWorkoutMinutes = SendMinutes
            totalworkoutTime.TotlaWorkoutSecond = SendSecond
            totalworkoutTime.modalTransitionStyle = .crossDissolve
            totalworkoutTime.modalPresentationStyle = .overCurrentContext
            self.present(totalworkoutTime, animated: true, completion: nil)
        }
 */
    }
   
    
    @objc func didTapDefaultViewHideshow()
    {
        
        if headerView.btnDefaultViewHideShow.isSelected {
            
            headerView.btnDefaultViewHideShow.isSelected = false
            headerView.View_AllRepsTimer.isHidden = true
            headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 523 - 101)
            headerView.Image_updownarrow.image = UIImage(named: "icon_arrowUp")

        } else {
            headerView.btnDefaultViewHideShow.isSelected = true
            headerView.View_AllRepsTimer.isHidden = false
            headerView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 523 )
            headerView.Image_updownarrow.image = UIImage(named: "icon_arrowDown")
        }
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        self.workoutTblView.reloadData()
    }
    
    @objc func didTapGoodforButton() {
        goodForListing = self.storyboard?.instantiateViewController(withIdentifier: "GoodForListingVC")as? GoodForListingVC
        goodForListing?.selected = arrSelectedGoodfor
        goodForListing?.arrIdData = arrGoodforId
        goodForListing?.completion = { (selected,arrID) in
            if selected != nil{
                self.arrSelectedGoodfor = selected!
                self.arrGoodforId = arrID!
                self.headerView.lblGoodFor.text = selected!.joined(separator: " | ")
                if let text = self.headerView.lblGoodFor.text, !text.isEmpty{
                    self.headerView.lblGoodFor.attributedText = fun_attributedstringNextLine1(StringTitle: " Good for", SelectString: selected!.joined(separator: " | "))
            self.headerView.btnGoodFor.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "", String2: ""), for: .normal)

                }
            else if self.headerView.lblGoodFor.text == ""{
                    
             self.headerView.btnGoodFor.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "Good For ", String2: "*"), for: .normal)

                }
                else{
                    
                }
            }
        }
        self.navigationController?.pushViewController(goodForListing!, animated: true)
    }
    
    @objc func didTapEquipmentsButton() {
        
        equipmentList = self.storyboard?.instantiateViewController(withIdentifier: "EquipmentListVC")as? EquipmentListVC
        equipmentList?.selected = arrSelectedequips
        equipmentList?.arrIdData = arrEquipsId
        equipmentList?.completion = { (selected,arrEquipId) in
            if selected != nil{
                self.arrSelectedequips = selected!
                self.arrEquipsId = arrEquipId!
                self.headerView.lblEquipments.text = selected!.joined(separator: " | ")
                if let text = self.headerView.lblEquipments.text, !text.isEmpty{
                    
                      self.headerView.lblEquipments.attributedText = fun_attributedstringNextLine1(StringTitle: " Equipments", SelectString: selected!.joined(separator: " | "))
                    self.headerView.btnEquipments.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "", String2: ""), for: .normal)

                }
                else if self.headerView.lblEquipments.text == ""{
        self.headerView.btnEquipments.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "Equipments ", String2: "*"), for: .normal)

                }
                else{
                }
            }
        }
        self.navigationController?.pushViewController(equipmentList! , animated: true)
        
    }
    
   // Number of reps selection
    @objc func didTapNoofRepsSelection_Button(sender : UIButton!) {
        
        if 400 ... 499 ~= sender.tag {
            if IsVideoOpen == "OPEN" {
                let idxPath : IndexPath = IndexPath(row: sender.tag - 400 , section: 0)
                ValidationRow = -1
                let objCell: CreateWorkoutCell = self.workoutTblView.cellForRow(at: idxPath) as! CreateWorkoutCell
                objCell.layer.borderWidth = 0
                objCell.layer.borderColor = UIColor.clear.cgColor
                self.closeVideoPlayForCell(indexPath: idxPath)
                return
            }
        }
        
        self.view.endEditing(true)
        var Reps = [String]()
        
        for i in 0...50{
            if i < 10
            {
                Reps.append("0\(i)")
            }
            else
            {
                Reps.append("\(i)")
            }
        }
        
        let mcPicker = McPicker(data: [Reps], selectedTitle:[""])
        mcPicker.label?.font = UIFont.tamilBold(size: 20)
        mcPicker.toolbarBarTintColor = UIColor(r: 235, g: 235, b: 235, alpha: 1)
        
        mcPicker.show { [weak self](selections: [Int : String]) -> Void in
            guard self != nil else{return}
            if let Reps = selections[0]{
                var objCellIdx: Int = 0
                
                if sender.tag == 1002  {
                    self?.headerView.btnNoOfReps.setTitle(Reps, for: .normal)
                    self!.defaultsetting.set(Reps, forKey: "numberOfRepas")

                    return
                }
                if 400 ... 499 ~= sender.tag {
                    print("100 - 199")
                    objCellIdx = sender.tag - 400
                }
                let idxPath : IndexPath = IndexPath(row: objCellIdx, section: 0)
                let objCell: CreateWorkoutCell = self?.workoutTblView.cellForRow(at: idxPath) as! CreateWorkoutCell
                if 400 ... 499 ~= sender.tag {
                    objCell.btnRepsSelection.setTitle(Reps, for: .normal)
                    
                    if Reps == "00"
                    {
                        objCell.lblSelectNoofReps.attributedText =      fun_attributedstring_forcolor(main_string: "Select number of Reps *", string_to_color: "*")
                    }
                    else
                    {
                        objCell.lblSelectNoofReps.text = "Select number of Reps"
                        self?.ValidationRow = -1
                        objCell.layer.borderWidth = 0
                        objCell.layer.borderColor = UIColor.clear.cgColor

                    }
                    
                    if CreateWorkoutVC.arrExercise[objCellIdx] is ExerciseDetails{
                        (CreateWorkoutVC.arrExercise[objCellIdx] as? ExerciseDetails)?.repsValue = Reps
                    }else{
                        (CreateWorkoutVC.arrExercise[objCellIdx] as? ModelWorkoutExerciseList)?.workoutRepeatText = Reps
                    }
                }
            }
        }
    }
    
    
    @objc func didTapTimeSelection_Button(sender : UIButton!) {
        self.view.endEditing(true)
        if let timeComponents = sender.titleLabel?.text?.components(separatedBy: ":") , timeComponents.count > 1{
            timeMinute = timeComponents[0]
            timeSecond = timeComponents[1]
        }
        
        let mcPicker = McPicker(data: [["00", "01","02","03","04","05", "06","07","08","09","10", "11","12","13","14","15", "16","17","18","19","20", "21","22","23","24","25", "26","27","28","29","30", "31","32","33","34","35", "36","37","38","39","40", "41","42","43","44","45", "46","47","48","49","50", "51","52","53","54","55", "56","57","58","59"],["00", "01","02","03","04","05", "06","07","08","09","10", "11","12","13","14","15", "16","17","18","19","20", "21","22","23","24","25", "26","27","28","29","30", "31","32","33","34","35", "36","37","38","39","40", "41","42","43","44","45", "46","47","48","49","50", "51","52","53","54","55", "56","57","58","59"]], selectedTitle: [timeMinute,timeSecond])
        let minLabel = UILabel(frame: CGRect(x: 0, y: ( 216 / 2) - 15, width: (UIScreen.main.bounds.width / 2) - 30, height: 30))
        minLabel.text = "Min"
        minLabel.textAlignment = .right
        mcPicker.picker.addSubview(minLabel)
        let secLabel = UILabel(frame: CGRect(x: UIScreen.main.bounds.width / 2, y: ( 216 / 2) - 15, width: (UIScreen.main.bounds.width / 2) - 30, height: 30))
        secLabel.text = "Sec"
        secLabel.textAlignment = .right
        mcPicker.picker.addSubview(secLabel)
        mcPicker.show {[weak self] (selections: [Int : String]) -> Void in
            guard self != nil else{return}
            if let minute = selections[0], let second = selections[1]{
                
                self?.timeMinute = minute
                self?.timeSecond = second
                
                let strTime: String = ("\(minute):\(second)")
                if sender.tag == 1001  {
                self?.headerView.btn_allTimerExcercise.setTitle(strTime, for: .normal)
                    
                     self!.defaultsetting.set(strTime, forKey: "allTimerExcercises")

                }  else if sender.tag == 1003{
                   self?.headerView.btn_TimeFinish_reps.setTitle(strTime, for: .normal)
                    self!.defaultsetting.set(strTime, forKey: "timeFinishEachExcercise")

                }
            }
        }
        mcPicker.bringSubview(toFront: minLabel)
        mcPicker.bringSubview(toFront: secLabel)
    }
    
    
    @objc func didTapTimeSelectionButton(sender : UIButton!) {
        
        var objIndex: Int = 0
        if 1000 ... 1999 ~= sender.tag {
            print("1000 - 1999")
            objIndex = sender.tag - 1000
        }else if 2000 ... 2999 ~= sender.tag {
            print("2000 - 2999")
            objIndex = sender.tag - 2000
        }else{
            objIndex = sender.tag - 3000
        }
        
        if IsVideoOpen == "OPEN" {
            let idxPath : IndexPath = IndexPath(row: objIndex, section: 0)
            ValidationRow = -1
            let objCell: CreateWorkoutCell = self.workoutTblView.cellForRow(at: idxPath) as! CreateWorkoutCell
            objCell.layer.borderWidth = 0
            objCell.layer.borderColor = UIColor.clear.cgColor
            self.closeVideoPlayForCell(indexPath: idxPath)
            return
        }
        
        self.view.endEditing(true)
        
        self.btnSelected = sender
        if let timeComponents = sender.titleLabel?.text?.components(separatedBy: ":") , timeComponents.count > 1{
            timeMinute = timeComponents[0]
            timeSecond = timeComponents[1]
        }
        
        let mcPicker = McPicker(data: [["00", "01","02","03","04","05", "06","07","08","09","10", "11","12","13","14","15", "16","17","18","19","20", "21","22","23","24","25", "26","27","28","29","30", "31","32","33","34","35", "36","37","38","39","40", "41","42","43","44","45", "46","47","48","49","50", "51","52","53","54","55", "56","57","58","59"],["00", "01","02","03","04","05", "06","07","08","09","10", "11","12","13","14","15", "16","17","18","19","20", "21","22","23","24","25", "26","27","28","29","30", "31","32","33","34","35", "36","37","38","39","40", "41","42","43","44","45", "46","47","48","49","50", "51","52","53","54","55", "56","57","58","59"]], selectedTitle: [timeMinute,timeSecond])
        let minLabel = UILabel(frame: CGRect(x: 0, y: ( 216 / 2) - 15, width: (UIScreen.main.bounds.width / 2) - 30, height: 30))
        minLabel.text = "Min"
        minLabel.textAlignment = .right
        mcPicker.picker.addSubview(minLabel)
        let secLabel = UILabel(frame: CGRect(x: UIScreen.main.bounds.width / 2, y: ( 216 / 2) - 15, width: (UIScreen.main.bounds.width / 2) - 30, height: 30))
        secLabel.text = "Sec"
        secLabel.textAlignment = .right
        mcPicker.picker.addSubview(secLabel)
        mcPicker.show {[weak self] (selections: [Int : String]) -> Void in
            guard self != nil else{return}
            if let minute = selections[0], let second = selections[1]{
                
                self?.timeMinute = minute
                self?.timeSecond = second
                var objCellIdx: Int = 0
                
                if 1000 ... 1999 ~= sender.tag {
                    print("1000 - 1999")
                    objCellIdx = sender.tag - 1000
                }else if 2000 ... 2999 ~= sender.tag {
                    print("2000 - 2999")
                    objCellIdx = sender.tag - 2000
                }else{
                    objCellIdx = sender.tag - 3000
                }
                let idxPath : IndexPath = IndexPath(row: objCellIdx, section: 0)
                let objCell: CreateWorkoutCell = self?.workoutTblView.cellForRow(at: idxPath) as! CreateWorkoutCell
                let strTime: String = ("\(minute):\(second)")
                if 1000 ... 1999 ~= sender.tag {
                    objCell.btnTimeSelection.setTitle(strTime, for: .normal)
                    if strTime == "00:00"
                    {
                        objCell.lblTimeSelection.attributedText = fun_attributedstring_forcolor(main_string: "Select time *", string_to_color: "*")
                    }
                    else
                    {
                        objCell.lblTimeSelection.text = "Select time"
                        self?.ValidationRow = -1
                        objCell.layer.borderWidth = 0
                        objCell.layer.borderColor = UIColor.clear.cgColor
                        
                    }
                
                    if CreateWorkoutVC.arrExercise[objCellIdx] is ExerciseDetails{
                        (CreateWorkoutVC.arrExercise[objCellIdx] as? ExerciseDetails)?.timeValue = strTime
                    }else{
                        (CreateWorkoutVC.arrExercise[objCellIdx] as? ModelWorkoutExerciseList)?.workoutExercisesTime = strTime
                    }
                } else if 2000 ... 2999 ~= sender.tag{
                    objCell.btnTotalTimeSelection.setTitle(strTime, for: .normal)
                    
                    if strTime == "00:00"
                    {
                        objCell.lblTimetoFinsihReps.attributedText =      fun_attributedstring_forcolor(main_string: "Time to finish Reps *", string_to_color: "*")
                    }
                    else
                    {
                        objCell.lblTimetoFinsihReps.text = "Time to finish Reps"
                        self?.ValidationRow = -1
                        objCell.layer.borderWidth = 0
                        objCell.layer.borderColor = UIColor.clear.cgColor

                    }
                    
                    if CreateWorkoutVC.arrExercise[objCellIdx] is ExerciseDetails{
                        (CreateWorkoutVC.arrExercise[objCellIdx] as? ExerciseDetails)?.totalTimeValue = strTime
                    }else{
                        (CreateWorkoutVC.arrExercise[objCellIdx] as? ModelWorkoutExerciseList)?.workoutTotalTime = strTime
                    }
                } else {
                    objCell.btnBreakTimeSelection.setTitle(strTime, for: .normal)
                    if CreateWorkoutVC.arrExercise[objCellIdx] is ExerciseDetails{
                        (CreateWorkoutVC.arrExercise[objCellIdx] as? ExerciseDetails)?.breakTimeValue = strTime
                    }else{
                        (CreateWorkoutVC.arrExercise[objCellIdx] as? ModelWorkoutExerciseList)?.workoutBreakTime = strTime
                    }
                }
            }
        }
        mcPicker.bringSubview(toFront: minLabel)
        mcPicker.bringSubview(toFront: secLabel)
    }
    
    
    @objc func didTapVideoPlayButton(sender : UIButton!) {
        
        let idxPath : IndexPath = IndexPath(row: sender.tag - 500, section: 0)
        ValidationRow = -1
        let objCell: CreateWorkoutCell = self.workoutTblView.cellForRow(at: idxPath) as! CreateWorkoutCell
        objCell.layer.borderWidth = 0
        objCell.layer.borderColor = UIColor.clear.cgColor

        self.openVideoUIForTableCell(indexPath: idxPath )
    }
    
    
    @objc func didTapDuplicateButton(sender : UIButton!) {
        
        if IsVideoOpen == "OPEN" {
            let idxPath : IndexPath = IndexPath(row: sender.tag - 50, section: 0)
            ValidationRow = -1
            let objCell: CreateWorkoutCell = self.workoutTblView.cellForRow(at: idxPath) as! CreateWorkoutCell
            objCell.layer.borderWidth = 0
            objCell.layer.borderColor = UIColor.clear.cgColor
            self.closeVideoPlayForCell(indexPath: idxPath)
            return
        }
        ValidationRow = -1

        
        let objNN = CreateWorkoutVC.arrExercise[sender.tag - 50] as? ExerciseDetails
        let newObj = objNN?.copy() as? ExerciseDetails
        newObj?.excerciseOption = objNN?.excerciseOption
        newObj?.exerciseName = objNN?.exerciseName
        newObj?.exerciseAccessLevel = objNN?.exerciseAccessLevel
        newObj?.exerciseAmount = objNN?.exerciseAmount
        newObj?.exerciseAmountDisplay = objNN?.exerciseAmountDisplay
        newObj?.exerciseCategory = objNN?.exerciseCategory
        newObj?.exerciseDescription = objNN?.exerciseDescription
        newObj?.exerciseImage = objNN?.exerciseImage
        newObj?.exerciseIsFavourite = objNN?.exerciseIsFavourite
        newObj?.exerciseIsFeatured = objNN?.exerciseIsFeatured
        newObj?.exerciseLevel = objNN?.exerciseLevel
        newObj?.exerciseShareUrl = objNN?.exerciseShareUrl
        newObj?.exerciseVideo = objNN?.exerciseVideo
        newObj?.exerciseVideoDuration = objNN?.exerciseVideoDuration
        newObj?.exerciseBodyParts = objNN?.exerciseBodyParts
        newObj?.exerciseEquipments = objNN?.exerciseEquipments
        newObj?.exerciseId = objNN?.exerciseId
        newObj?.exerciseTags = objNN?.exerciseTags
        newObj?.isLiked = objNN?.isLiked
        newObj?.timeValue = objNN?.timeValue
        newObj?.repsValue = objNN?.repsValue
        newObj?.breakTimeValue = objNN?.breakTimeValue
        newObj?.totalTimeValue = objNN?.totalTimeValue

        CreateWorkoutVC.arrExercise.insert(newObj as! ExerciseDetails, at: sender.tag - 50 + 1)
            self.workoutTblView.reloadData()
            self.workoutTblView.layoutIfNeeded()
            self.workoutTblView.setNeedsLayout()

    }
    
    @objc func didTapDeleteButton(sender : UIButton!) {

        if IsVideoOpen == "OPEN" {
            let idxPath : IndexPath = IndexPath(row: sender.tag , section: 0)
            ValidationRow = -1
            let objCell: CreateWorkoutCell = self.workoutTblView.cellForRow(at: idxPath) as! CreateWorkoutCell
            objCell.layer.borderWidth = 0
            objCell.layer.borderColor = UIColor.clear.cgColor
            self.closeVideoPlayForCell(indexPath: idxPath)
            return
        }
       
        
        UIAlertController.showAlert(in: self, withTitle: "", message: AlertMessages.deleteConfirmation, cancelButtonTitle: "No", destructiveButtonTitle: "Delete", otherButtonTitles: nil, tap: { (alertController, action, buttonIndex) in
            if buttonIndex == 1 {
                CreateWorkoutVC.arrExercise.remove(at: sender.tag) //Remove your element from
                self.workoutTblView.deleteRows(at: [IndexPath(row: sender.tag, section: 0)], with: .automatic)
                //   self.workoutTblView.beginUpdates()
                self.ValidationRow = -1
                self.workoutTblView.reloadData()
                self.workoutTblView.layoutIfNeeded()
                //   self.workoutTblView.endUpdates()
                
            }
        }
        )
    }
    // MARK: UITableview video play and close
    func openVideoUIForTableCell(indexPath: IndexPath)
    {
        if selectedRow == -1 {
            selectedRow = indexPath.row
            self.workoutTblView.isScrollEnabled = false
            IsVideoOpen  = "OPEN"
            let selectedCell = workoutTblView.cellForRow(at: indexPath) as! CreateWorkoutCell
            selectedCell.layer.borderWidth = 0
            selectedCell.layer.borderColor = UIColor.clear.cgColor
            ValidationRow = -1

        //    selectedCell.contentView.backgroundColor =         UIColor.colorWithRGB(r: 244, g: 244, b: 244)
          //  selectedCell.contentView.backgroundColor = UIColor.white

            selectedCell.btnVideo.setImage(UIImage(named: "ImgUparrow"), for: .normal)
            self.visibleCellsDuringAnimation = workoutTblView.visibleCells
            selectedCell.setUpPlayerInCell(urlString: (CreateWorkoutVC.arrExercise [indexPath.row] as! ExerciseDetails).exerciseVideo)
            
            for cell in self.self.visibleCellsDuringAnimation {
                if let theCell = cell as? CreateWorkoutCell {
                    // theCell.viewUpperLayer.isHidden = true
                   // theCell.contentView.backgroundColor = UIColor.colorWithRGB(r: 244, g: 244, b: 244)
                 //  theCell.contentView.backgroundColor = UIColor.red
                }
            }
            
            selectedCell.constPlayerHeight.constant =  playerHeight
            self.workoutTblView.beginUpdates()
            self.workoutTblView.endUpdates()
            
            DispatchQueue.main.async {
                self.workoutTblView.scrollToRow(at: indexPath, at: .none, animated: true)
                self.workoutTblView.layoutIfNeeded()
            }
        }
        else
        {
            self.closeVideoPlayForCell(indexPath: indexPath)
        }
    }
    
    func closeVideoPlayForCell(indexPath: IndexPath){
        
        guard let selectedCell = workoutTblView.cellForRow(at: IndexPath(row: selectedRow, section: 0)) as? CreateWorkoutCell else{
            DispatchQueue.main.async {
                self.workoutTblView.reloadData()
            }
            return
        }
        selectedCell.btnVideo.setImage(UIImage(named: "arrow_dropdown_New"), for: .normal)
        selectedCell.layer.borderWidth = 0
        selectedCell.layer.borderColor = UIColor.clear.cgColor
        ValidationRow = -1
        selectedRow = -1
        self.workoutTblView.beginUpdates()
        self.workoutTblView.endUpdates()
        self.workoutTblView.layoutIfNeeded()

        for cell in self.self.visibleCellsDuringAnimation {
            if let theCell = cell as? CreateWorkoutCell {
             // theCell.contentView.backgroundColor = UIColor.white
            }
        }
        selectedCell.constPlayerHeight.constant = 0
        selectedCell.stopVideo()
        selectedCell.removePlayerFromView()
        self.workoutTblView.scrollToRow(at:indexPath, at: .none, animated: true)
        self.workoutTblView.isScrollEnabled = true
        IsVideoOpen  = "CLOSED"

    }
    
    // MARK: UITableview DataSource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CreateWorkoutVC.arrExercise.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CreateWorkoutCell") as! CreateWorkoutCell
        
        if ValidationRow == indexPath.row
        {
            cell.layer.borderWidth = 1.0
            cell.layer.borderColor = UIColor.red.cgColor
        }
        else
        {
            cell.layer.borderWidth = 0
            cell.layer.borderColor = UIColor.clear.cgColor
        }
        
        cell.tag = indexPath.row
        cell.txtfldTime.tag = timeTxtfld_Tag + cell.tag
        cell.txtfldReps.tag = repsTxtfld_Tag + cell.tag
        cell.totalTimeView.tag = totalTimeTxtfld_Tag + cell.tag
        cell.txtfldBreakTime.tag = breakTimeTxtfld_Tag + cell.tag
        cell.breakView.isHidden = false
        cell.setLayoutofTextField()
        cell.delegate = self
        cell.btnTime.setImage(UIImage(named: "btn_selexc")?.withRenderingMode(.alwaysOriginal), for: .normal)
        cell.totalTimeView.isHidden = true
        cell.lblbreaktime.backgroundColor = UIColor.colorWithRGB(r: 231.0, g: 231.0, b: 231.0)
        cell.btnDelete.tag = indexPath.row
        cell.btnDuplicate.tag = indexPath.row + 50
        cell.btnTimeSelection.tag = indexPath.row + 1000
        cell.btnTotalTimeSelection.tag = indexPath.row + 2000
        cell.btnBreakTimeSelection.tag = indexPath.row + 3000
        cell.btnRepsSelection.tag = indexPath.row + 400
        cell.btnVideo.tag = indexPath.row + 500

        cell.btnDelete.addTarget(self, action: #selector(self.didTapDeleteButton), for: .touchUpInside)
        cell.btnVideo.addTarget(self, action: #selector(self.didTapVideoPlayButton), for: .touchUpInside)
        cell.btnDuplicate.addTarget(self, action: #selector(self.didTapDuplicateButton), for: .touchUpInside)

        cell.btnRepsSelection.addTarget(self, action: #selector(self.didTapNoofRepsSelection_Button(sender:)), for: .touchUpInside)

        cell.btnTimeSelection.addTarget(self, action: #selector(self.didTapTimeSelectionButton(sender:)), for: .touchUpInside)
        cell.btnTotalTimeSelection.addTarget(self, action: #selector(self.didTapTimeSelectionButton(sender:)), for: .touchUpInside)
        cell.btnBreakTimeSelection.addTarget(self, action: #selector(self.didTapTimeSelectionButton(sender:)), for: .touchUpInside)
        if indexPath.row == CreateWorkoutVC.arrExercise.count - 1 {
            cell.breakView.isHidden = true
            cell.lblbreaktime.backgroundColor = UIColor.clear
        }
        
        if let detail = CreateWorkoutVC.arrExercise[indexPath.row] as? ExerciseDetails{
            cell.setCellContent(detail)
        }
        return cell
    }
    
    
 // MARK: UITableview Delegate Methods
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    
    if self.selectedRow == indexPath.row{
        if indexPath.row == CreateWorkoutVC.arrExercise.count - 1{
            return cellVideoHeight - 14
        }
      return cellVideoHeight + 25
    }else{
        if indexPath.row == CreateWorkoutVC.arrExercise.count - 1{
            return (CreateWorkoutVC.arrExercise[indexPath.row] as? ExerciseDetails)?.excerciseOption == "Time" ? 113.0 : 113.0
        }
        if CreateWorkoutVC.arrExercise[indexPath.row] is ExerciseDetails{
            return (CreateWorkoutVC.arrExercise[indexPath.row] as? ExerciseDetails)?.excerciseOption == "Time" ? 153.0 : 153.0
        }
    }
    
      return 0
        /*
         if indexPath.row == CreateWorkoutVC.arrExercise.count - 1{
         return (CreateWorkoutVC.arrExercise[indexPath.row] as? ExerciseDetails)?.excerciseOption == "Time" ? 135.0 : 170.0
         }
         if CreateWorkoutVC.arrExercise[indexPath.row] is ExerciseDetails{
         return (CreateWorkoutVC.arrExercise[indexPath.row] as? ExerciseDetails)?.excerciseOption == "Time" ? 175.0 : 210.0
         }
         return 0
 */
   }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row < CreateWorkoutVC.arrExercise.count{
            let exeDetails = CreateWorkoutVC.arrExercise[indexPath.row]as! ExerciseDetails
            if exeDetails.exerciseAccessLevel.uppercased() == "OPEN".uppercased(){
                if selectedRow == -1
                {
                  //  self.openVideoUIForTableCell(indexPath: indexPath)
                }
                else
                {
                    self.closeVideoPlayForCell(indexPath: indexPath)
                }
               self.workoutTblView.reloadData()

            }else{
                if let upgradeVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "UpgradeToPremiumViewController") as? UpgradeToPremiumViewController{
                    upgradeVC.onPurchaseSuccess = {
                     //   self.updateUI()
                    }
            self.navigationController?.pushViewController(upgradeVC, animated: true)
                }
            }
        }
    }
 
    public func textFieldDidBeginEditing(_ textField: UITextField){
        
    }
    
    static func createWorkoutArrayContainsExercise(exerciseObj: Any) -> Bool {
        var isExerciseDetailsType = true
        var exerciseId: String!
        if let exercise = exerciseObj as? ExerciseDetails {
            exerciseId = exercise.exerciseId
        } else if let exercise = exerciseObj as? ModelWorkoutExerciseList {
            exerciseId = exercise.workoutExercisesId
            isExerciseDetailsType = false
        }
        let arrContainsSelectedObject = CreateWorkoutVC.arrExercise.contains(where: { (obj) -> Bool in
            if isExerciseDetailsType, let theObj = obj as? ExerciseDetails, theObj.exerciseId == exerciseId {
                return true
            } else if let theObj = obj as? ModelWorkoutExerciseList, theObj.workoutExercisesId == exerciseId {
                return true
            }
            return false
        })
        return arrContainsSelectedObject
    }
    
    static func indexOfObjectInCreateWorkoutArray(exerciseObj: Any) -> Int? {
        
        var isExerciseDetailsType = true
        var exerciseId: String!
        if let exercise = exerciseObj as? ExerciseDetails {
            exerciseId = exercise.exerciseId
        } else if let exercise = exerciseObj as? ModelWorkoutExerciseList {
            exerciseId = exercise.workoutExercisesId
            isExerciseDetailsType = false
        }
        if let indexOfObj = CreateWorkoutVC.arrExercise.index(where: { (obj) -> Bool in
            if isExerciseDetailsType, let theObj = obj as? ExerciseDetails, theObj.exerciseId == exerciseId {
                return true
            } else if let theObj = obj as? ModelWorkoutExerciseList, theObj.workoutExercisesId == exerciseId {
                return true
            }
            return false
        }) {
            return indexOfObj
        }
        return nil
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if segue.identifier == "createToList"{
			let cont = segue.destination as! MyWorkoutsVC
			cont.removePreviousCont = true
		}
        else if let dest = segue.destination as? WorkoutExeVC {
            dest.isFromCreateWorkout = true
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
class MyClass: AddWorkoutHeaderView {
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "AddWorkoutHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}

extension CreateWorkoutVC: CreateWorkoutCellDelegate{
    
    func btnRepsSelectionValue(_ RepsNo: String, _ index: Int) {
      
        if CreateWorkoutVC.arrExercise[index] is ExerciseDetails{
            (CreateWorkoutVC.arrExercise[index] as? ExerciseDetails)?.repsValue = RepsNo
        }
        else{
            (CreateWorkoutVC.arrExercise[index] as? ModelWorkoutExerciseList)?.workoutRepeatText = RepsNo
        }
    }
    
    func btnTimeSelectionValue(_ time: String,_ index: Int) {
      
        if CreateWorkoutVC.arrExercise[index] is ExerciseDetails{
            (CreateWorkoutVC.arrExercise[index] as? ExerciseDetails)?.timeValue = time
        }else{
            (CreateWorkoutVC.arrExercise[index] as? ModelWorkoutExerciseList)?.workoutExercisesTime = time
        }
    }
    
    func btnTotalTimeSelectionValue(_ totaltime: String,_ index: Int) {
       
        if CreateWorkoutVC.arrExercise[index] is ExerciseDetails{
            (CreateWorkoutVC.arrExercise[index] as? ExerciseDetails)?.totalTimeValue = totaltime
        }else{
            (CreateWorkoutVC.arrExercise[index] as? ModelWorkoutExerciseList)?.workoutTotalTime = totaltime
        }
    }
    
    func btnBreakTimeSelectionValue(_ breaktime: String,_ index: Int) {
       
        
        if CreateWorkoutVC.arrExercise[index] is ExerciseDetails{
            (CreateWorkoutVC.arrExercise[index] as? ExerciseDetails)?.breakTimeValue = breaktime
        }else{
            (CreateWorkoutVC.arrExercise[index] as? ModelWorkoutExerciseList)?.workoutBreakTime = breaktime
        }
    }
    
    func txtDidEndEditing(_ time: String, _ reps: String, _ breaktime: String, _ totaltime: String, _ index: Int) {
		if index < CreateWorkoutVC.arrExercise.count{
			if CreateWorkoutVC.arrExercise[index] is ExerciseDetails{
				(CreateWorkoutVC.arrExercise[index] as? ExerciseDetails)?.repsValue = reps
			}
			else{
				(CreateWorkoutVC.arrExercise[index] as? ModelWorkoutExerciseList)?.workoutRepeatText = reps
			}
		}
    }
    
    func txtDidBeginEditing(_ textField: UITextField) {
        print("textFieldDidBeginEditing")
    }
    
    func toggleOption(_ option: String, _ index: Int ) {
        
        if IsVideoOpen == "OPEN" {
            let idxPath : IndexPath = IndexPath(row: index , section: 0)
            ValidationRow = -1
            let objCell: CreateWorkoutCell = self.workoutTblView.cellForRow(at: idxPath) as! CreateWorkoutCell
            objCell.layer.borderWidth = 0
            objCell.layer.borderColor = UIColor.clear.cgColor
            self.closeVideoPlayForCell(indexPath: idxPath)
            return
        }
        
        if CreateWorkoutVC.arrExercise[index] is ExerciseDetails{
            (CreateWorkoutVC.arrExercise[index] as? ExerciseDetails)?.excerciseOption = option
            if option == "Time"{
                
                (CreateWorkoutVC.arrExercise[index] as? ExerciseDetails)?.repsValue = defaultsetting.string(forKey: "numberOfRepas") ?? ""
                (CreateWorkoutVC.arrExercise[index] as? ExerciseDetails)?.totalTimeValue = defaultsetting.string(forKey: "timeFinishEachExcercise") ?? ""
                
                
            }else{
                (CreateWorkoutVC.arrExercise[index] as? ExerciseDetails)?.timeValue = defaultsetting.string(forKey: "allTimerExcercises") ?? ""
                
                // Add line of code for uitableview * and validation manage
                (CreateWorkoutVC.arrExercise[index] as? ExerciseDetails)?.repsValue = defaultsetting.string(forKey: "numberOfRepas") ?? ""
                (CreateWorkoutVC.arrExercise[index] as? ExerciseDetails)?.totalTimeValue = defaultsetting.string(forKey: "timeFinishEachExcercise")
               
            }
        }else{
            
            (CreateWorkoutVC.arrExercise[index] as? ModelWorkoutExerciseList)?.workoutOption = option
            if option == "Time"{
                // Clear Reps
                btnTimeSelected = true
                (CreateWorkoutVC.arrExercise[index] as? ModelWorkoutExerciseList)?.workoutRepeatText = ""
                (CreateWorkoutVC.arrExercise[index] as? ModelWorkoutExerciseList)?.workoutTotalTime = "00:00"
            }else{
                // Clear Time
                btnTimeSelected = false
                
                (CreateWorkoutVC.arrExercise[index] as? ModelWorkoutExerciseList)?.workoutExercisesTime = "00:00"
            }
        }
        //    self.workoutTblView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        self.workoutTblView.beginUpdates()
        self.workoutTblView.endUpdates()
        
        let idxPath : IndexPath = IndexPath(row: index, section: 0)
        let objCell: CreateWorkoutCell = self.workoutTblView.cellForRow(at: idxPath) as! CreateWorkoutCell
        (CreateWorkoutVC.arrExercise[index] as? ExerciseDetails)?.excerciseOption = option
        if option == "Time"{
            objCell.totalTimeView.isHidden = true
            objCell.btnTime.setImage(UIImage(named: "btn_selexc")?.withRenderingMode(.alwaysOriginal), for: .normal)
            objCell.btnReps.setImage(UIImage(named: "btn_unselexc")?.withRenderingMode(.alwaysOriginal), for: .normal)
            objCell.lbl_TimeCircle.textColor = UIColor(r: 34/255.0, g: 34/255.0, b: 34/255.0, alpha: 1.0)
            objCell.lbl_RepsCircle.text = "Reps"
            objCell.lbl_RepsCircle.textColor = UIColor.lightGray

            objCell.btnRepsSelection.isHidden = true
            objCell.btnTimeSelection.isHidden = false
            objCell.lblTimeSelection.isHidden = false
            
            
           objCell.btnTimeSelection.setTitle(defaultsetting.string(forKey: "allTimerExcercises") ?? "", for: .normal)
            
            if defaultsetting.string(forKey: "allTimerExcercises") == "00:00"
            {
                objCell.lblTimeSelection.attributedText = fun_attributedstring_forcolor(main_string: "Select time *", string_to_color: "*")
            }
            else
            {
                objCell.lblTimeSelection.text = "Select time"
            }
            
            if option == "Time"{
          
                (CreateWorkoutVC.arrExercise[index] as? ExerciseDetails)?.repsValue = defaultsetting.string(forKey: "numberOfRepas") ?? ""
                (CreateWorkoutVC.arrExercise[index] as? ExerciseDetails)?.totalTimeValue = defaultsetting.string(forKey: "timeFinishEachExcercise") ?? ""
                
                // code add for uitableview * manage when scrolling
                (CreateWorkoutVC.arrExercise[index] as? ExerciseDetails)?.timeValue = defaultsetting.string(forKey: "allTimerExcercises") ?? ""
                
                
            }else{
             
                (CreateWorkoutVC.arrExercise[index] as? ExerciseDetails)?.timeValue = defaultsetting.string(forKey: "allTimerExcercises") ?? ""
            }
        }
        else{
            objCell.totalTimeView.isHidden = false
            //        bottomConstraint.constant = 15
            objCell.btnTime.setImage(UIImage(named: "btn_unselexc")?.withRenderingMode(.alwaysOriginal), for: .normal)
            objCell.lbl_TimeCircle.textColor = UIColor.lightGray
            objCell.btnReps.setImage(UIImage(named: "btn_selexc")?.withRenderingMode(.alwaysOriginal), for: .normal)
            objCell.lbl_RepsCircle.textColor = UIColor(r: 34/255.0, g: 34/255.0, b: 34/255.0, alpha: 1.0)
            objCell.lbl_RepsCircle.text = "Reps"
            objCell.btnRepsSelection.isHidden = false
            objCell.btnTimeSelection.isHidden = true
            objCell.lblTimeSelection.isHidden = true
            
       // loendra comment code below
            
//            objCell.lblTimeSelection.attributedText = fun_attributedstring_forcolor(main_string: "Select time *", string_to_color: "*")
//            objCell.lblTimetoFinsihReps.attributedText =      fun_attributedstring_forcolor(main_string: "Time to finish Reps *", string_to_color: "*")
//            objCell.btnTotalTimeSelection.setTitle("00:00", for: .normal)
//            objCell.txtfldReps.text = ""
//            objCell.btnRepsSelection.setTitle("00", for: .normal)
//            objCell.lblSelectNoofReps.attributedText =      fun_attributedstring_forcolor(main_string: "Select number of Reps *", string_to_color: "*")
            
            //////
            
            
            if defaultsetting.string(forKey: "allTimerExcercises") == "00:00"
            {
            objCell.lblTimeSelection.attributedText = fun_attributedstring_forcolor(main_string: "Select time *", string_to_color: "*")
            }
            else
            {
               objCell.lblTimeSelection.text = "Select time"
            }
          objCell.btnTotalTimeSelection.setTitle(defaultsetting.string(forKey: "timeFinishEachExcercise") ?? "", for: .normal)
          objCell.btnRepsSelection.setTitle(defaultsetting.string(forKey: "numberOfRepas") ?? "", for: .normal)

            objCell.txtfldReps.text = ""
            if defaultsetting.string(forKey: "numberOfRepas") == "00"
            {
                objCell.lblSelectNoofReps.attributedText =      fun_attributedstring_forcolor(main_string: "Select number of Reps *", string_to_color: "*")
            }
            else
            {
                objCell.lblSelectNoofReps.text =  "Select number of Reps"
            }
            
            if defaultsetting.string(forKey: "timeFinishEachExcercise") == "00:00"
            {
                objCell.lblTimetoFinsihReps.attributedText =      fun_attributedstring_forcolor(main_string: "Time to finish Reps *", string_to_color: "*")
            }
            else
            {
                objCell.lblTimetoFinsihReps.text =  "Time to finish Reps"
            }
            
        }
       // self.workoutTblView.beginUpdates()
       // self.workoutTblView.endUpdates()

    }
}

// loks add function for * set red color
func fun_attributedstring_forcolor(main_string:NSString ,string_to_color:NSString ) -> NSMutableAttributedString {
    let range = (main_string as NSString).range(of: string_to_color as String)

    let attribute = NSMutableAttributedString.init(string: main_string as String)

    attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red , range: range)
    return attribute
}

func fun_attributedstring_forcolorBtn(String1:String ,String2:String ) -> NSMutableAttributedString {
    
    let att = NSMutableAttributedString(string: "\(String1)\(String2)");
    att.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.init(r: 199.0, g: 199.0, b: 199.0, alpha: 1.0), range: NSRange(location: 0, length: String1.count))
    att.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: NSRange(location: String1.count, length: String2.count))
    return att

}


extension CreateWorkoutVC: TableViewDraggerDataSource, TableViewDraggerDelegate {
    func dragger(_ dragger: TableViewDragger, moveDraggingAt indexPath: IndexPath, newIndexPath: IndexPath) -> Bool {
        
        if selectedRow == -1
        {
        let item = CreateWorkoutVC.arrExercise[indexPath.row]
        CreateWorkoutVC.arrExercise.remove(at: indexPath.row)
        CreateWorkoutVC.arrExercise.insert(item, at: newIndexPath.row)
        workoutTblView.moveRow(at: indexPath, to: newIndexPath)
        return true
        }
        return false
    }
    func dragger(_ dragger: TableViewDragger, didEndDraggingAt indexPath: IndexPath) {
        DispatchQueue.main.async {
            self.workoutTblView.reloadData()
        }
    }
 
    
}

// MARK: WebService methods

extension CreateWorkoutVC {
    
    /**
     This method is used to call feed listing web service.
     */
    func callLevelDataListingWS(){
        guard let cUser = DoviesGlobalUtility.currentUser else {return}
        var dic = [String: Any]()
        dic[WsParam.authCustomerId] = cUser.customerAuthToken
        dic["module_type"] = ""
        
        DoviesWSCalls.callGetModelFilterDataAPI(dicParam: dic, onSuccess: { (sucess, filterArr, strMessage) in
            GlobalUtility.hideActivityIndi(viewContView: self.view)
            if sucess{
                if filterArr != nil{
                    let arrValue = filterArr!.filter {$0.groupName == "Level"}
                    if arrValue.count > 0{
                        self.arrLevelListing = arrValue[0].list
                    }
                    
                }
            }
        }) { (error) in
            
        }
    }
    
    /**
     This method is used to call 'Create Workout' api to add the workout information on the server. After creating the workout, screen popouts.
     */
    func createWorkoutWSCall(){
        GlobalUtility.showActivityIndi(viewContView: self.view)
        var dic = [String: Any]()
        guard let cUser = DoviesGlobalUtility.currentUser else {return}
        dic[WsParam.authCustomerId] = cUser.customerAuthToken
        dic[WsParam.workout_name] = headerView.txtfldTitle.text
        dic[WsParam.workout_image] = headerView.imgAddPhoto.image
        dic[WsParam.workout_level] = Str_SlectLevel
        dic[WsParam.workout_good_for] =  self.arrGoodforId.joined(separator:  ",")
        dic[WsParam.workout_equipment] = self.arrEquipsId.joined(separator: ",")
        dic[WsParam.workout_description] = headerView.txtView.text ?? ""
        
        if isNewworkout == false
        {
            if isEditSelected == true{
                dic[WsParam.parent_workout_id] = self.parentExeID
                dic["workout_id"] = self.parentExeID
            }
        }
        else
        {
            self.isEditSelected = false
        }

        var arrData : [[String : Any]] = [[String : Any]]()
        for i in 0...CreateWorkoutVC.arrExercise.count-1{
            if let model = CreateWorkoutVC.arrExercise[i] as? ExerciseDetails {
                var dic = [String: Any]()
                if model.excerciseOption == "Time"{
                    dic["exercise_time"] = "00:" + model.timeValue
                    dic["exercise_type"] = "Time"
                }else{
                    dic["exercise_time"] = "00:" + model.totalTimeValue
                    dic["exercise_type"] = "Repeat"
                }
                dic["exercise_id"] = model.exerciseId
                dic["exercise_repeat_text"] = model.repsValue
                if model.breakTimeValue == "00:00"{
                    dic["break_time"] = "00:00:00"
                }else if model.breakTimeValue == ""{
                    dic["break_time"] = "00:00:00"
                }else{
                    dic["break_time"] = "00:" + model.breakTimeValue
                }
                dic["sequence_number"] = "\(i + 1) "
                arrData.append(dic)
            }
        }
        let data = try! JSONSerialization.data(withJSONObject:arrData, options: [ ])
        let jsonString = String(data: data, encoding: .utf8)
        dic["exercise"] = jsonString!
        print(dic)
        DoviesWSCalls.callCreateWorkoutAPI(isForEdit: isEditSelected == true ? isCreatedByYou == true ? true : false : false  ,dicParam: dic, onSuccess: {[weak self] (isSuccess, message, response) in
            guard let weakSelf = self else{return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
            if isSuccess {
                if weakSelf.isForEditWork != nil{
                    weakSelf.isForEditWork!()
                }
                CreateWorkoutVC.arrExercise = [Any]()
				if weakSelf.isFromMyProfileCreate == true{
                    
                	weakSelf.performSegue(withIdentifier: "createToList", sender: nil)
				}
				else{
                      weakSelf.performSegue(withIdentifier: "createToList", sender: nil)
                    // weakSelf.navigationController?.popViewController(animated: true)
				}
				  GlobalUtility.showToastMessage(msg: "Done")
            }
			else{
            	DoviesGlobalUtility.showSimpleAlert(viewController: weakSelf, message: message, completion: nil)
			}
            
        }) { [weak self](error) in
            guard let weakSelf = self else{return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
        }
    }
    
}

