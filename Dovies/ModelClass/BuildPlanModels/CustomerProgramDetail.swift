
import Foundation


class CustomerProgramDetail : NSObject, NSCoding{

	var isProgramFavourite : String!
	var programAccessLevel : String!
	var programAmount : String!
	var programDescription : String!
	var programEquipments : String!
	var programFullImage : String!
	var programGoodFor : String!
	var programId : String!
	var programImage : String!
	var programName : String!
	var programShareUrl : String!
	var programWeekCount : String!
    var programFavStatus : Bool!
    var programLevel : String!
    
    var programNotification : String!
    var programAccessLevelSelect : String!
    var programNewsFeed : String!
    var programAllowedUserId : String!
    var programAllowedUsersName : String!

    var programGoodforMasterName : String!
    var programEquipmentMasterName : String!
    

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		isProgramFavourite = dictionary["is_program_favourite"] as? String ?? ""
		programAccessLevel = dictionary["program_access_level"] as? String ?? ""
		programAmount = dictionary["program_amount"] as? String ?? ""
		programDescription = dictionary["program_description"] as? String ?? ""
		programEquipments = dictionary["program_equipments"] as? String ?? ""
        programEquipmentMasterName = dictionary["equipmentMasterName"] as? String ?? ""
		programFullImage = dictionary["program_full_image"] as? String ?? ""
		programGoodFor = dictionary["program_good_for"] as? String ?? ""
        programGoodforMasterName = dictionary["goodforMasterName"] as? String ?? ""
		programId = dictionary["program_id"] as? String ?? ""
		programImage = dictionary["program_image"] as? String ?? ""
		programName = dictionary["program_name"] as? String ?? ""
		programShareUrl = dictionary["program_share_url"] as? String ?? ""
		programWeekCount = dictionary["program_week_count"] as? String ?? ""
        programLevel = dictionary["program_level"] as? String ?? ""
        
        programNotification = dictionary["allow_notification"] as? String ?? ""
        programAccessLevelSelect = dictionary["program_access_level_select"] as? String ?? ""
        programNewsFeed = dictionary["is_featured"] as? String ?? ""
        programAllowedUserId = dictionary["allowed_users"] as? String ?? ""
        programAllowedUsersName = dictionary["allowedUsersName"] as? String ?? ""


        if let favStatus = dictionary["is_program_favourite"] as? String , favStatus == "1"{
            programFavStatus = true
        }else if  let dietFavStat = dictionary["is_program_favourite"] as? Bool{
            programFavStatus = dietFavStat
        }else{
            programFavStatus = false
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if isProgramFavourite != nil{
			dictionary["is_program_favourite"] = isProgramFavourite
		}
		if programAccessLevel != nil{
			dictionary["program_access_level"] = programAccessLevel
		}
		if programAmount != nil{
			dictionary["program_amount"] = programAmount
		}
		if programDescription != nil{
			dictionary["program_description"] = programDescription
		}
		if programEquipments != nil{
			dictionary["program_equipments"] = programEquipments
		}
        if programEquipmentMasterName != nil{
            dictionary["equipmentMasterName"] = programEquipmentMasterName
        }

		if programFullImage != nil{
			dictionary["program_full_image"] = programFullImage
		}
		if programGoodFor != nil{
			dictionary["program_good_for"] = programGoodFor
		}
        if programGoodforMasterName != nil{
            dictionary["goodforMasterName"] = programGoodforMasterName
        }
		if programId != nil{
			dictionary["program_id"] = programId
		}
		if programImage != nil{
			dictionary["program_image"] = programImage
		}
		if programName != nil{
			dictionary["program_name"] = programName
		}
        if programAccessLevelSelect != nil{
            dictionary["program_access_level_select"] = programAccessLevelSelect
        }
        if programNewsFeed != nil{
            dictionary["is_featured"] = programNewsFeed
        }
        if programAllowedUserId != nil{
            dictionary["allowed_users"] = programAllowedUserId
        }
        if programAllowedUsersName != nil{
            dictionary["allowedUsersName"] = programAllowedUsersName
        }
        if programNotification != nil{
            dictionary["allow_notification"] = programNotification
        }
        
		if programShareUrl != nil{
			dictionary["program_share_url"] = programShareUrl
		}
		if programWeekCount != nil{
			dictionary["program_week_count"] = programWeekCount
		}
        if programFavStatus != nil{
            dictionary["is_program_favourite"] = programFavStatus
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         isProgramFavourite = aDecoder.decodeObject(forKey: "is_program_favourite") as? String
         programAccessLevel = aDecoder.decodeObject(forKey: "program_access_level") as? String
         programAmount = aDecoder.decodeObject(forKey: "program_amount") as? String
         programDescription = aDecoder.decodeObject(forKey: "program_description") as? String
         programEquipments = aDecoder.decodeObject(forKey: "program_equipments") as? String
         programEquipmentMasterName = aDecoder.decodeObject(forKey: "equipmentMasterName") as? String


         programFullImage = aDecoder.decodeObject(forKey: "program_full_image") as? String
         programGoodFor = aDecoder.decodeObject(forKey: "program_good_for") as? String
         programGoodforMasterName = aDecoder.decodeObject(forKey: "goodforMasterName") as? String
         programId = aDecoder.decodeObject(forKey: "program_id") as? String
         programImage = aDecoder.decodeObject(forKey: "program_image") as? String
         programName = aDecoder.decodeObject(forKey: "program_name") as? String
        
        programNotification = aDecoder.decodeObject(forKey: "allow_notification") as? String
        programAccessLevelSelect = aDecoder.decodeObject(forKey: "program_access_level_select") as? String
        programNewsFeed = aDecoder.decodeObject(forKey: "is_featured") as? String
        programAllowedUserId = aDecoder.decodeObject(forKey: "allowed_users") as? String
        programAllowedUsersName = aDecoder.decodeObject(forKey: "allowedUsersName") as? String

         programShareUrl = aDecoder.decodeObject(forKey: "program_share_url") as? String
         programWeekCount = aDecoder.decodeObject(forKey: "program_week_count") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if isProgramFavourite != nil{
			aCoder.encode(isProgramFavourite, forKey: "is_program_favourite")
		}
		if programAccessLevel != nil{
			aCoder.encode(programAccessLevel, forKey: "program_access_level")
		}
		if programAmount != nil{
			aCoder.encode(programAmount, forKey: "program_amount")
		}
		if programDescription != nil{
			aCoder.encode(programDescription, forKey: "program_description")
		}
		if programEquipments != nil{
			aCoder.encode(programEquipments, forKey: "program_equipments")
		}
        
        if programEquipmentMasterName != nil{
            aCoder.encode(programEquipmentMasterName, forKey: "equipmentMasterName")
        }
		if programFullImage != nil{
			aCoder.encode(programFullImage, forKey: "program_full_image")
		}
		if programGoodFor != nil{
			aCoder.encode(programGoodFor, forKey: "program_good_for")
		}
        if programGoodforMasterName != nil{
            aCoder.encode(programGoodforMasterName, forKey: "goodforMasterName")
        }
		if programId != nil{
			aCoder.encode(programId, forKey: "program_id")
		}
		if programImage != nil{
			aCoder.encode(programImage, forKey: "program_image")
		}
		if programName != nil{
			aCoder.encode(programName, forKey: "program_name")
		}
        if programNotification != nil{
            aCoder.encode(programNotification, forKey: "allow_notification")
        }
        if programAccessLevelSelect != nil{
            aCoder.encode(programAccessLevelSelect, forKey: "program_access_level_select")
        }
        if programNewsFeed != nil{
            aCoder.encode(programNewsFeed, forKey: "is_featured")
        }
        if programAllowedUserId != nil{
            aCoder.encode(programAllowedUserId, forKey: "allowed_users")
        }
        if programAllowedUsersName != nil{
            aCoder.encode(programAllowedUsersName, forKey: "allowedUsersName")
        }
		if programShareUrl != nil{
			aCoder.encode(programShareUrl, forKey: "program_share_url")
		}
		if programWeekCount != nil{
			aCoder.encode(programWeekCount, forKey: "program_week_count")
		}

	}

}
