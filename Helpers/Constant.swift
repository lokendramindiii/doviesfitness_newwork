//
//  Constant.swift
//
//  Copyright © 2017 CompanyName. All rights reserved.
//

import Foundation
import UIKit

let APP_DELEGATE = UIApplication.shared.delegate as! AppDelegate

let APP_NAME = "Time Text"


let IS_IPHONE = UI_USER_INTERFACE_IDIOM() == .phone
let IS_IPHONE_4 = IS_IPHONE && UIScreen.main.bounds.size.height == 480.0
let IS_IPHONE_5 = IS_IPHONE && UIScreen.main.bounds.size.height == 568.0
let IS_IPHONE_6 = IS_IPHONE && UIScreen.main.bounds.size.height == 667.0
let IS_IPHONE_6PLUS = IS_IPHONE && UIScreen.main.nativeScale == 3.0
let IS_IPHONE_6_PLUS = IS_IPHONE && UIScreen.main.bounds.size.height == 736.0

let IS_IPHONE_X = IS_IPHONE && UIScreen.main.bounds.size.height == 812.0 || IS_IPHONE && UIScreen.main.bounds.size.height == 896.0

let IS_IPHONE_XSMAX_XR   =   (IS_IPHONE && UIScreen.main.bounds.size.height == 896.0)

let IS_RETINA = UIScreen.main.scale == 2.0

var deviceToken: String? {
	get {
		return UserDefaults.standard.value(forKey: UserDefaultsKey.deviceToken) as? String
	}
}

let kDeviceType = "ios"

let MAIN_FRAME : CGRect = UIScreen.main.bounds

let SCREEN_WIDTH = UIScreen.main.bounds.size.width
let SCREEN_HEIGHT = UIScreen.main.bounds.size.height
let SCREEN_WIDTH_RATIO = UIScreen.main.bounds.size.width/320
let SCREEN_HEIGHT_RATIO = UIScreen.main.bounds.size.height/568

let DEVICE_LANGUAGE = (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode)!

//let DEVICE_COUNRTY = (Locale.current as NSLocale).object(forKey: NSLocale.Key.county)!
//
//let DEVICE_COUNRTY_CODE = (Locale.current as NSLocale).object(forKey: NSLocale.Key.languageCode)!

let FONT_ROKKITT_REGULAR = "Rokkitt-Regular"
let FONT_AYUTHAYA_REGULAR = "Ayuthaya"

let kAppNavigationColor = UIColor.white
let kAppNavigationColor_Settings = UIColor.colorWithRGB(r: 199.0, g: 235.0, b: 160.0, alpha: 0.45)//colorWithRGB(r: 199.0, g: 235.0, b: 160.0)

public enum Storyboard: String {
	case NewsFeed
    case Login
	case Main
	case WorkOut
	case DietPlan
	case Programs
	case MyPlan
	case SidePanel
	public func instantiate<VC: UIViewController>(_ viewController: VC.Type) -> VC {
		guard let vc = UIStoryboard(name: self.rawValue, bundle:nil).instantiateViewController(withIdentifier: VC.storyboardIdentifier) as? VC
		else{
			fatalError("Couldn't instantiate \(VC.storyboardIdentifier) from \(self.rawValue)")
		}
		return vc
	}
    func storyboard() -> UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
}

struct UserDefaultsKey {
	static let deviceToken = "device_token"
	static let authToken = "auth_token"
	static let logedInUser = "logedInUser"
    static let InstagramAccessToken = "InstagramAccessToken"
    static let userData = "userData"
    static let filterData = "filterData"
    static let filterDate = "filterDate"
    static let medicalConditions = "medicalConditions"
    static let allowNotifications = "allowNotifications"
    static let logoutId = "logoutId"
    
    // Lokendra - userdefult set
    static let iosPackageId = "iosPakageId"
    static let packageMasterId = "packageMasterId"
    static let productPurchased = "productPurchased"
    static let customerIdbyPayment = "customerIdbyPayment"

}


// Segues
struct SegueIdentifiers{
    static let createAccountFromLogin = "segCreateAccount"
    static let buildPlan = "pushToBuildPlan"
}

struct AppInfo {
    static let webSiteUrl = "http://doviesworkout.com"
    static let twitterConsumerKey = "q2LMTHwQQP51dcziF2ZXrwQP4"
    static let twitterConsumerSecret = "xm6BY07OqvIEHAsBUxbwiWQyrFwKv5CHzpjO9YhGCLg7yiQ6WR"
    static let placeholderImage = UIImage(named: "img_placeholder")
    static let placeholderImageRestDay = UIImage(named: "img_placeholder_Rest")

    static let userPlaceHolder = UIImage(named: "profile_placeHolder")
    static let placeholderLandscapeImage = UIImage(named: "img_placeholderpotr")
    
}

struct HeightConstants {
    static let baseCollectionViewFooterHeight : CGFloat = 40.0
}

struct StaticUrls {
    static let doviesFitness = "https://www.doviesfitness.com/"
    //static let HiddenBrains = "https://www.hiddenbrains.com"
    static let doviesTwitter = "https://twitter.com/Doviesfitness"
    static let doviesInstagram = "https://www.instagram.com/Doviesfitness"
    static let doviesFacebook = "https://www.facebook.com/Doviesfitness"
    static let iTunesRedirectLink = "itms://itunes.apple.com/us/app/apple-store/id1370793122?mt=8"
}

struct StaticPickerData {
    
    static let arrHeightInImperial = [["3'", "4'","5'","6'","7'"],["0\"", "1\"","2\"","3\"","4\"","5\"","6\"","7\"","8\"","9\"","10\"","11\""]]
    
    static func getArrHeightInMetrics()->[String]{
        var heights = [String]()
        for height in 91...242{
            heights.append("\(height) cm")
        }
        return heights
    }
    
    static func getArrHeightInCms()->[String]{
        var heights = [String]()
        for height in 91...242{
            heights.append("\(height)")
        }
        return heights
    }
    static let medicalConditions = ["Diabets","Thyroid","PCOS","Cholestrol", "Physical Injury", "Hypertension"]
    
    static func getArrHeightInFeet()->[String]{
        var feets = [String]()
        for f in 3...7{
            feets.append("\(f)")
        }
        return feets
    }
    
    static func getArrHeightInInches()->[String]{
        var feets = [String]()
        for f in 1...11{
            feets.append("\(f)")
        }
        return feets
    }
    
    
}

