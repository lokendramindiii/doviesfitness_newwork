//
//  WorkOutExerciseDescription_Detail.swift
//  Dovies
//
//  Created by Mindiii on 10/4/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class WorkOutExerciseDescription_Detail: UIViewController ,UITextViewDelegate {
    @IBOutlet var IBlblCreator: UILabel!
    @IBOutlet var IBlblContent: UILabel!

    @IBOutlet var detailTextView: UITextView!
    @IBOutlet var IBView: UIView!

    var exerciseDetails: ExerciseDetails?
    var modelWorkoutDetail: ModelWorkoutDetail?
    var workoutExerciseList: ModelWorkoutExerciseList?
    var faqModel : FaqModel?
    var workoutLogModel : ModelWorkoutList?
    var modelWorkoutList: ModelWorkoutList?
    
    var Str_CreateWorkoutFlage = ""
    
    @IBOutlet weak var constraintTextviewbottom : NSLayoutConstraint!
    @IBOutlet weak var constintHeight : NSLayoutConstraint!
    
    var isForWorkOut:(() -> ())?
    
    //MARK: ViewLifeCycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = false
    self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        IBView.layer.masksToBounds = true
        IBView.layer.cornerRadius = 20.0
        
        
        
        detailTextView.isEditable = false
        self.addCancelBackButton()
        
        if Str_CreateWorkoutFlage == "CreateWorkout"
        {
            IBlblCreator.text = "INFORMATION"
            
            let HtmlconvertDescription = "<p>Welcome to workout creation.<br>This page is going to explain how to create a workout.<br>Every star sign ( * ) needs to be filled.</p><p><strong>PHOTO</strong><br>You add a picture to give the workout details nice look.</p><p><strong>WORKOUT NAME</strong><br>You add the name so you can differentiate between other workouts.</p><p><strong>LEVEL</strong><br>Select the type of level you are creating. So if you send it to someone, they will know how challenging or easy the workout is.</p><p><strong>OVERVIEW</strong><br>This field is where you can write more information about the workout you are creating. You can add in additional details regarding equipment and even writing a personal message if you are sending this to someone. For personal trainers, this field is good to add any other suggestions and alternative equipment for clients. Also, you can write your weights you going to use in this field as well. So you can track your equipment weights during lifting.</p><p><strong>GOOD FOR</strong><br>There is a list of what most workouts are good for. You can select what the workout you are creating is good for so that if you share it with your friends, family or clients, they will have an idea right away what the workout is going to do for them.  For example if your workout is good for abs just select abs.</p><p><strong>EQUIPMENT</strong><br>There is a list of workout equipment listed. Select which one you are going to create the workout with. For example, if you have a dumbbell and you want to create a workout with it you can select that and then when you go to the exercise library you can filter the library with all dumbbell workouts. If you don't know the equipment you can leave this part and add all your exercise and then after that you can check the exercise demonstration you have added to your workout to see what equipment is involved and then you can add them to the <strong>EQUIPMENT</strong> section. This is to help others get an idea of what equipment they are going to need for the workout you created.</p><p><strong>DEFAULT SETTING FOR TIME AND REPS</strong><br> The default settings are there to make it easier for you to create your workout faster. We have currently put in a default timer and Reps settings for you. We have set 30 seconds for <strong>TIMER</strong> exercises, 10 reps for <strong>REPETITION</strong> exercise and 30 seconds for the <strong>TIME</strong> to finish that <strong>10 REPS</strong> exercise. Each rep number you chose needs time you going to use to finish it so chose the right time for reps exercises.<br>You can change the settings if you don't like the default setting. If you want to do 1 min for your timer exercise or 12 reps for repetition, you can set it as your default settings. Our setting is set up in a way to help you if you forget to set your own time or reps.  If you don’t change this setting before you add your exercises, you will have our default settings, that means every exercise you add will have 30 seconds for Timer or 10 reps for reps exercises and 30 second time to finish that reps.<br>If you make a mistake in your default settings before you add your exercise don't panic you can go back to the <strong>DEFAULT SETTINGS</strong> and change it then go to the exercise and select <strong>REPS</strong> and then switch it back to <strong>TIME</strong>, the new setting will auto-fill for that exercise. But in this case, you will have to do it manually by selection one exercise at a time. If you add your exercises and you decide to change the default settings, the new setting is  <strong>NOT</strong> going to change all the already added exercises automatically. But any new exercise you add will pick the current settings. All previous exercises you have already added must be changed manually.</p><p>Note that once you add your exercise after setting default time and reps. All the exercises will be select as <strong>TIME</strong>. So if you want to do <strong>REPS</strong>, then you have to select the <strong>REPS.</strong></p><p><strong>REST TIME:</strong><br>Any time you add an exercise, you will see a rest time. If you decide you want to add a rest time after every exercise then you have to add the time manually. You can't set default rest time.</p><p><strong>DELETING AND DUPLICATION</strong><br> Once you add an exercise, you will see a red icon on the thumbnail image. That is the delete icon, and the green icon is for Duplication. If you want to repeat the same exercise you can click the duplication icon, and it will copy the time or reps, and you can move it around.  During workout creation, you can also preview the video as well in case you want to double check the exercise to give it a correct time or reps. You can also press and hold on exercise to move it up or down.</p><p><strong>UNDERSTANDING  REP TIME</strong><br>Each rep time you set will still play as a timer during the player mode, and after the time you set to finish the reps chosen is done, it will go to next exercise. So make sure you set a time you believe you can complete the specific exercise. If you think you can do 10 reps of squats in 30 seconds, then select 30 seconds.  Every workout on the app is an interval timer workout. When the time reach, it goes to next automatically. Unless you pause or go back to previews exercise.</p><p><strong>TOTAL WORKOUT TIME</strong><br>All the time you put in your exercise is going to add up to get the total time for the workout you are creating. So if you add 5 exercises without rest time and each exercise is 1 min, your entire workout time will be 5 minutes. You can check your total workout time with the time icon on your right before you save your workout. That will help you know the duration of the workout you are creating. Workout time doesn't add seconds when you save it. So if you create for example 1min 10 seconds, it will save as 1min, but when you are playing your workout, it will play all 1min 10 seconds. We did this to give the <strong>TOTAL TIME</strong> in workout details simple look.  You can check the workout collection and see how the workouts have been created to provide you with an understanding of how to create your own.</p><p>For more information, you can also go to our website <a href=\"http://www.doviesfitness.com\">  <strong>www.doviesfitness.com</strong></a> .</p><p>Tutorial videos will be uploaded on our feed, and also our Instagram page <a href=\"https://www.instagram.com/Doviesfitness\">  <strong>@doviesfitness</strong></a> and  @doviesworkout will show lots of live streaming sections on how to create workouts, plans, and more. You can also contact our support for additional information at <a href=\"mailto:support@doviesfitness.com\"> <strong> support@doviesfitness.com</strong></a></p>"
            
            
            detailTextView.attributedText = "<!DOCTYPE html> <style>html * {font-size: 11.0pt !important; font-family: TamilSangamMN !important;line-height:19.0pt;} p{color: #8C8C8C;} p strong, p b{color: #242424 !important;} </style><html><body>\(HtmlconvertDescription )</body></html>".convertHtml()
            
            detailTextView.linkTextAttributes = [NSAttributedStringKey.foregroundColor.rawValue : UIColor.colorWithRGB(r: 36.0, g: 36.0, b: 36.0)] as [String:Any]
            
        }
        
        
        if workoutExerciseList != nil{
            self.navigationItem.title = workoutExerciseList?.workoutExerciseName
            if let desc = workoutExerciseList?.workoutExerciseDescription, !desc.isEmpty
            {
                detailTextView.text = desc
            }
            else
            {
               detailTextView.text = "No description available"
            }
            IBlblCreator.text = ""
        }
        else if modelWorkoutDetail != nil{
            self.navigationItem.title = modelWorkoutDetail?.workoutName
            detailTextView.text = modelWorkoutDetail?.workoutDescription

            IBlblCreator.text = modelWorkoutDetail?.creatorName
        }
        else if exerciseDetails != nil{
            self.navigationItem.title = exerciseDetails?.exerciseName
            
            if (exerciseDetails?.exerciseDescription.isEmpty)!{
                detailTextView.text = "No description available"
            }
            else{
               detailTextView.text = exerciseDetails?.exerciseDescription
            }
            IBlblCreator.text = ""
        }
        else if modelWorkoutList != nil{
            self.navigationItem.title = modelWorkoutList?.workoutName
            
            if (modelWorkoutList?.workoutDescription.isEmpty)!{
                detailTextView.text = "No description available"
            }
            else{
                detailTextView.text = modelWorkoutList?.workoutDescription
            }
            IBlblCreator.text = ""
        }
        
        if let model = faqModel{
            detailTextView.text = model.faqAnswer
        }
        if let model = workoutLogModel{
            IBlblCreator.text = "Notes"
            if model.workoutNote.isEmpty{
                detailTextView.text = "No description available"
            }else{
                 detailTextView.text = model.workoutNote
            }
        }
        
        let strTitle = IBlblCreator.text
        IBlblCreator.text = strTitle?.uppercased()
        
        IBlblContent.text = detailTextView.text
        IBlblContent.makeSpacingWithLeftAlignmentPopup(lineSpace: 9.0, font: UIFont.tamilRegular(size: 15.8), color: #colorLiteral(red: 0.5490196078, green: 0.5490196078, blue: 0.5490196078, alpha: 1))
        
        
        if Str_CreateWorkoutFlage != "CreateWorkout"
        {
        detailTextView.makeSpacingWithleftAlignmentTextview(lineSpace: 9.0, font:UIFont.tamilRegular(size: 15.8), color:#colorLiteral(red: 0.5490196078, green: 0.5490196078, blue: 0.5490196078, alpha: 1))
        }

        let attributetHeight =   detailTextView.text.heightWithConstrainedWidthLineSpacing(width:ScreenSize.width - 40, lineSpace:9.0 , font:UIFont.tamilRegular(size: 15.8) , Str_String:detailTextView.text)
       print("ATTRIBUTED HEIGHT \(attributetHeight)")
        
        
        if attributetHeight > 400
        {
            constraintTextviewbottom.isActive = true
            IBlblContent.isHidden = true
            constintHeight.constant = 400
            detailTextView.frame.size.height = 400
            detailTextView.isScrollEnabled = true
        }
        else
        {
            constraintTextviewbottom.isActive = false
            IBlblContent.isHidden = false
            detailTextView.isHidden = true
        }
        
        self.view.addTapGesture(target: self, action: #selector(self.dismissView))

        detailTextView.layoutIfNeeded()
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Instance methods
    
    func addCancelBackButton() {
        self.navigationItem.leftBarButtonItems = GlobalUtility.getNavigationButtonItems(target: self, selector: #selector(self.btnCancelTapped(sender:)), imageName: "icon_close")
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.black
    }
    
    //MARK: IBAction methods
    @objc func dismissView(){
        if Str_CreateWorkoutFlage == "CreateWorkout"
        {
        }
        else
        {
            isForWorkOut?()
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func btnCancelTapped(sender: UIButton) {
        if Str_CreateWorkoutFlage == "CreateWorkout"
        {
        }
        else
        {
            isForWorkOut?()
        }
        _ = self.navigationController?.popViewController(animated: false)
        
    }
    
    @IBAction func closeTapped(_ sender : UIButton){
        if Str_CreateWorkoutFlage == "CreateWorkout"
        {
        }
        else
        {
            isForWorkOut?()
        }
        self.dismiss(animated: true, completion: nil)
    }
   
}

