//
//  BaseViewController.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}

fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l > r
    default:
        return rhs < lhs
    }
}


class BaseViewController: UIViewController
{
    var currentDirection: Int = 0
    var percentDrivenInteractiveTransition: UIPercentDrivenInteractiveTransition!
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    //MARK: ViewLifeCycle methods
    override func viewDidLoad()
    {
        super.viewDidLoad()
    self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        
    }
    
//    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
//        if gestureRecognizer == self.navigationController?.interactivePopGestureRecognizer && (self.view != nil) {
//            return false
//        }else{
//            return true
//        }
//    }
    
    @objc func dismiss(fromGesture gesture: UISwipeGestureRecognizer) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        GlobalUtility.hideActivityIndi(viewContView: self.view)
    }
    
    //MARK: Instance methods
    func inititalUISetUp(){
        
    }
    
    func addNavigationBackButton() {
        self.navigationItem.leftBarButtonItems = GlobalUtility.getNavigationButtonItems(target: self, selector: #selector(self.btnBackTapped(sender:)), imageName: "btn_top_back")
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.black
    }
    
    func addNavigationWhiteBackButton() {
        self.navigationItem.leftBarButtonItems = GlobalUtility.getNavigationButtonItems(target: self, selector: #selector(self.btnBackTapped(sender:)), imageName: "btn_top_backWhite")
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.black
    }
    
    
    
    @objc func btnBackTapped(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func redirectToTab(tabIndex: Int){
        self.tabBarController?.selectedIndex = tabIndex
        if let vc = self.tabBarController?.selectedViewController {
            let viewCont = GlobalUtility.topViewController(withRootViewController: (vc))
            viewCont.navigationController?.popToRootViewController(animated: false)
        }
    }
    
    func addGesture() {
        
        guard navigationController?.viewControllers.count > 1 else {
            return
        }
        
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(BaseViewController.handlePanGesture(_:)))
        self.view.addGestureRecognizer(panGestureRecognizer)
    }
    
    @objc func handlePanGesture(_ panGesture: UIPanGestureRecognizer) {
        
        let percent = max(panGesture.translation(in: view).x, 0) / view.frame.width
        print(percent)
        
        let vel = panGesture.velocity(in: self.view)
        if vel.x > 0 {
            // user dragged towards the right
            print("right")
            //        }
            //        else {
            //            // user dragged towards the left
            //            print("left")
            //        }
            //
            //        if vel.y > 0 {
            //            // user dragged towards the down
            //            print("down")
            //        }
            //        else {
            //            // user dragged towards the up
            //            print("up")
            //        }
            
            switch panGesture.state {
                
            case .began:
                navigationController?.delegate = self
                _ = navigationController?.popViewController(animated: true)
            case .changed:
                if let percentDrivenInteractiveTransition = percentDrivenInteractiveTransition {
                    percentDrivenInteractiveTransition.update(percent)
                }
                
            case .ended:
                let velocity = panGesture.velocity(in: view).x
                
                // Continue if drag more than 50% of screen width or velocity is higher than 1000
                
                if percent > 0.5 || velocity > 1000 {
                    percentDrivenInteractiveTransition.finish()
                } else {
                    percentDrivenInteractiveTransition.cancel()
                    
                }
                
            case .cancelled, .failed:
                percentDrivenInteractiveTransition.cancel()
                
            default:
                break
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension BaseViewController: UIGestureRecognizerDelegate{
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
}

extension BaseViewController: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        return SlideAnimatedTransitioning()
    }
    
    func navigationController(_ navigationController: UINavigationController, interactionControllerFor animationController: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
        
        navigationController.delegate = nil
        
        if panGestureRecognizer.state == .began {
            percentDrivenInteractiveTransition = UIPercentDrivenInteractiveTransition()
            percentDrivenInteractiveTransition.completionCurve = .easeOut
        } else {
            percentDrivenInteractiveTransition = nil
        }
        
        return percentDrivenInteractiveTransition
    }
}

