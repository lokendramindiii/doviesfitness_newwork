//
//  WSURLConstant.swift
//
//  Created by ComapnyName
//

import Foundation
import UIKit

//let BASE_URL = "http://local.configure.it/dovies4254/WS/"
//let BASE_URL = "http://projectspreview.net:1050/WS/"
//let BASE_URL = "https://doviesworkout.projectspreview.net/WS/"
//let BASE_URL = "http://13.59.77.103/WS/"

// old url  http://13.59.77.103/WS/  loks

 // let BASE_URL = "https://www.doviesfitness.com/dovies/WS/"  // live mindiii

      let BASE_URL = "https://www.doviesfitness.com/WS/" // live upload mindiii aap stroe////

 //    let BASE_URL = "https://dev.doviesfitness.com/WS/"

//      let BASE_URL = "https://dev.doviesfitness.com/webservice_v2/"

//Api urls

//com.dovies.doviesapp
//com.dovies.newadmin

//devdoviesfitness

//Doviesfitness
struct WsUrl {
    
  //  static let verifyReceiptUrl         = "https://sandbox.itunes.apple.com/verifyReceipt"
    
    static let verifyReceiptUrl         = "https://buy.itunes.apple.com/verifyReceipt"

	static let login                 	= BASE_URL + "customer_login"
	static let signUp					= BASE_URL + "sign_up"
    static let forgotPassword           = BASE_URL + "forgot_password"
    static let userAvailability         = BASE_URL + "user_availability"
    static let emailAvailability        = BASE_URL + "email_availability"

    static let insgramAPI               = "https://api.instagram.com/oauth/access_token"
    static let dietPlanCategories     = BASE_URL + "diet_plan_categories"
    static let dietPlans     = BASE_URL + "diet_plans"
    static let newsFeed        = BASE_URL + "news_feed"
    static let dietPlanDetails     = BASE_URL + "diet_plan_detail"
    static let programs    = BASE_URL + "programs"
    static let programDetails    = BASE_URL + "program_detail"
    static let likeUnlike    = BASE_URL + "like_unlike"
    static let makeFavourite    = BASE_URL + "make_favorite"
    
    static let likeUnlikeCommentCustom    = BASE_URL + "custom_comment_post"
    
    static let makeActiveInactive    = BASE_URL + "plan_admin_status"
    static let newsFeedDetails    = BASE_URL + "news_feed_detail"
	static let exerciseLibrary	= BASE_URL + "exercise_library"
	static let getNewsFeedComments	= BASE_URL + "get_news_feed_comments"
	static let newsCommentPost	= BASE_URL + "news_comment_post"
    static let filterData    = BASE_URL + "filter_data"
	static let featuredWorkouts = BASE_URL + "featured_workouts"
	static let workout_detail = BASE_URL + "workout_detail"
    static let exerciseDetail = BASE_URL + "exercise_detail"
	static let exerciseListing = BASE_URL + "exercise_listing"
	static let getCustomerFavourites = BASE_URL + "get_customer_favourites"
    static let updateCustomerDetail = BASE_URL + "update_customer_detail"
    static let getCustomerDetail = BASE_URL + "get_customer_detail"
    static let addProgram = BASE_URL + "add_program"
    static let getCustomerWorkouts = BASE_URL + "get_customer_workouts"
	static let getCustomerPlans = BASE_URL + "get_customer_plans"
    static let planDetail = BASE_URL + "plan_detail"
    static let staticPage = BASE_URL + "static_page"
	static let contactDetail = BASE_URL + "contact_detail"
    static let contactUs = BASE_URL + "contact_us"
	static let customerWorkoutLog = BASE_URL + "customer_workout_log"
    static let editProgramDietPlan = BASE_URL + "edit_program_diet_plan"
    static let editProgramDetail = BASE_URL + "edit_program_detail"
	static let purchaseHistory = BASE_URL + "purchase_history"
	static let subscriptionHistory = BASE_URL + "subscription_history"
	static let deleteWorkout = BASE_URL + "delete_workout"
	static let deleteWorkoutLog = BASE_URL + "delete_workout_log"
    static let editProgramWorkout = BASE_URL + "edit_program_workout"
	static let getCustomerDiet = BASE_URL + "get_customer_diet"
    static let updateProgramWorkoutStatus = BASE_URL + "update_program_workout_status"
    static let addWorkout = BASE_URL + "add_workout"
    static let editWorkout = BASE_URL + "edit_workout"
    static let updateCustomerCountry = BASE_URL + "update_customer_country"
    static let addCompleteProgram = BASE_URL + "add_complete_program"
    static let groupWorkoutList = BASE_URL + "group_workout_list"
    static let deletePlan = "delete_plan"
    static let addToMyPlan = "add_to_my_plan"
    static let addToMyDiet = "add_to_my_diet"
    static let addWorkoutFeedback = "add_workout_feedback"
    static let notificationList = "notification_list"
	static let packageList = "package_list"
	static let completePayment = "complete_payment"
    static let subscriptionStatus = "subscription_status"
    static let productPurchaseSuccess = "product_purchase_success"
    static let deleteComment = "delete_comment"
    static let reportComment = "report_news_feed_comment"
    
    static let deleteDietPlan = "delete_my_diet"
    static let getFaq = "get_faq"
    static let editCustomerUnits = "edit_customer_units?"
    static let addToMyWorkout = "add_to_my_workout"
	static let changePassword = BASE_URL + "change_password"
    
    static let Getcustomerlist = BASE_URL + "get_customer_list"
    
    static let GetcustomerlistNew = BASE_URL + "get_customer_list_new"
    
    
    static let Getadminworkouts = BASE_URL + "get_admin_workouts"
    static let Getpublishworkout = BASE_URL + "publish_workout"
    
    static let getNotificationStatus = BASE_URL + "update_notification_status"
    
    static let deleteNotificationComment = "delete_custom_comment"
    static let reportCustomComment = "report_custom_comment"
    static let deleteNotification = "delete_notification"

}

//Api parameters
struct WsParam {
    // itunes password
    static let itunesPassword             = "c7fbfcb09edc473da37834ff09ed64c5"

    static let weightUnit             = "weight_unit"
    static let heightUnit             = "height_unit"

    
    static let count             = "count"
	static let settings 			= "settings"
	static let success 				= "success"
	static let message				= "message"
	static let nextPage		 	    = "next_page"
	static let data 				= "data"
	static let pageIndex    = "page_index"
	static let authCustomerId		= "auth_customer_id"
    static let pageCode        = "page_code"
	static let customerAuthToken = "customer_auth_token"
    static let customerUserName = "customer_user_name"
    
	//Login
	static let email				= "email"
    static let userName                = "user_name"
    static let userId                = "user_id"

	static let password				= "password"
	static let deviceId			= "device_id"
	static let deviceToken			= "device_token"
    static let deviceType           = "device_type"
	static let socialNetwork_type = "social_network_type"
    static let socialNetworkId = "social_network_id"
    // contact use
    
    static let contactName                = "contactName"

    //SignUp
    static let name = "name"
    static let confirmPassword = "confirm_password"
    static let gender = "gender"
    static let height = "height"
    static let weight = "weight"
    static let mobileNumber = "mobile_number"
    static let isdCode = "isd_code"
    static let customerCountryId = "customer_country_id"
	static let countryId = "country_id"
    static let customerCountryName = "customer_country_name"
    static let countryCode = "country_code"
    static let dob = "dob"
    static let email_upadates = "email_upadates"
    static let countryName = "country_name"
    static let userAvailable = "user_name"
    static let emailAvailable = "email"
     // contact us
    static let subject                = "subject"
    static let cutomerId                = "iCustomerId"

    //DietPlan
    static let authCustomerSubscription = "auth_customer_subscription"
    static let dietPlanCategoryId = "diet_plan_category_id"
    static let dietListing = "diet_listing"
    static let dietStaticContent = "diet_static_content"
    static let getStaticContent = "get_static_content"

    // News Feed
    static let featuredNews = "featured_news"
    static let allOtherFeatured = "all_other_then_featured"
    static let dietPlanId = "diet_plan_id"
    
    //NewsFeed comment
    static let getNewsFeedDetail = "get_news_feed_detail"
    static let newsFeedComments = "news_feed_comments"
    
    //Programs
    static let getAllPrograms = "get_all_programs"
    static let getProgramStaticContent = "get_program_static_content"
    static let programId = "program_id"
    static let programName = "program_name"
    static let programLevel = "program_level"
    static let programWeekCount = "program_week_count"
    static let programDescription = "program_description"
    static let programImage = "program_image"
    static let programWorkoutId = "program_workout_id"
    
    //NewsFeed
    static let newsId = "news_id"
    static let moduleId = "module_id"
    static let moduleName = "module_name"
	
	//Comments
	static let recLimit = "rec_limit"
	static let commentText = "comment_text"
	static let commentId = "comment_id"
	
	//Workout
	static let workoutId = "workout_id"
	static let workoutDetail = "workout_detail"
	static let workoutExerciseList = "workout_exercise_list"
	static let type	= "type"
	static let featured = "Featured"
    static let feedbackStatus = "feedback_status"
	
	//Exercise
	static let filterCategory = "filter_category"
	static let exerciseId = "exercise_id"
	static let moduleType = "module_type"
    static let filterKeyword = "filter_keyword"
	//update profile
	static let profile_pic = "profile_pic"
    
    //Create Workout
    
    static let workout_name = "workout_name"
    static let workout_image = "workout_image"
    static let workout_level = "workout_level"
    static let workout_good_for = "workout_good_for"
    static let workout_equipment = "workout_equipment"
    static let workout_description = "workout_description"
    static let parent_workout_id = "parent_workout_id"
    static let workout_time = "workout_time"
    static let note = "note"
    static let workout_images = "workout_images"
    static let workout_image_date = "workout_image_date"
    
    static let workout_addedBy = "addedBy"
    static let workout_workoutGroupId = "workoutGroupId"
    static let workout_allowedUsers = "allowedUsers"
    static let workout_accessLevel = "accessLevel"
    static let workout_allowNotification = "allowNotification"
    static let workout_isFeatured = "isFeatured"
    static let workout_addedById = "addedById"
    static let workout_collectionId = "workout_collection_id"

    //Subscription
    static let isSubscribed = "is_subscribed"
    static let title = "title"
    
    static let productId = "product_id"
    static let productType = "product_type"
    static let transactionDetail = "transaction_detail"
    static let quantity = "quantity"
    static let transactionId = "transactionId"
    
    static let customerUnits = "customer_units"
    static let encodedKey = "encodedKey"
    
    // notification status
    static let notificationId = "notificationId"
    
    // CustomNotification
    static let connectionId = "connectionId"
    static let like = "like"
    static let comment = "comment"
    static let comment_Id = "comment_id"

    static let reportText = "report_text"
    static let customNotificationId = "custom_notification_id"
    static let notificaitonId = "notification_id"
    
    // Get user
    
    static let searchQuery = "search_query"
    static let  allowedUserId = "allowed_user_id"

    
    
}

//API Headers
struct WsHeader {
    static let appVersion = "APIVERSION"
    static let apiKey = "APIKEY"
    static let authToken = "AUTHTOKEN"
    static let apiKeyValue = "HBDEV"
    static let appVersionValue = "1"
}

struct ModuleName
{
    static let newsFeed = "NEWSFEED"
    static let dietPlan = "DIET_PLAN"
    static let workout = "WORKOUT"
    static let exercise = "EXERCISE"
    static let program = "PROGRAM"
    static let custom = "CUSTOM"
}

// loks add mindii admin username
struct DoviesAdmin
{
      static let username = "doviesfitness"
}






