//
//  Workout+CoreDataProperties.swift
//  
//
//  Created by Neel Shah on 01/05/18.
//
//

import Foundation
import CoreData


extension Workout {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Workout> {
        return NSFetchRequest<Workout>(entityName: "Workout")
    }

    @NSManaged public var workoutCategory: String?
    @NSManaged public var workoutExerciseCount: String?
    @NSManaged public var workoutFavStatus: String?
    @NSManaged public var workoutGroupDescription: String?
    @NSManaged public var workoutGroupId: String?
    @NSManaged public var workoutGroupName: String?
    @NSManaged public var workoutId: String?
    @NSManaged public var workoutImage: String?
    @NSManaged public var workoutLevel: String?
    @NSManaged public var workoutName: String?
    @NSManaged public var workoutShareUrl: String?
    @NSManaged public var workoutTime: String?
    @NSManaged public var workoutTotalWorkTime: String?
    @NSManaged public var creatorId: String?
    @NSManaged public var creatorName: String?
    @NSManaged public var creatorProfileImage: String?
    @NSManaged public var workoutDescription: String?
    @NSManaged public var workoutEquipment: String?
    @NSManaged public var workoutFullImage: String?
    @NSManaged public var workoutGoodFor: String?
    @NSManaged public var workoutTotalTime: String?
    @NSManaged public var workoutZipFile: String?
    @NSManaged public var exercise: NSSet?
    @NSManaged public var workoutCreatedDate: Date?
    @NSManaged public var workoutCreatedBy: String?

}

// MARK: Generated accessors for exercise
extension Workout {

    @objc(addExerciseObject:)
    @NSManaged public func addToExercise(_ value: Exercise)

    @objc(removeExerciseObject:)
    @NSManaged public func removeFromExercise(_ value: Exercise)

    @objc(addExercise:)
    @NSManaged public func addToExercise(_ values: NSSet)

    @objc(removeExercise:)
    @NSManaged public func removeFromExercise(_ values: NSSet)

}
