//
//  CustomSideMenuController.swift
//  Dovies
//
//  Created by Neel Shah on 16/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import SideMenuController

class CustomSideMenuController: SideMenuController {
	
	required init?(coder aDecoder: NSCoder) {
		SideMenuController.preferences.drawing.menuButtonImage = UIImage(named: "btn_menuWhite")

        
		SideMenuController.preferences.drawing.sidePanelPosition = .overCenterPanelLeft
        let w = 200 * CGRect.widthRatio
		SideMenuController.preferences.drawing.sidePanelWidth = w
		SideMenuController.preferences.drawing.centerPanelShadow = true
        SideMenuController.preferences.drawing.centerPanelOverlayColor = UIColor(hue:0.15, saturation:0.21, brightness:0.17, alpha:0.1)
		SideMenuController.preferences.animating.statusBarBehaviour = .showUnderlay
        SideMenuController.preferences.interaction.swipingEnabled = false
        
		super.init(coder: aDecoder)
	}

    override func viewDidLoad() {
        super.viewDidLoad()

		performSegue(withIdentifier: "TabBarController", sender: nil)
		performSegue(withIdentifier: "containSideMenu", sender: nil)
    }

    override func didReceiveMemoryWarning() {   
        super.didReceiveMemoryWarning()
    }
}
