//
//	WorkoutDetail.swift
//
//	Create by hb on 12/3/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class ModelWorkoutDetail : NSObject, NSCoding{

	var creatorName : String!
	var creatorProfileImage : String!
	var isFeatured : String!
	var workoutAccessLevel : String!
    var workoutSelectAccessLevel : String!
    var workoutAllowedNotification : String!
    var workoutAllowedUsername : String!
    var workoutAllowedUsernameID : String!

	var workoutAmount : String!
	var workoutAmountDisplay : String!
	var workoutDescription : String!
	var workoutEquipment : String!
	var workoutExerciseCount : String!
	var workoutFullImage : String!
	var workoutGoodFor : String!
	var workoutImage : String!
	var workoutLevel : String!
	var workoutName : String!
	var workoutShareUrl : String!

	var workoutTotalTime : String!
	var creatorId: String!
	var workoutExerciseList : [ModelWorkoutExerciseList]!
	var workoutZipFile : String!
    var workoutEquipmentIds : String!
    var workoutGoodForIds  : String!
    var workoutCreatedBy:  String!
	var workoutId: String!
	var progress: CGFloat!
	var sDownloadLabel: String!
    
    var isWorkoutContainPremiumExercise : Bool{
        let arrPremiumEx = self.workoutExerciseList.filter { $0.workoutAccessLevel == "LOCK"}
        
        let isPrem =  arrPremiumEx.count > 0 ? true : false
        return isPrem
    }
    
	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		creatorName = dictionary["creator_name"] as? String ?? ""
		creatorProfileImage = dictionary["creator_profile_image"] as? String ?? ""
		isFeatured = dictionary["is_featured"] as? String ?? ""
        
        if  DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName {
            workoutAccessLevel = "OPEN"
        }
        else
        {
            workoutAccessLevel = dictionary["workout_access_level"] as? String ?? ""
        }
		workoutAmount = dictionary["workout_amount"] as? String ?? ""
		workoutAmountDisplay = dictionary["workout_amount_display"] as? String ?? ""
		workoutDescription = dictionary["workout_description"] as? String ?? ""
		workoutEquipment = dictionary["workout_equipment"] as? String ?? ""
		workoutExerciseCount = dictionary["workout_exercise_count"] as? String ?? ""
		workoutFullImage = dictionary["workout_full_image"] as? String ?? ""
		workoutGoodFor = dictionary["workout_good_for"] as? String ?? ""
		workoutImage = dictionary["workout_image"] as? String ?? ""
		workoutLevel = dictionary["workout_level"] as? String ?? ""
        
        workoutAllowedNotification = dictionary["allow_notification"] as? String ?? ""
        workoutAllowedUsername = dictionary["allowedUsersName"] as? String ?? ""
        workoutAllowedUsernameID = dictionary["allowed_users"] as? String ?? ""


        workoutSelectAccessLevel = dictionary["access_level"] as? String ?? ""
		workoutName = dictionary["workout_name"] as? String ?? ""
		workoutShareUrl = dictionary["workout_share_url"] as? String ?? ""
		workoutTotalTime = dictionary["workout_total_time"] as? String ?? ""
		creatorId = dictionary["creator_id"] as? String ?? ""
		workoutZipFile = dictionary["workout_zip_file"] as? String ?? ""
        workoutEquipmentIds =  dictionary["workout_equipment_ids"] as? String ?? ""
        workoutGoodForIds =  dictionary["workout_good_for_ids"] as? String ?? ""
        workoutCreatedBy =  dictionary["workout_created_by"] as? String ?? ""
        workoutId = dictionary["workout_id"] as? String ?? ""
		
		workoutExerciseList = [ModelWorkoutExerciseList]()
        
		if let workoutExerciseListArray = dictionary["workout_exercise_list"] as? [[String:Any]]{
			for dic in workoutExerciseListArray{
				let value = ModelWorkoutExerciseList(fromDictionary: dic)
				workoutExerciseList.append(value)
			}
		}
        print("workoutExerciseList = \(workoutExerciseList)")
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if creatorName != nil{
			dictionary["creator_name"] = creatorName
		}
		if creatorProfileImage != nil{
			dictionary["creator_profile_image"] = creatorProfileImage
		}
		if isFeatured != nil{
			dictionary["is_featured"] = isFeatured
		}
		if workoutAccessLevel != nil{
			dictionary["workout_access_level"] = workoutAccessLevel
		}
		if workoutAmount != nil{
			dictionary["workout_amount"] = workoutAmount
		}
		if workoutAmountDisplay != nil{
			dictionary["workout_amount_display"] = workoutAmountDisplay
		}
		if workoutDescription != nil{
			dictionary["workout_description"] = workoutDescription
		}
		if workoutEquipment != nil{
			dictionary["workout_equipment"] = workoutEquipment
		}
		if workoutExerciseCount != nil{
			dictionary["workout_exercise_count"] = workoutExerciseCount
		}
		if workoutFullImage != nil{
			dictionary["workout_full_image"] = workoutFullImage
		}
		if workoutGoodFor != nil{
			dictionary["workout_good_for"] = workoutGoodFor
		}
		if workoutImage != nil{
			dictionary["workout_image"] = workoutImage
		}
		if workoutLevel != nil{
			dictionary["workout_level"] = workoutLevel
		}

        if workoutAllowedNotification != nil{
            dictionary["allow_notification"] = workoutSelectAccessLevel
        }
        if workoutAllowedUsername != nil{
            dictionary["allowedUsersName"] = workoutAllowedUsername
        }
        if workoutAllowedUsernameID != nil{
            dictionary["allowed_users"] = workoutAllowedUsernameID
        }

        if workoutSelectAccessLevel != nil{
            dictionary["access_level"] = workoutSelectAccessLevel
        }

		if workoutName != nil{
			dictionary["workout_name"] = workoutName
		}
		if workoutShareUrl != nil{
			dictionary["workout_share_url"] = workoutShareUrl
		}
		if workoutTotalTime != nil{
			dictionary["workout_total_time"] = workoutTotalTime
		}
		if workoutZipFile != nil{
			dictionary["workout_zip_file"] = workoutZipFile
		}
        if workoutGoodForIds != nil{
            dictionary["workout_good_for_ids"] = workoutGoodForIds
        }
        if workoutEquipmentIds != nil{
            dictionary["workout_equipment_ids"] = workoutEquipmentIds
        }
		if workoutExerciseList != nil{
			var dictionaryElements = [[String:Any]]()
			for workoutExerciseListElement in workoutExerciseList {
				dictionaryElements.append(workoutExerciseListElement.toDictionary())
			}
			dictionary["workout_exercise_list"] = dictionaryElements
		}
		return dictionary
	}
	
	@objc required init(modelWorkout: Workout)
	{
		creatorName = modelWorkout.creatorName
		creatorProfileImage = modelWorkout.creatorProfileImage
		workoutDescription = modelWorkout.workoutDescription
		workoutEquipment = modelWorkout.workoutEquipment
		workoutExerciseCount = modelWorkout.workoutExerciseCount
		workoutFullImage = modelWorkout.workoutFullImage
		workoutGoodFor = modelWorkout.workoutGoodFor
		workoutImage = modelWorkout.workoutImage
		workoutLevel = modelWorkout.workoutLevel

		workoutName = modelWorkout.workoutName
        workoutShareUrl = modelWorkout.workoutShareUrl
        workoutTotalTime = modelWorkout.workoutTotalTime
		creatorId = modelWorkout.creatorId
		workoutZipFile = modelWorkout.workoutZipFile
		workoutId = modelWorkout.workoutId
        workoutCreatedBy = modelWorkout.workoutCreatedBy

		workoutExerciseList = [ModelWorkoutExerciseList]()
		if let workoutExerciseListArray = modelWorkout.exercise{
			for dic in workoutExerciseListArray{
				let value = ModelWorkoutExerciseList(modelExercise: dic as! Exercise)
				workoutExerciseList.append(value)
			}
		}
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         creatorName = aDecoder.decodeObject(forKey: "creator_name") as? String
         creatorProfileImage = aDecoder.decodeObject(forKey: "creator_profile_image") as? String
         isFeatured = aDecoder.decodeObject(forKey: "is_featured") as? String
    
        if  DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName {
            workoutAccessLevel = "OPEN"
        }
        else
        {
            workoutAccessLevel = aDecoder.decodeObject(forKey: "workout_access_level") as? String
        }
         workoutAmount = aDecoder.decodeObject(forKey: "workout_amount") as? String
         workoutAmountDisplay = aDecoder.decodeObject(forKey: "workout_amount_display") as? String
         workoutDescription = aDecoder.decodeObject(forKey: "workout_description") as? String
         workoutEquipment = aDecoder.decodeObject(forKey: "workout_equipment") as? String
         workoutExerciseCount = aDecoder.decodeObject(forKey: "workout_exercise_count") as? String
         workoutFullImage = aDecoder.decodeObject(forKey: "workout_full_image") as? String
         workoutGoodFor = aDecoder.decodeObject(forKey: "workout_good_for") as? String
         workoutImage = aDecoder.decodeObject(forKey: "workout_image") as? String
         workoutLevel = aDecoder.decodeObject(forKey: "workout_level") as? String
        
        workoutAllowedNotification = aDecoder.decodeObject(forKey: "allow_notification") as? String
        workoutAllowedUsername = aDecoder.decodeObject(forKey: "allowedUsersName") as? String
        workoutAllowedUsernameID = aDecoder.decodeObject(forKey: "allowed_users") as? String



        workoutSelectAccessLevel = aDecoder.decodeObject(forKey: "access_level") as? String


         workoutName = aDecoder.decodeObject(forKey: "workout_name") as? String
         workoutShareUrl = (aDecoder.decodeObject(forKey: "workout_share_url") as? String)
         workoutTotalTime = aDecoder.decodeObject(forKey: "workout_total_time") as? String
		workoutExerciseList = aDecoder.decodeObject(forKey :"workout_exercise_list") as? [ModelWorkoutExerciseList]
		workoutZipFile = aDecoder.decodeObject(forKey: "workout_zip_file") as? String
        workoutGoodForIds = aDecoder.decodeObject(forKey: "workout_good_for_ids") as? String
        workoutEquipmentIds = aDecoder.decodeObject(forKey: "workout_equipment_ids") as? String
        
        
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if creatorName != nil{
			aCoder.encode(creatorName, forKey: "creator_name")
		}
		if creatorProfileImage != nil{
			aCoder.encode(creatorProfileImage, forKey: "creator_profile_image")
		}
		if isFeatured != nil{
			aCoder.encode(isFeatured, forKey: "is_featured")
		}
		if workoutAccessLevel != nil{
			aCoder.encode(workoutAccessLevel, forKey: "workout_access_level")
		}
		if workoutAmount != nil{
			aCoder.encode(workoutAmount, forKey: "workout_amount")
		}
		if workoutAmountDisplay != nil{
			aCoder.encode(workoutAmountDisplay, forKey: "workout_amount_display")
		}
		if workoutDescription != nil{
			aCoder.encode(workoutDescription, forKey: "workout_description")
		}
		if workoutEquipment != nil{
			aCoder.encode(workoutEquipment, forKey: "workout_equipment")
		}
		if workoutExerciseCount != nil{
			aCoder.encode(workoutExerciseCount, forKey: "workout_exercise_count")
		}
		if workoutFullImage != nil{
			aCoder.encode(workoutFullImage, forKey: "workout_full_image")
		}
		if workoutGoodFor != nil{
			aCoder.encode(workoutGoodFor, forKey: "workout_good_for")
		}
		if workoutImage != nil{
			aCoder.encode(workoutImage, forKey: "workout_image")
		}
		if workoutLevel != nil{
			aCoder.encode(workoutLevel, forKey: "workout_level")
		}
        if workoutAllowedNotification != nil{
            aCoder.encode(workoutAllowedNotification, forKey: "allow_notification")
        }
        if workoutAllowedUsername != nil{
            aCoder.encode(workoutAllowedUsername, forKey: "allowedUsersName")
        }
        if workoutAllowedUsernameID != nil{
            aCoder.encode(workoutAllowedUsernameID, forKey: "allowed_users")
        }

        if workoutSelectAccessLevel != nil{
            aCoder.encode(workoutSelectAccessLevel, forKey: "access_level")
        }

		if workoutName != nil{
			aCoder.encode(workoutName, forKey: "workout_name")
		}
		if workoutShareUrl != nil{
			aCoder.encode(workoutShareUrl, forKey: "workout_share_url")
		}
		if workoutTotalTime != nil{
			aCoder.encode(workoutTotalTime, forKey: "workout_total_time")
		}
		if workoutExerciseList != nil{
			aCoder.encode(workoutExerciseList, forKey: "workout_exercise_list")
		}
		if workoutZipFile != nil{
			aCoder.encode(workoutZipFile, forKey: "workout_zip_file")
		}
        if workoutGoodForIds != nil{
            aCoder.encode(workoutZipFile, forKey: "workout_good_for_ids")
        }
        if workoutEquipmentIds != nil{
            aCoder.encode(workoutZipFile, forKey: "workout_equipment_ids")
        }

	}

}
