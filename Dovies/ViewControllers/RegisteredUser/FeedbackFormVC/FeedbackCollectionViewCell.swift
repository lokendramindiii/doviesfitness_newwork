//
//  FeedbackCollectionViewCell.swift
//  Dovies
//
//  Created by Mindiii on 9/24/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class FeedbackCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imgWorkout : UIImageView!
    @IBOutlet weak var btnRemoveImage : UIButton!
    @IBOutlet weak var btnTakeImage : UIButton!

    var btnRemoveHnadler: ((_ button : UIButton, _ iPath: IndexPath) ->())?
    var btnTakeImageHnadler: ((_ button : UIButton) ->())?
    var iPath: IndexPath!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setCellUI(_ cellObj :  ModelWorkoutLogImage, iPath: IndexPath){
        
        self.imgWorkout.image = cellObj.image
        self.iPath = iPath
        //imgWorkout.sd_setImage(with: URL(string: model.workoutImage), placeholderImage: AppInfo.placeholderImage)

    }
    
    @IBAction func actionRemoveImage(_sender:UIButton){
        if let handler = self.btnRemoveHnadler{
            handler(_sender, self.iPath)
        }
    }
    
    @IBAction func actionTakeImage(_sender:UIButton){
        if let handler = self.btnTakeImageHnadler{
            handler(_sender)
        }
    }
}
