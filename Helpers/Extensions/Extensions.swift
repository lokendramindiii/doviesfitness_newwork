//
//  Extensions.swift
//  BaseProj
//
//  Copyright © 2017 hb. All rights reserved.
//

import Foundation

extension UIFont{
	
	convenience init(fontRokkittRegularFontSize: CGFloat){
		self.init(name: FONT_ROKKITT_REGULAR, size: fontRokkittRegularFontSize)!
	}
    
    convenience init(fontAyuthayaRegularFontSize: CGFloat){
        self.init(name: FONT_AYUTHAYA_REGULAR, size: fontAyuthayaRegularFontSize)!
    }

}

extension CGRect{
	
	static var widthRatio:CGFloat {
		return SCREEN_WIDTH / 320
	}
	
	static var heightRatio:CGFloat {
		return ((max(SCREEN_HEIGHT, 568)) / 568)
	}
}

extension NSAttributedString {
	
	public func replaceWithBlueColor(range: Range<Int>, with replacementString: String) -> NSMutableAttributedString {
		let str: NSMutableAttributedString = NSMutableAttributedString(attributedString: self)
		str.replaceCharacters(in: NSMakeRange(range.lowerBound, range.count), with: replacementString)
		//        str.addAttributes([NSForegroundColorAttributeName: appColorBlue], range: NSMakeRange(range.lowerBound, replacementString.characters.count))
		return str
	}
}

extension Notification.Name {
	//	static let songChanged = Notification.Name("songChanged")
}

extension UIColor {
   class func colorWithRGB(r: CGFloat, g: CGFloat, b: CGFloat, alpha: CGFloat = 1.0) -> UIColor {
		return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: alpha)
	}
    
    
}
extension UIViewController {
    
    func show() {
        let window = UIApplication.shared.delegate?.window
        let visibleVC = window??.getVisibleViewController
        visibleVC?()?.present(self, animated: true, completion: nil)
    }
}


// MARK: - UIWindow

extension UIWindow {
    
    func getVisibleViewController() -> UIViewController? {
        return UIWindow.getVisibleViewControllerFrom(vc: self.rootViewController)
    }
    
    
    public static func getVisibleViewControllerFrom(vc: UIViewController?) -> UIViewController? {
        
        if let nc = vc as? UINavigationController {
            return UIWindow.getVisibleViewControllerFrom(vc: nc.visibleViewController)
            
        } else if let tc = vc as? UITabBarController {
            return UIWindow.getVisibleViewControllerFrom(vc: tc.selectedViewController)
            
        } else {
            if let pvc = vc?.presentedViewController {
                return UIWindow.getVisibleViewControllerFrom(vc: pvc)
                
            } else {
                return vc
                
            }
        }
    }
    
}

//MARK: - Fonts
extension UIFont {
    //class func pickerTitleFont() -> UIFont { return UIFont.robotoRegular(size: 16.0) }
    //class func pickerToolbarFont() -> UIFont { return UIFont(name: "Roboto-Medium",   size: 15.0) ?? UIFont.systemFont(ofSize: 15.0) }
    
    class func pickerTitleFont() -> UIFont { return UIFont.tamilRegular(size: 20.0) }
    
    class func pickerToolbarFont() -> UIFont { return UIFont.tamilMedium(size: 18.0) }
    
    class func robotoBlack(size: CGFloat) -> UIFont { return UIFont(name: "Roboto-Black", size: size) ?? UIFont.systemFont(ofSize: size) }
    class func robotoBold(size: CGFloat) -> UIFont { return  UIFont(descriptor:UIFontDescriptor(name: "Roboto", size: size).withSymbolicTraits(.traitBold) ?? UIFontDescriptor(name: "Roboto", size: size), size: size) }
    class func robotoLight(size: CGFloat) -> UIFont { return UIFont(name: "Roboto-Light",   size: size) ?? UIFont.systemFont(ofSize: size) }
    class func robotoMedium(size: CGFloat) -> UIFont { return UIFont(name: "Roboto-Medium",  size: size) ?? UIFont.systemFont(ofSize: size) }
    class func robotoRegular(size: CGFloat) -> UIFont { return UIFont(name: "Roboto-Regular",  size: size) ?? UIFont.systemFont(ofSize: size) }
    class func robotoThin(size: CGFloat) -> UIFont { return UIFont(name: "Roboto-Thin",  size: size) ?? UIFont.systemFont(ofSize: size) }
    class func robotoItalic(size: CGFloat) -> UIFont { return UIFont(name: "Roboto-Italic", size: size) ?? UIFont.systemFont(ofSize: size) }
    class func tamilRegular(size: CGFloat) -> UIFont { return UIFont(name: "TamilSangamMN", size: size) ?? UIFont.systemFont(ofSize: size) }
    class func tamilMedium(size: CGFloat) -> UIFont { return UIFont(name: "TamilSangamMN-Medium", size: size) ?? UIFont.systemFont(ofSize: size) }
    class func tamilBold(size: CGFloat) -> UIFont { return UIFont(name: "TamilSangamMN-Bold", size: size) ?? UIFont.systemFont(ofSize: size) }
    class func futuraBold(size: CGFloat) -> UIFont { return UIFont(name: "Futura-Bold", size: size) ?? UIFont.systemFont(ofSize: size) }

}

//Colors
extension UIColor {
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat, alpha: CGFloat) {
        self.init(red: r/255.0, green:g/255.0, blue: b/255.0, alpha: alpha)
    }
    
    class func appBlackColor() -> UIColor{return UIColor(r: 34, g: 34, b: 34, alpha: 1.0)}
    
    class func appBlackColorNew() -> UIColor{return UIColor(r: 0, g: 0, b: 0, alpha: 1.0)}
    
    class func appBlackColorSplash() -> UIColor{return UIColor(r: 18, g: 18, b:18, alpha: 1.0)}
    
    class func appNavColor() -> UIColor{return UIColor(r: 0, g: 0, b: 0, alpha: 1.0)}


    class func appGrayColor() -> UIColor{return UIColor(r: 119 , g: 119, b: 199, alpha: 1.0)}
    
    class func descriptionGrayColor() -> UIColor{return UIColor(r: 136 , g: 136, b: 136, alpha: 1.0)}
    
    class func separatorGrayColor() -> UIColor{return UIColor(r: 237 , g: 237, b: 237, alpha: 1.0)}
    class func segmentTitleColor() ->  UIColor{return UIColor(r: 153, g: 153, b: 153, alpha: 1.0)}
    class func segmentDarkTitleColor() ->  UIColor{return UIColor(r: 204, g: 204, b: 204, alpha: 1.0)}

    class func pickerToolBarButtonColor() -> UIColor { return UIColor.appBlackColor() }
    
    //class func pickerToolBarTintColor() -> UIColor { return UIColor.white }
    class func pickerToolBarTintColor() -> UIColor{return UIColor(r: 235, g: 235, b: 235, alpha: 1)}
    
//    class func pickerBackgroundColor() -> UIColor { return UIColor.white }
    class func pickerBackgroundColor() ->  UIColor{return UIColor(r: 205, g: 209, b: 214, alpha: 1)}
    
    class func separatorColor() -> UIColor { return UIColor.groupTableViewBackground }
    
}

public extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}

extension UIImageView{
    func makeCircular() {
        self.layer.cornerRadius = self.bounds.size.height/2
        self.layer.masksToBounds = true
    }
    
    func makeCircular(borderColor: UIColor, borderWidth: CGFloat) {
        self.layer.cornerRadius = self.bounds.size.height/2
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.masksToBounds = true
    }
}

extension UIButton{
    /// this method used to set underline text to button
    ///
    /// - Parameter button: UIButton object
    func setUnderLineTitle(with text: String?){
        
        if let title = text, title.count > 0  {
            let attributeString = NSMutableAttributedString(string: title)
            attributeString.addAttribute(NSAttributedStringKey.underlineStyle, value: 1, range: NSMakeRange(0, title.count))
            attributeString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.appBlackColor(), range: NSMakeRange(0, title.count))
			attributeString.addAttribute(NSAttributedStringKey.font, value: UIFont.robotoRegular(size: 12.0), range: NSMakeRange(0, title.count))
            setTitle(text, for: .normal)
            self.setAttributedTitle(attributeString, for: .normal)
            
        }
    }
    
    func makeCorner(radius: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.masksToBounds = true
    }
}

extension UITableView{
	open override func awakeFromNib() {
		super.awakeFromNib()
		if #available(iOS 11.0, *) {
			self.contentInsetAdjustmentBehavior = .never
		} else {
			// Fallback on earlier versions
		}
	}
}

extension UICollectionView{
	open override func awakeFromNib() {
		super.awakeFromNib()
		if #available(iOS 11.0, *) {
			self.contentInsetAdjustmentBehavior = .never
		} else {
			// Fallback on earlier versions
		}
	}
}

extension UILabel{
    func makeOutLine(){
        let strokeTextAttributes = [
        NSAttributedStringKey.strokeColor : UIColor.gray,
        NSAttributedStringKey.foregroundColor : UIColor.white,
        NSAttributedStringKey.strokeWidth : -1.0,
        NSAttributedStringKey.font : self.font
        ] as [NSAttributedStringKey : Any]
        self.attributedText = NSMutableAttributedString(string: self.text ?? "", attributes: strokeTextAttributes)
    }
    
    func makeSpacing(lineSpace: CGFloat, font:UIFont){
        let style = NSMutableParagraphStyle()
        style.lineSpacing = lineSpace
        let att = [NSAttributedStringKey.font : font, NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.5333333333, green: 0.5333333333, blue: 0.5333333333, alpha: 1), NSAttributedStringKey.paragraphStyle : style, NSAttributedStringKey.kern : 0.5]
            as [NSAttributedStringKey : Any]
        
        let attStr = NSMutableAttributedString(string: self.text!, attributes: att
        )
        
        DispatchQueue.main.async {
            self.attributedText = attStr
        }
    }
    
    
    // loks add code makeSpacingWithLeftAlignment
    func makeSpacingWithLeftAlignment(lineSpace: CGFloat, font:UIFont, color: UIColor){
        let style = NSMutableParagraphStyle()
        style.lineSpacing = lineSpace
        style.alignment = .left
        let att = [NSAttributedStringKey.font : font, NSAttributedStringKey.foregroundColor : color, NSAttributedStringKey.paragraphStyle : style, NSAttributedStringKey.kern : 0.5]
            as [NSAttributedStringKey : Any]
        
        let attStr = NSMutableAttributedString(string: self.text!, attributes: att)
        self.attributedText = attStr
        
        //        DispatchQueue.main.async {
        //            self.attributedText = attStr
        //
        //        }
    }
    
    
    func makeLineSpacing(linespacing: CGFloat){
        let style = NSMutableParagraphStyle()
        style.lineSpacing = linespacing
        style.alignment =  self.textAlignment
        let att = [NSAttributedStringKey.font : self.font, NSAttributedStringKey.foregroundColor : self.textColor, NSAttributedStringKey.paragraphStyle : style, NSAttributedStringKey.kern : 0.5]
            as [NSAttributedStringKey : Any]
        
        let attStr = NSMutableAttributedString(string: self.text!, attributes: att)
        self.attributedText = attStr

    }
    
    
    func makeSpacingWithCenterAlignment(lineSpace: CGFloat, font:UIFont, color: UIColor){
        let style = NSMutableParagraphStyle()
        style.lineSpacing = lineSpace
        style.alignment = .center
        let att = [NSAttributedStringKey.font : font, NSAttributedStringKey.foregroundColor : color, NSAttributedStringKey.paragraphStyle : style, NSAttributedStringKey.kern : 0.5]
            as [NSAttributedStringKey : Any]
        
        let attStr = NSMutableAttributedString(string: self.text!, attributes: att
        )
        
        DispatchQueue.main.async {
            self.attributedText = attStr
        }
        
    }
    
    func makeSpacingWithLeftAlignmentPopup(lineSpace: CGFloat, font:UIFont, color: UIColor){
        let style = NSMutableParagraphStyle()
        style.lineSpacing = lineSpace
        style.alignment = .left
        let att = [NSAttributedStringKey.font : font, NSAttributedStringKey.foregroundColor : color, NSAttributedStringKey.paragraphStyle : style, NSAttributedStringKey.kern : 0.5]
            as [NSAttributedStringKey : Any]
        
        let attStr = NSMutableAttributedString(string: self.text!, attributes: att
        )
        
        DispatchQueue.main.async {
            self.attributedText = attStr
        }
        
    }
    
    func makeCorner(radius: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.masksToBounds = true
    }
}

extension UITextField {
    func addDropdown(width: CGFloat) {
        self.rightViewMode = .always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: 11))
        imageView.contentMode = .left
        imageView.image = imageDropdown
        self.rightView = imageView
    }
    
    func addDropdownWithImage(width: CGFloat, height:CGFloat, image: UIImage) {
        self.rightViewMode = .always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: width, height: height))
        imageView.contentMode = .left
        imageView.image = image
        self.rightView = imageView
    }
    
    func makeFieldCorner(radius: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.masksToBounds = true
    }

}

extension UIView{
    func makeViewCorner(radius: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.masksToBounds = true
    }
}

extension UITextView{
    func makeTextViewCorner(radius: CGFloat, borderColor: UIColor, borderWidth: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderWidth
        self.layer.masksToBounds = true
    }
    // loks add makeSpacingWithleftAlignmentTextview
    func makeSpacingWithleftAlignmentTextview(lineSpace: CGFloat, font:UIFont, color: UIColor){
        let style = NSMutableParagraphStyle()
        style.lineSpacing = lineSpace
        style.alignment = .left
        let att = [NSAttributedStringKey.font : font, NSAttributedStringKey.foregroundColor : color, NSAttributedStringKey.paragraphStyle : style, NSAttributedStringKey.kern : 0.5]
            as [NSAttributedStringKey : Any]
        
        let attStr = NSMutableAttributedString(string: self.text!, attributes: att
        )
        
        DispatchQueue.main.async {
            self.attributedText = attStr
        }
    }
}

extension Date{
    func strrigWithFormat(format:String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        if let str  = formatter.string(from:self) as String?{
              return str
        }else{
           return ""
        }
      
    }
}

extension String{
    func dateWithFormat(format:String) -> Date? {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.date(from:self)
    }
}


extension URL {
    public var queryParameters: [String: String]? {
        guard let components = URLComponents(url: self, resolvingAgainstBaseURL: true), let queryItems = components.queryItems else {
            return nil
        }
        var parameters = [String: String]()
        for item in queryItems {
            parameters[item.name] = item.value
        }
        return parameters
    }
}


extension UITabBarController {
    open override var childViewControllerForStatusBarStyle: UIViewController? {
        return selectedViewController
    }
}

extension UINavigationController {
    open override var childViewControllerForStatusBarStyle: UIViewController? {
        return visibleViewController
    }
}
