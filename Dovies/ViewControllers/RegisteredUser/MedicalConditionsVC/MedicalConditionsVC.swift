//
//  MedicalConditionsVC.swift
//  Dovies
//
//  Created by HB-PC on 02/05/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class MedicalConditionsVC: BaseViewController {

    @IBOutlet var tableV: UITableView!
    
    var arrMedicalConditions = StaticPickerData.medicalConditions
    var cellCount = 10
    var arrSelectedCondition = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "Medical Conditions".uppercased()
        navigationItem.hidesBackButton  = true
        addNavigationBackButton()
        if let selectedConditions = GlobalUtility.getPref(key: UserDefaultsKey.medicalConditions) as? [String]{
            arrSelectedCondition = selectedConditions
        }
        self.navigationItem.rightBarButtonItems = [GlobalUtility.getNavigationRightButtonItem(target: self, selector: #selector(self.saveTapped), imageName: "save_btn_s27b").0!]
        tableV.register(UINib(nibName: "MedicalConditionsCell", bundle: nil), forCellReuseIdentifier: "MedicalConditionsCell")
     
    }
    
    @objc func saveTapped(){
        GlobalUtility.setPref(value: arrSelectedCondition, key: UserDefaultsKey.medicalConditions)
        self.navigationController?.popViewController(animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension MedicalConditionsVC : UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard arrMedicalConditions.count > indexPath.row else{return}
        if arrSelectedCondition.contains(arrMedicalConditions[indexPath.row]){
            if let indexOfObj = arrSelectedCondition.index(of: arrMedicalConditions[indexPath.row]){
                arrSelectedCondition.remove(at: indexOfObj)
            }
        }else{
                arrSelectedCondition.append(arrMedicalConditions[indexPath.row])
        }
        tableV.reloadRows(at: [indexPath], with: .none)
    }
    
    
    
    
}

extension MedicalConditionsVC : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMedicalConditions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MedicalConditionsCell") as! MedicalConditionsCell
        cell.lblTitle.text = arrMedicalConditions[indexPath.row]
        cell.btnSelection.isHidden = !arrSelectedCondition.contains(arrMedicalConditions[indexPath.row])
        return cell
    }
    
}

