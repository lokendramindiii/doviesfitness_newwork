//
//  WSHelper.swift
//
//  Copyright © 2017 Hidden Brains. All rights reserved.
//

import UIKit
import FBSDKLoginKit

class WSHelper: NSObject {
	
	static let sharedInstance: WSHelper = {
		let instance = WSHelper()
		// setup code
		
		return instance
	}()
	
    class func sendRequestWithURL(isToCheckLogin: Bool = false, wsUrl: String, method: HTTPMethod = .get, param: [String: Any]? = nil, header: HTTPHeaders? = nil, wsResponce:((_ error:DataResponse<Any>) -> ())?) {
		
//		guard NetworkReachabilityManager()?.isReachable == true else {
//			//print("Show Alert here")
//			UIAlertController.showAlert(in: UIApplication.shared.keyWindow!.rootViewController!, withTitle: AlertMessages.noNetwrok, message: "", cancelButtonTitle: "Ok", destructiveButtonTitle: nil, otherButtonTitles: nil, tap: nil)
//			if let _ = UIApplication.shared.keyWindow?.rootViewController {
//
//			}
//			return
//		}
//
        var dicHeader: [String: String]?
        if header != nil {
            dicHeader = header
        } else {
            dicHeader = [String: String]()
        }
        
        dicHeader?[WsHeader.appVersion] = WsHeader.appVersionValue
        dicHeader?[WsHeader.apiKey] = WsHeader.apiKeyValue
        if let user = DoviesGlobalUtility.currentUser{
            dicHeader?[WsHeader.authToken] = user.customerAuthToken
        }
        
        var dicParam: [String: Any]?
        if param != nil{
            dicParam = param
        }
        else{
            dicParam = [String: Any]()
        }
        
        dicParam![WsParam.deviceId] = UIDevice.current.identifierForVendor!.uuidString
        dicParam![WsParam.deviceToken] = GlobalUtility.getPref(key: UserDefaultsKey.deviceToken)
        dicParam![WsParam.deviceType] = "Ios"
        
        request(self.getFinalWSURL(wsUrl), method: method, parameters: dicParam, headers: dicHeader).responseJSON
            {
                response in
                
                switch response.result
                {
                case .success:
                    if let JSON = response.result.value as? NSDictionary
                    {
						print(JSON)
                        GlobalUtility.hideActivityIndi(viewContView: APP_DELEGATE.window!)
                        
                        if let success = JSON[WsParam.success] {
                            let successValue = "\(success)"
                            if successValue == "-200" {
                                
                            } else {
                                wsResponce?(response)
                            }
                        } else {
							if let setting = JSON[WsParam.settings]{
								let dicSetting = setting as! [String: Any]
								let success = dicSetting[WsParam.success] as! String
								if success == "401"{

									UserDefaults.standard.removeObject(forKey: UserDefaultsKey.userData)
									UserDefaults.standard.synchronize()
									APP_DELEGATE.setStoryBoardBasedOnUserLogin()
								}
							}
                            wsResponce?(response)
                        }
                    }
                case .failure(let error):
                    print(error)
                GlobalUtility.hideActivityIndi(viewContView: APP_DELEGATE.window!)
                    wsResponce?(response)
                }
        }
    }
 
    class func sendRequestWithURLWithoutOuthToken(isToCheckLogin: Bool = false, wsUrl: String, method: HTTPMethod = .get, param: [String: Any]? = nil, header: HTTPHeaders? = nil, wsResponce:((_ error:DataResponse<Any>) -> ())?) {
        
       
        var dicHeader: [String: String]?
        if header != nil {
            dicHeader = header
        } else {
            dicHeader = [String: String]()
        }
        
//        dicHeader?[WsHeader.appVersion] = WsHeader.appVersionValue
//        dicHeader?[WsHeader.apiKey] = WsHeader.apiKeyValue
//        if let user = DoviesGlobalUtility.currentUser{
//            dicHeader?[WsHeader.authToken] = user.customerAuthToken
//        }
//
        var dicParam: [String: Any]?
        if param != nil{
            dicParam = param
        }
        else{
            dicParam = [String: Any]()
        }
        
        dicParam![WsParam.deviceId] = UIDevice.current.identifierForVendor!.uuidString
        dicParam![WsParam.deviceToken] = GlobalUtility.getPref(key: UserDefaultsKey.deviceToken)
        dicParam![WsParam.deviceType] = "Ios"
        
        request(self.getFinalWSURL(wsUrl), method: method, parameters: dicParam, headers: dicHeader).responseJSON
            {
                response in
                
                switch response.result
                {
                case .success:
                    if let JSON = response.result.value as? NSDictionary
                    {
                      print(JSON)
                   GlobalUtility.hideActivityIndi(viewContView: APP_DELEGATE.window!)
                        
                        if let success = JSON[WsParam.success] {
                            let successValue = "\(success)"
                            if successValue == "-200" {
                                
                            } else {
                                wsResponce?(response)
                            }
                        } else {
                            if let setting = JSON[WsParam.settings]{
                                let dicSetting = setting as! [String: Any]
                                let success = dicSetting[WsParam.success] as! String
                                if success == "401"{
                                    UserDefaults.standard.removeObject(forKey: UserDefaultsKey.userData)
                                    UserDefaults.standard.synchronize()
                                    APP_DELEGATE.setStoryBoardBasedOnUserLogin()
                                }
                            }
                            wsResponce?(response)
                        }
                    }
                case .failure(let error):
                    print(error)
                    GlobalUtility.hideActivityIndi(viewContView: APP_DELEGATE.window!)
                    wsResponce?(response)
                }
         }
    }
    
    
    
	class func getFinalWSURL(_ url:String) -> String {
		var finalURL = url
		if (finalURL.lowercased().contains("http:") == false) && (finalURL.lowercased().contains("https:") == false) {
			finalURL = BASE_URL.appending(url)
		}
		return finalURL
	}
	
	func getResponseFromURL(url: String, requestType method: HTTPMethod? = nil, requestParameters dictParams: [String: Any]? = nil, completion: @escaping (_ result: NSDictionary, _ error: Error? ) -> Void ) {
		
		self.getResponseFromURL(url: url, requestType: method, requestParameters: dictParams, uploadProgress:nil, completion: completion)
	}
	
	func getResponseFromURL(url: String, requestType method: HTTPMethod? = nil, requestParameters dictParams: [String: Any]? = nil,uploadProgress: ((_ result:Progress) -> Void)? , completion: @escaping (_ result: NSDictionary, _ error: Error? ) -> Void ) {
		
		guard NetworkReachabilityManager()?.isReachable == true else {
			//print("Show Alert here")
            UIAlertController.showAlert(in: UIApplication.shared.keyWindow!.rootViewController!, withTitle: AlertMessages.noNetwrok, message: "", cancelButtonTitle: "Ok", destructiveButtonTitle: nil, otherButtonTitles: nil, tap: nil)
			if let _ = UIApplication.shared.keyWindow?.rootViewController {
				
			}
			completion(NSDictionary(), nil)
			return
		}
		
		var headerParameters = [
			"Content-Type": "application/json",
			"Accept": "application/json",
//            ws_header.APIVERSION : ws_header.APIVERSION_VALUE,
//            ws_header.APIKEY : ws_header.APIKEY_VALUE,
//            ws_header.AUTHTOKEN : GlobalUtility.getPref(key: UserDefaultsKey.authToken) as? String
		]
		headerParameters[WsHeader.appVersion] = WsHeader.appVersionValue
		headerParameters[WsHeader.apiKey] = WsHeader.apiKeyValue
		if let user = DoviesGlobalUtility.currentUser {
			headerParameters[WsHeader.authToken] = user.customerAuthToken
		}
		
		
		var dicParam: [String: Any]?
		if dictParams != nil {
			dicParam = dictParams
		} else{
			dicParam = [String: Any]()
		}
		dicParam![WsParam.deviceId] = UIDevice.current.identifierForVendor!.uuidString
		dicParam![WsParam.deviceToken] = GlobalUtility.getPref(key: UserDefaultsKey.deviceToken)
		dicParam![WsParam.deviceType] = "Ios"
        
        //print("URL==\(url)\nParam===\(dicParam)\nheaders===\(headerParameters)")
        
		if method == HTTPMethod.get{
			request(WSHelper.getFinalWSURL(url), method: method!, parameters: dicParam, encoding:URLEncoding.default, headers: headerParameters as HTTPHeaders).validate().responseJSON{ (dataResponse:DataResponse<Any>) in
				
				
				//let request = dataResponse.request
				//let response = dataResponse.response
				let result = dataResponse.result
				
				let JSON = result.value
				let error = result.error as NSError?
				if error != nil{
					completion(NSDictionary(), error)
				}else{
					print("response---\(JSON as! NSDictionary)")
					completion(JSON as! NSDictionary , error)
				}
				
			}
		}
		else if method == HTTPMethod.post
		{
			var arrayMultiFormData = Array<Dictionary<String, Any>>()
			var mutableDictParams:NSMutableDictionary =  NSMutableDictionary()
			if dicParam != nil {
				let fm:FileManager = FileManager.default
				
				mutableDictParams  = NSMutableDictionary(dictionary: dicParam!)
				for strKey in mutableDictParams.allKeys {
					if mutableDictParams[strKey] is UIImage {
                        print("params name = \(strKey)")

						var dictMultiFormData = Dictionary<String, Any>()
						dictMultiFormData["image"] = mutableDictParams[strKey]
						dictMultiFormData["keyName"] = strKey
						arrayMultiFormData.append(dictMultiFormData)
						mutableDictParams.removeObject(forKey: strKey)
					}
					else if mutableDictParams[strKey] is String {
						print("params name = \(strKey)")
						if fm.fileExists(atPath: mutableDictParams[strKey] as! String){
							
							let path = mutableDictParams[strKey] as! String
							var ext:String = (path as NSString).pathExtension
							
							if(ext.count>0){
								ext = ext.lowercased()
								let key = "media"
								var dictMultiFormData = Dictionary<String, Any>()
								dictMultiFormData[key] = mutableDictParams[strKey]
								dictMultiFormData["keyName"] = strKey
								arrayMultiFormData.append(dictMultiFormData)
								mutableDictParams.removeObject(forKey: strKey)
							}
						}
					}
				}
				
				if arrayMultiFormData.count == 0 {
                    
					request(WSHelper.getFinalWSURL(url), method: .get, parameters: dicParam, encoding: URLEncoding.default, headers: headerParameters as HTTPHeaders).validate().responseJSON{ (dataResponse:DataResponse<Any>) in
						
						//let request = dataResponse.request
						//let response = dataResponse.response
						let result = dataResponse.result
						
						let JSON = result.value
						let error = result.error as NSError?
						if error != nil{
							completion(NSDictionary(), error)
						}else{
					// loks hide		print("response---\(JSON as! NSDictionary)")
							completion(JSON as! NSDictionary , error)
						}
					}
				}
				else
				{
					upload(multipartFormData: { (multipartFormData) in
						for dict in arrayMultiFormData {
							if(dict["media"] != nil){
								let url:NSURL = NSURL(fileURLWithPath: dict["media"]! as! String)
								do {
									let videoData:Data = try Data(contentsOf: url as URL)
									multipartFormData.append(videoData, withName: dict["keyName"] as! String, fileName: "\(Date.init(timeIntervalSinceReferenceDate: NSTimeIntervalSince1970))", mimeType: "")
								} catch {
									print(error.localizedDescription)
								}
							}
							else if(dict["image"] != nil){
								print(dict["keyName"] as! String)
								let compressedImage = UIImageJPEGRepresentation((dict["image"] as! UIImage), 0.8)!
								multipartFormData.append(compressedImage, withName: dict["keyName"] as! String, fileName: "\(Date.init(timeIntervalSinceReferenceDate: NSTimeIntervalSince1970)).png", mimeType: "image/png")
							}
						}
						
						for (key, value) in mutableDictParams {
							let str = "\(value)"
							multipartFormData.append(str.data(using: String.Encoding.utf8)!, withName: key as! String)
						}
					}, to: WSHelper.getFinalWSURL(url), method: method!, headers: headerParameters as HTTPHeaders)
					{ (result) in
						switch result {
						case .success(let upload, _, _):
							
							upload.uploadProgress(closure: { (progress) in
								//Print progress
								//print(progress.fractionCompleted)
								if(uploadProgress != nil){
									uploadProgress!(progress)
								}
								
							})
							
							upload.responseJSON { response in
								//print response.result
								if let result = response.result.value {
									let JSON = result as! NSDictionary
									print("response---\(JSON)")
                                    
									completion(JSON, nil)
								}else {
									completion(NSDictionary(), nil)
								}
							}
							
						case .failure(let encodingError):
							//print encodingError.description
							completion(NSDictionary(), encodingError)
						}
					}
				}
			}
		}
	}
    
    func postMultipartDataWithImageArray(url: String, requestType method: HTTPMethod? = nil, requestParameters dictParams: [String: Any]? = nil,uploadProgress: ((_ result:Progress) -> Void)? , completion: @escaping (_ result: NSDictionary, _ error: Error? ) -> Void ) {
        
        guard NetworkReachabilityManager()?.isReachable == true else {
            //print("Show Alert here")
            UIAlertController.showAlert(in: UIApplication.shared.keyWindow!.rootViewController!, withTitle: AlertMessages.noNetwrok, message: "", cancelButtonTitle: "Ok", destructiveButtonTitle: nil, otherButtonTitles: nil, tap: nil)
            if let _ = UIApplication.shared.keyWindow?.rootViewController {
                
            }
            completion(NSDictionary(), nil)
            return
        }
        
        var headerParameters = [
            "Content-Type": "application/json",
            "Accept": "application/json",
            //            ws_header.APIVERSION : ws_header.APIVERSION_VALUE,
            //            ws_header.APIKEY : ws_header.APIKEY_VALUE,
            //            ws_header.AUTHTOKEN : GlobalUtility.getPref(key: UserDefaultsKey.authToken) as? String
        ]
        headerParameters[WsHeader.appVersion] = WsHeader.appVersionValue
        headerParameters[WsHeader.apiKey] = WsHeader.apiKeyValue
        if let user = DoviesGlobalUtility.currentUser {
            headerParameters[WsHeader.authToken] = user.customerAuthToken
        }
        
        
        var dicParam: [String: Any]?
        if dictParams != nil {
            dicParam = dictParams
        } else{
            dicParam = [String: Any]()
        }
        dicParam![WsParam.deviceId] = UIDevice.current.identifierForVendor!.uuidString
        dicParam![WsParam.deviceToken] = GlobalUtility.getPref(key: UserDefaultsKey.deviceToken)
        dicParam![WsParam.deviceType] = "Ios"
        print("URL==\(url)\nParam===\(dicParam)\nheaders===\(headerParameters)")
        
        if method == HTTPMethod.post
            {
            var arrayMultiFormData = Array<Dictionary<String, Any>>()
            var mutableDictParams:NSMutableDictionary =  NSMutableDictionary()
            if dicParam != nil {
                mutableDictParams  = NSMutableDictionary(dictionary: dicParam!)
                for strKey in mutableDictParams.allKeys {
                    if mutableDictParams[strKey] is [UIImage] {
                        var dictMultiFormData = Dictionary<String, Any>()
                        dictMultiFormData["images"] = mutableDictParams[strKey]
                        dictMultiFormData["keyName"] = strKey
                        arrayMultiFormData.append(dictMultiFormData)
                        mutableDictParams.removeObject(forKey: strKey)
                    }
                }
                
                if arrayMultiFormData.count == 0 {
                    request(WSHelper.getFinalWSURL(url), method: .get, parameters: dicParam, encoding: URLEncoding.default, headers: headerParameters as HTTPHeaders).validate().responseJSON{ (dataResponse:DataResponse<Any>) in
                        
                        //let request = dataResponse.request
                        //let response = dataResponse.response
                        let result = dataResponse.result
                        
                        let JSON = result.value
                        let error = result.error as NSError?
                        if error != nil{
                            completion(NSDictionary(), error)
                        }else{
                           print("response---\(JSON as! NSDictionary)")
                            completion(JSON as! NSDictionary , error)
                        }
                    }
                }
                else{
                    upload(multipartFormData: { (multipartFormData) in
                        for dict in arrayMultiFormData {
                            
                            if(dict["images"] != nil){
                                let arrImages = dict["images"] as! [UIImage]
                                    print("arr images = \(arrImages)")
                                let imageParamName = dict["keyName"] as? String ?? ""
                                print("images param name = \(imageParamName)")
                                for img in arrImages{
                                        let compressedImage = UIImageJPEGRepresentation(img, 0.8)!
                                        multipartFormData.append(compressedImage, withName: "\(imageParamName)[]", fileName: "\(Date.init(timeIntervalSinceReferenceDate: NSTimeIntervalSince1970)).png", mimeType: "image/png")
                                    
                                }
                            }
                        }
                        
                        for (key, value) in mutableDictParams {
                            let str = "\(value)"
                            multipartFormData.append(str.data(using: String.Encoding.utf8)!, withName: key as! String)
                        }
                    }, to: WSHelper.getFinalWSURL(url), method: method!, headers: headerParameters as HTTPHeaders)
                    { (result) in
                        switch result {
                        case .success(let upload, _, _):
                            
                            upload.uploadProgress(closure: { (progress) in
                                //Print progress
                                //print(progress.fractionCompleted)
                                if(uploadProgress != nil){
                                    uploadProgress!(progress)
                                }
                                
                            })
                            
                            upload.responseJSON { response in
                                //print response.result
                                if let result = response.result.value {
                                    let JSON = result as! NSDictionary
                                print("response---\(JSON)")
                                    
                                    completion(JSON, nil)
                                }else {
                                    completion(NSDictionary(), nil)
                                }
                            }
                            
                        case .failure(let encodingError):
                            //print encodingError.description
                            completion(NSDictionary(), encodingError)
                        }
                    }
                }
            }
        }
    }
}
