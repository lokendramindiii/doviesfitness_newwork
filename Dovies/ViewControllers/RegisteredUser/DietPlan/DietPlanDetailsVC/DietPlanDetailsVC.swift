//
//  DietPlanDetailsVC.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

//This view controller is used to display diet plan details
class DietPlanDetailsVC: BaseViewController {

    @IBOutlet weak var imgDietPlan : UIImageView!
    @IBOutlet weak var lblTitle: CustomLabel!
    @IBOutlet weak var lblDescription : CustomLabel!
    @IBOutlet weak var scrollView : UIScrollView!
	@IBOutlet weak var activityIndicatorView : NVActivityIndicatorView!
    @IBOutlet weak var btnPurchase : UIButton!
    @IBOutlet weak var Img_Lock : UIImageView!
    @IBOutlet weak var btnShare : UIButton!
    @IBOutlet weak var IbViewGradient : UIView!

    var selectedPlan : DietPlan?
    var planId : String?
    var dietPlanDetails : DietPlanDetails?
    var favouriteButton : (UIBarButtonItem?,UIButton?)?
    
    var programId : String?
    var programDietPlans : [ProgramDietPlan]?
    var isPlanAdd : Bool = false
    
    var onPurchaseSuccess:(()->())?
    
    //MARK: ViewLifeCycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.scrollView.contentInset = UIEdgeInsets(top:  0 , left: 0, bottom: self.tabBarController!.tabBar.frame.height, right: 0)

        
        self.scrollView.isHidden = true
        
        inititalUISetUp()
        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            // Fallback on earlier versions
        }

		activityIndicatorView.padding = 20
		activityIndicatorView.color = UIColor.lightGray
		activityIndicatorView.type = .circleStrokeSpin
		activityIndicatorView.startAnimating()
    }
    
  
    //MARK: - Intital view controller UI setup
    
    override func inititalUISetUp() {
        lblTitle.text = ""
        lblDescription.text = " "
   // lokendra     addNavigationBackButton()
    //    self.navigationItem.title = "OVERVIEW"
        btnPurchase.isHidden = true
        setPurchaseButton()
        callGetDietPlanDetailsAPI()
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        self.navigationController?.isNavigationBarHidden = true
     //   self.navigationController?.navigationBar.barTintColor = UIColor.appNavColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,
                                                                        NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]
        
    }
    
    override func viewWillDisappear(_ animated: Bool)
        
    {
        super.viewWillDisappear(animated)
        
        self.navigationController?.isNavigationBarHidden = false

    //    self.navigationController?.navigationBar.barTintColor = UIColor.white
//        self.navigationController?.navigationBar.titleTextAttributes = [
//            NSAttributedStringKey.foregroundColor : UIColor.colorWithRGB(r: 34.0, g: 34.0, b: 34.0),
//            NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]
    }
    

    func getBuildDietPlanAddStatus()->Bool{
        var dietPlanId : String = ""
        if let dPlanID = planId{
            dietPlanId = dPlanID
        }else if let plan = selectedPlan{
            dietPlanId = plan.dietPlanId
        }
        if let dietPlans = programDietPlans {
            return !dietPlans.contains(where: { $0.programDietId == dietPlanId })
        }
        return true
    }
    
    
    func setPurchaseButton(){
        btnPurchase.setTitle("PURCHASE" , for: .normal)
    }
    
    func loadUI(){
        guard let planDetails = dietPlanDetails else{return}
		
//       self.navigationItem.title = planDetails.dietPlanName
		imgDietPlan.sd_setImage(with: URL(string: planDetails.dietPlanFullImage )) { (image, error, cache, url) in
			self.activityIndicatorView.stopAnimating()
		}

        
//        let viewGradient1 =  UIView(frame: CGRect(x: 0, y: 0, width:ScreenSize.width, height: IbViewGradient.frame.size.height))
//        let gradientLayer1:CAGradientLayer = CAGradientLayer()
//
//        gradientLayer1.frame.size =  viewGradient1.frame.size
//        gradientLayer1.colors = [UIColor.clear.cgColor,UIColor.appBlackColorNew().withAlphaComponent(1.0).cgColor]
//        viewGradient1.layer.addSublayer(gradientLayer1)
        
 //       IbViewGradient.addSubview(viewGradient1)
        
        
        lblTitle.text = planDetails.dietPlanName.uppercased()
        
     //   lblTitle.makeSpacingWithCenterAlignment(lineSpace: 2.0, font: UIFont.futuraBold(size: 16), color: UIColor.colorWithRGB(r: 34, g: 34, b: 34, alpha: 1.0))
        
        lblTitle.makeSpacingWithCenterAlignment(lineSpace: 2.0, font: UIFont.futuraBold(size: 24), color: UIColor.white)

        lblDescription.text = planDetails.dietPlanDescription
     //   lblDescription.makeSpacingWithLeftAlignment(lineSpace: 9.0, font: UIFont.tamilRegular(size: 14), color: UIColor.colorWithRGB(r: 136, g: 136, b: 136, alpha: 1.0))
        
        lblDescription.makeSpacingWithLeftAlignment(lineSpace: 9.0, font: UIFont.tamilRegular(size: 14), color: UIColor.white)

        Img_Lock.isHidden = (planDetails.dietPlanAccessLevel.uppercased() == "OPEN".uppercased() || planDetails.dietPlanAccessLevel.isEmpty)

        
		setDietPlanPurchaseButtonBasedOnAccesslevel()
        
        btnShare.isHidden = false
        
       //lokendra addNavigationBackButton()
       // lokendra addRightNavigationBarItems()
    }
    
    
    /**
     This method is to set the Purchase button based on access level.
     */
    
    func setDietPlanPurchaseButtonBasedOnAccesslevel(){
        guard let planDetails = dietPlanDetails else{return}
        isPlanAdd = getBuildDietPlanAddStatus()
		if planDetails.isAdded == true, programId == nil{
			btnPurchase.isHidden = false
            btnPurchase.setTitle("ALREADY ADDED" , for: .normal)
            btnPurchase.isEnabled = false
			return
		}
		btnPurchase.isHidden = false
        if programId != nil{
            if planDetails.dietPlanAccessLevel.uppercased() == "LOCK"{
                
                switch planDetails.dietAccessLevel.uppercased(){
                case "FREE":
                    isPlanAdd = getBuildDietPlanAddStatus()
                    btnPurchase.setTitle(isPlanAdd ? "ADD"  : "DELETE" , for: .normal)
                case "Subscribers".uppercased():
                    btnPurchase.setTitle("Subscribe".uppercased() , for: .normal)
                case "PAID":
                    btnPurchase.setTitle("Purchase".uppercased() , for: .normal)
                default:
                    break
                }
                
            }else{
                isPlanAdd = getBuildDietPlanAddStatus()
                btnPurchase.setTitle(isPlanAdd ? "ADD"  : "DELETE" , for: .normal)
            }
            
            
        }else{
            if planDetails.dietPlanAccessLevel.uppercased() == "LOCK"{
               
                switch planDetails.dietAccessLevel.uppercased(){
                case "FREE":
                    btnPurchase.setTitle("ADD TO MY DIET PLAN" , for: .normal)
                case "Subscribers".uppercased():
                    btnPurchase.setTitle("Subscribe".uppercased() , for: .normal)
                case "PAID":
                    btnPurchase.setTitle("Purchase".uppercased() , for: .normal)
                default:
                    break
                }
            
            }else{
               btnPurchase.setTitle("ADD TO MY DIET PLAN" , for: .normal)
            }
        }
    }
    
    /**
     This method is to add 'Favourite' and 'Share' button to the Navigation bar.
     */
    func addRightNavigationBarItems() {
           favouriteButton = GlobalUtility.getNavigationRightButtonItem(target: self, selector: #selector(self.favouriteTapped(sender:)), imageName: "add_favorite_btn_s33")
		
        if  let shareButton = GlobalUtility.getNavigationRightButtonItem(target: self, selector: #selector(self.shareTapped(sender:)), imageName: "btn_share").0, let navFavButton = favouriteButton?.0{
          //  self.navigationItem.rightBarButtonItems = [ navFavButton, shareButton]
            
            self.navigationItem.rightBarButtonItems = [ shareButton]
            
            if let dietDetails = dietPlanDetails, let navFavButton = favouriteButton?.1{
                navFavButton.isSelected = dietDetails.dietPlanFavStatus
            }
        }
    }
    
    /**
     This method is to add a plan if it is open otherwise will redirect to 'update premium' screen. If its a paid one, will check the product with in-app purchase.
     */
    func purchaseActionFromPlan(){
        isPlanAdd = getBuildDietPlanAddStatus()
        if isPlanAdd{
            if dietPlanDetails?.dietPlanAccessLevel.uppercased() == "OPEN" ||  DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName{
                callAddToPlanAPI()
            }else{
                guard let planDetails =  dietPlanDetails else{return}
                switch planDetails.dietAccessLevel.uppercased(){
                case "Subscribers".uppercased():
                    redirectToUpdateToPremium()
                case "PAID":
                    purchaseProductWith(productId : planDetails.dietPlanPackageCode)
                default:
                    break
                }
            }
        }else{
            UIAlertController.showAlert(in: self, withTitle: AlertTitle.appName, message: AlertMessages.deleteConfirmation, cancelButtonTitle: "No", destructiveButtonTitle: "Delete", otherButtonTitles: nil, tap: { (alertCont, action, index) in
                if index == 1{
                    self.callAddToPlanAPI()
                }
            })
        }
    }
 
    /**
     This method is to add a diet plan if it is open otherwise will redirect to 'update premium' screen. If its a paid one, will check the product with in-app purchase.
     */
    func purchaseAction(){
        if dietPlanDetails?.dietPlanAccessLevel.uppercased() == "OPEN" || DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName{
            callAddToMyDietPlansAPI()
        }
        else{
            guard let planDetails =  dietPlanDetails else{return}
            switch planDetails.dietAccessLevel.uppercased(){
            case "Subscribers".uppercased():
                redirectToUpdateToPremium()
            case "PAID":
                purchaseProductWith(productId : planDetails.dietPlanPackageCode)
            default:
                break
            }
        }
    }
    
    /**
     This method is used to redirect the user to premium screen when the plan or diet is under subscription.
     */
    func redirectToUpdateToPremium(){
        if let upgradeVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "UpgradeToPremiumViewController") as? UpgradeToPremiumViewController{
            upgradeVC.onPurchaseSuccess = {
                self.dietPlanDetails?.dietPlanAccessLevel = "OPEN"
                self.dietPlanDetails?.dietAccessLevel = "FREE"
                self.onPurchaseSuccess?()
                self.loadUI()
            }
            
            self.navigationController?.pushViewController(upgradeVC, animated: true)
        }
    }
    
    //MARK: - ACtions
    @IBAction func btnPurchaseTapped(_ sender : UIButton){
        if programId != nil{
            purchaseActionFromPlan()
        }else{
            purchaseAction()
		}
    }
    
    @IBAction  func BtnBackTapped(sender: UIButton) {
        
         self.navigationController?.popViewController(animated: true)
    }
    

    func purchaseProductWith(productId : String){
        if productId.isEmpty{
            GlobalUtility.showSimpleAlert(viewController: self, message: "Package id is blank")
            return
        }
        GlobalUtility.showActivityIndi(viewContView: self.view)
        PurchaseHelper.payForPackage(packageId: productId, packageMasterId: "", onSuccss: { (purchaseDetails, encodedData ,receipt_Data) in
            self.callpaymentSuccessAPI(with: purchaseDetails, encodedInfo: encodedData)
        }) { (alert) in
            GlobalUtility.hideActivityIndi(viewContView: self.view)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBAction func shareTapped(sender : UIButton){
        var dietPlanId : String = ""
        if let dPlanID = planId{
            dietPlanId = dPlanID
        }else if let plan = selectedPlan{
            dietPlanId = plan.dietPlanId
        }
        
//        DoviesGlobalUtility.generateBranchIoUrl(with: ModuleName.dietPlan, moduleId: dietPlanId, completionHandler: { (urlString) in
//            DoviesGlobalUtility.showShareSheet(on: self, shareText: urlString)
//        })
        
        DoviesGlobalUtility.showShareSheet(on: self, shareText: self.dietPlanDetails?.dietPlanShareUrl ?? "")
        
    }
    
    @objc func favouriteTapped(sender : UIBarButtonItem){
        changeFavouriteState()
        callFavouriteAPI()
        
    }
    
    func changeFavouriteState(){
        if let favButton = favouriteButton?.1{
            favButton.isSelected = !favButton.isSelected
            dietPlanDetails!.dietPlanFavStatus = dietPlanDetails!.dietPlanFavStatus
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

//API Calls
extension DietPlanDetailsVC{

    /**
     This method is used to get diet plan list and update the UI.
     */
    func callGetDietPlanDetailsAPI(){
        
        var dietPlanId : String = ""
        if let dPlanID = planId{
            dietPlanId = dPlanID
        }else if let plan = selectedPlan{
            dietPlanId = plan.dietPlanId
        }
        
        guard  let cUser = DoviesGlobalUtility.currentUser else{return}
        scrollView.showLoader()
        var parameters = [String : Any]()
        parameters[WsParam.authCustomerId] =  cUser.customerAuthToken
       // parameters[WsParam.authCustomerSubscription] = "Free"//Change after payment
        parameters[WsParam.authCustomerSubscription] =  DoviesGlobalUtility.currentUser?.customerUserType ?? ""

        
        parameters[WsParam.dietPlanId] = dietPlanId
        
        DoviesWSCalls.callGetDietPlanDetailsAPI(dicParam: parameters, onSuccess: {[weak self] (success, detailsDict, message) in
            guard let weakSelf = self else{return}
            
            weakSelf.scrollView.hideLoader()
            weakSelf.scrollView.isHidden = false

            if success{
                weakSelf.dietPlanDetails = detailsDict
                weakSelf.loadUI()
            }else{
                DoviesGlobalUtility.showSimpleAlert(viewController: weakSelf, message: message, completion: nil)
            }
            

            
        }) {[weak self] (error) in
            guard let weakSelf = self else{return}
            weakSelf.scrollView.isHidden = false
        }
    }
    
    /**
     This method is used favourite the selected plan.
     */
    func callFavouriteAPI(){
        var dietPlanId : String = ""
        if let dPlanID = planId{
            dietPlanId = dPlanID
        }else if let plan = selectedPlan{
            dietPlanId = plan.dietPlanId
        }
        
        guard let cUser = DoviesGlobalUtility.currentUser else{return}
        var parameters = [String : Any]()
        parameters[WsParam.authCustomerId] =  cUser.customerAuthToken
        parameters[WsParam.moduleId] = dietPlanId
        parameters[WsParam.moduleName] = ModuleName.dietPlan
        
        DoviesWSCalls.callFavouriteAPI(dicParam: parameters, onSuccess: {[weak self] (success, message, response) in
            guard let weakSelf = self else{return}
            if !success, weakSelf.dietPlanDetails != nil{
                weakSelf.changeFavouriteState()
            }
        }) { [weak self] (error) in
            guard let weakSelf = self else{return}
            if weakSelf.dietPlanDetails != nil{
                weakSelf.changeFavouriteState()
            }
        }
    }
    
    /**
     This method is used to add program to my plans list
     */
    func callAddToMyDietPlansAPI(){
        var dietPlanId : String = ""
        if let dPlanID = planId{
            dietPlanId = dPlanID
        }else if let plan = selectedPlan{
            dietPlanId = plan.dietPlanId
        }
        
        guard let cUser = DoviesGlobalUtility.currentUser else{return}
        var parameters = [String : Any]()
        parameters[WsParam.authCustomerId] =  cUser.customerAuthToken
        parameters[WsParam.dietPlanId] = dietPlanId
        
        DoviesWSCalls.callAddToMyDietPlanAPI(dicParam: parameters, onSuccess: { (success, response, message) in
			
			if success{
				GlobalUtility.showToastMessage(msg: "Done")
                self.dietPlanDetails!.isAdded = true
                self.btnPurchase.isHidden = false
                self.btnPurchase.setTitle("ALREADY ADDED" , for: .normal)
                self.btnPurchase.isEnabled = false
                let myDietPlanVC = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "MyDietPlanViewController") as! MyDietPlanViewController
                self.navigationController?.pushViewController(myDietPlanVC, animated: true)
			}
			else{
				DoviesGlobalUtility.showSimpleAlert(viewController: self, message: message, completion: nil)
			}
        }) { (error) in
            
        }
    }
    
    /**
     This method is used to add the Diet plan.
     */
    func callAddToPlanAPI(){
        if let viewControllers = self.navigationController?.viewControllers{       
            var planVC : BuildPlanViewController?
            for controller in viewControllers{
                if let buildPlanVC = controller as? BuildPlanViewController{
                    planVC = buildPlanVC
                    
                }
            }
            if let vc = planVC{
                let buildPlanDietVC = vc.dietPlanVc
                var dict = [String : Any]()
                dict["program_diet_id"] = selectedPlan?.dietPlanId ?? ""
                dict["program_diet_image"] = selectedPlan?.dietPlanFullImage ?? ""
                dict["program_diet_name"] = selectedPlan?.dietPlanTitle ?? ""
                dict["diet_plan_pdf"] = selectedPlan?.dietPlanPdf ?? ""
                let dietplan = ProgramDietPlan(fromDictionary: dict)
                buildPlanDietVC?.planUpdated?(dietplan, self.isPlanAdd)
                self.navigationController?.popToViewController(vc, animated: true)
            }
        }
    }
    
    /**
     This method is used to update the plan and purchanse details to server and confirms the payment was done, so that the diet plan access will be open.
     */
    func callpaymentSuccessAPI(with paymentModel: PurchaseDetails, encodedInfo : String){
        var dietPlanId : String = ""
        if let dPlanID = planId{
            dietPlanId = dPlanID
        }else if let plan = selectedPlan{
            dietPlanId = plan.dietPlanId
        }
        
        var tansactionDetailsDict = [String: Any]()
        tansactionDetailsDict[WsParam.productId] = paymentModel.productId
        tansactionDetailsDict[WsParam.quantity] = paymentModel.quantity
        if let tID = paymentModel.transaction.transactionIdentifier{
            tansactionDetailsDict[WsParam.transactionId] = tID
        }
        tansactionDetailsDict[WsParam.encodedKey] = encodedInfo
        
        let data = try! JSONSerialization.data(withJSONObject:tansactionDetailsDict, options: [ ])
        
        guard let cUser = DoviesGlobalUtility.currentUser, let jsonString = String(data: data, encoding: .utf8) else{return}
        var parameters = [String : Any]()
        parameters[WsParam.authCustomerId] =  cUser.customerAuthToken
        parameters[WsParam.productId] = dietPlanId
        parameters[WsParam.productType] = "DIET_PLAN"
        parameters[WsParam.transactionDetail] = jsonString

        DoviesWSCalls.callProductPurchaseSuccessAPI(dicParam: parameters, onSuccess: {[weak self] (success, message) in
            guard let weakSelf = self else{return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
            if success{
                weakSelf.dietPlanDetails?.dietPlanAccessLevel = "OPEN"
                weakSelf.dietPlanDetails?.dietAccessLevel = "FREE"
                weakSelf.loadUI()
            }
            if let msg = message{
                DoviesGlobalUtility.showSimpleAlert(viewController: weakSelf, message: msg)
            }
        }) { (error) in
            GlobalUtility.hideActivityIndi(viewContView: self.view)
        }
    }
}
