//
//  ShapeBodyTableviewCell.swift
//  Dovies
//
//  Created by hb on 26/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import TagListView
import AVFoundation
import MediaPlayer
import CoreData

class ExerciseDetailTableviewCell: UITableViewCell {

    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblLevel: UILabel!
    @IBOutlet var lblEquipment: UILabel!
    @IBOutlet var imgExercise: UIImageView!
   // @IBOutlet var btnDetails: UIButton!
    @IBOutlet var btnvideo: UIButton!

   // @IBOutlet var viewBodyType: TagListView!
    @IBOutlet var lblBodyType: UILabel!

	@IBOutlet weak var IBbtnFavo: UIButton!
	@IBOutlet weak var IBbtnShare: UIButton!
	@IBOutlet weak var IBbtnLock: UIButton!
	@IBOutlet weak var IBbtnVideoClose: UIButton!
	@IBOutlet weak var IBbtnVideoPlay: UIButton!
	@IBOutlet weak var imgViewCheckBox: UIImageView!
    @IBOutlet weak var btnCheckBox: UIButton!
	
	@IBOutlet weak var viewUpperLayer: UIView!
	
	@IBOutlet weak var viewVideoPlayer: VideoContainerView!
	
	@IBOutlet weak var IBacitivtyIndicatorVideo: UIActivityIndicatorView!
    @IBOutlet weak var constPlayerHeight: NSLayoutConstraint!
	@IBOutlet weak var constlockMiddleTop: NSLayoutConstraint!
	@IBOutlet weak var constLockFavTop: NSLayoutConstraint!
	
	var downloadRequest: DownloadRequest?
	
    var cellIndexPath : IndexPath!
	var playerItem: AVPlayerItem!
	var player: AVPlayer!
	var playerLayer: AVPlayerLayer!
	
	var likeTapped:(() -> ())?
	var shareTapped:(() -> ())?
	var imageTapped:((IndexPath) -> ())?
    var selectionTappedAt:((IndexPath) -> ())?
	var videoCloseBtnTap:(() -> ())?
    var videoUpperLayerTapped:(()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func selectionButtonTapped(_ sender : UIButton){
        selectionTappedAt?(cellIndexPath)
    }
    
    func setData(_ obj:ExerciseDetails, iPath : IndexPath){
        
        cellIndexPath = iPath
        let attributedString = NSMutableAttributedString(string: obj.exerciseName)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5.0
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
        
        lblTitle.attributedText = attributedString
        if obj.exerciseLevel == "All"
        {
            lblLevel.text = "All Level"
        }
        else
        {
            lblLevel.text = obj.exerciseLevel
        }
       
		lblEquipment.text =  obj.exerciseEquipments.replacingOccurrences(of: " | ", with: ", ")
        
       // imgExercise.sd_setImage(with: URL(string: obj.exerciseImage), placeholderImage: AppInfo.placeholderImage)

        imgExercise.sd_setImage(with: URL(string: obj.exerciseImage), placeholderImage: nil)

        
//        let imagepath = Exercise_List.fetchFromCoredata(exerciseId: obj.exerciseId)
//        if imagepath == ""
//        {
//            imgExercise.sd_setImage(with: URL(string: obj.exerciseImage), placeholderImage: AppInfo.placeholderImage)
//        }
//        else
//        {
//          imgExercise.image = UIImage(contentsOfFile: imagepath)
//        
//        }
        
//        let image = Exercise_List.fetchFromCoredata(exerciseId: obj.exerciseId)
//        if image.isEmpty
//        {
//            print("isempty")
//            imgExercise.sd_setImage(with: URL(string: obj.exerciseImage), placeholderImage: AppInfo.placeholderImage)
//        }
//        else
//        {
//            print("coredat")
//            imgExercise.image = UIImage(data: image)
//        }
        
        lblBodyType.text =  obj.exerciseBodyParts.replacingOccurrences(of: " | ", with: ", ")
		IBbtnFavo.isSelected = obj.exerciseIsFavourite
		IBacitivtyIndicatorVideo.isHidden = true
		IBbtnVideoPlay.isHidden = true
        
        IBbtnLock.isHidden = obj.exerciseAccessLevel.uppercased() == "open".uppercased() || obj.exerciseAccessLevel.isEmpty
        
        if IBbtnLock.isHidden == true {
            self.btnvideo.isHidden = false
        }
        else
        {
            self.btnvideo.isHidden = true
        }
        
		let tapGes = UITapGestureRecognizer(target: self, action: #selector(tapOnImage))
		imgExercise.addGestureRecognizer(tapGes)
        
       // viewUpperLayer.addTapGesture(target: self, action: #selector(tapOnVideoUpperLayer))
    }
	
	@IBAction func btnFavoTap(sender: UIButton){
		likeTapped?()
	}
	
	@IBAction func btnSahreTap(sender: UIButton){
		shareTapped?()
	}
	
	@IBAction func btnVideoCloseTap(sender: UIButton){
		videoCloseBtnTap?()
	}
	
	@objc func tapOnImage(){
		imageTapped?(cellIndexPath)
	}
    
    @objc func tapOnVideoUpperLayer(){
        videoUpperLayerTapped?()
    }
	
	func setUpPlayerInCell(urlString: String)
	{
        
		let theFileName = (urlString as NSString).lastPathComponent
		
		if theFileName.isEmpty == true{
			return
		}
		IBacitivtyIndicatorVideo.isHidden = true
		
		let videoThere = DoviesGlobalUtility.checkWorkOutDownloadedOrNot(folderName: theFileName)
		if videoThere == false{
			IBacitivtyIndicatorVideo.isHidden = false
           // viewUpperLayer.isHidden = false
			IBacitivtyIndicatorVideo.startAnimating()
			downloadRequest = ModelVideoListing.downloadVideoForExercise(sVideoZipUrl: urlString, sFileName: theFileName, currentProgress: { (progress, url, id) in
				
			}, response: { (downloadResponce, id) in
				if downloadResponce.error == nil{
                    self.playVideoFromDocumentDirectory(videoLastPathComponent: theFileName)
				}
				else{
				}
				self.IBacitivtyIndicatorVideo.isHidden = true
				self.IBacitivtyIndicatorVideo.stopAnimating()
			})
		}
		else{
			self.playVideoFromDocumentDirectory(videoLastPathComponent: theFileName)
		}
	}
	
	func playVideoFromDocumentDirectory(videoLastPathComponent: String){
		
		let urlString = DoviesGlobalUtility.pathForVideoFile(videoFileName: videoLastPathComponent)
		
		player = AVPlayer()
		
		playerLayer = AVPlayerLayer(player: player)
		playerLayer.videoGravity = .resizeAspect;
		playerLayer.backgroundColor = UIColor.clear.cgColor
		if (playerLayer.superlayer != nil){
			playerLayer.removeFromSuperlayer()
		}
		
		playerLayer.frame =  CGRect(x: 0, y: 0, width: viewVideoPlayer.bounds.width, height: 200);
		
		viewVideoPlayer.layer.addSublayer(playerLayer);
		player.actionAtItemEnd = .pause
		viewVideoPlayer.playerLayer = playerLayer;
		
		let asset = AVURLAsset(url: URL(string: urlString.absoluteString)!)
		
		playerItem = AVPlayerItem(asset: asset)
		self.player.replaceCurrentItem(with: self.playerItem)
		
		self.playVideoForCell()
		self.playerEndObserver()
	}
	
	@IBAction func playVideoForCell(){
		self.playerLayer.player?.play()
		IBbtnVideoPlay.isHidden = true
	}
	
	func playerEndObserver(){
		NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: .main) { [weak self] _ in
			self?.player.seek(to: kCMTimeZero)
			self?.playerLayer.player?.play()
		}
	}
	
	func stopVideo(){
		downloadRequest?.cancel()
		if (player != nil){
			if player.isPlaying{
				self.player.pause()
				IBbtnVideoPlay.isHidden = false
			}
		}
	}
	
	func removePlayerFromView(){
		if (player != nil){
			playerLayer.removeFromSuperlayer()
		}
	}
	
	func setupForCreateWorkout() {
		self.IBbtnFavo.isHidden = true
		self.IBbtnShare.isHidden = true
		self.IBbtnLock.isHidden = true
		self.imgViewCheckBox.isHidden = false
        self.btnCheckBox.isHidden = false
        self.btnvideo.isHidden = false
	}
    
    func Fetchfromcoredata(exerciseId:String) ->  Data
    {
        let data = Data()
        let context = APP_DELEGATE.managedObjectContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ExerciseImgEntity")
        request.predicate = NSPredicate(format: "exerciseVideoId = %@", exerciseId)
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                let image = data.value(forKey: "exerciseImage") as! NSData
                return image as Data
            }
        } catch {
            print("Failed")
            return  data
        }
        return  data
    }
}

