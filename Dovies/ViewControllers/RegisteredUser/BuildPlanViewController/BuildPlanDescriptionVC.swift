//
//  BuildPlanDescriptionVC.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

class BuildPlanDescriptionVC: BaseViewController {

    @IBOutlet weak var txtOverView  : UITextView!
    @IBOutlet weak var txtOverViewDetail  : UITextView!

    @IBOutlet weak var lblOverView  : UILabel!
    @IBOutlet weak var lblNoData    : UILabel!
    @IBOutlet weak var separator    : UIImageView!
    @IBOutlet weak var IBlbl_Goodfor  : UILabel!
    @IBOutlet weak var IBlbl_Equipments    : UILabel!
    @IBOutlet weak var IBView_PlanInfo    : UIView!

    
    var isFromMyPlan                : Bool = false
    var isFromEditAdmin             : Bool = false

    var buildPlanModel              : BuildPlanModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        inititalUISetUp()
    }

    override func inititalUISetUp() {
        guard let model = buildPlanModel else{return}
        
        if isFromEditAdmin
        {
            IBView_PlanInfo.isHidden = false

            var txtGoodFor = model.customerProgramDetail[0].programGoodforMasterName
            txtGoodFor = txtGoodFor?.replacingOccurrences(of: "|", with: ",", options: .literal, range: nil)
            IBlbl_Goodfor.text = txtGoodFor

            var txtEquip = model.customerProgramDetail[0].programEquipmentMasterName
            txtEquip = txtEquip?.replacingOccurrences(of: "|", with: ",", options: .literal, range: nil)
            IBlbl_Equipments.text = txtEquip

            var txtoverview = model.customerProgramDetail[0].programDescription
            txtoverview = txtoverview?.replacingOccurrences(of: "|", with: ",", options: .literal, range: nil)
            txtOverViewDetail.text = txtoverview
            
             if txtoverview == ""
             {
              txtOverViewDetail.text  = "No overview found"
           
            }
        }
        else
        {
            IBView_PlanInfo.isHidden = true
        }
        
        txtOverView.text = model.customerProgramDetail[0].programDescription
        txtOverView.isEditable = !isFromMyPlan
        separator.isHidden = isFromMyPlan
        if isFromMyPlan{
            lblNoData.isHidden = !model.customerProgramDetail[0].programDescription.isEmpty
        }
        
        lblOverView.text = isFromMyPlan ? "" : "Overview"
        
       
    }
    
    func saveData(){
//        if let text = txtOverView.text, text.isEmpty{
//            DoviesGlobalUtility.showMessageWith(title: AlertTitle.appName, subTitle: "Please add overview", theme: .error)
//        }else{
            view.endEditing(true)
            callUpdateOverViewApi()
//        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension BuildPlanDescriptionVC : UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
         BuildPlanViewController.isDataChanged = true
    }
}

//API calls
extension BuildPlanDescriptionVC{
    
    func callUpdateOverViewApi(){
        guard let user = DoviesGlobalUtility.currentUser ,let pId = buildPlanModel?.customerProgramDetail[0].programId else{return}
        var params = [String : Any]()
        params[WsParam.authCustomerId] = user.customerAuthToken
        params[WsParam.programId] = pId
        params[WsParam.programDescription] = txtOverView.text
        DoviesWSCalls.callEditProgramDetailsAPI(dicParam: params, onSuccess: { (success, message, response) in
            if success{
				GlobalUtility.showToastMessage(msg: "Done")
//                DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.updatePlanSuccess , completion: nil)
                if !self.isFromMyPlan {
                    if let viewControllers = self.navigationController?.viewControllers{
                        for vc in viewControllers{
                            if let myplansVc = vc as? MyPlanVC{
                                myplansVc.isReload = true
                                self.navigationController?.popToViewController(myplansVc, animated: true)
                                return
                            }
                        }
                        let myPlanVC = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "MyPlanVC") as! MyPlanVC
                        myPlanVC.isReload = true
                        self.navigationController?.viewControllers.insert(myPlanVC, at: 1)
                        self.navigationController?.popToViewController(myPlanVC, animated: true)
                    }
                }else{
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }) { (error) in
        }
    }
}
