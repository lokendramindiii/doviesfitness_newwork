//
//  CreateWorkoutCell.swift
//  Dovies
//
//  Created by hb on 26/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

protocol CreateWorkoutCellDelegate {
    func toggleOption(_ option:String, _ index: Int)
    func txtDidBeginEditing(_ textField: UITextField)
    func txtDidEndEditing(_ time: String, _ reps: String, _ breaktime: String, _ totaltime: String, _ index: Int)
    func btnTimeSelectionValue(_ time: String, _ index: Int)
    func btnTotalTimeSelectionValue(_ totaltime: String, _ index: Int)
    func btnBreakTimeSelectionValue(_ breaktime: String, _ index: Int)
    func btnRepsSelectionValue(_ RepsNo: String, _ index: Int)

}

class CreateWorkoutCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet  var bottomConstraintCell: NSLayoutConstraint!
    
    @IBOutlet var lblbreaktime: UILabel!
    @IBOutlet var btnDelete: UIButton!
    @IBOutlet var btnDuplicate: UIButton!
    @IBOutlet var btnVideo: UIButton!

    @IBOutlet var breakTimeBottomView: NSLayoutConstraint!
    @IBOutlet var totalTimeView: UIView!
    @IBOutlet var btnTime: UIButton!
    @IBOutlet var btnReps: UIButton!
    @IBOutlet var txtfldTime: UITextField!
    @IBOutlet var txtfldBreakTime: UITextField!
    @IBOutlet var txtfldReps: UITextField!
    @IBOutlet var breakView: UIView!
    @IBOutlet var txtfldTotalTime: UITextField!
    @IBOutlet var lblTitleExercise: UILabel!
    @IBOutlet var imgExercise: UIImageView!
    
    @IBOutlet var btnBreakTimeSelection: UIButton!
    @IBOutlet var btnTotalTimeSelection: UIButton!
    @IBOutlet var btnTimeSelection: UIButton!
    @IBOutlet var btnRepsSelection: UIButton!

    @IBOutlet var lblTimeSelection: UILabel!
    // loks add * on label
    @IBOutlet var lblSelectNoofReps: UILabel!
    @IBOutlet var lblTimetoFinsihReps: UILabel!

    @IBOutlet var lbl_TimeCircle: UILabel!
    @IBOutlet var lbl_RepsCircle: UILabel!

    // loks add video module
    @IBOutlet weak var constPlayerHeight: NSLayoutConstraint!
    @IBOutlet weak var IBbtnVideoClose: UIButton!
    @IBOutlet weak var IBbtnVideoPlay: UIButton!
    @IBOutlet weak var viewVideoPlayer: VideoContainerView!
    
    @IBOutlet weak var IBacitivtyIndicatorVideo: UIActivityIndicatorView!
    
    var downloadRequest: DownloadRequest?
    
    var playerItem: AVPlayerItem!
    var player: AVPlayer!
    var playerLayer: AVPlayerLayer!
    
    var favouriteTap:(() -> ())?
    var btnDetailTap:(() -> ())?
    var imageTapped:(() -> ())?
    var videoCloseBtnTap:(() -> ())?
    var videoUpperLayerTapped:(()->())?
    var videoDownloaded:(()->())?
    
//
    var delegate: CreateWorkoutCellDelegate?
    var model:Any?
    
    @IBAction func btnTimeSelection_Tapped(_ sender: UIButton) {
        self.delegate?.btnTimeSelectionValue((self.btnTimeSelection.titleLabel?.text)!, self.tag)
    }
    
    @IBAction func btnTotalTimeSelection_Tapped(_ sender: UIButton) {
        self.delegate?.btnTotalTimeSelectionValue((self.btnTotalTimeSelection.titleLabel?.text)!, self.tag)
    }
    
    @IBAction func btnBreakTimeSelection_Tapped(_ sender: UIButton){
        self.delegate?.btnBreakTimeSelectionValue((self.btnBreakTimeSelection.titleLabel?.text)!, self.tag)
    }
    
    @IBAction func btnRepsSelection_Tapped(_ sender: UIButton) {
        
        self.delegate?.btnRepsSelectionValue((self.btnRepsSelection.titleLabel?.text)!, self.tag)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setLayoutofTextField(){
      //  btnTimeSelection.layer.borderColor = (UIColor.lightGray).cgColor
        btnTimeSelection.layer.borderColor = UIColor(red:231/255, green:231/255, blue:231/255, alpha: 1).cgColor
        btnTimeSelection.layer.borderWidth = 1.0
        btnTimeSelection.layer.cornerRadius = 5.0
        // loks comment code
      //  txtfldReps.layer.borderColor = (UIColor.lightGray).cgColor
        btnRepsSelection.layer.borderColor = UIColor(red:231/255, green:231/255, blue:231/255, alpha: 1).cgColor
        btnRepsSelection.layer.borderWidth = 1.0
        btnRepsSelection.layer.cornerRadius = 5.0
        
      //  btnTotalTimeSelection.layer.borderColor = (UIColor.lightGray).cgColor
         btnTotalTimeSelection.layer.borderColor = UIColor(red:231/255, green:231/255, blue:231/255, alpha: 1).cgColor
        btnTotalTimeSelection.layer.borderWidth = 1.0
        btnTotalTimeSelection.layer.cornerRadius = 5.0

      //  btnTotalTimeSelection.layer.borderColor = (UIColor.lightGray).cgColor
         btnTotalTimeSelection.layer.borderColor = UIColor(red:231/255, green:231/255, blue:231/255, alpha: 1).cgColor
        btnTotalTimeSelection.layer.borderWidth = 1.0
        btnTotalTimeSelection.layer.cornerRadius = 5.0

     //   btnBreakTimeSelection.layer.borderColor = (UIColor.lightGray).cgColor
        btnBreakTimeSelection.layer.borderColor = UIColor(red:231/255, green:231/255, blue:231/255, alpha: 1).cgColor
        btnBreakTimeSelection.layer.borderWidth = 1.0
        btnBreakTimeSelection.layer.cornerRadius = 5.0
        
        // loks chage textfield to button
      //  txtfldReps.leftView = UIView(frame: CGRect(x: -50, y: 0, width: 10, height: self.txtfldReps.frame.height))
      //  txtfldReps.leftViewMode = .always
    }
    
    func setCellContent(_ model:ExerciseDetails){
        
        IBbtnVideoPlay.isHidden = true
        lblTitleExercise.text = model.exerciseName
      //  imgExercise.sd_setImage(with: URL(string: model.exerciseImage), placeholderImage: AppInfo.placeholderImage)
        
        imgExercise.sd_setImage(with: URL(string: model.exerciseImage), placeholderImage: nil)

        btnBreakTimeSelection.setTitle(model.breakTimeValue, for: .normal)
        if model.excerciseOption == "Time"{
            totalTimeView.isHidden = true
            btnTime.setImage(UIImage(named: "btn_selexc")?.withRenderingMode(.alwaysOriginal), for: .normal)
            btnReps.setImage(UIImage(named: "btn_unselexc")?.withRenderingMode(.alwaysOriginal), for: .normal)
            
            lbl_RepsCircle.textColor = UIColor.lightGray
            lbl_RepsCircle.text = "Reps"
            lbl_TimeCircle.textColor = UIColor(r: 33/255.0, g: 33/255.0, b: 33/255.0, alpha: 1.0)
            self.btnRepsSelection.isHidden = true
            self.btnTimeSelection.isHidden = false
            self.lblTimeSelection.isHidden = false
        }
        else{
            totalTimeView.isHidden = false
            //        bottomConstraintCell.constant = 15
            btnTime.setImage(UIImage(named: "btn_unselexc")?.withRenderingMode(.alwaysOriginal), for: .normal)
            btnReps.setImage(UIImage(named: "btn_selexc")?.withRenderingMode(.alwaysOriginal), for: .normal)
            lbl_RepsCircle.textColor = UIColor(r: 33/255.0, g: 33/255.0, b: 33/255.0, alpha: 1.0)
            lbl_RepsCircle.text = "Reps"
            lbl_TimeCircle.textColor = UIColor.lightGray
            self.btnRepsSelection.isHidden = false
            self.btnTimeSelection.isHidden = true
            self.lblTimeSelection.isHidden = true

        }
        
        if  model.timeValue == "" {
            btnTimeSelection.setTitle("00:00", for: .normal)
            lblTimeSelection.attributedText =      fun_attributedstring_forcolor(main_string: "Select time *", string_to_color: "*")
        }
        else{
            btnTimeSelection.setTitle(String(model.timeValue), for: .normal)
            if model.timeValue == "00:00"
            {
                lblTimeSelection.attributedText =      fun_attributedstring_forcolor(main_string: "Select time *", string_to_color: "*")
            }
            else
            {
                lblTimeSelection.text = "Select time"
            }
        }
        // TotalTime value
        if  model.totalTimeValue == "" {
            btnTotalTimeSelection.setTitle("00:00", for: .normal)
            lblTimetoFinsihReps.attributedText =      fun_attributedstring_forcolor(main_string: "Time to finish Reps *", string_to_color: "*")

        }
        else{
            btnTotalTimeSelection.setTitle(String(model.totalTimeValue), for: .normal)
            if model.totalTimeValue == "00:00"
            {
                lblTimetoFinsihReps.attributedText =      fun_attributedstring_forcolor(main_string: "Time to finish Reps *", string_to_color: "*")
            }
            else
            {
                lblTimetoFinsihReps.text = "Time to finish Reps"
            }
            
        }
        
        // Reps no of value
        if  model.repsValue == "" {
            btnRepsSelection.setTitle("00", for: .normal)
            lblSelectNoofReps.attributedText =      fun_attributedstring_forcolor(main_string: "Select number of Reps *", string_to_color: "*")
        }
        else{
            btnRepsSelection.setTitle(String(model.repsValue), for: .normal)
            if model.repsValue == "00"
            {
                lblSelectNoofReps.attributedText =      fun_attributedstring_forcolor(main_string: "Select number of Reps *", string_to_color: "*")
            }
            else
            {
                lblSelectNoofReps.text = "Select number of Reps"
            }
        }
        
        if model.breakTimeValue == "" {
            btnBreakTimeSelection.setTitle("00:00", for: .normal)
        }
        else{
            btnBreakTimeSelection.setTitle(String(model.breakTimeValue), for: .normal)
        }    
        txtfldTime.text = model.timeValue
        txtfldReps.text = model.repsValue
        txtfldBreakTime.text = model.breakTimeValue
        txtfldTotalTime.text = model.totalTimeValue
        

    }
    
    
    func fun_attributedstring_forcolor(main_string:NSString ,string_to_color:NSString ) -> NSMutableAttributedString {
        let range = (main_string as NSString).range(of: string_to_color as String)
        let attribute = NSMutableAttributedString.init(string: main_string as String)
        attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red , range: range)
       return attribute
    }
    
    @IBAction func btnTimeTapped(_ sender: Any) {
        if self.delegate != nil{
            self.delegate?.toggleOption("Time", self.tag)
        }
     
    }
    @IBAction func btnRepsClicked(_ sender: Any) {
        if self.delegate != nil{
            self.delegate?.toggleOption("Reps", self.tag)
        }
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField){
        if self.delegate != nil{
            self.delegate?.txtDidBeginEditing(textField)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if self.delegate != nil{
            self.delegate?.txtDidEndEditing(txtfldTime.text!, txtfldReps.text!, txtfldBreakTime.text!, txtfldTotalTime.text!, self.tag)
        }
    }
    // MARK: Video section
    
    
    @objc func tapOnVideoUpperLayer(){
        videoUpperLayerTapped?()
    }
    
    func setUpPlayerInCell(urlString: String)
    {
        
        let theFileName = (urlString as NSString).lastPathComponent
        
        if theFileName.isEmpty == true{
            return
        }
        IBacitivtyIndicatorVideo.isHidden = true
        
        let videoThere = DoviesGlobalUtility.checkWorkOutDownloadedOrNot(folderName: theFileName)
        if videoThere == false{
            IBacitivtyIndicatorVideo.isHidden = false
            IBacitivtyIndicatorVideo.startAnimating()
            downloadRequest = ModelVideoListing.downloadVideoForExercise(sVideoZipUrl: urlString, sFileName: theFileName, currentProgress: { (progress, url, id) in
                
            }, response: { (downloadResponce, id) in
                if downloadResponce.error == nil{
                    self.playVideoFromDocumentDirectory(videoLastPathComponent: theFileName)
                    
                }
                else{
                    
                }
                self.IBacitivtyIndicatorVideo.isHidden = true
                self.IBacitivtyIndicatorVideo.stopAnimating()
            })
        }
        else{
            self.playVideoFromDocumentDirectory(videoLastPathComponent: theFileName)
        }
    }
    func playVideoFromDocumentDirectory(videoLastPathComponent: String){
        
        let urlString = DoviesGlobalUtility.pathForVideoFile(videoFileName: videoLastPathComponent)
        
        player = AVPlayer()
        
        playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = .resizeAspect;
        playerLayer.backgroundColor = UIColor.clear.cgColor
        
        if (playerLayer.superlayer != nil){
            playerLayer.removeFromSuperlayer()
        }
        
      playerLayer.frame =  CGRect(x: 0, y: 0, width: viewVideoPlayer.bounds.width, height: 200);
       
        
        viewVideoPlayer.layer.addSublayer(playerLayer);
        player.actionAtItemEnd = .pause
        viewVideoPlayer.playerLayer = playerLayer;
        
        let asset = AVURLAsset(url: URL(string: urlString.absoluteString)!)
        
        playerItem = AVPlayerItem(asset: asset)
        self.player.replaceCurrentItem(with: self.playerItem)
        
        self.playVideoForCell()
        self.playerEndObserver()
    }
    @IBAction func playVideoForCell(){
        self.playerLayer.player?.play()
        IBbtnVideoPlay.isHidden = true
    }
    
    func playerEndObserver(){
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: .main) { [weak self] _ in
            self?.player.seek(to: kCMTimeZero)
            self?.playerLayer.player?.play()
        }
    }
    
    func stopVideo(){
        downloadRequest?.cancel()
        if (player != nil){
            if player.isPlaying{
                self.player.pause()
               IBbtnVideoPlay.isHidden = false
            }
        }
    }
    
    func removePlayerFromView(){
        if (player != nil){
            playerLayer.removeFromSuperlayer()
        }
    }
    
}
