//
//  NewsFeedMediaDetailsVC.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit
import ReadMoreTextView
import IQKeyboardManagerSwift
import AutoScrollLabel

import AVFoundation
import MediaPlayer
import NVActivityIndicatorView

enum NewsFeedContentType : String{
    case video = "Video"
    case image = "Image"
    case text = "Text"
    case dietPlan = "DIET_PLAN"
    case workout = "WORKOUT"
    case program = "PROGRAM"
	case Exercise	= "EXERCISE"
}

class NewsFeedMediaDetailsVC: BaseViewController {
    
    
    @IBOutlet weak var activityIndicatorView : NVActivityIndicatorView!{
        didSet{
            activityIndicatorView.padding = 20
            activityIndicatorView.color = UIColor.lightGray
            activityIndicatorView.type = .circleStrokeSpin
        }
    }
    
    @IBOutlet var inputToolbar: UIView!
    @IBOutlet weak var growingTextView: GrowingTextView!
    @IBOutlet weak var textViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var constNewsFeedImageH: NSLayoutConstraint!
    
    @IBOutlet weak var imgUserProfile : UIImageView!
    @IBOutlet weak var btnUsername : UIButton!
    @IBOutlet weak var imgFeed : UIImageView!
    @IBOutlet weak var btnCommentsCount : UIButton!
    @IBOutlet weak var lbl_VideoPlaying : UILabel!
    @IBOutlet weak var View_bgComment : UIView!

    
	//@IBOutlet var btnUserProfile: UIButton!
    
    @IBOutlet weak var btnViewAllComments : UIButton!
    @IBOutlet weak var imgViewFirstComment : UIImageView!
    @IBOutlet weak var imgViewSecondComment : UIImageView!
    @IBOutlet weak var lblFirstComment : UILabel!
    @IBOutlet weak var lblSecondComment : UILabel!
    
    @IBOutlet weak var imgViewVideo : UIImageView!
    @IBOutlet weak var txtViewDescription : ReadMoreTextView!
    @IBOutlet weak var viewVideo : UIView!
    @IBOutlet weak var constSpaceImgViewFeedToStackViewWithButtons : NSLayoutConstraint!
    @IBOutlet weak var constSpaceVideoViewToStackViewWithButtons : NSLayoutConstraint!
    @IBOutlet weak var constSpaceTxtViewDescToStackViewWithButtons : NSLayoutConstraint!
    @IBOutlet weak var constBtnViewAllCommentsHeight : NSLayoutConstraint!
    @IBOutlet weak var constSpaceBtnViewAllCommentsToHorizontalLine : NSLayoutConstraint!
    @IBOutlet weak var Btn_ViewAllCommenttopConstraints : NSLayoutConstraint!

    @IBOutlet weak var txtViewDescBottom : ReadMoreTextView!
    
	@IBOutlet weak var IBactivityIndicatorForDownload: UIActivityIndicatorView!
    
    @IBOutlet weak var viewFirstComment : UIView!
    @IBOutlet weak var viewLastComment : UIView!
    @IBOutlet weak var btnCommentSend : UIButton!
    
    @IBOutlet weak var lblDays : UILabel!
    @IBOutlet weak var btnLikesCount : UIButton!
    @IBOutlet weak var btnLike : UIButton!
    @IBOutlet weak var btnFavourite : UIButton!
    @IBOutlet weak var scrollV : UIScrollView!
    @IBOutlet weak var btnChat : UIButton!

    var newsFeedId : String?
    var newsFeed : NewsFeedModel?
    var newsFeedDetails : NewsFeedDetailsModel?
    var newsFeedComments : [NewsFeedComment]?
	
	@IBOutlet weak var IBbtnPlayVideo: UIButton!
	@IBOutlet weak var IBbtnBigPlayVideo: UIButton!
	@IBOutlet weak var IBviewPlayer: UIView!
	@IBOutlet weak var IBsliderVideoSeek: UISlider!
	@IBOutlet weak var IBlblCurrentTime: UILabel!
	@IBOutlet weak var IBlblDurationTime: UILabel!
    let tableSquareCellHeight = ScreenSize.width
    let tableLandScapeCellHeight = 180*CGRect.widthRatio
    let tablePotraitCellHeight = 400*CGRect.widthRatio
    var isAllowCommenting: Bool = false
	var playerItem: AVPlayerItem!
	var player: AVPlayer!
	var playerLayer: AVPlayerLayer!
	var videoPlayer: VideoContainerView!
    var isFromShare: Bool = false
    var isShowControls: Bool = true
    var isviwallconstraints: Bool = true


    
	var downloadRequest: DownloadRequest?

    var autoHideClosure = CancellableClosure()
    var observer:Any?

    //MARK: ViewLifeCycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        inititalUISetUp()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
        tap.numberOfTapsRequired = 2
        imgFeed.isUserInteractionEnabled = true
        imgFeed.addGestureRecognizer(tap)

        
        scrollV.alpha = 0
        
        //   inputToolbar.layer.borderWidth = 0.6
        
        inputToolbar.layer.cornerRadius = 3
     //   inputToolbar.layer.borderColor =  UIColor(red: 119/255.0, green: 119/255.0, blue: 119/255.0, alpha: 1.0).cgColor

        inputToolbar.layer.borderWidth = 0.4
        inputToolbar.layer.cornerRadius = 3
        inputToolbar.layer.borderColor =  UIColor(r: 34, g: 34, b: 34, alpha: 1.0).cgColor

        inputToolbar.isHidden = !isAllowCommenting
        inputToolbar.backgroundColor = .black
        
        // Do any additional setup after loading the view.
    }
    
    @objc func doubleTapped() {
        
        
        if !btnLike.isSelected == true
        {
            
            var newsId = ""
            if let nId = newsFeed?.newsId{
                newsId = nId
            }else if let newId = newsFeedId{
                newsId = newId
            }
            updatesLikeStatus()
            callNewsFeedLikeAPI(with: newsId)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        addNavigationWhiteBackButton()
        self.navigationController?.navigationBar.isTranslucent = false
        
        //if isViewSlideVertical == false{
        callGetNewsFeedDetailsAPI()
        //        }else{
        //            if let strUrl = newsFeedDetails?.newsVideo{
        //                self.setUpVideoPlayerAndItsFrame(videoUrl: strUrl)
        //            }
        //        }
        
        IQKeyboardManager.shared.enable = false
    }
 
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isTranslucent = true

        activityIndicatorView.stopAnimating()
		downloadRequest?.cancel()
		IBactivityIndicatorForDownload.isHidden = true
		if player != nil{
			player.pause()
			IBbtnPlayVideo.isSelected = false
			IBviewPlayer.isHidden = false
		}
        IQKeyboardManager.shared.enable = true
        
        if let observer = self.observer{
            self.player.removeTimeObserver(observer)
            self.observer = nil
        }
    }
    
    deinit {
		downloadRequest?.cancel()
        NotificationCenter.default.removeObserver(self)
		if videoPlayer != nil{
			self.player.pause()
			videoPlayer.removeFromSuperview()
		}
    }
    
    //MARK: Instance methods
    
    override func inititalUISetUp() {
        
        imgUserProfile.circle()
         self.textViewLayoutConfi()
        // *** Listen to keyboard show / hide ***
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandler))
        view.addGestureRecognizer(tapGesture)
        setViewTitle()
        if let feed = newsFeed{
            switch feed.newsImageRatio.uppercased(){
            case  "squre".uppercased():
                constNewsFeedImageH.constant =  tableSquareCellHeight
            case "landscape".uppercased():
                constNewsFeedImageH.constant =  tableLandScapeCellHeight
            case "portrait".uppercased():
                constNewsFeedImageH.constant =  tablePotraitCellHeight
            default:
                constNewsFeedImageH.constant =  tableLandScapeCellHeight
            }
        }
    }
    
    @objc func tapOnPlayer(){
        
        
        isShowControls = !isShowControls
        if isShowControls{
            loadTimer()
        }
        UIView.animate(withDuration: 0.5) {
            self.IBviewPlayer.alpha = self.isShowControls ? 1.0 : 0.0
        }
        
    }
    
    func setViewTitle(){
        guard let newsFeedObj = newsFeed else{return}
        switch newsFeedObj.newsMediaType.uppercased() {
        case NewsFeedContentType.image.rawValue.uppercased():
            self.title = "Photo".uppercased()
        case NewsFeedContentType.video.rawValue.uppercased():
            self.title = "Video".uppercased()
            self.lbl_VideoPlaying.isHidden = false
            self.View_bgComment.isHidden = true
            
        case NewsFeedContentType.text.rawValue.uppercased():
            self.title = "Text".uppercased()
        default:
            break
        }
    }
    
    func textViewLayoutConfi (){
        automaticallyAdjustsScrollViewInsets = false
        growingTextView.layer.cornerRadius = 4.0
        growingTextView.maxLength = 500
        growingTextView.trimWhiteSpaceWhenEndEditing = false
        growingTextView.placeholder = "Write a Comment..."
        growingTextView.placeholderColor = UIColor(white: 0.8, alpha: 1.0)
        growingTextView.minHeight = 25.0
        growingTextView.maxHeight = 100.0
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 1.5
     //   growingTextView.typingAttributes = [NSAttributedStringKey.paragraphStyle.rawValue : style]
        
        growingTextView.typingAttributes = [NSAttributedStringKey.paragraphStyle.rawValue : style ,NSAttributedStringKey.foregroundColor: UIColor.white] as! [String : Any]
        
        growingTextView.backgroundColor = UIColor.black
        
    }
    
    @objc private func keyboardWillChangeFrame(_ notification: Notification) {
        if let endFrame = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue, let height = self.navigationController?.navigationBar.frame.size.height {
            
            var keyboardHeight = view.bounds.height - endFrame.origin.y + height + 17

            keyboardHeight = keyboardHeight > 0 ?  IS_IPHONE_X ? keyboardHeight - 10 : keyboardHeight  : 0
            textViewBottomConstraint.constant = keyboardHeight

            
            
            view.updateConstraintsIfNeeded()
            view.layoutIfNeeded()
        }
    }

    @objc func tapGestureHandler() {
        view.endEditing(true)
    }
    
    //MARK: - Custom methods
    func updateUI(){
        if let newsFeedDetailsObj = newsFeedDetails{
            updateProfilesInfo(with: newsFeedDetailsObj)
            textImageVideoSetup(with: newsFeedDetailsObj)
            setDescriptionTextView(with: newsFeedDetailsObj)
        }
        updateNewsFeedComments()
    }
    
    func updateProfilesInfo(with newsFeedDetailsObj : NewsFeedDetailsModel){
        if let feed = newsFeed{
            switch feed.newsImageRatio.uppercased(){
            case "landscape".uppercased():
              //  imgFeed.sd_setImage(with:  URL(string: newsFeedDetailsObj.newsImage), placeholderImage: AppInfo.placeholderLandscapeImage)
                
                imgFeed.sd_setImage(with:  URL(string: newsFeedDetailsObj.newsImage), placeholderImage: nil)

            default:
             //   imgFeed.sd_setImage(with:  URL(string: newsFeedDetailsObj.newsImage), placeholderImage: AppInfo.placeholderImage)
                
                imgFeed.sd_setImage(with:  URL(string: newsFeedDetailsObj.newsImage), placeholderImage: nil)

            }
        }else{
         //   imgFeed.sd_setImage(with:  URL(string: newsFeedDetailsObj.newsImage), placeholderImage: AppInfo.placeholderImage)
            
            
            imgFeed.sd_setImage(with:  URL(string: newsFeedDetailsObj.newsImage), placeholderImage: nil)

        }
        
       // imgUserProfile.sd_setImage(with:  URL(string: newsFeedDetailsObj.newsCreatorProfileImage), placeholderImage: AppInfo.placeholderImage)

        imgUserProfile.sd_setImage(with:  URL(string: newsFeedDetailsObj.newsCreatorProfileImage), placeholderImage: nil)

        btnUsername.setTitle(newsFeedDetailsObj.newsCreatorName.capitalized, for: .normal)
        
        lblDays.text = newsFeedDetailsObj.newsAddedDate
		if newsFeedDetailsObj.newsLikeCount == "0" || newsFeedDetailsObj.newsLikeCount == "1"{
			btnLikesCount.setTitle("\(newsFeedDetailsObj.newsLikeCount!) like", for: .normal)
		}
		else{
        	btnLikesCount.setTitle("\(newsFeedDetailsObj.newsLikeCount!) likes", for: .normal)
		}
        
        btnLike.isSelected = newsFeedDetailsObj.newsLikeStatus
        btnFavourite.isSelected = newsFeedDetailsObj.newsFavouriteStatus
    }
    
    func updateNewsFeedComments(){
        if let newsFeedCommentsList = newsFeedComments, newsFeedCommentsList.count > 0{
            viewLastComment.isHidden = !(newsFeedCommentsList.count > 1)
            viewFirstComment.isHidden = false
            if newsFeedDetails?.newsCommentAllow == false{
                btnViewAllComments.isHidden = true
                viewFirstComment.isHidden = true
                imgViewFirstComment.isHidden =  true
                imgViewSecondComment.isHidden =  true
                viewLastComment.isHidden = true
            }
            else{
                viewFirstComment.isHidden = false
                imgViewFirstComment.isHidden =  false
                btnViewAllComments.isHidden = false
                imgViewSecondComment.isHidden =  false
                viewLastComment.isHidden = false
            }
            setComment(comment: newsFeedCommentsList[0], commentIndex: 0)
            if newsFeedCommentsList.count > 1{
                setComment(comment: newsFeedCommentsList[1], commentIndex: 1)
            }
            
                
                if newsFeedDetails?.newsCommentCount == "0" || newsFeedDetails?.newsCommentCount == "1"{
                    btnViewAllComments.setTitle("View all \(newsFeedDetails?.newsCommentCount ?? "") comment", for: .normal)
                    btnCommentsCount.setTitle("\(newsFeedDetails?.newsCommentCount ?? "") comment", for: .normal)
                }
                else{
                    btnViewAllComments.setTitle("View all \(newsFeedDetails?.newsCommentCount ?? "") comments", for: .normal)
                    btnCommentsCount.setTitle("\(newsFeedDetails?.newsCommentCount ?? "") comments", for: .normal)
                }
                
            
                if newsFeedDetails?.newsCommentCount == "1" || newsFeedDetails?.newsCommentCount == "2"
                {
                    btnViewAllComments.isHidden = true
                    constBtnViewAllCommentsHeight.constant = 10
                }
                else
                {
                    if newsFeedDetails?.newsCommentAllow == false
                    {
                        btnViewAllComments.isHidden = true
                    }
                    else
                    {
                        btnViewAllComments.isHidden = false
                        constBtnViewAllCommentsHeight.constant = 30
                    }
                }
        }else{
            btnViewAllComments.isHidden = true
            btnCommentsCount.isHidden = true
            viewFirstComment.isHidden = true
            imgViewFirstComment.isHidden =  true
            imgViewSecondComment.isHidden =  true
            viewLastComment.isHidden = true
        }
    }
    
    func setComment(comment : NewsFeedComment , commentIndex : Int){
        var attr: [NSAttributedStringKey: Any] = [NSAttributedStringKey.font :UIFont.tamilBold(size: 14.0)]
        let attrStrUserName = NSMutableAttributedString(string: "\(comment.customerName.isEmpty ? "Unknown " : comment.customerName.capitalized) ", attributes: attr)
        attr = [NSAttributedStringKey.font :UIFont.tamilRegular(size: 14.0), NSAttributedStringKey.foregroundColor: UIColor.gray]
        let attrStrComment = NSMutableAttributedString(string: comment.newsComment, attributes: attr)
        attrStrUserName.append(attrStrComment)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 3.0
        attrStrUserName.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attrStrUserName.length))
        
        if commentIndex == 0{
           // imgViewFirstComment.sd_setImage(with:  URL(string: comment.customerProfileImage), placeholderImage: AppInfo.placeholderImage)
            
            imgViewFirstComment.sd_setImage(with:  URL(string: comment.customerProfileImage), placeholderImage: nil)
            
            lblFirstComment.attributedText = attrStrUserName
        }else{
           // imgViewSecondComment.sd_setImage(with:  URL(string: comment.customerProfileImage), placeholderImage: AppInfo.placeholderImage)
            
            imgViewSecondComment.sd_setImage(with:  URL(string: comment.customerProfileImage), placeholderImage: nil)
            
            lblSecondComment.attributedText = attrStrUserName
        }
        
    }

    func textImageVideoSetup(with newsFeedObj : NewsFeedDetailsModel) {
		
        if newsFeedObj.newsContentType.uppercased() == NewsFeedContentType.video.rawValue.uppercased() {
            self.imgFeed.isHidden = true
            self.viewVideo.isHidden = false
            self.imgViewVideo.isHidden = true
            self.constSpaceImgViewFeedToStackViewWithButtons.priority = UILayoutPriority(rawValue: 250)
            self.constSpaceVideoViewToStackViewWithButtons.priority = UILayoutPriority(rawValue: 750)
		
			let block: SDWebImageCompletionBlock = {(image, error, cacheType, imageURL) -> Void in
//                self.IBbtnBigPlayVideo.isHidden = false
			}
			
			//imgViewVideo.sd_setImage(with: URL(string: newsFeedObj.newsFullImage), placeholderImage: AppInfo.placeholderImage, options: SDWebImageOptions(rawValue: 0), completed: block)
            
            imgViewVideo.sd_setImage(with: URL(string: newsFeedObj.newsFullImage), placeholderImage: nil, options: SDWebImageOptions(rawValue: 0), completed: block)
            
            btnVideoPlayTap()
			
        } else if newsFeedObj.newsContentType.uppercased() == NewsFeedContentType.text.rawValue.uppercased() {
            self.imgFeed.isHidden = true
            self.txtViewDescription.isHidden = false
            self.txtViewDescBottom.isHidden = true
            self.constSpaceTxtViewDescToStackViewWithButtons.priority = UILayoutPriority(rawValue: 750)
            self.constSpaceImgViewFeedToStackViewWithButtons.priority = UILayoutPriority(rawValue: 250)
            
            self.constSpaceBtnViewAllCommentsToHorizontalLine.priority = UILayoutPriority(rawValue: 800)
        }
    }
    
    func setDescriptionTextView(with newsFeedObj : NewsFeedDetailsModel) {
        guard !newsFeedObj.newsDescription.isEmpty else {
            if isviwallconstraints == true
            {
                isviwallconstraints = false
                Btn_ViewAllCommenttopConstraints.isActive = false
            }
            return}
        if (newsFeedObj.newsContentType.uppercased() == NewsFeedContentType.text.rawValue.uppercased()){
            Btn_ViewAllCommenttopConstraints.isActive = true

            self.txtViewDescription.shouldTrim = false
            self.txtViewDescription.isScrollEnabled = false
            self.txtViewDescription.textContainerInset = UIEdgeInsets.zero
            self.txtViewDescription.textContainer.lineFragmentPadding = 0
            
            let style = NSMutableParagraphStyle()
            style.lineSpacing = 3.0
            let TextColor   = UIColor(red: 36/255.0, green: 36/255.0, blue: 36/255.0, alpha: 1.0)

            var attr: [NSAttributedStringKey: Any] = [NSAttributedStringKey.font :UIFont.tamilBold(size: 14.0), NSAttributedStringKey.kern : 0.5, NSAttributedStringKey.paragraphStyle : style]
            let attrStrUserName1 = NSMutableAttributedString(string: "\(newsFeedObj.newsCreatorName.capitalized) ", attributes: attr)
            attr = [NSAttributedStringKey.font :UIFont.tamilRegular(size: 14.0), NSAttributedStringKey.foregroundColor: TextColor, NSAttributedStringKey.paragraphStyle : style]
            let attrStrComment1 = NSMutableAttributedString(string: newsFeedObj.newsDescription, attributes: attr)
            attrStrUserName1.append(attrStrComment1)
            self.txtViewDescription.attributedText = attrStrUserName1

        }else{
             Btn_ViewAllCommenttopConstraints.isActive = true
             self.txtViewDescBottom.shouldTrim = false
            self.txtViewDescBottom.isScrollEnabled = false
            self.txtViewDescBottom.textContainerInset = UIEdgeInsets.zero
            self.txtViewDescBottom.textContainer.lineFragmentPadding = 0
            var attr: [NSAttributedStringKey: Any] = [NSAttributedStringKey.font :UIFont.tamilBold(size: 14.0),NSAttributedStringKey.foregroundColor: UIColor.white]

            let attrStrUserName1 = NSMutableAttributedString(string: "\(newsFeedObj.newsCreatorName.capitalized) ", attributes: attr)
            attr = [NSAttributedStringKey.font :UIFont.tamilRegular(size: 14.0), NSAttributedStringKey.foregroundColor: UIColor.white]
            let attrStrComment1 = NSMutableAttributedString(string: newsFeedObj.newsDescription, attributes: attr)
            
            attrStrUserName1.append(attrStrComment1)

            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 3.0
            attrStrUserName1.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attrStrUserName1.length))
            
                self.txtViewDescBottom.attributedText = attrStrUserName1

        }
        
    }
    
    func loadTimer() {
        self.autoHideClosure.cancelled = true
        let cancelClosure = CancellableClosure()
        cancelClosure.closure = {
            self.isShowControls = false
            UIView.animate(withDuration: 0.5) {
                self.IBviewPlayer.alpha = 0.0
            }
        }
        cancelClosure.runAfter(delay: 3.0)
        self.autoHideClosure = cancelClosure
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension NewsFeedMediaDetailsVC : GrowingTextViewDelegate{
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        btnCommentSend.isSelected = !textView.text.isEmpty
    }
}

//Actions
extension NewsFeedMediaDetailsVC{
    @IBAction func btnLikeTapped(sender: UIButton) {
        var newsId = ""
        if let nId = newsFeed?.newsId{
            newsId = nId
        }else if let newId = newsFeedId{
            newsId = newId
        }
        updatesLikeStatus()
        callNewsFeedLikeAPI(with: newsId)
        
    }
    
    @IBAction func btnFavoriteTapped(sender: UIButton) {
        var newsId = ""
        if let nId = newsFeed?.newsId{
            newsId = nId
        }else if let newId = newsFeedId{
            newsId = newId
        }
        updatesFavouriteStatus()
        callNewsFeedFavouriteAPI(with: newsId)
    }
    
    @IBAction func btnChatTapped(sender: UIButton) {
        let comments = self.storyboard?.instantiateViewController(withIdentifier: "CommentsVC")as! CommentsVC
        comments.newsFeed = newsFeed
        comments.newsFeedId = newsFeedId
        comments.isAllowCommentsAdd = isAllowCommenting
        self.navigationController?.pushViewController(comments, animated: true)
        
    }
    
    @IBAction func btnShareTapped(sender: UIButton) {
//        var newsId = ""
//        if let nId = newsFeed?.newsId{
//            newsId = nId
//        }else if let newId = newsFeedId{
//            newsId = newId
//        }
//        DoviesGlobalUtility.generateBranchIoUrl(with: ModuleName.newsFeed, moduleId: newsId, completionHandler: { (urlString) in
//            DoviesGlobalUtility.showShareSheet(on: self, shareText: urlString)
//        })
        if let feedObj = newsFeedDetails{
            DoviesGlobalUtility.showShareSheet(on: self, shareText: feedObj.newsShareUrl)
        }
    }

    @IBAction func btnViewAllCommentsTapped(sender: UIButton) {
        let comments = self.storyboard?.instantiateViewController(withIdentifier: "CommentsVC")as! CommentsVC
        comments.newsFeed = newsFeed
        comments.newsFeedId = newsFeedId
        comments.isAllowCommentsAdd = isAllowCommenting
        self.navigationController?.pushViewController(comments, animated: true)
    }
    
    @IBAction func sendCommentTapped(_ sender : UIButton){
        //API Call
		if self.growingTextView.text.isEmptyAfterRemovingWhiteSpacesAndNewlines() == true {
			return
		}
		callPostCommentAPI()
		self.growingTextView.resignFirstResponder()
    }
    
}

//API calls
extension NewsFeedMediaDetailsVC{
    
    /**
     This method used to call the api getting news feed details.
     */
    func callGetNewsFeedDetailsAPI(){
        var newsId = ""
        if let nId = newsFeed?.newsId{
            newsId = nId
        }else if let newId = newsFeedId{
            newsId = newId
        }
      
        // loks comment show loader 17-12-2019 and add code  self.view.isUserInteractionEnabled = false
     //   scrollV.showLoader()
        
        self.view.isUserInteractionEnabled = false
        
        self.activityIndicatorView.startAnimating()
        guard let cUser = DoviesGlobalUtility.currentUser  else {return}
        var params = [String: Any]()
        params[WsParam.authCustomerId] = cUser.customerAuthToken
        params[WsParam.newsId] = newsId
        //params[WsParam.authCustomerSubscription] = ""
        params[WsParam.authCustomerSubscription] =  DoviesGlobalUtility.currentUser?.customerUserType ?? ""
        
        DoviesWSCalls.callGetNewsFeedDetailsAPI(dicParam: params, onSuccess: { [weak self] (success, feedDetail, commentsList, message) in
            
            self?.view.isUserInteractionEnabled = true
            guard let weakSelf = self else{return}
            if success{
                weakSelf.scrollV.hideLoader()
                weakSelf.newsFeedDetails = feedDetail
                
                
                self!.inputToolbar.isHidden = !(feedDetail?.newsCommentAllow)!
                self!.isAllowCommenting = (feedDetail?.newsCommentAllow)!
                self?.btnChat.isHidden = !(feedDetail?.newsCommentAllow)!
                self?.btnViewAllComments.isHidden = !(feedDetail?.newsCommentAllow)!
                weakSelf.newsFeedComments = commentsList
                
                weakSelf.updateUI()
                UIView.animate(withDuration: 0.2, animations: {
                    weakSelf.scrollV.alpha = 1
                })
                
                self?.lbl_VideoPlaying.isHidden = true
                self?.activityIndicatorView.stopAnimating()
                
                if feedDetail?.newsCommentAllow == false
                {
                  self?.View_bgComment.isHidden = true
                }
                else
                {
                    self?.View_bgComment.isHidden = false
                }
                
            }
            else
            {
                self?.activityIndicatorView.stopAnimating()
                self?.lbl_VideoPlaying.isHidden = true
                self?.View_bgComment.isHidden = true
                
                if message == "No details found!"
                {
                    GlobalUtility.showToastMessage(msg: "No longer available.")
                }
            }
            
        }) {(error) in
            self.activityIndicatorView.stopAnimating()
                self.lbl_VideoPlaying.isHidden = true
             self.view.isUserInteractionEnabled = true
        }
    }
    
    /**
     This method used to like or dislike a news feed
     */
    func callNewsFeedLikeAPI(with newsFeedId : String){
        guard let cUser = DoviesGlobalUtility.currentUser else {return}
        var dic = [String: Any]()
        dic[WsParam.authCustomerId] = cUser.customerAuthToken
        dic[WsParam.moduleId] = newsFeedId
        dic[WsParam.moduleName] = ModuleName.newsFeed
        btnLike.isUserInteractionEnabled = false
        
        DoviesWSCalls.callLikeDislikeAPI(dicParam: dic, onSuccess: { [weak self](success, message, responseDict) in
            guard let weakSelf = self else{return}
            weakSelf.btnLike.isUserInteractionEnabled = true
            
            if !success{
                weakSelf.updatesLikeStatus()
                
            }
        }) { [weak self](error) in
            
            guard let weakSelf = self else{return}
            weakSelf.btnLike.isUserInteractionEnabled = true
            weakSelf.updatesLikeStatus()
            
        }
        
    }
    
    /**
     This method used to favourite the news feed and update the status of the feed in the UI
     */
    func callNewsFeedFavouriteAPI(with newsFeedId : String){
        btnFavourite.isUserInteractionEnabled = false
        guard let cUser = DoviesGlobalUtility.currentUser else{return}
        var parameters = [String : Any]()
        parameters[WsParam.authCustomerId] =  cUser.customerAuthToken
        parameters[WsParam.moduleId] = newsFeedId
        parameters[WsParam.moduleName] = ModuleName.newsFeed
        
        DoviesWSCalls.callFavouriteAPI(dicParam: parameters, onSuccess: {[weak self] (success, message, response) in
            guard let weakSelf = self else{return}
            weakSelf.btnFavourite.isUserInteractionEnabled = true
            
            if !success{
                weakSelf.updatesFavouriteStatus()
                
            }
        }) { [weak self] (error) in
            guard let weakSelf = self else{return}
            weakSelf.btnLike.isUserInteractionEnabled = true
            weakSelf.updatesFavouriteStatus()
        }
        
    }
    
    /**
     This method used to post a comment for particular news feed.
     */
    func callPostCommentAPI(){
        guard let cUser = DoviesGlobalUtility.currentUser  else {return}
        var newsId = ""
        if let nId = newsFeed?.newsId{
            newsId = nId
        }else if let newId = newsFeedId{
            newsId = newId
        }
        GlobalUtility.showActivityIndi(viewContView: self.view)
        var params = [String: Any]()
        params[WsParam.authCustomerId] = cUser.customerAuthToken
        params[WsParam.newsId] = newsId
        params[WsParam.commentText] = self.growingTextView.text.removeWhiteSpace()
        GlobalUtility.showActivityIndi(viewContView: self.view)
		self.growingTextView.text = ""
        DoviesWSCalls.callPostNewsCommentAPI(dicParam: params, onSuccess: {[weak self] (success, message, commentObj) in
            guard let weakSelf = self else{return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
            if success, let newsComment = commentObj{
                if weakSelf.newsFeedComments != nil{
                    weakSelf.newsFeedComments?.insert(newsComment, at: 0)
                }else{
                    weakSelf.newsFeedComments = [NewsFeedComment]()
                    weakSelf.newsFeedComments?.append(newsComment)
                }
                weakSelf.updateNewsFeedComments()
                weakSelf.growingTextView.text = ""
                if let details = weakSelf.newsFeedDetails{
                    if let count = NSInteger(details.newsCommentCount){
                        details.newsCommentCount = "\(count + 1)"
                        if weakSelf.newsFeed != nil{
                            weakSelf.newsFeed!.feedCommentsCount = "\(count + 1)"
                        }
                    }else{
                        details.newsCommentCount = "1"
                        if weakSelf.newsFeed != nil{
                            weakSelf.newsFeed!.feedCommentsCount = "1"
                        }
                    }

                    if let newsFeedVC = weakSelf.navigationController?.viewControllers[0] as? NewsFeedViewController{
                        newsFeedVC.updateTableCells?()
                    }
                    
                    if details.newsCommentCount == "0" || details.newsCommentCount == "1"
                    {
                    weakSelf.btnViewAllComments.setTitle("View all \(details.newsCommentCount ?? "") comment", for: .normal)
                    weakSelf.btnCommentsCount.setTitle("\(details.newsCommentCount ?? "") comment", for: .normal)
                    }
                    else
                    {
                        weakSelf.btnViewAllComments.setTitle("View all \(details.newsCommentCount ?? "") comments", for: .normal)
                        weakSelf.btnCommentsCount.setTitle("\(details.newsCommentCount ?? "") comments", for: .normal)
                    }
                    
                    if details.newsCommentCount == "1" || details.newsCommentCount == "2"
                    {
                        self!.btnViewAllComments.isHidden = true
                        self!.constBtnViewAllCommentsHeight.constant = 10

                    }
                    else
                    {
                        self!.btnViewAllComments.isHidden = false
                        self!.constBtnViewAllCommentsHeight.constant = 30
                    }
                }
            }
			self?.btnCommentSend.isSelected = false
        }) { [weak self](error) in
            guard let weakSelf = self else{return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
        }
    }
    
    /**
     This method used to updates the likes count.
     */
    func updatesLikeStatus(){
        btnLike.isSelected = !btnLike.isSelected
        if newsFeed != nil{
            newsFeed!.newsLikeStatus = !newsFeed!.newsLikeStatus
            
            if let likesCount = Int(newsFeed!.customerLikes){
                var lCount = likesCount
                if newsFeed!.newsLikeStatus{
                    lCount += 1
                }else{
                    lCount -= 1
                }
                newsFeed!.customerLikes = "\(lCount)"
            }
        }
        if let newsFeedVC = self.navigationController?.viewControllers[0] as? NewsFeedViewController{
            newsFeedVC.updateTableCells?()
        }
        
        if let details = newsFeedDetails{
            details.newsLikeStatus = !details.newsLikeStatus
            if details.newsLikeStatus{
                if let count = NSInteger(details.newsLikeCount){
                    details.newsLikeCount = "\(count + 1)"
                }
            }else{
                if let count = NSInteger(details.newsLikeCount), count > 0{
                    details.newsLikeCount = "\(count - 1)"
                }
            }
            if details.newsLikeCount == "1" || details.newsLikeCount == "0"
            {
                btnLikesCount.setTitle("\(details.newsLikeCount!) like", for: .normal)
            }
            else
            {
                btnLikesCount.setTitle("\(details.newsLikeCount!) likes", for: .normal)
            }
            
        }
    }
	
    /**
     This method used to updates the favourite status.
     */
    func updatesFavouriteStatus(){
        btnFavourite.isSelected = !btnFavourite.isSelected
         if newsFeed != nil{
            newsFeed!.newsFavStatus = !newsFeed!.newsFavStatus
        }
        if let newsFeedVC = self.navigationController?.viewControllers[0] as? NewsFeedViewController{
            newsFeedVC.updateTableCells?()
        }
        if let details = newsFeedDetails{
            details.newsFavouriteStatus = !details.newsFavouriteStatus
            
        }
    }
}

//Video Player and it's methods
extension NewsFeedMediaDetailsVC{
	@IBAction func btnVideoPlayTap(){
		
		if player != nil && player.isPlaying{
			IBbtnPlayVideo.isSelected = false
			player.pause()
			return
		}
		
		IBbtnBigPlayVideo.isHidden = true
        //change
        if let strUrl = newsFeedDetails?.newsVideo{
            self.setUpVideoPlayerAndItsFrame(videoUrl: strUrl)
        }
	}
	
   
	func setUpVideoPlayerAndItsFrame(videoUrl: String){
        
		IBbtnPlayVideo.isSelected = true
		if player != nil{
			playerLayer.player?.play()
			return
		}
       
        if videoUrl == "" || videoUrl.isEmpty{
            GlobalUtility.showToastMessage(msg: "Cannot play invalid video url.")
            return
        }
        let urlString = URL(string: videoUrl)!
		player = AVPlayer()

		playerLayer = AVPlayerLayer(player: player)
		playerLayer.videoGravity = .resizeAspectFill;
		playerLayer.backgroundColor = UIColor.clear.cgColor
		if (playerLayer.superlayer != nil){
			playerLayer.removeFromSuperlayer()
		}
        
		playerLayer.frame =  CGRect(x: 0, y: 0, width: viewVideo.bounds.width, height: viewVideo.bounds.height);
		
		videoPlayer = VideoContainerView(frame: CGRect(x: 0, y: 0, width: viewVideo.bounds.width, height: viewVideo.bounds.height))
		viewVideo.addSubview(videoPlayer)
		
		videoPlayer.layer.addSublayer(playerLayer);
		player.actionAtItemEnd = .pause
		videoPlayer.playerLayer = playerLayer;

		let asset = AVURLAsset(url: URL(string: urlString.absoluteString)!)
		
//        playerItem = AVPlayerItem(asset: asset)
        playerItem = AVPlayerItem(url: urlString)
		player.replaceCurrentItem(with: playerItem)
        
    
		videoPlayer.isHidden = false
		videoPlayer.addTapGesture(target: self, action: #selector(tapOnPlayer))
        loadTimer()
		viewVideo.bringSubview(toFront: videoPlayer)
		
		viewVideo.bringSubview(toFront: IBbtnPlayVideo)
		viewVideo.bringSubview(toFront: IBviewPlayer)
		IBviewPlayer.isHidden = false
		playerLayer.player?.play()
		

		IBsliderVideoSeek.minimumValue = 0
		IBsliderVideoSeek.maximumValue = Float(CMTimeGetSeconds(asset.duration))
		IBsliderVideoSeek.isContinuous = true
		
		let dMinutes: Int = Int(IBsliderVideoSeek.maximumValue) % 3600 / 60
		let dSeconds: Int = Int(IBsliderVideoSeek.maximumValue) % 3600 % 60
		IBlblCurrentTime.text = "00:00"
		IBlblDurationTime.text = "\(String(format: "%02d", dMinutes)):\(String(format: "%02d", dSeconds))"
		
		playerEndObserver()
		IBsliderVideoSeek.addTarget(self, action: #selector(onSliderValChanged(slider:event:)), for: .valueChanged)
        
          self.observer = player!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, 1), queue: DispatchQueue.main) {[weak self] (CMTime) -> Void in

             guard let strongSelf = self else {return}

            let status = strongSelf.player!.currentItem?.status
			if status == .readyToPlay {
				let time : Float64 = CMTimeGetSeconds(strongSelf.player!.currentTime());
				if strongSelf.IBbtnPlayVideo.isSelected == true{
					strongSelf.IBsliderVideoSeek!.value = Float ( time );
				}
				let dMinutes: Int = Int(time) % 3600 / 60
				let dSeconds: Int = Int(time) % 3600 % 60
				strongSelf.IBlblCurrentTime.text = "\(String(format: "%02d", dMinutes)):\(String(format: "%02d", dSeconds))"
                
                if strongSelf.player.currentItem?.isPlaybackLikelyToKeepUp == true {
                    print("Playing ")
                } else if strongSelf.player!.currentItem?.isPlaybackBufferEmpty == true {
                    print("Buffer empty - show loader")
                    //strongSelf.IBactivityIndicatorForDownload.isHidden = false

                }  else if strongSelf.player!.currentItem?.isPlaybackBufferFull == true {
                    print("Buffer full - hide loader")
                    //strongSelf.IBactivityIndicatorForDownload.isHidden = true

                } else {
                    print("Buffering ")
                    //strongSelf.IBactivityIndicatorForDownload.isHidden = false

                }
			}
            else if status == .failed{
               print("Error = \(strongSelf.player!.currentItem?.error.debugDescription)")
                //strongSelf.IBactivityIndicatorForDownload.isHidden = true

            }
		}
	}

	@objc func onSliderValChanged(slider: UISlider, event: UIEvent) {
        
        
		let seconds : Int64 = Int64(IBsliderVideoSeek.value)
		let targetTime:CMTime = CMTimeMake(seconds, 1)
		player!.seek(to: targetTime)
		
		if let touchEvent = event.allTouches?.first {
			switch touchEvent.phase {
			case .began:
				print("began")
				player.pause()
				IBbtnPlayVideo.isSelected = false
                self.autoHideClosure.cancelled = true
			// handle drag began
			case .moved:
				print("moved")
                
			// handle drag moved
			case .ended:
				print("ended")
                loadTimer()
				if player!.rate == 0
				{
					playerLayer.player?.play()
					IBbtnPlayVideo.isSelected = true
				}
			// handle drag ended
			default:
				break
			}
		}
	}
	
	func playerEndObserver(){
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: .main) { [weak self] _ in
			self?.player.seek(to: kCMTimeZero)
			self?.IBbtnPlayVideo.isSelected = false
		}
	}
}


