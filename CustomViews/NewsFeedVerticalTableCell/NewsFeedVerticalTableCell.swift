//
//  NewsFeedVerticalTableCell.swift
//  Dovies
//
//  Created by Neel Shah on 16/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import ReadMoreTextView

class NewsFeedVerticalTableCell: UITableViewCell ,UITextViewDelegate      {

    
    @IBOutlet weak var imgUserProfile : UIImageView!
    @IBOutlet weak var btnUserName : UIButton!
    @IBOutlet weak var IBbtnNumberOfComments: UIButton!
    @IBOutlet weak var IBbtnNumberOfLikes: UIButton!
    let tableSquareCellHeight = ScreenSize.width
    let tableLandScapeCellHeight = 180*CGRect.widthRatio
    let tablePotraitCellHeight = 400*CGRect.widthRatio
    var iPath : IndexPath!
    
    @IBOutlet weak var txtViewDescription : ReadMoreTextView!
    @IBOutlet weak var View_DobleGesture : UIView!

    @IBOutlet weak var btnFavourite : UIButton!
    @IBOutlet weak var imgBackground : UIImageView!
    @IBOutlet weak var btnLike : UIButton!
    @IBOutlet weak var btnComment : UIButton!
    @IBOutlet weak var btnShare : UIButton!
    @IBOutlet weak var btnMore : UIButton!
    @IBOutlet weak var btnMoreHeightConstraint : NSLayoutConstraint!

    @IBOutlet weak var lblTime : UILabel!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var btnLock : UIButton!
    var newsFeed : NewsFeedModel!
	@IBOutlet weak var IBimgIsVideo: UIImageView!
    @IBOutlet weak var IBbtnNew: UIButton!
    
    @IBOutlet weak var IBConstImageViewH: NSLayoutConstraint!
    @IBOutlet weak var IBConstReadMoreViewH: NSLayoutConstraint!
    @IBOutlet weak var IBConst_TextviewBottom: NSLayoutConstraint!

    
    @IBOutlet weak var IBviewTitle : UIView!
    @IBOutlet weak var IBviewComment : UIView!
    

    var likeTapped:((_ newsFeed : NewsFeedModel, _ button : UIButton, _ indexP : IndexPath) -> ())?
    
    
    var commentTapped:((_ newsFeed : NewsFeedModel,  _ indexP : IndexPath) -> ())?
    
    
    var SingleTapGesture:((_ newsFeed : NewsFeedModel,  _ indexP : IndexPath)->())?
    
    var shareTapped:((_ newsFeed : NewsFeedModel) -> ())?
    
    var favouriteTapped:((_ newsFeed : NewsFeedModel, _ button : UIButton, _ indexP : IndexPath)->())?

    
    var addMoreTapped:((_ newsFeed : NewsFeedModel, _ button : UIButton, _ indexP : IndexPath) -> ())?

    
    override func awakeFromNib() {
        super.awakeFromNib()


        txtViewDescription.delegate = self
        selectionStyle = .none
        imgUserProfile.circle()
        let path = UIBezierPath(roundedRect:IBbtnNew.bounds,
                                byRoundingCorners:[.topRight],
                                cornerRadii: CGSize(width: 5.0, height:  5.0))
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        IBbtnNew.layer.mask = maskLayer

    }
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    func setNewsFeedCellUI(newsFeed:NewsFeedModel, indexPath : IndexPath , isFromNewsFeed : Bool)
    {
        
        if isFromNewsFeed
        {
            
            btnShare.setImage(UIImage(named: "btn_shareNew"), for: .normal)
            
            btnComment.setImage(UIImage(named: "ico_comment_nf"), for: .normal)
            
            btnLike.setImage(UIImage(named: "icon_likeNew"), for: .normal)
            btnLike.setImage(UIImage(named: "icon_like_hNew"), for: .selected)
            
            btnFavourite.setImage(UIImage(named: "btn_fav_s21aNew"), for: .normal)
            btnFavourite.setImage(UIImage(named: "favorite_icon_s34_hNewsfeed"), for: .selected)
            
            contentView.backgroundColor = UIColor.appBlackColorNew()
            IBviewTitle.backgroundColor = UIColor.appBlackColorNew()
            IBviewComment.backgroundColor = UIColor.appBlackColorNew()
            btnUserName.setTitleColor(UIColor.white, for: .normal)
            lblTime.textColor = UIColor(r: 130, g: 130, b: 130, alpha: 1.0)
            
            IBbtnNumberOfComments.setTitleColor(UIColor(r: 130, g: 130, b: 130, alpha: 1.0), for: .normal)
            
            IBbtnNumberOfLikes.setTitleColor(UIColor.white, for: .normal)
            
            txtViewDescription.backgroundColor = UIColor.appBlackColorNew()
        }
        
        btnMoreHeightConstraint.constant = 22
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(doubleTapped))
        tap.numberOfTapsRequired = 2
        View_DobleGesture.addGestureRecognizer(tap)
        
        iPath = indexPath
        IBbtnNew.isHidden = !newsFeed.isNew
        
        lblTime.text = newsFeed.newsPostedDays
        lblTitle.text = ""
        btnLike.isSelected = newsFeed.newsLikeStatus
        btnComment.isHidden = !(newsFeed.newsCommentAllow)
        IBbtnNumberOfComments.isHidden = !(newsFeed.newsCommentAllow)
        
        btnLock.isHidden = (newsFeed.workoutAccessLevel.uppercased() == "OPEN".uppercased() || newsFeed.workoutAccessLevel.isEmpty)
        self.newsFeed = newsFeed
        switch newsFeed.newsMediaType.uppercased() {
        case NewsFeedContentType.video.rawValue.uppercased():
            IBimgIsVideo.isHidden = false
        default:
            IBimgIsVideo.isHidden = true
        }
        IBimgIsVideo.layer.cornerRadius = IBimgIsVideo.bounds.width / 2
        imgUserProfile.sd_setImage(with: URL(string: newsFeed.newsCreatorProfileImage), placeholderImage: AppInfo.userPlaceHolder)
        
        btnUserName.setTitle(newsFeed.newsCreatorName.capitalized, for: .normal)
        
        
        if newsFeed.feedCommentsCount! == "0" {
            IBbtnNumberOfComments.setTitle("Write a Comment...", for: .normal)
            
        } else if  newsFeed.feedCommentsCount! == "1" {
            IBbtnNumberOfComments.setTitle("View " + newsFeed.feedCommentsCount + " comment", for: .normal)
        }
        else
        {
            IBbtnNumberOfComments.setTitle("View all " + newsFeed.feedCommentsCount + " comments", for: .normal)
        }
        
        if newsFeed.customerLikes == "0" || newsFeed.customerLikes == "1"
        {
            IBbtnNumberOfLikes.setTitle(newsFeed.customerLikes + " like", for: .normal)
        }
        else
        {
            IBbtnNumberOfLikes.setTitle(newsFeed.customerLikes + " likes", for: .normal)
        }
        
        switch newsFeed.newsImageRatio.uppercased(){
        case  "squre".uppercased():
            IBConstImageViewH.constant =  tableSquareCellHeight
            //   imgBackground.sd_setImage(with:  URL(string: newsFeed.newsImage), placeholderImage: AppInfo.placeholderImage)
            
            imgBackground.sd_setImage(with:  URL(string: newsFeed.newsImage), placeholderImage: nil)
            
        case "landscape".uppercased():
            IBConstImageViewH.constant =  tableLandScapeCellHeight
            // imgBackground.sd_setImage(with:  URL(string: newsFeed.newsImage), placeholderImage: AppInfo.placeholderLandscapeImage)
            
            imgBackground.sd_setImage(with:  URL(string: newsFeed.newsImage), placeholderImage: nil)
            
        case "portrait".uppercased():
            IBConstImageViewH.constant =  tablePotraitCellHeight
            // imgBackground.sd_setImage(with:  URL(string: newsFeed.newsImage), placeholderImage: AppInfo.placeholderImage)
            
            imgBackground.sd_setImage(with:  URL(string: newsFeed.newsImage), placeholderImage: nil)
            
        default:
            IBConstImageViewH.constant =  tableLandScapeCellHeight
            
            // imgBackground.sd_setImage(with:  URL(string: newsFeed.newsImage), placeholderImage: AppInfo.placeholderLandscapeImage)
            
            imgBackground.sd_setImage(with:  URL(string: newsFeed.newsImage), placeholderImage: nil)
            
        }
        setDescriptionTextView(with: newsFeed , isfromNewsFeed : isFromNewsFeed )
        btnFavourite.isSelected = newsFeed.newsFavStatus
    }
    
    func setDescriptionTextView(with newsFeedObj : NewsFeedModel , isfromNewsFeed:Bool) {
        guard !newsFeedObj.newsDescription.isEmpty else {
            IBConstReadMoreViewH.constant = 0
            btnMore.isHidden = true
            return
        }
        
        self.IBConst_TextviewBottom.constant = 10
        self.txtViewDescription.shouldTrim = true
        self.txtViewDescription.isScrollEnabled = false
        self.txtViewDescription.textContainerInset = UIEdgeInsets.zero
        self.txtViewDescription.textContainer.lineFragmentPadding = 0
        
        var attr: [NSAttributedStringKey: Any] = [NSAttributedStringKey.font :UIFont.tamilBold(size: 14.0)]
        
        
        let attrStrUserName1 = NSMutableAttributedString(string: "\(newsFeedObj.newsCreatorName.capitalized) ", attributes: attr)
        
        
        attr = [NSAttributedStringKey.font :UIFont.tamilRegular(size: 14.0)]
        
        
        if isfromNewsFeed
        {
            attrStrUserName1.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.white, range: NSRange(location: 0, length: attrStrUserName1.length))
            
            
            var attrStrDescription = NSMutableAttributedString(string: newsFeedObj.newsDescription, attributes: attr)
            
            
            attrStrDescription = NSMutableAttributedString(string: newsFeedObj.newsDescription.removingNewlines, attributes: attr)
            attrStrDescription.addAttribute(NSAttributedStringKey.foregroundColor, value:UIColor.colorWithRGB(r: 225, g: 225, b: 225, alpha: 1.0), range: NSRange(location: 0, length: attrStrDescription.length))
            
            //   let attrStrComment1 = NSMutableAttributedString(string: newsFeedObj.newsDescription.removingNewlines, attributes: attr)
            
            attrStrUserName1.append(attrStrDescription)
            // attrStrUserName1.append(attrStrComment1)
            
        }
        else
        {
            let attrStrComment1 = NSMutableAttributedString(string: newsFeedObj.newsDescription.removingNewlines, attributes: attr)
            
            attrStrUserName1.append(attrStrComment1)
        }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 3.0
        
        attrStrUserName1.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attrStrUserName1.length))
        
        self.txtViewDescription.attributedText = attrStrUserName1
        self.txtViewDescription.maximumNumberOfLines = 3
        
        let textdot = NSMakeRange(0, 3)
        let attributedTextMore = NSMutableAttributedString(string: "... more")
        
        if isfromNewsFeed
        {
            attributedTextMore.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.colorWithRGB(r: 225, g: 225, b: 225, alpha: 1.0), range: textdot)
        }
        else
        {
            attributedTextMore.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.colorWithRGB(r: 36, g: 36, b: 36, alpha: 1.0), range: textdot)
        }
        
        
        attributedTextMore.addAttributes(attr, range: textdot)
        let textRangeMore = NSMakeRange(4, 4)
        
        if isfromNewsFeed
        {
            attributedTextMore.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.colorWithRGB(r: 130, g: 130, b: 130, alpha: 1.0), range: textRangeMore)
        }
        else
        {
            attributedTextMore.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.colorWithRGB(r: 119, g: 119, b: 119, alpha: 1.0), range: textRangeMore)
        }
        
        attributedTextMore.addAttributes(attr, range: textRangeMore)
        
        self.txtViewDescription.attributedReadMoreText = attributedTextMore
        self.txtViewDescription.isUserInteractionEnabled = true
        
        let finalText = newsFeedObj.newsCreatorName + newsFeedObj.newsDescription + "... more"
        
        let finalHeight = finalText.heightWithConstrainedWidth(width: ScreenSize.width-20, font: UIFont.tamilRegular(size: 14.0), lineSpace: 3.0)
        self.IBConstReadMoreViewH.constant = finalHeight > 55 ? 55 : finalHeight
        
        
        if self.IBConstReadMoreViewH.constant > 40
        {
            btnMore.isHidden = false
        }
        else
        {
            btnMore.isHidden = true
        }
        
    }
    

    

   
    @objc func doubleTapped() {
        
        let sender = UIButton()
        if  !newsFeed.newsLikeStatus == true
        {
            btnLike.isSelected = true
            likeTapped?(newsFeed,sender, iPath)
        }
    }
    
  
    
    @IBAction func btnLikeTapped(_ sender: UIButton){
        likeTapped?(newsFeed,sender, iPath)
    }
    
    @IBAction func btn_More(_ sender: UIButton){
        addMoreTapped?(newsFeed,sender, iPath)
    }
    
    @IBAction func btnFavouriteTapped(_ sender: UIButton){
        favouriteTapped?(newsFeed,sender, iPath)
    }
    
    @IBAction func btnCommentTapped(_ sender: UIButton){
        commentTapped?(newsFeed, iPath)
    }
    
    @IBAction func btnShareTapped(_ sender: UIButton){
        shareTapped?(newsFeed)
    }
}

