//
//  PurchaseHelper.swift
//  Dovies
//
//  Created by Companyname.
//  Copyright © Companyname. All rights reserved.
//

import UIKit

//This class used to perform purchase actions
class PurchaseHelper {
    
    static var PackageMasterId :String = ""
    static var isloadRecipt  = false

    static  var successHelper : ((_ purchaseProductDetials : PurchaseDetails, _ receiptData : String ,_ receipt_Data : Data  )->())!
    
    class func payForPackage(packageId: String ,packageMasterId: String, onSuccss : @escaping (_ purchaseProductDetials : PurchaseDetails,_ receiptData : String , _ receipt_Data : Data  )->(), onFail: @escaping (_ alertToShow : UIAlertController)->()){
        successHelper = onSuccss
        PackageMasterId = packageMasterId
        isloadRecipt = true
        SwiftyStoreKit.purchaseProduct(packageId, atomically: false) { result in
            if case .success(let purchase) = result {
             
                UserDefaults.standard.set(packageId, forKey: UserDefaultsKey.iosPackageId)
               
                if purchase.needsFinishTransaction {

                    SwiftyStoreKit.fetchReceipt(forceRefresh: false) { result in
                        switch result {
                        case .success(let receiptData):
                            
                            let encryptedReceipt = receiptData.base64EncodedString(options: [])
                            
                            UserDefaults.standard.set("purchased", forKey: UserDefaultsKey.productPurchased)

                            SwiftyStoreKit.finishTransaction(purchase.transaction)
                            
                            // Arvind- If we send puchase = yes and receipt = no last time then call api to send receipt to our server ragading previously purchased plan.
                            
                            onSuccss(purchase, encryptedReceipt ,receiptData)
                            print("Fetch receipt success:\n\(encryptedReceipt)")
                        case .error(let error):
                            print("Fetch receipt failed: \(error)")
                        }
                    }
                }
            }
            if let alert = self.alertForPurchaseResult(result) {
                
                onFail(alert)
            }
        }
    }
    
    class private func alertWithTitle(_ title: String, message: String) -> UIAlertController {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alert
    }
    
    class private func alertForPurchaseResult(_ result: PurchaseResult) -> UIAlertController? {
        switch result {
        case .success(let purchase):
            print("Purchase Success: \(purchase.productId)")
//            successHelper(purchase)
            return nil
        case .error(let error):
            print("Purchase Failed: \(error)")
            switch error.code {
            case .unknown: return alertWithTitle("Purchase failed", message: error.localizedDescription)
            case .clientInvalid: // client is not allowed to issue the request, etc.
                return alertWithTitle("Purchase failed", message: "Not allowed to make the payment")
            case .paymentCancelled: // user cancelled the request, etc.
                
                if isloadRecipt == true
                {
                    sharedobjec.restorePurchases()
                    PurchaseHelper.uploadReceipt()
                }
                
            return alertWithTitle("Purchase Cancelled", message: "Your transaction was cancelled and you have not been charged.")
                
            case .paymentInvalid: // purchase identifier was invalid, etc.
                return alertWithTitle("Purchase failed", message: "The purchase identifier was invalid")
            case .paymentNotAllowed: // this device is not allowed to make the payment
                return alertWithTitle("Purchase failed", message: "The device is not allowed to make the payment")
            case .storeProductNotAvailable: // Product is not available in the current storefront
                return alertWithTitle("Purchase failed", message: "The product is not available in the current storefront")
            case .cloudServicePermissionDenied: // user has not allowed access to cloud service information
                return alertWithTitle("Purchase failed", message: "Access to cloud service information is not allowed")
            case .cloudServiceNetworkConnectionFailed: // the device could not connect to the nework
                return alertWithTitle("Purchase failed", message: "Could not connect to the network")
            case .cloudServiceRevoked: // user has revoked permission to use this cloud service
                return alertWithTitle("Purchase failed", message: "Cloud service was revoked")
                
            case .privacyAcknowledgementRequired:  // The user needs to acknowledge Apple's privacy policy


                return alertWithTitle("Purchase failed", message: "The user needs to acknowledge Apple's privacy policy")

            case .unauthorizedRequestData: // The app is attempting to use SKPayment's requestData property, but does not have the appropriate entitlement

                return alertWithTitle("Purchase failed", message: "The app is attempting to use SKPayment's requestData property, but does not have the appropriate entitlement")


            case.invalidSignature: // The cryptographic signature provided is not valid
                return alertWithTitle("Purchase failed", message: "The cryptographic signature provided is not valid")

            case .invalidOfferPrice: // The price of the selected offer is not valid (e.g. lower than the current base subscription price)

                return alertWithTitle("Purchase failed", message: " The price of the selected offer is not valid (e.g. lower than the current base subscription price)")
                

            case.invalidOfferIdentifier : // The specified subscription offer identifier is not valid
                return alertWithTitle("Purchase failed", message: "The specified subscription offer identifier is not valid")

                
            case .missingOfferParams : // One or more parameters from SKPaymentDiscount is missing
                
                return alertWithTitle("Purchase failed", message: "One or more parameters from SKPaymentDiscount is missing")

            }
        }
    }
    
    
    //MARK:Upload  current Recipt from server ----xxxx start --xxxx
    
    class private  func uploadReceipt(completion: ((_ success: Bool) -> Void)? = nil) {
        DispatchQueue.main.async {
            
            if let receiptData = self.loadReceipt() {
                DispatchQueue.main.async {
                    self.upload(receipt: receiptData)
                }
            }
        }
    }
    
    class  private func loadReceipt() -> Data? {
        
        guard let url = Bundle.main.appStoreReceiptURL else {
            return nil
        }
        do {
            let data = try Data(contentsOf: url)
            return data
        } catch {
            return nil
        }
    }
    
    class  func upload(receipt data: Data) {
        
        GlobalUtility.showActivityIndiWindow(viewContView: UIApplication.shared.keyWindow!)

        let receiptdataBase64 = data.base64EncodedString()
        
        let body = [
            "receipt-data":receiptdataBase64 ,
            "password": WsParam.itunesPassword
        ]
        let bodyData = try! JSONSerialization.data(withJSONObject: body, options: [])
        
        let url = URL(string: WsUrl.verifyReceiptUrl)!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = bodyData
        
        let task = URLSession.shared.dataTask(with: request) { (responseData, response, error) in
            
            DispatchQueue.main.async {
                
                if let responseData = responseData {
                    let json = try! JSONSerialization.jsonObject(with: responseData, options: []) as! Dictionary<String, Any>
                    
                    if let receiptInfo: NSArray = json["latest_receipt_info"] as? NSArray {
                        
                        let lastReceipt = receiptInfo.lastObject as! NSDictionary
                        
                        self.callCompletePaymentAPI(purchaseDetails: lastReceipt as! [String : String], model_Packageid: PurchaseHelper.PackageMasterId, encodedString: receiptdataBase64)
                    }
                }
            }
        }
        
        task.resume()
    }
    
    
    class private func callCompletePaymentAPI(purchaseDetails: [String : String], model_Packageid : String, encodedString : String){
        
        var  modelPackageid:String = ""
        modelPackageid = model_Packageid
        
        if UpgradeToPremiumVC.structArrayModelUpgradePremium?.getAllPackages.count ?? 0 > 0
        {
            for obj in UpgradeToPremiumVC.structArrayModelUpgradePremium!.getAllPackages
            {
                if purchaseDetails["product_id"] == obj.iosPackageId
                {
                  modelPackageid = obj.packageMasterId
                  print("ModelPackageId -- \(modelPackageid)")
                }
            }
        }
        
        
        var tansactionDetail_Info = [String: Any]()
        tansactionDetail_Info["expires_date"] = purchaseDetails["expires_date"] ?? ""
        tansactionDetail_Info["expires_date_ms"] = purchaseDetails["expires_date_ms"] ?? ""
        tansactionDetail_Info["expires_date_pst"] = purchaseDetails["expires_date_pst"] ?? ""
        tansactionDetail_Info["is_in_intro_offer_period"] = purchaseDetails["is_in_intro_offer_period"] ?? ""
        tansactionDetail_Info["is_trial_period"] = purchaseDetails["is_trial_period"] ?? ""
        tansactionDetail_Info["original_purchase_date"] = purchaseDetails["original_purchase_date"] ?? ""
        tansactionDetail_Info["original_purchase_date_ms"] = purchaseDetails["original_purchase_date_ms"] ?? ""
        tansactionDetail_Info["original_purchase_date_pst"] = purchaseDetails["original_purchase_date_pst"] ?? ""
        tansactionDetail_Info["original_transaction_id"] = purchaseDetails["original_transaction_id"] ?? ""
        tansactionDetail_Info["product_id"] = purchaseDetails["product_id"] ?? ""
        tansactionDetail_Info["purchase_date"] = purchaseDetails["purchase_date"] ?? ""
        tansactionDetail_Info["purchase_date_ms"] = purchaseDetails["purchase_date_ms"] ?? ""
        tansactionDetail_Info["purchase_date_pst"] = purchaseDetails["purchase_date_pst"] ?? ""
        tansactionDetail_Info["quantity"] = purchaseDetails["quantity"] ?? ""
        tansactionDetail_Info["transaction_id"] = purchaseDetails["transaction_id"] ?? ""
        tansactionDetail_Info["web_order_line_item_id"] = purchaseDetails["web_order_line_item_id"] ?? ""
        
        tansactionDetail_Info["latest_receipt"] = encodedString
        
        let dataTransactionDetail = try! JSONSerialization.data(withJSONObject:tansactionDetail_Info, options: [ ])
        
        let jsonStringTransactionDetail = String(data: dataTransactionDetail, encoding: .utf8)
        
        var tansactionDetailsDict = [String: Any]()
        tansactionDetailsDict[WsParam.productId] = purchaseDetails["product_id"]
        tansactionDetailsDict[WsParam.quantity] = purchaseDetails["quantity"]
        tansactionDetailsDict[WsParam.encodedKey] = encodedString
        
        if let tID = purchaseDetails["transaction_id"]{
            tansactionDetailsDict[WsParam.transactionId] = tID
        }
        let data = try! JSONSerialization.data(withJSONObject:tansactionDetailsDict, options: [ ])
        guard let cUser = DoviesGlobalUtility.currentUser, let jsonString = String(data: data, encoding: .utf8) else{return}
        var dic = [String: Any]()
        dic["package_id"] = modelPackageid
        dic[WsParam.authCustomerId] =  cUser.customerAuthToken
        
        if encodedString != ""
        {
            dic["transaction_detail"] = jsonString
            dic["parsed_receipt_detail"] = jsonStringTransactionDetail
        }
        else
        {
            dic["transaction_detail"] = ""
            dic["parsed_receipt_detail"] = ""
        }
        
        DoviesWSCalls.callCompletePaymentAPI(dicParam: dic, onSuccess: { (success, msg) in
            
            GlobalUtility.hideActivityIndiWindow(viewContView: UIApplication.shared.keyWindow!)

            
            let alert = UIAlertController(title: "Alert", message: msg, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            alert.show()
            
            let controller:UIViewController? = UIApplication.shared.keyWindow?.rootViewController
            
            if let present =  controller?.presentedViewController
            {
                controller?.dismiss(animated: true, completion: nil)
                print("dismisviewcontroller")
            }
            else
            {
                controller?.navigationController?.popViewController(animated: true)
                print("Popviewcontroller")
            }
            
        }) { (error) in
            GlobalUtility.hideActivityIndiWindow(viewContView: UIApplication.shared.keyWindow!)
        }
    }
    //MARK:Upload  current Recipt from server ----xxxx END --xxxx
}



