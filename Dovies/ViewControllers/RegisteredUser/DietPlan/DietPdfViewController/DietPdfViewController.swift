//
//  DietPdfViewController.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

//This view controller used to show diet plan pdf viewer
class DietPdfViewController: BaseViewController , UIWebViewDelegate{

    @IBOutlet weak var webView : UIWebView!
    @IBOutlet weak var loader : UIActivityIndicatorView!
    
    var strTitle : String?
    var strUrl : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadWebView()
      //  addNavigationBackButton()
        addNavigationWhiteBackButton()
    }
    
    func loadWebView(){
        let Str_Title = strTitle?.uppercased()
        title = Str_Title != nil ? Str_Title! : "PDF"
        
        guard let url = strUrl ,let finalUrl = URL(string: url) else{return}
        loader.startAnimating()
        webView.loadRequest(URLRequest(url: finalUrl))
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func webViewDidFinishLoad(_ webView : UIWebView) {
        loader.stopAnimating()
    }
}
