
import Foundation


class BuildPlanModel {

	var customerProgramDetail : [CustomerProgramDetail]!
	var programDietPlans : [ProgramDietPlan]!
	var programWorkouts : ProgramWorkout!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
        customerProgramDetail = [CustomerProgramDetail]()
		if let getCustomerProgramDetailArray = dictionary["get_customer_program_detail"] as? [[String:Any]]{
			for dic in getCustomerProgramDetailArray{
				let value = CustomerProgramDetail(fromDictionary: dic)
                customerProgramDetail.append(value)
			}
		}
        programDietPlans = [ProgramDietPlan]()
		if let getProgramDietPlansArray = dictionary["get_program_diet_plans"] as? [[String:Any]]{
			for dic in getProgramDietPlansArray{
				let value = ProgramDietPlan(fromDictionary: dic)
				programDietPlans.append(value)
			}
		}
		if let getProgramWorkoutsData = dictionary["get_program_workouts"] as? [String:Any]{
			programWorkouts = ProgramWorkout(fromDictionary: getProgramWorkoutsData)
		}
	}
    
}
