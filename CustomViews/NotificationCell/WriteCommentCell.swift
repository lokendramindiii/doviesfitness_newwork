//
//  WriteCommentCell.swift
//  Dovies
//
//  Created by Mindiii on 2/27/19.
//  Copyright © 2019 Hidden Brains. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class WriteCommentCell: UITableViewCell ,GrowingTextViewDelegate{

    
    @IBOutlet var inputToolbar: UIView!
    @IBOutlet var ViewReadallComm: UIView!
    @IBOutlet weak var growingTextView: GrowingTextView!
    @IBOutlet weak var textViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet var btnCommentSend                : UIButton!

    var sendComment:((_ Message:String) -> ())?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        inputToolbar.backgroundColor = .white
        inputToolbar.layer.borderWidth = 0.6
        inputToolbar.layer.cornerRadius = 3
        inputToolbar.layer.borderColor =  UIColor(red: 239/255.0, green: 239/255.0, blue: 239/255.0, alpha: 1.0).cgColor
        
        self.textViewLayoutConfi()
    }

    func textViewLayoutConfi (){
        
        growingTextView.layer.cornerRadius = 4.0
      //  growingTextView.maxLength = 500
        growingTextView.trimWhiteSpaceWhenEndEditing = false
        growingTextView.placeholder = "Write a Comment..."
        growingTextView.placeholderColor = UIColor(white: 0.8, alpha: 1.0)
        growingTextView.minHeight = 25.0
        growingTextView.maxHeight = 100.0
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 1.5
        growingTextView.typingAttributes = [NSAttributedStringKey.paragraphStyle.rawValue : style]
        growingTextView.backgroundColor = UIColor.white
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

      // Configure the view for the selected state
    }
    
    @IBAction func sendCommentTapped(_ sender : UIButton){
        if self.growingTextView.text.isEmptyAfterRemovingWhiteSpacesAndNewlines() == true {
            return
        }
        sendComment?(self.growingTextView.text.removeWhiteSpace())
        self.growingTextView.text = ""
        self.growingTextView.resignFirstResponder()
        btnCommentSend.isSelected = false
    }
    
    func textViewDidChange(_ textView: UITextView) {
        btnCommentSend.isSelected = !textView.text.isEmpty
    }

}
