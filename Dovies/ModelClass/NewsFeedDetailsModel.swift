//
//  NewsFeedDetailsModel.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © 2018 CompanyName. All rights reserved.
//

import UIKit

//This class is used to store newsfeed details data
class NewsFeedDetailsModel: NSObject,NSCoding {
    
    var newsAddedDate : String!
    var newsCommentCount : String!
    var newsContentType : String!
    var newsCreatorName : String!
    var newsCreatorProfileImage : String!
    var newsDescription : String!
    var newsFavouriteStatus : Bool!
    var newsFullImage : String!
    var newsImage : String!
    var newsLikeCount : String!
    var newsLikeStatus : Bool!
    var newsName : String!
    var newsShareUrl : String!
    var newsVideo : String!
    var newsCommentAllow : Bool!

    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        newsAddedDate = dictionary["news_added_date"] as? String ?? ""
        newsCommentCount = dictionary["news_comment_count"] as? String ?? ""
        newsContentType = dictionary["news_content_type"] as? String ?? ""
        newsCreatorName = dictionary["news_creator_name"] as? String ?? ""
        newsCreatorProfileImage = dictionary["news_creator_profile_image"] as? String ?? ""
        newsDescription = dictionary["news_description"] as? String ?? ""
        
        if let favStatus = dictionary["news_favourite_status"] as? String , favStatus == "1"{
            newsFavouriteStatus = true
        }else if  let bFavStatus = dictionary["news_favourite_status"] as? Bool{
            newsFavouriteStatus = bFavStatus
        }else{
            newsFavouriteStatus = false
        }
      
        if let commentStatus = dictionary["news_comment_allow"] as? String , commentStatus == "1"{
            newsCommentAllow = true
        }else if  let commentStatus = dictionary["news_comment_allow"] as? Bool{
            newsCommentAllow = commentStatus
        }else{
            newsCommentAllow = false
        }
        
        
        if let likeStatus = dictionary["news_like_status"] as? String , likeStatus == "1"{
            newsLikeStatus = true
        }else if  let bLikeStatus = dictionary["news_like_status"] as? Bool{
            newsLikeStatus = bLikeStatus
        }else{
            newsLikeStatus = false
        }
        
        newsFullImage = dictionary["news_full_image"] as? String ?? ""
        newsImage = dictionary["news_image"] as? String ?? ""
        newsLikeCount = dictionary["news_like_count"] as? String ?? ""
        newsName = dictionary["news_name"] as? String ?? ""
        newsShareUrl = dictionary["news_share_url"] as? String ?? ""
        newsVideo = dictionary["news_video"] as? String ?? ""
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if newsAddedDate != nil{
            dictionary["news_added_date"] = newsAddedDate
        }
        if newsCommentCount != nil{
            dictionary["news_comment_count"] = newsCommentCount
        }
        if newsContentType != nil{
            dictionary["news_content_type"] = newsContentType
        }
        if newsCreatorName != nil{
            dictionary["news_creator_name"] = newsCreatorName
        }
        if newsCreatorProfileImage != nil{
            dictionary["news_creator_profile_image"] = newsCreatorProfileImage
        }
        if newsDescription != nil{
            dictionary["news_description"] = newsDescription
        }
        if newsFavouriteStatus != nil{
            dictionary["news_favourite_status"] = newsFavouriteStatus
        }
        if newsFullImage != nil{
            dictionary["news_full_image"] = newsFullImage
        }
        if newsImage != nil{
            dictionary["news_image"] = newsImage
        }
        if newsLikeCount != nil{
            dictionary["news_like_count"] = newsLikeCount
        }
        if newsLikeStatus != nil{
            dictionary["news_like_status"] = newsLikeStatus
        }
        if newsName != nil{
            dictionary["news_name"] = newsName
        }
        if newsShareUrl != nil{
            dictionary["news_share_url"] = newsShareUrl
        }
        if newsVideo != nil{
            dictionary["news_video"] = newsVideo
        }
        if newsCommentAllow != nil{
            dictionary["news_comment_allow"] = newsCommentAllow
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        newsAddedDate = aDecoder.decodeObject(forKey: "news_added_date") as? String
        newsCommentCount = aDecoder.decodeObject(forKey: "news_comment_count") as? String
        newsContentType = aDecoder.decodeObject(forKey: "news_content_type") as? String
        newsCreatorName = aDecoder.decodeObject(forKey: "news_creator_name") as? String
        newsCreatorProfileImage = aDecoder.decodeObject(forKey: "news_creator_profile_image") as? String
        newsDescription = aDecoder.decodeObject(forKey: "news_description") as? String
        newsFavouriteStatus = aDecoder.decodeObject(forKey: "news_favourite_status") as? Bool
        newsFullImage = aDecoder.decodeObject(forKey: "news_full_image") as? String
        newsImage = aDecoder.decodeObject(forKey: "news_image") as? String
        newsLikeCount = aDecoder.decodeObject(forKey: "news_like_count") as? String
        newsLikeStatus = aDecoder.decodeObject(forKey: "news_like_status") as? Bool
        newsName = aDecoder.decodeObject(forKey: "news_name") as? String
        newsShareUrl = aDecoder.decodeObject(forKey: "news_share_url") as? String
        newsVideo = aDecoder.decodeObject(forKey: "news_video") as? String
        
        newsCommentAllow = aDecoder.decodeObject(forKey: "news_comment_allow") as? Bool

        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if newsAddedDate != nil{
            aCoder.encode(newsAddedDate, forKey: "news_added_date")
        }
        if newsCommentCount != nil{
            aCoder.encode(newsCommentCount, forKey: "news_comment_count")
        }
        if newsContentType != nil{
            aCoder.encode(newsContentType, forKey: "news_content_type")
        }
        if newsCreatorName != nil{
            aCoder.encode(newsCreatorName, forKey: "news_creator_name")
        }
        if newsCreatorProfileImage != nil{
            aCoder.encode(newsCreatorProfileImage, forKey: "news_creator_profile_image")
        }
        if newsDescription != nil{
            aCoder.encode(newsDescription, forKey: "news_description")
        }
        if newsFavouriteStatus != nil{
            aCoder.encode(newsFavouriteStatus, forKey: "news_favourite_status")
        }
        if newsFullImage != nil{
            aCoder.encode(newsFullImage, forKey: "news_full_image")
        }
        if newsImage != nil{
            aCoder.encode(newsImage, forKey: "news_image")
        }
        if newsLikeCount != nil{
            aCoder.encode(newsLikeCount, forKey: "news_like_count")
        }
        if newsLikeStatus != nil{
            aCoder.encode(newsLikeStatus, forKey: "news_like_status")
        }
        if newsName != nil{
            aCoder.encode(newsName, forKey: "news_name")
        }
        if newsShareUrl != nil{
            aCoder.encode(newsShareUrl, forKey: "news_share_url")
        }
        if newsVideo != nil{
            aCoder.encode(newsVideo, forKey: "news_video")
        }
        if newsCommentAllow != nil{
            aCoder.encode(newsVideo, forKey: "news_comment_allow")
        }
    }
    
}
