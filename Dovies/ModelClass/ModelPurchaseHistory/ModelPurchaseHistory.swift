//
//  ModelPurchaseHistory.swift
//  Dovies
//
//  Created by Neel Shah on 24/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class ModelPurchaseHistory: NSObject, NSCoding{
	
	var displayContent : String!
	var displayImage : String!
	var displayTitle : String!
	var purchaseAmount : String!
	var purchaseDate : String!
	var purchaseType : String!
	
	
	/**
	* Instantiate the instance using the passed dictionary values to set the properties values
	*/
	init(fromDictionary dictionary: [String:Any]){
		displayContent = dictionary["display_content"] as? String ?? ""
		displayImage = dictionary["display_image"] as? String ?? ""
		displayTitle = dictionary["display_title"] as? String ?? ""
		purchaseAmount = dictionary["purchase_amount"] as? String ?? ""
		purchaseDate = dictionary["purchase_date"] as? String ?? ""
		purchaseType = dictionary["purchase_type"] as? String ?? ""
	}
	
	/**
	* Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	*/
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if displayContent != nil{
			dictionary["display_content"] = displayContent
		}
		if displayImage != nil{
			dictionary["display_image"] = displayImage
		}
		if displayTitle != nil{
			dictionary["display_title"] = displayTitle
		}
		if purchaseAmount != nil{
			dictionary["purchase_amount"] = purchaseAmount
		}
		if purchaseDate != nil{
			dictionary["purchase_date"] = purchaseDate
		}
		if purchaseType != nil{
			dictionary["purchase_type"] = purchaseType
		}
		return dictionary
	}
	
	/**
	* NSCoding required initializer.
	* Fills the data from the passed decoder
	*/
	@objc required init(coder aDecoder: NSCoder)
	{
		displayContent = aDecoder.decodeObject(forKey: "display_content") as? String
		displayImage = aDecoder.decodeObject(forKey: "display_image") as? String
		displayTitle = aDecoder.decodeObject(forKey: "display_title") as? String
		purchaseAmount = aDecoder.decodeObject(forKey: "purchase_amount") as? String
		purchaseDate = aDecoder.decodeObject(forKey: "purchase_date") as? String
		purchaseType = aDecoder.decodeObject(forKey: "purchase_type") as? String
		
	}
	
	/**
	* NSCoding required method.
	* Encodes mode properties into the decoder
	*/
	@objc func encode(with aCoder: NSCoder)
	{
		if displayContent != nil{
			aCoder.encode(displayContent, forKey: "display_content")
		}
		if displayImage != nil{
			aCoder.encode(displayImage, forKey: "display_image")
		}
		if displayTitle != nil{
			aCoder.encode(displayTitle, forKey: "display_title")
		}
		if purchaseAmount != nil{
			aCoder.encode(purchaseAmount, forKey: "purchase_amount")
		}
		if purchaseDate != nil{
			aCoder.encode(purchaseDate, forKey: "purchase_date")
		}
		if purchaseType != nil{
			aCoder.encode(purchaseType, forKey: "purchase_type")
		}
	}
}
