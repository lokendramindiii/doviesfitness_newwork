//
//  SettingCell.swift
//  Dovies
//
//  Created by hb on 16/04/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import UserNotifications

class SettingCell: UITableViewCell {

    @IBOutlet weak var Img_leadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var Img_Rightarrow: UIImageView!

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var setSwitch: UISwitch!
    @IBOutlet weak var txtReminderTime: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension SettingCell: UITextFieldDelegate
{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtReminderTime {
            var dateSel = Date()
            if txtReminderTime.text?.isEmpty == false{
                
            //   dateSel = NSDate.stringToTime(strTime: txtReminderTime.text, format: "hh:mm a")
                dateSel = NSDate.stringToTime(strTime: txtReminderTime.text, format: "HH:mm")
            }
            
            ActionSheetDatePicker.show(withTitle: "Select Reminder Time", datePickerMode: .time, selectedDate: dateSel, minimumDate: nil, maximumDate: nil, doneBlock: { (picker, value, index) in
//                if let dateSelected = value as? NSDate{
//                    let dateTimeFormattor = DateFormatter()
//                    dateTimeFormattor.locale = Locale(identifier: "en_US_POSIX")
//                    dateTimeFormattor.dateFormat = "yyyy-MM-dd h:mm a"
//                    dateTimeFormattor.amSymbol = "AM"
//                    dateTimeFormattor.pmSymbol = "PM"
//                    let dateTimeString = dateTimeFormattor.string(from: Date())
//                    let todayDate = NSDate.stringToDate(strDate: dateTimeString, format: "yyyy-MM-dd h:mm a")
//                    print(dateTimeString)
//                    let formatter = DateFormatter()
//                    formatter.locale = Locale(identifier: "en_US_POSIX")
//                    formatter.dateFormat = "yyyy-MM-dd"
//                    let dateString = formatter.string(from: Date())
//                    let strSelectedTime = NSDate.dateToString(date: dateSelected as Date, format: "hh:mm a")
//                    UserDefaults.standard.set(strSelectedTime, forKey: "isReminderTime")
//                    let selectedTimeDate = dateString + " " + strSelectedTime
//                    print(selectedTimeDate)
//                    self.txtReminderTime.text = NSDate.dateToString(date: dateSelected as Date, format: "hh:mm a")
//                    let selectedDate = NSDate.stringToDate(strDate: selectedTimeDate, format: "yyyy-MM-dd h:mm a")
//                    if todayDate.compare(selectedDate) == .orderedAscending {
//                        self.setExerciseReminder(date: selectedDate)
//                    }
//                    if todayDate.compare(selectedDate) == .orderedDescending{
//                        var dayComponent = DateComponents()
//                        dayComponent.day = 1
//                        let theCalendar = Calendar.current
//                        let nextDate = theCalendar.date(byAdding: dayComponent, to: Date())
//                        let strNextDay = NSDate.dateToString(date: nextDate, format: "yyyy-MM-dd")
//                        let NextDayTime = strNextDay + " " + strSelectedTime
//                        let strNextDayTime = NSDate.stringToDate(strDate: NextDayTime, format: "yyyy-MM-dd h:mm a")
//                        self.setExerciseReminder(date: strNextDayTime)
//                    }
//
//                }
                
                
                
                if let dateSelected = value as? NSDate{
                    let dateTimeFormattor = DateFormatter()
                    dateTimeFormattor.locale = Locale(identifier: "en_US_POSIX")
                    dateTimeFormattor.dateFormat = "yyyy-MM-dd HH:mm"
//                    dateTimeFormattor.amSymbol = "AM"
//                    dateTimeFormattor.pmSymbol = "PM"
                    let dateTimeString = dateTimeFormattor.string(from: Date())
                    let todayDate = NSDate.stringToDate(strDate: dateTimeString, format: "yyyy-MM-dd HH:mm")
                    print(dateTimeString)
                    let formatter = DateFormatter()
                    formatter.locale = Locale(identifier: "en_US_POSIX")
                    formatter.dateFormat = "yyyy-MM-dd"
                    let dateString = formatter.string(from: Date())
                    let strSelectedTime = NSDate.dateToString(date: dateSelected as Date, format: "HH:mm")
                    UserDefaults.standard.set(strSelectedTime, forKey: "isReminderTime")
                    let selectedTimeDate = dateString + " " + strSelectedTime
                    print(selectedTimeDate)
                    self.txtReminderTime.text = NSDate.dateToString(date: dateSelected as Date, format: "HH:mm")
                    let selectedDate = NSDate.stringToDate(strDate: selectedTimeDate, format: "yyyy-MM-dd HH:mm")
                    if todayDate.compare(selectedDate) == .orderedAscending {
                        self.setExerciseReminder(date: selectedDate)
                    }
                    if todayDate.compare(selectedDate) == .orderedDescending{
                        var dayComponent = DateComponents()
                        dayComponent.day = 1
                        let theCalendar = Calendar.current
                        let nextDate = theCalendar.date(byAdding: dayComponent, to: Date())
                        let strNextDay = NSDate.dateToString(date: nextDate, format: "yyyy-MM-dd")
                        let NextDayTime = strNextDay + " " + strSelectedTime
                        let strNextDayTime = NSDate.stringToDate(strDate: NextDayTime, format: "yyyy-MM-dd HH:mm")
                        self.setExerciseReminder(date: strNextDayTime)
                    }
                
                }
            }, cancel: { (picker) in
                
                
            }, origin: textField)
            return false
        }
        
        return true
    }
    
    func setExerciseReminder(date: Date){
        LocalNotification.dispatchlocalNotification(with: "", body: "Get going! It is Workout time!", at: date)
    }
    
}
