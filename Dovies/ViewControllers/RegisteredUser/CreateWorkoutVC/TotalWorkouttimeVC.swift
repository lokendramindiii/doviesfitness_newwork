//
//  TotalWorkouttimeVC.swift
//  Dovies
//
//  Created by Mindiii on 4/17/19.
//  Copyright © 2019 Hidden Brains. All rights reserved.
//

import UIKit

class TotalWorkouttimeVC: UIViewController {

    
    @IBOutlet var IBlblCreator: UILabel!
    @IBOutlet var IBlblTime: UILabel!
    @IBOutlet var IBlblMinutes: UILabel!
    @IBOutlet var IBimgWatch: UIImageView!
    @IBOutlet var IBlblContent: UILabel!
    @IBOutlet var IBView: UIView!

    var TotalWorkoutMinutes:String = ""
    var TotlaWorkoutSecond:String = ""

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        
        IBView.layer.masksToBounds = true
        IBView.layer.cornerRadius = 20.0
        
        
        if TotalWorkoutMinutes == ""
        {
           IBlblContent.isHidden = false
            IBimgWatch.isHidden = false
            IBlblMinutes.isHidden = true
            IBlblTime.isHidden = true
        }
        else
        {
            IBlblContent.isHidden = true
            IBimgWatch.isHidden = true
            IBlblMinutes.isHidden = false
            IBlblTime.isHidden = false
            
            if TotlaWorkoutSecond !=  ""
            {
                if TotalWorkoutMinutes == "1" || TotalWorkoutMinutes == "0"
                {
                    IBlblMinutes.text = "Second"
                }
                else{
                    IBlblMinutes.text = "Seconds"
                }
            }
            else
            {
                if TotalWorkoutMinutes == "1"
                {
                    IBlblMinutes.text = "Minute"
                }
            }
            IBlblTime.text = TotalWorkoutMinutes

        }
        
        
        self.addCancelBackButton()

        self.view.addTapGesture(target: self, action: #selector(self.dismissView))

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Instance methods
    
    func addCancelBackButton() {
        self.navigationItem.leftBarButtonItems = GlobalUtility.getNavigationButtonItems(target: self, selector: #selector(self.btnCancelTapped(sender:)), imageName: "icon_close")
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.black
    }
    
    //MARK: IBAction methods
    @objc func dismissView(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func btnCancelTapped(sender: UIButton) {
//        if Str_CreateWorkoutFlage == "CreateWorkout"
//        {
//        }
//        else
//        {
//            isForWorkOut?()
//        }
        _ = self.navigationController?.popViewController(animated: false)
        
    }
    
    @IBAction func closeTapped(_ sender : UIButton){
//        if Str_CreateWorkoutFlage == "CreateWorkout"
//        {
//        }
//        else
//        {
//            isForWorkOut?()
//        }
        self.dismiss(animated: true, completion: nil)
    }
    
}
