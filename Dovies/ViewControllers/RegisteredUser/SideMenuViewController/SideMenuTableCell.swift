//
//  SideMenuTableCell.swift
//  Dovies
//
//  Created by hb on 17/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class SideMenuTableCell: UITableViewCell {

    @IBOutlet var imgMenuIcon: UIImageView!
    @IBOutlet var lblMenuItem: UILabel!
    @IBOutlet var separatorFull: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   

}
