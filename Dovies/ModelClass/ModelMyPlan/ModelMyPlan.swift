//
//  ModelMyPlan.swift
//  Dovies
//
//  Created by Neel Shah on 23/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class ModelMyPlan: NSObject, NSCoding{
	
	var programId : String!
	var programImage : String!
	var programName : String!
    var programShareUrl : String!
    var programFavStatus : Bool!
    var programAccessLevel : String!
    var programActive :  Bool!
    var programWeekCount : String!

	/**
	* Instantiate the instance using the passed dictionary values to set the properties values
	*/
    
	init(fromDictionary dictionary: [String:Any]){
		programId = dictionary["program_id"] as? String ?? ""
		programImage = dictionary["program_image"] as? String ?? ""
		programName = dictionary["program_name"] as? String ?? ""
        programShareUrl = dictionary["program_share_url"] as? String ?? ""
        
        programWeekCount = dictionary["program_week_count"] as? String ?? ""
        
        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName {
            programAccessLevel = "OPEN"
        }
        else
        {
            programAccessLevel = dictionary["program_access_level"] as? String ?? ""
        }

        if let favStatus = dictionary["program_fav_status"] as? String , favStatus == "1"{
            programFavStatus = true
        }else if  let dietFavStat = dictionary["program_fav_status"] as? Bool{
            programFavStatus = dietFavStat
        }else{
            programFavStatus = false
        }
        
        if let ActiveStatus = dictionary["status"] as? String , ActiveStatus == "1"{
            programActive = true
        }else if  let dietActStat = dictionary["status"] as? Bool{
            programActive = dietActStat
        }else{
            programActive = false
        }
	}
	
	/**
	* Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	*/
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if programId != nil{
			dictionary["program_id"] = programId
		}
		if programImage != nil{
			dictionary["program_image"] = programImage
		}
		if programName != nil{
			dictionary["program_name"] = programName
		}
        if programShareUrl != nil{
            dictionary["program_share_url"] = programName
        }
        if programFavStatus != nil{
            dictionary["program_fav_status"] = programFavStatus
        }
        if programActive != nil{
            dictionary["status"] = programActive
        }
        if programWeekCount != nil{
            dictionary["program_week_count"] = programWeekCount
        }
        if programAccessLevel != nil{
            dictionary["program_access_level"] = programAccessLevel
        }
		return dictionary
	}
	
	/**
	* NSCoding required initializer.
	* Fills the data from the passed decoder
	*/
	@objc required init(coder aDecoder: NSCoder)
	{
		programId = aDecoder.decodeObject(forKey: "program_id") as? String
		programImage = aDecoder.decodeObject(forKey: "program_image") as? String
		programName = aDecoder.decodeObject(forKey: "program_name") as? String
        programShareUrl = aDecoder.decodeObject(forKey: "program_share_url") as? String
        
        programWeekCount = aDecoder.decodeObject(forKey: "program_week_count") as? String
	}
	
	/**
	* NSCoding required method.
	* Encodes mode properties into the decoder
	*/
	@objc func encode(with aCoder: NSCoder)
	{
		if programId != nil{
			aCoder.encode(programId, forKey: "program_id")
		}
		if programImage != nil{
			aCoder.encode(programImage, forKey: "program_image")
		}
		if programName != nil{
			aCoder.encode(programName, forKey: "program_name")
		}
        if programShareUrl != nil{
            aCoder.encode(programName, forKey: "program_share_url")
        }
        if programWeekCount != nil{
            aCoder.encode(programWeekCount, forKey: "program_week_count")
        }

	}
	
}
