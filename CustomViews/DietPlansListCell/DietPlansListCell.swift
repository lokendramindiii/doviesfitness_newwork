//
//  DietPlansListCell.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

class DietPlansListCell: UICollectionViewCell {

    @IBOutlet weak var imgLock: UIImageView!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var imgBackground : UIImageView!
    // loks  add imgFadeBackground
    @IBOutlet weak var imgFadeBackground : UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        let gradientView = UIView(frame: CGRect(x: 0, y: (NewsFeedViewController.cellWidth)/2, width: NewsFeedViewController.cellWidth, height: (NewsFeedViewController.cellWidth)/2))
        let gradientLayer:CAGradientLayer = CAGradientLayer()
        gradientLayer.frame.size =  gradientView.frame.size
        gradientLayer.colors = [UIColor.clear.cgColor,UIColor.appBlackColorNew().withAlphaComponent(0.7).cgColor]
        gradientView.layer.addSublayer(gradientLayer)
        self.imgBackground.addSubview(gradientView)
    }
    
    func setCellUI(with dietPlan: DietPlan){
        lblTitle.text = dietPlan.dietPlanTitle.uppercased()
       // imgBackground.sd_setImage(with:  URL(string: dietPlan.dietPlanFullImage), placeholderImage: AppInfo.placeholderImage)
        
        imgBackground.sd_setImage(with:  URL(string: dietPlan.dietPlanFullImage), placeholderImage: nil)
        
        if dietPlan.dietPlanAccessLevel == "LOCK" {
            if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
            {
                imgLock.isHidden = true
            }
            else
            {
                imgLock.isHidden = false
            }
        }
        else{
             imgLock.isHidden = true
        }
    }
    
    func setFavCellUI(with dietPlan: DietPlan){
        lblTitle.text = dietPlan.dietPlanTitle.uppercased()
      //  imgBackground.sd_setImage(with:  URL(string: dietPlan.dietPlanImage), placeholderImage: AppInfo.placeholderImage)
        
        imgBackground.sd_setImage(with:  URL(string: dietPlan.dietPlanImage), placeholderImage: nil)

        if dietPlan.dietPlanAccessLevel == "LOCK" {
            
            if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
            {
                imgLock.isHidden = true
            }
            else
            {
                imgLock.isHidden = false
            }

        }
        else{
            imgLock.isHidden = true
        }
    }

}
