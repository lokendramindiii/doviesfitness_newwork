//
//  FeedbackFormVC.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit
import  YPImagePicker
//This view controller is used to show feedback from on workout completion.
class FeedbackFormVC: UIViewController {
    
    @IBOutlet weak var IBlblTitle: UILabel!
    @IBOutlet weak var IBlblDuration: UILabel!
    @IBOutlet var IBArrButtons: [UIButton]!
    @IBOutlet weak var IBbtnGreat : UIButton!
    @IBOutlet weak var IBbtnGood : UIButton!
    @IBOutlet weak var IBbtnReasonable : UIButton!
    @IBOutlet weak var IBbtnBad : UIButton!
    @IBOutlet weak var IBbtnWeight : UIButton!
    @IBOutlet weak var IBtxtNote : UITextView!
    @IBOutlet weak var IBtxtWeight: DTTextField!
    @IBOutlet weak var IBimgWorkout : UIImageView!
    @IBOutlet weak var IBlblPlaceholder : UILabel!
    
    @IBOutlet weak var viewWeight : UIView!
    @IBOutlet weak var collectionImages : UICollectionView!
    @IBOutlet weak var bnSubmit : UIButton!

    var imagePicker = UIImagePickerController()
    
    let noOfCellInRow = 5
    let cellPading:CGFloat = 10.0
    let collectionLeftPadding:CGFloat = 25.0
    
    var cellSizeValue : CGFloat = 0
    var weight : String = ""
    let validator = Validator()
    var workoutTime = ""
    var workoutId = ""
	var workoutName = ""
	var workOutImage = ""
	
	var disMissView:((Bool) -> ())?
    var arrImages = [ModelWorkoutLogImage]()
    
    //MARK: - View Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let totalPadding :CGFloat = cellPading*(CGFloat(noOfCellInRow-1)) + collectionLeftPadding*2
        self.cellSizeValue =  (ScreenSize.width - totalPadding)/CGFloat(noOfCellInRow)
        

        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white,
                                                                        NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]

        inititalUISetUp()
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        // Do any additional setup after loading the view
        
    }
    

    //MARK: - View Controller styling
    func inititalUISetUp() {
        
        //****** new changes ****
        let borderColor = UIColor(r: 229, g: 229, b: 229, alpha: 1)
        self.viewWeight.makeViewCorner(radius: 10, borderColor: borderColor, borderWidth: 0.5)
        self.IBtxtNote.makeTextViewCorner(radius: 10, borderColor: borderColor, borderWidth: 0.5)
        self.IBtxtNote.contentInset = UIEdgeInsets(top: 0, left: 7, bottom: 0, right: 10)
        self.bnSubmit.makeCorner(radius: self.bnSubmit.frame.size.height/2, borderColor: UIColor.clear, borderWidth: 0)
        self.IBtxtWeight.addDropdownWithImage(width: 30,height:15, image: UIImage(named:"ico_dropdown")!)
        //****** new changes ****

        
        IBlblTitle.text = workoutName
        let arrTime = workoutTime.components(separatedBy: ":")
        
        if arrTime.count > 0, arrTime[0] == "00"{
            IBlblDuration.text = "Duration: \(arrTime[1]) secs"
        }else{
            IBlblDuration.text = "Duration: \(workoutTime) mins"
        }
        
        IBbtnGreat.isSelected = true
        IBtxtNote.text = ""
        title = "Workout Log".uppercased()
		self.navigationItem.hidesBackButton = true
		if workOutImage.count > 0{
        //	IBimgWorkout.sd_setImage(with: URL(string: workOutImage), placeholderImage: AppInfo.placeholderImage)
            
            IBimgWorkout.sd_setImage(with: URL(string: workOutImage), placeholderImage: nil)

		}
        IBtxtNote.addSeparator(with: UIColor.lightGray, size: 1.0)
		
		self.navigationItem.rightBarButtonItem = GlobalUtility.getNavigationButtonItemWith(target: self, selector: #selector(self.cancelTap), title: "Cancel")

    }
    
    
    //MARK: - custom methods
    
    /// this method used to set validations for the add feedback form
    func setFormValidations(){
        validator.styleTransformers(success:{ (validationRule) -> Void in
            if let textField = validationRule.field as? DTTextField {
                textField.hideError()
            }
        }, error:{ (validationError) -> Void in
            if let textField = validationError.field as? DTTextField {
                textField.showError(message: validationError.errorMessage)
            }
        })
        validator.registerField(IBtxtWeight, errorLabel: nil , rules: [RequiredRule(message: AlertMessages.emptyWeight)])
    }
    
    //MARK: Actions
    @IBAction func btnSubmitTapped(_ sender : UIButton){
        //Submit action
		self.validationSuccessful()
        
		
    }
    
    @IBAction func btnToggleTapped(_ sender:  UIButton){
        for btn in IBArrButtons{
            btn.isSelected = sender == btn
        }
    }
	
	@objc func cancelTap(){
		UIAlertController.showAlert(in: (APP_DELEGATE.window?.rootViewController)!, withTitle: "", message: AlertMessages.cancelLoggingWorkoutConfirmation, cancelButtonTitle: "No", destructiveButtonTitle: "Yes", otherButtonTitles: nil, tap: { (alertCont, action, index) in
			if index == 1{
                self.disMissView?(false)
                self.dismiss(animated: false, completion: nil)
			}
		})
	}
    
    func getSelectedFeedbackStatus()->String{
        for btn in IBArrButtons{
            if btn.isSelected{
                switch btn{
                case IBbtnGood:
                    return "Good"
                case IBbtnBad:
                    return "Bad"
                case IBbtnGreat:
                    return "Great"
                case IBbtnReasonable:
                    return "Reasonable"
                default:
                    return ""
                }
            }
        }
        return ""
    }
    
    @IBAction func btnWeightTapped(_ sender : UIButton){
        self.view.endEditing(true)
        self.view.endEditing(true)
        var weight = [String]()
        if let units = DoviesGlobalUtility.currentUser?.customerUnits{
            if units.uppercased() == "imperial".uppercased(){
                for i in 30...500{
                    weight.append("\(i)" + " lbs")
                }
            }else{
                for i in 13...227{
                    weight.append("\(i)" + " kgs")
                }
            }
        }
        let mcPicker = McPicker(data: [weight], selectedTitle:[self.weight],pickerTitle : "Weight")
        mcPicker.show { [weak self](selections: [Int : String]) -> Void in
            guard let weakSelf = self else{return}
            if let weight = selections[0]{
                weakSelf.IBtxtWeight.text = weight
                weakSelf.weight = weight
            }
        }
    }
    
    
    func getWeight() -> String{
        guard let user = DoviesGlobalUtility.currentUser else{return ""}
        if let text = IBtxtWeight.text{
            if user.customerUnits.uppercased() == "imperial".uppercased(){
                let kgs = Conversions.getKgsFromLBS(lbs: text.replacingOccurrences(of: " lbs", with: ""))
                return "\(kgs)"
            }else{
                return text.replacingOccurrences(of: " kgs", with: "")
            }
        }
        return ""
    }
    //AMRK: - didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension FeedbackFormVC: ValidationDelegate{
    func validationSuccessful() {
        //API call
        callAddWorkoutFeedbackAPI()
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        
    }
}

extension FeedbackFormVC : UITextViewDelegate{
    func textViewDidChange(_ textView: UITextView) {
        IBlblPlaceholder.isHidden = !textView.text.isEmpty
    }
}

//API Calls
extension FeedbackFormVC{
    
    /**
     This method is used to submit the workout feedback of the user like workout time, how much user lost the weight, etc.,.
     */
//    func callAddWorkoutFeedbackAPI(){
//        self.view.isUserInteractionEnabled = false
//        guard let user = DoviesGlobalUtility.currentUser else{return}
//        var params = [String : Any]()
//        params[WsParam.authCustomerId] = user.customerAuthToken
//        params[WsParam.workoutId] = workoutId
//        params[WsParam.feedbackStatus] = getSelectedFeedbackStatus()
//        params[WsParam.weight] = getWeight()
//        params[WsParam.workout_time] = "00:"+workoutTime
//        params[WsParam.note] = IBtxtNote.text ?? ""
//        DoviesWSCalls.callAddWorkoutFeedbackAPI(dicParam: params, onSuccess: {(success, response, message) in
//            self.view.isUserInteractionEnabled = true
//            GlobalUtility.showToastMessage(msg: "Done")
//                let workoutLogVc = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "MyWorkOutLogViewController") as! MyWorkOutLogViewController
//                self.navigationController?.pushViewController(workoutLogVc, animated: true)
//                self.disMissView?(true)
////            })
//
//        }) { (error) in
//
//        }
//    }
    
    
    func callAddWorkoutFeedbackAPI(){
        self.view.isUserInteractionEnabled = false
        guard let user = DoviesGlobalUtility.currentUser else{return}
        var params = [String : Any]()
        params[WsParam.authCustomerId] = user.customerAuthToken
        params[WsParam.workoutId] = workoutId
        params[WsParam.feedbackStatus] = getSelectedFeedbackStatus()
        params[WsParam.weight] = getWeight()
        
        
        let arrTime1 = workoutTime.components(separatedBy: ":")
        if arrTime1.count > 2
        {
            params[WsParam.workout_time] = workoutTime
        }
        else
        {
            params[WsParam.workout_time] = "00:"+workoutTime
        }
        
        
        params[WsParam.note] = IBtxtNote.text ?? ""
        
        let imageArr = self.arrImages.map{$0.image}
        let timeArr = self.arrImages.map{$0.time}
        
        let strTimeArr = timeArr.map{$0!.strrigWithFormat(format: "yyyy-MM-dd HH:mm:ss")}
        let strArr = strTimeArr.joined(separator: ",")
        
        print("strArr = \(strArr)")

        if imageArr.count>0{
            params[WsParam.workout_images] = imageArr
            params[WsParam.workout_image_date] = strArr
        }

        GlobalUtility.showActivityIndi(viewContView: self.view)
        DoviesWSCalls.callAddWorkoutFeedbackPostAPI(dicParam: params, onSuccess: {(success, response, message) in
            GlobalUtility.hideActivityIndi(viewContView: self.view)

            if success{
                self.view.isUserInteractionEnabled = true
                //GlobalUtility.showToastMessage(msg: "Done")
                let workoutLogVc = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "MyWorkOutLogViewController") as! MyWorkOutLogViewController
                self.navigationController?.pushViewController(workoutLogVc, animated: true)
                self.disMissView?(true)
            }
            else{
                DoviesGlobalUtility.showSimpleAlert(viewController: self, message: message)
            }
            
        }) { (error) in
            GlobalUtility.hideActivityIndi(viewContView: self.view)

        }
    }
}

extension FeedbackFormVC: UIGestureRecognizerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}

extension FeedbackFormVC: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrImages.count + 1
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedbackCollectionViewCell", for: indexPath) as! FeedbackCollectionViewCell
        
        if indexPath.item == 0{
            cell.btnRemoveImage.isHidden = true
            cell.btnTakeImage.isHidden = false
            cell.imgWorkout.image = nil
        }
        else{
            cell.setCellUI(arrImages[indexPath.item-1], iPath: indexPath)
            cell.btnRemoveImage.isHidden = false
            cell.btnTakeImage.isHidden = true
        }
        
        cell.btnRemoveHnadler = { (btn, iPath) in
            print("action remove image @ \(iPath.item)")
            self.arrImages.remove(at: iPath.item-1)
            self.collectionImages.reloadData()
            
        }
        
        cell.btnTakeImageHnadler = {[weak self](btnTakeImage) in
            guard let weakself = self else {
                return
            }
            print("action take image")
            
            weakself.view.endEditing(true)

            guard weakself.arrImages.count < 4 else {return}
            weakself.actionTakeImage(btnTakeImage)
        }
  
        return cell
    }


}

extension FeedbackFormVC : UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        }

}

extension FeedbackFormVC: UICollectionViewDelegateFlowLayout{
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: cellSizeValue, height: cellSizeValue)
//    }



}

extension FeedbackFormVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
     func actionTakeImage(_ sender: UIButton) {
        
        
        /*
        let alert:UIAlertController=UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.default){
            UIAlertAction in
            self.openPicker(type: "camera")
        }

        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel){
            UIAlertAction in
        }
        alert.addAction(cameraAction)
        alert.addAction(cancelAction)
        if let popoverController = alert.popoverPresentationController {
            popoverController.sourceView = self.view
            popoverController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverController.permittedArrowDirections = []
        }
        self.present(alert, animated: true, completion: nil)
        */
        
        // loks code comment  self.openPicker(type: "camera")
       // self.openPicker(type: "camera")
        self.showPickerInstagram()
    }
    
    func showPickerInstagram() {
        
        
        var config = YPImagePickerConfiguration()
        
      //  config.library.mediaType = .photoAndVideo
        
        config.library.mediaType = .photo
        
        
        /* Adds a Filter step in the photo taking process. Defaults to true */
        config.showsFilters = false
        
        
        config.shouldSaveNewPicturesToAlbum = false
        
        
        //loks comment config.startOnScreen = .library  19-03-19
        // config.startOnScreen = .library
        
        /* Defines which screens are shown at launch, and their order.
         Default value is `[.library, .photo]` */
        config.screens = [.photo]
        
       
        config.hidesStatusBar = false
        
       
        let picker = YPImagePicker(configuration: config)
        
        
        picker.didFinishPicking { [weak self] items, cancelled in
                
                print("finish")
                
                if cancelled {
                    print("Picker was canceled")
                    picker.dismiss(animated: true, completion: nil)
                    return
                }
                _ = items.map { print("🧀 \($0)") }
                
                guard let weakself = self else {
                    picker.dismiss(animated: true, completion: nil)
                    return
                }
                // self.selectedItems = items
                if let firstItem = items.first {
                    
                    
                    switch firstItem {
                    case .photo(let photo):
                        let cImage = HBImagePicker.sharedInstance().compressImage(photo.image)
                        
                        let imgaeObj = ModelWorkoutLogImage(image: cImage!,time: Date() )
                        weakself.arrImages.append(imgaeObj)
                        
                        print("images count = \(weakself.arrImages.count)")
                        weakself.collectionImages.reloadData()
                        picker.dismiss(animated: true, completion: nil)
                        
                    case .video(let video):
                        
                        
                        picker.dismiss(animated: true, completion: { [weak self] in
                            
                        })
                    }
                }
            }
        
        present(picker, animated: true, completion: nil)

    }
    
    
    
    func openPicker(type: String){
        if type == "camera"{
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera)){
                self.imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            }
        }
        else{
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            
        }
        //self.imagePicker.allowsEditing = true
        self.imagePicker.delegate = self
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            let cImage = HBImagePicker.sharedInstance().compressImage(image)
            
            let imgaeObj = ModelWorkoutLogImage(image: cImage!,time: Date() )
            self.arrImages.append(imgaeObj)
            
            print("images count = \(self.arrImages.count)")
            self.collectionImages.reloadData()
            dismiss(animated: true, completion: nil)
        }
        
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        dismiss(animated: true, completion: nil)
    }
    
    
}


