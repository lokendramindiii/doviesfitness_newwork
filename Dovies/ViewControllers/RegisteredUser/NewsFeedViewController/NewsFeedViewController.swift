//
//  NewsFeedViewController.swift
//  Dovies
//
//  Created by Neel Shah on 16/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//


import UIKit

class NewsFeedViewController: BaseViewController  {
    
    @IBOutlet weak var emptyView : UIView!
    @IBOutlet weak var IBtblNewFeed: UITableView!
    //  static let cellPadding : CGFloat = 6.0
    // cellPadding = 60 21-08-2019
    static let cellPadding : CGFloat = 60.0
    static let cellWidth : CGFloat  = (ScreenSize.width - NewsFeedViewController.cellPadding)/2
    
    //    let collectionCellHeight: CGFloat = cellWidth + 33
    //    static let cellWidth : CGFloat  = (ScreenSize.width)/2
    
    //    static let cellWidth : CGFloat  =  152
    
    let collectionCellHeight: CGFloat = cellWidth
    
    
    static let tableElementsPadding : CGFloat = 128
    let tableLandScapeCellHeight = 180*CGRect.widthRatio + tableElementsPadding
    
    var featuredNewsArr:[NewsFeedModel] = [NewsFeedModel]()
    var otherNewsArr:[NewsFeedModel] = [NewsFeedModel]()
    var pullToRefreshCtrl:UIRefreshControl!
    var placeholderCellsCount = 0
    var currentPageIndex = 1
    var selectedIndexPath : IndexPath!
    var selectedNewsFeed : NewsFeedModel!
    var isLoadData = true
    var isUpdgradeToPrimium = true
    var iosPackage_Id:String = ""
    var packageMasterId = ""
    
    var lblCountHeader = UILabel()
    
    var shouldLoadMore = true {
        didSet {
            if shouldLoadMore == false {
                IBtblNewFeed.tableFooterView = UIView()
            } else {
                IBtblNewFeed.tableFooterView = DoviesGlobalUtility.loadingFooter()
            }
        }
    }
    
    var updateTableCells:(()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.removeSideMenuButton()
        self.navigationController?.addSideMenuButtonBlackImage()
        
        NewsFeedHorizontalTableCell.appearance().backgroundColor = UIColor.black
        NewsFeedVerticalTableCell.appearance().backgroundColor = UIColor.black
        UIApplication.shared.statusBarStyle = .lightContent
        
        self.view.backgroundColor = UIColor.appBlackColorNew()
        IBtblNewFeed.backgroundColor = UIColor.appBlackColorNew()
        
        IBtblNewFeed.estimatedRowHeight = 700.0
        IBtblNewFeed.rowHeight = UITableViewAutomaticDimension
        
        lblCountHeader.adjustsFontSizeToFitWidth = true
        lblCountHeader.minimumScaleFactor = 0.1
        lblCountHeader.isHidden = true
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("Notification_TabbaritemClick"), object: nil)
        
        self.navigationItem.title = "Feed".uppercased()
        self.setUpViewNeeds()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.GetsubscriptionStatus), name: NSNotification.Name(rawValue:"UpgradeToPrimiumStatus"), object: nil)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @objc func GetsubscriptionStatus(notification: NSNotification) {
        
        if isUpdgradeToPrimium
        {
            isUpdgradeToPrimium = false
            
            let subStatus = UserDefaults.standard.bool(forKey: WsParam.isSubscribed) as? Bool ?? false
            
            if subStatus == false{
                DoviesGlobalUtility.showSubscriptionPopupOnRegistration = false
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
                    
                    if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
                    {
                    }
                    else{
                        self.showSubscriptionScreen()
                    }
                }
            }
        }
    }
    
    
    @objc func methodOfReceivedNotification(notification: Notification) {
        
        self.NewsfeedTablecontentOffset()
    }
    
    
    //    func NewsfeedTablecontentOffset()
    //    {
    //        DispatchQueue.main.async(execute: {
    //
    //            self.IBtblNewFeed.setContentOffset(CGPoint(x: 0, y: -90), animated: true)
    //            self.IBtblNewFeed.layoutIfNeeded()
    //        })
    //
    //    }
    func NewsfeedTablecontentOffset()
    {
        DispatchQueue.main.async(execute: {
            
            let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
                (self.navigationController?.navigationBar.frame.height ?? 0.0)
            
            self.IBtblNewFeed.setContentOffset(CGPoint(x: 0, y: -topBarHeight), animated: true)
            self.IBtblNewFeed.layoutIfNeeded()
        })
        
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        print("topBarHeighttopBarHeight\(topBarHeight)")
        
        
        self.IBtblNewFeed.contentInset = UIEdgeInsets(top:  topBarHeight , left: 0, bottom: self.tabBarController!.tabBar.frame.height, right: 0)
        
        
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor : UIColor.white,
            NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]
        
        self.getUserProfileWSCall()
        if notificationVC.readUnreadStatus == "Read"
        {
            self.lblCountHeader.isHidden = true
        }
        else if notificationVC.readUnreadStatus == "Unread"
        {
            self.lblCountHeader.text = DoviesGlobalUtility.currentUser?.notifyCount
            self.lblCountHeader.isHidden = false
        }
        
        self.addRightNavigationButton()
        
        let MyprofileNav = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
        
        APP_DELEGATE.profileNavController = MyprofileNav.navigationController.self
        IBtblNewFeed.reloadData()
        pullToTop()
        
        self.IBtblNewFeed.isScrollEnabled = true
        if isLoadData || otherNewsArr.count == 0 , featuredNewsArr.count == 0{
            DispatchQueue.main.async { [weak self] in
                guard let weakSelf = self else{return}
                // weakSelf.placeholderCellsCount = 10
                weakSelf.placeholderCellsCount = 0
                
                weakSelf.IBtblNewFeed.reloadData()
                weakSelf.IBtblNewFeed.layoutIfNeeded()
                weakSelf.IBtblNewFeed.showLoader()
                
            }
            self.callNewsFeedListingWS()
        }
        
        if APP_DELEGATE.isFromNotification{
            self.callNewsFeedListingWS()
            APP_DELEGATE.isFromNotification = false
        }
    }
    
    func getUserProfileWSCall(){
        DoviesWSCalls.callGetUserInfoAPI(dicParam: nil, onSuccess: {[weak self] (isSuccess, message, response) in
            guard let weakSelf = self else{return}
            if isSuccess, let dic = response {
                UserDefaults.standard.set(dic, forKey: UserDefaultsKey.userData)
                DoviesGlobalUtility.currentUser = User.init(fromDictionary: dic)
                
                if DoviesGlobalUtility.currentUser?.notifyCount == "0"
                {
                    notificationVC.readUnreadStatus = "Read"
                    self!.lblCountHeader.isHidden = true
                }
                else
                {
                    notificationVC.readUnreadStatus = "Unread"
                    self?.lblCountHeader.text = DoviesGlobalUtility.currentUser?.notifyCount
                    self!.lblCountHeader.isHidden = false
                }
            }
        }) { (error) in
        }
    }
    
    func addRightNavigationButton(){
        
        let detailNotification = UIButton(type: .custom)
        detailNotification.setImage(UIImage(named:"Notification_IconHeaderNew"), for: .normal)
        detailNotification.setImage(UIImage(named:"Notification_IconHeaderNew"), for: .highlighted)
        detailNotification.addTarget(self, action: #selector(self.didTapNotificationButton), for: .touchUpInside)
        detailNotification.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        let detailsBarItem = UIBarButtonItem(customView: detailNotification)
        navigationItem.rightBarButtonItems = [detailsBarItem]
        
        lblCountHeader.frame = CGRect(x: 18, y: -5, width: 16, height: 16)
        
        lblCountHeader.font = UIFont.robotoRegular(size: 10)
        lblCountHeader.layer.masksToBounds = true
        lblCountHeader.layer.cornerRadius = 8
        lblCountHeader.textColor = UIColor.white
        lblCountHeader.textAlignment = .center
        lblCountHeader.backgroundColor = UIColor.red
        detailNotification.addSubview(lblCountHeader)
    }
    
    
    @objc func didTapNotificationButton(){
        print("NotificationVC")
        
        let Notification = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "NotificationVC") as? NotificationVC
        self.navigationController?.pushViewController(Notification!, animated: false)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setUpViewNeeds(){
        
        IBtblNewFeed.register(UINib(nibName: "NewsFeedHorizontalTableCell", bundle: nil), forCellReuseIdentifier: "NewsFeedHorizontalTableCell")
        IBtblNewFeed.register(UINib(nibName: "NewsFeedVerticalTableCell", bundle: nil), forCellReuseIdentifier: "NewsFeedVerticalTableCell")
        
        self.pullToRefreshCtrl = UIRefreshControl()
        self.pullToRefreshCtrl.addTarget(self, action: #selector(self.pullToRefreshClick(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            self.IBtblNewFeed.refreshControl  = self.pullToRefreshCtrl
        } else {
            self.IBtblNewFeed.addSubview(self.pullToRefreshCtrl)
        }
        
        //update cells after like or favourite status changed in details screen
        updateTableCells = { [weak self] in
            guard let weakSelf = self, let inedexPath = weakSelf.selectedIndexPath else{return}
            weakSelf.IBtblNewFeed.reloadRows(at: [inedexPath], with: .none)
        }
        
    }
    
    func pullToTop(){
        
        let cell1 = IBtblNewFeed.dequeueReusableCell(withIdentifier: "NewsFeedHorizontalTableCell") as? NewsFeedHorizontalTableCell
        cell1?.recommendedCollection.contentOffset.x = 0
    }
    
    @objc func pullToRefreshClick(sender:UIRefreshControl) {
        currentPageIndex = 1
        shouldLoadMore = false
        callNewsFeedListingWS()
        sender.endRefreshing()
        
        // loks Update subscription status in side menu
        if let viewController = self.sideMenuController?.sideViewController as? SideMenuViewController{
            viewController.setSubscriptionStatus()
        }
    }
    
    func pushToSubscriptionScreen(){
        if let upgradeVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "UpgradeToPremiumViewController") as? UpgradeToPremiumViewController{
            upgradeVC.onPurchaseSuccess = {
                //self.callWorkOutDetailAPI()
            }
            self.navigationController?.pushViewController(upgradeVC, animated: false)
        }
    }
    
    func showSubscriptionScreen(){
        if let upgradeVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "UpgradeToPremiumViewController") as? UpgradeToPremiumViewController{
            upgradeVC.onPurchaseSuccess = {
                // loks add code for subscription update status
                if let viewController = self.sideMenuController?.sideViewController as? SideMenuViewController{
                    viewController.setSubscriptionStatus()
                }
                //self.callWorkOutDetailAPI()
            }
            
            upgradeVC.isPresent = true
            let subscriptionNav = UINavigationController(rootViewController: upgradeVC)
            self.present(subscriptionNav, animated: true, completion:nil)
        }
    }
    
    
    func redirectToNewsFeedDetailsBasedOnSelectedFeed(){
        guard let selectedFeed = self.selectedNewsFeed else{return}
        switch selectedFeed.newsMediaType.uppercased() {
        case NewsFeedContentType.image.rawValue.uppercased(), NewsFeedContentType.text.rawValue.uppercased(), NewsFeedContentType.video.rawValue.uppercased():
            
            self.performSegue(withIdentifier: "gotoNewsFeedMedia", sender: nil)
            
        default:
            print("")
        }
    }
    
    
    // MARK: - Navigation
    func redirectToRespectiveDetailsBasedOnSelectedFeed(){
        guard let selectedFeed = self.selectedNewsFeed else{return}
        switch selectedFeed.newsMediaType.uppercased() {
        case NewsFeedContentType.image.rawValue.uppercased(), NewsFeedContentType.text.rawValue.uppercased(), NewsFeedContentType.video.rawValue.uppercased():
            
            if  selectedFeed.newsMediaType.uppercased() == "IMAGE"
            {
            }
            else
            {
                self.performSegue(withIdentifier: "gotoNewsFeedMedia", sender: nil)
            }
            
        case NewsFeedContentType.dietPlan.rawValue.uppercased():
            let dietDetailsVC = Storyboard.DietPlan.instantiate(DietPlanDetailsVC.self)
            dietDetailsVC.planId = selectedFeed.newsModuleId
            self.navigationController?.pushViewController(dietDetailsVC, animated: true)
            
        case NewsFeedContentType.workout.rawValue.uppercased():
            let featuredWorkOutDetailViewControlle = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "FeaturedWorkOutDetailViewController") as! FeaturedWorkOutDetailViewController
            featuredWorkOutDetailViewControlle.navigationItem.title = selectedFeed.newsTitle
            featuredWorkOutDetailViewControlle.workoutId = selectedFeed.newsModuleId
            self.navigationController?.pushViewController(featuredWorkOutDetailViewControlle, animated: true)
            
        case NewsFeedContentType.program.rawValue.uppercased():
            let programDetailsVC = Storyboard.Programs.instantiate(ProgramDetail_VC.self)
            programDetailsVC.programId = selectedFeed.newsModuleId
            self.navigationController?.pushViewController(programDetailsVC, animated: true)
            
        case NewsFeedContentType.Exercise.rawValue.uppercased():
            let workoutDetailsVC = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "WorkoutDetailsVC") as! WorkoutDetailsVC
            workoutDetailsVC.screenType = .workOutDetailList
            workoutDetailsVC.exerciseId = selectedFeed.newsModuleId
            workoutDetailsVC.navigationItem.title = selectedFeed.newsTitle
            self.navigationController?.pushViewController(workoutDetailsVC, animated: true)
        default:
            print("")
        }
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let newsFeedDetails = segue.destination as? NewsFeedMediaDetailsVC{
            newsFeedDetails.newsFeed = selectedNewsFeed
            newsFeedDetails.isAllowCommenting = selectedNewsFeed.newsCommentAllow
        }
    }
    
}

extension NewsFeedViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard otherNewsArr.count > indexPath.row else{return}
        selectedNewsFeed = otherNewsArr[indexPath.row]
        selectedIndexPath = indexPath
        redirectToRespectiveDetailsBasedOnSelectedFeed()
        
    }
    
//    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
//        return 0.001
//    }
//
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 0.001
//    }
//
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 0{
            return collectionCellHeight
        }else{
            return tableLandScapeCellHeight
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 && indexPath.row == 0{
            
            if  featuredNewsArr.count > 0 {
                return collectionCellHeight + 10
            }else{
                return UITableViewAutomaticDimension
            }
            
        }else{
            return UITableViewAutomaticDimension
        }
    }
}

extension NewsFeedViewController: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if  featuredNewsArr.count > 0 {
            return 2
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isLoadData{
            return placeholderCellsCount
        }else{
            if section == 0 && featuredNewsArr.count > 0{
                return 1
            }
            return otherNewsArr.count
            
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 && featuredNewsArr.count > 0{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsFeedHorizontalTableCell") as? NewsFeedHorizontalTableCell else{return UITableViewCell()}
            cell.recommendedCollection.register(UINib(nibName: "NewsFeedHorizontalCollectionCell", bundle: nil), forCellWithReuseIdentifier: "NewsFeedHorizontalCollectionCell")
            cell.contentView.backgroundColor = UIColor.appBlackColorNew()
            cell.recommendedCollection.dataSource = self
            cell.recommendedCollection.delegate = self
            cell.ViewBackground.backgroundColor = UIColor.appBlackColorNew()
            
            cell.recommendedCollection.reloadData()
            return cell
        }
        else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsFeedVerticalTableCell") as? NewsFeedVerticalTableCell else {return UITableViewCell()}
            if otherNewsArr.count > 0{
                
                
                cell.setNewsFeedCellUI(newsFeed: self.otherNewsArr[indexPath.row], indexPath: indexPath ,isFromNewsFeed : true)
                
                cell.View_DobleGesture.backgroundColor = UIColor.appBlackColorNew()
                
                //Like button action
                cell.likeTapped = {  (newsFeedObj , likeButton,iPath) in
                    let selectedCell = self.IBtblNewFeed.cellForRow(at: iPath) as! NewsFeedVerticalTableCell
                    likeButton.isSelected = !likeButton.isSelected
                    newsFeedObj.newsLikeStatus = !newsFeedObj.newsLikeStatus
                    likeButton.isUserInteractionEnabled = false
                    if let likesCount = Int(newsFeedObj.customerLikes){
                        var lCount = likesCount
                        if newsFeedObj.newsLikeStatus{
                            lCount += 1
                        }else{
                            lCount -= 1
                        }
                        newsFeedObj.customerLikes = "\(lCount)"
                        
                        if newsFeedObj.customerLikes == "0" || newsFeedObj.customerLikes == "1"
                        {
                            selectedCell.IBbtnNumberOfLikes.setTitle("\(lCount) like", for: .normal)
                        }
                        else
                        {
                            selectedCell.IBbtnNumberOfLikes.setTitle("\(lCount) likes", for: .normal)
                        }
                    }
                    
                    self.callNewsFeedLikeAPI(with: newsFeedObj, likeButton: likeButton)
                }
                
                // More button
                cell.addMoreTapped = {  (newsFeedObj , moreButton,iPath) in
                    
                    guard self.otherNewsArr.count > indexPath.row else{return}
                    self.selectedNewsFeed = self.otherNewsArr[iPath.row]
                    self.selectedIndexPath = iPath
                    self.redirectToNewsFeedDetailsBasedOnSelectedFeed()
                }
                
                //Comment button action
                cell.commentTapped = {  (newsFeedObj, iPath) in
                    
                    let comments = self.storyboard?.instantiateViewController(withIdentifier: "CommentsVC")as! CommentsVC
                    comments.newsFeed = newsFeedObj
                    self.selectedIndexPath  = iPath
                    comments.isAllowCommentsAdd = newsFeedObj.newsCommentAllow
                    self.navigationController?.pushViewController(comments, animated: true)
                }
                
                //share button action
                cell.shareTapped = {  (newsFeedObj) in
                    
                    DoviesGlobalUtility.showShareSheet(on: self, shareText: newsFeedObj.newsShareUrl)
                    
                }
                
                cell.favouriteTapped = { (newsFeedObj , favoriteButton,iPath) in
                    favoriteButton.isSelected = !favoriteButton.isSelected
                    newsFeedObj.newsFavStatus = !newsFeedObj.newsFavStatus
                    favoriteButton.isUserInteractionEnabled = false
                    self.callNewsFeedFavouriteAPI(with: newsFeedObj, favButton : favoriteButton)
                }
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (otherNewsArr.count - 1) && shouldLoadMore == true {
            callNewsFeedListingWS()
        }
    }
}

extension NewsFeedViewController:UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return featuredNewsArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "NewsFeedHorizontalCollectionCell", for: indexPath) as? NewsFeedHorizontalCollectionCell else{return UICollectionViewCell()}
        cell.contentView.backgroundColor = UIColor.black
        cell.setNewsFeedCellUI(newsFeed: self.featuredNewsArr[indexPath.item])
        return cell
    }
}

extension NewsFeedViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard featuredNewsArr.count > indexPath.item else{return}
        selectedNewsFeed = featuredNewsArr[indexPath.item]
        redirectToRespectiveDetailsBasedOnSelectedFeed()
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: NewsFeedViewController.cellWidth - 10 ,height: NewsFeedViewController.cellWidth - 10)
    }
    
}

//API Calls
extension NewsFeedViewController{
    
    /**
     This method used to call feed listing WS.
     */
    func callNewsFeedListingWS(){
        guard let cUser = DoviesGlobalUtility.currentUser else {return}
        var dic = [String: Any]()
        dic[WsParam.authCustomerId] = cUser.customerAuthToken
        dic[WsParam.pageIndex] = "\(currentPageIndex)"
        DoviesWSCalls.callGetNewsFeedsAPI(dicParam: dic, onSuccess: { [weak self](success, featuredNews, otherNews, message,receiptStatus,iosPackageId,pakageMasterId, nextPageStatus) in
            guard let weakSelf = self else{return}
            
            let ReciptRecived = receiptStatus ?? ""
            if ReciptRecived  == "0"
            {
                weakSelf.iosPackage_Id = iosPackageId ?? ""
                weakSelf.packageMasterId = pakageMasterId ?? ""
                // weakSelf.iosPackage_Id != ""  condition by Manish sir
                
                if weakSelf.iosPackage_Id != ""
                {
                    sharedobjec.restorePurchases()
                    DispatchQueue.main.async {
                        self?.uploadReceipt { (success) in
                        }
                    }
                }
            }
                
            else if ReciptRecived  == "2"
            {
                let  ProductPurcasedStatus = UserDefaults.standard.string(forKey: UserDefaultsKey.productPurchased)
                let  CustomerId  = UserDefaults.standard.string(forKey: UserDefaultsKey.customerIdbyPayment)
                
                if  ProductPurcasedStatus == "purchased" && DoviesGlobalUtility.currentUser?.customerId == CustomerId
                {
                    let  packageMasterId = UserDefaults.standard.string(forKey: UserDefaultsKey.packageMasterId)
                    weakSelf.packageMasterId = packageMasterId ?? ""
                    print("pakeage masterId -  \(packageMasterId ?? "")")
                    sharedobjec.restorePurchases()
                    DispatchQueue.main.async {
                        self?.uploadReceipt { (success) in
                        }
                    }
                }
            }
            
            weakSelf.isLoadData = false
            if success{
                weakSelf.featuredNewsArr =  featuredNews!
                if weakSelf.currentPageIndex == 1{
                    weakSelf.otherNewsArr = otherNews!
                    weakSelf.emptyView.isHidden = weakSelf.otherNewsArr.count > 0 || weakSelf.featuredNewsArr.count > 0
                }else{
                    weakSelf.otherNewsArr +=  otherNews!
                }
            }else{
                if weakSelf.currentPageIndex == 1{
                    weakSelf.emptyView.isHidden = false
                }
            }
            
            if nextPageStatus{
                weakSelf.currentPageIndex += 1
            }
            weakSelf.shouldLoadMore = nextPageStatus
            weakSelf.placeholderCellsCount = 0
            weakSelf.IBtblNewFeed.hideLoader()
            weakSelf.IBtblNewFeed.reloadData()
            weakSelf.pullToTop()
        }) { (error) in
        }
    }
    
    /**
     This method used to like or dislike a news feed
     */
    func callNewsFeedLikeAPI(with newsFeed : NewsFeedModel, likeButton : UIButton){
        guard let cUser = DoviesGlobalUtility.currentUser else {return}
        var dic = [String: Any]()
        dic[WsParam.authCustomerId] = cUser.customerAuthToken
        dic[WsParam.moduleId] = newsFeed.newsId
        dic[WsParam.moduleName] = ModuleName.newsFeed
        
        DoviesWSCalls.callLikeDislikeAPI(dicParam: dic, onSuccess: { (success, message, responseDict) in
            likeButton.isUserInteractionEnabled = true
            
            if !success{
                likeButton.isSelected = !likeButton.isSelected
                newsFeed.newsLikeStatus = !newsFeed.newsLikeStatus
            }
        }) { (error) in
            likeButton.isUserInteractionEnabled = true
            likeButton.isSelected = !likeButton.isSelected
            newsFeed.newsLikeStatus = !newsFeed.newsLikeStatus
        }
    }
    
    /**
     This method used to favourite a news feed
     */
    func callNewsFeedFavouriteAPI(with newsFeed : NewsFeedModel, favButton : UIButton){
        favButton.isUserInteractionEnabled = false
        guard let cUser = DoviesGlobalUtility.currentUser else{return}
        var parameters = [String : Any]()
        parameters[WsParam.authCustomerId] =  cUser.customerAuthToken
        parameters[WsParam.moduleId] = newsFeed.newsId
        parameters[WsParam.moduleName] = ModuleName.newsFeed
        
        DoviesWSCalls.callFavouriteAPI(dicParam: parameters, onSuccess: { (success, message, response) in
            
            favButton.isUserInteractionEnabled = true
            if !success{
                favButton.isSelected = !favButton.isSelected
                newsFeed.newsFavStatus = !newsFeed.newsFavStatus
            }
            
        }) { (error) in
            favButton.isUserInteractionEnabled = true
            favButton.isSelected = !favButton.isSelected
            newsFeed.newsFavStatus = !newsFeed.newsFavStatus
        }
    }
    
    //MARK:Upload  current Recipt from server ----xxxx start --xxxx
    
    func uploadReceipt(completion: ((_ success: Bool) -> Void)? = nil) {
        DispatchQueue.main.async {
            
            if let receiptData = self.loadReceipt() {
                DispatchQueue.main.async {
                    self.upload(receipt: receiptData)
                }
            }
        }
    }
    
    private func loadReceipt() -> Data? {
        
        guard let url = Bundle.main.appStoreReceiptURL else {
            return nil
        }
        do {
            let data = try Data(contentsOf: url)
            return data
        } catch {
            return nil
        }
    }
    
    func upload(receipt data: Data) {
        
        
        let receiptdataBase64 = data.base64EncodedString()
        
        let body = [
            "receipt-data":receiptdataBase64 ,
            "password": WsParam.itunesPassword
        ]
        let bodyData = try! JSONSerialization.data(withJSONObject: body, options: [])
        
        let url = URL(string: WsUrl.verifyReceiptUrl)!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = bodyData
        
        let task = URLSession.shared.dataTask(with: request) { (responseData, response, error) in
            
            DispatchQueue.main.async {
                
                if let responseData = responseData {
                    let json = try! JSONSerialization.jsonObject(with: responseData, options: []) as! Dictionary<String, Any>
                    
                    if let receiptInfo: NSArray = json["latest_receipt_info"] as? NSArray {
                        
                        //  print("latestReceiptInfoList----\(receiptInfo)")
                        //LatestRecipt
                        let lastReceipt = receiptInfo.lastObject as! NSDictionary
                        
                        //  print("latestReceipt----\(receiptInfo)")
                        
                        self.callCompletePaymentAPI(purchaseDetails: lastReceipt as! [String : String], modelPackageid: self.packageMasterId, encodedString: receiptdataBase64)
                    }
                }
            }
        }
        
        task.resume()
    }
    
    
    func callCompletePaymentAPI(purchaseDetails: [String : String], modelPackageid : String, encodedString : String){
        
        var tansactionDetail_Info = [String: Any]()
        tansactionDetail_Info["expires_date"] = purchaseDetails["expires_date"] ?? ""
        tansactionDetail_Info["expires_date_ms"] = purchaseDetails["expires_date_ms"] ?? ""
        tansactionDetail_Info["expires_date_pst"] = purchaseDetails["expires_date_pst"] ?? ""
        tansactionDetail_Info["is_in_intro_offer_period"] = purchaseDetails["is_in_intro_offer_period"] ?? ""
        tansactionDetail_Info["is_trial_period"] = purchaseDetails["is_trial_period"] ?? ""
        tansactionDetail_Info["original_purchase_date"] = purchaseDetails["original_purchase_date"] ?? ""
        tansactionDetail_Info["original_purchase_date_ms"] = purchaseDetails["original_purchase_date_ms"] ?? ""
        tansactionDetail_Info["original_purchase_date_pst"] = purchaseDetails["original_purchase_date_pst"] ?? ""
        tansactionDetail_Info["original_transaction_id"] = purchaseDetails["original_transaction_id"] ?? ""
        tansactionDetail_Info["product_id"] = purchaseDetails["product_id"] ?? ""
        tansactionDetail_Info["purchase_date"] = purchaseDetails["purchase_date"] ?? ""
        tansactionDetail_Info["purchase_date_ms"] = purchaseDetails["purchase_date_ms"] ?? ""
        tansactionDetail_Info["purchase_date_pst"] = purchaseDetails["purchase_date_pst"] ?? ""
        tansactionDetail_Info["quantity"] = purchaseDetails["quantity"] ?? ""
        tansactionDetail_Info["transaction_id"] = purchaseDetails["transaction_id"] ?? ""
        tansactionDetail_Info["web_order_line_item_id"] = purchaseDetails["web_order_line_item_id"] ?? ""
        
        tansactionDetail_Info["latest_receipt"] = encodedString
        
        let dataTransactionDetail = try! JSONSerialization.data(withJSONObject:tansactionDetail_Info, options: [ ])
        
        let jsonStringTransactionDetail = String(data: dataTransactionDetail, encoding: .utf8)
        
        var tansactionDetailsDict = [String: Any]()
        tansactionDetailsDict[WsParam.productId] = purchaseDetails["product_id"]
        tansactionDetailsDict[WsParam.quantity] = purchaseDetails["quantity"]
        tansactionDetailsDict[WsParam.encodedKey] = encodedString
        
        if let tID = purchaseDetails["transaction_id"]{
            tansactionDetailsDict[WsParam.transactionId] = tID
        }
        let data = try! JSONSerialization.data(withJSONObject:tansactionDetailsDict, options: [ ])
        guard let cUser = DoviesGlobalUtility.currentUser, let jsonString = String(data: data, encoding: .utf8) else{return}
        var dic = [String: Any]()
        dic["package_id"] = modelPackageid
        dic[WsParam.authCustomerId] =  cUser.customerAuthToken
        
        if encodedString != ""
        {
            dic["transaction_detail"] = jsonString
            dic["parsed_receipt_detail"] = jsonStringTransactionDetail
        }
        else
        {
            dic["transaction_detail"] = ""
            dic["parsed_receipt_detail"] = ""
        }
        
        DoviesWSCalls.callCompletePaymentAPI(dicParam: dic, onSuccess: { (success, msg) in
            GlobalUtility.hideActivityIndi(viewContView: self.view)
            if let sideMenu = self.sideMenuController?.sideViewController as? SideMenuViewController{
                sideMenu.setSubscriptionStatus()
            }
            
        }) { (error) in
            GlobalUtility.hideActivityIndi(viewContView: self.view)
        }
    }
    //MARK:Upload  current Recipt from server ----xxxx END --xxxx
}


