//
//  SubOrPurHistoryVC.swift
//  Dovies
//
//  Created by hb on 28/04/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import SJSegmentedScrollView

class SubOrPurHistoryVC: BaseViewController,SJSegmentedViewControllerDelegate {

    var selectedSegment: SJSegmentTab?
    let segmentController = SJSegmentedViewController()
    var selectIndexForFavourite = 0
    var isSelectedTab = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton  = true
        self.navigationItem.title = "My Payments".uppercased()
        self.addNavigationBackButton()
        self.initialSetUp()
    }
        func initialSetUp(){
            if let storyboard = self.storyboard {
            let PurHistory = (storyboard.instantiateViewController(withIdentifier: "PurchaseHistoryVC")) as! PurchaseHistoryVC
                PurHistory.title = "Purchase History"
            let SubHistory = (storyboard.instantiateViewController(withIdentifier: "SubHistoryVC")) as! SubHistoryVC
                    SubHistory.title = "Subscription History"
                segmentController.segmentControllers = [PurHistory,SubHistory]
                }
                segmentController.headerViewController = UIViewController()
               // segmentController.segmentControllers = [purchas ]
                segmentController.headerViewHeight = 0
                segmentController.selectedSegmentViewHeight = 2.0
                segmentController.headerViewOffsetHeight = 0
                segmentController.segmentTitleColor = UIColor.colorWithRGB(r: 153, g: 153, b: 153)
                segmentController.selectedSegmentViewColor = UIColor.colorWithRGB(r: 34, g: 34, b: 34)
                segmentController.segmentShadow = SJShadow.light()
                segmentController.segmentBounces = true
                segmentController.delegate = self
                segmentController.segmentTitleFont = UIFont.tamilRegular(size: 14.0)
                addChildViewController(segmentController)
                self.view.addSubview(segmentController.view)
                segmentController.didMove(toParentViewController: self)
                segmentController.setSelectedSegmentAt(selectIndexForFavourite, animated: false)
        }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func didMoveToPage(_ controller: UIViewController, segment: SJSegmentTab?, index: Int) {
        if selectedSegment != nil {
            selectedSegment?.titleColor(UIColor.colorWithRGB(r: 153, g: 153, b: 153))
        }
        
        if segmentController.segments.count > 0 {
            selectedSegment = segmentController.segments[index]
            selectedSegment?.titleColor(.black)
        }
    }

}
