//
//  ModelVideoListing.swift
//  Dovies
//
//  Created by Neel Shah on 16/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import AVFoundation

class ModelVideoListing: NSObject {
	
	var urlVideoPath: URL!
	var sVideoInSeconds: String!
	var sOriginalVideoInSeconds: String!
	var fTotalSeconds: CGFloat!
	var fOriginalTotalSeconds: CGFloat!
	var fBreakTime: Int!
	var isBreakTimeThere = false
	var isTimeRepeat = true

	var isSetOfReps = false
	var sVideoName: String!
	var sBrackTime: String!
	var sTotalTime: String!
	var isWorkoutFav: Bool!
	var workoutExercisesId: String!
    var workoutRepeatText: String!
	
	static var downloadRequest: DownloadRequest?
	static var downloadRequestWorkout: DownloadRequest?
	
	class func downloadVideoZip(sVideoZipUrl: String, modelVideo: ModelVideoListing ,sFileName: String, currentProgress:((_ progress: Progress) -> ())?, response:((_ response: DefaultDownloadResponse) -> ())?)  -> DownloadRequest {
		
		ModelVideoListing.downloadRequestWorkout?.cancel()
		
		ModelVideoListing.downloadRequestWorkout = download(
			sVideoZipUrl,
			method: .get,
			parameters: nil,
			encoding: JSONEncoding.default,
			headers: nil,
			to: { temporaryURL, response in
				
				//fileManager
				let fileManager = FileManager.default
				let directoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
				
				//create folder 'Video' if does not exist
				let folderPathComponent = directoryURL.appendingPathComponent("Videos").appendingPathComponent("\(sFileName)")
				if !fileManager.fileExists(atPath: folderPathComponent.path) {
					do {
						try fileManager.createDirectory(atPath: folderPathComponent.path, withIntermediateDirectories: true, attributes: nil)
					} catch let error as NSError {
						print("error----\(error.localizedDescription)");
					}
				}
				
				let fileUrl = DoviesGlobalUtility.pathForVideoZip(fileName: sFileName, downloadUrl: sVideoZipUrl)
				return (fileUrl, [])
			
		}).downloadProgress(closure: { (progress) in
			//progress closure
			print("progress----\(progress.fractionCompleted)")
			currentProgress?(progress)
			
		}).response(completionHandler: { (DefaultDownloadResponse) in
			//result closure
			print(DefaultDownloadResponse)
			
			let success: Bool = SSZipArchive.unzipFile(atPath: DoviesGlobalUtility.tempZipPath(fileName: sFileName),
													   toDestination: DoviesGlobalUtility.tempUnzipPath(fileName: sFileName),
													   preserveAttributes: true,
													   overwrite: true,
													   nestedZipLevel: 1,
													   password: nil,
													   error: nil,
													   delegate: nil,
													   progressHandler: nil,
													   completionHandler: { (path, success, error) in
														if success == true{
															let fileManager = FileManager.default
															do {
																try fileManager.removeItem(atPath: path)
															} catch let error as NSError {
																print("error----\(error.localizedDescription)");
															}
														}
														print(path)
														
			})
			
			if success != false {
				print("Success unzip")
			} else {
				print("No success unzip")
				return
			}
			response?(DefaultDownloadResponse)
		})
		
		return ModelVideoListing.downloadRequestWorkout!
	}
	
	class func createStaticJson(arrModelWorkoutExerciseList: [ModelWorkoutExerciseList]) -> [ModelVideoListing]{
		var arrModel = [ModelVideoListing]()
		
		for exercise in arrModelWorkoutExerciseList{
			let model = ModelVideoListing()
            guard arrModelWorkoutExerciseList.count > 0 else{return arrModel}
			
			model.sVideoName = exercise.workoutExerciseName
			let theFileName = (exercise.workoutExerciseVideo as NSString).lastPathComponent
			
			if theFileName.isEmpty == true
			{
				
			}
			else
			{
				let urlString = DoviesGlobalUtility.pathForVideoFile(videoFileName: theFileName)
				model.urlVideoPath = urlString
				model.sBrackTime = exercise.workoutExerciseBreakTime
				model.isWorkoutFav = exercise.workoutExerciseIsFavourite
				model.workoutExercisesId = exercise.workoutExercisesId
				model.workoutRepeatText = exercise.workoutRepeatText
				var timeArray = exercise.workoutExercisesTime.components(separatedBy: ":")
				var secondsToInterval = 0
				if timeArray.count > 0{
					if timeArray.count > 1{
						secondsToInterval = secondsToInterval + (Int(timeArray.first!)! * 3600)
					}
					if timeArray.count > 2{
						secondsToInterval = secondsToInterval + (Int(timeArray[1])! * 60)
					}
					if timeArray.count == 3{
						secondsToInterval = secondsToInterval + Int(timeArray.last!)!
					}
				}
				
				let seekingCM = CMTimeMakeWithSeconds(Float64(secondsToInterval), 1000000)
				
				timeArray = exercise.workoutExerciseBreakTime.components(separatedBy: ":")
				secondsToInterval = 0
				if timeArray.count > 0{
					if timeArray.count > 1{
						secondsToInterval = secondsToInterval + (Int(timeArray.first!)! * 3600)
					}
					if timeArray.count > 2{
						secondsToInterval = secondsToInterval + (Int(timeArray[1])! * 60)
					}
					if timeArray.count == 3{
						secondsToInterval = secondsToInterval + Int(timeArray.last!)!
					}
				}
				
				model.fBreakTime = secondsToInterval
				
				if secondsToInterval > 0{
					model.isBreakTimeThere = true
				}
				
	//			let time = ModelVideoListing.getVideoTotalDuration(sourceURL: path)
				
				if exercise.workoutExerciseType.uppercased() == "TIME"
				{
					model.isTimeRepeat = true
					model.fOriginalTotalSeconds = CGFloat(CMTimeGetSeconds(seekingCM))
					model.fTotalSeconds = CGFloat(CMTimeGetSeconds(seekingCM))
				}
				else
				{
					model.isTimeRepeat = true
					model.fOriginalTotalSeconds = CGFloat(CMTimeGetSeconds(seekingCM))
					model.fTotalSeconds = CGFloat(CMTimeGetSeconds(seekingCM))
					model.isSetOfReps = true
				}
				
				let dMinutes: Int = Int(model.fTotalSeconds) % 3600 / 60
				let dSeconds: Int = Int(model.fTotalSeconds) % 3600 % 60
				model.sVideoInSeconds = "\(dMinutes):\(String(format: "%02d", dSeconds))"
				model.sOriginalVideoInSeconds = model.sVideoInSeconds
				
				arrModel.append(model)
			}
		}
		
		return arrModel
	}
	
	class func extractFirstFrame(fromFilepath filepath: String) -> UIImage {
		let movieAsset = AVURLAsset(url: URL(fileURLWithPath: filepath), options: nil)
		let assetImageGemerator = AVAssetImageGenerator(asset: movieAsset)
		assetImageGemerator.appliesPreferredTrackTransform = true
		let frameRef = (try? assetImageGemerator.copyCGImage(at: CMTimeMake(1, 2), actualTime: nil))
		return UIImage(cgImage: frameRef!)
	}
	
	class func getVideoTotalDuration(sourceURL:URL) -> CMTime{
		let asset = AVAsset(url: sourceURL)
		
		return asset.duration
	}
	
	class func thumbnail(sourceURL:URL) -> (UIImage, CMTime) {
		let asset = AVAsset(url: sourceURL)
//		print(CMTimeGetSeconds(asset.duration))
		let imageGenerator = AVAssetImageGenerator(asset: asset)
		let time = CMTime(seconds: 1, preferredTimescale: 1)
		
		do {
			let imageRef = try imageGenerator.copyCGImage(at: time, actualTime: nil)
			return (UIImage(cgImage: imageRef), asset.duration)
		} catch {
			print(error)
			return (UIImage(named: "some generic thumbnail")!, kCMTimeZero)
		}
	}
	
	class func downloadVideoForExercise(sVideoZipUrl: String, sFileName: String, workoutId: String = "" , currentProgress:((_ progress: Progress, _ sVideoZipUrl: String, _ workoutId: String) -> ())?, response:((_ response: DefaultDownloadResponse, _ workoutId: String) -> ())?) -> DownloadRequest{
		
		ModelVideoListing.downloadRequest?.cancel()
		
		ModelVideoListing.downloadRequest = download(
			sVideoZipUrl,
			method: .get,
			parameters: nil,
			encoding: JSONEncoding.default,
			headers: nil,
			to: { temporaryURL, response in
				
				//fileManager
				let fileManager = FileManager.default
				let directoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
				
				//create folder 'Video' if does not exist
				let folderPathComponent = directoryURL.appendingPathComponent("Videos")
				if !fileManager.fileExists(atPath: folderPathComponent.path) {
					do {
						try fileManager.createDirectory(atPath: folderPathComponent.path, withIntermediateDirectories: false, attributes: nil)
					} catch let error as NSError {
						print("error----\(error.localizedDescription)");
					}
				}
				
				let fileUrl = DoviesGlobalUtility.pathForVideoFile(videoFileName: sFileName)
				return (fileUrl, [])
				
		}).downloadProgress(closure: { (progress) in
			//progress closure
			print("progress----\(progress.fractionCompleted)")
			currentProgress?(progress, sVideoZipUrl, workoutId)
			
		}).response(completionHandler: { (DefaultDownloadResponse) in
			//result closure
			print(DefaultDownloadResponse)
			response?(DefaultDownloadResponse, workoutId)
		})
		
		return ModelVideoListing.downloadRequest!
	}
	
	class func downloadVideoForWorkout(sVideoZipUrl: String, sFileName: String, workoutId: String = "" , currentProgress:((_ progress: Progress, _ sVideoZipUrl: String, _ workoutId: String) -> ())?, response:((_ response: DefaultDownloadResponse, _ workoutId: String) -> ())?) -> DownloadRequest{
		
//		ModelVideoListing.downloadRequestWorkout?.cancel()
		
		ModelVideoListing.downloadRequestWorkout = download(
			sVideoZipUrl,
			method: .get,
			parameters: nil,
			encoding: JSONEncoding.default,
			headers: nil,
			to: { temporaryURL, response in
				
				//fileManager
				let fileManager = FileManager.default
				let directoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
				
				//create folder 'Video' if does not exist
				let folderPathComponent = directoryURL.appendingPathComponent("Videos")
				if !fileManager.fileExists(atPath: folderPathComponent.path) {
					do {
						try fileManager.createDirectory(atPath: folderPathComponent.path, withIntermediateDirectories: false, attributes: nil)
					} catch let error as NSError {
						print("error----\(error.localizedDescription)");
					}
				}
				
				let fileUrl = DoviesGlobalUtility.pathForVideoFile(videoFileName: sFileName)
				return (fileUrl, [])
				
		}).downloadProgress(closure: { (progress) in
			//progress closure
			print("progress----\(progress.fractionCompleted)")
			currentProgress?(progress, sVideoZipUrl, workoutId)
			
		}).response(completionHandler: { (DefaultDownloadResponse) in
			//result closure
			print(DefaultDownloadResponse)
			response?(DefaultDownloadResponse, workoutId)
		})
		
		return ModelVideoListing.downloadRequestWorkout!
	}
}
