//
//  ModelSubscriptionHistory.swift
//  Dovies
//
//  Created by Neel Shah on 24/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class ModelSubscriptionHistory: NSObject, NSCoding{
	
	var amountPaid : String!
    var packagetext : String!
    var endsIn : String!
	var expDate : String!
	var packageName : String!
	var startDate : String!
	
	
	/**
	* Instantiate the instance using the passed dictionary values to set the properties values
	*/
	init(fromDictionary dictionary: [String:Any]){
		amountPaid = dictionary["amount_paid"] as? String ?? ""
        packagetext = dictionary["package_text"] as? String ?? ""
        endsIn = dictionary["ends_in"] as? String ?? ""

		expDate = dictionary["exp_date"] as? String ?? ""
        
		packageName = dictionary["package_name"] as? String ?? ""
		startDate = dictionary["start_date"] as? String ?? ""
	}
	
	/**
	* Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	*/
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if amountPaid != nil{
			dictionary["amount_paid"] = amountPaid
		}
        if packagetext != nil{
            dictionary["package_text"] = packagetext
        }
        if endsIn != nil{
            dictionary["ends_in"] = endsIn
        }
       
		if expDate != nil{
			dictionary["exp_date"] = expDate
		}
		if packageName != nil{
			dictionary["package_name"] = packageName
		}
		if startDate != nil{
			dictionary["start_date"] = startDate
		}
		return dictionary
	}
	
	/**
	* NSCoding required initializer.
	* Fills the data from the passed decoder
	*/
	@objc required init(coder aDecoder: NSCoder)
	{
		amountPaid = aDecoder.decodeObject(forKey: "amount_paid") as? String
        packagetext = aDecoder.decodeObject(forKey: "package_text") as? String
        endsIn = aDecoder.decodeObject(forKey: "ends_in") as? String

		expDate = aDecoder.decodeObject(forKey: "exp_date") as? String
		packageName = aDecoder.decodeObject(forKey: "package_name") as? String
		startDate = aDecoder.decodeObject(forKey: "start_date") as? String
		
	}
	
	/**
	* NSCoding required method.
	* Encodes mode properties into the decoder
	*/
	@objc func encode(with aCoder: NSCoder)
	{
		if amountPaid != nil{
			aCoder.encode(amountPaid, forKey: "amount_paid")
		}
		if expDate != nil{
			aCoder.encode(expDate, forKey: "exp_date")
		}
		if packageName != nil{
			aCoder.encode(packageName, forKey: "package_name")
		}
		if startDate != nil{
			aCoder.encode(startDate, forKey: "start_date")
		}
	}
}
