//
//  WsCalls.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

//This call contains common API methods
class WsCalls: NSObject {
    class func processResponse(dict: [String: Any]?) -> (success: Bool, message: String?, data: Any?, strSuccess: String?) {
        var isSuccess = false
        var sSuccess: String?
        var theData: Any?
        var sMessage: String?

        if let dic = dict {
            if let dicSettings = dic[WsParam.settings] as? [String: Any] {
                if let success = dicSettings[WsParam.success] as? String {
                    sSuccess = success
                    if success == "1" {
                        isSuccess = true
                    }
                }
                if let message = dicSettings[WsParam.message] as? String {
                    sMessage = message
                }
            }
            theData = dic[WsParam.data]
        }
        return (isSuccess, sMessage, theData, sSuccess )
    }
    
    class func ProcessResponse(dict: [String: Any]?) -> (success: Bool,  workoutcount: String? ,message: String?, data: Any?, strSuccess: String?) {
        var isSuccess = false
        var sSuccess: String?
        var theData: Any?
        var sMessage: String?
        var sWorkoutcount: String?

        if let dic = dict {
            if let dicSettings = dic[WsParam.settings] as? [String: Any] {
                if let success = dicSettings[WsParam.success] as? String {
                    sSuccess = success
                    if success == "1" {
                        isSuccess = true
                    }
                }
                if let message = dicSettings[WsParam.message] as? String {
                    sMessage = message
                }
                if let workoutcout = dicSettings[WsParam.count] as? String {
                    sWorkoutcount = workoutcout
                }
            }
            theData = dic[WsParam.data]
        }
        return (isSuccess, sWorkoutcount , sMessage, theData, sSuccess )
    }
    
    
    class func processResponseWithPagination(dict: [String: Any]?) -> (success: Bool, message: String?, data: Any?, strSuccess: String?, nextPageStatus: Bool) {
        var isSuccess = false
        var sSuccess: String?
        var theData: Any?
        var sMessage: String?
        var hasNextPage = false
        if let dic = dict {
            if let dicSettings = dic[WsParam.settings] as? [String: Any] {
                if let success = dicSettings[WsParam.success] as? String {
                    sSuccess = success
                    if success == "1" {
                        isSuccess = true
                    }
                }
                if let message = dicSettings[WsParam.message] as? String {
                    sMessage = message
                }
                if let nextPage = dicSettings[WsParam.nextPage] as? String, nextPage == "1" {
                    hasNextPage = true
                }else{
                    hasNextPage = false
                }
            }
            theData = dic[WsParam.data]
        }
        return (isSuccess, sMessage, theData, sSuccess,hasNextPage)
    }
    
    class func callInstagramAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ userData: [String: Any]?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        request(WsUrl.insgramAPI, method: .post,  parameters: dicParam)
            .responseJSON
            {
                response in
                switch response.result
                {
                case .success:
                    if let JSON = response.result.value
                    {
                        if let userDict = (JSON as AnyObject).object(forKey: "user") {
                            if let dicData = userDict as? [String: Any]{
                                onSuccess?(true, "", dicData)
                            }
                            //                                APP_DELEGATE.mDicParamSocialNetwork.removeAllObjects()
                            //                                APP_DELEGATE.mDicParamSocialNetwork.setObject(dicData["full_name"] as! String, forKey: SOCIAL_NAME as NSCopying)
                            //                                APP_DELEGATE.mDicParamSocialNetwork.setObject(dicData["id"] as! String, forKey: SOCIAL_ID as NSCopying)
                            //                                APP_DELEGATE.mDicParamSocialNetwork.setObject(dicData["profile_picture"] as! String!, forKey: SOCIAL_IMAGEURL as NSCopying)
                            //                                APP_DELEGATE.mDicParamSocialNetwork.setObject("instagram", forKey: SOCIAL_TYPE as NSCopying)
                            //                            if userDict.object(forKey: "username") != nil
                            //                            {
                            //                                    let username = dicData.object(forKey: "username") as! String
                            //                                    APP_DELEGATE.mDicParamSocialNetwork.setObject(username, forKey: SOCIAL_USERNAME as NSCopying)
                            //                            }
                            //                                self.performSegue(withIdentifier: "pushSignUpUserProfile", sender: nil)
                        }else{
                            onSuccess!(false, "", nil)
                        }
                    }
                case .failure(let error):
                    print(error)
                    onFailure?(error)
                }
        }
    }
}
