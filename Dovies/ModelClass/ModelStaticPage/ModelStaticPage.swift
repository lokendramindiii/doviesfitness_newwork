//
//  ModelStaticPage.swift
//  Dovies
//
//  Created by Neel Shah on 24/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class ModelStaticPage: NSObject, NSCoding{
	
	var staticPageUrl : String!
	
	/**
	* Instantiate the instance using the passed dictionary values to set the properties values
	*/
	init(fromDictionary dictionary: [String:Any]){
		staticPageUrl = dictionary["static_page_url"] as? String ?? ""
	}
	
	/**
	* Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	*/
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if staticPageUrl != nil{
			dictionary["static_page_url"] = staticPageUrl
		}
		return dictionary
	}
	
	/**
	* NSCoding required initializer.
	* Fills the data from the passed decoder
	*/
	@objc required init(coder aDecoder: NSCoder)
	{
		staticPageUrl = aDecoder.decodeObject(forKey: "static_page_url") as? String
		
	}
	
	/**
	* NSCoding required method.
	* Encodes mode properties into the decoder
	*/
	@objc func encode(with aCoder: NSCoder)
	{
		if staticPageUrl != nil{
			aCoder.encode(staticPageUrl, forKey: "static_page_url")
		}
	}
}
