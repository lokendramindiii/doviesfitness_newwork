//
//  NotificationDetailVC.swift
//  Dovies
//
//  Created by Mindiii on 1/21/19.
//  Copyright © 2019 Hidden Brains. All rights reserved.
//

import UIKit
import ParallaxHeader
import NVActivityIndicatorView

class NotificationDetailVC: BaseViewController ,UITableViewDelegate, UITableViewDataSource  {
    
 //   var activityIndicatorNew : NVActivityIndicatorView!

    let tableSquareCellHeight = ScreenSize.width
    let tableLandScapeCellHeight = 180*CGRect.widthRatio
    let tablePotraitCellHeight = 400*CGRect.widthRatio
    
    var arrobj = [NotificationModel]()
    var arrayCommentList = [CommentlistModel]()
    var arrNotificationsDetail = [NotificationModel]()

    var Index_Path: Int = 0
    var iscommentcount  = true

    weak var headerImageView: UIView?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let frame = CGRect(x: self.view.center.x-22, y: self.view.center.y-22, width: 45, height: 45)
//        activityIndicatorNew = NVActivityIndicatorView(frame: frame)
//        activityIndicatorNew.center = self.view.center
//        activityIndicatorNew.type = . circleStrokeSpin // add your type
//        activityIndicatorNew.color = UIColor.lightGray // add your color
//        APP_DELEGATE.window?.addSubview(activityIndicatorNew)
//        activityIndicatorNew.startAnimating()
//
        self.tableView.estimatedRowHeight = 3500
        self.tableView.rowHeight = UITableViewAutomaticDimension

        self.GetNotificationRead()
        self.callNotificationDetailAPI()
        
         setupParallaxHeader()
        automaticallyAdjustsScrollViewInsets = false

    }
    
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }
    
    override func viewDidDisappear(_ animated: Bool) {
       // activityIndicatorNew.stopAnimating()
    }
  
    private func setupParallaxHeader() {
        
        let imageView = UIImageView()
        if arrobj[0].notificationImage.isEmpty
        {
            imageView.isHidden = true
        }
        else
        {
           // imageView.sd_setImage(with: URL(string: arrobj[0].notificationImage), placeholderImage: AppInfo.placeholderImage)
            
            imageView.sd_setImage(with: URL(string: arrobj[0].notificationImage), placeholderImage: nil)

        }
        
        imageView.contentMode = .scaleToFill
        
        headerImageView = imageView
        tableView.parallaxHeader.view = imageView
        if arrobj[0].notificationImage.isEmpty
        {
            tableView.parallaxHeader.height = 80
            tableView.parallaxHeader.minimumHeight = 0
        }
        else
        {
            if arrobj[0].notificationType == "Portrait"
            {
                tableView.parallaxHeader.height = tablePotraitCellHeight
                tableView.parallaxHeader.minimumHeight = 0
            }
            else if arrobj[0].notificationType == "Square"
            {
                tableView.parallaxHeader.height = tableSquareCellHeight
                tableView.parallaxHeader.minimumHeight = 0
            }
            else
            {
                tableView.parallaxHeader.height = tableSquareCellHeight
                tableView.parallaxHeader.minimumHeight = 0
            }
        }
        tableView.parallaxHeader.mode = .centerFill
        tableView.parallaxHeader.parallaxHeaderDidScrollHandler = { parallaxHeader in
        }
    }
    
    
    //MARK: table view data source/delegate
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        if arrobj[0].notificationCode.uppercased() == "WELCOME"
        {
            return 1
        }
        else
        {
            if 5 < arrayCommentList.count + 2
            {
                Index_Path = 4
                return 5
            }
            else
            {
                Index_Path = arrayCommentList.count + 1
                return arrayCommentList.count + 2
            }
        }
        return  0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationDetail_Cell") as! NotificationDetail_Cell
        
        if indexPath.row == 0 {
            
            if arrobj[0].notificationCode.uppercased() == "WELCOME"
            {
                cell.lblLikeCount.isHidden = true
                cell.lblCommentCount.isHidden = true
                cell.btnLike.isHidden = true
                cell.btnComments.isHidden = true
            }
            
            cell.lblTitle.text = arrobj[0].notificationTitle.uppercased()
            cell.lblTitle.makeSpacingWithLeftAlignment(lineSpace: 9.0, font: UIFont.futuraBold(size: 24), color: UIColor.colorWithRGB(r: 36, g: 36, b: 36, alpha: 1.0))
            
            if arrobj[0].notificationBtnTitle.isEmpty
            {
                cell.viewBgTitle.isHidden = true
            }
            else
            {
                cell.viewBgTitle.isHidden = false
            }
        cell.btnTitle.setTitle(arrobj[0].notificationBtnTitle.uppercased(), for: .normal)
              cell.btnTitle.addTarget(self, action:#selector(actionBtnTitle(_:)) , for: .touchUpInside)
            
            if arrobj[0].notificationLikeCount == "0" || arrobj[0].notificationLikeCount == "1"
            {
                cell.lblLikeCount.text = arrobj[0].notificationLikeCount + " like"
            }
            else
            {
                cell.lblLikeCount.text = arrobj[0].notificationLikeCount + " likes"
            }
            
            if arrayCommentList.count == 0 || arrayCommentList.count == 1
            {
                cell.lblCommentCount.text = "\(arrayCommentList.count) comment"
                iscommentcount = false
            }
            else
            {
                
                if iscommentcount == true
                {
                    iscommentcount = false
                    cell.lblCommentCount.text = arrobj[0].notificationCommentCount + " comments"
                }
                else
                {
                cell.lblCommentCount.text = "\(arrayCommentList.count) comments"
                }
            }
            
            if arrobj[0].notificationMyLike == "1"
            {
                cell.btnLike.isSelected = true
            }
            else
            {
                cell.btnLike.isSelected = false
            }
            
            cell.btnLike.addTarget(self, action:#selector(actionLike(_:)) , for: .touchUpInside)
            
            cell.commentTapped = { () in
                let comments = self.storyboard?.instantiateViewController(withIdentifier: "CommentNotificationListVC")as! CommentNotificationListVC
                comments.notificationConnectionid = self.arrobj[0].notificationConnectionId
                comments.arrayComment_List = self.arrayCommentList
                comments.notificationId = self.arrobj[0].notificationId
                
                comments.updateCustomerComment = {[weak self] (UpdateCommentList) in
                    
                    guard let weakself = self else{
                        return
                    }
                    weakself.arrayCommentList = UpdateCommentList
                    DispatchQueue.main.async {
                        weakself.tableView.reloadData()
                    }
                }
            self.navigationController?.pushViewController(comments, animated: true)
            }
            
            let HtmlconvertDescription = arrobj[0].notificationMessage
            
            cell.txt_View.attributedText = "<!DOCTYPE html> <style>html * {font-size: 11.0pt !important;color: #7a7a7a; font-family: TamilSangamMN !important;line-height:19.0pt;} p strong, p b{color: #242424 !important;}h1,h2,h3,h4,h5,h6{color: #242424 !important;}</style><html><body>\(HtmlconvertDescription ?? "")</body></html>".convertHtml()

            let contentSizeComment = cell.txt_View.sizeThatFits(cell.txt_View.bounds.size)
            
            cell.txt_contentHeightCons.constant = contentSizeComment.height

            return cell
        }
//MARK: Write comment in tableview cell ---------
        else if indexPath.row == Index_Path
        {
            let write_Cell = tableView.dequeueReusableCell(withIdentifier: "WriteCommentCell") as! WriteCommentCell
            
            if arrayCommentList.count > 3
            {
                write_Cell.ViewReadallComm.isHidden = false
            }
            else{
                write_Cell.ViewReadallComm.isHidden = true
            }
            
            write_Cell.sendComment = {(comment)in
                self.callCustomNotificationPostCommentAPI(likeUnlike: "", CommentText: comment)
            }
            return write_Cell
        }
//MARK : comment list in tableview cell ---------

        else
        {
            let cellComment = tableView.dequeueReusableCell(withIdentifier: "CommentsCell") as! CommentsCell
            
            let obj = arrayCommentList[indexPath.row - 1]
            
            cellComment.imgViewProfile.sd_setImage(with: URL(string: obj.profileImage), placeholderImage: AppInfo.userPlaceHolder)
            
            cellComment.lblName.text = obj.name
            cellComment.lblName.makeSpacingWithLeftAlignment(lineSpace: 4.0, font: UIFont.tamilBold(size: 14), color: UIColor.colorWithRGB(r: 22, g: 22, b: 2, alpha: 1.0))

            cellComment.lblTime.text = obj.notificationAgo
            
            cellComment.btnMore.tag = indexPath.row - 1
            
            cellComment.lblComment.text = obj.comment
            cellComment.lblComment.makeSpacingWithLeftAlignment(lineSpace: 3.0, font: UIFont.tamilRegular(size: 14), color: UIColor.colorWithRGB(r: 122, g: 122, b: 122, alpha: 1.0))

            cellComment.moreButtonAction = { (cellIndex) in
                if let indexOfCell = cellIndex{
                    
                     self.showActionSheet(at : indexOfCell)
                }
            }
            return cellComment
        }
        
        return cell
    }
  

    
    
    @objc  func actionLike (_ sender:UIButton)
    {
        let indexPath = IndexPath(row: 0, section: 0)
        let cell = tableView.cellForRow(at: indexPath) as! NotificationDetail_Cell
        
        if arrobj[0].notificationMyLike == "0"
        {
            
            arrobj[0].notificationMyLike = "1"
            sender.isSelected = true
            
            var  likecount : Int
                = Int(arrobj[0].notificationLikeCount) ?? 0
            likecount = likecount + 1
            arrobj[0].notificationLikeCount = "\(likecount)"
            
            if arrobj[0].notificationLikeCount == "0" || arrobj[0].notificationLikeCount == "1"
            {
                cell.lblLikeCount.text = arrobj[0].notificationLikeCount + " like"
            }
            else
            {
                cell.lblLikeCount.text = arrobj[0].notificationLikeCount + " likes"
            }
            
            callCustomNotificationLikeCommentAPI(with: sender, likeUnlike: "YES", CommentText: "")
        }
        else{
            arrobj[0].notificationMyLike = "0"
            var  likecount : Int
                = Int(arrobj[0].notificationLikeCount) ?? 0
            likecount = likecount - 1
            sender.isSelected = false

            arrobj[0].notificationLikeCount = "\(likecount)"
            
            if arrobj[0].notificationLikeCount == "0" || arrobj[0].notificationLikeCount == "1"
            {
                cell.lblLikeCount.text = arrobj[0].notificationLikeCount + " like"
            }
            else
            {
                cell.lblLikeCount.text = arrobj[0].notificationLikeCount + " likes"
            }
            callCustomNotificationLikeCommentAPI(with: sender, likeUnlike: "NO", CommentText:
                "")
        }
    }
    
    
    @IBAction func ReadAllComments(_ sender : UIButton){
      
        let comments = self.storyboard?.instantiateViewController(withIdentifier: "CommentNotificationListVC")as! CommentNotificationListVC
        comments.notificationConnectionid = arrobj[0].notificationConnectionId
        comments.arrayComment_List = self.arrayCommentList
        comments.notificationId = arrobj[0].notificationId

        comments.updateCustomerComment = { [weak self](UpdateCommentList) in
            
            guard let weakself = self else{return }
            weakself.arrayCommentList = UpdateCommentList
            weakself.tableView.reloadData()
            
        }
        self.navigationController?.pushViewController(comments, animated: true)
        
    }
    
    // MARK:  Call Report a Problem and comment delete api
    func showActionSheet(at indexPath : IndexPath){
        let arrData: [alertActionData]!
        
        print(indexPath.row)
        
        if let cUser = DoviesGlobalUtility.currentUser, arrayCommentList[indexPath.row].customerId == cUser.customerId{
            arrData = [
                alertActionData(title: "Delete", imageName: "btn_del_s21a", highlighedImageName : "btn_del_s21a ")
            ]
        }else{
            arrData = [
                alertActionData(title: "Report Comment", imageName: "abouts_icon_42", highlighedImageName : "abouts_icon_42_h")
            ]
        }
        DoviesGlobalUtility.showDefaultActionSheet(on:self,actionData: arrData,cancelTitle: "Cancel") { (clickedIndex:Int, isCancel:Bool) in
            if clickedIndex == 0{
                if let cUser = DoviesGlobalUtility.currentUser, self.arrayCommentList[indexPath.row].customerId == cUser.customerId{
                    self.callDeleteComment(at: indexPath)
                }
                else{
                    self.callReportComment(at: indexPath)
                }
            }
        }
    }
    
    @objc  func actionBtnTitle (_ sender:UIButton)
    {
        if arrobj[0].notificationModuleName == "other" || arrobj[0].notificationModuleName == ""
        {
            GlobalUtility.openUrlFromApp(url: arrobj[0].notificationBtnUrl)
        }
        else
        {
        self.redirectBasedOnModuleName(arrobj[0].notificationModuleName, arrobj[0].notificationModuleId)
        }
    }
    
    @IBAction func Action_Header(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    // MARK:  Call Delete comment api and  Report a problem api

    
    /**
     This method is used to call the delete comment api to delete the comment and after successful deletion, the comment count will update in UI.
     - Parameter indexPath: is used to get commentId for particular index to delete the comment
     */
    
    func callDeleteComment(at indexPath : IndexPath){
        guard let cUser = DoviesGlobalUtility.currentUser, arrayCommentList.count > indexPath.row else {return}
        var params = [String: Any]()
        params[WsParam.authCustomerId] = cUser.customerAuthToken
        params[WsParam.comment_Id] = arrayCommentList[indexPath.row].customNotificationLikeCommentId
        self.arrayCommentList.remove(at: indexPath.row)
        self.tableView.reloadData()

        DoviesWSCalls.callDeleteNotificationCommentAPI(dicParam: params, onSuccess: { (success, message) in
            if success{
            }
            
        }) { (error) in
        }
    }
    
    
    /**
     This method is used to call the delete comment api to report the comment and after successful reporting.
     - Parameter indexPath: is used to get commentId for particular index to report the comment
     */
    func callReportComment(at indexPath : IndexPath){
        guard let cUser = DoviesGlobalUtility.currentUser, arrayCommentList.count > indexPath.row  else {return}
        var params = [String: Any]()
        params[WsParam.authCustomerId] = cUser.customerAuthToken
        params[WsParam.comment_Id] = arrayCommentList[indexPath.row].customNotificationLikeCommentId
        params[WsParam.reportText] = ""
        params[WsParam.customNotificationId] = arrayCommentList[indexPath.row].customNotificationId
        
        DoviesWSCalls.callReportNotificationCommentAPI(dicParam: params, onSuccess: { (success, message) in
            GlobalUtility.showToastMessage(msg: "Done")
        }) { (error) in
        }
    }
    
    
    // MARK:  Call customer like comment api
    func callCustomNotificationLikeCommentAPI(with  favButton : UIButton , likeUnlike:String , CommentText:String){
      
        guard let cUser = DoviesGlobalUtility.currentUser else{return}
        var parameters = [String : Any]()
        parameters[WsParam.authCustomerId] =  cUser.customerAuthToken
        parameters[WsParam.connectionId] = arrobj[0].notificationConnectionId
        parameters[WsParam.like] = likeUnlike
        parameters[WsParam.comment] = CommentText
        DoviesWSCalls.callCustomNotificationLikeCommentAPI(dicParam: parameters, onSuccess: { (success, message, response) in
            
            if success{
                
            }
        })
        { (error) in
        }
    }
    
    
    
    // MARK:  Call Post comment api
    func callCustomNotificationPostCommentAPI(  likeUnlike:String , CommentText:String){
        
        guard let cUser = DoviesGlobalUtility.currentUser else{return}
        var parameters = [String : Any]()
        parameters[WsParam.authCustomerId] =  cUser.customerAuthToken
        parameters[WsParam.connectionId] = arrobj[0].notificationConnectionId
        parameters[WsParam.like] = likeUnlike
        parameters[WsParam.comment] = CommentText
        DoviesWSCalls.callCustomNotificationPostCommentAPI(dicParam: parameters, onSuccess: { (success, message ,commentObj) in
            
            if let comment = commentObj, success {
                self.arrayCommentList.insert(comment, at: 0)
                self.tableView.reloadData()
            }
        })
        { (error) in
        }
    }
    
    // MARK: Notification read unread status api
    func GetNotificationRead(){
        
        var dic = [String: Any]()
        dic[WsParam.notificationId] = arrobj[0].notificationId
        DoviesWSCalls.callGetNotificationStatusAPI(dicParam: dic, onSuccess: {[weak self] (isSuccess, message, response) in
            guard let weakSelf = self else{return}
            if isSuccess, let dic = response {
                
            }
        }) { (error) in
        }
    }
    
    
    func redirectBasedOnModuleName(_ moduleName : String , _ moduleId : String){
        
        guard let vc = UIApplication.shared.keyWindow?.getVisibleViewController(),let centerVC = vc.sideMenuController?.centerViewController as? TabBarViewController else{ return}
        
        if let finalVC = (centerVC.selectedViewController as? UINavigationController)?.viewControllers.last{
            switch moduleName {
            case ModuleName.dietPlan:
                let respectiveVC = Storyboard.DietPlan.instantiate(DietPlanDetailsVC.self) as DietPlanDetailsVC
                respectiveVC.planId = moduleId
                finalVC.navigationController?.pushViewController(respectiveVC, animated: true)
                
            case ModuleName.exercise:
                let wVC = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "WorkoutDetailsVC") as! WorkoutDetailsVC
                wVC.exerciseId = moduleId
                wVC.screenType = .workOutDetailList
                finalVC.navigationController?.pushViewController(wVC, animated: true)
                
            case ModuleName.newsFeed:
                let newsFeedDetailsVC = Storyboard.NewsFeed.storyboard().instantiateViewController(withIdentifier: "NewsFeedMediaDetailsVC") as! NewsFeedMediaDetailsVC
                newsFeedDetailsVC.newsFeedId = moduleId
                //   newsFeedDetailsVC.isFromShare = true
                finalVC.navigationController?.pushViewController(newsFeedDetailsVC, animated: true)
                
            case ModuleName.program:
                let programDetailsVC = Storyboard.Programs.instantiate(ProgramDetail_VC.self)
                programDetailsVC.programId = moduleId
                //  programDetailsVC.isFromShared = true
                finalVC.navigationController?.pushViewController(programDetailsVC, animated: true)
                
            case ModuleName.workout:
                let featuredWorkOutDetailViewControlle = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "FeaturedWorkOutDetailViewController") as! FeaturedWorkOutDetailViewController
                featuredWorkOutDetailViewControlle.workoutId = moduleId
                featuredWorkOutDetailViewControlle.workoutScreenType = .sharedWorkout
                featuredWorkOutDetailViewControlle.navigationItem.title = ""
                finalVC.navigationController?.pushViewController(featuredWorkOutDetailViewControlle, animated: true)
                
            default:
                break
            }
        }
    }
}


extension String{
    func convertHtml() -> NSAttributedString{
        
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do{
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding: String.Encoding.utf8.rawValue ],documentAttributes: nil)
        }catch{
            return NSAttributedString()
        }
    }
}


extension NotificationDetailVC
{
    
    func callNotificationDetailAPI(){
        
        guard let user = DoviesGlobalUtility.currentUser else{return}
        
        var params = [String : Any]()
        params[WsParam.authCustomerId] = user.customerAuthToken
        params[WsParam.pageIndex] = "1"
        params[WsParam.notificaitonId] = arrobj[0].notificationId
        DoviesWSCalls.callGetNotificationsAPI(dicParam: params, onSuccess: { [weak self](success, notifications, message,hasNextPage) in
            guard let weakSelf = self else {return}
            
            if success, let notificationList = notifications{
                if  let notificationList = notifications
                {
                    
                  weakSelf.arrNotificationsDetail = notificationList
                  weakSelf.arrayCommentList = weakSelf.arrNotificationsDetail[0].commentList
                    
                }
            }
            weakSelf.tableView.reloadData()
            //self!.activityIndicatorNew.stopAnimating()

            
        }) { [weak self](error) in
            guard let weakSelf = self else {return}
            weakSelf.tableView.reloadData()
          //  self!.activityIndicatorNew.stopAnimating()

        }
    }
}




