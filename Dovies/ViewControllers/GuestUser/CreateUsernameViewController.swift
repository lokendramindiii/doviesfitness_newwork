//
//  CreateUsernameViewController.swift
//  Dovies
//
//  Created by Mindiii on 9/15/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

let ColorRed = UIColor(r: 228, g: 0, b: 0, alpha: 1)
let ColorGreen = UIColor(r: 0, g: 196, b: 113, alpha: 1)

class CreateUsernameViewController: UIViewController {
    let validator = Validator()

    var signupParams : [String:Any]?
    
    
    var strUserNameText = ""

    @IBOutlet weak var txtUserName: UITextField!
    @IBOutlet weak var lblErrorUserame: UILabel!
    @IBOutlet weak var btnNext: UIButton!

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    fileprivate var returnKeyHandler : IQKeyboardReturnKeyHandler!

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func setupUI() {
        self.lblMessage.makeSpacingWithCenterAlignment(lineSpace: 9.0, font: UIFont.tamilRegular(size: 14), color: UIColor(r: 27, g: 27, b: 27, alpha: 1))        
        setFormValidators()
        returnKeyHandler = IQKeyboardReturnKeyHandler(controller: self)
        returnKeyHandler.lastTextFieldReturnKeyType = UIReturnKeyType.done
        
        
        self.lblErrorUserame.text = AlertMessages.emptyUserName
        self.lblErrorUserame.isHidden = true
        self.layout()
    }
    
    func setFormValidators(){
        validator.styleTransformers(success:{ (validationRule) -> Void in
    
            if let textField = validationRule.field as? UITextField {
                self.hideErroeLabel()
            }
            
        }, error:{ (validationError) -> Void in
    
            if let textField = validationError.field as? UITextField {
                print(validationError.errorMessage)
                self.showErroeLabel()
                
            }
    })
    
        validator.registerField(txtUserName, errorLabel: nil , rules: [RequiredRule(message: AlertMessages.emptyUserName)])
        
    }
    
    func hideErroeLabel()  {
        self.lblErrorUserame.isHidden = true
        self.layout()
    }
    
    func showErroeLabel()  {
        self.lblErrorUserame.isHidden = false
        self.layout()
    }
    
    func layout()  {
        DispatchQueue.main.async {
            self.view.layoutIfNeeded()
            self.view.updateConstraintsIfNeeded()
        }
        
    }
    
    @IBAction func actionNext(_ sender: Any) {
            validator.validate(self)
      
    }
}


extension CreateUsernameViewController : ValidationDelegate{
    
    func validationSuccessful() {
        signupParams![WsParam.userName] = self.txtUserName.text ?? ""
        strUserNameText =  self.txtUserName.text ?? ""
        
        print(self.txtUserName.text ?? "")
        print(strUserNameText)
        
        let num = Int(strUserNameText);
        if strUserNameText == "" || strUserNameText.count == 1 || strUserNameText.count == 2
        {
        }
        else if num != nil
        {
        }
        else
        {
            self.navigateToPhotoScreen(params: signupParams!)
        }
        //checkUserAvailibility(name: txtUserName.text!)
    }
    
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        
    }
    
    func navigateToPhotoScreen(params: [String: Any])  {
        let vc = Storyboard.Login.storyboard().instantiateViewController(withIdentifier: "AddPhotoViewController")as! AddPhotoViewController
        vc.signupParams = params;
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension CreateUsernameViewController: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        //dtTextField.hideError()
        //hideErroeLabel()
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        validator.validateField(textField){ error in
            if error == nil {
                // Field validation was successful
            } else {
                // Validation error occurred
            }
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        validator.validateField(textField){ error in
            if error == nil {
                // Field validation was successful
            } else {
                // Validation error occurred
            }
        }
    }

    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
                if textField == txtUserName{
                    
                    if let text = textField.text, let swtRange = Range(range, in: text) {
                        let fullString = text.replacingCharacters(in: swtRange, with: string)
                        if fullString.count < 3{
                           // self.lblErrorUserame.textColor = ColorRed
                          //  self.lblErrorUserame.text = AlertMessages.emptyUserName
                        }
                        else
                        {
                            if fullString.isAlphanumeric == true
                            {
                                let num = Int(fullString);
                                if num != nil {
                                    print("Valid Integer")
                                    self.hideErroeLabel()
                                }
                                else {
                                    self.checkUserAvailibility(name: fullString)
                                }
                            }
                        }
                    }
                    if string.isEmpty {
                        return true
                    }
                    let isAllowed = isCharacterAllowed(char: string)
                    return isAllowed
                }else{
                    return true
                }
        
    }
    
   
    func isCharacterAllowed(char: String) -> Bool {
        let regex = "[a-z0-9_.]"
        let allowedCharacter = NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: char)
        return allowedCharacter
    }
    
}

extension CreateUsernameViewController{
    func checkUserAvailibility(name:String){
        
        var dic = [String: Any]()
        dic[WsParam.userAvailable] = name
        DoviesWSCalls.callUsernameAvailabilityAPI(dicParam: dic, onSuccess: {[weak self]  (isSuccess, response, message) in
            guard let weakSelf = self else{return}
            if isSuccess, let userInfo = response{
                if (weakSelf.txtUserName.text?.count)! > 0{
                    let isAvailable = userInfo["is_available"] as? String ?? ""
                    weakSelf.btnNext.isEnabled = (isAvailable == "1") ? true : false
                    weakSelf.lblErrorUserame.text = userInfo["message"] as? String ?? ""
                    weakSelf.lblErrorUserame.textColor = (isAvailable == "1") ? ColorGreen : ColorRed
                    weakSelf.showErroeLabel()
                }
                else{
                    weakSelf.lblErrorUserame.textColor = ColorRed
                    weakSelf.lblErrorUserame.text = AlertMessages.emptyUserName
                    weakSelf.showErroeLabel()

                }
            }
        }) { (error) in
            
        }
        
    }
}

extension String {
    var isAlphanumeric: Bool {
        return !isEmpty && range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }
}

