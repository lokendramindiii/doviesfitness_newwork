//
//  EquipmentListCell.swift
//  Dovies
//
//  Created by hb on 23/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class EquipmentListCell: UITableViewCell {

    @IBOutlet var equipList: UILabel!
    @IBOutlet var btnSelection: UIButton!
    
    var model:String?
    var indexPath : IndexPath!
    var obj : FilterDataModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(_ obj:FilterDataModel?,at indexPath : IndexPath){
        self.obj = obj
        equipList.text = obj?.gmDisplayName.capitalizingFirstLetter()
        self.indexPath = indexPath
    }
    
}
