//
//  HBImagePicker.h
//  HBImagePicker
//
//  Created by Venky on 3/12/15.
//  Copyright (c) 2015 Hidden Brains. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <UIKit/UIKit.h>

typedef void (^ImagePickerCompletionHandler)(BOOL success, NSMutableDictionary *mediaDict);
 enum mediaType {
    MediaTypeImage,
    MediaTypeVideo
};
@interface HBImagePicker : NSObject

+ (HBImagePicker*) sharedInstance;

// This method can call to Handle entire UIImagePickerController fuctionality with media types images and movies(videos). Developer can simply give media type, action sheet title , all button titles and can fetch the media from mediaDict dictionary. If he use media type image, Key name should be 'image' and if he use media type video, Key name should be 'video'.

//- (void)openImagePickerWithMediaType:(enum mediaType)media actionSheetTitle:(NSString *)title cancelButtonTitle:(NSString *)cancelTitle cameraButtonTitle:(NSString *)cameraTitle  galleryButtonTitle:(NSString *)galleryTitle withCompletion:(ImagePickerCompletionHandler)completionHandler;

- (void)openImagePickerWithMediaType:(enum mediaType)media actionSheetTitle:(NSString *)title cancelButtonTitle:(NSString *)cancelTitle cameraButtonTitle:(NSString *)cameraTitle  galleryButtonTitle:(NSString *)galleryTitle canEdit:(BOOL)canEdit withCompletion:(ImagePickerCompletionHandler)completionHandler;


- (void)openVideoImagePickerWithactionSheetTitle:(NSString *)title cancelButtonTitle:(NSString *)cancelTitle captureImageTitle:(NSString *)captureImageTitle  pickImageTitle:(NSString *)pickImageTitle captureVideoTitle:(NSString *)captureVideoTitle  pickVideoTitle:(NSString *)pickVideoTitle canEdit:(BOOL)canEdit withCompletion:(ImagePickerCompletionHandler)completionHandler;

- (void)openCameraEdit:(BOOL)canEdit completion:(ImagePickerCompletionHandler)completionHandler;

- (void)openPhotoVideo:(BOOL)canEdit completion:(ImagePickerCompletionHandler)completionHandler;

-(UIImage *)reduceImageSize:(UIImage *)img squareImgWidth:(float)imgWidth;
-(UIImage *)reduceImageSize:(UIImage *)img imgWidth:(float)imgWidth;
-(UIImage *)compressImage:(UIImage *)img;

-(UIImage *)rotateImage:(UIImage*)image;



@end
