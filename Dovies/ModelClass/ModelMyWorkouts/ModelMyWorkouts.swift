//
//  ModelMyWorkouts.swift
//  Dovies
//
//  Created by Neel Shah on 23/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class ModelMyWorkouts: NSObject, NSCoding{
	
	var workoutCategory : String!
	var workoutId : String!
	var workoutImage : String!
	var workoutName : String!
	var workoutShareUrl : String!

	var workoutTime : String!
	
	
	/**
	* Instantiate the instance using the passed dictionary values to set the properties values
	*/
	init(fromDictionary dictionary: [String:Any]){
		workoutCategory = dictionary["workout_category"] as? String ?? ""
		workoutId = dictionary["workout_id"] as? String ?? ""
		workoutImage = dictionary["workout_image"] as? String ?? ""
		workoutName = dictionary["workout_name"] as? String ?? ""
		workoutShareUrl = dictionary["workout_share_url"] as? String ?? ""
		workoutTime = dictionary["workout_time"] as? String ?? ""
	}
	
	/**
	* Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	*/
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if workoutCategory != nil{
			dictionary["workout_category"] = workoutCategory
		}
		if workoutId != nil{
			dictionary["workout_id"] = workoutId
		}
		if workoutImage != nil{
			dictionary["workout_image"] = workoutImage
		}
		if workoutName != nil{
			dictionary["workout_name"] = workoutName
		}
		if workoutShareUrl != nil{
			dictionary["workout_share_url"] = workoutShareUrl
		}
		if workoutTime != nil{
			dictionary["workout_time"] = workoutTime
		}
		return dictionary
	}
	
	/**
	* NSCoding required initializer.
	* Fills the data from the passed decoder
	*/
	@objc required init(coder aDecoder: NSCoder)
	{
		workoutCategory = aDecoder.decodeObject(forKey: "workout_category") as? String
		workoutId = aDecoder.decodeObject(forKey: "workout_id") as? String
		workoutImage = aDecoder.decodeObject(forKey: "workout_image") as? String
		workoutName = aDecoder.decodeObject(forKey: "workout_name") as? String
		workoutShareUrl = aDecoder.decodeObject(forKey: "workout_share_url") as? String
		workoutTime = aDecoder.decodeObject(forKey: "workout_time") as? String
	}
	
	/**
	* NSCoding required method.
	* Encodes mode properties into the decoder
	*/
	@objc func encode(with aCoder: NSCoder)
	{
		if workoutCategory != nil{
			aCoder.encode(workoutCategory, forKey: "workout_category")
		}
		if workoutId != nil{
			aCoder.encode(workoutId, forKey: "workout_id")
		}
		if workoutImage != nil{
			aCoder.encode(workoutImage, forKey: "workout_image")
		}
		if workoutName != nil{
			aCoder.encode(workoutName, forKey: "workout_name")
		}
		if workoutShareUrl != nil{
			aCoder.encode(workoutShareUrl, forKey: "workout_share_url")
		}
		if workoutTime != nil{
			aCoder.encode(workoutTime, forKey: "workout_time")
		}
	}
}
