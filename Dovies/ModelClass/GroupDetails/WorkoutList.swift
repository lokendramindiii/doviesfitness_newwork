//
//	WorkoutList.swift
//
//	Create by Praveen on 13/6/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class WorkoutList : NSObject, NSCoding{

	var cfCustomerFavoritesId : String!
	var levelName : String!
	var workoutAccessLevel : String!
	var workoutCategory : String!
	var workoutCount : String!
	var workoutExerciseCount : String!
	var workoutFavStatus : String!
	var workoutGroupDescription : String!
	var workoutGroupId1 : String!
	var workoutGroupName : String!
	var workoutId : String!
	var workoutImage : String!
	var workoutLevel : String!
	var workoutList : String!
	var workoutName : String!
	var workoutShareUrl : String!
	var workoutTime : String!
	var workoutTotalWorkTime : String!
    var isNew : Bool!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		cfCustomerFavoritesId = dictionary["cf_customer_favorites_id"] as? String ?? ""
		levelName = dictionary["level_name"] as? String ?? ""
        
        if let new = dictionary["is_new"] as? String , new == "1"{
            isNew = true
        }else if  let newStatus = dictionary["is_new"] as? Bool{
            isNew = newStatus
        }else{
            isNew = false
        }
        
        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        {
            workoutAccessLevel = "OPEN"
        }
        else
        {
            workoutAccessLevel = dictionary["workout_access_level"] as? String ?? ""
        }
        
		workoutCategory = dictionary["workout_category"] as? String ?? ""
		workoutCount = dictionary["workout_count"] as? String ?? ""
		workoutExerciseCount = dictionary["workout_exercise_count"] as? String ?? ""
		workoutFavStatus = dictionary["workout_fav_status"] as? String ?? ""
		workoutGroupDescription = dictionary["workout_group_description"] as? String ?? ""
		workoutGroupId1 = dictionary["workout_group_id1"] as? String ?? ""
		workoutGroupName = dictionary["workout_group_name"] as? String ?? ""
		workoutId = dictionary["workout_id"] as? String ?? ""
		workoutImage = dictionary["workout_image"] as? String ?? ""
		workoutLevel = dictionary["workout_level"] as? String ?? ""
		workoutList = dictionary["workout_list"] as? String ?? ""
		workoutName = dictionary["workout_name"] as? String ?? ""
		workoutShareUrl = dictionary["workout_share_url"] as? String ?? ""
		workoutTime = dictionary["workout_time"] as? String ?? ""
		workoutTotalWorkTime = dictionary["workout_total_work_time"] as? String ?? ""
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if cfCustomerFavoritesId != nil{
			dictionary["cf_customer_favorites_id"] = cfCustomerFavoritesId
		}
		if levelName != nil{
			dictionary["level_name"] = levelName
		}
		if workoutAccessLevel != nil{
			dictionary["workout_access_level"] = workoutAccessLevel
		}
		if workoutCategory != nil{
			dictionary["workout_category"] = workoutCategory
		}
		if workoutCount != nil{
			dictionary["workout_count"] = workoutCount
		}
		if workoutExerciseCount != nil{
			dictionary["workout_exercise_count"] = workoutExerciseCount
		}
		if workoutFavStatus != nil{
			dictionary["workout_fav_status"] = workoutFavStatus
		}
		if workoutGroupDescription != nil{
			dictionary["workout_group_description"] = workoutGroupDescription
		}
		if workoutGroupId1 != nil{
			dictionary["workout_group_id1"] = workoutGroupId1
		}
		if workoutGroupName != nil{
			dictionary["workout_group_name"] = workoutGroupName
		}
		if workoutId != nil{
			dictionary["workout_id"] = workoutId
		}
		if workoutImage != nil{
			dictionary["workout_image"] = workoutImage
		}
		if workoutLevel != nil{
			dictionary["workout_level"] = workoutLevel
		}
		if workoutList != nil{
			dictionary["workout_list"] = workoutList
		}
		if workoutName != nil{
			dictionary["workout_name"] = workoutName
		}
		if workoutShareUrl != nil{
			dictionary["workout_share_url"] = workoutShareUrl
		}
		if workoutTime != nil{
			dictionary["workout_time"] = workoutTime
		}
		if workoutTotalWorkTime != nil{
			dictionary["workout_total_work_time"] = workoutTotalWorkTime
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         cfCustomerFavoritesId = aDecoder.decodeObject(forKey: "cf_customer_favorites_id") as? String
         levelName = aDecoder.decodeObject(forKey: "level_name") as? String
        
        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        {
            workoutAccessLevel = "OPEN"
        }
        else
        {
            workoutAccessLevel = aDecoder.decodeObject(forKey: "workout_access_level") as? String
        }
         workoutCategory = aDecoder.decodeObject(forKey: "workout_category") as? String
         workoutCount = aDecoder.decodeObject(forKey: "workout_count") as? String
         workoutExerciseCount = aDecoder.decodeObject(forKey: "workout_exercise_count") as? String
         workoutFavStatus = aDecoder.decodeObject(forKey: "workout_fav_status") as? String
         workoutGroupDescription = aDecoder.decodeObject(forKey: "workout_group_description") as? String
         workoutGroupId1 = aDecoder.decodeObject(forKey: "workout_group_id1") as? String
         workoutGroupName = aDecoder.decodeObject(forKey: "workout_group_name") as? String
         workoutId = aDecoder.decodeObject(forKey: "workout_id") as? String
         workoutImage = aDecoder.decodeObject(forKey: "workout_image") as? String
         workoutLevel = aDecoder.decodeObject(forKey: "workout_level") as? String
         workoutList = aDecoder.decodeObject(forKey: "workout_list") as? String
         workoutName = aDecoder.decodeObject(forKey: "workout_name") as? String
         workoutShareUrl = aDecoder.decodeObject(forKey: "workout_share_url") as? String
         workoutTime = aDecoder.decodeObject(forKey: "workout_time") as? String
         workoutTotalWorkTime = aDecoder.decodeObject(forKey: "workout_total_work_time") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if cfCustomerFavoritesId != nil{
			aCoder.encode(cfCustomerFavoritesId, forKey: "cf_customer_favorites_id")
		}
		if levelName != nil{
			aCoder.encode(levelName, forKey: "level_name")
		}
		if workoutAccessLevel != nil{
			aCoder.encode(workoutAccessLevel, forKey: "workout_access_level")
		}
		if workoutCategory != nil{
			aCoder.encode(workoutCategory, forKey: "workout_category")
		}
		if workoutCount != nil{
			aCoder.encode(workoutCount, forKey: "workout_count")
		}
		if workoutExerciseCount != nil{
			aCoder.encode(workoutExerciseCount, forKey: "workout_exercise_count")
		}
		if workoutFavStatus != nil{
			aCoder.encode(workoutFavStatus, forKey: "workout_fav_status")
		}
		if workoutGroupDescription != nil{
			aCoder.encode(workoutGroupDescription, forKey: "workout_group_description")
		}
		if workoutGroupId1 != nil{
			aCoder.encode(workoutGroupId1, forKey: "workout_group_id1")
		}
		if workoutGroupName != nil{
			aCoder.encode(workoutGroupName, forKey: "workout_group_name")
		}
		if workoutId != nil{
			aCoder.encode(workoutId, forKey: "workout_id")
		}
		if workoutImage != nil{
			aCoder.encode(workoutImage, forKey: "workout_image")
		}
		if workoutLevel != nil{
			aCoder.encode(workoutLevel, forKey: "workout_level")
		}
		if workoutList != nil{
			aCoder.encode(workoutList, forKey: "workout_list")
		}
		if workoutName != nil{
			aCoder.encode(workoutName, forKey: "workout_name")
		}
		if workoutShareUrl != nil{
			aCoder.encode(workoutShareUrl, forKey: "workout_share_url")
		}
		if workoutTime != nil{
			aCoder.encode(workoutTime, forKey: "workout_time")
		}
		if workoutTotalWorkTime != nil{
			aCoder.encode(workoutTotalWorkTime, forKey: "workout_total_work_time")
		}

	}

}
