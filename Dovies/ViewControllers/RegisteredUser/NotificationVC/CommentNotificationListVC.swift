//
//  CommentNotificationListVC.swift
//  Dovies
//
//  Created by Mindiii on 2/26/19.
//  Copyright © 2019 Hidden Brains. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
import IQKeyboardManagerSwift

class CommentNotificationListVC: BaseViewController ,UITableViewDelegate,UITableViewDataSource , GrowingTextViewDelegate{
    
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet var inputToolbar: UIView!
    @IBOutlet weak var growingTextView: GrowingTextView!
    @IBOutlet weak var textViewBottomConstraint : NSLayoutConstraint!
    @IBOutlet var btnCommentSend                : UIButton!

    var arrayComment_List = [CommentlistModel]()
    var arrNotificationsDetail = [NotificationModel]()

    var pullToRefreshCtrl:UIRefreshControl!

    var notificationConnectionid = ""
    var notificationId = ""

    var updateCustomerComment:((_ commentList:[CommentlistModel]) -> ())?

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Comments".uppercased()
        
        self.navigationItem.hidesBackButton  = true
        
        self.addNavigationBackButton()
 
        inputToolbar.backgroundColor = .white
        inputToolbar.layer.borderWidth = 0.6
        inputToolbar.layer.cornerRadius = 3
        inputToolbar.layer.borderColor =  UIColor(red: 239/255.0, green: 239/255.0, blue: 239/255.0, alpha: 1.0).cgColor
        
        self.textViewLayoutConfi()
     //   self.setBottomViewTopLine()
        growingTextView.layer.cornerRadius = 4.0
        // *** Listen to keyboard show / hide ***
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        
        // *** Hide keyboard when tapping outside ***
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandler))
        view.addGestureRecognizer(tapGesture)
        
        
        if arrayComment_List.count == 0
        {
        // DoviesGlobalUtility.setNoLabelData(tblView: self.tableView, text: "No comments found.")
        }
        tableView.reloadData()
        
        self.setPullToRefresh()

    }
    
    
    func setPullToRefresh(){
        
        pullToRefreshCtrl = UIRefreshControl()
        
        pullToRefreshCtrl.addTarget(self, action: #selector(self.pullToRefreshClick(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            tableView.refreshControl  = pullToRefreshCtrl
        } else {
            tableView.addSubview(pullToRefreshCtrl)
        }
    }
    
    @objc func pullToRefreshClick(sender:UIRefreshControl) {
        self.pullToRefreshCtrl?.endRefreshing()
        
        self.callNotificationDetailAPI()
        self.pullToRefreshCtrl?.endRefreshing()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
        IQKeyboardManager.shared.enable = false

    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isTranslucent = true
        IQKeyboardManager.shared.enable = true
        
    }
    
   override func addNavigationBackButton() {
        self.navigationItem.leftBarButtonItems = GlobalUtility.getNavigationButtonItems(target: self, selector: #selector(self.btnBackTapped(sender:)), imageName: "btn_top_back")
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.black
    }
    
    @objc override func btnBackTapped(sender: UIButton) {
        
        
         self.updateCustomerComment?(arrayComment_List)
        _ = self.navigationController?.popViewController(animated: true)
    }
    

 
    //MARK: Instance methods
    
    @objc private func keyboardWillChangeFrame(_ notification: Notification) {
        if let endFrame = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue, let  height = self.navigationController?.navigationBar.frame.size.height {
            
            var keyboardHeight = view.bounds.height  - endFrame.origin.y + height + 17
            keyboardHeight = keyboardHeight > 0 ? keyboardHeight : 0
            
            textViewBottomConstraint.constant =  -keyboardHeight
            
            view.updateConstraintsIfNeeded()
            view.layoutIfNeeded()
        }
    }
    
    @objc func tapGestureHandler() {
        view.endEditing(true)
    }
//MARK: table view data source/delegate --------
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayComment_List.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
            let cellComment = tableView.dequeueReusableCell(withIdentifier: "CommentsCell") as! CommentsCell
            
            let obj = arrayComment_List[indexPath.row]
            
           // cellComment.imgViewProfile.sd_setImage(with: URL(string: obj.profileImage), placeholderImage: AppInfo.userPlaceHolder)
        
           cellComment.imgViewProfile.sd_setImage(with: URL(string: obj.profileImage), placeholderImage: nil)

            cellComment.lblName.text = obj.name
            cellComment.lblName.makeSpacingWithLeftAlignment(lineSpace: 4.0, font: UIFont.tamilBold(size: 14), color: UIColor.colorWithRGB(r: 22, g: 22, b: 2, alpha: 1.0))

            cellComment.lblTime.text = obj.notificationAgo
            
            cellComment.btnMore.tag = indexPath.row
            
            cellComment.lblComment.text = obj.comment
            cellComment.lblComment.makeSpacingWithLeftAlignment(lineSpace: 3.0, font: UIFont.tamilRegular(size: 14), color: UIColor.colorWithRGB(r: 122, g: 122, b: 122, alpha: 1.0))

            cellComment.moreButtonAction = { (cellIndex) in
                if let indexOfCell = cellIndex{
                    
                    self.showActionSheet(at : indexOfCell)
                }
            }
        
            return cellComment
    }
    
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    
    @IBAction func sendCommentTapped(_ sender : UIButton){
        if self.growingTextView.text.isEmptyAfterRemovingWhiteSpacesAndNewlines() == true {
            return
        }
        callCustomNotificationPostCommentAPI(with: sender, likeUnlike: "", CommentText:
            self.growingTextView.text.removeWhiteSpace())
           self.growingTextView.resignFirstResponder()
        
    }
    
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func textViewLayoutConfi () {
        automaticallyAdjustsScrollViewInsets = false
        growingTextView.layer.cornerRadius = 4.0
       // growingTextView.maxLength = 500
        growingTextView.trimWhiteSpaceWhenEndEditing = false
        growingTextView.placeholder = "Write a Comment..."
        growingTextView.placeholderColor = UIColor(white: 0.8, alpha: 1.0)
        growingTextView.minHeight = 25.0
        growingTextView.maxHeight = 100.0
        growingTextView.backgroundColor = UIColor.white
    }
    
    
    
    func setBottomViewTopLine(){
        let topBorder = UIView()
        topBorder.backgroundColor = UIColor.separatorGrayColor()
        topBorder.frame = CGRect(x: 0, y: 0, width: 500, height: 1)
        inputToolbar.addSubview(topBorder)
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        btnCommentSend.isSelected = !textView.text.isEmpty
    }
    
    
    // MARK:  Call Post comment api
    func callCustomNotificationPostCommentAPI(with  favButton : UIButton , likeUnlike:String , CommentText:String){

        guard let cUser = DoviesGlobalUtility.currentUser else{return}
        var parameters = [String : Any]()
        parameters[WsParam.authCustomerId] =  cUser.customerAuthToken
        parameters[WsParam.connectionId] = notificationConnectionid
        parameters[WsParam.like] = likeUnlike
        parameters[WsParam.comment] = CommentText
        DoviesWSCalls.callCustomNotificationPostCommentAPI(dicParam: parameters, onSuccess: { (success, message ,commentObj) in
            
            if let comment = commentObj, success {
                self.growingTextView.text = ""
                self.btnCommentSend.isSelected = false
                self.arrayComment_List.insert(comment, at: 0)
                self.tableView.reloadData()

                if self.arrayComment_List.count == 0
                {
                   // DoviesGlobalUtility.setNoLabelData(tblView: self.tableView, text: "No comments found.")
                }
                else
                {
                    self.tableView.backgroundView = nil
                }
            }

        })
        { (error) in
            

        }
    }
    
    
    // MARK:  Call Report a Problem and comment delete api

    func showActionSheet(at indexPath : IndexPath){
        let arrData: [alertActionData]!
        
        print(indexPath.row)
        
        if let cUser = DoviesGlobalUtility.currentUser, arrayComment_List[indexPath.row].customerId == cUser.customerId{
            arrData = [
                alertActionData(title: "Delete", imageName: "btn_del_s21a", highlighedImageName : "btn_del_s21a ")
            ]
        }else{
            arrData = [
                alertActionData(title: "Report Comment", imageName: "abouts_icon_42", highlighedImageName : "abouts_icon_42_h")
            ]
        }
        DoviesGlobalUtility.showDefaultActionSheet(on:self,actionData: arrData,cancelTitle: "Cancel") { (clickedIndex:Int, isCancel:Bool) in
            if clickedIndex == 0{
                if let cUser = DoviesGlobalUtility.currentUser, self.arrayComment_List[indexPath.row].customerId == cUser.customerId{
                    self.callDeleteComment(at: indexPath)
                }
                else{
                    self.callReportComment(at: indexPath)
                }
            }
        }
    }
    
    /**
     This method is used to call the delete comment api to delete the comment and after successful deletion, the comment count will update in UI.
     - Parameter indexPath: is used to get commentId for particular index to delete the comment
     */
    
    func callDeleteComment(at indexPath : IndexPath){
        guard let cUser = DoviesGlobalUtility.currentUser, arrayComment_List.count > indexPath.row else {return}
        var params = [String: Any]()
        params[WsParam.authCustomerId] = cUser.customerAuthToken
        params[WsParam.comment_Id] = arrayComment_List[indexPath.row].customNotificationLikeCommentId
        self.arrayComment_List.remove(at: indexPath.row)
        
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
        if arrayComment_List.count == 0
        {
          //  DoviesGlobalUtility.setNoLabelData(tblView: self.tableView, text: "No comments found.")
        }
        else
        {
            self.tableView.backgroundView = nil
        }
        DoviesWSCalls.callDeleteNotificationCommentAPI(dicParam: params, onSuccess: { (success, message) in
            if success{
               // self.updateCount(isAdd : false)
            }
            
        }) { (error) in
        }
    }
    
   

    /**
     This method is used to call the delete comment api to report the comment and after successful reporting.
     - Parameter indexPath: is used to get commentId for particular index to report the comment
     */
    func callReportComment(at indexPath : IndexPath){
        guard let cUser = DoviesGlobalUtility.currentUser, arrayComment_List.count > indexPath.row  else {return}
        var params = [String: Any]()
        params[WsParam.authCustomerId] = cUser.customerAuthToken
        params[WsParam.comment_Id] = arrayComment_List[indexPath.row].customNotificationLikeCommentId
        params[WsParam.reportText] = ""
        params[WsParam.customNotificationId] = arrayComment_List[indexPath.row].customNotificationId
        
        DoviesWSCalls.callReportNotificationCommentAPI(dicParam: params, onSuccess: { (success, message) in
            GlobalUtility.showToastMessage(msg: "Done")
        }) { (error) in
        }
    }
}

extension CommentNotificationListVC
{
 
        func callNotificationDetailAPI(){
            
            guard let user = DoviesGlobalUtility.currentUser else{return}
            
            var params = [String : Any]()
            params[WsParam.authCustomerId] = user.customerAuthToken
            params[WsParam.pageIndex] = "1"
            params[WsParam.notificaitonId] = notificationId
            DoviesWSCalls.callGetNotificationsAPI(dicParam: params, onSuccess: { [weak self](success, notifications, message,hasNextPage) in
                guard let weakSelf = self else {return}
                
                if success, let notificationList = notifications{
                    if  let notificationList = notifications
                    {
                        
                        weakSelf.arrNotificationsDetail = notificationList
                        weakSelf.arrayComment_List = weakSelf.arrNotificationsDetail[0].commentList
                        
                    }
                }
                weakSelf.tableView.reloadData()
                
            }) { [weak self](error) in
                guard let weakSelf = self else {return}
                weakSelf.tableView.reloadData()
            }
        }
    }
