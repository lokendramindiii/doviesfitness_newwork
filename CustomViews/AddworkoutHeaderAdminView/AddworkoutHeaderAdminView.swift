//
//  AddworkoutHeaderAdminView.swift
//  Dovies
//
//  Created by Mindiii on 12/3/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class AddworkoutHeaderAdminView: UIView {
    
    @IBOutlet var btnProfile: UIButton!
    @IBOutlet var txtfldTitle: UITextField!
    @IBOutlet var lblLevel: UILabel!
    
    @IBOutlet var btnLevel: UIButton!
    
    @IBOutlet var btnLevelImage: UIButton!
    @IBOutlet var txtView: UITextView!
    @IBOutlet var btnGoodFor: UIButton!
    @IBOutlet var btnGoodForImage: UIButton!
    @IBOutlet var lblGoodFor: UILabel!
    @IBOutlet var lblEquipments: UILabel!
    @IBOutlet var btnEquipments: UIButton!
    @IBOutlet var btnEquipImage: UIButton!
    
    // admin
    @IBOutlet var btnAllowedUser: UIButton!
    @IBOutlet var btnAllowedUserImage: UIButton!
    @IBOutlet var lblAlowedUser: UILabel!
    
    @IBOutlet var btnAccesslevel: UIButton!
    @IBOutlet var btnAccesslevelImage: UIButton!
    @IBOutlet var lblAccesslevel: UILabel!
    
    @IBOutlet var btnDisplayNewsfeed: UIButton!
    @IBOutlet var btnDisplayNewsfeedImage: UIButton!
    @IBOutlet var lblDisplayNewsfeed: UILabel!
    // change 6 dec 2018 remove workout group
    @IBOutlet var btnWorkoutGroup: UIButton!
    @IBOutlet var btnWorkoutGroupImage: UIButton!
    @IBOutlet var lblWorkoutGroup: UILabel!
    
    @IBOutlet var btnAllowNotification: UIButton!
    @IBOutlet var btnAllowNotificationImage: UIButton!
    @IBOutlet var lblAllowNotification: UILabel!
    
    @IBOutlet var btnCreatedBy: UIButton!
    @IBOutlet var btnCreatedByImage: UIButton!
    @IBOutlet var lblCreatedBy: UILabel!

    
    @IBOutlet var btnExercise: UIButton!
    @IBOutlet var btnExerciseImage: UIButton!
    
    @IBAction func btnClickPhoto(_ sender: Any) {
    }
    @IBOutlet var btnClickPhoto: UIButton!
    @IBAction func btnProfileClicked(_ sender: Any) {
    }
    @IBOutlet var imgAddPhoto: UIImageView!
    @IBOutlet var img_StarPhoto: UIImageView!
    
    // loks add functionality new desgine
    @IBOutlet var btnDefaultViewHideShow: UIButton!
    @IBOutlet var Image_updownarrow: UIImageView!
    
    @IBOutlet var btn_allTimerExcercise: UIButton!
    @IBOutlet var btn_TimeFinish_reps: UIButton!
    @IBOutlet var btnNoOfReps: UIButton!
    
    @IBOutlet var ViewTimerHightConstraint: NSLayoutConstraint!
    @IBOutlet var View_AllRepsTimer: UIView!

}
