//
//  Exercise+CoreDataClass.swift
//  
//
//  Created by Neel Shah on 01/05/18.
//
//

import Foundation
import CoreData


public class Exercise: NSManagedObject {
	
	class func getExerciseFromContext(params:NSPredicate?,sortOn:String? = nil ,isAsending:Bool? = nil) -> [AnyObject]? {
		var sortAry = [NSSortDescriptor]()
		if let sortKey = sortOn {
			let sortDesc = NSSortDescriptor(key: sortKey, ascending: isAsending!, selector:#selector(NSString.localizedCaseInsensitiveCompare(_:)))
			sortAry = [sortDesc]
		}
		return self.getExerciseSortArrayFromContext(params: params, sortDescAry: sortAry as NSArray?)
	}
	
    
	class func getExerciseSortArrayFromContext(params:NSPredicate?,sortDescAry:NSArray? = nil) -> [AnyObject]? {
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Exercise")
		if let predict = params {
			fetchRequest.predicate = predict
		}
		
		let context = APP_DELEGATE.managedObjectContext
		
		let entity = NSEntityDescription.entity(forEntityName: "Exercise", in: (context))
		fetchRequest.entity = entity
		
		if let sortKey = sortDescAry {
			fetchRequest.sortDescriptors = sortKey as? [NSSortDescriptor]
		}
		do {
			let fetchedDreams = try context.fetch(fetchRequest)
			if fetchedDreams.count > 0 {
				return fetchedDreams as [AnyObject]?
			} else {
				return nil
			}
		} catch {
			return nil
		}
	}
	
	class func deleteAllDataOfExercise()
	{
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Exercise")
		let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
		
		do {
			try APP_DELEGATE.managedObjectContext.execute(deleteRequest)
		} catch {
			// Error Handling
		}
	}
    
	
	class func addNewRecoredInDB(modelWorkoutExerciseList:ModelWorkoutExerciseList, exercise: Exercise, parentId: String){
		
        exercise.workoutShareUrl = modelWorkoutExerciseList.workoutShareUrl

		exercise.workoutExerciseBodyParts = modelWorkoutExerciseList.workoutExerciseBodyParts
		exercise.workoutExerciseBreakTime = modelWorkoutExerciseList.workoutExerciseBreakTime
		exercise.workoutExerciseDescription = modelWorkoutExerciseList.workoutExerciseDescription
		exercise.workoutExerciseDetail = modelWorkoutExerciseList.workoutExerciseDetail
		exercise.workoutExerciseEquipments = modelWorkoutExerciseList.workoutExerciseEquipments
		exercise.workoutExerciseImage = modelWorkoutExerciseList.workoutExerciseImage
		exercise.workoutExerciseLevel = modelWorkoutExerciseList.workoutExerciseLevel
		exercise.workoutExerciseName = modelWorkoutExerciseList.workoutExerciseName
		exercise.workoutExercisesId = modelWorkoutExerciseList.workoutExercisesId
		exercise.workoutExerciseVideo = modelWorkoutExerciseList.workoutExerciseVideo
		exercise.workoutExerciseTime = modelWorkoutExerciseList.workoutExercisesTime
		exercise.workoutRepeatText = modelWorkoutExerciseList.workoutRepeatText
		exercise.workoutExerciseType = modelWorkoutExerciseList.workoutExerciseType
		exercise.workoutExerciseBreakTime = modelWorkoutExerciseList.workoutBreakTime
		exercise.workoutAccessLevel = modelWorkoutExerciseList.workoutAccessLevel
		exercise.exerciseParentId = parentId
        // workoutExerciseSequenceSequenceNumber 18-02-19
        exercise.workoutExerciseSequenceSequenceNumber = modelWorkoutExerciseList.workoutExerciseSequenceSequenceNumber
        }
	
	class func saveContactInDB(modelWorkoutExerciseList:ModelWorkoutExerciseList, parentId: String) ->  Exercise{
        
        let predicate = NSPredicate(format: "workoutExercisesId == '\(modelWorkoutExerciseList.workoutExercisesId!)' && exerciseParentId == '\(parentId)' && workoutExerciseSequenceSequenceNumber == '\(modelWorkoutExerciseList.workoutExerciseSequenceSequenceNumber!)'")
        
		var exercise: Exercise!
        if let arrMusicTrack = Exercise.getExerciseFromContext(params: predicate) , arrMusicTrack.count > 0 {
            exercise = arrMusicTrack.first as? Exercise
        }
        else
        {
			let context = APP_DELEGATE.managedObjectContext
			let entity = NSEntityDescription.entity(forEntityName: "Exercise", in: (context))
			exercise = Exercise(entity: entity!, insertInto: (context))
		}
		Exercise.addNewRecoredInDB(modelWorkoutExerciseList: modelWorkoutExerciseList, exercise: exercise, parentId: parentId)
		
		return exercise
	}
}
