//
//	Data.swift
//
//	Create by hb on 12/3/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class FilterGroup : NSObject, NSCoding{

	var groupName : String!
	var list : [FilterDataModel]!
    var groupKey : String!

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		groupName = dictionary["group_name"] as? String ?? ""
        groupKey = dictionary["group_key"] as? String ?? ""
		list = [FilterDataModel]()
		if let listArray = dictionary["list"] as? [[String:Any]]{
			for dic in listArray{
				let value = FilterDataModel(fromDictionary: dic)
				list.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if groupName != nil{
			dictionary["group_name"] = groupName
		}
        if groupKey != nil{
            dictionary["group_key"] = groupKey
        }
		if list != nil{
			var dictionaryElements = [[String:Any]]()
			for listElement in list {
				dictionaryElements.append(listElement.toDictionary())
			}
			dictionary["list"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         groupName = aDecoder.decodeObject(forKey: "group_name") as? String
         groupKey = aDecoder.decodeObject(forKey: "group_key") as? String
         list = aDecoder.decodeObject(forKey :"list") as? [FilterDataModel]
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if groupName != nil{
			aCoder.encode(groupName, forKey: "group_name")
		}
        if groupKey != nil{
            aCoder.encode(groupName, forKey: "group_key")
        }
		if list != nil{
			aCoder.encode(list, forKey: "list")
		}
	}

}
