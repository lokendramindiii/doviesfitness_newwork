//
//  FeaturedGroupDetailsVC.swift
//  Dovies
//
//  Created by HB-PC on 13/06/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class FeaturedGroupDetailsVC: BaseViewController {

    var selectedFeaturedGroup : ModelFeaturedWorkouts?
    let cellSizeValue : CGFloat  = (ScreenSize.width - 62 )/2
    
    
    var pullToRefreshCtrl:UIRefreshControl!
    var yForHeader:CGFloat = 0.0
    @IBOutlet weak var colGroups : UICollectionView!
    @IBOutlet weak var viewMainBackgroud: UIView!

    var arrGroups : [GroupDetailsModel] = [GroupDetailsModel]()
    var selectedIndex : IndexPath!
    var Workoutcount : String!
    var isGradientLoad  = true
    var isFromFeaturesVC  = false

    //MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 20
        layout.minimumLineSpacing = 20
        colGroups!.collectionViewLayout = layout

        self.view.backgroundColor = UIColor.appBlackColorNew()
        
        if #available(iOS 11.0, *) {
            //colGroups.contentInsetAdjustmentBehavior = .always
        } else {
            // Fallback on earlier versions
        }


        Workoutcount = ""
        colGroups.register(UINib(nibName: "GroupDetailsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "GroupDetailsCollectionCell")
        
        colGroups.register(UINib(nibName: "FeaturedGroupsHeaderView", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "FeaturedGroupsHeaderView")
        
        viewMainBackgroud.backgroundColor = UIColor.appBlackColorNew()
        addNavigationBackButton()
        addTitleWithImage()
        setPullToRefresh()
        callFeaturedAPI()
        
    }
    
    @IBAction func actionBack() {
        if isFromFeaturesVC
        {
            self.navigationController?.popViewController(animated: false)
        }
        else
        {
            self.navigationController?.popViewController(animated: true)
        }
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }

    //MARK: - Custom methods
    func addTitleWithImage(){
        let imageView = UIImageView(image:UIImage(named: "logo_topbar"))
        self.navigationItem.titleView = imageView
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
            self.colGroups.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
    }

    
    func setPullToRefresh(){
        pullToRefreshCtrl = UIRefreshControl()
        pullToRefreshCtrl.addTarget(self, action: #selector(self.pullToRefreshClick(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            colGroups.refreshControl  = pullToRefreshCtrl
        } else {
            colGroups.addSubview(pullToRefreshCtrl)
        }
    }
    
    @objc func pullToRefreshClick(sender:UIRefreshControl) {
        callFeaturedAPI()
        sender.endRefreshing()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}


// API Calls
extension FeaturedGroupDetailsVC {
    /**
     This method is used fetch the group details of workout group.
     */
    func callFeaturedAPI(){
        var dictParams = [String : Any]()
        dictParams["workout_group_id"] = selectedFeaturedGroup!.workoutGroupId
        DoviesWSCalls.callGroupDetailsAPI(dicParam: dictParams, onSuccess: { (success , workoutcount , msg, arrGroupModels) in
            if success{
                self.Workoutcount = workoutcount
                self.arrGroups = arrGroupModels
                self.colGroups.reloadData()
            }else{
                
            }
        }) { (error) in
            
        }
    }
}


extension FeaturedGroupDetailsVC : UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0{
            return 0
        }else{
            if arrGroups.count >= section{
                let group = arrGroups[section-1]
                if group.isOpen{
                    return  group.workoutList.count
                }else{
                    if group.workoutList.count > 2{
                        return 2
                    }else{
                        return group.workoutList.count
                    }
                }
            }else{
                return 0
            }
        }
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return arrGroups.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GroupDetailsCollectionCell", for: indexPath) as? GroupDetailsCollectionCell else{return UICollectionViewCell()}
        let group = arrGroups[indexPath.section - 1]
        cell.viewBackground.backgroundColor = UIColor.appBlackColorNew()
        cell.setCell(with: group.workoutList[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            if indexPath.section == 0{
                guard let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,withReuseIdentifier: "GroupWorkoutsCollectionHeader", for: indexPath) as? GroupWorkoutsCollectionHeader else{return UICollectionReusableView()}
                
                if let group = selectedFeaturedGroup {
              
                    if isGradientLoad
                    {
                          isGradientLoad = false
                        let gradientView = UIView(frame: CGRect(x: 0, y: (ScreenSize.width)/2, width: ScreenSize.width, height: (ScreenSize.width) - (ScreenSize.width)/2))
                        let gradientLayer:CAGradientLayer = CAGradientLayer()
                        gradientLayer.frame.size =  gradientView.frame.size

                        gradientLayer.colors = [UIColor.clear.cgColor,UIColor.appBlackColorNew().withAlphaComponent(15).cgColor]

                        gradientView.layer.addSublayer(gradientLayer)
                        headerView.imgGroup.addSubview(gradientView)
                    }
                    headerView.viewBackgroud.backgroundColor = UIColor.appBlackColorNew()
                    headerView.imgGroup.sd_setImage(with: URL(string: group.workoutGroupImage), placeholderImage: nil)
                    
                    headerView.lblGroupName.text = group.workoutGroupName.uppercased()
                    
                    if group.groupWorkoutCount == "0"
                    {
                        if Workoutcount == ""
                        {
                            headerView.lblGroupWorkoutsDetails.text = group.workoutGroupLevel + " -  Coming soon"
                        }
                        else
                        {
                            if self.Workoutcount == "0" || self.Workoutcount == "1"
                            {
                                headerView.lblGroupWorkoutsDetails.text = group.workoutGroupLevel + " - " + self.Workoutcount + " Workout"
                            }
                            else
                            {
                                headerView.lblGroupWorkoutsDetails.text = group.workoutGroupLevel + " - " + self.Workoutcount + " Workouts"
                            }
                        }
                    }
                    else
                    {
                        if self.Workoutcount == "0" || self.Workoutcount == "1"
                        {
                            headerView.lblGroupWorkoutsDetails.text = group.workoutGroupLevel + " - " + self.Workoutcount + " Workout"
                        }
                        else
                        {
                            headerView.lblGroupWorkoutsDetails.text = group.workoutGroupLevel + " - " + self.Workoutcount + " Workouts"
                            
                        }
                    }
                    let style = NSMutableParagraphStyle()
                    style.lineSpacing = 9.0
                    
                    // 136 136 136  color
                    let att = [NSAttributedStringKey.font : UIFont.tamilRegular(size: 15.0), NSAttributedStringKey.foregroundColor : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), NSAttributedStringKey.paragraphStyle : style, NSAttributedStringKey.kern : 0.5]
                        as [NSAttributedStringKey : Any]
                    
                    let attStr = NSMutableAttributedString(string: group.workoutGroupDescription, attributes: att
                    )
                    DispatchQueue.main.async {
                        headerView.lblGroupDescription.attributedText = attStr
                    }
                }
                return headerView
            } else {
                guard let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,withReuseIdentifier: "FeaturedGroupsHeaderView", for: indexPath) as? FeaturedGroupsHeaderView else{return UICollectionReusableView()}
                let group = self.arrGroups[indexPath.section - 1]
                headerView.btnViewAll.isHidden = !(group.workoutList.count > 2)
                
                if let levelname = group.levelName , levelname == "All"
                {
                    if group.workoutList.count == 0 || group.workoutList.count == 1
                    {
                        headerView.lblTItle.text = "All Levels" + " - " + "\(group.workoutList.count)" + " workout"
                    }
                    else
                    {
                        headerView.lblTItle.text = "All Levels" + " - " + "\(group.workoutList.count)" + " workouts"
                    }
                }
                else
                {
                    if group.workoutList.count == 0 || group.workoutList.count == 1
                    {
                        headerView.lblTItle.text = group.levelName + " - " + "\(group.workoutList.count)" + " workout"
                    }
                    else
                    {
                        headerView.lblTItle.text = group.levelName + " - " + "\(group.workoutList.count)" + " workouts"
                    }
                }
                
                if group.isOpen {
                    
                    headerView.btnViewAll.setTitle("Hide All", for: .normal)
                }
                else
                {
                    headerView.btnViewAll.setTitle("View All", for: .normal)
                    
                }

                
                headerView.indexPath = indexPath
                headerView.viewAllTapped = { (iPath) in
                    if !group.isOpen{
                        
                        let group = self.arrGroups[iPath.section - 1]
                        group.isOpen = true
                        collectionView.reloadSections(IndexSet(integer: iPath.section))
                        collectionView.scrollToItem(at: iPath, at: .bottom, animated: true)
                    }
                    else
                    {
                        group.isOpen = false
                        collectionView.reloadData()
                    }
                }
                return headerView
            }
            
        default:
            return UIView() as! UICollectionReusableView
        }
    }
    
}

extension FeaturedGroupDetailsVC : UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let workout = arrGroups[indexPath.section - 1].workoutList[indexPath.row]
        selectedIndex = indexPath
        let featuredWorkOutDetailViewControlle = storyboard?.instantiateViewController(withIdentifier: "FeaturedWorkOutDetailViewController") as! FeaturedWorkOutDetailViewController
        featuredWorkOutDetailViewControlle.navigationItem.title = workout.workoutName
        featuredWorkOutDetailViewControlle.modelWorkoutList = ModelWorkoutList(fromDictionary: workout.toDictionary())
        featuredWorkOutDetailViewControlle.onUpdateFavStatus = { (favStatus) in
            self.arrGroups[indexPath.section - 1].workoutList[indexPath.row].workoutFavStatus = favStatus ? "1" : "0"
            
        }
//        featuredWorkOutDetailViewControlle.workoutId = workout.workoutId
        featuredWorkOutDetailViewControlle.deleteWorkout = { () in
            
//            self.arrModelFeature[indexPath.section].workoutList.remove(at: indexPath.row)
//            self.IBFeatureTblView.reloadData()
        }
        self.navigationController?.pushViewController(featuredWorkOutDetailViewControlle, animated: true)
    }
}

extension FeaturedGroupDetailsVC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    //    return CGSize(width: cellSizeValue, height: cellSizeValue + 76)
        return CGSize(width: cellSizeValue, height: cellSizeValue )

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 20, bottom: 0, right:20)

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0{
            guard let group = selectedFeaturedGroup else{return CGSize.zero}
            var collectionHeaderHeight = ScreenSize.width
            
            let heightOfLabel = group.workoutGroupDescription.heightWithConstrainedWidth(width: ScreenSize.width-40, font: UIFont.tamilRegular(size: 15.0), lineSpace: 9)
            
            if  heightOfLabel > 0, !group.workoutGroupDescription.isEmpty{
             //   collectionHeaderHeight += heightOfLabel + 80
                collectionHeaderHeight += heightOfLabel + 40
            }
            return CGSize(width: ScreenSize.width, height: collectionHeaderHeight)
        }else{
            return CGSize(width: ScreenSize.width, height: 80)

        }
    }
}

extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle.main.loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}

//This collection resuable view is used to show header view of plan list
class GroupWorkoutsCollectionHeader: UICollectionReusableView {
    @IBOutlet weak var imgGroup : UIImageView!
    @IBOutlet weak var lblGroupName : UILabel!
    @IBOutlet weak var lblGroupWorkoutsDetails : UILabel!
    @IBOutlet weak var lblGroupDescription : UILabel!
    @IBOutlet weak var constLabelHeight: NSLayoutConstraint!

    @IBOutlet weak var viewBackgroud: UIView!

}

