

import Foundation


class ProgramDietPlan : NSObject, NSCoding{

	var programDietId : String!
	var programDietImage : String!
	var programDietName : String!
    var programDietPlanPdf : String!

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		programDietId = dictionary["program_diet_id"] as? String ?? ""
		programDietImage = dictionary["program_diet_image"] as? String ?? ""
		programDietName = dictionary["program_diet_name"] as? String ?? ""
        programDietPlanPdf = dictionary["diet_plan_pdf"] as? String ?? ""
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if programDietId != nil{
			dictionary["program_diet_id"] = programDietId
		}
		if programDietImage != nil{
			dictionary["program_diet_image"] = programDietImage
		}
		if programDietName != nil{
			dictionary["program_diet_name"] = programDietName
		}
        if programDietPlanPdf != nil{
            dictionary["diet_plan_pdf"] = programDietPlanPdf
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         programDietId = aDecoder.decodeObject(forKey: "program_diet_id") as? String
         programDietImage = aDecoder.decodeObject(forKey: "program_diet_image") as? String
         programDietName = aDecoder.decodeObject(forKey: "program_diet_name") as? String
        programDietPlanPdf = aDecoder.decodeObject(forKey: "diet_plan_pdf") as? String
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if programDietId != nil{
			aCoder.encode(programDietId, forKey: "program_diet_id")
		}
		if programDietImage != nil{
			aCoder.encode(programDietImage, forKey: "program_diet_image")
		}
		if programDietName != nil{
			aCoder.encode(programDietName, forKey: "program_diet_name")
		}
        if programDietPlanPdf != nil{
            aCoder.encode(programDietPlanPdf, forKey: "diet_plan_pdf")
        }

	}

}
