//
//	RootClass.swift
//
//	Create by Praveen on 13/6/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class GroupDetailsModel : NSObject, NSCoding{

	var levelName : String!
	var workoutCount : String!
	var workoutList : [WorkoutList]!
    var isOpen : Bool!

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		levelName = dictionary["level_name"] as? String ?? ""
		workoutCount = dictionary["workout_count"] as? String ?? ""
		workoutList = [WorkoutList]()
        isOpen = false
		if let workoutListArray = dictionary["workout_list"] as? [[String:Any]]{
			for dic in workoutListArray{
				let value = WorkoutList(fromDictionary: dic)
				workoutList.append(value)
			}
		}
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if levelName != nil{
			dictionary["level_name"] = levelName
		}
		if workoutCount != nil{
			dictionary["workout_count"] = workoutCount
		}
		if workoutList != nil{
			var dictionaryElements = [[String:Any]]()
			for workoutListElement in workoutList {
				dictionaryElements.append(workoutListElement.toDictionary())
			}
			dictionary["workout_list"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         levelName = aDecoder.decodeObject(forKey: "level_name") as? String
         workoutCount = aDecoder.decodeObject(forKey: "workout_count") as? String
         workoutList = aDecoder.decodeObject(forKey :"workout_list") as? [WorkoutList]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if levelName != nil{
			aCoder.encode(levelName, forKey: "level_name")
		}
		if workoutCount != nil{
			aCoder.encode(workoutCount, forKey: "workout_count")
		}
		if workoutList != nil{
			aCoder.encode(workoutList, forKey: "workout_list")
		}

	}

}
