//
//  StartUpViewController.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

class CancellableClosure{
    var cancelled = false
    var closure: () -> () = {}
    
    func run() {
        if (cancelled == false) {
            cancelled = true     // in case they also did a runAfterDelayOf
            closure()
        }
    }
    
    func runAfter(delay: Double = 0.35) {
        if (cancelled == false) {
            DoviesGlobalUtility.delay(delay: delay, closure: {
                if self.cancelled == false {
                    self.closure()
                }
            })
        }
    }
}

//This view controller used to show tutorial screens
//and used to select sign up or login option
class StartUpViewController: BaseViewController {

    @IBOutlet weak var collectionV : UICollectionView!
    @IBOutlet weak var pageControl : UIPageControl!
    
    var currentIndex = 0
    var tutorialList = TutoroalList()
    var headerScrollDelay = 5.0
    
    var cancellableClosure = CancellableClosure()
    var autoScrollClosure = CancellableClosure()
    //MARK:- View Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpLayout()
        // Do any additional setup after loading the view.
    }
    
    //MARK: - Custom methods
    /// using this function application will redirect to login screen
    func gotoLogin(){
        self.performSegue(withIdentifier: "", sender: nil)
    }
    
    func gotoSignUp(){
        self.performSegue(withIdentifier: "", sender: nil)
    }
    
    func setUpLayout(){
        self.pageControl.numberOfPages = tutorialList.tutorialItems.count
        self.pageControl.currentPage = 0
        self.currentIndex = 0
        collectionV.isUserInteractionEnabled = true
        pageControl.isUserInteractionEnabled = true
        loadTimer()
    }

    func loadTimer() {
        self.autoScrollClosure.cancelled = true
        
        let cancelClosure = CancellableClosure()
        var reloadIndex = 0
        cancelClosure.closure = {
            
            if let firstIP = self.collectionV.indexPathsForVisibleItems.first {
                let itemNum = firstIP.item
                if self.tutorialList.tutorialItems.count > itemNum {
                    reloadIndex = itemNum + 1
                }
            }
            
            if reloadIndex < self.tutorialList.tutorialItems.count
			{
                self.collectionV.scrollToItem(at: IndexPath(item: reloadIndex, section: 0), at: UICollectionViewScrollPosition.right, animated: true)
            }
			else
			{
                self.collectionV.scrollToItem(at: IndexPath(item: 0, section: 0), at: UICollectionViewScrollPosition.left, animated: false)
                reloadIndex = 0
            }
            
            self.updatePageDisplay(index: NSNumber(value: reloadIndex))
            
            cancelClosure.runAfter(delay: self.headerScrollDelay)
            
        }
        cancelClosure.runAfter(delay: headerScrollDelay)
        
        self.autoScrollClosure = cancelClosure
        
    }
    
    func updatePageDisplay(index: NSNumber? = nil) {
        
        self.cancellableClosure.cancelled = true
        
        let cancelClosure = CancellableClosure()
        
        if let num = index {
            pageControl.currentPage = num.intValue
            
        } else {
            pageControl.currentPage = Int(collectionV.contentOffset.x / ScreenSize.width)
            
        }
        
        cancelClosure.runAfter(delay: headerScrollDelay)
        
        self.cancellableClosure = cancelClosure
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        updatePageDisplay()
    }
    
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        
    }
    
    //MARK: - Actions
    @IBAction func pageControlValueChanged(_ sender : UIPageControl){
        collectionV.scrollToItem(at: IndexPath.init(row: sender.currentPage, section: 0), at: (sender.currentPage < currentIndex ? .left : .right), animated: true)
        currentIndex = sender.currentPage
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension StartUpViewController : UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tutorialList.tutorialItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TutorialCell", for: indexPath) as? TutorialCell else{return UICollectionViewCell()}
        cell.setCellForTutorialData(data:tutorialList.tutorialItems[indexPath.item])
        return cell
    }
    
    // Animation of UICollectionViewCell
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.alpha = 0
        UIView.animate(withDuration: 0.5, delay: 0.1, options: UIViewAnimationOptions.curveEaseOut, animations: {
            cell.alpha = 1
        }) { (isCompleted) in
            
        }
    }
    
}

extension StartUpViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: ScreenSize.width, height: ScreenSize.height)
    }
    
}


///This collection cell used to load tutorial view
class TutorialCell: UICollectionViewCell {
    
    @IBOutlet weak var iconV : UIImageView!
    
    /// this method used to set title and icon for cell based on indexpath
    ///
    /// - Parameter indexPath: IndexPath object
    func setCellForTutorialData(data : TutorialData){
        iconV.image = data.backgroundImage
        
    }
    
}

//Menu data with menu item properties
struct TutorialData {
   
    var backgroundImage : UIImage!
    
    init(_backgroundImage : String) {
        backgroundImage = UIImage(named: _backgroundImage)
    }
}

//Menu items list will used to get menu items
struct TutoroalList {
    var tutorialItems : [TutorialData]
    init() {
        let tutorialDataList = [TutorialData(_backgroundImage:"FIRST_PAGE_DESIGN_FOR_APP.png"),
                                TutorialData(_backgroundImage: "2ND_PAGE.jpg"),
                                TutorialData(_backgroundImage: "3RD_PAGE.jpg"),
//                                TutorialData(_backgroundImage: "img_tutorial.jpg"),
//                                TutorialData(_backgroundImage: "img_tutorial.jpg"),
                            ]
        
        tutorialItems = tutorialDataList
        
    }

}



