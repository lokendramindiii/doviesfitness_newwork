//
//  ProgramsViewController.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

//This view controller is used to show programs list for user
class ProgramsViewController: BaseViewController {

    var pullToRefreshCtrl:UIRefreshControl!
   // @IBOutlet weak var collectionV : BaseCollectionView!
    @IBOutlet weak var IBTableview : UITableView!
    @IBOutlet weak var loaderView : UIView!
    @IBOutlet weak var lbl_NoProgram : UILabel!

    static let cellIndentifier = "ProgramsCollectionCell"
    static let cellPadding : CGFloat = 5.0

  //  let cellSizeValue : CGFloat  = (ScreenSize.width - (ProgramsViewController.cellPadding))/2
    let cellSizeValue : CGFloat  = (ScreenSize.width - 1)/2

    var headerHeight : CGFloat = 80
    var programs = [Program]()

    var programsStaticInfo : ProgramStaticInfo?
    var selectedProgram : Program?
    var placeholderCellsCount = 0
    
    var shouldLoadMore = false
    var Ispulltorefresh = false

    var currentPageIndex = 1
    
    //MARK: - View Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        inititalUISetUp()
    }
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)


     //   self.navigationController?.navigationBar.barTintColor = UIColor.appNavColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,
                                                                        NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]

        
        Ispulltorefresh = false
        if APP_DELEGATE.isFromNotification{
            APP_DELEGATE.isFromNotification = false
            currentPageIndex = 1
            callGetProgramsListAPI()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Navigation--Color

//        self.navigationController?.navigationBar.barTintColor = UIColor.white
//
//        self.navigationController?.navigationBar.titleTextAttributes = [
//            NSAttributedStringKey.foregroundColor : UIColor.colorWithRGB(r: 34.0, g: 34.0, b: 34.0),
//            NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]
    }
    
    //MARK: - View Styling
    override func inititalUISetUp() {
   
        self.view.backgroundColor = UIColor.appBlackColorNew()
        IBTableview.backgroundColor = UIColor.appBlackColorNew()
        
        IBTableview.register(UINib(nibName: "DietPlanCell", bundle: nil), forCellReuseIdentifier: "DietPlanCell")

      //  IBTableview.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
        (self.navigationController?.navigationBar.frame.height ?? 0.0)
        self.IBTableview.contentInset = UIEdgeInsets(top: topBarHeight  , left: 0, bottom: self.tabBarController!.tabBar.frame.height , right: 0)

        self.title = "Workout Plan"
        self.navigationItem.title = "Workout Plan".uppercased()
        setPullToRefresh()
      //  showLoader(isShow: true)
		callGetProgramsListAPI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
        (self.navigationController?.navigationBar.frame.height ?? 0.0)
        self.IBTableview.contentInset = UIEdgeInsets(top: topBarHeight  , left: 0, bottom: self.tabBarController!.tabBar.frame.height , right: 0)

    }

    func showLoader(isShow : Bool){
        if isShow{
            loaderView.showLoader()
        }else{
            loaderView.hideLoader()
        }
        
        UIView.animate(withDuration: 0.1) {[weak self] in
            guard let weakSelf = self else{return}
            weakSelf.IBTableview.alpha = isShow ? 0.0 : 1.0
        }
    }
    
    func setPullToRefresh(){
        pullToRefreshCtrl = UIRefreshControl()
        pullToRefreshCtrl.addTarget(self, action: #selector(self.pullToRefreshClick(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            IBTableview.refreshControl  = pullToRefreshCtrl
        } else {
            IBTableview.addSubview(pullToRefreshCtrl)
        }
    }
    
    @objc func pullToRefreshClick(sender:UIRefreshControl) {
        sender.endRefreshing()

        if  Ispulltorefresh == false
        {
        currentPageIndex = 1
        shouldLoadMore = false
        callGetProgramsListAPI()
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        //IBTableview.collectionViewLayout.invalidateLayout()
        self.navigationController?.isNavigationBarHidden = false
        
    }

    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let programDetailVc = segue.destination as? ProgramDetail_VC{
            
            programDetailVc.isFromProgramList = true
            programDetailVc.selectedProgram = selectedProgram
            programDetailVc.hidesBottomBarWhenPushed = true
            programDetailVc.onPurchaseSuccess = {
                self.selectedProgram?.programAccessLevel = "OPEN"
                self.selectedProgram?.accessLevel = "FREE"
                self.IBTableview.reloadData()
            }
        }
    }
    
    //MARK: - didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ProgramsViewController:UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return programs.count == 0 ? placeholderCellsCount : programs.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
     //   return ScreenSize.width

        return ScreenSize.width - 40
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DietPlanCell") as? DietPlanCell else
        {return UITableViewCell() }
        
        if programs.count != 0
        {
            cell.setCellUIProgram(with: programs[indexPath.row])
            
//            for subview in cell.imgBackground.subviews {
//                subview.removeFromSuperview()
//            }
//
//            let viewGradient =  UIView(frame: CGRect(x: 0, y: (ScreenSize.width - 40)/2, width:ScreenSize.width - 40, height: (ScreenSize.width - 40)/2))
//            let gradientLayer:CAGradientLayer = CAGradientLayer()
//            gradientLayer.frame.size =  viewGradient.frame.size
//            gradientLayer.colors = [UIColor.clear.cgColor,UIColor.appBlackColorNew().withAlphaComponent(0.5).cgColor]
//            viewGradient.layer.addSublayer(gradientLayer)
//            cell.imgBackground.addSubview(viewGradient)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.item == (programs.count - 1) && shouldLoadMore == true {
            shouldLoadMore = false
            callGetProgramsListAPI()
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if programs.count == 0{
            return
        }
        selectedProgram = programs[indexPath.item]
        self.performSegue(withIdentifier: "pushToProgramDetails", sender: nil)
    }
}

/*
extension ProgramsViewController : UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return programs.count == 0 ? placeholderCellsCount : programs.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProgramsViewController.cellIndentifier, for: indexPath) as? ProgramsCollectionCell else{return UICollectionViewCell()}
        if programs.count != 0
		{
            cell.setCellUI(with: programs[indexPath.item])
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == (programs.count - 1) && shouldLoadMore == true {
            shouldLoadMore = false
            callGetProgramsListAPI()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            guard let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,withReuseIdentifier: "ProgramsCollectionHeader", for: indexPath) as? ProgramsCollectionHeader else{return UICollectionReusableView()}
            if let programInfo = programsStaticInfo{
				headerView.lblDescription.textAlignment = .left
                
                let attributedString = NSMutableAttributedString(string: programInfo.content)
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.lineSpacing = 9.0
            
                attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
                
                attributedString.addAttribute(NSAttributedStringKey.kern, value: CGFloat(0.5), range: NSRange(location: 0, length: attributedString.length))
                
                headerView.lblDescription.attributedText = attributedString
            }
            return headerView
            
        case UICollectionElementKindSectionFooter:
            guard let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,withReuseIdentifier: BaseCollectionView.collectionViewLoadMoreIdentifier, for: indexPath) as? CollectionLoadMoreView else{return UICollectionReusableView()}
            return footerView
            
        default:
            return UIView() as! UICollectionReusableView
        }
    }
}

extension ProgramsViewController : UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        //Program tap action
		if programs.count == 0{
			return
		}
        selectedProgram = programs[indexPath.item]
        self.performSegue(withIdentifier: "pushToProgramDetails", sender: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {

        if let headerView = collectionView.visibleSupplementaryViews(ofKind: UICollectionElementKindSectionHeader).first as? ProgramsCollectionHeader
        {
            headerView.layoutIfNeeded()
            headerHeight = headerView.contentView.systemLayoutSizeFitting(UILayoutFittingExpandedSize).height
            return CGSize(width: collectionView.frame.width, height: headerHeight)
        }
        return CGSize.zero// CGSize(width: collectionView.frame.width, height: headerHeight)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return shouldLoadMore ? CGSize(width: ScreenSize.width, height: 40) : CGSize.zero
    }
	
/Users/ms-ios/Desktop/DoviesNew work 30august/Dovies/ViewControllers/RegisteredUser/Programs/ProgramsViewController}
*/
extension ProgramsViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellSizeValue,height: cellSizeValue )
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: -5, left: 0, bottom: 0, right: 0)
        // loks  ProgramsViewController.cellPadding relpace top:-5
    }
    
}

//API Calls
extension ProgramsViewController{

    /**
     This method used to get diet plans list
     */
    func callGetProgramsListAPI(){
        Ispulltorefresh = true
        
        guard let cUser = DoviesGlobalUtility.currentUser else{return}
    
        var parameters = [String : Any]()
        parameters[WsParam.authCustomerId] =  cUser.customerAuthToken
        parameters[WsParam.pageIndex] = "\(currentPageIndex)"
		
        DoviesWSCalls.callGetPrograListAPI(dicParam: parameters, onSuccess: { [weak self](success, programs, programsInfo, message, hasNextPage) in
            guard let weakSelf = self else{return}
            weakSelf.placeholderCellsCount = 0
            weakSelf.IBTableview.hideLoader()
            weakSelf.shouldLoadMore = hasNextPage
            if success, let programsList = programs{
                
                if weakSelf.currentPageIndex == 1{
                    weakSelf.programs = programsList
                }else{
                    weakSelf.programs += programsList
                }
                if let programData = programsInfo{
                    weakSelf.programsStaticInfo = programData
                }
                if hasNextPage{
                    weakSelf.currentPageIndex += 1
                }
                
                weakSelf.lbl_NoProgram.isHidden = true
                
            }else{
                weakSelf.programs = programs ?? []
                weakSelf.lbl_NoProgram.text = message
                weakSelf.lbl_NoProgram.isHidden = false
              
            }
            weakSelf.IBTableview.reloadData()
            weakSelf.showLoader(isShow: false)
            weakSelf.Ispulltorefresh = false

        }) { (error) in
            self.Ispulltorefresh = false
        }
    }
}

class ProgramsCollectionHeader: UICollectionReusableView {
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDescription : CustomLabel!
    @IBOutlet weak var contentView: UIView!
}

