//
//	User.swift
//
//	Create by CompanyName
//	Copyright © CompanyName. All rights reserved.

import Foundation

class User : NSObject, NSCoding{

    var customerAuthToken : String!
    var customerEmail : String!
    var customerEmailVerified : String!
    var customerFullName : String!
    var customerGender : String!
    var customerHeight : String!
    var customerId : String!
    var customerMobileNumber : String!
    var customerNotification : String!
    var customerNotifyRemainder : String!
    var customerProfileImage : String!
    var customerSocialNetwork : String!
    var customerSocialNetworkId : String!
    var customerStatus : String!
    var customerUnits : String!
    var customerUserName : String!
    var customerWeight : String!
    var customerCountryId : String!
    var customerCountryName : String!
    var customerIsdCode: String!
    var customerDob: String!
    var customerIsadmin: String!
    var notifyCount: String!
    var customerUserType: String!

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        customerAuthToken = dictionary["customer_auth_token"] as? String ?? ""
        notifyCount = dictionary["notifyCount"] as? String ?? ""

        customerUserType = dictionary["customer_user_type"] as? String ?? ""
        
        customerEmail = dictionary["customer_email"] as? String ?? ""
        customerEmailVerified = dictionary["customer_email_verified"] as? String ?? ""
        customerFullName = dictionary["customer_full_name"] as? String ?? ""
        customerGender = dictionary["customer_gender"] as? String ?? ""
        customerHeight = dictionary["customer_height"] as? String ?? ""
        customerId = dictionary["customer_id"] as? String ?? ""
        customerMobileNumber = dictionary["customer_mobile_number"] as? String ?? ""
        customerNotification = dictionary["customer_notification"] as? String ?? ""
        customerNotifyRemainder = dictionary["customer_notify_remainder"] as? String ?? ""
        customerProfileImage = dictionary["customer_profile_image"] as? String ?? ""
        customerSocialNetwork = dictionary["customer_social_network"] as? String ?? ""
        customerSocialNetworkId = dictionary["customer_social_network_id"] as? String ?? ""
        customerStatus = dictionary["customer_status"] as? String ?? ""
        customerUnits = dictionary["customer_units"] as? String ?? ""
        customerUserName = dictionary["customer_user_name"] as? String ?? ""
        customerWeight = dictionary["customer_weight"] as? String ?? ""
        
        customerCountryId = dictionary["customer_country_id"] as? String ?? ""
        customerCountryName = dictionary["customer_country_name"] as? String ?? ""
        customerIsdCode = dictionary["customer_isd_code"] as? String ?? ""
        customerDob = dictionary["dob"] as? String ?? ""
        customerIsadmin = dictionary["is_admin"] as? String ?? ""
        
        if customerIsadmin == "Yes"
        {
            DoviesGlobalUtility.isAdminUserName = customerUserName
        }

    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if customerAuthToken != nil{
            dictionary["customer_auth_token"] = customerAuthToken
        }
        if notifyCount != nil{
            dictionary["notifyCount"] = notifyCount
        }
        
        if customerUserType != nil{
            dictionary["customer_user_type"] = customerUserType
        }

        if customerEmail != nil{
            dictionary["customer_email"] = customerEmail
        }
        if customerEmailVerified != nil{
            dictionary["customer_email_verified"] = customerEmailVerified
        }
        if customerFullName != nil{
            dictionary["customer_full_name"] = customerFullName
        }
        if customerGender != nil{
            dictionary["customer_gender"] = customerGender
        }
        if customerHeight != nil{
            dictionary["customer_height"] = customerHeight
        }
        if customerId != nil{
            dictionary["customer_id"] = customerId
        }
        if customerMobileNumber != nil{
            dictionary["customer_mobile_number"] = customerMobileNumber
        }
        if customerNotification != nil{
            dictionary["customer_notification"] = customerNotification
        }
        if customerNotifyRemainder != nil{
            dictionary["customer_notify_remainder"] = customerNotifyRemainder
        }
        if customerProfileImage != nil{
            dictionary["customer_profile_image"] = customerProfileImage
        }
        if customerSocialNetwork != nil{
            dictionary["customer_social_network"] = customerSocialNetwork
        }
        if customerSocialNetworkId != nil{
            dictionary["customer_social_network_id"] = customerSocialNetworkId
        }
        if customerStatus != nil{
            dictionary["customer_status"] = customerStatus
        }
        if customerUnits != nil{
            dictionary["customer_units"] = customerUnits
        }
        if customerUserName != nil{
            dictionary["customer_user_name"] = customerUserName
        }
        if customerIsadmin != nil{
            dictionary["is_admin"] = customerIsadmin
        }
        
        if customerWeight != nil{
            dictionary["customer_weight"] = customerWeight
        }
        
        if customerCountryId != nil{
            dictionary["customer_country_id"] = customerCountryId
        }
        if customerCountryName != nil{
            dictionary["customer_country_name"] = customerCountryName
        }
        if customerIsdCode != nil{
            dictionary["customer_isd_code"] = customerIsdCode
        }
        
        if customerDob != nil{
            dictionary["dob"] = customerDob
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        notifyCount = aDecoder.decodeObject(forKey: "notifyCount") as? String

        customerUserType = aDecoder.decodeObject(forKey: "customer_user_type") as? String

        customerAuthToken = aDecoder.decodeObject(forKey: "customer_auth_token") as? String
        customerEmail = aDecoder.decodeObject(forKey: "customer_email") as? String
        customerEmailVerified = aDecoder.decodeObject(forKey: "customer_email_verified") as? String
        customerFullName = aDecoder.decodeObject(forKey: "customer_full_name") as? String
        customerGender = aDecoder.decodeObject(forKey: "customer_gender") as? String
        customerHeight = aDecoder.decodeObject(forKey: "customer_height") as? String
        customerId = aDecoder.decodeObject(forKey: "customer_id") as? String
        customerMobileNumber = aDecoder.decodeObject(forKey: "customer_mobile_number") as? String
        customerNotification = aDecoder.decodeObject(forKey: "customer_notification") as? String
        customerNotifyRemainder = aDecoder.decodeObject(forKey: "customer_notify_remainder") as? String
        customerProfileImage = aDecoder.decodeObject(forKey: "customer_profile_image") as? String
        customerSocialNetwork = aDecoder.decodeObject(forKey: "customer_social_network") as? String
        customerSocialNetworkId = aDecoder.decodeObject(forKey: "customer_social_network_id") as? String
        customerStatus = aDecoder.decodeObject(forKey: "customer_status") as? String
        customerUnits = aDecoder.decodeObject(forKey: "customer_units") as? String
        customerUserName = aDecoder.decodeObject(forKey: "customer_user_name") as? String
        customerIsadmin = aDecoder.decodeObject(forKey: "is_admin") as? String
        customerWeight = aDecoder.decodeObject(forKey: "customer_weight") as? String
       
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {

        if notifyCount != nil{
            aCoder.encode(notifyCount, forKey: "notifyCount")
        }
       

        if customerUserType != nil{
            aCoder.encode(customerUserType, forKey: "customer_user_type")
        }
        
        if customerAuthToken != nil{
            aCoder.encode(customerAuthToken, forKey: "customer_auth_token")
        }
        if customerEmail != nil{
            aCoder.encode(customerEmail, forKey: "customer_email")
        }
        if customerEmailVerified != nil{
            aCoder.encode(customerEmailVerified, forKey: "customer_email_verified")
        }
        if customerFullName != nil{
            aCoder.encode(customerFullName, forKey: "customer_full_name")
        }
        if customerGender != nil{
            aCoder.encode(customerGender, forKey: "customer_gender")
        }
        if customerHeight != nil{
            aCoder.encode(customerHeight, forKey: "customer_height")
        }
        if customerId != nil{
            aCoder.encode(customerId, forKey: "customer_id")
        }
        if customerMobileNumber != nil{
            aCoder.encode(customerMobileNumber, forKey: "customer_mobile_number")
        }
        if customerNotification != nil{
            aCoder.encode(customerNotification, forKey: "customer_notification")
        }
        if customerNotifyRemainder != nil{
            aCoder.encode(customerNotifyRemainder, forKey: "customer_notify_remainder")
        }
        if customerProfileImage != nil{
            aCoder.encode(customerProfileImage, forKey: "customer_profile_image")
        }
        if customerSocialNetwork != nil{
            aCoder.encode(customerSocialNetwork, forKey: "customer_social_network")
        }
        if customerSocialNetworkId != nil{
            aCoder.encode(customerSocialNetworkId, forKey: "customer_social_network_id")
        }
        if customerStatus != nil{
            aCoder.encode(customerStatus, forKey: "customer_status")
        }
        if customerUnits != nil{
            aCoder.encode(customerUnits, forKey: "customer_units")
        }
        if customerUserName != nil{
            aCoder.encode(customerUserName, forKey: "customer_user_name")
        }
        
        if customerIsadmin != nil{
            aCoder.encode(customerIsadmin, forKey: "is_admin")
        }
        if customerWeight != nil{
            aCoder.encode(customerWeight, forKey: "customer_weight")
        }
        
    }

}
