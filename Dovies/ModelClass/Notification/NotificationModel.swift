

import Foundation


class NotificationModel : NSObject, NSCoding{
    
    var notificationDate : String!
    var notificationId : String!
    var notificationMessage : String!
    var attrubutedMessage:String!
    var notificationStatus : String!
    var notificationCode : String!
    var notificationImage : String!
    var notificationTitle : String!
    var notificationConnectionId : String!
    var notificationMyLike : String!
    var notificationLikeCount : String!
    var notificationType : String!
    var notificationBtnTitle : String!
    var notificationBtnUrl : String!
    var notificationModuleId : String!
    var notificationModuleName : String!
    var notificationCommentCount : String!

    
    var commentList : [CommentlistModel]!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        
        commentList = [CommentlistModel]()
        if let commentListArray = dictionary["notification_comment"] as? [[String:Any]]{
            for dic in commentListArray{
                let value = CommentlistModel(fromDictionary: dic)
                commentList.append(value)
            }
        }
        
        notificationCommentCount = dictionary["notification_commentCount"] as? String ?? ""

        notificationModuleId = dictionary["notification_module_id"] as? String ?? ""
        notificationModuleName = dictionary["notification_module_name"] as? String ?? ""
        notificationMyLike = dictionary["notification_myLike"] as? String ?? ""
        
        notificationLikeCount = dictionary["notification_likeCount"] as? String ?? ""
        
        notificationConnectionId = dictionary["notification_connection_id"] as? String ?? ""
        notificationType = dictionary["notification_type"] as? String ?? ""
        
        notificationBtnTitle = dictionary["notification_button_title"] as? String ?? ""
        notificationBtnUrl = dictionary["notification_button_url"] as? String ?? ""
        
        notificationCode = dictionary["notification_code"] as? String ?? ""
        
        notificationImage = dictionary["notification_image"] as? String ?? ""
        
        notificationTitle = dictionary["notification_title"] as? String ?? ""
        
        notificationDate = dictionary["notification_date"] as? String ?? ""
        notificationId = dictionary["notification_id"] as? String ?? ""
        notificationMessage = dictionary["notification_message"] as? String ?? ""
        attrubutedMessage = notificationMessage
        
        do {
            let finalstring    =  try NSAttributedString(data: notificationMessage.data(using: String.Encoding.utf8)!, options: [.documentType: NSAttributedString.DocumentType.html,.characterEncoding: String.Encoding.utf8.rawValue],documentAttributes: nil)
            
            attrubutedMessage = finalstring.string
            
        } catch {
            print("Cannot convert html string to attributed string: \(error)")
        }
    
        notificationStatus = dictionary["notification_status"] as? String ?? ""
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        
        if commentList != nil{
            var dictionaryElements = [[String:Any]]()
            for CommentListElement in commentList {
                dictionaryElements.append(CommentListElement.toDictionary())
            }
            dictionary["notification_comment"] = dictionaryElements
        }
        
        
        if notificationCommentCount != nil{
            dictionary["notification_commentCount"] = notificationCommentCount
        }
        
        
        if notificationMyLike != nil{
            dictionary["notification_myLike"] = notificationMyLike
        }
        
        if notificationLikeCount != nil{
            dictionary["notification_likeCount"] = notificationLikeCount
        }
        
        if notificationModuleId != nil{
            dictionary["notification_module_id"] = notificationModuleId
        }
        if notificationModuleName != nil{
            dictionary["notification_module_name"] = notificationModuleName
        }
        
        
        if notificationBtnTitle != nil{
            dictionary["notification_button_title"] = notificationBtnTitle
        }
        if notificationBtnUrl != nil{
            dictionary["notification_button_url"] = notificationBtnUrl
        }
        
        if notificationConnectionId != nil{
            dictionary["notification_connection_id"] = notificationConnectionId
        }
        if notificationType != nil{
            dictionary["notification_type"] = notificationType
        }
        if notificationCode != nil{
            dictionary["notification_code"] = notificationCode
        }
        if notificationImage != nil{
            dictionary["notification_image"] = notificationImage
        }
        if notificationTitle != nil{
            dictionary["notification_title"] = notificationTitle
        }
        
        if notificationDate != nil{
            dictionary["notification_date"] = notificationDate
        }
        if notificationId != nil{
            dictionary["notification_id"] = notificationId
        }
        if notificationMessage != nil{
            dictionary["notification_message"] = notificationMessage
        }
        if notificationStatus != nil{
            dictionary["notification_status"] = notificationStatus
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        
        commentList = aDecoder.decodeObject(forKey :"notification_comment") as? [CommentlistModel]

        notificationCommentCount = aDecoder.decodeObject(forKey: "notification_commentCount") as? String

        
        notificationMyLike = aDecoder.decodeObject(forKey: "notification_myLike") as? String
        
        notificationLikeCount = aDecoder.decodeObject(forKey: "notification_likeCount") as? String
        
        notificationModuleId = aDecoder.decodeObject(forKey: "notification_module_id") as? String
        notificationModuleName = aDecoder.decodeObject(forKey: "notification_module_name") as? String
        
        
        
        notificationBtnTitle = aDecoder.decodeObject(forKey: "notification_button_title") as? String
        notificationBtnUrl = aDecoder.decodeObject(forKey: "notification_button_url") as? String
        
        notificationConnectionId = aDecoder.decodeObject(forKey: "notification_connection_id") as? String
        notificationType = aDecoder.decodeObject(forKey: "notification_type") as? String
        
        notificationDate = aDecoder.decodeObject(forKey: "notification_date") as? String
        notificationId = aDecoder.decodeObject(forKey: "notification_id") as? String
        notificationMessage = aDecoder.decodeObject(forKey: "notification_message") as? String
        notificationStatus = aDecoder.decodeObject(forKey: "notification_status") as? String
        
        notificationCode = aDecoder.decodeObject(forKey: "notification_code") as? String
        notificationImage = aDecoder.decodeObject(forKey: "notification_image") as? String
        notificationTitle = aDecoder.decodeObject(forKey: "notification_title") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        
        
        if commentList != nil{
            aCoder.encode(commentList, forKey: "notification_comment")
        }
        
        if notificationCommentCount != nil{
            aCoder.encode(notificationCommentCount, forKey: "notification_commentCount")
        }
        
        if notificationMyLike != nil{
            aCoder.encode(notificationMyLike, forKey: "notification_myLike")
        }
        if notificationLikeCount != nil{
            aCoder.encode(notificationLikeCount, forKey: "notification_likeCount")
        }
        
        
        if notificationModuleId != nil{
            aCoder.encode(notificationModuleId, forKey: "notification_module_id")
        }
        if notificationModuleName != nil{
            aCoder.encode(notificationModuleName, forKey: "notification_module_name")
        }
        
        if notificationBtnTitle != nil{
            aCoder.encode(notificationBtnTitle, forKey: "notification_button_title")
        }
        if notificationBtnUrl != nil{
            aCoder.encode(notificationBtnUrl, forKey: "notification_button_url")
        }
        
        if notificationConnectionId != nil{
            aCoder.encode(notificationConnectionId, forKey: "notification_connection_id")
        }
        if notificationType != nil{
            aCoder.encode(notificationType, forKey: "notification_type")
        }
        
        if notificationDate != nil{
            aCoder.encode(notificationDate, forKey: "notification_date")
        }
        if notificationId != nil{
            aCoder.encode(notificationId, forKey: "notification_id")
        }
        if notificationMessage != nil{
            aCoder.encode(notificationMessage, forKey: "notification_message")
        }
        if notificationStatus != nil{
            aCoder.encode(notificationStatus, forKey: "notification_status")
        }
        
        if notificationCode != nil{
            aCoder.encode(notificationCode, forKey: "notification_code")
        }
        if notificationImage != nil{
            aCoder.encode(notificationImage, forKey: "notification_image")
        }
        if notificationTitle != nil{
            aCoder.encode(notificationTitle, forKey: "notification_title")
        }
    }
}

