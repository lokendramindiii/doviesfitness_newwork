//
//  ProgramDetails.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

struct ProgramDetails {
    var programAccessLevel : String!
    var programAmount : String!
    var programDescription : String!
    var programEquipments : String!
    var programFullImage : String!
    var programGoodFor : String!
    var programId : String!
    var programImage : String!
    var programName : String!
    var programPackageCode : String!
    var programShareUrl : String!
    var programWeekCount : String!
	var programFavourite: Bool!
    var accessLevel : String!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        programAccessLevel = dictionary["program_access_level"] as? String ?? ""
        programAmount = dictionary["program_amount"] as? String ?? ""
        programDescription = dictionary["program_description"] as? String ?? ""
        programEquipments = dictionary["program_equipments"] as? String ?? ""
        programFullImage = dictionary["program_full_image"] as? String ?? ""
        programGoodFor = dictionary["program_good_for"] as? String ?? ""
        programId = dictionary["program_id"] as? String ?? ""
        programImage = dictionary["program_image"] as? String ?? ""
        programName = dictionary["program_name"] as? String ?? ""
        programPackageCode = dictionary["program_package_code"] as? String ?? ""
        programShareUrl = dictionary["program_share_url"] as? String ?? ""
        programWeekCount = dictionary["program_week_count"] as? String ?? ""
		accessLevel = dictionary["access_level"] as? String ?? ""
        
		if let favStatus = dictionary["is_program_favourite"] as? String , favStatus == "1"{
			programFavourite = true
		}else if  let dietFavStat = dictionary["is_program_favourite"] as? Bool{
			programFavourite = dietFavStat
		}else{
			programFavourite = false
		}
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if programFavourite != nil{
            dictionary["is_program_favourite"] = programFavourite
        }
        if programAccessLevel != nil{
            dictionary["program_access_level"] = programAccessLevel
        }
        if programAmount != nil{
            dictionary["program_amount"] = programAmount
        }
        if programDescription != nil{
            dictionary["program_description"] = programDescription
        }
        if programEquipments != nil{
            dictionary["program_equipments"] = programEquipments
        }
        if programFullImage != nil{
            dictionary["program_full_image"] = programFullImage
        }
        if programGoodFor != nil{
            dictionary["program_good_for"] = programGoodFor
        }
        if programId != nil{
            dictionary["program_id"] = programId
        }
        if programImage != nil{
            dictionary["program_image"] = programImage
        }
        if programName != nil{
            dictionary["program_name"] = programName
        }
        if programPackageCode != nil{
            dictionary["program_package_code"] = programPackageCode
        }
        if programShareUrl != nil{
            dictionary["program_share_url"] = programShareUrl
        }
        if programWeekCount != nil{
            dictionary["program_week_count"] = programWeekCount
        }
        if accessLevel != nil{
            dictionary["access_level"] = accessLevel
        }
        return dictionary
    }
}
