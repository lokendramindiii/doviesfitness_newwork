//
//  ExerciseLibrary+CoreDataClass.swift
//  Dovies
//
//  Created by Mindiii on 4/3/19.
//  Copyright © 2019 Hidden Brains. All rights reserved.
//

import Foundation
import CoreData

public class Exercise_library: NSManagedObject {
    
    class func saveContactInDBExerciseLibraryEntity(ExerciseLibraryArray:[ModelExerciseLibrary]){
       
        DispatchQueue.main.async {
         
        
        for obj in ExerciseLibraryArray {
            
            if someEntityExists(exercisCat_Id: "\(obj.exerciseCategoryId ?? "")") == true
            {
                print("exist")
                return
            }
            else
            {
                let url = URL(string:obj.image)
                if let data = try? Data(contentsOf: url!)
                {
                    saveImageToDB(exerciseCateory_Id: obj.exerciseCategoryId, image: data)
                }
            }
        }
    }

    }
    
    
    
    class  func someEntityExists(exercisCat_Id: String) -> Bool {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "ExerciseLibrary")
        fetchRequest.predicate = NSPredicate(format: "exerciseCategoryId = %@", exercisCat_Id)
        var results: [NSManagedObject] = []
        
        do {
            results = try APP_DELEGATE.managedObjectContext.fetch(fetchRequest)
        }
        catch {
            print("error executing fetch request: \(error)")
        }
        return results.count > 0
    }
    
 class  func saveImageToDB(exerciseCateory_Id: String, image: Data) {
        
        let context = APP_DELEGATE.managedObjectContext
        let entity = NSEntityDescription.entity(forEntityName: "ExerciseLibrary", in: context)!
        let ExerciseLibraryEntity = NSManagedObject(entity: entity, insertInto: context)
        
        ExerciseLibraryEntity.setValue(image, forKey: "exerciseLibraryImage")
        ExerciseLibraryEntity.setValue(exerciseCateory_Id, forKey: "exerciseCategoryId")
        
        do {
            try context.save()
            print("saved!")
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    class func deleteAllDataOfExerciseLibrary()
    {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ExerciseLibrary")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try APP_DELEGATE.managedObjectContext.execute(deleteRequest)
        } catch {
        }
    }
    
    
}
