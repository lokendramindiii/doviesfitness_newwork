//
//  ProgramDetailsVC.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

import NVActivityIndicatorView
//This view controller used to show selected program details
//used to purchase program
class ProgramDetailsVC: UITableViewController {

    @IBOutlet weak var tableV : UITableView!
    @IBOutlet weak var lblDescription : CustomLabel!
    @IBOutlet weak var imgProgram : UIImageView!
    @IBOutlet weak var IbViewGradient : UIView!
    @IBOutlet weak var IblblProgramName : UILabel!

    @IBOutlet weak var Img_Lock : UIImageView!

    @IBOutlet weak var lblGoodFor : UILabel!
    @IBOutlet weak var lblEquipments : UILabel!
	@IBOutlet weak var IBbtnPurchase: UIButton!
    
    var selectedProgram : Program?
    var programId: String?
    
    var programDetails : ProgramDetails?
	var favouriteButton : (UIBarButtonItem?,UIButton?)?
	var onPurchaseSuccess:(()->())?
    var isFromShared : Bool = false
    
	@IBOutlet weak var activityIndicatorView : NVActivityIndicatorView!{
		didSet{
			activityIndicatorView.padding = 20
			activityIndicatorView.color = UIColor.lightGray
			activityIndicatorView.type = .circleStrokeSpin
			activityIndicatorView.startAnimating()
		}
	}
	
    //MARK: - View Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
		//self.navigationItem.title = selectedProgram?.programName.uppercased()

        inititalUISetUp()
		callGetProgramDetailsAPI()
        
        let adjustForTabbarInsets: UIEdgeInsets = UIEdgeInsetsMake(0, 0, self.tabBarController!.tabBar.frame.height, 0)
        self.tableV.contentInset = adjustForTabbarInsets
        self.tableV.scrollIndicatorInsets = adjustForTabbarInsets
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,
                                                                        NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]

        //load data
		if programDetails == nil{
        	
		}
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
      //  self.navigationController?.navigationBar.barTintColor = UIColor.white
     //   self.navigationController?.navigationBar.titleTextAttributes = [
      //      NSAttributedStringKey.foregroundColor : UIColor.colorWithRGB(r: 34.0, g: 34.0, b: 34.0),
      //      NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]
    }
    
    //MARK: - View Controller set up methods
    func inititalUISetUp() {
        lblDescription.text = ""
	    addNavigationBackButton()
    }
    
    func loadUI(){
        guard let programInfo = programDetails else{return}
 //       self.navigationItem.title = programInfo.programName.uppercased()
//        lblDescription.text = programInfo.programDescription
        
        self.IblblProgramName.text = programInfo.programName.uppercased()
        
        let style = NSMutableParagraphStyle()
        style.alignment = .left
        style.lineSpacing = 9.0
        
        let att = [NSAttributedStringKey.font : UIFont.tamilRegular(size: 14.0), NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.5411764706, green: 0.5411764706, blue: 0.5411764706, alpha: 1), NSAttributedStringKey.paragraphStyle : style]
        let attStr = NSMutableAttributedString(string: programInfo.programDescription, attributes: att)
        lblDescription.attributedText = attStr
        
		imgProgram.sd_setImage(with: URL(string: programInfo.programImage )) { (image, error, cache, url) in
			self.activityIndicatorView.stopAnimating()
		}
        
        
        let gradientLayer:CAGradientLayer = CAGradientLayer()
        gradientLayer.frame.size =  IbViewGradient.frame.size
        gradientLayer.colors = [UIColor.clear.cgColor,UIColor.appBlackColorNew().withAlphaComponent(1.0).cgColor]
        IbViewGradient.layer.addSublayer(gradientLayer)
        
        Img_Lock.isHidden = (programInfo.programAccessLevel.uppercased() == "OPEN".uppercased() || programInfo.programAccessLevel.isEmpty) || DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName

        var txtGoodFor = programInfo.programGoodFor ?? ""
        txtGoodFor = txtGoodFor.replacingOccurrences(of: "|", with: ",", options: .literal, range: nil)

        
        var txtEquipments = programInfo.programEquipments ?? ""
        txtEquipments = txtEquipments.replacingOccurrences(of: "|", with: ",", options: .literal, range: nil)
        
        let style1 = NSMutableParagraphStyle()
        style1.alignment = .center
        style1.lineSpacing = 9.0
        
        let att1 = [NSAttributedStringKey.font : UIFont.tamilRegular(size: 14.0), NSAttributedStringKey.foregroundColor : #colorLiteral(red: 0.5411764706, green: 0.5411764706, blue: 0.5411764706, alpha: 1), NSAttributedStringKey.paragraphStyle : style1]
        
        let attrGoodFor = NSMutableAttributedString(string: txtGoodFor, attributes: att1);
        lblGoodFor.attributedText = attrGoodFor
        
        let attrEquipments = NSMutableAttributedString(string: txtEquipments, attributes: att1);
        lblEquipments.attributedText = attrEquipments
        
        //lblGoodFor.text = programInfo.programGoodFor
        //lblEquipments.text = programInfo.programEquipments
        tableV.hideLoader()
        tableV.reloadData()
		addRightNavigationBarItems()
        
        IBbtnPurchase.addTarget(self, action: #selector(purchaseTapped), for: .touchUpInside)
		if programInfo.programAccessLevel.uppercased() == "OPEN" || DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName{
			IBbtnPurchase.setTitle("ADD TO MY PLAN", for: .normal)
            if isFromShared{
                UIAlertController.showAlert(in: self, withTitle: nil, message: "Do you want to add this to your Plans?", cancelButtonTitle: "No", destructiveButtonTitle: "Yes", otherButtonTitles: nil, tap: { (alertController, action, selectedIndex) in
                    if selectedIndex == 1{
                        self.purchaseTapped()
                    }
                })
                
            }
		}else{
            guard let programD = programDetails else{return}
            switch programD.accessLevel.uppercased(){
            case "Subscribers".uppercased():
                IBbtnPurchase.setTitle("Subscribe".uppercased(), for: .normal)
                if isFromShared{
                    UIAlertController.showAlert(in: self, withTitle: nil, message: "Do you want to subscribe to access this to Plan?", cancelButtonTitle: "No", destructiveButtonTitle: "Yes", otherButtonTitles: nil, tap: { (alertController, action, selectedIndex) in
                        if selectedIndex == 1{
                            self.purchaseTapped()
                        }
                    })
                    
                }
            case "PAID":
             //   IBbtnPurchase.setTitle("purchase".uppercased(), for: .normal)
                
                IBbtnPurchase.setTitle("Upgrade to view plan".uppercased(), for: .normal)
                
                if isFromShared{
                    UIAlertController.showAlert(in: self, withTitle: nil, message: "Do you want to Purchase this Plan?", cancelButtonTitle: "No", destructiveButtonTitle: "Yes", otherButtonTitles: nil, tap: { (alertController, action, selectedIndex) in
                        if selectedIndex == 1{
                            self.purchaseTapped()
                        }
                    })
                    
                }
            default:
                break
            }
			
		}
        self.view.layoutSubviews()
    }
	
    @objc func purchaseTapped(){
         guard let programInfo = programDetails else{return}
        if programInfo.programAccessLevel.uppercased() == "OPEN" || DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName{
            callAddToMyPlansAPI()
        }else{
            guard let programD = programDetails else{return}
            switch programD.accessLevel.uppercased(){
            case "Subscribers".uppercased():
                redirectToUpdateToPremium()
            case "PAID":
                purchaseProductWith(productId: programD.programPackageCode)
            default:
                break
            }
        }
    }
    
    func redirectToUpdateToPremium(){
        if let upgradeVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "UpgradeToPremiumViewController") as? UpgradeToPremiumViewController{
            upgradeVC.onPurchaseSuccess = {
                self.programDetails?.programAccessLevel = "OPEN"
                self.programDetails?.accessLevel = "FREE"
                self.onPurchaseSuccess?()
                self.loadUI()
            }
            self.navigationController?.pushViewController(upgradeVC, animated: true)
        }
    }
    
    func purchaseProductWith(productId : String){
        
        if productId.isEmpty{
            GlobalUtility.showSimpleAlert(viewController: self, message: "Package id is blank")
            return
        }
        GlobalUtility.showActivityIndi(viewContView: self.view)
        // Change by arvind sir
        PurchaseHelper.payForPackage(packageId: productId, packageMasterId: "", onSuccss: { (purchaseDetails,  encodedString , receipt_Data) in
            self.callpaymentSuccessAPI(with: purchaseDetails, encodedString: encodedString)
        }) { (alert) in
            GlobalUtility.hideActivityIndi(viewContView: self.view)
            self.present(alert, animated: true, completion: nil)
        }
    }
	
	func addRightNavigationBarItems() {
		favouriteButton = GlobalUtility.getNavigationRightButtonItem(target: self, selector: #selector(self.favouriteTapped(sender:)), imageName: "add_favorite_btn_s33")
		if  let shareButton = GlobalUtility.getNavigationRightButtonItem(target: self, selector: #selector(self.shareTapped(sender:)), imageName: "btn_shareWhite").0, let navFavButton = favouriteButton?.0{
		//	self.navigationItem.rightBarButtonItems = [ navFavButton, shareButton]
			self.navigationItem.rightBarButtonItems = [ shareButton]
			if let programDetail = programDetails, let navFavButton = favouriteButton?.1{
				navFavButton.isSelected = programDetail.programFavourite
			}
		}
	}
    
    func addNavigationBackButton() {
        self.navigationItem.leftBarButtonItems = GlobalUtility.getNavigationButtonItems(target: self, selector: #selector(self.btnBackTapped(sender:)), imageName: "btn_top_backWhite")
        self.navigationItem.leftBarButtonItem?.tintColor = UIColor.black
    }
    
    @objc func btnBackTapped(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
	
	@objc func shareTapped(sender : UIBarButtonItem){
//        guard let programDetail = programDetails else{return}
        var programID = ""
        if let pId = programId{
            programID = pId
        }else if let sProgram = selectedProgram{
            programID = sProgram.programId
        }
//        DoviesGlobalUtility.generateBranchIoUrl(with: ModuleName.program, moduleId: programID, completionHandler: { (urlString) in
//            DoviesGlobalUtility.showShareSheet(on: self, shareText: urlString)
//        })
//
        DoviesGlobalUtility.showShareSheet(on: self, shareText: self.programDetails?.programShareUrl ?? "")

	}
	
	@objc func favouriteTapped(sender : UIBarButtonItem){
		changeFavouriteState()
		callFavouriteAPI()
	}
	
	func changeFavouriteState(){
		if let favButton = favouriteButton?.1{
			favButton.isSelected = !favButton.isSelected
			programDetails!.programFavourite = programDetails!.programFavourite
		}
	}
    
    //MARK: - didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.0001
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0001
    }
    
}


//API Calls
extension ProgramDetailsVC{
    
    /// This method used to get diet plans list
    func callGetProgramDetailsAPI(){
        
        var programID = ""
        if let pId = programId{
          programID = pId
        }else if let sProgram = selectedProgram{
            programID = sProgram.programId
        }
        
        guard let cUser = DoviesGlobalUtility.currentUser else{return}
        var parameters = [String : Any]()
        parameters[WsParam.authCustomerId] =  cUser.customerAuthToken
        parameters[WsParam.programId] = programID
        
        DoviesWSCalls.callGetProgramDetailsAPI(dicParam: parameters, onSuccess: { [weak self](success, programInfo, message) in
            guard let weakSelf = self else{return}
            
            if success{
                weakSelf.programDetails  = programInfo
				self?.loadUI()
            }else{
                //DoviesGlobalUtility.showSimpleAlert(viewController: weakSelf, message: message, completion: nil)
                weakSelf.activityIndicatorView.stopAnimating()
                    GlobalUtility.showToastMessage(msg: "No longer available.")
                self?.navigationController?.popViewController(animated: true)
            }
            
        }) {(error) in
			
        }
    }
	
    //This method used to call program favourite api
    //Used to add program to favourite list
	func callFavouriteAPI(){
		
		var programID = ""
		if let pId = programId{
			programID = pId
		}else if let sProgram = selectedProgram{
			programID = sProgram.programId
		}
		
		guard let cUser = DoviesGlobalUtility.currentUser else{return}
		var parameters = [String : Any]()
		parameters[WsParam.authCustomerId] =  cUser.customerAuthToken
		parameters[WsParam.moduleId] = programID
		parameters[WsParam.moduleName] = ModuleName.program
		
		DoviesWSCalls.callFavouriteAPI(dicParam: parameters, onSuccess: {[weak self] (success, message, response) in
			guard let weakSelf = self else{return}
			if !success, weakSelf.programDetails != nil{
				weakSelf.changeFavouriteState()
			}
		}) { [weak self] (error) in
			guard let weakSelf = self else{return}
			if weakSelf.programDetails != nil{
				weakSelf.changeFavouriteState()
			}
		}
	}
    
    /**
     This method used to add program to my plans list
     */
    func callAddToMyPlansAPI(){
        var programID = ""
        if let pId = programId{
            programID = pId
        }else if let sProgram = selectedProgram{
            programID = sProgram.programId
        }
        
        guard let cUser = DoviesGlobalUtility.currentUser else{return}
        var parameters = [String : Any]()
        parameters[WsParam.authCustomerId] =  cUser.customerAuthToken
        parameters[WsParam.programId] = programID
        
        DoviesWSCalls.callAddToMyPlanAPI(dicParam: parameters, onSuccess: { (success, response, message) in
            if success{
				GlobalUtility.showToastMessage(msg: "Done")
				let myPlanVC = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "MyPlanVC") as! MyPlanVC
				self.navigationController?.pushViewController(myPlanVC, animated: true)
            }else{
                DoviesGlobalUtility.showSimpleAlert(viewController: self, message: message, completion: nil)
            }
            
        }) { (error) in
            
        }
        
        
    }
    
    func callpaymentSuccessAPI(with paymentModel: PurchaseDetails, encodedString : String){
        var programID = ""
        if let pId = programId{
            programID = pId
        }else if let sProgram = selectedProgram{
            programID = sProgram.programId
        }
        
        var tansactionDetailsDict = [String: Any]()
        tansactionDetailsDict[WsParam.productId] = paymentModel.productId
        tansactionDetailsDict[WsParam.quantity] = paymentModel.quantity
        if let tID = paymentModel.transaction.transactionIdentifier{
            tansactionDetailsDict[WsParam.transactionId] = tID
        }
        tansactionDetailsDict[WsParam.encodedKey] = encodedString
        
        let data = try! JSONSerialization.data(withJSONObject:tansactionDetailsDict, options: [ ])
        
        guard let cUser = DoviesGlobalUtility.currentUser, let jsonString = String(data: data, encoding: .utf8) else{return}
        var parameters = [String : Any]()
        parameters[WsParam.authCustomerId] =  cUser.customerAuthToken
        parameters[WsParam.productId] = programID
        parameters[WsParam.productType] = "PROGRAM"
        parameters[WsParam.transactionDetail] = jsonString
        DoviesWSCalls.callProductPurchaseSuccessAPI(dicParam: parameters, onSuccess: {[weak self] (success, message) in
            guard let weakSelf = self else{return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
            if success{
                weakSelf.programDetails?.programAccessLevel = "OPEN"
                weakSelf.IBbtnPurchase.setTitle("ADD TO MY PLAN", for: .normal)
            }
            if let msg = message{
                DoviesGlobalUtility.showSimpleAlert(viewController: weakSelf, message: msg)
            }
        }) { (error) in
            GlobalUtility.hideActivityIndi(viewContView: self.view)
        }
    }
    
}




