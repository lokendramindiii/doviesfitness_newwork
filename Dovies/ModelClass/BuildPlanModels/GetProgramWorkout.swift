//
//	GetProgramWorkout.swift
//
//	Create by Praveen on 24/3/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ProgramWorkout{

    var weeksData : [[WorkoutDayModel]]!

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		weeksData = [[WorkoutDayModel]]()
            
		if let week1Array = dictionary["Week1"] as? [[String:Any]]{
            var arrWeek = [WorkoutDayModel]()
			for dic in week1Array{
				let value = WorkoutDayModel(fromDictionary: dic)
				arrWeek.append(value)
			}
            weeksData.append(arrWeek)
		}
		
		if let week2Array = dictionary["Week2"] as? [[String:Any]]{
            var arrWeek = [WorkoutDayModel]()
            for dic in week2Array{
                let value = WorkoutDayModel(fromDictionary: dic)
                arrWeek.append(value)
            }
            weeksData.append(arrWeek)
		}
		
		if let week3Array = dictionary["Week3"] as? [[String:Any]]{
            var arrWeek = [WorkoutDayModel]()
            for dic in week3Array{
                let value = WorkoutDayModel(fromDictionary: dic)
                arrWeek.append(value)
            }
            weeksData.append(arrWeek)
		}
        
        if let week4Array = dictionary["Week4"] as? [[String:Any]]{
            var arrWeek = [WorkoutDayModel]()
            for dic in week4Array{
                let value = WorkoutDayModel(fromDictionary: dic)
                arrWeek.append(value)
            }
            weeksData.append(arrWeek)
        }
	}
    
}
