//
//  BuildDietPlanVC.swift
//  Dovies
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

//This view cotnroller used to add dietplan
class BuildDietPlanVC: BaseViewController {

    @IBOutlet weak var addPlanStackView     : UIStackView!
    @IBOutlet weak var dietCollectionView   : UICollectionView!
    @IBOutlet weak var btnAddDietPlan       : UIButton!
    @IBOutlet weak var lblNoData            : UILabel!
    

    @IBOutlet weak var IbConstAddB          : NSLayoutConstraint!

    
    var pullToRefreshCtrl                   : UIRefreshControl!
    static let cellIndentifier              : String = "DietPlanCollectionCell"
    static let cellPadding                  : CGFloat = 5.0
   // let cellSizeValue                       : CGFloat  = (ScreenSize.width - (BuildDietPlanVC.cellPadding))/2  - 30 -11-2019
    
    let cellSizeValue : CGFloat  = (ScreenSize.width - 62)/2

    var placeholderCellsCount               : Int = 0
    var isFromMyPlan                        : Bool = false
    var isFromEditAdmin                     : Bool = false

    var buildPlanModel                      : BuildPlanModel?
    var arrDietPlans                        : [ProgramDietPlan]!
    
    var planUpdated : ((ProgramDietPlan, Bool)->())?
    
    //MARK: - ViewLifeCycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 20
        layout.minimumLineSpacing = 20
        dietCollectionView.collectionViewLayout = layout

        if isFromEditAdmin
        {
         self.dietCollectionView.backgroundColor = UIColor.black
            self.view.backgroundColor = UIColor.black
            lblNoData.textColor = UIColor.white

        }
        inititalUISetUp()
    }
   
    //MARK: Instance methods
    
    override func inititalUISetUp() {
        guard let plan = buildPlanModel else{return}
        arrDietPlans = plan.programDietPlans
        addPlanStackView.isHidden = arrDietPlans.count > 0
        btnAddDietPlan.isHidden = arrDietPlans.count == 0
        if arrDietPlans.count == 0{
        }
        setBottomOfAddButton()
        
        if isFromMyPlan{
            addPlanStackView.isHidden = true
            btnAddDietPlan.isHidden = true
            lblNoData.isHidden = arrDietPlans.count > 0
        }
        dietCollectionView.register(UINib(nibName: BuildDietPlanVC.cellIndentifier, bundle: nil), forCellWithReuseIdentifier: BuildDietPlanVC.cellIndentifier)

        dietCollectionView.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
        
        planUpdated = { [weak self](dietPlan, isAdded) in
            guard let weakSelf = self else{return}
             BuildPlanViewController.isDataChanged = true
            if isAdded{
                weakSelf.arrDietPlans.append(dietPlan)
                weakSelf.buildPlanModel?.programDietPlans = weakSelf.arrDietPlans
            }else{
                if let id = dietPlan.programDietId{
                    if let plan = weakSelf.arrDietPlans.first(where: { $0.programDietId == id }) , let delIndex = weakSelf.arrDietPlans.index(of: plan){
                        weakSelf.arrDietPlans.remove(at: delIndex)
                    }
                }
            }
            weakSelf.addPlanStackView.isHidden = weakSelf.arrDietPlans.count > 0
            weakSelf.btnAddDietPlan.isHidden = weakSelf.arrDietPlans.count == 0
            weakSelf.setBottomOfAddButton()
            weakSelf.dietCollectionView.reloadData()
        }
    }

    func setBottomOfAddButton(){
        switch arrDietPlans.count {
        case 0,1,2:
            IbConstAddB.constant =  ScreenSize.height/2
            break
        case 3,4 :
            IbConstAddB.constant = (ScreenSize.height/3) + 20
        default:
            IbConstAddB.constant = 80
        }
    }
    
    
    override func  viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        if !isFromMyPlan
        {
        self.navigationController?.isNavigationBarHidden = false
      //  self.navigationController?.navigationBar.barTintColor = UIColor.white
            
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor : UIColor.white,
            NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]
        }
    }
    
//MARK: IBAction methods ---
    
    @IBAction func addDietPlanTapped(sender : UIButton){
        if let dietPlanVC = Storyboard.DietPlan.storyboard().instantiateViewController(withIdentifier: "DietPlanVC") as? DietPlanVC{
            dietPlanVC.programId = buildPlanModel?.customerProgramDetail[0].programId
            dietPlanVC.programDietPlans = arrDietPlans
            dietPlanVC.isFromBuildPlan = true
            self.navigationController?.pushViewController(dietPlanVC, animated: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension BuildDietPlanVC : UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrDietPlans.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DietPlanVC.cellIndentifier, for: indexPath) as? DietPlanCollectionCell else{return UICollectionViewCell()}
        if indexPath.item < arrDietPlans.count{
            cell.isFromEditAdmin = isFromEditAdmin
            cell.setCellForBuildPlan(plan : arrDietPlans[indexPath.item])
        }
		if isFromMyPlan == false{
			cell.btnMore.setImage(UIImage(named: "delete_btn_s27b_h"), for: .normal)
			cell.btnMore.isHidden = false
			cell.btnMore.backgroundColor = UIColor.white
			cell.btnMore.layer.cornerRadius = cell.btnMore.bounds.width / 2
			cell.btnMore.layer.borderColor = UIColor.black.cgColor
			cell.btnMore.layer.borderWidth = 1.0
			cell.btnMore.alpha = 0.7
			cell.moreButtonDeleteAction = { [weak self] in
				guard let weakSelf = self else{return}
					BuildPlanViewController.isDataChanged = true
					weakSelf.arrDietPlans.remove(at: indexPath.row)
					collectionView.performBatchUpdates({() -> Void in
						collectionView.deleteItems(at: [IndexPath(item: indexPath.row, section: 0)])
					}, completion: { (success) in
						collectionView.reloadData()
					})
					weakSelf.addPlanStackView.isHidden = weakSelf.arrDietPlans.count > 0
					weakSelf.btnAddDietPlan.isHidden = weakSelf.arrDietPlans.count == 0
			}
		}
        return cell
    }
}

extension BuildDietPlanVC : UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if let pdfViewer = Storyboard.DietPlan.storyboard().instantiateViewController(withIdentifier: "DietPdfViewController") as? DietPdfViewController{
            pdfViewer.strTitle = arrDietPlans[indexPath.item].programDietName
            pdfViewer.strUrl = arrDietPlans[indexPath.item].programDietPlanPdf
            self.navigationController?.pushViewController(pdfViewer, animated: true)
        }
    }
}

extension BuildDietPlanVC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellSizeValue, height: cellSizeValue)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    //    return UIEdgeInsets(top: -5, left: 0, bottom: BuildDietPlanVC.cellPadding, right: 0)
        return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)

   //     return UIEdgeInsets(top: BuildDietPlanVC.cellPadding, left: 0, bottom: BuildDietPlanVC.cellPadding, right: 0)
    }
}

