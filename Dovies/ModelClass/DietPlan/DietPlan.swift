//
//  DietPlan.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

class DietPlan {
    var dietPlanAccessLevel : String!
    var dietPlanAmount : String!
    var dietPlanAmountDisplay : String!
    var dietPlanDescription : String!
    var dietPlanId : String!
    var dietPlanImage : String!
    var dietPlanPdf : String!
    var dietPlanShareUrl : String!
    var dietPlanTitle : String!
    var dietPlanFavStatus : Bool!
    var dietPlanFullImage : String!
	var dietAddedInMyDiet = false
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        
        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        {
            dietPlanAccessLevel = "OPEN"
        }
        else
        {
            dietPlanAccessLevel = dictionary["diet_plan_access_level"] as? String ?? ""
        }
        dietPlanAmount = dictionary["diet_plan_amount"] as? String ?? ""
        dietPlanAmountDisplay = dictionary["diet_plan_amount_display"] as? String ?? ""
        dietPlanDescription = dictionary["diet_plan_description"] as? String ?? ""
        dietPlanId = dictionary["diet_plan_id"] as? String ?? ""
        dietPlanImage = dictionary["diet_plan_image"] as? String ?? ""
        dietPlanPdf = dictionary["diet_plan_pdf"] as? String ?? ""
        dietPlanShareUrl = dictionary["diet_plan_share_url"] as? String ?? ""
        dietPlanTitle = dictionary["diet_plan_title"] as? String ?? ""
        dietPlanFullImage = dictionary["diet_plan_full_image"] as? String ?? ""
		
		if let favStatus = dictionary["is_customer_added"] as? String , favStatus == "1"{
			dietAddedInMyDiet = true
		}else if  let dietFavStat = dictionary["is_customer_added"] as? Bool{
			dietAddedInMyDiet = dietFavStat
		}else{
			dietAddedInMyDiet = false
		}
		
        if let favStatus = dictionary["diet_plan_fav_status"] as? String , favStatus == "1"{
            dietPlanFavStatus = true
        }else if  let dietFavStat = dictionary["diet_plan_fav_status"] as? Bool{
            dietPlanFavStatus = dietFavStat
        }else{
            dietPlanFavStatus = false
        }
        
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if dietPlanAccessLevel != nil{
            dictionary["diet_plan_access_level"] = dietPlanAccessLevel
        }
        if dietPlanAmount != nil{
            dictionary["diet_plan_amount"] = dietPlanAmount
        }
        if dietPlanAmountDisplay != nil{
            dictionary["diet_plan_amount_display"] = dietPlanAmountDisplay
        }
        if dietPlanDescription != nil{
            dictionary["diet_plan_description"] = dietPlanDescription
        }
        if dietPlanId != nil{
            dictionary["diet_plan_id"] = dietPlanId
        }
        if dietPlanImage != nil{
            dictionary["diet_plan_image"] = dietPlanImage
        }
        if dietPlanPdf != nil{
            dictionary["diet_plan_pdf"] = dietPlanPdf
        }
        if dietPlanShareUrl != nil{
            dictionary["diet_plan_share_url"] = dietPlanShareUrl
        }
        if dietPlanTitle != nil{
            dictionary["diet_plan_title"] = dietPlanTitle
        }
        if dietPlanFavStatus != nil{
            dictionary["diet_plan_fav_status"] = dietPlanFavStatus
        }
        if dietPlanFullImage != nil{
            dictionary["diet_plan_full_image"] = dietPlanFullImage
        }
        return dictionary
    }

}

struct DietPlanStaticData {
    var content : String!
    var title : String!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        content = dictionary["content"] as? String ?? ""
        title = dictionary["title"] as? String ?? ""
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if content != nil{
            dictionary["content"] = content
        }
        if title != nil{
            dictionary["title"] = title
        }
        return dictionary
    }
    
}
