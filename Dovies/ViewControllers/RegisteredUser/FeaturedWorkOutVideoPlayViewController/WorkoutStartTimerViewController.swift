//
//  WorkoutStartTimerViewController.swift
//  Dovies
//
//  Created by Mindiii on 11/29/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class WorkoutStartTimerViewController: UIViewController {

    
    //---Narendra for Go Timer
    var workoutStartTimer: Timer!
    var secondsForWorkoutStartTime = 6
    var isWorkoutStratTimerRunning = false
    @IBOutlet weak var lblWorkoutStartTimer: UILabel!
    @IBOutlet weak var viewWorkoutStartTimer: UIView!
    
    var timerEndCompletion: (()->())?
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scheduleWorkoutTimer()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func scheduleWorkoutTimer() {
        self.workoutStartTimer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateWorkoutSratTimer)), userInfo: nil, repeats: true)
    }
    
    
    @objc func updateWorkoutSratTimer() {
        if secondsForWorkoutStartTime == 0{
            self.isWorkoutStratTimerRunning = false
            self.workoutStartTimer.invalidate()
            self.timerEndCompletion?()
        }
            
        else if secondsForWorkoutStartTime == 1{
            secondsForWorkoutStartTime -= 1
            self.lblWorkoutStartTimer.text = "GO"
        }
        else{
            secondsForWorkoutStartTime -= 1
            self.lblWorkoutStartTimer.text = String(secondsForWorkoutStartTime)
        }
        
    }
    

   

}
