//
//  ExerciseCollectionCell.swift
//  Dovies
//
//  Created by hb on 24/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class ExerciseCollectionCell: UICollectionViewCell {

    @IBOutlet var imgBackground: UIImageView!
	@IBOutlet var IBlblExerciseName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
	func setCellUI(modelExerciseLibrary: ModelExerciseLibrary){
      //  imgBackground.sd_setImage(with:  URL(string: modelExerciseLibrary.image), placeholderImage: AppInfo.placeholderImage)
        
        imgBackground.sd_setImage(with:  URL(string: modelExerciseLibrary.image), placeholderImage: nil)

		IBlblExerciseName.text = modelExerciseLibrary.displayName
    }

}
