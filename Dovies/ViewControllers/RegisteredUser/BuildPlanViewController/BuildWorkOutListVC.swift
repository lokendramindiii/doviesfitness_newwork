//
//  BuildWorkOutListVC.swift
//  Dovies
//
//  Created by Companyname.
//  Copyright © Companyname. All rights reserved.
//

import UIKit
import SJSegmentedScrollView

class BuildWorkOutListVC: BaseViewController {

    var selectedSegment             : SJSegmentTab?
    let segmentController           : SJSegmentedViewController = SJSegmentedViewController()
    
    var myWorkoutsVC                : MyWorkoutsVC!
    var selectedWorkout             : ModelWorkoutList?
    var workoutDayModel             : WorkoutDayModel?
    var featureWorkoutPlansVC       : FeaturedWorkOutVC!
    var weekViewVC                  : WeekPlanViewController?
    
    var planID                      : String?
    var isForPop                    : Bool = true
    
    //MARK: ViewLifeCycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        inititalUISetUp()
    }
   
    override func inititalUISetUp() {
       // addNavigationBackButton()
        addNavigationWhiteBackButton()
        inititalSegmentSetUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isTranslucent = true
    }

    //MARK: Instance methods

    func inititalSegmentSetUp(){
        self.title = "Workouts".uppercased()
        segmentController.headerViewController = UIViewController()
        self.navigationItem.rightBarButtonItem = GlobalUtility.getNavigationButtonItemWhiteAndRedWith(target: self, selector: #selector(self.saveTapped), title: "Done")
        myWorkoutsVC = (Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "MyWorkoutsVC")) as! MyWorkoutsVC
        myWorkoutsVC.title = "My Workouts"
        myWorkoutsVC.isFromBuildPlan = true
        myWorkoutsVC.isPublishEnable = false
        myWorkoutsVC.workoutSelected = { (workoutModel, isCellSelected) in
			if isCellSelected == true{
            	self.selectedWorkout = workoutModel
			}
			else{
				self.selectedWorkout = nil
			}
			self.featureWorkoutPlansVC.previousSelectedIndex = nil
			self.featureWorkoutPlansVC.IBcollectionCategory.reloadData()
        }
        
        featureWorkoutPlansVC = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "FeaturedWorkOutVC") as! FeaturedWorkOutVC
        featureWorkoutPlansVC.workoutSelected = { (workoutModel, isCellSelected) in
			if isCellSelected == true{
				let wModel = workoutModel
				wModel.workoutTime = workoutModel.workoutTotalWorkTime
				self.selectedWorkout = wModel
			}
			else{
				self.selectedWorkout = nil
			}
            self.myWorkoutsVC.previousSelectedIndex = nil
            self.myWorkoutsVC.tblViewWorkout.reloadData()
        }
		featureWorkoutPlansVC.doneTap = { () in
			self.isForPop = false
			self.saveTapped()
		}
        featureWorkoutPlansVC.title = "Workout Collection"
        
        segmentController.segmentControllers = [myWorkoutsVC, featureWorkoutPlansVC]
        
        segmentController.headerViewHeight = 0
        segmentController.segmentViewHeight = 38.0
        segmentController.selectedSegmentViewHeight = 2.0
        segmentController.segmentTitleColor = UIColor.segmentTitleColor()
        segmentController.selectedSegmentViewColor = UIColor.appBlackColor()
        segmentController.segmentShadow = SJShadow.light()
        segmentController.segmentBounces = true
        segmentController.delegate = self
        segmentController.segmentTitleFont = UIFont.tamilRegular(size: 14.0)
        addChildViewController(segmentController)
        self.view.addSubview(segmentController.view)
        segmentController.didMove(toParentViewController: self)
        
    }
    
    @objc func saveTapped(){
        guard let workout = selectedWorkout else {return}
        self.weekViewVC?.reloadAfterAddedWorkout?(workout)
		if isForPop{
        	self.navigationController?.popViewController(animated: true)
		}
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}

extension BuildWorkOutListVC : SJSegmentedViewControllerDelegate {
    func didMoveToPage(_ controller: UIViewController, segment: SJSegmentTab?, index: Int) {
		if index == 0{
			self.navigationItem.rightBarButtonItem = GlobalUtility.getNavigationButtonItemWhiteAndRedWith(target: self, selector: #selector(self.saveTapped), title: "Done")
		}
		else{
			self.navigationItem.rightBarButtonItem = nil
		}
        if selectedSegment != nil {
            selectedSegment?.titleColor(.lightGray)
        }
        
        if segmentController.segments.count > 0 {
            selectedSegment = segmentController.segments[index]
            selectedSegment?.titleColor(.black)
        }
    }
}
