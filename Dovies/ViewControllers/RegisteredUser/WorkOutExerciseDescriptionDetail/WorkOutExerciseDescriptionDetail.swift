//
//  ShapeBodyExeVC.swift
//  Dovies
//
//  Created by hb on 27/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class WorkOutExerciseDescriptionDetail: BaseViewController {

    @IBOutlet var detailTextView: UITextView!
	@IBOutlet var IBlblCreator: UILabel!
	
	var exerciseDetails: ExerciseDetails?
	var modelWorkoutDetail: ModelWorkoutDetail?
	var workoutExerciseList: ModelWorkoutExerciseList?
	var faqModel : FaqModel?
    var workoutLogModel : ModelWorkoutList?
	var modelWorkoutList: ModelWorkoutList?
    
    
    @IBOutlet weak var constintHeight : NSLayoutConstraint!
    
	var isForWorkOut:(() -> ())?
    
    //MARK: ViewLifeCycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
		self.navigationController?.isNavigationBarHidden = false
		self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false

        detailTextView.isEditable = false
		self.addCancelBackButton()
		
		if workoutExerciseList != nil{
            self.navigationItem.title = workoutExerciseList?.workoutExerciseName
            if let desc = workoutExerciseList?.workoutExerciseDescription, !desc.isEmpty
			{
                detailTextView.text = desc
            }
			else
			{
                detailTextView.text = "No description available"
            }
			IBlblCreator.text = ""
		}
		else if modelWorkoutDetail != nil{
            self.navigationItem.title = modelWorkoutDetail?.workoutName
			
            detailTextView.text = modelWorkoutDetail?.workoutDescription
            IBlblCreator.text = modelWorkoutDetail?.creatorName
		}
		else if exerciseDetails != nil{
            self.navigationItem.title = exerciseDetails?.exerciseName
			
			if (exerciseDetails?.exerciseDescription.isEmpty)!{
				detailTextView.text = "No description available"
			}
			else{
            	detailTextView.text = exerciseDetails?.exerciseDescription
			}
			IBlblCreator.text = ""
		}
		else if modelWorkoutList != nil{
			self.navigationItem.title = modelWorkoutList?.workoutName
			
			if (modelWorkoutList?.workoutDescription.isEmpty)!{
				detailTextView.text = "No description available"
			}
			else{
				detailTextView.text = modelWorkoutList?.workoutDescription
			}
			IBlblCreator.text = ""
		}
        
        if let model = faqModel{
            detailTextView.text = model.faqAnswer
        }
        if let model = workoutLogModel{
            IBlblCreator.text = "Note"
            if model.workoutNote.isEmpty{
                detailTextView.text = "No description available"
            }else{
                detailTextView.text = model.workoutNote
            }
            
        }
		
		let attr: [NSAttributedStringKey: Any] = [NSAttributedStringKey.font :UIFont.tamilRegular(size: 14.0)]//, NSAttributedStringKey.kern : 1.5
		let attrStrComment1 = NSMutableAttributedString(string: detailTextView.text, attributes: attr)
		let paragraphStyle = NSMutableParagraphStyle()
		paragraphStyle.lineSpacing = 10.0
        attrStrComment1.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attrStrComment1.length))
        
        attrStrComment1.addAttribute(NSAttributedStringKey.kern, value: CGFloat(1.0), range: NSRange(location: 0, length: attrStrComment1.length))

		
		detailTextView.attributedText = attrStrComment1

        self.view.addTapGesture(target: self, action: #selector(self.dismissView))
        
        constintHeight.constant = 480*CGRect.heightRatio
        detailTextView.layoutIfNeeded()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if detailTextView.frame.size.height >= 460*CGRect.heightRatio{
            detailTextView.isScrollEnabled = true
        }else{
            detailTextView.isScrollEnabled = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: Instance methods
    
	func addCancelBackButton() {
		self.navigationItem.leftBarButtonItems = GlobalUtility.getNavigationButtonItems(target: self, selector: #selector(self.btnCancelTapped(sender:)), imageName: "icon_close")
		self.navigationItem.leftBarButtonItem?.tintColor = UIColor.black
    }
    
    //MARK: IBAction methods
    
    @objc func dismissView(){
        isForWorkOut?()
        self.dismiss(animated: true, completion: nil)
    }
	
    @objc func btnCancelTapped(sender: UIButton) {
        isForWorkOut?()
        _ = self.navigationController?.popViewController(animated: false)
        
    }
    
    @IBAction func closeTapped(_ sender : UIButton){
        isForWorkOut?()
        self.dismiss(animated: true, completion: nil)
    }
}
