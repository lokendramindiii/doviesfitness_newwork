//
//  FeaturesVC.swift
//  Dovies
//
//  Created by hb on 27/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class FeaturesVC: BaseViewController {
    
    @IBOutlet var IBFeatureTblView: UITableView!
    @IBOutlet weak var IBEmptyData: UIView!
    
    var pullToRefreshCtrl:UIRefreshControl!
    var arrModelFeature = [ModelFeaturedWorkouts]()
    
    static let cellPadding : CGFloat = 62
    static let cellWidth : CGFloat  = (ScreenSize.width - FeaturesVC.cellPadding)/2
    
//MARK: - View Contrller life cycle
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.IBFeatureTblView.backgroundColor = UIColor.appBlackColorNew()
        
        self.setUpViewNeeds()
        self.callFeaturedAPI()
        // Do any additional setup after loading the view.
    }
    
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        

        self.IBFeatureTblView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: self.tabBarController!.tabBar.frame.height, right: 0)

        self.IBFeatureTblView.isScrollEnabled = true
        if APP_DELEGATE.isFromNotification{
            APP_DELEGATE.isFromNotification = false
            self.callFeaturedAPI()
        }
    }
  
  

    func setUpViewNeeds(){
        IBFeatureTblView.register(UINib(nibName: "NewsFeedHorizontalTableCell", bundle: nil), forCellReuseIdentifier: "NewsFeedHorizontalTableCell")
        IBFeatureTblView.register(UINib(nibName: "FeaturesVerticallTableCell", bundle: nil), forCellReuseIdentifier: "FeaturesVerticallTableCell")
        IBFeatureTblView.register(UINib(nibName: "FeaturedGroupCell", bundle: nil), forCellReuseIdentifier: "FeaturedGroupCell")
        self.pullToRefreshCtrl = UIRefreshControl()
        self.pullToRefreshCtrl.addTarget(self, action: #selector(self.pullToRefreshClick(sender:)), for: .valueChanged)
        self.IBFeatureTblView.addSubview(self.pullToRefreshCtrl)
    }
    
    func pullToTop()
    {
        IBFeatureTblView.setContentOffset(CGPoint.zero, animated: true)
    }
    
    @objc func pullToRefreshClick(sender:AnyObject)
    {
        callFeaturedAPI()
        self.pullToRefreshCtrl.endRefreshing()
    }
    
    func redirectToDetailScreen(indexPath: IndexPath){
        let featuredWorkOutDetailViewControlle = storyboard?.instantiateViewController(withIdentifier: "FeaturedWorkOutDetailViewController") as! FeaturedWorkOutDetailViewController
         featuredWorkOutDetailViewControlle.navigationItem.title = arrModelFeature[indexPath.section].workoutList[indexPath.row].workoutName
        featuredWorkOutDetailViewControlle.modelWorkoutList = arrModelFeature[indexPath.section].workoutList[indexPath.row]
        featuredWorkOutDetailViewControlle.deleteWorkout = { () in
            self.arrModelFeature[indexPath.section].workoutList.remove(at: indexPath.row)
            self.IBFeatureTblView.reloadData()
        }
        featuredWorkOutDetailViewControlle.isFromFeaturesVC = true
    self.navigationController?.pushViewController(featuredWorkOutDetailViewControlle, animated: false)

    }
}

extension FeaturesVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        self.redirectToDetailScreen(indexPath: indexPath)
        let detailsView = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "FeaturedGroupDetailsVC") as! FeaturedGroupDetailsVC
        if indexPath.section == 0{
                detailsView.selectedFeaturedGroup = arrModelFeature[indexPath.row]
        }else{
            detailsView.selectedFeaturedGroup = arrModelFeature[indexPath.row+1]
        }
        detailsView.isFromFeaturesVC = true

        self.navigationController?.pushViewController(detailsView, animated: false)

    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 60
        }else{
            return 50
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            if arrModelFeature[indexPath.section].workoutList.count > 0 {
        
                return 185

            }else{
                return ScreenSize.width
            }
        }
        return ScreenSize.width - 20
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if arrModelFeature.count > 0{
            return 2
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        if section == 0 {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 60))
            
            //            let label = UILabel(frame: CGRect(x: 20, y: 12, width: 250, height: 20))
        //    let label2 = UILabel(frame: CGRect(x: 20, y: 30, width: 250, height: 20))

            let label = UILabel(frame: CGRect(x: 20, y: 18, width: 250, height: 20))
            label.font = UIFont.tamilBold(size: 15)
            
            let label2 = UILabel(frame: CGRect(x: 20, y: 39, width: 250, height: 20))
            label2.font = UIFont.tamilRegular(size: 13)
            label.text = arrModelFeature[section].workoutGroupName
            label2.text = "All Levels"
            // label.textColor = UIColor.colorWithRGB(r: 34, g: 34, b: 34)
            label.textColor = UIColor.white
            
            //  label2.textColor = UIColor.colorWithRGB(r: 136, g: 136, b: 136)
            
            label2.textColor = UIColor.colorWithRGB(r: 183, g: 183, b: 183)
            view.addSubview(label)
            view.addSubview(label2)
            view.backgroundColor = UIColor.appBlackColorNew()
            return view
        }else{
            let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 55))
            let label = UILabel(frame: CGRect(x: 20, y: 0, width: tableView.frame.size.width - 20, height: 45))
            label.font = UIFont.tamilBold(size: 15)
            label.textColor = UIColor.white
            label.text = "All Workout Collections"
            view.addSubview(label)
            view.backgroundColor = UIColor.appBlackColorNew()
            
            return view
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension FeaturesVC: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrModelFeature.count > 0 && section == 0{
            return 1
        }
        return arrModelFeature.count - 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0,arrModelFeature[indexPath.section].workoutList.count > 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsFeedHorizontalTableCell") as? NewsFeedHorizontalTableCell else{return UITableViewCell()}
            cell.recommendedCollection.register(UINib(nibName: "FeaturesHorizontalCollCell", bundle: nil), forCellWithReuseIdentifier: "FeaturesHorizontalCollCell")
            cell.recommendedCollection.dataSource = self
            cell.recommendedCollection.delegate = self
            cell.ViewBackground.backgroundColor = UIColor.appBlackColorNew()
//            cell.btnRightIndicator.isHidden = arrModelFeature[indexPath.section].workoutList.count < 3
            
            cell.recommendedCollection.reloadData()
            return cell
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FeaturedGroupCell") as? FeaturedGroupCell else {return UITableViewCell()}
            if indexPath.section == 0,!(arrModelFeature[indexPath.section].workoutList.count > 0) {
                cell.setCellFor(model: arrModelFeature[indexPath.row])
                cell.viewBackground.backgroundColor = UIColor.appBlackColorNew()

            }else{
                
                for subview in cell.imgGroup.subviews {
                    subview.removeFromSuperview()
                }
                
                let viewGradient =  UIView(frame: CGRect(x: 0, y: (ScreenSize.width - 40)/2, width:ScreenSize.width - 40, height: (ScreenSize.width - 40)/2))
                let gradientLayer:CAGradientLayer = CAGradientLayer()
                gradientLayer.frame.size =  viewGradient.frame.size
                gradientLayer.colors = [UIColor.clear.cgColor,UIColor.appBlackColorNew().withAlphaComponent(0.5).cgColor]
                viewGradient.layer.addSublayer(gradientLayer)
                cell.imgGroup.addSubview(viewGradient)

                
                cell.setCellFor(model: arrModelFeature[indexPath.row+1])
                cell.viewBackground.backgroundColor = UIColor.appBlackColorNew()
            }
            
            return cell
        }
    }
}

extension FeaturesVC:UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return arrModelFeature[section].workoutList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeaturesHorizontalCollCell", for: indexPath) as? FeaturesHorizontalCollCell else{return UICollectionViewCell()}
          cell.setFeaturedCell(modelWorkoutList: arrModelFeature[indexPath.section].workoutList[indexPath.row])
         cell.layoutIfNeeded()

        return cell
    }
}

extension FeaturesVC: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.redirectToDetailScreen(indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width:FeaturesVC.cellWidth  - 10 ,height:FeaturesVC.cellWidth - 10 )
    }
    
}

//API Calls
extension FeaturesVC{
    /**
     This method is used fetch the featured6workout list and update the table UI.
     */
    func callFeaturedAPI(){
        DoviesWSCalls.callFeaturedWorkoutsAPI(dicParam: [WsParam.type : WsParam.featured], onSuccess: { (success, msg, arrModel) in
            if success{
                self.arrModelFeature = arrModel
                self.IBFeatureTblView.reloadData()
                self.IBEmptyData.isHidden = self.arrModelFeature.count > 0
            }else{
                self.IBEmptyData.isHidden = false
            }
        }) { (error) in
            
        }
    }
    
}
