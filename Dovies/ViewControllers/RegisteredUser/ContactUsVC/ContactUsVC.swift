//
//  ContactUsVC.swift
//  Dovies
//
//  Created by hb on 19/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import MessageUI

class ContactUsVC:BaseViewController{
    
    @IBOutlet weak var lblName: UILabel!

    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var lblSubject: UILabel!
    @IBOutlet weak var lblFeedback: UILabel!

    @IBOutlet weak var lblTitle: UILabel!
    
    @IBOutlet weak var lblSubTitle: UILabel!
    
    @IBOutlet weak var txtName: UITextField!

    @IBOutlet weak var lblNameError: UILabel!

    @IBOutlet weak var txtEmail: UITextField!
    
    @IBOutlet weak var lblEmailError: UILabel!
    
    @IBOutlet weak var btnSubject: UIButton!
    
    @IBOutlet weak var lblSubjectError: UILabel!
    
    @IBOutlet weak var txtViewFeedback: UITextView!
    
    @IBOutlet weak var lblFeeedbackError: UILabel!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    
    @IBOutlet var IBBtnFB: UIButton!
    @IBOutlet var IBBtnInstagram: UIButton!
    @IBOutlet var IBBtnHiddenBrains: UIButton!
    @IBOutlet var IBBtnDovies: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "CONTACT US"
        self.navigationItem.hidesBackButton  = true
       // self.addNavigationBackButton()
        self.addNavigationWhiteBackButton()

//        self.lblSubTitle.makeSpacingWithLeftAlignment(lineSpace: 3.0, font: UIFont.tamilRegular(size: 14), color: UIColor(r: 27, g: 27, b: 27, alpha: 1))
        setupUI()
 
    }
    
    
    func setupUI()  {
        txtViewFeedback.textContainerInset = UIEdgeInsets(top: 10, left: 5, bottom: 10, right: 5)

        self.lblSubTitle.makeLineSpacing(linespacing: 4)
        self.hideAllError()
        self.lblName.attributedText = stringWithStar(String1: "Name ", String2: "*")

        self.lblEmail.attributedText = stringWithStar(String1: "Email Address ", String2: "*")
        
        self.lblSubject.attributedText = stringWithStar(String1: "Subject / Feedback Type ", String2: "*")
        
        self.lblFeedback.attributedText = stringWithStar(String1: "Message / Feedbacks ", String2: "*")
    }
    
    func hideAllError(){
        
        self.lblNameError.text = "Please enter name"
        self.lblEmailError.text = "Please enter email address"
        self.lblSubjectError.text = "Plese select subject"
        self.lblFeeedbackError.text = "Please enter your feedback"

        self.lblNameError.isHidden = true
        self.lblEmailError.isHidden = true
        self.lblSubjectError.isHidden = true
        self.lblFeeedbackError.isHidden = true
        self.layout()
    }
    
    func layout()  {
        DispatchQueue.main.async {
            self.view.layoutIfNeeded()
            self.view.updateConstraintsIfNeeded()
        }
    }
    
    @IBAction func socialButtonTapped(_ sender : UIButton){
        
        switch sender {
        case IBBtnFB:
            GlobalUtility.openUrlFromApp(url: StaticUrls.doviesFacebook)
            
        case IBBtnInstagram:
            GlobalUtility.openUrlFromApp(url: StaticUrls.doviesInstagram)
            
        case IBBtnHiddenBrains:
            GlobalUtility.openUrlFromApp(url: StaticUrls.doviesTwitter)
            return
        case IBBtnDovies:
            GlobalUtility.openUrlFromApp(url: StaticUrls.doviesFitness)
            return
            
        default:
            return
        }
        
    }
    
    @IBAction func actionSubject(_ sender: Any) {
        self.view.endEditing(true)
        var weight = [String]()
        weight.append("Report an issue")
        weight.append("General Feedback")
        weight.append("Advertisement And Promo")

        let mcPicker = McPicker(data: [weight], selectedTitle:[""])
        mcPicker.show { [weak self](selections: [Int : String]) -> Void in
            guard self != nil else{return}
            if var weight = selections[0]{
                weight = weight.replacingOccurrences(of: " kgs", with: "")
                weight = weight.replacingOccurrences(of: " lbs", with: "")
                self?.btnSubject.setTitle(weight, for: .normal)
                
            }
        }
    }
    
    
    @IBAction func actionSubmit(_ sender: Any) {
        self.hideAllError()
        
        guard let name = txtName.text, name.count > 0 else {
            self.lblNameError.text = "Please enter name"
            self.lblNameError.isHidden = false
            layout()
            return
        }
        
        
        guard let email = txtEmail.text, email.count > 0 else {
            self.lblEmailError.text = "Please enter email address"
            self.lblEmailError.isHidden = false
            layout()
            return
        }
        
        guard self.isValidateEmail(strEmail: email) else {
            self.lblEmailError.text = "Email address is invalid"
            self.lblEmailError.isHidden = false
            layout()
            return
        }
        
        
        guard let subject = self.btnSubject.title(for: .normal), subject.count > 0 else {
            self.lblSubjectError.text = "Please select subject"
            self.lblSubjectError.isHidden = false
            layout()
            return
        }
        
        
        var result = txtViewFeedback.text
        result = result!.trim()

        if  result!.count > 0
        {
        }
        else
        {
            self.lblFeeedbackError.text = "Please enter your feedback"
            self.lblFeeedbackError.isHidden = false
            layout()
            return
        }
        
        guard let feedback = txtViewFeedback.text, feedback.count > 0 else {
            self.lblFeeedbackError.text = "Please enter your feedback"
            self.lblFeeedbackError.isHidden = false
            layout()
            return
        }
        self.callContactAPI()
        
    }
    
    func stringWithStar(String1:String ,String2:String ) -> NSMutableAttributedString {
        
        let att = NSMutableAttributedString(string: "\(String1)\(String2)");
        att.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.init(r: 27, g: 27, b: 27, alpha: 1.0), range: NSRange(location: 0, length: String1.count))
        att.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: NSRange(location: String1.count, length: String2.count))
        return att
    }
    
    func isValidateEmail(strEmail: String) -> Bool {
        let REGEX: String
        REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: strEmail)
    }
    
}

extension ContactUsVC
{
    func callContactAPI(){
        if NetworkReachabilityManager()?.isReachable == true {
            GlobalUtility.showActivityIndi(viewContView: self.view)
        }
        var dic = [String: Any]()
        
        dic[WsParam.contactName] = txtName.text
        dic[WsParam.email] = txtEmail.text
        dic[WsParam.subject] = btnSubject.titleLabel!.text
        dic[WsParam.message] = txtViewFeedback.text
        if let user = DoviesGlobalUtility.currentUser{
            dic[WsParam.cutomerId] = user.customerId
        }
        DoviesWSCalls.callContactUsAPI(dicParam: dic, onSuccess: { (success, msg) in
            GlobalUtility.hideActivityIndi(viewContView: self.view)

            if success{
                GlobalUtility.showToastMessage(msg: msg!)
                self.txtName.text = ""
                self.txtViewFeedback.text = ""
                self.txtEmail.text = ""
                self.btnSubject.setTitle("", for: .normal)
                self.navigationController?.popViewController(animated: true)

            }
        } ) { (error) in
            GlobalUtility.hideActivityIndi(viewContView: self.view)

        }
    }
}

extension String
{
    func trim() -> String
    {
        return self.trimmingCharacters(in: NSCharacterSet.whitespaces)
    }
}

/*
class ContactUsVC: BaseViewController, MFMailComposeViewControllerDelegate {
    
    @IBOutlet var lblEmail: UILabel!
    @IBOutlet var lblContact: UILabel!
    @IBOutlet var lblAddress: UILabel!
    @IBOutlet var contactStack: UIStackView!
    @IBOutlet var imgLogo: UIImageView!
    
    var backgroundImage : UIImage?
    var shadowImage : UIImage?
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ViewLifeCycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Contact Us".uppercased()
        addNavigationBackButton()
        lblEmail.text = ""
        lblContact.text = ""
        lblAddress.text = ""
        callContactAPI()
        
    }
    
    //MARK: Instance methods
    
    func stackAnimateWith(showStatus : Bool){
        UIView.animate(withDuration: 1.0) {
            self.contactStack.alpha = showStatus ? 1.0 : 0.0
            self.imgLogo.alpha = showStatus ? 1.0 : 0.0
        }
    }
    
    //MARK: IBAction methods
    
    @IBAction private func btnPhoneNumberTapped(_ sender : UIButton ){
        if let contact = lblContact.text
        {
            var strContact = contact
            strContact = contact.replacingOccurrences(of: " ", with: "")
            //GlobalUtility.openUrlFromApp(url: "tel://\(strContact)")
            let message = GlobalUtility.openCallApp(mobileNumber: strContact)
            if message.count != 0
            {
                GlobalUtility.showSimpleAlert(viewController: self, message: message, completion: nil);
            }
            else{}
        }
        else
        {
            GlobalUtility.showSimpleAlert(viewController: self, message: "Invalid phone number", completion: nil);
        }
    }
    
    @IBAction private func btnEmailNumberTapped(_ sender : UIButton ){
        let emailTitle = "Feedback"
        let toRecipents = [lblEmail.text ?? ""]
        
        if MFMailComposeViewController.canSendMail()
        {
            let mc: MFMailComposeViewController = MFMailComposeViewController()
            mc.mailComposeDelegate = self
            mc.setSubject(emailTitle)
            mc.setToRecipients(toRecipents)
            self.present(mc, animated: true, completion: nil)
        }
        else
        {
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: "Have you configured any email account in your mobile? Please check.", completion: nil);
        }
    }
    
    @IBAction private func btnLocationTapped(_ sender : UIButton ){
        let originalString = lblAddress.text ?? ""
        if let escapedString = originalString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed){
            GlobalUtility.openUrlFromApp(url: "http://maps.apple.com/?address=\(escapedString)")
        }
    }
    
    //MARK: MFMailComposeViewControllerDelegate methods
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
    
    //MARK: Webservice methods
    
    /**
     This method is used to call the contact API and updates the details into the contacts screen.
     */
    private func callContactAPI(){
        DoviesWSCalls.callContactAPI(dicParam: nil, onSuccess: { (success, modelContact, msg) in
            if success{
                self.lblEmail.text = (modelContact?.companyEmail)!
                self.lblAddress.text = modelContact?.companyAddress
                self.lblContact.text = modelContact?.companyContactNumber
                self.stackAnimateWith(showStatus : true)
            }
        }) { (error) in
            
        }
    }
}
 */
