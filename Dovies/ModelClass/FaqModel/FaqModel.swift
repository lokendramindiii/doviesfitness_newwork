

import Foundation


class FaqModel {

	var faqAnswer : String!
	var faqId : String!
	var faqQuestion : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		faqAnswer = dictionary["faq_answer"] as? String ?? ""
		faqId = dictionary["faq_id"] as? String ?? ""
		faqQuestion = dictionary["faq_question"] as? String ?? ""
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if faqAnswer != nil{
			dictionary["faq_answer"] = faqAnswer
		}
		if faqId != nil{
			dictionary["faq_id"] = faqId
		}
		if faqQuestion != nil{
			dictionary["faq_question"] = faqQuestion
		}
		return dictionary
	}

}
