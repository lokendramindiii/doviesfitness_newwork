//
//  CollectionThumbCell.swift
//  CollectionViewPhotoGellery
//
//  Created by Narendra Goojer on 30/09/18.
//  Copyright © 2018 Narendra Goojer. All rights reserved.
//

import UIKit

class CollectionThumbCell: UICollectionViewCell {
    
    @IBOutlet weak var image: UIImageView!
    
    override func awakeFromNib() {
        image.layer.cornerRadius = 5
        image.layer.masksToBounds = true
    }
    
    func loadCellUI(model: ModelWorkoutLogListImage) {
       image.sd_setImage(with: URL(string: model.logImageURL), placeholderImage: AppInfo.placeholderImage)
        
    }
}
