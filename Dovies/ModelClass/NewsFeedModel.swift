//
//	NewsFeedModel.swift
//
//	Create by CompanyName
//	Copyright © CompanyName. All rights reserved.

import Foundation


class NewsFeedModel : NSObject, NSCoding{
    
    var newsDescription : String!
    var newsFavStatus : Bool!
    var newsId : String!
    var newsImage : String!
    var newsLikeStatus : Bool!
    var newsMediaType : String!
    var newsModuleId  : String!
    var newsModuleType : String!
    var newsPostedDate : String!
    var newsPostedDays : String!
    var newsShareUrl : String!
    var newsTitle : String!
    var newsVideo : String!
    var newsCommentAllow : Bool!
    var workoutAccessLevel : String!
    var isNew : Bool!
    var newsImageRatio :String!
    var feedCommentsCount : String!
    var customerLikes : String!
    var newsCreatorName : String!
    var newsCreatorProfileImage : String!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        if let new = dictionary["is_new"] as? String , new == "1"{
            isNew = true
        }else if  let newStatus = dictionary["is_new"] as? Bool{
            isNew = newStatus
        }else{
            isNew = false
        }
        newsDescription = dictionary["news_description"] as? String ?? ""
       
        workoutAccessLevel = dictionary["workout_access_level"] as? String ?? ""
        
        newsId = dictionary["news_id"] as? String ?? ""
        newsImage = dictionary["news_image"] as? String ?? ""
        if let likeStatus = dictionary["news_like_status"] as? String , likeStatus == "1"{
            newsLikeStatus = true
        }else if  let bLikeStatus = dictionary["news_like_status"] as? Bool{
            newsLikeStatus = bLikeStatus
        }else{
            newsLikeStatus = false
        }
        
        if let favStatus = dictionary["news_fav_status"] as? String , favStatus == "1"{
            newsFavStatus = true
        }else if  let bFavStatus = dictionary["news_fav_status"] as? Bool{
            newsFavStatus = bFavStatus
        }else{
            newsFavStatus = false
        }
        
        if let commentStatus = dictionary["news_comment_allow"] as? String , commentStatus == "1"{
            newsCommentAllow = true
        }else if  let commentStatus = dictionary["news_comment_allow"] as? Bool{
            newsCommentAllow = commentStatus
        }else{
            newsCommentAllow = false
        }
        
        newsImageRatio = dictionary["news_image_ratio"] as? String ?? ""
        newsMediaType = dictionary["news_media_type"] as? String ?? ""
        newsModuleId = dictionary["news_module_id"] as? String ?? ""
        newsModuleType = dictionary["news_module_type"] as? String ?? ""
        newsPostedDate = dictionary["news_posted_date"] as? String ?? ""
        newsPostedDays = dictionary["news_posted_days"] as? String ?? ""
        newsShareUrl = dictionary["news_share_url"] as? String ?? ""
        newsTitle = dictionary["news_title"] as? String ?? ""
        newsVideo = dictionary["news_video"] as? String ?? ""
        feedCommentsCount = dictionary["feed_comments_count"] as? String ?? ""
        customerLikes = dictionary["customer_likes"] as? String ?? ""
        newsCreatorProfileImage = dictionary["news_creator_profile_image"] as? String ?? ""
        newsCreatorName = dictionary["news_creator_name"] as? String ?? ""
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if newsDescription != nil{
            dictionary["news_description"] = newsDescription
        }
        if newsFavStatus != nil{
            dictionary["news_fav_status"] = newsFavStatus
        }
        if newsId != nil{
            dictionary["news_id"] = newsId
        }
        if newsImage != nil{
            dictionary["news_image"] = newsImage
        }
        if newsLikeStatus != nil{
            dictionary["news_like_status"] = newsLikeStatus
        }
        if newsMediaType != nil{
            dictionary["news_media_type"] = newsMediaType
        }
        if newsModuleId != nil{
            dictionary["news_module_id"] = newsModuleId
        }
        if newsModuleType != nil{
            dictionary["news_module_type"] = newsModuleType
        }
        if newsPostedDate != nil{
            dictionary["news_posted_date"] = newsPostedDate
        }
        if newsPostedDays != nil{
            dictionary["news_posted_days"] = newsPostedDays
        }
        if newsShareUrl != nil{
            dictionary["news_share_url"] = newsShareUrl
        }
        if newsTitle != nil{
            dictionary["news_title"] = newsTitle
        }
        if newsVideo != nil{
            dictionary["news_video"] = newsVideo
        }
        if newsCommentAllow != nil{
            dictionary["news_comment_allow"] = newsCommentAllow
        }
        if newsImageRatio != nil{
            dictionary["news_image_ratio"] = newsImageRatio
        }
        
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        newsDescription = aDecoder.decodeObject(forKey: "news_description") as? String
        newsFavStatus = aDecoder.decodeObject(forKey: "news_fav_status") as? Bool
        newsId = aDecoder.decodeObject(forKey: "news_id") as? String
        newsImage = aDecoder.decodeObject(forKey: "news_image") as? String
        newsLikeStatus = aDecoder.decodeObject(forKey: "news_like_status") as? Bool
        newsMediaType = aDecoder.decodeObject(forKey: "news_media_type") as? String
        
        newsModuleId = aDecoder.decodeObject(forKey: "news_module_id") as? String

        newsModuleType = aDecoder.decodeObject(forKey: "news_module_type") as? String
        newsPostedDate = aDecoder.decodeObject(forKey: "news_posted_date") as? String
        newsPostedDays = aDecoder.decodeObject(forKey: "news_posted_days") as? String
        newsShareUrl = aDecoder.decodeObject(forKey: "news_share_url") as? String
        newsTitle = aDecoder.decodeObject(forKey: "news_title") as? String
        newsVideo = aDecoder.decodeObject(forKey: "news_video") as? String
        newsCommentAllow = aDecoder.decodeObject(forKey: "news_comment_allow") as? Bool
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if newsDescription != nil{
            aCoder.encode(newsDescription, forKey: "news_description")
        }
        if newsFavStatus != nil{
            aCoder.encode(newsFavStatus, forKey: "news_fav_status")
        }
        if newsId != nil{
            aCoder.encode(newsId, forKey: "news_id")
        }
        if newsImage != nil{
            aCoder.encode(newsImage, forKey: "news_image")
        }
        if newsLikeStatus != nil{
            aCoder.encode(newsLikeStatus, forKey: "news_like_status")
        }
        if newsMediaType != nil{
            aCoder.encode(newsMediaType, forKey: "news_media_type")
        }
        if newsModuleId != nil{
            aCoder.encode(newsModuleId, forKey: "news_module_id")
        }
        if newsModuleType != nil{
            aCoder.encode(newsModuleType, forKey: "news_module_type")
        }
        if newsPostedDate != nil{
            aCoder.encode(newsPostedDate, forKey: "news_posted_date")
        }
        if newsPostedDays != nil{
            aCoder.encode(newsPostedDays, forKey: "news_posted_days")
        }
        if newsShareUrl != nil{
            aCoder.encode(newsShareUrl, forKey: "news_share_url")
        }
        if newsTitle != nil{
            aCoder.encode(newsTitle, forKey: "news_title")
        }
        if newsVideo != nil{
            aCoder.encode(newsVideo, forKey: "news_video")
        }
        if newsCommentAllow != nil{
            aCoder.encode(newsVideo, forKey: "news_comment_allow")
        }
        
    }

}
