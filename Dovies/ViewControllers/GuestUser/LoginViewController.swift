//
//  LoginViewController.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit
import TwitterKit
import IQKeyboardManagerSwift

class LoginViewController: BaseViewController {

    @IBOutlet weak var txtUserName: DTTextField!
    @IBOutlet weak var txtPassword: DTTextField!
    fileprivate var returnKeyHandler : IQKeyboardReturnKeyHandler!
    let validator = Validator()
    var userSocialInfo : [String : Any]?
    var isSocialLogin : Bool = false
    
    //MARK: ViewLifeCycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        inititalUISetUp()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        userSocialInfo = [String: Any]()
        userSocialInfo?[WsParam.userName] = ""
        userSocialInfo?[WsParam.email] = ""
        userSocialInfo?[WsParam.gender] = ""
    }
    
    //MARK: Instance methods
    override func inititalUISetUp(){
        setFormValidators()
        returnKeyHandler = IQKeyboardReturnKeyHandler(controller: self)
        returnKeyHandler.lastTextFieldReturnKeyType = UIReturnKeyType.done
        
    }
	
    /**
     Seting Validation message for login
     */
    func setFormValidators(){
        validator.styleTransformers(success:{ (validationRule) -> Void in
            if let textField = validationRule.field as? DTTextField {
                textField.hideError()
            }
        }, error:{ (validationError) -> Void in
            if let textField = validationError.field as? DTTextField {
                textField.showError(message: validationError.errorMessage)
            }
        })
        
        validator.registerField(txtUserName, errorLabel: nil , rules: [RequiredRule(message: AlertMessages.emptyUserName)])//, EmailRule(message: AlertMessages.invalidEmail)])
        validator.registerField(txtPassword, errorLabel: nil, rules: [RequiredRule(message: AlertMessages.emptyPassword)])
        
    }
    
    //MARK: IBAction methods
	
    /**
     This is an action method where we used to Login With Facebook
     - Parameter sender: button object
     */
    @IBAction func btnLoginWithFaceBookTapped(_ sender : UIButton) {
        self.view.endEditing(true)
        isSocialLogin = true
        GlobalUtility.showActivityIndi(viewContView: self.view)
        CustomUtility.shared.getFacebookData(sender: self, completionHandler: {[weak self] (sucess, userDict) in
            guard let weakSelf = self else {return}
            if sucess == true, let responseDict = userDict{
                weakSelf.userSocialInfo = [String : Any]()
                var apiParams = [WsParam.socialNetwork_type : "FB"]
                if let email = responseDict["email"] as? String{
                    weakSelf.userSocialInfo![WsParam.email] = email
                }
                if let name = responseDict["name"] as? String{
                    weakSelf.userSocialInfo![WsParam.name] = name
                    weakSelf.userSocialInfo![WsParam.userName] = name.replacingOccurrences(of: " ", with: ".")
                }
                if let fbId = responseDict["id"] as? String{
                    weakSelf.userSocialInfo![WsParam.socialNetworkId] = fbId
                    apiParams[WsParam.socialNetworkId] = fbId
                }
                if let gender = responseDict["gender"] as? String{
                    weakSelf.userSocialInfo![WsParam.gender] = gender
                }
                weakSelf.userSocialInfo![WsParam.socialNetwork_type] = "FB"
                weakSelf.callLoginApi(with: apiParams)
            }
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
        }, isForFBFriends: false, completionHandlerFriends: nil)
        
    }
	
	
    /**
     This is an action method where we used to Login with Instagram
     - Parameter sender: button object
     */
    @IBAction func btnLoginWithInstagramTapped(_ sender: UIButton){
        let instaGramViewController = InstaGramViewController()
        isSocialLogin = true
        instaGramViewController.onTokenSuccess = { [weak self] token in
            guard let weakSelf = self else{return}
            weakSelf.getInstagramData(token: token)
        }
        self.present(instaGramViewController, animated: true) {
            
        }
        
    }
    
    /**
     This is an action method where we used to Login with Twitter
     - Parameter sender: button object
     */
	@IBAction func btnLoginWithTwitterTapped(_ sender: UIButton) {
		isSocialLogin = true
		TWTRTwitter.sharedInstance().logIn(completion: { [weak self] (session, error) in
			guard let weakSelf = self else {return}
			if (session != nil) {
				print("signed in as \(session!.userName)\nuser_id---\(session!.userID)");
				
				weakSelf.userSocialInfo = [String : Any]()
                weakSelf.userSocialInfo![WsParam.userName] = session!.userName
				weakSelf.userSocialInfo![WsParam.socialNetwork_type] = "TW"
				var apiParams = [WsParam.socialNetwork_type : "TW"]
				
				weakSelf.userSocialInfo![WsParam.socialNetworkId] = session!.userID
				apiParams[WsParam.socialNetworkId] = session!.userID
				
				weakSelf.callLoginApi(with: apiParams)
				
			} else {
				DoviesGlobalUtility.showSimpleAlert(viewController: weakSelf, message: "Error logging in.")
				print("error: ");
			}
		})
	}
	
    /**
     This is tap action method where we used to Login button
     - Parameter sender: button object
     */
    @IBAction func btnLoginTapped(_ sender : UIButton){
        isSocialLogin = false
        validator.validate(self)
        
//        guard let window = UIApplication.shared.windows.first else { return }
//        window.rootViewController = Storyboard.Main.storyboard().instantiateInitialViewController()
        
    }
    
    /// Sign up button action
    ///
    /// - Parameter sender: button object
    @IBAction func btnSignUpTapped(_ sender : UIButton){
        self.performSegue(withIdentifier: "gotoSignUpFromLogin", sender: nil)
    }
    
    /// Back button action
    ///
    /// - Parameter sender: button object
    @IBAction override func btnBackTapped(sender: UIButton) {
        _ = self.navigationController?.popViewController(animated: true)
    }
	
	
    /// Forgot password action
    ///
    /// - Parameter sender: button object
    @IBAction func forgotPasswordButtonTapped(_ sender: UIButton){
        UIAlertController.showForgotPasswordAlert(self) { (textField) in
            guard let emailText = textField.text, emailText.isValidEmail()  else{
                DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.invalidEmail, completion: nil)
                return
                
            }
            let dict = [WsParam.email : emailText]
            self.callForgotPasswordApi(with: dict)
            //WS call done here
        }
    }
    
    /**
     This method is used to calling Insta API to get User info.
     -Parameter token: token which we get from user sign up
     */
    func getInstagramData(token : String)
    {
        let dict = [
            "client_id" : instagram_clientID,
            "client_secret" : instagram_clientSecret,
            "grant_type" : "authorization_code",
            "redirect_uri" : instagram_redirectURI,
            "code" : token
        ]
        GlobalUtility.setPref(value: token, key: UserDefaultsKey.InstagramAccessToken)
		
		GlobalUtility.showActivityIndi(viewContView: self.view)
		
        WsCalls.callInstagramAPI(dicParam: dict, onSuccess: { (success, message, responseDict) in
			GlobalUtility.hideActivityIndi(viewContView: self.view)
			if success, let dict = responseDict {
				
				self.userSocialInfo = [String : Any]()
				
				self.userSocialInfo![WsParam.socialNetwork_type] = "INSTA"
				var apiParams = [WsParam.socialNetwork_type : "INSTA"]
				
				if let id = dict["id"] as? String {
					self.userSocialInfo![WsParam.socialNetworkId] = id
                    self.userSocialInfo?[WsParam.userName] = dict["username"]
                    self.userSocialInfo?[WsParam.name] = dict["full_name"]

					apiParams[WsParam.socialNetworkId] = id
				}
                
				self.callLoginApi(with: apiParams)
				
			}
        }, onFailure: { (error) in
            GlobalUtility.hideActivityIndi(viewContView: self.view)
        })

    }
    
     // MARK: Navigation methods
     
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationVC = segue.destination as? RegistrationViewController{
            destinationVC.socialDict = userSocialInfo
        }
        
        if let destinationVC = segue.destination as? UINavigationController, let vc = destinationVC.viewControllers[0] as? RegistrationViewController {
            vc.socialDict = userSocialInfo
        }
        
     }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension LoginViewController: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let dtTextField = textField as? DTTextField else{return}
        dtTextField.hideError()
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        validator.validateField(textField){ error in
            if error == nil {
                // Field validation was successful
            } else {
                // Validation error occurred
            }
        }
        return true
    }

}

extension LoginViewController : ValidationDelegate{
    func validationSuccessful() {
        var loginParams = [String : Any]()
        loginParams[WsParam.userName] = txtUserName.text
        loginParams[WsParam.password] = txtPassword.text
        callLoginApi(with: loginParams)
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        
    }
}

//API calls
extension LoginViewController{
    
    /**
     This method is used to call Login API.
     -Parameter parameters: is having user information in dictionary format
     */
    func callLoginApi(with parameters:[String: Any]){
        GlobalUtility.showActivityIndi(viewContView: self.view)
        DoviesWSCalls.callLoginApi(dicParam: parameters, onSuccess: {[weak self] (isSuccess, message, response) in
            guard let weakSelf = self else{return}
            if isSuccess, let userInfo = response{
                //afterlogin success
                if let userID = UserDefaults.standard.value(forKey: UserDefaultsKey.logoutId) as? String,   userID != userInfo[WsParam.customerUserName] as? String{
                    GlobalUtility.removePref(key: UserDefaultsKey.logoutId)
                    DoviesGlobalUtility.deleteCasheForNewUser()
                }
                
                //=====Set subscription status  START
                
                if let subStatus = (userInfo[WsParam.isSubscribed] as? String)?.toBool(){
                    DoviesGlobalUtility.isSubscribed = subStatus
                    UserDefaults.standard.set(subStatus, forKey: WsParam.isSubscribed)
                }

                if let daysLeft = (userInfo[WsParam.title] as? String){
                    DoviesGlobalUtility.subscriptionDaysLeft = daysLeft
                }
                
                
                print("IS_SUBSCIBED = \(DoviesGlobalUtility.isSubscribed)")
                
                //=====Set subscription status END

                UserDefaults.standard.set(userInfo, forKey: UserDefaultsKey.userData)
                DoviesGlobalUtility.currentUser = User.init(fromDictionary: userInfo)
                DoviesGlobalUtility.setFilterData()
                DoviesGlobalUtility.showSimpleAlert(viewController: weakSelf, message: message, completion: nil)
                guard let window = UIApplication.shared.windows.first else { return }
                window.rootViewController = Storyboard.Main.storyboard().instantiateInitialViewController()
                
            }else if weakSelf.isSocialLogin{
//                weakSelf.callRegistrationApi()
                weakSelf.performSegue(withIdentifier: "gotoSignUpFromLogin", sender: nil)
            }else{
                DoviesGlobalUtility.showSimpleAlert(viewController: weakSelf, message: message, completion: nil)
            }
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
            
        }) {[weak self] (error) in
            guard let weakSelf = self else{return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
        }
    }
    
	
    /**
     This method is used for Social Sign up API call.
     */
    func callRegistrationApi(){
        guard let infoDict = userSocialInfo else {return}
        GlobalUtility.showActivityIndi(viewContView: self.view)
        var params = [String : Any]()
        params[WsParam.userName] = infoDict[WsParam.userName]
        params[WsParam.email] = infoDict[WsParam.email]
        params[WsParam.mobileNumber] = ""
        params[WsParam.password] = ""
        params[WsParam.gender] = infoDict[WsParam.gender]

        params[WsParam.socialNetwork_type] = infoDict[WsParam.socialNetwork_type]
        params[WsParam.socialNetworkId] = infoDict[WsParam.socialNetworkId]
        
        DoviesWSCalls.callSingupWS(dicParam: params, onSuccess: {[weak self] (isSuccess, message, response) in
            guard let weakSelf = self else{return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
            if isSuccess, let userInfo = response{
                if let userID = UserDefaults.standard.value(forKey: UserDefaultsKey.logoutId) as? String,   userID != userInfo[WsParam.customerUserName] as? String{
                    GlobalUtility.removePref(key: UserDefaultsKey.logoutId)
                    DoviesGlobalUtility.deleteCasheForNewUser()
                }
                UserDefaults.standard.set(userInfo, forKey: UserDefaultsKey.userData)
                DoviesGlobalUtility.currentUser = User.init(fromDictionary: userInfo)
                DoviesGlobalUtility.setFilterData()
                DoviesGlobalUtility.showSimpleAlert(viewController: weakSelf, message: message, completion: nil)
                guard let window = UIApplication.shared.windows.first else { return }
                window.rootViewController = Storyboard.Main.storyboard().instantiateInitialViewController()
            }else if weakSelf.isSocialLogin{
                weakSelf.performSegue(withIdentifier: "gotoSignUpFromLogin", sender: nil)
            }else{
                DoviesGlobalUtility.showSimpleAlert(viewController: weakSelf, message: message, completion: nil)
            }
        }) { [weak self](error) in
            guard let weakSelf = self else{return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
        }
    }
	
    /**
     This method is used for Forgot password API call
     - Parameter parameters: parameter object in dictionary form
     */
    func callForgotPasswordApi(with parameters:[String: Any]){
        DoviesWSCalls.callForgotPasswordAPI(dicParam: parameters, onSuccess: {[weak self] (isSuccess, message, response) in
            guard let weakSelf = self else{return}
            DoviesGlobalUtility.showSimpleAlert(viewController: weakSelf, message: message, completion: nil)
            
        }) {(error) in
        
        }
    }
    
}


