
import Foundation


class WorkoutDayModel : NSObject, NSCoding{

	var programDayNumber : String!
	var programWeekNumber : String!
	var programWorkoutFlag : String!
	var programWorkoutGoodFor : String!
	var programWorkoutId : String!
	var programWorkoutImage : String!
	var programWorkoutName : String!
	var programWorkoutStatus : String!
	var programWorkoutTime : String!
	var workoutId : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		programDayNumber = dictionary["program_day_number"] as? String ?? ""
		programWeekNumber = dictionary["program_week_number"] as? String ?? ""
		programWorkoutFlag = dictionary["program_workout_flag"] as? String ?? ""
		programWorkoutGoodFor = dictionary["program_workout_good_for"] as? String ?? ""
		programWorkoutId = dictionary["program_workout_id"] as? String ?? ""
		programWorkoutImage = dictionary["program_workout_image"] as? String ?? ""
		programWorkoutName = dictionary["program_workout_name"] as? String ?? ""
		programWorkoutStatus = dictionary["program_workout_status"] as? String ?? ""
		programWorkoutTime = dictionary["program_workout_time"] as? String ?? ""
		workoutId = dictionary["workout_id"] as? String ?? ""
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if programDayNumber != nil{
			dictionary["program_day_number"] = programDayNumber
		}
		if programWeekNumber != nil{
			dictionary["program_week_number"] = programWeekNumber
		}
		if programWorkoutFlag != nil{
			dictionary["program_workout_flag"] = programWorkoutFlag
		}
		if programWorkoutGoodFor != nil{
			dictionary["program_workout_good_for"] = programWorkoutGoodFor
		}
		if programWorkoutId != nil{
			dictionary["program_workout_id"] = programWorkoutId
		}
		if programWorkoutImage != nil{
			dictionary["program_workout_image"] = programWorkoutImage
		}
		if programWorkoutName != nil{
			dictionary["program_workout_name"] = programWorkoutName
		}
		if programWorkoutStatus != nil{
			dictionary["program_workout_status"] = programWorkoutStatus
		}
		if programWorkoutTime != nil{
			dictionary["program_workout_time"] = programWorkoutTime
		}
		if workoutId != nil{
			dictionary["workout_id"] = workoutId
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         programDayNumber = aDecoder.decodeObject(forKey: "program_day_number") as? String
         programWeekNumber = aDecoder.decodeObject(forKey: "program_week_number") as? String
         programWorkoutFlag = aDecoder.decodeObject(forKey: "program_workout_flag") as? String
         programWorkoutGoodFor = aDecoder.decodeObject(forKey: "program_workout_good_for") as? String
         programWorkoutId = aDecoder.decodeObject(forKey: "program_workout_id") as? String
         programWorkoutImage = aDecoder.decodeObject(forKey: "program_workout_image") as? String
         programWorkoutName = aDecoder.decodeObject(forKey: "program_workout_name") as? String
         programWorkoutStatus = aDecoder.decodeObject(forKey: "program_workout_status") as? String
         programWorkoutTime = aDecoder.decodeObject(forKey: "program_workout_time") as? String
         workoutId = aDecoder.decodeObject(forKey: "workout_id") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if programDayNumber != nil{
			aCoder.encode(programDayNumber, forKey: "program_day_number")
		}
		if programWeekNumber != nil{
			aCoder.encode(programWeekNumber, forKey: "program_week_number")
		}
		if programWorkoutFlag != nil{
			aCoder.encode(programWorkoutFlag, forKey: "program_workout_flag")
		}
		if programWorkoutGoodFor != nil{
			aCoder.encode(programWorkoutGoodFor, forKey: "program_workout_good_for")
		}
		if programWorkoutId != nil{
			aCoder.encode(programWorkoutId, forKey: "program_workout_id")
		}
		if programWorkoutImage != nil{
			aCoder.encode(programWorkoutImage, forKey: "program_workout_image")
		}
		if programWorkoutName != nil{
			aCoder.encode(programWorkoutName, forKey: "program_workout_name")
		}
		if programWorkoutStatus != nil{
			aCoder.encode(programWorkoutStatus, forKey: "program_workout_status")
		}
		if programWorkoutTime != nil{
			aCoder.encode(programWorkoutTime, forKey: "program_workout_time")
		}
		if workoutId != nil{
			aCoder.encode(workoutId, forKey: "workout_id")
		}

	}

}
