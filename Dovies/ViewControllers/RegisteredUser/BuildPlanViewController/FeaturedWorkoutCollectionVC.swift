//
//  FeaturedWorkoutCollectionVC.swift
//  Dovies
//
//  Created by Neel Shah on 03/07/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class FeaturedWorkoutCollectionVC: BaseViewController {
	
	@IBOutlet weak var tblViewWorkout: UITableView!
	var arrModelMyWorkouts = [ModelWorkoutList]()
	var workoutGroupId = ""
	var cellCount = 10
	
	var previousSelectedIndex : IndexPath?
	var workoutSelected:((ModelWorkoutList, Bool)->())?
	var doneTap:(()->())?

    //MARK: ViewLifeCycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
	//	self.addNavigationBackButton()
        self.addNavigationWhiteBackButton()
		
		tblViewWorkout.register(UINib(nibName: "MyWorkoutsCell", bundle: nil), forCellReuseIdentifier: "MyWorkoutsCell")
		self.navigationItem.rightBarButtonItem = GlobalUtility.getNavigationButtonItemWhiteAndRedWith(target: self, selector: #selector(self.saveTapped), title: "Done")
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
        
        
        if self.navigationController?.navigationBar.isTranslucent == true
        {
            let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
                (self.navigationController?.navigationBar.frame.height ?? 0.0)
            
            self.tblViewWorkout.contentInset = UIEdgeInsets(top:  topBarHeight , left: 0, bottom: 0, right: 0)

        }
        
		if arrModelMyWorkouts.count == 0{
			DispatchQueue.main.async { [weak self] in
				guard let weakSelf = self else{return}
				weakSelf.tblViewWorkout.reloadData()
				weakSelf.tblViewWorkout.layoutIfNeeded()
				weakSelf.tblViewWorkout.showLoader()
			}
			self.callFeaturedWorkoutAPI()
		}
	}
    
    //MARK: Instance methods
	
    @objc func saveTapped(){
        self.doneTap?()
        var viewContOld: BuildPlanViewController!
        for viewCont in (self.navigationController?.viewControllers)!{
            if viewCont.isKind(of: BuildPlanViewController.self){
                viewContOld = viewCont as! BuildPlanViewController
            }
        }
        if viewContOld != nil{
            self.navigationController?.popToViewController(viewContOld, animated: true)
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK: Webservice methods
    
    /**
     This method is used to call the feature workout list based on 'GroupId' and updates the UI
     */
	func callFeaturedWorkoutAPI(){
		var dictParams = [String : Any]()
		dictParams["group_id"] = workoutGroupId
		dictParams["type"] = "List"
		DoviesWSCalls.callFeaturedWorkoutsListAPI(dicParam: dictParams, onSuccess: { (success, msg, arrGroupModels) in
			if success{
				self.arrModelMyWorkouts = arrGroupModels
				self.tblViewWorkout.hideLoader()
				self.cellCount = 0
				self.tblViewWorkout.reloadData()
			}
			else
			{
				self.cellCount = 10
			}
			
		}) { (error) in
			
		}
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension FeaturedWorkoutCollectionVC: UITableViewDelegate, UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return arrModelMyWorkouts.count > 0 ? arrModelMyWorkouts.count : cellCount
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "MyWorkoutsCell") as! MyWorkoutsCell
		if arrModelMyWorkouts.count > 0{
			cell.setFeaturedWorkoutsData(modelWorkOuts: arrModelMyWorkouts[indexPath.row])
		}
		
		if let iPath = previousSelectedIndex, iPath == indexPath{
			cell.isCellSelected = true
		}else{
			cell.isCellSelected = false
		}
		
		cell.imgRightArrow.isHidden = true
		cell.btnSelect.isHidden = false
		return cell
	}
	
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if arrModelMyWorkouts.count > 0{
			if arrModelMyWorkouts[indexPath.row].workoutAccessLevel.uppercased() == "OPEN".uppercased(){
				let cell = tableView.cellForRow(at: indexPath) as! MyWorkoutsCell
				if previousSelectedIndex == indexPath{
					cell.isCellSelected = false
				}
				else{
					cell.isCellSelected = true
				}
				self.workoutSelected?(arrModelMyWorkouts[indexPath.row], cell.isCellSelected)
				if let iPath = previousSelectedIndex, iPath != indexPath{
					if let cell = tableView.cellForRow(at: iPath) as? MyWorkoutsCell{
						cell.isCellSelected = false
					}
				}
				if cell.isCellSelected == false{
					previousSelectedIndex = nil
				}
				else{
					previousSelectedIndex = indexPath
				}
				
			}else{
				if let upgradeVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "UpgradeToPremiumViewController") as? UpgradeToPremiumViewController{
					upgradeVC.onPurchaseSuccess = {
						self.callFeaturedWorkoutAPI()
					}
					self.navigationController?.pushViewController(upgradeVC, animated: true)
				}
			}
			
		}
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 75.0
	}
}

