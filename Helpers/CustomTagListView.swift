//
//  TagListView+extension.swift
//  ParkAnyWhere
//
//  Created by Company Name on 21/11/17.
//  Copyright © 2017 Company Name. All rights reserved.
//

import Foundation
import TagListView

// MARK: - This extension of TagListView sets required properties(common in whole application) to itself in awakeFromNib method.
class CustomTagListView: TagListView {
	override func awakeFromNib() {
		super.awakeFromNib()
		self.tagBackgroundColor = UIColor.white
		self.borderWidth = 1.0
		self.textFont = UIFont.robotoRegular(size: 13.0)
		self.paddingX = 10
		self.paddingY = 5
		self.marginY = 5
		self.marginX = 5
		
		self.textColor = UIColor.lightGray
		self.borderColor = UIColor.lightGray
		self.selectedTextColor = UIColor.lightGray
		self.selectedBorderColor = UIColor.lightGray
	}
}
