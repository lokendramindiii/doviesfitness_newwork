//
//  SettingViewController.swift
//  Dovies
//
//  Created by hb on 16/04/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import UserNotifications
import FBSDKLoginKit

class SettingViewController: BaseViewController{
    
    @IBOutlet weak var tblViewSetting: UITableView!
    
    var timePicker = UIDatePicker()
    var selectedButton = UIButton()
    var toolBar = UIToolbar()
    var sections = ["About You","Settings","Dovies Workout App"]
    var items = [["Edit Profile","Change Password"], ["Units", "Reminder time", /*"Preview next workout",*/"Notifications","Country","Rate us on App Store","Invite Friends"], ["About Application", "Terms And Conditions", "Privacy Policy","APP FAQs","Contact Us","Logout"]]
	var profileUpdated: (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Settings".uppercased()
        self.navigationItem.hidesBackButton  = true
        //self.addNavigationBackButton()
        self.addNavigationWhiteBackButton()

        self.tblViewSetting.register(UINib(nibName: "SettingCell", bundle: nil), forCellReuseIdentifier: "SettingCell")
        self.tblViewSetting.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        self.timePicker.datePickerMode = .time
        
        // If 12 hours time formate change to 24 hour
        let ReminderTime =  UserDefaults.standard.string(forKey: "isReminderTime")
        
        if ReminderTime !=  nil
        {
            if ReminderTime!.contains("AM") ||  ReminderTime!.contains("PM") {
                
                let time = ReminderTime ?? ""
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "hh:mm a"
                let fullDate = dateFormatter.date(from: time)
                dateFormatter.dateFormat = "HH:mm"
                let time2 = dateFormatter.string(from: fullDate!)
                UserDefaults.standard.set(time2, forKey: "isReminderTime")
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func switchWorkoutIsChanged(mySwitch: UISwitch) {
        UserDefaults.standard.set(mySwitch.isOn, forKey: "isSwitchWorkout")
    }
    
    @objc func switchNotificationIsChanged(mySwitch: UISwitch) {
        UserDefaults.standard.set(mySwitch.isOn, forKey: "isNotification")
        if mySwitch.isOn{
            if #available(iOS 10.0, *) {
                
                // SETUP FOR NOTIFICATION FOR iOS >= 10.0
                let center  = UNUserNotificationCenter.current()
                center.requestAuthorization(options: [.sound, .alert, .badge]) { (granted, error) in
                    if error == nil{
                        DispatchQueue.main.async(execute: {
                            UIApplication.shared.registerForRemoteNotifications()
                        })
                    }
                }
                
            } else {
                
                // SETUP FOR NOTIFICATION FOR iOS < 10.0
                
                let settings = UIUserNotificationSettings(types: [.sound, .alert, .badge], categories: nil)
                UIApplication.shared.registerUserNotificationSettings(settings)
                
                // This is an asynchronous method to retrieve a Device Token
                // Callbacks are in AppDelegate.swift
                // Success = didRegisterForRemoteNotificationsWithDeviceToken
                // Fail = didFailToRegisterForRemoteNotificationsWithError
                UIApplication.shared.registerForRemoteNotifications()
            }
        }else{
            UIApplication.shared.unregisterForRemoteNotifications()
            
        }
    }
    
    func showUnitsTypePicker(){
        self.view.endEditing(true)
        var selectedTitle = ""
        if let user = DoviesGlobalUtility.currentUser{
            selectedTitle = user.customerUnits
        }
        let mcPicker = McPicker(data: [["Imperial","Metric"]], selectedTitle:[selectedTitle])
        mcPicker.show { [weak self](selections: [Int : String]) -> Void in
            guard self != nil else{return}
            if let selectedUnit = selections[0]{
                if let user = DoviesGlobalUtility.currentUser{
                    user.customerUnits = selectedUnit
                    self?.callUpdateUnitsAPI(with: selectedUnit)
                self?.tblViewSetting.reloadRows(at: [IndexPath(row: 0, section: 1)], with: .none)
                }
            }
        }
    }
    
}
extension SettingViewController: UITableViewDelegate,UITableViewDataSource
{
    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String?{
        return sections[section]
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?{
        
        //Create label and autoresize it
        let headerView = UIView(frame : CGRect(x: 0, y: 0, width:UIScreen.main.bounds.size.width , height: 43.67 * CGRect.widthRatio))
        headerView.backgroundColor = UIColor.white
        let headerLabel = UILabel(frame: CGRect(x: 15, y: 0, width:UIScreen.main.bounds.size.width , height: 43.67 * CGRect.widthRatio))
        headerLabel.font = UIFont.tamilBold(size: 14.0)
        headerLabel.textColor = UIColor.colorWithRGB(r: 34, g: 34, b: 34)
        headerLabel.text = self.tableView(self.tblViewSetting, titleForHeaderInSection: section)
        let bottom = UIView(frame: CGRect(x: -15, y: headerView.frame.size.height, width: headerView.frame.size.width, height: 1))
        bottom.backgroundColor = UIColor.colorWithRGB(r: 153.0, g: 153.0, b: 153.0)
        //  headerView.addSubview(bottom)

        let BottomImg = UIImageView(frame: CGRect(x: 0, y:headerView.frame.size.height - 0.6 , width: headerView.frame.size.width, height: 0.6))
        BottomImg.image = UIImage(named: "seprator_line")
        headerView.addSubview(BottomImg)

        headerView.addSubview(headerLabel)
        
        return headerView
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int{
        return sections.count
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
       
        return items[section].count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingCell
        // loks add code for seprator line  cell.Img_leadingConstraint.constant
        let totalRows = tableView.numberOfRows(inSection: indexPath.section)
        if indexPath.row == totalRows - 1 {
            cell.Img_leadingConstraint.constant = 0
        }
        else
        {
            cell.Img_leadingConstraint.constant = 15
        }
      
        // Configure the cell...
        if (indexPath.section) == 0 {
            cell.txtReminderTime.isHidden = true
            cell.setSwitch.isHidden = true
            // loks add img rightarrow
            cell.Img_Rightarrow.isHidden = false
//   let rightArrow = UIImage(named: "right_arrow_s25_h")
//            cell.accessoryType = .disclosureIndicator
//            cell.accessoryView = UIImageView(image: rightArrow)
        }
        else if (indexPath.section) == 1 {
            if indexPath.section == indexPath.last
            {
                //cell.Img_leadingConstraint.constant = 0
            }
            if indexPath.row == 0{
                if let user = DoviesGlobalUtility.currentUser{
                    cell.txtReminderTime.text = user.customerUnits
                }else{
                    cell.txtReminderTime.text = ""
                }
                
            cell.txtReminderTime.isUserInteractionEnabled = false
                cell.setSwitch.isHidden = true
                // loks add img rightarrow
                cell.Img_Rightarrow.isHidden = false
//                let rightArrow = UIImage(named: "right_arrow_s25_h")
//                cell.accessoryType = .disclosureIndicator
//                cell.accessoryView = UIImageView(image: rightArrow)
            }
            else if indexPath.row == 1{
                if UserDefaults.standard.value(forKey: "isReminderTime") ==  nil{
                 //   let time = "07:00 AM"
                    let time = "07:00"
                    
                    cell.txtReminderTime.text = time
                    UserDefaults.standard.set(cell.txtReminderTime.text, forKey: "isReminderTime")
                } else{
                    cell.txtReminderTime.text = UserDefaults.standard.string(forKey: "isReminderTime")
                }
                cell.setSwitch.isHidden = true
                // loks add img rightarrow
                cell.Img_Rightarrow.isHidden = false
//                let rightArrow = UIImage(named: "right_arrow_s25_h")
//                cell.accessoryType = .disclosureIndicator
//                cell.accessoryView = UIImageView(image: rightArrow)
                
            }
				/*
            else if indexPath.row == 2{
                cell.txtReminderTime.isHidden = true
                cell.setSwitch.isHidden = false
                if UserDefaults.standard.value(forKey: "isSwitchWorkout") ==  nil{
                    cell.setSwitch.setOn(false, animated: true)
                } else {
                    cell.setSwitch.setOn(UserDefaults.standard.bool(forKey: "isSwitchWorkout"), animated: true)
                }
                cell.setSwitch.addTarget(self, action: #selector(self.switchWorkoutIsChanged(mySwitch:)), for: UIControlEvents.valueChanged)
            }
*/
            else if indexPath.row == 2
            {
                cell.txtReminderTime.isHidden = true
                cell.txtReminderTime.isUserInteractionEnabled = true
                cell.setSwitch.isHidden = false
                if UserDefaults.standard.value(forKey: "isNotification") ==  nil{
                    cell.setSwitch.setOn(false, animated: true)
                }
                else{
                    cell.setSwitch.setOn(UserDefaults.standard.bool(forKey: "isNotification"), animated: true)
                }
                // loks add img rightarrow
                cell.Img_Rightarrow.isHidden = true
                cell.setSwitch.addTarget(self, action: #selector(self.switchNotificationIsChanged(mySwitch:)), for: UIControlEvents.valueChanged)
            }
            else if indexPath.row == 3{
                // loks add img rightarrow
                cell.Img_Rightarrow.isHidden = false
                cell.txtReminderTime.isHidden = false
                cell.txtReminderTime.isUserInteractionEnabled = false
                cell.setSwitch.isHidden = true
                if let user = DoviesGlobalUtility.currentUser, let currentCountry = SRCountryPickerController.getCountryName(countryCode: user.customerCountryId)
				{
                    cell.txtReminderTime.text = currentCountry
                }
				else
				{
                    cell.txtReminderTime.text = ""
                }
//                let rightArrow = UIImage(named: "right_arrow_s25_h")
//                cell.accessoryType = .disclosureIndicator
//                cell.accessoryView = UIImageView(image: rightArrow)
            }
            else{
                cell.txtReminderTime.isHidden = true
                cell.setSwitch.isHidden = true
                // loks add img rightarrow
                cell.Img_Rightarrow.isHidden = false
//                let rightArrow = UIImage(named: "right_arrow_s25_h")
//                cell.accessoryType = .disclosureIndicator
//                cell.accessoryView = UIImageView(image: rightArrow)
            }
        }
        else{
            cell.txtReminderTime.isHidden = true
            cell.txtReminderTime.isUserInteractionEnabled = false
            cell.setSwitch.isHidden = true
            // loks add img rightarrow
            cell.Img_Rightarrow.isHidden = false
//            let rightArrow = UIImage(named: "right_arrow_s25_h")
//            cell.accessoryType = .disclosureIndicator
//            cell.accessoryView = UIImageView(image: rightArrow)
            if indexPath.row == 6{
                
            }
        }
        cell.tintColor = .black
        
        if items[indexPath.section][indexPath.row] == "Logout"
        {
           cell.lblTitle.textColor = #colorLiteral(red: 0.9882352941, green: 0.3215686275, blue: 0.1607843137, alpha: 1)
        }
        else
        {
            cell.lblTitle.textColor = #colorLiteral(red: 0.1333333333, green: 0.1333333333, blue: 0.1333333333, alpha: 1)
        }
        

        cell.lblTitle?.text = items[indexPath.section][indexPath.row]
        cell.selectionStyle = .none
        
       
        return cell
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        let sectionNumber = indexPath.section
        if sectionNumber == 0 {
            if indexPath.row == 0 {
                let AboutYou = self.storyboard?.instantiateViewController(withIdentifier: "AboutYouVC")as! AboutYouVC
				AboutYou.profileUpdated = {
					self.profileUpdated?()
					tableView.reloadData()
				}
                self.navigationController?.pushViewController(AboutYou, animated: true)
            }
            if indexPath.row == 1 {
                //let medicalConditionVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "MedicalConditionsVC")as! MedicalConditionsVC
                let changePasswordVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "ChangePasswordVC")as! ChangePasswordVC
                self.navigationController?.pushViewController(changePasswordVC, animated: true)
            }
        }
        else if sectionNumber == 1{
            switch indexPath.row{
				case 0:
					showUnitsTypePicker()
				case 1:
					if let cell = tableView.cellForRow(at: indexPath) as? SettingCell{
						cell.txtReminderTime.becomeFirstResponder()
					}
				
				case 2:
					break
				case 3:
					SRCountryPickerHelper.shared.showCountryPicker { (country) in
						if let cell = tableView.cellForRow(at: indexPath) as? SettingCell{
							if let user = DoviesGlobalUtility.currentUser{
								user.customerCountryId = country.country_code
								user.customerCountryName = country.country_name
								user.customerIsdCode = country.dial_code
							}
							cell.txtReminderTime.text = country.country_name
							var dic = [String: Any]()
							dic[WsParam.countryCode] = country.country_code
							dic[WsParam.isdCode] = country.dial_code
							self.callUpdateUserProfile(with: dic)
						}
					}
					break
				case 4:
					GlobalUtility.openUrlFromApp(url: StaticUrls.iTunesRedirectLink)
				default:
					let InviteFriendsVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "InviteFriendsViewController") as? InviteFriendsViewController
					self.navigationController?.pushViewController(InviteFriendsVC!, animated: true)
            }
        }
        else{
            switch indexPath.row{
				case 0:
					let staticPageVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "StaticPagesViewController")as! StaticPagesViewController
					staticPageVC.pageType = .aboutApplication
					self.navigationController?.pushViewController(staticPageVC, animated: true)
				
				
				case 1:
					let staticPageVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "StaticPagesViewController")as! StaticPagesViewController
					staticPageVC.pageType = .termsAndConditions
					self.navigationController?.pushViewController(staticPageVC, animated: true)
				
				case 2:
					let staticPageVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "StaticPagesViewController")as! StaticPagesViewController
					staticPageVC.pageType = .privacyPolicy
					self.navigationController?.pushViewController(staticPageVC, animated: true)
				
				case 3:
					let faqVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "FaqViewController")as! FaqViewController
					self.navigationController?.pushViewController(faqVC, animated: true)
				
				case 4:
					let ContactUsVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "ContactUsVC") as? ContactUsVC
					self.navigationController?.pushViewController(ContactUsVC!, animated: true)
				
				case 5:
					UIAlertController.showAlert(in: (APP_DELEGATE.window?.rootViewController)!, withTitle: "", message: AlertMessages.logoutConfirmation, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Logout", otherButtonTitles: nil, tap: { (alertCont, action, index) in
						if index == 1{
							if let dict = GlobalUtility.getPref(key: UserDefaultsKey.userData) as? [String : Any], let userId = dict[WsParam.customerUserName] as? String{
								GlobalUtility.setPref(value: userId, key: UserDefaultsKey.logoutId)
							}
                            let objFBSDKLoginManager : FBSDKLoginManager = FBSDKLoginManager()
                            objFBSDKLoginManager.logOut()
                            UserDefaults.standard.removeObject(forKey: WsParam.isSubscribed)
							UserDefaults.standard.removeObject(forKey: UserDefaultsKey.userData)
							APP_DELEGATE.setStoryBoardBasedOnUserLogin()
						}
					})
				
				default:
					break
				
				}
        }
    }
    
    class func tempUnzipPath() -> String {
        var path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        path += "/Videos"
        let url = URL(fileURLWithPath: path)
        return url.path
    }
    
    class func clearTempFolder() {
        let fileManager = FileManager.default
        do {
            try fileManager.removeItem(atPath: SettingViewController.tempUnzipPath())
        } catch {
            print("Could not clear temp folder: \(error)")
        }
    }
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
     return 43.67 * CGRect.widthRatio

    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 43.67 * CGRect.widthRatio
    }
    
    public func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == sections.count - 1{
            return 20.0
        }
        return 0.1
    }
    
    public func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == 2 {
            let footerView = UIView(frame: CGRect(x: 0, y: 0, width:UIScreen.main.bounds.size.width , height: 40))
            footerView.backgroundColor = .white
            let currentVersion = UILabel(frame: CGRect(x: 15, y: 10, width:UIScreen.main.bounds.size.width / 2  , height: 20))
            let versionNumber = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as! String
            currentVersion.text = "Current Version " + versionNumber
            currentVersion.textColor = UIColor.colorWithRGB(r: 153.0, g: 153.0, b: 153.0)
            currentVersion.font = UIFont.tamilRegular(size: 14)
            let copyRight = UILabel(frame: CGRect(x: UIScreen.main.bounds.size.width / 2, y: 10, width:UIScreen.main.bounds.size.width / 2 - 15  , height: 20))
            copyRight.text = "© Copyrights Dovies"
            copyRight.textAlignment = .right
            copyRight.textColor = UIColor.colorWithRGB(r: 153.0, g: 153.0, b: 153.0)
            copyRight.font = UIFont.tamilRegular(size: 14)
            footerView.addSubview(currentVersion)
            footerView.addSubview(copyRight)
            let top = UIView(frame: CGRect(x: -15 , y: 0, width: footerView.frame.size.width + 15, height: 1))
            top.backgroundColor = UIColor.colorWithRGB(r: 153.0, g: 153.0, b: 153.0)
            let bottom = UIView(frame: CGRect(x: -15, y: footerView.frame.size.height, width: footerView.frame.size.width + 15, height: 1))
            bottom.backgroundColor = UIColor.colorWithRGB(r: 153.0, g: 153.0, b: 153.0, alpha: 0.3)
            
            footerView.addSubview(bottom)
            return footerView
        }else {
            let emptyView = UIView(frame: CGRect(x: 0, y: 0, width:0 , height: 0))
            return emptyView
        }
    }
}

//API Calls
extension SettingViewController{
    func callUpdateUnitsAPI(with unitsType : String){
        guard let user = DoviesGlobalUtility.currentUser else{return}
        DoviesWSCalls.callUpdateUserUnitsAPI(dicParam: [WsParam.authCustomerId : user.customerAuthToken, WsParam.customerUnits : unitsType], onSuccess: { (success, message,response) in
            if success, let arrResponse = response, arrResponse.count > 0{
                UserDefaults.standard.set(arrResponse[0], forKey: UserDefaultsKey.userData)
                DoviesGlobalUtility.currentUser = User.init(fromDictionary: arrResponse[0])
            }
        }) { (error) in
            
        }
    }
    
    func callUpdateUserProfile(with params : [String : Any]){
        guard DoviesGlobalUtility.currentUser != nil else{return}
        
        DoviesWSCalls.callUpdateUserCountryAPI(dicParam: params, onSuccess: {[weak self] (isSuccess, message, response) in
            guard let weakSelf = self else{return}
            if let dic = response{
                UserDefaults.standard.set(dic, forKey: UserDefaultsKey.userData)
                DoviesGlobalUtility.currentUser = User.init(fromDictionary: dic)
            }
            if isSuccess{
                GlobalUtility.showToastMessage(msg: "Done")
            }
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
            
        }) { [weak self](error) in
            guard let weakSelf = self else{return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
        }
    }
    
}
