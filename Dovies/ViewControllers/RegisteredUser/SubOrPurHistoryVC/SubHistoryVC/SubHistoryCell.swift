//
//  SubHistoryCell.swift
//  Dovies
//
//  Created by hb on 20/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class SubHistoryCell: UITableViewCell {
	

    @IBOutlet weak var IBlbPlanType: UILabel!
    @IBOutlet weak var IBlblEndIn: UILabel!
	@IBOutlet weak var IBlblName: UILabel!
	@IBOutlet weak var IBlblAmount: UILabel!
	@IBOutlet weak var IBlblStartDate: UILabel!
	@IBOutlet weak var IBlblEndDate: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
	
	func setSubHistoryData(model: ModelSubscriptionHistory){
        
        IBlbPlanType.text = model.packagetext
        IBlblEndIn.text = model.endsIn + " days"
        IBlblName.text = model.packageName
        IBlblAmount.text = model.amountPaid
        IBlblEndDate.text = model.expDate
        IBlblStartDate.text = model.startDate
	}

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
