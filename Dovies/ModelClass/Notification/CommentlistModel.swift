//
//  CommentlistModel.swift
//  Dovies
//
//  Created by Mindiii on 2/26/19.
//  Copyright © 2019 Hidden Brains. All rights reserved.
//

import UIKit

class CommentlistModel: NSObject , NSCoding {
    
    
    var comment : String!
    var customNotificationId : String!
    var customNotificationLikeCommentId : String!
    var addedDate : String!
    var customerId : String!
    var sysRecDeleted : String!
    var userId : String!
    var name : String!
    var isLike : String!
    var profileImage : String!
    var notificationAgo : String!


    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        
        comment = dictionary["comment"] as? String ?? ""
        
        notificationAgo = dictionary["comment_ago"] as? String ?? ""
        
        customNotificationId = dictionary["customNotificationId"] as? String ?? ""
        
        isLike = dictionary["isLike"] as? String ?? ""
       
        customNotificationLikeCommentId = dictionary["customNotificationLikeCommentId"] as? String ?? ""

        addedDate = dictionary["dAddedDate"] as? String ?? ""
        customerId = dictionary["iCustomerId"] as? String ?? ""
        sysRecDeleted = dictionary["iSysRecDeleted"] as? String ?? ""
        userId = dictionary["userId"] as? String ?? ""
        
        name = dictionary["vName"] as? String ?? ""
        
        profileImage = dictionary["vProfileImage"] as? String ?? ""
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if comment != nil{
            dictionary["comment"] = comment
        }
        
        if notificationAgo != nil{
            dictionary["comment_ago"] = notificationAgo
        }
        
        if customNotificationId != nil{
            dictionary["customNotificationId"] = customNotificationId
        }
        if isLike != nil{
            dictionary["isLike"] = isLike
        }
        if customNotificationLikeCommentId != nil{
            dictionary["customNotificationLikeCommentId"] = customNotificationLikeCommentId
        }
        if addedDate != nil{
            dictionary["dAddedDate"] = addedDate
        }
        if customerId != nil{
            dictionary["iCustomerId"] = customerId
        }
        if sysRecDeleted != nil{
            dictionary["iSysRecDeleted"] = sysRecDeleted
        }
        if userId != nil{
            dictionary["userId"] = userId
        }
        
        if name != nil{
            dictionary["vName"] = name
        }
        if profileImage != nil{
            dictionary["vProfileImage"] = profileImage
        }
      
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        comment = aDecoder.decodeObject(forKey: "comment") as? String
        
        notificationAgo = aDecoder.decodeObject(forKey: "comment_ago") as? String

        customNotificationId = aDecoder.decodeObject(forKey: "customNotificationId") as? String
        
        isLike = aDecoder.decodeObject(forKey: "isLike") as? String
        
        customNotificationLikeCommentId = aDecoder.decodeObject(forKey: "customNotificationLikeCommentId") as? String
        addedDate = aDecoder.decodeObject(forKey: "dAddedDate") as? String
        
        customerId = aDecoder.decodeObject(forKey: "iCustomerId") as? String
        
        sysRecDeleted = aDecoder.decodeObject(forKey: "iSysRecDeleted") as? String
        userId = aDecoder.decodeObject(forKey: "userId") as? String
        
        name = aDecoder.decodeObject(forKey: "vName") as? String
        
        profileImage = aDecoder.decodeObject(forKey: "vProfileImage") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if comment != nil{
            aCoder.encode(comment, forKey: "comment")
        }
        
        if notificationAgo != nil{
            aCoder.encode(notificationAgo, forKey: "comment_ago")
        }
        
        if customNotificationId != nil{
            aCoder.encode(customNotificationId, forKey: "customNotificationId")
        }
        if isLike != nil{
            aCoder.encode(isLike, forKey: "isLike")
        }
        
        if customNotificationLikeCommentId != nil{
            aCoder.encode(customNotificationLikeCommentId, forKey: "customNotificationLikeCommentId")
        }
        if addedDate != nil{
            aCoder.encode(addedDate, forKey: "dAddedDate")
        }
        if customerId != nil{
            aCoder.encode(customerId, forKey: "iCustomerId")
        }
        if sysRecDeleted != nil{
            aCoder.encode(sysRecDeleted, forKey: "iSysRecDeleted")
        }
        if userId != nil{
            aCoder.encode(userId, forKey: "userId")
        }
        
        if name != nil{
            aCoder.encode(name, forKey: "vName")
        }
        if profileImage != nil{
            aCoder.encode(profileImage, forKey: "vProfileImage")
        }
    }
}
