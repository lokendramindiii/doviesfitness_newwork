//
//  DietPlanCollectionCell.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit


class DietPlanCollectionCell: UICollectionViewCell {

    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDisplayname : UILabel!

    @IBOutlet weak var imgBackground : UIImageView!
    
    @IBOutlet weak var lblDietPlanCategoryName : UILabel!
    @IBOutlet weak var imgFadeBackground : UIImageView!

    var isFromEditAdmin             : Bool = false

    var cellIndexPath : IndexPath?
    
    @IBOutlet weak var btnMore : UIButton!
    @IBOutlet weak var imgLock : UIImageView!
    @IBOutlet weak var constTitleHeight  : NSLayoutConstraint!
	@IBOutlet weak var constOptionTop  : NSLayoutConstraint!
    
    var moreButtonAction:((DietPlan?)->())?
    var moreButtonMyPlanAction:((ModelMyPlan?)->())?
	var moreButtonDeleteAction:(()->())?
    var dietPlan : DietPlan?
    var myPlan : ModelMyPlan?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imgFadeBackground.isHidden = true
        lblDietPlanCategoryName.isHidden = true
        
        let gradientView = UIView(frame: CGRect(x: 0, y: (NewsFeedViewController.cellWidth)/2, width: NewsFeedViewController.cellWidth, height: (NewsFeedViewController.cellWidth)/2))
        let gradientLayer:CAGradientLayer = CAGradientLayer()
        gradientLayer.frame.size =  gradientView.frame.size
        gradientLayer.colors = [UIColor.clear.cgColor,UIColor.appBlackColorNew().withAlphaComponent(0.7).cgColor]
        gradientView.layer.addSublayer(gradientLayer)
        self.imgBackground.addSubview(gradientView)

    }
    
    @IBAction func btnMoreTapped(_ sender : UIButton){
        if let plan = dietPlan{
            moreButtonAction?(plan)
        }
        if let mPlan = myPlan{
            moreButtonMyPlanAction?(mPlan)
        }
		if moreButtonDeleteAction != nil{
			moreButtonDeleteAction?()
		}
    }
    
    func setCellUI(with category : DietPlanCategory){
     //   imgBackground.sd_setImage(with:  URL(string: category.dietPlanCategoryImage), placeholderImage: AppInfo.placeholderImage)
        
        imgBackground.sd_setImage(with:  URL(string: category.dietPlanCategoryImage), placeholderImage: nil)

        
        imgFadeBackground.isHidden = false
        lblDietPlanCategoryName.isHidden = false
        lblDietPlanCategoryName.text = category.dietPlanCategoryName.uppercased()
        constTitleHeight.constant = 0
    }
    
    func setCellUI(modelExerciseLibrary: ModelExerciseLibrary){
       // imgBackground.sd_setImage(with:  URL(string: modelExerciseLibrary.image), placeholderImage: AppInfo.placeholderImage)
        
        imgBackground.sd_setImage(with:  URL(string: modelExerciseLibrary.image), placeholderImage: nil)

        constTitleHeight.constant = 0
    }

	func setCellForExerciseLibraryUI(modelExerciseLibrary: ModelExerciseLibrary){
        
		//imgBackground.sd_setImage(with:  URL(string: modelExerciseLibrary.image), placeholderImage: AppInfo.placeholderImage)
    
        imgBackground.backgroundColor = UIColor.black
        imgBackground.sd_setImage(with:  URL(string: modelExerciseLibrary.image), placeholderImage: nil)


        constTitleHeight.constant = 0
        lblDisplayname.isHidden = false
        lblDisplayname.text = modelExerciseLibrary.displayName.uppercased()
	}
	
    func setMyPlanUI(modelMyPlan: ModelMyPlan){
		//imgBackground.sd_setImage(with:  URL(string: modelMyPlan.programImage), placeholderImage: AppInfo.placeholderImage)
        
        imgBackground.sd_setImage(with:  URL(string: modelMyPlan.programImage), placeholderImage: nil)

        
        imgFadeBackground.isHidden = false
        lblDietPlanCategoryName.isHidden = false
        lblDietPlanCategoryName.text = modelMyPlan.programName.uppercased()

        imgLock.isHidden = modelMyPlan.programAccessLevel.uppercased() == "OPEN".uppercased() || DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
		if imgLock.isHidden == false{
			self.constOptionTop.constant = 45
		}
		else{
			self.constOptionTop.constant = 10
		}
        
        constTitleHeight.constant = 0
//        btnMore.isHidden = !(modelMyPlan.programAccessLevel.uppercased() == "OPEN".uppercased())
        contentView.backgroundColor = UIColor.white
        myPlan = modelMyPlan
	}
    
    func setCellForBuildPlan(plan : ProgramDietPlan){
       // imgBackground.sd_setImage(with:  URL(string: plan.programDietImage), placeholderImage: AppInfo.placeholderImage)
        
        imgBackground.sd_setImage(with:  URL(string: plan.programDietImage), placeholderImage: nil)
        
        if isFromEditAdmin
        {
        lblDietPlanCategoryName.isHidden = false
        lblDietPlanCategoryName.text = plan.programDietName.uppercased()
        }
        constTitleHeight.constant = 0
    }
	
    func setCellUI(with dietPlanObj: DietPlan){
        dietPlan = dietPlanObj
      //  imgBackground.sd_setImage(with:  URL(string: dietPlanObj.dietPlanImage), placeholderImage: AppInfo.placeholderImage)
        
        imgBackground.sd_setImage(with:  URL(string: dietPlanObj.dietPlanImage), placeholderImage: nil)

        
        imgLock.isHidden = dietPlanObj.dietPlanAccessLevel.uppercased() == "OPEN".uppercased() || DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        
            btnMore.isHidden = !(dietPlanObj.dietPlanAccessLevel.uppercased() == "OPEN".uppercased())
        
        imgFadeBackground.isHidden = false
        lblDietPlanCategoryName.isHidden = false
        lblDietPlanCategoryName.text = dietPlanObj.dietPlanTitle.uppercased()
        constTitleHeight.constant = 0

    }
    
}
