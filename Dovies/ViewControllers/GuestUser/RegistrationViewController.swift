//
//  RegistrationViewController.swift
//  Dovies
//
//  Created by CompanyName
//  Copyright © CompanyName. All rights reserved.
//

import UIKit
import TwitterKit
import IQKeyboardManagerSwift
import SideMenuController
import TTTAttributedLabel

let imageUnCheck = UIImage(named: "check_box.png")
let imageCheck = UIImage(named: "check_box_h.png")
let imageDropdown = UIImage(named: "ico_drop_down.png")

class RegistrationViewController: BaseViewController {

    @IBOutlet weak var txtUserName: DTTextField!
    @IBOutlet weak var txtEmail: DTTextField!
    @IBOutlet weak var txtMobileNumber: DTTextField!
    @IBOutlet weak var txtPassword: DTTextField!
    @IBOutlet weak var txtConfirmPassword: DTTextField!
    @IBOutlet weak var txtGender: DTTextField!
    @IBOutlet weak var txtFullName: DTTextField!
	@IBOutlet weak var txtMobileCode: DTTextField!
	@IBOutlet var IBstackView: UIStackView!
    
    @IBOutlet var stackPassword: UIStackView!
    @IBOutlet var stackConfPassword: UIStackView!

    @IBOutlet weak var btnGender : UIButton!
    @IBOutlet weak var lblPrivacyPolicy : TTTAttributedLabel!
    
    @IBOutlet weak var constFormHeight :NSLayoutConstraint!
    @IBOutlet weak var constSocialStackH :NSLayoutConstraint!
    @IBOutlet weak var btnCreateAccount: UIButton!
    @IBOutlet weak var btnCheckbox: UIButton!

    @IBOutlet weak var lblErrorFullName: UILabel!
    @IBOutlet weak var lblErrorEmail: UILabel!
    @IBOutlet weak var lblErrorDOB: UILabel!
    @IBOutlet weak var lblErrorCountry: UILabel!
    @IBOutlet weak var lblErrorMobile: UILabel!
    @IBOutlet weak var lblErrorPassword: UILabel!
    @IBOutlet weak var lblErrorConfPassword: UILabel!
    
    @IBOutlet weak var lblCheckBox: UILabel!
    var datePicker = UIDatePicker()
    var Str_Dob : String = ""

    var isCloseScreen = false
    var isNavigateScreen = true

    let validator = Validator()
    var gender : String = "Male"
    var isPrivacyValid : Bool = true
    fileprivate var returnKeyHandler : IQKeyboardReturnKeyHandler!
    var socialDict: [String:Any]?
	
	var countryObj = Country(country_code: "", dial_code: "", country_name: "")
    
    var signupParams = [String :Any]()
	
    //MARK: ViewLifeCycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblErrorDOB.isHidden = true
		IBstackView.setNeedsLayout()
		IBstackView.layoutIfNeeded()
        inititalUISetUp()
        self.txtUserName.addDropdown(width: 20)
        self.txtMobileCode.addDropdown(width: 15)
        self.txtGender.addDropdown(width: 20)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isStatusBarHidden = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        UIApplication.shared.isStatusBarHidden = false
    }
    
 
    
    //MARK: Instance methods
    
    override func inititalUISetUp() {
        setFormValidators()
        self.setDefaulErrorLabels()
        self.hideAllErrorLabels()
        
        returnKeyHandler = IQKeyboardReturnKeyHandler(controller: self)
        returnKeyHandler.lastTextFieldReturnKeyType = UIReturnKeyType.done
        
        fillDetailsBasedOnSocialInfo()
        if socialDict == nil{
            socialDict?[WsParam.userName] = ""
            socialDict?[WsParam.email] = ""
            socialDict?[WsParam.gender] = ""
            socialDict?[WsParam.name] = ""
        }
		
		lblPrivacyPolicy.isUserInteractionEnabled = true
		
		let text = "By creating an account, you agree to Doviesfitness's Privacy Policy and Terms Of Use"
		
		let paragraphStyle = NSMutableParagraphStyle()
		paragraphStyle.lineSpacing = 5
		paragraphStyle.alignment = .center
		
		let defaultAttriString = NSMutableAttributedString(string: text, attributes: [NSAttributedStringKey.font: UIFont.tamilRegular(size: 12.0), NSAttributedStringKey.foregroundColor: fieldTextColor, NSAttributedStringKey.paragraphStyle: paragraphStyle, NSAttributedStringKey.kern : 0.5])
		
		self.lblPrivacyPolicy.delegate = self
		self.lblPrivacyPolicy.setText(defaultAttriString)
		
		let subscriptionNoticeLinkAttributes = [
            NSAttributedStringKey.font: UIFont.tamilBold(size: 11.0),
			NSAttributedStringKey.foregroundColor: fieldTextColor,
			NSAttributedStringKey.underlineStyle: NSNumber(value:true),
		]
		lblPrivacyPolicy.linkAttributes = subscriptionNoticeLinkAttributes
		
		let subscriptionNoticeActiveLinkAttributes = [
			NSAttributedStringKey.foregroundColor: UIColor.colorWithRGB(r: 153, g: 153, b: 153, alpha: 0.3),
			NSAttributedStringKey.backgroundColor: UIColor.lightGray,
			NSAttributedStringKey.underlineStyle: NSNumber(value:false),
		]
		
		lblPrivacyPolicy.activeLinkAttributes = subscriptionNoticeActiveLinkAttributes
		
		let range1 = (text as NSString).range(of: "Terms Of Use")
        self.lblPrivacyPolicy.addLink(to: NSURL(string: "URL_TERMS_AND_CONDITIONS") as URL?, with:range1)
		
		let range2 = (text as NSString).range(of: "Privacy Policy")
        self.lblPrivacyPolicy.addLink(to: NSURL(string: "URL_PRIVACY_POLICY") as URL?, with:range2)
        self.lblCheckBox.makeSpacing(lineSpace: 2, font: UIFont.tamilRegular(size: 11))
    }
    
    func hideAllErrorLabels() {
        self.lblErrorFullName.isHidden = true
        self.lblErrorFullName.text = AlertMessages.emptyUserFullName
        self.lblErrorEmail.isHidden = true
        self.lblErrorEmail.text = AlertMessages.emptyEmail

      //  self.lblErrorDOB.isHidden = true
     //   self.lblErrorDOB.text = AlertMessages.emptyDOB

        self.lblErrorCountry.isHidden = true
        self.lblErrorCountry.text = AlertMessages.emptyCountry

        self.lblErrorMobile.isHidden = true
        self.lblErrorMobile.text = AlertMessages.emptyMobile

        self.lblErrorPassword.isHidden = true
        self.lblErrorPassword.text = AlertMessages.emptyPassword

        self.lblErrorConfPassword.isHidden = true
        self.lblErrorConfPassword.text = AlertMessages.emptyRePassword

    }
    
    func setDefaulErrorLabels(){
        self.lblErrorFullName.text = AlertMessages.emptyUserFullName
        self.lblErrorEmail.text = AlertMessages.emptyEmail
      //  self.lblErrorDOB.text = AlertMessages.emptyDOB
        self.lblErrorCountry.text = AlertMessages.emptyCountry
        self.lblErrorMobile.text = AlertMessages.emptyMobile
        self.lblErrorPassword.text = AlertMessages.emptyPassword
        self.lblErrorConfPassword.text = AlertMessages.emptyRePassword
    }
    
    func showErrorLabelForField(field: UITextField, message: String) {
        //hideAllErrorLabels()
        switch field {
        case txtFullName:
            self.lblErrorFullName.text = message
            self.lblErrorFullName.isHidden = false
            self.layout()
        case txtEmail:
            self.lblErrorEmail.text = message
            self.lblErrorEmail.isHidden = false
            self.layout()

//        case txtUserName:
//            self.lblErrorDOB.text = message
//            self.lblErrorDOB.isHidden = false
//            self.layout()

        case txtGender:
            self.lblErrorCountry.text = message
            self.lblErrorCountry.isHidden = false
            self.layout()

        case txtMobileNumber:
            self.lblErrorMobile.text = message
            self.lblErrorMobile.isHidden = false
            self.layout()

        case txtPassword:
            self.lblErrorPassword.text = message
            self.lblErrorPassword.isHidden = false
            self.layout()

        case txtConfirmPassword:
            self.lblErrorConfPassword.text = message
            self.lblErrorConfPassword.isHidden = false
            self.layout()

        default:
            print("no case match")
            return
        }
    }
    
    func layout()  {
        DispatchQueue.main.async {
            self.view.layoutIfNeeded()
            self.view.updateConstraintsIfNeeded()
        }

    }
    
    func hideErrorLabelForField(field: UITextField) {
        //hideAllErrorLabels()
        switch field {
        case txtFullName:
            self.lblErrorFullName.isHidden = true
            self.layout()

        case txtEmail:
            self.lblErrorEmail.isHidden = true
            self.layout()

//        case txtUserName:
//            self.lblErrorDOB.isHidden = true
//            self.layout()

        case txtGender:
            self.lblErrorCountry.isHidden = true
            self.layout()

        case txtMobileNumber:
            self.lblErrorMobile.isHidden = true
            self.layout()

        case txtPassword:
            self.lblErrorPassword.isHidden = true
            self.layout()

        case txtConfirmPassword:
            self.lblErrorConfPassword.isHidden = true
            self.layout()

        default:
            print("no case match")
            return
        }
    }
    
    
    
    
    func setFormValidators(){
        
        validator.styleTransformers(success:{ (validationRule) -> Void in
            
            if let textField = validationRule.field as? UITextField {
                //textField.hideError()
                self.hideErrorLabelForField(field: textField)
            }
        }, error:{ (validationError) -> Void in
            
            if let textField = validationError.field as? UITextField {
                //textField.showError(message: validationError.errorMessage)
                print(validationError.errorMessage)
                self.showErrorLabelForField(field: textField, message: validationError.errorMessage)
            }
        })
        
     //   validator.registerField(txtUserName, errorLabel: nil , rules: [RequiredRule(message: AlertMessages.emptyDOB)])
        
        validator.registerField(txtEmail, errorLabel: nil , rules: [RequiredRule(message: AlertMessages.emptyEmail), EmailRule(message: AlertMessages.invalidEmail)])
        
        
     //   validator.registerField(txtPassword, errorLabel: nil, rules: [RequiredRule(message: AlertMessages.emptyPassword),MinLengthRule(length: 6, message: AlertMessages.validatePassword)])
        
        validator.registerField(txtPassword, errorLabel: nil, rules: [RequiredRule(message: AlertMessages.emptyPassword)])

		validator.registerField(txtFullName, errorLabel: nil , rules: [RequiredRule(message: AlertMessages.emptyUserFullName)])
        
        validator.registerField(txtConfirmPassword, errorLabel: nil , rules: [RequiredRule(message: AlertMessages.emptyRePassword), ConfirmationRule(confirmField: txtPassword, message: AlertMessages.invalidatePasswordCheck)])
        
        validator.registerField(txtGender, errorLabel: nil , rules: [RequiredRule(message: AlertMessages.emptyCountry)])
    }
	
	
    /**
     This method is used to fill form details based on social info
     */
    func fillDetailsBasedOnSocialInfo() {
        
        guard let info = socialDict else{return}
        if let email = info[WsParam.email] as? String{
            txtEmail.text = email
            if let Email = txtEmail.text , Email.isEmpty
            {
                txtEmail.isUserInteractionEnabled = true
            }
            else
            {
                txtEmail.isUserInteractionEnabled = false
            }
        }
        if let name = info[WsParam.name] as? String{
            txtFullName.text = name
        }
        
        if let gender = info[WsParam.gender] as? String{
            txtGender.text = gender.capitalized
        }
        if let text = info[WsParam.socialNetwork_type] as? String, !text.isEmpty{
            stackPassword.isHidden = true
            stackConfPassword.isHidden = true
//            txtPassword.isHidden = true
//            txtConfirmPassword.isHidden = true
            //constFormHeight.constant = 351
            validator.registerField(txtPassword, errorLabel: nil, rules: [])
            validator.registerField(txtConfirmPassword, errorLabel: nil, rules:[])
            
            validator.registerField(txtEmail, errorLabel: nil, rules: [RequiredRule(message: AlertMessages.emptyEmail), EmailRule(message: AlertMessages.invalidEmail)])
            //validator.registerField(txtGender, errorLabel: nil, rules: [])
            validator.registerField(txtGender, errorLabel: nil , rules: [RequiredRule(message: AlertMessages.emptyCountry)])
            //constSocialStackH.constant = 0
        }
        
    }
    
    //MARK: Webservice methods
    
	
    func btnCountrySelect(){
        SRCountryPickerHelper.shared.showCountryPicker {[weak self] (country) in
            guard let weakSelf = self else{return}

            weakSelf.countryObj = country
            weakSelf.txtGender.text = country.country_name
            weakSelf.hideErrorLabelForField(field: weakSelf.txtGender)

            weakSelf.txtMobileCode.text = country.dial_code
            weakSelf.IBstackView.setNeedsLayout()
            weakSelf.IBstackView.layoutIfNeeded()
        }
    }
    
	func btnCountryCodeSelect(){
		SRCountryPickerHelper.shared.showCountryPicker { (country) in
			//self.countryObj = country
			self.txtMobileCode.text = country.dial_code
			self.IBstackView.setNeedsLayout()
			self.IBstackView.layoutIfNeeded()
		}
	}
    
    //MARK: IBAction methods
    
    @IBAction func btnCloseTapped(_ sender: UIButton){
        isCloseScreen = true
    self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnGenderTapped(_ sender : UIButton){
        self.view.endEditing(true)
        let mcPicker = McPicker(data: [["Male", "Female"]], selectedTitle: [gender], pickerTitle : "Gender")
        mcPicker.show { [weak self] (selections: [Int : String]) -> Void in
            guard let weakSelf = self else{return}
            if let gender = selections[0]{
                weakSelf.txtGender.text = gender
                weakSelf.gender = gender
            }
        }
        
    }
    
    @IBAction func btnSignUpTapped(_ sedner : UIButton){
        self.view.endEditing(true)
        
        let string = txtPassword.text ?? ""
        print("string --\(string)")
        if string == " "
        {
            txtPassword.text = ""
        }
        else
        {
            txtPassword.text = string.trimmingCharacters(in: .whitespaces)
        }
        print("psd --\( txtPassword.text ?? "")")

        validator.validate(self)
       // self.navigateToGenderSelectionScreen()
    }
    
    
    func navigateToGenderSelectionScreen()  {
        let selectGenderVC = Storyboard.Login.storyboard().instantiateViewController(withIdentifier: "SelectGenderViewController")as! SelectGenderViewController
        selectGenderVC.signupParams = self.signupParams
        
    self.navigationController?.pushViewController(selectGenderVC, animated: true)
    }
    
    
    @IBAction func btnCheckBoxTapped(_ sender: UIButton) {
        if sender.currentImage == imageUnCheck{
            sender.setImage(imageCheck, for: .normal)
        }
        else{
            sender.setImage(imageUnCheck, for: .normal)
        }
    }
    
    @IBAction func btnLoginWithFaceBookTapped(_ sender : UIButton) {
        self.view.endEditing(true)
        CustomUtility.shared.getFacebookData(sender: self, completionHandler: {[weak self] (sucess, userDict) in
            guard let weakSelf = self else {return}
            if sucess == true, let responseDict = userDict{
                var apiParams = [WsParam.socialNetwork_type : "FB"]
                weakSelf.socialDict = [WsParam.socialNetwork_type : "FB"]
                if let fbId = responseDict["id"] as? String {
					weakSelf.socialDict![WsParam.socialNetworkId] = fbId
                    apiParams[WsParam.socialNetworkId] = fbId
                }
                if let gender = responseDict["gender"] as? String{
					weakSelf.socialDict![WsParam.gender] = gender
                }
                if let email = responseDict["email"] as? String{
					weakSelf.socialDict![WsParam.email] = email
                }
                if let name = responseDict["name"] as? String{
                    weakSelf.socialDict![WsParam.name] = name
                    weakSelf.socialDict![WsParam.userName] = name.replacingOccurrences(of: " ", with: ".")
                }
                weakSelf.callLoginApi(with: apiParams)
                
            }
            }, isForFBFriends: false, completionHandlerFriends: nil)
        
    }
    
    /*
    @IBAction func btnLoginWithInstagramTapped(_ sender: UIButton){
        let instaGramViewController = InstaGramViewController()
        
        instaGramViewController.onTokenSuccess = { [weak self] token in
            guard let weakSelf = self else{return}
            weakSelf.getInstagramData(token: token)
        }
        self.present(instaGramViewController, animated: true) {
            
        }
        
    }
    
    @IBAction func btnLoginWithTwitterTapped(_ sender: UIButton){
        TWTRTwitter.sharedInstance().logIn(completion: { [weak self] (session, error) in
			guard let weakSelf = self else {return}
            if (session != nil) {
                print("signed in as \(session!.userName)\nuser_id---\(session!.userID)");
				
				weakSelf.socialDict = [WsParam.socialNetwork_type : "TW"]
				weakSelf.socialDict![WsParam.socialNetworkId] = session!.userID
                weakSelf.socialDict![WsParam.userName] = session!.userName
//                weakSelf.fillDetailsBasedOnSocialInfo()
                var apiParams = [WsParam.socialNetwork_type : "TW"]
                apiParams[WsParam.socialNetworkId] = session!.userID
                
                weakSelf.callLoginApi(with: apiParams)
            } else {
				DoviesGlobalUtility.showSimpleAlert(viewController: weakSelf, message: "Error logging in.")
                print("error: ");
            }
        })
    }
     
     /**
     This method is used for calling Insta API to get User info
     */
     func getInstagramData(token : String){
     let dict = [
     "client_id" : instagram_clientID,
     "client_secret" : instagram_clientSecret,
     "grant_type" : "authorization_code",
     "redirect_uri" : instagram_redirectURI,
     "code" : token
     ]
     GlobalUtility.setPref(value: token, key: UserDefaultsKey.InstagramAccessToken)
     
     GlobalUtility.showActivityIndi(viewContView: self.view)
     
     WsCalls.callInstagramAPI(dicParam: dict, onSuccess: { (success, message, responseDict) in
     
     if success, let dict = responseDict {
     var apiParams = [WsParam.socialNetwork_type : "INSTA"]
     self.socialDict = [WsParam.socialNetwork_type : "INSTA"]
     if let id = dict["id"] as? String {
     self.socialDict![WsParam.socialNetworkId] = id
     apiParams[WsParam.socialNetworkId] = id
     self.socialDict![WsParam.userName] = dict["username"]
     if let name = dict["full_name"] as? String{
     self.socialDict![WsParam.name] = name
     }
     }
     
     self.callLoginApi(with: apiParams)
     }
     
     GlobalUtility.hideActivityIndi(viewContView: self.view)
     }, onFailure: { (error) in
     GlobalUtility.hideActivityIndi(viewContView: self.view)
     })
     
     }
  */
    
    @IBAction func btnPrivacyPolicyTapped(){
        //redirect to privacy policy
        let staticPageVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "StaticPagesViewController")as! StaticPagesViewController
        staticPageVC.pageType = .privacyPolicy
        let navi = UINavigationController(rootViewController: staticPageVC)
        self.present(navi, animated: true, completion: nil)
    }
    
    @IBAction func btnTermsTapped(){
        //redirect to privacy policy
        let staticPageVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "StaticPagesViewController")as! StaticPagesViewController
        staticPageVC.pageType = .termsAndConditions
        let navi = UINavigationController(rootViewController: staticPageVC)
        self.present(navi, animated: true, completion: nil)
    }
	
	@IBAction func handleTapOnLabel(gesture: UITapGestureRecognizer) {
		
		guard let text = lblPrivacyPolicy.attributedText?.string else {
			return
		}
		
		if let range = text.range(of: "Terms Of Use"),
			gesture.didTapAttributedTextInLabel(label: lblPrivacyPolicy, inRange: NSRange(range, in: text)) {
			self.btnTermsTapped()
		} else if let range = text.range(of: "Privacy Policy"),
			gesture.didTapAttributedTextInLabel(label: lblPrivacyPolicy, inRange: NSRange(range, in: text)) {
			self.btnPrivacyPolicyTapped()
		}
	}
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


//=======For date picker ======
extension RegistrationViewController{
    func openTimePicker(_ dateSelectionTextField: UITextField?) {
        datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        dateSelectionTextField?.inputView = datePicker
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 44))
        toolBar.tintColor = UIColor.white
        toolBar.barTintColor = UIColor(r: 235, g: 235, b: 235, alpha: 1)
        let cancelBtn = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelTimePicker))
        let doneBtn = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneTimePicker))
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.items = [cancelBtn, space, doneBtn]
        dateSelectionTextField?.inputAccessoryView = toolBar
    }
    
    @objc func cancelTimePicker() {
        self.txtUserName.resignFirstResponder()
    }
    
    @objc func doneTimePicker() {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd yyyy"
        Str_Dob = "\(formatter.string(from: datePicker.date))"
        formatter.dateFormat = "dd MMM, yyyy"
        self.txtUserName.text = "\(formatter.string(from: datePicker.date))"
         self.txtUserName.resignFirstResponder()

    }
    
}

extension RegistrationViewController : ValidationDelegate{
	
    func validationSuccessful() {

        if isPrivacyValid, isPhoneNumberValid(){

            signupParams[WsParam.email] = txtEmail.text
            signupParams[WsParam.mobileNumber] = txtMobileNumber.text ?? ""
            signupParams[WsParam.password] = txtPassword.text
			signupParams[WsParam.isdCode] = txtMobileCode.text ?? ""
			signupParams[WsParam.countryId] = countryObj.country_code
            signupParams[WsParam.gender] = ""
            signupParams[WsParam.name] = txtFullName.text
            signupParams[WsParam.email_upadates] = (self.btnCheckbox.currentImage == imageUnCheck) ? "0" : "1"
			if let theDic = self.socialDict {
				signupParams[WsParam.socialNetwork_type] = theDic[WsParam.socialNetwork_type]
				signupParams[WsParam.socialNetworkId] = theDic[WsParam.socialNetworkId]
			}
            
            
            if isNavigateScreen == true
            {
            self.checkEmailAvailibility(name: txtEmail.text!)
            }
            
            //self.navigateToGenderSelectionScreen()
            //self.callRegistrationApi(with: signupParams)
        }
    }
    
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        
    }
    
    func isPhoneNumberValid()->Bool{
        if let text = txtMobileNumber.text, text.isEmpty{
            return true
        }
        else if let text = txtMobileNumber.text{
            if text.isValidPhoneNumber(){
                return true
            }else{
                 GlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.validMobileNumber)
                return false
            }
            
        }
        else{
            GlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.validMobileNumber)
            return false
        }
    }
}

extension RegistrationViewController: UITextFieldDelegate{
	func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
		if textField == txtMobileCode{
			self.btnCountryCodeSelect()
			return false
		}
        else if textField == txtGender{
            self.btnCountrySelect()
            return false
        }
        else if textField == txtUserName {
            self.openTimePicker(textField)
        }
        
		return true
	}
	
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let dtTextField = textField as? UITextField else{return}
        //dtTextField.hideError()
        self.hideErrorLabelForField(field: textField)
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        validator.validateField(textField){ error in
            if error == nil {
                // Field validation was successful
            } else {
                // Validation error occurred
            }
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        validator.validateField(textField){ error in
            if error == nil {
                // Field validation was successful
            } else {
                // Validation error occurred
            }
        }
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        if let vc : UIViewController = self.presentedViewController {
            vc.dismiss(animated: flag, completion: completion)
        }
        if isCloseScreen{
            super.dismiss(animated: flag, completion: completion)
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var shouldReturn = true
        let newLength = (textField.text?.count ?? 0) + string.count - Int(range.length)

        if textField == txtUserName {
            shouldReturn =  (newLength > 0) ? false : true
        }
        
//        if textField == txtPassword {
//            if newLength > 0
//            {
//            var substring: String = txtPassword.text!
//            let trimmedString = substring.trimmingCharacters(in: .whitespaces)
//            print(trimmedString)
//            }
//        }
        
        return shouldReturn

    }
    
}

//API calls
extension RegistrationViewController {
    /**
     This method is used to call registration api to register a user.
     - Parameter params: It is having user details to register with Dovies app.
     */
    /*
    func callRegistrationApi(with params: [String:Any]){
        GlobalUtility.showActivityIndi(viewContView: self.view)
        DoviesWSCalls.callSingupWS(dicParam: params, onSuccess: {[weak self] (isSuccess, message, response) in
            guard let weakSelf = self else{return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
            if isSuccess, let userInfo = response{
                if let userID = UserDefaults.standard.value(forKey: UserDefaultsKey.logoutId) as? String,   userID != userInfo[WsParam.customerUserName] as? String{
                    GlobalUtility.removePref(key: UserDefaultsKey.logoutId)
                    DoviesGlobalUtility.deleteCasheForNewUser()
                }
                UserDefaults.standard.set(userInfo, forKey: UserDefaultsKey.userData)
                DoviesGlobalUtility.currentUser = User.init(fromDictionary: userInfo)
                DoviesGlobalUtility.setFilterData()
                guard let window = UIApplication.shared.windows.first else { return }
                window.rootViewController = Storyboard.Main.storyboard().instantiateInitialViewController()
				
				let centerView = window.rootViewController as! SideMenuController
				let viewCont = centerView.centerViewController
				if viewCont != nil{
					DoviesGlobalUtility.showSimpleAlert(viewController: viewCont!, message: message, completion: nil)
				}
            }else{
                DoviesGlobalUtility.showSimpleAlert(viewController: weakSelf, message: message, completion: nil)
            }
        }) { [weak self](error) in
            guard let weakSelf = self else{return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
        }
    }
    */
    
    /**
     This method is used to call login api.
     - Parameter params: It is having user details to loggedin with Dovies app.
     */
    func callLoginApi(with parameters:[String: Any]){
        GlobalUtility.showActivityIndi(viewContView: self.view)
        DoviesWSCalls.callLoginApi(dicParam: parameters, onSuccess: {[weak self] (isSuccess, message, response) in
            
            guard let weakSelf = self else{return}
            if isSuccess, let userInfo = response{
                //afterlogin success
                if let userID = UserDefaults.standard.value(forKey: UserDefaultsKey.logoutId) as? String,   userID != userInfo[WsParam.userName] as? String{
                    GlobalUtility.removePref(key: UserDefaultsKey.logoutId)
                    DoviesGlobalUtility.deleteCasheForNewUser()
                }
                UserDefaults.standard.set(userInfo, forKey: UserDefaultsKey.userData)
                DoviesGlobalUtility.currentUser = User.init(fromDictionary: userInfo)
                DoviesGlobalUtility.setFilterData()
                guard let window = UIApplication.shared.windows.first else { return }
                window.rootViewController = Storyboard.Main.storyboard().instantiateInitialViewController()
				
				let centerView = window.rootViewController as! SideMenuController
				let viewCont = centerView.centerViewController
				if viewCont != nil{
					DoviesGlobalUtility.showSimpleAlert(viewController: viewCont!, message: message, completion: nil)
				}
                
            }else if weakSelf.socialDict != nil{
                weakSelf.fillDetailsBasedOnSocialInfo()
//                weakSelf.callRegistrationApi()
            }else{
                DoviesGlobalUtility.showSimpleAlert(viewController: weakSelf, message: message, completion: nil)
            }
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
            
        }) {[weak self] (error) in
            guard let weakSelf = self else{return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
        }
    }
    
    /**
     This method we are not using right now;
 
    func callRegistrationApi(){
        guard let infoDict = socialDict else {return}
        GlobalUtility.showActivityIndi(viewContView: self.view)
        var params = [String : Any]()
        params[WsParam.userName] = infoDict[WsParam.userName]
        params[WsParam.name] = infoDict[WsParam.name]
        params[WsParam.email] = infoDict[WsParam.email]
        params[WsParam.mobileNumber] = ""
        params[WsParam.password] = ""
        params[WsParam.gender] = infoDict[WsParam.gender]
        params[WsParam.socialNetwork_type] = infoDict[WsParam.socialNetwork_type]
        params[WsParam.socialNetworkId] = infoDict[WsParam.socialNetworkId]
        
        DoviesWSCalls.callSingupWS(dicParam: params, onSuccess: {[weak self] (isSuccess, message, response) in
            guard let weakSelf = self else{return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
            if isSuccess, let userInfo = response{
                if let userID = UserDefaults.standard.value(forKey: UserDefaultsKey.logoutId) as? String,   userID != userInfo[WsParam.customerUserName] as? String{
                    GlobalUtility.removePref(key: UserDefaultsKey.logoutId)
                    DoviesGlobalUtility.deleteCasheForNewUser()
                }
                UserDefaults.standard.set(userInfo, forKey: UserDefaultsKey.userData)
                DoviesGlobalUtility.currentUser = User.init(fromDictionary: userInfo)
                DoviesGlobalUtility.setFilterData()
                guard let window = UIApplication.shared.windows.first else { return }
                window.rootViewController = Storyboard.Main.storyboard().instantiateInitialViewController()
				
				let centerView = window.rootViewController as! SideMenuController
				let viewCont = centerView.centerViewController
				if viewCont != nil{
					DoviesGlobalUtility.showSimpleAlert(viewController: viewCont!, message: message, completion: nil)
				}
				
            }else if weakSelf.socialDict != nil{
                weakSelf.fillDetailsBasedOnSocialInfo()
            }else{
                DoviesGlobalUtility.showSimpleAlert(viewController: weakSelf, message: message, completion: nil)
            }
        }) { [weak self](error) in
            guard let weakSelf = self else{return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
        }
    }
  */
}

extension UITapGestureRecognizer {
	
	func didTapAttributedTextInLabel(label: UILabel, inRange targetRange: NSRange) -> Bool {
		// Create instances of NSLayoutManager, NSTextContainer and NSTextStorage
		let layoutManager = NSLayoutManager()
		let textContainer = NSTextContainer(size: CGSize.zero)
		let textStorage = NSTextStorage(attributedString: label.attributedText!)
		
		// Configure layoutManager and textStorage
		layoutManager.addTextContainer(textContainer)
		textStorage.addLayoutManager(layoutManager)
		
		// Configure textContainer
		textContainer.lineFragmentPadding = 0.0
		textContainer.lineBreakMode = label.lineBreakMode
		textContainer.maximumNumberOfLines = label.numberOfLines
		let labelSize = label.bounds.size
		textContainer.size = labelSize
		
		// Find the tapped character location and compare it to the specified range
		let locationOfTouchInLabel = self.location(in: label)
		let textBoundingBox = layoutManager.usedRect(for: textContainer)
		let textContainerOffset = CGPoint(x: (labelSize.width - textBoundingBox.size.width) * 0.5 - textBoundingBox.origin.x,
											  y:(labelSize.height - textBoundingBox.size.height) * 0.5 - textBoundingBox.origin.y);
		let locationOfTouchInTextContainer = CGPoint(x: locationOfTouchInLabel.x - textContainerOffset.x,
														  y: locationOfTouchInLabel.y - textContainerOffset.y);
		let indexOfCharacter = layoutManager.characterIndex(for: locationOfTouchInTextContainer, in: textContainer, fractionOfDistanceBetweenInsertionPoints: nil)
		
		return NSLocationInRange(indexOfCharacter, targetRange)
	}
	
}

extension RegistrationViewController: TTTAttributedLabelDelegate {
	func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
		if url.absoluteString == "URL_TERMS_AND_CONDITIONS"{
			self.btnTermsTapped()
		}
		else if url.absoluteString == "URL_PRIVACY_POLICY"{
			self.btnPrivacyPolicyTapped()
		}
	}
}

extension RegistrationViewController{
    func checkEmailAvailibility(name:String){
        
        isNavigateScreen = false
        
        var dic = [String: Any]()
        dic[WsParam.emailAvailable] = name
        DoviesWSCalls.callEmailAvailabilityAPI(dicParam: dic, onSuccess: {[weak self]  (isSuccess, response, message) in
            guard let weakSelf = self else{return}
            if isSuccess, let userInfo = response{
                
                let isAvailable = userInfo["is_available"] as? String ?? ""
                if isAvailable == "1"{
                    
                    weakSelf.signupParams[WsParam.dob] = weakSelf.Str_Dob
                    weakSelf.navigateToGenderSelectionScreen()
                }
                else{
                    weakSelf.showErrorLabelForField(field: weakSelf.txtEmail, message: userInfo["message"] as? String ?? "")
                }
            }
            weakSelf.isNavigateScreen = true
            
        }) { (error) in
            
            self.isNavigateScreen = true
        }
    }
}

