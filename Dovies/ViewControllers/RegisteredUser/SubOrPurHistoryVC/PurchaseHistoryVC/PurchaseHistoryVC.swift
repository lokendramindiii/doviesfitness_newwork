
//
//  PurchaseHistoryVC.swift
//  Dovies
//
//  Created by hb on 20/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class PurchaseHistoryVC: BaseViewController,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet var purchaseTblView: UITableView!
	@IBOutlet weak var emptyDataView : UIView!
    
	var arrPurchaseHistory = [ModelPurchaseHistory]()
    
	var cellCount = 10
	
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Purchase History"
        self.navigationItem.hidesBackButton  = true
        self.addNavigationBackButton()
        self.purchaseTblView.tableFooterView = UIView()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		if arrPurchaseHistory.count == 0{
			cellCount = 10
			DispatchQueue.main.async { [weak self] in
				guard let weakSelf = self else{return}
				weakSelf.purchaseTblView.reloadData()
				weakSelf.purchaseTblView.layoutIfNeeded()
				weakSelf.purchaseTblView.showLoader()
			}
			self.callPurchaseHistoryAPI()
		}
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
	@objc func addNavigationRightButton(){
		let button: UIButton = UIButton(type: UIButtonType.custom)
		button.setImage(UIImage(named: "btn_twitter"), for: UIControlState.normal)
		let barButton = UIBarButtonItem(customView: button)
		//assign button to navigationbar
		self.navigationItem.rightBarButtonItem = barButton
	}
     // MARK: WSCalls Methods
	
    /**
     This method used to fetch purchase history and update the tableview.
     */
	func callPurchaseHistoryAPI(){
		DoviesWSCalls.callpurchaseHistoryAPI(dicParam: nil, onSuccess: { (success, arrModelPurchaseHistory, msg) in
			if success{
				self.arrPurchaseHistory = arrModelPurchaseHistory
			}
			else{
				self.cellCount  = 0
			}
            self.emptyDataView.isHidden = self.arrPurchaseHistory.count > 0
			self.purchaseTblView.hideLoader()
			self.purchaseTblView.reloadData()
		}) { (error) in
			
		}
	}
     // MARK: UITableview DataSource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return arrPurchaseHistory.count > 0 ? arrPurchaseHistory.count : cellCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "PurchaseExeCell") as! PurchaseExeCell
		if arrPurchaseHistory.count > 0{
			cell.setPurchaseHistoryData(model: arrPurchaseHistory[indexPath.row])
		}
		return cell
       
    }
     // MARK: UITableview Delegate Methods
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76
    }
}
