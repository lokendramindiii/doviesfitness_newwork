//
//  BuildWorkoutPlanCell.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

//This cell used to show build workout plan
class BuildWorkoutPlanCell: UITableViewCell {

    enum WorkOutCellType {
        case recoveryDay
        case workoutDay
    }
    
    var cellType : WorkOutCellType = .recoveryDay
    var workoudDayModel : WorkoutDayModel!
    var workoutIndexPath : IndexPath!
    var isFromViewPlan : Bool = false
    var isFromEditAdmin : Bool = false

    @IBOutlet weak var viewRecoveryDay : UIView!
    @IBOutlet weak var viewWorkOutDay : UIView!
    @IBOutlet weak var viewRestDay : UIView!

    @IBOutlet weak var imgWorkout : UIImageView!
    @IBOutlet weak var imgRestDay : UIImageView!

    @IBOutlet weak var img_Sepratorline : UIImageView!
    @IBOutlet weak var img_HorizontalLine : UIImageView!


    @IBOutlet weak var lblWorkoutTime : UILabel!
    @IBOutlet weak var lblTitle  : UILabel!
    @IBOutlet weak var lblRestDay  : UILabel!

    @IBOutlet weak var lblWorkOutType : UILabel!
    @IBOutlet weak var lblRecoveryDayTitle : UILabel!
    @IBOutlet weak var lblRecoveryDayTitle_Rest : UILabel!

    @IBOutlet weak var lblRDayTitle : UILabel!
    

    @IBOutlet weak var btnEdit : UIButton!
    @IBOutlet weak var btnDelete : UIButton!
    @IBOutlet weak var btnWorkoutStatus : UIButton!
    @IBOutlet weak var btnWorkoutEdit : UIButton!
    
    @IBOutlet weak var editStackView : UIStackView!
    @IBOutlet weak var lblWeekNumber : UILabel!
    
    var viewWorkoutTapped:((IndexPath)->())?
    var deleteTapped:((IndexPath)->())?
    var editTapped:((IndexPath)->())?
    var workoutStatusTapped:((IndexPath) -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
   
    // -----
    func setCellWith(workOut : WorkoutDayModel , for indexPath : IndexPath){
        workoudDayModel = workOut
        workoutIndexPath = indexPath
        if isFromEditAdmin
        {
            viewWorkOutDay.backgroundColor = UIColor.black
            viewRecoveryDay.backgroundColor = UIColor.black
            img_Sepratorline.isHidden = true
            
            lblWeekNumber.textColor = UIColor(red:184.0/255.0, green: 184.0/255.0, blue:184.0/255.0, alpha: 1.0)
            
            lblWorkoutTime.textColor =  UIColor(red:100.0/255.0, green: 100.0/255.0, blue:100.0/255.0, alpha: 1.0)
            
            lblTitle.textColor = UIColor.white
            lblTitle.font = UIFont.tamilBold(size: 14)

            lblWorkOutType.textColor =  UIColor(red:100.0/255.0, green: 100.0/255.0, blue:100.0/255.0, alpha: 1.0)
            
            lblRDayTitle.textColor = UIColor(red:184.0/255.0, green: 184.0/255.0, blue:184.0/255.0, alpha: 1.0)
            
            lblRecoveryDayTitle.textColor = UIColor(red:100.0/255.0, green: 100.0/255.0, blue:100.0/255.0, alpha: 1.0)

            img_HorizontalLine.backgroundColor = UIColor(red:100.0/255.0, green: 100.0/255.0, blue:100.0/255.0, alpha: 1.0)
            
            }
        
        cellType = workoudDayModel.programWorkoutFlag.toBool()! ? .workoutDay : .recoveryDay
        viewRecoveryDay.isHidden = cellType != .recoveryDay
        viewWorkOutDay.isHidden = cellType != .workoutDay
        switch cellType {
        case .recoveryDay:
            setViewForRecoveryDay()
        case .workoutDay:
            setViewForWorkoutWeekDay()

        }
        
    }

    
    func setViewForRecoveryDay(){
        
        if isFromEditAdmin
        {
            viewRestDay.isHidden = false
            viewRecoveryDay.isHidden = true
            btnEdit.setImage(UIImage(named: "pluce_top_icon_s25"), for: .normal)
            btnEdit.setImage(UIImage(named: "pluce_top_icon_s25_h"), for: .highlighted)
            btnEdit.isHidden = true
            lblRDayTitle.isHidden = true
            lblRecoveryDayTitle.isHidden = true
            lblRestDay.text = "REST DAY"
            lblRecoveryDayTitle_Rest.text =  "Day \(workoutIndexPath.row + 1)"
            
            imgWorkout.isHidden = true
            imgRestDay.isHidden = false
        }
        else{
            //imgRestDay.isHidden = true
            //imgWorkout.isHidden = false
            
            imgRestDay.isHidden = false
            imgWorkout.isHidden = true
            
            
            // btnEdit.setImage(UIImage(named: "pluce_top_icon_s25"), for: .normal)
            //  btnEdit.setImage(UIImage(named: "pluce_top_icon_s25_h"), for: .highlighted)
            
            btnEdit.setImage(UIImage(named: "pluce_top_icon_s25_h"), for: .normal)
            btnEdit.setImage(UIImage(named: "pluce_top_icon_s25"), for: .highlighted)
            
            btnEdit.isHidden = isFromViewPlan
            lblRDayTitle.isHidden = false
            lblRecoveryDayTitle.text = workoudDayModel.programWorkoutName
            
            lblRDayTitle.text =  "Day \(workoutIndexPath.row + 1)"
            // imgWorkout.sd_setImage(with: URL(string: workoudDayModel.programWorkoutImage), placeholderImage: AppInfo.placeholderImage)
            print("lblRDayTitle.textlblRDayTitle.textlblRDayTitle.text\(lblRDayTitle.text)")
            
            print("workoudDayModel.programWorkoutImage\(workoudDayModel.programWorkoutImage)")

            imgWorkout.sd_setImage(with: URL(string: workoudDayModel.programWorkoutImage), placeholderImage: nil)
            
        }
    }
    
    func setViewForWorkoutWeekDay(){
        btnEdit.setImage(UIImage(named: "edit_icon_top_s24"), for: .normal)
        btnEdit.setImage(UIImage(named: "edit_icon_top_s24_h"), for: .highlighted)
        lblTitle.text = workoudDayModel.programWorkoutName
        lblWeekNumber.text = "Day \(workoutIndexPath.row + 1)"
        
        let fulltime : [String] = workoudDayModel.programWorkoutTime.components(separatedBy: ":")
        
        if fulltime.count > 0
        {
            if fulltime[0].range(of:"min") != nil {
                lblWorkoutTime.text = fulltime[0]
            }
            else
            {
                lblWorkoutTime.text = fulltime[0] + " min"
            }
        }
        else
        {
            lblWorkoutTime.text = workoudDayModel.programWorkoutTime
        }

        lblWorkOutType.text = workoudDayModel.programWorkoutGoodFor
      //  imgWorkout.sd_setImage(with: URL(string: workoudDayModel.programWorkoutImage), placeholderImage: AppInfo.placeholderImage)
       
        // 23-12-2019
        if workoudDayModel.programWorkoutImage != ""
            
        {
            imgRestDay.isHidden = true
            imgWorkout.isHidden = false
        }
        
        imgWorkout.sd_setImage(with: URL(string: workoudDayModel.programWorkoutImage), placeholderImage: nil)

        print(workoudDayModel.programWorkoutImage)
        
        
        btnWorkoutStatus.isSelected = workoudDayModel.programWorkoutStatus.toBool() ?? false
        if isFromViewPlan{
            btnWorkoutEdit.isHidden = true
            btnDelete.setImage(UIImage(named: "right_arrow_s25"), for: .normal)
            btnDelete.setImage(UIImage(named: "right_arrow_s25_h"), for: .highlighted)
            btnWorkoutStatus.isHidden = false
            editStackView.spacing = 5
            
            if workoudDayModel.programWorkoutStatus == "1"
            {
                btnWorkoutStatus.isHidden = false
            }
            else
            {
                btnWorkoutStatus.isHidden = true
            }
            
        }else{
            editStackView.spacing = 0
            btnWorkoutEdit.isHidden = false
            btnWorkoutStatus.isHidden = true
        }
        

    }
    
    @IBAction func deleteButtonTapped(_ sender : UIButton){
        if isFromViewPlan{
            viewWorkoutTapped?(workoutIndexPath)
        }else{
            deleteTapped?(workoutIndexPath)
        }
    }
    
    @IBAction func editButtonTapped(_ sender : UIButton){
        editTapped?(workoutIndexPath)
    }
    
    @IBAction func btnWorkoutStatusTapped(_ sender : UIButton){
        workoutStatusTapped?(workoutIndexPath)
    }
    
    @IBAction func btnWorkoutEditTapped(_ sender : UIButton){
        editTapped?(workoutIndexPath)
    }
    
}
