//
//  LocalNotification.swift
//  CairnXL
//
//  Created by hb1 on 06/11/17.
//  Copyright © 2017 Hidden Brains. All rights reserved.
//

import UIKit
import UserNotifications

class LocalNotification: NSObject, UNUserNotificationCenterDelegate {
    
    class func registerForLocalNotification(on application:UIApplication) {
		if #available(iOS 10.0, *) {
			UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
				(granted, error) in
				//Parse errors and track state
			}
		}
		else{
			if (UIApplication.instancesRespond(to: #selector(UIApplication.registerUserNotificationSettings(_:)))) {
				let notificationCategory:UIMutableUserNotificationCategory = UIMutableUserNotificationCategory()
				notificationCategory.identifier = "com.dovies.doviesapp.notification"
				
				//registerting for the notification.
				application.registerUserNotificationSettings(UIUserNotificationSettings(types:[.sound, .alert, .badge], categories: nil))
			}
		}
    }
    
    class func dispatchlocalNotification(with title: String, body: String, userInfo: [AnyHashable: Any]? = nil, at date:Date) {
        
        if #available(iOS 10.0, *) {
            
            
            let center = UNUserNotificationCenter.current()
            center.removeAllPendingNotificationRequests()
            let content = UNMutableNotificationContent()
            content.title = title
            content.body = body
            content.categoryIdentifier = "com.dovies.doviesapp.notification"
            
            if let info = userInfo {
                content.userInfo = info
            }
            
            content.sound = UNNotificationSound.default()
            let locale = Locale(identifier: "en_US")
            var calendar = Calendar.current
            if let aName = NSTimeZone(name: "GMT") {
                calendar.timeZone = aName as TimeZone
            }
            calendar.locale = locale
            let components: DateComponents = calendar.dateComponents([.hour, .minute, .second], from: date)
            
            let trigger = UNCalendarNotificationTrigger(dateMatching: components, repeats: true)
            
            let request = UNNotificationRequest(identifier: UUID().uuidString, content: content, trigger: trigger)
            
            center.add(request) { (error) in
                print(error as Any)
            }
            
        } else {
            UIApplication.shared.cancelAllLocalNotifications()
            let notification = UILocalNotification()
            notification.fireDate = date
            notification.alertTitle = title
            notification.alertBody = body
            
            if let info = userInfo {
                notification.userInfo = info
            }
            notification.repeatInterval = NSCalendar.Unit.day
            notification.soundName = UILocalNotificationDefaultSoundName
            UIApplication.shared.scheduleLocalNotification(notification)
            
        }
        
        print("WILL DISPATCH LOCAL NOTIFICATION AT ", date, Date())
        
    }
}

extension Date {
    func addedBy(seconds:Int) -> Date {
        return Calendar.current.date(byAdding: .second, value: seconds, to: self)!
    }
}
