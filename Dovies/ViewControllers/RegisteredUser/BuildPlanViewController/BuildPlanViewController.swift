//
//  BuildPlanViewController.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit
import SJSegmentedScrollView
import NVActivityIndicatorView


var activityIndicatorNew : NVActivityIndicatorView!

//This view controller used to add and edit plan
class BuildPlanViewController: BaseViewController ,BuildPlan_DetailDelegate {
   
    
    enum PlanScreenType {
        case planView
        case planEdit
        case planAdd
    }
    
    //vijay
    @IBOutlet weak var activityIndicatorView : NVActivityIndicatorView!{
        didSet{
            activityIndicatorView.padding = 20
            activityIndicatorView.color = UIColor.lightGray
            activityIndicatorView.type = .circleStrokeSpin
        }
    }

   @IBOutlet weak var loaderView   : UIView!

    var planScreenType              : PlanScreenType = .planView
    var segmentController           : SJSegmentedViewController = SJSegmentedViewController()
    var selectedSegment             : SJSegmentTab?
    var workoutVc                   : WorkoutPlanVC?
    
    var BuildPlanDetail            :BuildPlan_Detail?
    
    var buildPlanModel              : BuildPlanModel?
    var dietPlanVc                  : BuildDietPlanVC!
    var workoutPlanVc               : BuildWorkoutPlanVC!
    var descriptionVc               : BuildPlanDescriptionVC!
    var IsadminNewworkout           : Bool = false
    var isFromEditAdmin             : Bool = false
    var isFromfavrouitePlan             : Bool = false

    static var isDataChanged        : Bool = false
    var planId                      : String?

    var currentSegmentIndex         : Int = 0
    
    var updateOnDeletePlan:((String)->())?
    var updateOnFavStateChange:((String,Bool)->())?
    
    
    var viewBlackIntialLoad         : UIView!
    
    //MARK: ViewLifeCycle methods
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        viewBlackIntialLoad = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: SCREEN_HEIGHT))
        viewBlackIntialLoad.backgroundColor = UIColor.appBlackColorNew()
        self.view.addSubview(viewBlackIntialLoad)

        inititalUISetUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //self.activityIndicatorView.startAnimating()
        let frame = CGRect(x: self.view.center.x-22, y: self.view.center.y-22, width: 45, height: 45)
        activityIndicatorNew = NVActivityIndicatorView(frame: frame)
        activityIndicatorNew.center = self.view.center
        activityIndicatorNew.type = . circleStrokeSpin // add your type
        activityIndicatorNew.color = UIColor.lightGray // add your color
        APP_DELEGATE.window?.addSubview(activityIndicatorNew)
        // or use  webView.addSubview(activityIndicator)
        
        activityIndicatorNew.startAnimating()
        
        if isFromEditAdmin || isFromfavrouitePlan
        {
            self.navigationController?.isNavigationBarHidden = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if isFromEditAdmin || isFromfavrouitePlan
        {
            self.navigationController?.isNavigationBarHidden = true
        }
    }
    
    func showSubscriptionScreen(){
        if let upgradeVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "UpgradeToPremiumViewController") as? UpgradeToPremiumViewController{
            upgradeVC.onPurchaseSuccess = {
                //self.callWorkOutDetailAPI()
            }
            upgradeVC.isPresent = true
            
            let subscriptionNav = UINavigationController(rootViewController: upgradeVC)
            
            self.present(subscriptionNav, animated: true, completion: nil)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        activityIndicatorNew.stopAnimating()

        if isFromEditAdmin || isFromfavrouitePlan
        {
          self.navigationController?.isNavigationBarHidden = false
        }
    }
    
    func actionBack_andActionInfo(sender: Int) {
        if sender == 1
        {
            self.navigationController?.popViewController(animated: true)
        }
        else if sender == 2
        {
            self.showActionSheetToPerformOnWorkout()
        }
    }
    
    func GetHeightImage( Height: CGFloat)
    {
        segmentController.headerViewHeight = Height
    }
  
    //MARK: Instance methods
    override func inititalUISetUp() {
        switch planScreenType {
        case .planView:
           // addNavigationBackButton()
            addNavigationWhiteBackButton()
            callPlanDetailsWS()
        case .planEdit:
            self.navigationItem.leftBarButtonItem = GlobalUtility.getNavigationButtonItemWith(target: self, selector: #selector(self.cancelTap), title: "Cancel")
            callPlanDetailsWS()
        case .planAdd:
            self.navigationItem.leftBarButtonItem = GlobalUtility.getNavigationButtonItemWith(target: self, selector: #selector(self.cancelTap), title: "Cancel")
            createSampleDataModel()
        }
    }
    
    func inititalSegmentSetUpWith(selectedIndex : Int = 0){
        guard let model = buildPlanModel else{return}
        self.title = model.customerProgramDetail[0].programName.uppercased()
        if  segmentController.segmentControllers.count > 0{
            selectedSegment = nil
            segmentController.view.removeFromSuperview()
            segmentController.removeFromParentViewController()
            segmentController = SJSegmentedViewController()
        }
        workoutPlanVc = (Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "BuildWorkoutPlanVC")) as! BuildWorkoutPlanVC
        workoutPlanVc.buildPlanModel =  model
        workoutPlanVc.title = "Workout Plan"
        if isFromEditAdmin
        {
            workoutPlanVc.title = "WORKOUT PLAN"
        }
        workoutPlanVc.isFromMyPlan = planScreenType == .planView// isFromMyPlan
        workoutPlanVc.isFromEditAdmin = isFromEditAdmin
        
        dietPlanVc = (Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "BuildDietPlanVC")) as! BuildDietPlanVC
        dietPlanVc.isFromEditAdmin = isFromEditAdmin
        dietPlanVc.title = "Diet Plan"
        if isFromEditAdmin
        {
            dietPlanVc.title = "DIET PLAN"
        }
        
        dietPlanVc.buildPlanModel =  model
        dietPlanVc.isFromMyPlan = planScreenType == .planView //isFromMyPlan
        descriptionVc = (Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "BuildPlanDescriptionVC")) as! BuildPlanDescriptionVC
        descriptionVc.isFromEditAdmin = isFromEditAdmin
        descriptionVc.title = "Plan Overview"
        if isFromEditAdmin
        {
            descriptionVc.title = "PLAN INFO"
        }
        descriptionVc.isFromMyPlan = planScreenType == .planView //isFromMyPlan
        descriptionVc.buildPlanModel =  model
        
        switch planScreenType {
        case .planView:
            segmentController.headerViewController = UIViewController()
            BuildPlanDetail = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "BuildPlan_Detail") as? BuildPlan_Detail
            
            BuildPlanDetail?.isFavrouitePlanVC = isFromfavrouitePlan
            
            BuildPlanDetail?.programName =  model.customerProgramDetail[0].programName.uppercased()
            
            BuildPlanDetail?.ProgrameImage = model.customerProgramDetail[0].programImage
            
            BuildPlanDetail?.delegate = self
            segmentController.headerViewController = BuildPlanDetail!
            
        case .planEdit, .planAdd:
            workoutVc = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "WorkoutPlanVC") as? WorkoutPlanVC
            segmentController.headerViewController = workoutVc!
            if planScreenType == .planEdit{
                workoutVc?.buildPlan = self.buildPlanModel
            }
            
            if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
            {
                if self.navigationController?.navigationBar.isTranslucent == true
                {
                    if IS_IPHONE_X
                    {
                        segmentController.headerViewHeight = 660
                    }
                    else
                    {
                        segmentController.headerViewHeight =  630
                    }
                }
                else
                {
                    if IS_IPHONE_XSMAX_XR
                    {
                        segmentController.headerViewHeight = 580
                    }
                    else
                    {
                        segmentController.headerViewHeight = 570
                    }
                }
            }
            else
            {
                if self.navigationController?.navigationBar.isTranslucent == true
                {
                    if IS_IPHONE_X
                    {
                        segmentController.headerViewHeight = 370
                    }
                    else
                    {
                       segmentController.headerViewHeight = 351
                    }
                }
                else
                {
                    if IS_IPHONE_XSMAX_XR
                    {
                        segmentController.headerViewHeight = 295
                    }
                    else
                    {
                        segmentController.headerViewHeight = 290
                    }
                }
            }
        }
        
        segmentController.segmentControllers = [workoutPlanVc ,dietPlanVc, descriptionVc]
        segmentController.segmentViewHeight = 44.0
        segmentController.segmentBounces = false
        segmentController.delegate = self
        
        if isFromEditAdmin
        {
            segmentController.segmentBackgroundColor = UIColor.black
            segmentController.segmentTitleColor = UIColor.segmentTitleColor()
            segmentController.selectedSegmentViewColor = UIColor.white
            segmentController.selectedSegmentViewHeight = 0
            
            segmentController.segmentTitleFont = UIFont.tamilBold(size: 14.0)
            segmentController.segmentShadow = SJShadow.dark()
            
        }
        else
        {
            segmentController.segmentShadow = SJShadow.light()
            segmentController.segmentTitleColor = UIColor.segmentTitleColor()
            segmentController.selectedSegmentViewColor = UIColor.appBlackColor()
            segmentController.selectedSegmentViewHeight = 2.0
            
            segmentController.segmentTitleFont = UIFont.tamilRegular(size: 14.0)
            
        }
        addChildViewController(segmentController)
        self.view.addSubview(segmentController.view)
        segmentController.didMove(toParentViewController: self)
        segmentController.setSelectedSegmentAt(selectedIndex, animated: false)
        if planScreenType == .planView{
            addRightNavigationButton()
        }
    }
    func addRightNavigationButton(){
        
        navigationItem.rightBarButtonItems = [GlobalUtility.getNavigationRightButtonItem(target: self, selector: #selector(self.showActionSheetToPerformOnWorkout), imageName: "btn_more").0!]

    }
    
    /**
     This method opens the actionsheet to select the options where user can share, edit or delete the workout.
     */
 
    
    @objc func showActionSheetToPerformOnWorkout(){
        guard let model = buildPlanModel else{return}
        let arrData: [alertActionData]!
        
        arrData = [
            alertActionData(title: "Edit", imageName: "btn_edit_s21a", highlighedImageName : "btn_edit_s21a"),
            alertActionData(title: "Share", imageName: "btn_share", highlighedImageName : "btn_share"),
            alertActionData(title: model.customerProgramDetail[0].programFavStatus == false ? "Favourite" : "Unfavourite", imageName: model.customerProgramDetail[0].programFavStatus == false ? "btn_fav_s21a" : "favorite_icon_s34_h", highlighedImageName : "favorite_icon_s34_h"),
            alertActionData(title: "Delete", imageName: "delete_btn_s27b_h", highlighedImageName : "delete_btn_s27b_h")
        ]
        
        DoviesGlobalUtility.showDefaultActionSheet(on:self,actionData: arrData,cancelTitle: "Cancel") { (clickedIndex:Int, isCancel:Bool) in
            
            switch clickedIndex{
                
            case 0:
                guard let plan = self.buildPlanModel else{return}
                let buildPlanVC = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "BuildPlanViewController") as! BuildPlanViewController
                
                if DoviesGlobalUtility.isSubscribed == true || DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName{
                    
                    buildPlanVC.planId = plan.customerProgramDetail[0].programId
                    buildPlanVC.planScreenType = .planEdit
                    self.navigationController?.pushViewController(buildPlanVC, animated: true)
                    
                }else{
                    
                    self.showSubscriptionScreen()
                }
                
            case 1:
                guard let model = self.buildPlanModel else {return}
//                DoviesGlobalUtility.generateBranchIoUrl(with: ModuleName.program, moduleId: model.customerProgramDetail[0].programId, completionHandler: { (urlString) in
//                    DoviesGlobalUtility.showShareSheet(on: self, shareText: urlString)
//                })
                
                 DoviesGlobalUtility.showShareSheet(on: self, shareText: model.customerProgramDetail[0].programShareUrl)
                
            case 2:
                model.customerProgramDetail[0].programFavStatus = !model.customerProgramDetail[0].programFavStatus
                self.updateOnFavStateChange?(model.customerProgramDetail[0].programId, model.customerProgramDetail[0].programFavStatus)
                self.callFavouriteAPI()
                
            case 3:
                self.deletePlanAction()
            default:
                break
            }
            
        }
    }

    /**
     This method is to show a confirmation alert before deleting the work plan.
     */
    
    func deletePlanAction(){
        UIAlertController.showAlert(in: self, withTitle: AlertTitle.appName, message: AlertMessages.deleteConfirmation, cancelButtonTitle: "No", destructiveButtonTitle: "Delete", otherButtonTitles: nil) { (alertControl, action, index) in
            if index == 1{
                self.callDeletePlanAPI()
            }
        }
    }
    
    func redirectViewBasedOnCreateStatus(){
        switch planScreenType {
        case .planView:
            self.navigationController?.popViewController(animated: true)
        case .planEdit:
            checkCancelAction()
        case .planAdd:
            checkCancelAction()
        }
    }
    
    func checkCancelAction(){
        if BuildPlanViewController.isDataChanged == true{
            if planScreenType != .planView{
                UIAlertController.showAlert(in: (APP_DELEGATE.window?.rootViewController)!, withTitle: "", message: AlertMessages.cancleCreatePlan, cancelButtonTitle: "No", destructiveButtonTitle: "Yes", otherButtonTitles: nil, tap: { (alertCont, action, index) in
                    if index == 1{
                        self.redirectAction()
                    }
                })
            }
        }else{
            redirectAction()
        }
    }
    
    func redirectAction(){
        switch planScreenType {
        case .planView:
            self.navigationController?.popViewController(animated: true)
        case .planEdit:
            self.navigationController?.popViewController(animated: true)
        case .planAdd:
            redirectForCreateView()
        }
    }
    
    func redirectForCreateView(){
        if let viewControllers = self.navigationController?.viewControllers{
            for vc in viewControllers{
                if let myplansVc = vc as? MyPlanVC{
                    myplansVc.isReload = true
                    self.navigationController?.popToViewController(myplansVc, animated: true)
                    return
                }
            }
            let myPlanVC = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "MyPlanVC") as! MyPlanVC
            myPlanVC.isReload = true
            self.navigationController?.viewControllers.insert(myPlanVC, at: 1)
            self.navigationController?.popToViewController(myPlanVC, animated: true)
        }
    }
    
    override func btnBackTapped(sender: UIButton) {
        redirectViewBasedOnCreateStatus()
    }
    
    @objc func cancelTap(){
        redirectViewBasedOnCreateStatus()
    }
    
    //MARK: - Model creation
    func createSampleDataModel(){
        guard buildPlanModel == nil else{return}
        buildPlanModel = BuildPlanModel(fromDictionary: getBuildPlanDict())
        self.inititalSegmentSetUpWith(selectedIndex: 0)
    }
    
    
    func getBuildPlanDict()->[String : Any]{
        var planDict = [String : Any]()
        planDict["get_program_workouts"]  = getProgramWorkout()
        planDict["get_customer_program_detail"] = [getCustomerProgramDeatils()]
        planDict["get_program_diet_plans"] = [Any]()
        return planDict
    }
    
    func getCustomerProgramDeatils()->[String: Any]{
        var programDict = [String : Any]()
        programDict["is_program_favourite"] = "0"
        programDict["program_access_level"] = "OPEN"
        programDict["program_amount"] = "$0.00"
        programDict["program_description"] = ""
        programDict["program_equipments"] = ""
        programDict["program_full_image"] = ""
        programDict["program_good_for"] = ""
        programDict["program_id"] = "0"
        programDict["program_image"] = ""
        programDict["program_name"] = "Workout Plan"
        programDict["program_share_url"] = ""
        programDict["program_week_count"] = "4"
        return programDict
    }
    
    func getProgramWorkout() -> [String : Any]{
        var dictProgramDict = [String : Any]()
        for i in 1...4{
            let weekDict = getSampleWeekModel(with: "\(i)")
            dictProgramDict["Week\(i)"] = weekDict
        }
        return dictProgramDict
    }
    
    func getSampleWeekModel(with weekNumber : String)-> [[String : Any]]{
        var arrDays = [[String : Any]]()
        for i in 0...6{
            let dayDict = getSampleDayDict(with: weekNumber, dayNumber: "\(i)")
            arrDays.append(dayDict)
        }
        return arrDays
    }
    
    func getSampleDayDict(with weekNumber : String , dayNumber : String)->[String : Any]{
        var dayDict = [String : Any]()
        dayDict["program_day_number"] = dayNumber
        dayDict["program_week_number"] = weekNumber
        dayDict["program_workout_flag"] = "0"
        dayDict["program_workout_good_for"] = ""
        dayDict["program_workout_id"] = "0"
        dayDict["program_workout_image"] = ""
        dayDict["program_workout_name"] = "Rest Day"
        dayDict["program_workout_status"] = "0"
        dayDict["program_workout_time"] = "0:"
        dayDict["workout_id"] = "0"
        return dayDict
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension BuildPlanViewController : SJSegmentedViewControllerDelegate{
    func didMoveToPage(_ controller: UIViewController, segment: SJSegmentTab?, index: Int) {
        currentSegmentIndex = index
        if selectedSegment != nil {
            selectedSegment?.titleColor(.lightGray)
        }
        
        if segmentController.segments.count > 0 {
            selectedSegment = segmentController.segments[index]
            if isFromEditAdmin
            {
            selectedSegment?.titleColor(.white)
            }
            else
            {
                selectedSegment?.titleColor(.black)
            }
        }
        switch planScreenType {
        case .planView:
            addRightNavigationButton()
        case .planEdit, .planAdd:
            self.navigationItem.rightBarButtonItems = [GlobalUtility.getNavigationRightButtonItem(target: self, selector: #selector(self.saveTapped), imageName: "save_btn_s27b_h").0!]
   
        }
        
    }
    
    @objc func saveTapped()
    {

        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        {
            if IsadminNewworkout == true
            {
                guard let vc = self.workoutVc else{return}
                vc.validator.validate(self)
                return
            }
            if checkForImageValidation()
            {
            }
            else
            {
              return
            }
            let arrData:[alertActionData] =
                [alertActionData(title: "Save as new workout plan", imageName: "ico_workout_tab_NewBlack", highlighedImageName : "ico_workout_tab_NewBlack"),
                 alertActionData(title: "Overwrite current workout plan", imageName: "btn_edit_s21a", highlighedImageName : "btn_edit_s21a")
            ]
            DoviesGlobalUtility.showDefaultActionSheet(on:self,actionData: arrData,cancelTitle: "CANCEL") { (clickedIndex:Int, isCancel:Bool) in
                if clickedIndex == 0
                {
                    self.planScreenType = .planAdd
                    guard let vc = self.workoutVc else{return}
                    vc.validator.validate(self)
                }
                else{
                    guard let vc = self.workoutVc else{return}
                    vc.validator.validate(self)
                }
            }
         
        }
        else
        {
            guard let vc = workoutVc else{return}
            vc.validator.validate(self)
        }
        
    }
    
    func getDictToCallCreateAPI() -> [String : Any]{
        var params = [String : Any]()
        params["program_name"] = workoutVc?.txtName.text ?? ""
        params["program_level"] = workoutVc?.workoutPlanModel.lbl_level
        params["program_description"] = getDescription()
        if let planImage = workoutVc?.planImage{
            params["program_image"] = planImage
        }
        params["program_diet_plan"] = getDietPlans()
        params["program_goodfor"] = workoutVc?.arrGoodforId.joined(separator: ",")
        params["program_equipments"] = workoutVc?.arrEquipsId.joined(separator: ",")
        params["workout_list"] = createWorkoutsJosn()
        
        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        {
            params["program_access_level"] = workoutVc?.workoutPlanModel.lbl_AccessLevel
            params["program_amount"] = workoutVc?.txtfldAmount.text ?? ""
            
            params["display_in_news_fedd"] = workoutVc?.workoutPlanModel.lblDisplayInNewsfeed
            params["allow_notification"] = workoutVc?.workoutPlanModel.lblallowNotification
            params["allow_user"] = workoutVc?.arrUserId.joined(separator:  ",")
            params["added_by_type"] = "Admin"
        }
        return params
    }
    
    /**
     This method generates a json string for creating the workout.
     */
    func createWorkoutsJosn() -> String{
        guard let bModel = workoutPlanVc.buildPlanModel else{return ""}
        var arrWorkouts = [[String : Any]]()
        if let weekWorkouts = bModel.programWorkouts.weeksData{
            for dayWorkouts in weekWorkouts{
                for workout in dayWorkouts{
                    var dict = [String : Any]()
                    dict["workout_id"] = workout.workoutId
                    dict["workout_week_num"] = workout.programWeekNumber
                    dict["workout_day_num"] = workout.programDayNumber
                    arrWorkouts.append(dict)
                }
            }
        }
        return "\(json(from: arrWorkouts) ?? "")"
    }
    
    func getDietPlans()->String{
        let ids: [String] = dietPlanVc.arrDietPlans.map { $0.programDietId}
        return ids.joined(separator: ",")
    }
    
    
    func getDescription()->String{
        return descriptionVc.txtOverView.text
    }
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {return nil}
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
}

extension BuildPlanViewController : ValidationDelegate{
    /**
     This method is used to call the create plan API based on add or edit
     */
    
    func validationSuccessful() {
        switch planScreenType {
        case .planView:
            break
        case .planEdit :
            guard let idPlan = buildPlanModel?.customerProgramDetail[0].programId else{return}
          //  var params = getDictToCallCreateAPI()
            if checkForImageValidation(){
                var params = getDictToCallCreateAPI()
                params["program_id"] = idPlan
                
            print("------\(params)")
                callCreatePlanAPI(with: params)
            }
            
         case .planAdd:
            
            if checkForImageValidation(){
                let params = getDictToCallCreateAPI()
                print("------\(params)")

                callCreatePlanAPI(with: params)
            }
        }

    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        
    }
    
    func checkForImageValidation() -> Bool{
        guard let wVc = workoutVc else{return false}
        if wVc.planImage == nil{
            wVc.img_StarPhoto.image = UIImage(named: "img_star")
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyImageForPlan, completion: nil)
            return false
        }
        
       else if let text =  wVc.txtName.text, text.isEmpty{
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyPlanName, completion: nil)
            return false
        }
       
        else if  let text =  wVc.lblLevel.text, text.isEmpty || workoutVc?.workoutPlanModel.lbl_level == ""{
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyLevel, completion: nil)
            return false

        }
       else if let text = wVc.lblGoodFor.text, text.isEmpty{
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyWorkoutGoodFor, completion: nil)
            return false
        }
        else  if let text = wVc.lblEquipments.text, text.isEmpty{
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyWorkoutEquipments, completion: nil)
            return false

        }
        else  if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        {
             if let text = wVc.lblAccesslevel.text, text.isEmpty || workoutVc?.workoutPlanModel.lbl_AccessLevel == "" {
                DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyAccessLevel, completion: nil)
                return false
            }
                
             if  wVc.IsAmount{
                if let text =  wVc.txtfldAmount.text, text.isEmpty{
                    DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyPlanAmount, completion: nil)
                    return false
                }
            }
                
            else if let text = wVc.lblDisplayNewsfeed.text, text.isEmpty || workoutVc?.workoutPlanModel.lblDisplayInNewsfeed == ""{
                DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyDisplayInNewfeed, completion: nil)
                return false
            }

            else if let text = wVc.lblAllowNotification.text, text.isEmpty || workoutVc?.workoutPlanModel.lblallowNotification == ""{
                DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyAllowNotification, completion: nil)
                return false
            }
        }
        
        return true
    }
}

//API Calls
extension BuildPlanViewController{
   
    /**
     This method is used to call the api for getting the plan details
     */
    func callPlanDetailsWS(){
        guard let user = DoviesGlobalUtility.currentUser,let pId = planId else{return}
        //loaderView.showLoader()
        self.activityIndicatorView.startAnimating()
        var params = [String : Any]()
        params[WsParam.authCustomerId] = user.customerAuthToken
        params[WsParam.programId] = pId
        
        DoviesWSCalls.callGetPlanDetailsAPI(dicParam: params, onSuccess: { [weak self] (success, buildModel, message) in
            guard let weakSelf = self else {return}
            weakSelf.loaderView.hideLoader()
            if success , let buldPlan = buildModel{
                weakSelf.buildPlanModel = buldPlan
                weakSelf.updateModelForEditPlan()
                weakSelf.inititalSegmentSetUpWith(selectedIndex: 0)
            }
        }) { (error) in
             self.loaderView.hideLoader()
        }
    }
    
    /**
     This method updates the edit plan model.
     */
    func updateModelForEditPlan(){
        if planScreenType == .planEdit{
            guard let plan = buildPlanModel else{return}
            plan.customerProgramDetail[0].programWeekCount = "4"
            plan.programWorkouts.weeksData = createWeeksDataBasedOnWeekCount(with: plan.programWorkouts.weeksData)
        }
    }
    
    func createWeeksDataBasedOnWeekCount(with planWorkoutWeeks : [[WorkoutDayModel]]) -> [[WorkoutDayModel]]{
        let numberOfWeeksRemain = 4 - planWorkoutWeeks.count
        var weeksData = planWorkoutWeeks
        if numberOfWeeksRemain > 0{
            for i in 1...numberOfWeeksRemain{
                let daysArray = getSampleWeekModel(with: "\(planWorkoutWeeks.count + i)")
                var daysList = [WorkoutDayModel]()
                for dict in daysArray{
                    let weekD = WorkoutDayModel(fromDictionary: dict)
                    daysList.append(weekD)
                }
                weeksData.append(daysList)
            }
        }
        print(weeksData)
        return weeksData
        
    }
    
    /**
     This method is used to add program to favourite list
     */
    func callFavouriteAPI(){
        guard let cUser = DoviesGlobalUtility.currentUser,let model = buildPlanModel else{return}
        var parameters = [String : Any]()
        parameters[WsParam.authCustomerId] =  cUser.customerAuthToken
        parameters[WsParam.moduleId] = model.customerProgramDetail[0].programId
        parameters[WsParam.moduleName] = ModuleName.program
        
        DoviesWSCalls.callFavouriteAPI(dicParam: parameters, onSuccess: {(success, message, response) in
            if !success{
                model.customerProgramDetail[0].programFavStatus = !model.customerProgramDetail[0].programFavStatus
            }
        }) { (error) in
            model.customerProgramDetail[0].programFavStatus = !model.customerProgramDetail[0].programFavStatus
        }
    }
    /**
     This method used to delete the program from favourite list
     */
    func callDeletePlanAPI(){
        guard let user = DoviesGlobalUtility.currentUser,let pId = planId else{return}
        loaderView.showLoader()
        var params = [String : Any]()
        params[WsParam.authCustomerId] = user.customerAuthToken
        params[WsParam.programId] = pId
        DoviesWSCalls.callDeletePlanAPI(dicParam: params, onSuccess: { [weak self](success, response, message) in
            guard let weakself = self else{return}
            if success{
                weakself.updateOnDeletePlan?(weakself.planId!)
                weakself.navigationController?.popViewController(animated: true)
            }
        }) { (error) in
            
        }
    }
    
    /**
     This method used to create a plan based on user information
     - Parameter params : is having the plan details to create a plan for the week.
     */
    func callCreatePlanAPI(with params : [String : Any]){
        guard DoviesGlobalUtility.currentUser != nil else{return}
        GlobalUtility.showActivityIndi(viewContView: self.view)
        DoviesWSCalls.callCreatePlanAPI(dicParam: params, onSuccess: { [weak self](success, message, response) in
            guard let weakSelf = self else {return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
            weakSelf.redirectOnPlanCreateOrUpdate()
            if success , let planDict = response{
				GlobalUtility.showToastMessage(msg: "Done")
                print(planDict )
            }
        }) { (error) in
            GlobalUtility.hideActivityIndi(viewContView: self.view)
        }
        
    }
    
    func redirectOnPlanCreateOrUpdate(){
        if let viewControllers = self.navigationController?.viewControllers{
            for vc in viewControllers{
                if let myplansVc = vc as? MyPlanVC{
                    myplansVc.isReload = true
                    self.navigationController?.popToViewController(myplansVc, animated: true)
                    return
                }
            }
            let myPlanVC = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "MyPlanVC") as! MyPlanVC
            myPlanVC.isReload = true
            self.navigationController?.viewControllers.insert(myPlanVC, at: 1)
            self.navigationController?.popToViewController(myPlanVC, animated: true)
        }
     }
}
