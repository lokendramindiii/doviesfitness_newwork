//
//  NewsFeedHorizontalTableCell.swift
//  Dovies
//
//  Created by Neel Shah on 16/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class NewsFeedHorizontalTableCell: UITableViewCell {

	@IBOutlet weak var recommendedCollection: UICollectionView!
	@IBOutlet weak var btnRightIndicator: UIButton!
    @IBOutlet weak var ViewBackground: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
    
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
