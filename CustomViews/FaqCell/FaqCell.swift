//
//  FaqCell.swift
//  Dovies
//
//  Created by HB-PC on 25/04/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
class FaqCell: UITableViewCell {

    @IBOutlet weak var lblQuestion : UILabel!
    @IBOutlet weak var lblAnswer: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        self.lblAnswer.text = "";
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func set(content: FaqModel, shouldExpand:Bool) {
        self.lblQuestion.text = content.faqQuestion
        //self.lblAnswer.text = shouldExpand == true ? content.faqAnswer : nil
        if shouldExpand
        {
            
            let faqAnswer = content.faqAnswer.replacingOccurrences(of: "&apos;", with: "'")

            let attributedString = NSMutableAttributedString(string: faqAnswer)
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 5.0
            attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
            
            self.lblAnswer.attributedText = attributedString
        }
        else
        {
            self.lblAnswer.text = "";
        }
    }
}
