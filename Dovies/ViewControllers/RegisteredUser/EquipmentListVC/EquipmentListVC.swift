//
//  EquipmentListVC.swift
//  Dovies
//
//  Created by hb on 23/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class EquipmentListVC: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet var equipTblview: UITableView!
    var  arrFilterData: [FilterGroup] = DoviesGlobalUtility.arrFilterData
    var selected: [String] = [String]()
    var arrIdData = [String]()
    var completion : ((_ selected: [String]?,_ arrEquipID: [String]?) -> ())?
    var arrListing = [FilterDataModel]()
    
    //MARK: ViewLifeCycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Equipments".uppercased()
        self.navigationItem.hidesBackButton  = true
       // self.addNavigationBackButton()
        addNavigationWhiteBackButton()
        self.equipTblview.tableFooterView = UIView()
        self.getArraylisting()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    func getArraylisting(){
        let arrValue = arrFilterData.filter {$0.groupName == "Equipments"}
        if arrValue.count > 0{
            self.arrListing = arrValue[0].list
        }
    }
    
    // MARK: UITableviewDatasource Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrListing.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EquipmentListCell") as! EquipmentListCell
        cell.btnSelection.tag = indexPath.row
        cell.btnSelection.isSelected = arrIdData.contains(arrListing[indexPath.row].gmGoogforMasterId)
        cell.setData(arrListing[indexPath.row], at: indexPath)
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        return cell
    }
    
    // MARK: UITableviewDelegate Methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if arrIdData.contains(arrListing[indexPath.row].gmGoogforMasterId){
            if let index = self.arrIdData.index(where: {$0 == arrListing[indexPath.row].gmGoogforMasterId}){
                self.arrIdData.remove(at: index)
                self.selected.remove(at: index)
            }
        }else{
            self.arrIdData.append(arrListing[indexPath.row].gmGoogforMasterId)
            self.selected.append(arrListing[indexPath.row].gmDisplayName)
        }
        equipTblview.reloadRows(at: [indexPath], with: .none)
    }
    
    @objc func buttonAction(_ sender: UIButton!) {
        print("Button tapped")
    }
    
    @IBAction func btnDoneClicked(_ sender: Any) {
        self.completion?(self.selected, self.arrIdData)
        self.navigationController?.popViewController(animated: true)
    }
}
