//
//  DietPlansListVC.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

//This view controller used to show list of diet plans in respective category
class DietPlansListVC: BaseViewController {

    @IBOutlet weak var collectionV : BaseCollectionView!
    var activityIndicatorNew : NVActivityIndicatorView!

    static let cellIndentifier = "DietPlansListCell"
    static let cellPadding : CGFloat = 5.0
   // let cellSizeValue : CGFloat  = (ScreenSize.width - (ProgramsViewController.cellPadding))/2
  //  let cellSizeValue : CGFloat  = (ScreenSize.width - 1)/2
    
    let cellSizeValue : CGFloat  = (ScreenSize.width - 62)/2

    var headerHeight : CGFloat = 80
    var placeholderCellsCount = 0
    
    var dietPlans  = [DietPlan]()
    var dietPlanStaticData : DietPlanStaticData!
    var selectedCategory : DietPlanCategory?
    var selectedPlan : DietPlan?
    var programDietPlans : [ProgramDietPlan]?
    
    var programId : String?
    
    var shouldLoadMore = false
    var currentPageIndex = 1
	
	@IBOutlet weak var IBlblNoData: UILabel!
    
    //MARK: - View Controller life cycle
    override func viewDidLoad() {
		super.viewDidLoad()
        

        inititalUISetUp()
        addNavigationWhiteBackButton()
        setupActivityIndicator()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //load data
        
      //  self.navigationController?.navigationBar.barTintColor = UIColor.appNavColor()
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,
                                                                        NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]

        
		if dietPlans.count == 0{
//			DispatchQueue.main.async {
//				self.placeholderCellsCount = 10
//				self.collectionV.reloadData()
//				self.collectionV.layoutIfNeeded()
//				self.collectionV.showLoader()
//        	}
            self.IBlblNoData.isHidden = true
			callGetDietPlanListAPI()
    	}
	}
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        
    //    self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor : UIColor.colorWithRGB(r: 34.0, g: 34.0, b: 34.0),
            NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]
    }
   
    override  func inititalUISetUp() {
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 20
        layout.minimumLineSpacing = 20
        self.collectionV.collectionViewLayout = layout
        
        
        
        self.collectionV.register(UINib(nibName: DietPlansListVC.cellIndentifier, bundle: nil), forCellWithReuseIdentifier: DietPlansListVC.cellIndentifier)
        collectionV.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
        
        if let selectedPlan = selectedCategory{
            self.title = selectedPlan.dietPlanCategoryName.uppercased()
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        collectionV.collectionViewLayout.invalidateLayout()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        activityIndicatorNew.stopAnimating()
    }
    
    
    func setupActivityIndicator() {
         let frame = CGRect(x: self.view.center.x-22, y: self.view.center.y-22, width: 45, height: 45)
        activityIndicatorNew = NVActivityIndicatorView(frame: frame)
        activityIndicatorNew.center = self.view.center
        activityIndicatorNew.type = . circleStrokeSpin // add your type
        activityIndicatorNew.color = UIColor.lightGray // add your color
        APP_DELEGATE.window?.addSubview(activityIndicatorNew)
        activityIndicatorNew.startAnimating()
    }
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dietPlanVc = segue.destination as? DietPlanDetailsVC{
            dietPlanVc.selectedPlan = selectedPlan
            dietPlanVc.programId = programId
            dietPlanVc.programDietPlans = programDietPlans
            dietPlanVc.onPurchaseSuccess = {
                self.selectedPlan?.dietPlanAccessLevel = "OPEN"
                self.collectionV.reloadData()
            }
        }
    }
    
    //MARK: - didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension DietPlansListVC : UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dietPlans.count > 0 ? dietPlans.count : placeholderCellsCount

    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DietPlansListVC.cellIndentifier, for: indexPath) as? DietPlansListCell  else{return UICollectionViewCell()}
        if indexPath.item < dietPlans.count{
            cell.setCellUI(with: dietPlans[indexPath.item])
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
            if indexPath.item == (dietPlans.count - 1) && shouldLoadMore == true {
                callGetDietPlanListAPI()
            }

    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            guard let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,withReuseIdentifier: "DietPlansCollectionHeader", for: indexPath) as? DietPlansCollectionHeader else{return UICollectionReusableView()}
            if  let category = selectedCategory{
                headerView.lblTitle.text = "Overview"
                //headerView.lblDescription.text = category.dietPlanCategoryDescription
                //headerView.lblDescription.characterSpacing = 0.5
                
                /* loks comment
                let attributedString = NSMutableAttributedString(string: category.dietPlanCategoryDescription)
                let paragraphStyle = NSMutableParagraphStyle()
                paragraphStyle.lineSpacing = 7.0
                attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
                headerView.lblDescription.attributedText = attributedString
 */
                
               let dietplancateDescription = category.dietPlanCategoryDescription.replacingOccurrences(of: "&apos;", with: "'")
                
                headerView.lblDescription.text = dietplancateDescription
         //       headerView.lblDescription.makeSpacingWithLeftAlignment(lineSpace: 9.0, font: UIFont.tamilRegular(size: 14), color: UIColor(r: 136, g: 136, b: 136, alpha: 1))
                
                headerView.lblDescription.makeSpacingWithLeftAlignment(lineSpace: 9.0, font: UIFont.tamilRegular(size: 14), color: UIColor.white)

                
            }else{
                headerView.lblTitle.text = ""
                headerView.lblDescription.text = ""
            }
            return headerView
            
        case UICollectionElementKindSectionFooter:
            guard let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,withReuseIdentifier: BaseCollectionView.collectionViewLoadMoreIdentifier, for: indexPath) as? CollectionLoadMoreView else{return UICollectionReusableView()}
            return footerView
            
        default:
            fatalError("this is NOT should happen!!")
        }
    }
}

extension DietPlansListVC : UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        selectedPlan = dietPlans[indexPath.item]
        self.performSegue(withIdentifier: "gotoDietPlanDetails", sender: nil)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if dietPlanStaticData != nil{
            if let headerView = collectionView.visibleSupplementaryViews(ofKind: UICollectionElementKindSectionHeader).first as? DietPlansCollectionHeader {
                headerView.layoutIfNeeded()
                headerHeight = headerView.contentView.systemLayoutSizeFitting(UILayoutFittingExpandedSize).height
                return CGSize(width: collectionView.frame.width, height: headerHeight)
            }
            return CGSize(width: collectionView.frame.width, height: headerHeight)
            
        }else{
            return CGSize.zero
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return shouldLoadMore ? CGSize(width: ScreenSize.width, height: HeightConstants.baseCollectionViewFooterHeight) : CGSize.zero
        
    }
}

extension DietPlansListVC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellSizeValue, height: cellSizeValue)
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsets(top: DietPlansListVC.cellPadding, left: 0, bottom: DietPlansListVC.cellPadding, right: 0)
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    }

    
}

//API Calls
extension DietPlansListVC{
    
    /**
     This method is used to get diet plans list.
     */
    func callGetDietPlanListAPI(){
        guard let category = selectedCategory , let cUser = DoviesGlobalUtility.currentUser else{return}
        
        var parameters = [String : Any]()
        parameters[WsParam.authCustomerId] =  cUser.customerAuthToken
       // parameters[WsParam.authCustomerSubscription] = "Free"//Change after payment
        
        parameters[WsParam.authCustomerSubscription] =  DoviesGlobalUtility.currentUser?.customerUserType ?? ""
        
        parameters[WsParam.dietPlanCategoryId] = category.dietPlanCategoryId
        parameters[WsParam.pageIndex] = "\(currentPageIndex)"

        DoviesWSCalls.callGetDietPlanListAPI(dicParam: parameters, onSuccess: {[weak self] (success, dietPlansList,dietPlanData, message, hasNextPage) in
            guard let weakSelf = self else{return}
            weakSelf.placeholderCellsCount = 0
            weakSelf.collectionV.hideLoader()
            weakSelf.shouldLoadMore = hasNextPage
            weakSelf.activityIndicatorNew.stopAnimating()
            if success, let plans = dietPlansList{
                if weakSelf.currentPageIndex == 1{
                    weakSelf.dietPlans = plans
                }else{
                    weakSelf.dietPlans += plans
                }

                weakSelf.dietPlanStaticData = dietPlanData
                if hasNextPage{
                    weakSelf.currentPageIndex += 1
                }
                self?.IBlblNoData.isHidden = true
				self?.view.sendSubview(toBack: (self?.IBlblNoData)!)
            }else{
                self?.IBlblNoData.isHidden = false
                self?.view.bringSubview(toFront: (self?.IBlblNoData)!)
               
            }

            weakSelf.collectionV.reloadData()
        }) { (error) in
            self.activityIndicatorNew.stopAnimating()
        }
    }
}

//This collection resuable view is used to show header view of plan list
class DietPlansCollectionHeader: UICollectionReusableView {
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblDescription : CustomLabel!
    @IBOutlet weak var contentView: UIView!
    
}



