//
//  WorkoutExeVC.swift
//  Dovies
//
//  Created by hb on 23/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import SJSegmentedScrollView
import SideMenuController
import IQKeyboardManagerSwift

class WorkoutExeVC: BaseViewController,UISearchBarDelegate,SJSegmentedViewControllerDelegate {
    
    @IBOutlet weak var tblFilter : UITableView!
    
    var selectedSegment: SJSegmentTab?
    let segmentController = SJSegmentedViewController()
    var exerciseLibraryVC : ExeLibraryVC!
    var workoutDetailsVC : WorkoutDetailsVC!
    var backgroundImage : UIImage?
    var shadowImage : UIImage?
    var searchButton: UIButton!
    var filterButton : UIButton!
    var filterSearchData = [FilterDataModel]()
    var arrSeachedFilter = [FilterDataModel]()
    var searchBar : UISearchBar!
    var isFromCreateWorkout = false
    var isFromWorkoutExeVC = true
    var darkMode = false
    var SegmentBackground : UIView!

    @IBOutlet weak var lblNoRecord: UILabel!
    var selectIndexForFavourite = 0
   

    //MARK: ViewLifeCycle methods
    override func viewDidLoad() {

        DoviesGlobalUtility.setFilterData()
        super.viewDidLoad()
        self.navigationController?.navigationBar.isTranslucent = false

        self.setNeedsStatusBarAppearanceUpdate()

        self.navigationController?.removeSideMenuButton()
        
        DispatchQueue.main.async {
            self.addRightNavigationButton()
        }

        self.inititalSegmentSetUp()
        
        if isFromCreateWorkout {
            self.addNavigationWhiteBackButton()
        } else {
            self.addTitleWithImage()
        }
        
        for model in DoviesGlobalUtility.arrFilterData{
			if model.groupKey != "filter_good_for"{
            	filterSearchData.append(contentsOf: model.list)
			}
        }
		
        print(filterSearchData)
        view.bringSubview(toFront: tblFilter)
        tblFilter.backgroundColor = UIColor.appBlackColorNew()
        tblFilter.register(UINib(nibName: "SearchTableCell", bundle: nil), forCellReuseIdentifier: "SearchTableCell")
        tblFilter.contentInset = UIEdgeInsetsMake(0, 0, 176, 0)
        

    }
    

    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
 
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
         self.navigationController?.navigationBar.isTranslucent = false
        UITextField.appearance().keyboardAppearance = UIKeyboardAppearance.dark

        let barButtonItem = UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self])
        barButtonItem.title = NSLocalizedString("Cancel", comment: "")
        barButtonItem.setTitleTextAttributes([.font            : UIFont.robotoRegular (size: 18.0),
                                              .foregroundColor : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)], for: .normal)

        
    //   self.navigationController?.navigationBar.barTintColor = UIColor.appNavColor()

        let MyprofileNav = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
        APP_DELEGATE.profileNavController = MyprofileNav.navigationController.self

        self.navigationController?.isNavigationBarHidden = false
        
         IQKeyboardManager.shared.enable = false
        self.backgroundImage = self.navigationController?.navigationBar.backgroundImage(for: .default)
        self.shadowImage = self.navigationController?.navigationBar.shadowImage
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)

        self.navigationController?.navigationBar.shadowImage = UIImage()
    }

    override func viewDidLayoutSubviews() {
        self.navigationController?.navigationBar.isTranslucent = false

    }
    

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isTranslucent = true
        UITextField.appearance().keyboardAppearance = UIKeyboardAppearance.light


        let barButtonItem = UIBarButtonItem.appearance(whenContainedInInstancesOf: [UISearchBar.self])
        barButtonItem.title = NSLocalizedString("Cancel", comment: "")
        barButtonItem.setTitleTextAttributes([.font            : UIFont.robotoRegular (size: 18.0),
                                              .foregroundColor : #colorLiteral(red: 0.6274509804, green: 0.6274509804, blue: 0.6274509804, alpha: 1)], for: .normal)

        IQKeyboardManager.shared.enable = true
    self.navigationController?.navigationBar.setBackgroundImage(self.backgroundImage, for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = self.shadowImage
        
        // Navigation--Color
      //  self.navigationController?.navigationBar.barTintColor = UIColor.white
        
    }
  
    //MARK: WebService methods
    
    func inititalSegmentSetUp(){
        

        if let storyboard = self.storyboard {
            
            exerciseLibraryVC = (storyboard.instantiateViewController(withIdentifier: "ExeLibraryVC")) as! ExeLibraryVC
            
            exerciseLibraryVC.isFromCreateWorkout = self.isFromCreateWorkout
            
            exerciseLibraryVC.title = "Exercise Library"
            
            var secondVC: UIViewController!
            if isFromCreateWorkout {
                let vc = (Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "WorkoutDetailsVC")) as! WorkoutDetailsVC
                vc.isFromCreateWorkout = self.isFromCreateWorkout
                vc.screenType = .favoriteWorkoutList
                vc.Str_UpcomingFavrouite = "Favorite Exercises"
                workoutDetailsVC = vc
                secondVC = vc
                secondVC.title = "Favorite Exercises"
            } else {
                secondVC = (storyboard.instantiateViewController(withIdentifier: "FeaturesVC"))
                secondVC.title = "Workout Collections"
            }
            
            segmentController.headerViewController = UIViewController()
            segmentController.segmentControllers = [exerciseLibraryVC, secondVC]
            segmentController.headerViewHeight = 0
          //  segmentController.selectedSegmentViewHeight = 4.0
            segmentController.selectedSegmentViewHeight = 3.0

            segmentController.headerViewOffsetHeight = 0
            
// llll          segmentController.segmentViewHeight = 45
            
//   llll          segmentController.segmentTitleColor     = UIColor.colorWithRGB(r: 119, g: 119, b: 119)
//
//  llll           segmentController.selectedSegmentViewColor = UIColor.colorWithRGB(r: 255, g: 80, b: 52)
            
            segmentController.segmentTitleColor     = UIColor.colorWithRGB(r: 119, g: 119, b: 119)
            
            segmentController.selectedSegmentViewColor = UIColor.colorWithRGB(r: 255, g: 255, b: 255)


            segmentController.segmentBackgroundColor =  UIColor.appBlackColorNew()
            
            segmentController.segmentShadow = SJShadow.light()
            
          //  segmentController.segmentShadow = SJShadow.darkblack()

            segmentController.segmentBounces = true
            segmentController.delegate = self
            segmentController.segmentTitleFont = UIFont.tamilBold(size: 15.0)
            
            
            addChildViewController(segmentController)
            self.view.addSubview(segmentController.view)
            
            segmentController.didMove(toParentViewController: self)
         
            segmentController.setSelectedSegmentAt(selectIndexForFavourite, animated: false)
        }
 }
    
    func didMoveToPage(_ controller: UIViewController, segment: SJSegmentTab?, index: Int) {
        if searchButton != nil{
            if index == 0{
                addRightNavigationButton()
            }
            else{
                if isFromCreateWorkout{
                    addNavigationsDoneButton()
                }else{
                    navigationItem.rightBarButtonItems = nil
                    navigationItem.leftBarButtonItem = nil
                }
            }
        }
        
        if selectedSegment != nil {
          //  selectedSegment?.titleColor(.lightGray)
            
            selectedSegment?.titleColor(.colorWithRGB(r: 119, g: 119, b: 119))

        }
        
        if segmentController.segments.count > 0 {
            selectedSegment = segmentController.segments[index]
            //selectedSegment?.titleColor(.black)
            
            selectedSegment?.titleColor(.white)

        }
    }
    
    //MARK: - Functions
    func addRightNavigationButton(){
       let navButtons =  GlobalUtility.getNavigationRightButtonItem(target: self, selector: #selector(self.didTapSearchButton), imageName: "btn_searchNew")
        searchButton = navButtons.1!
        
        filterButton = UIButton(type: .custom)
        filterButton.setImage(UIImage(named:"btn_filterNew"), for: .normal)
        filterButton.setImage(UIImage(named:"btn_filterNew"), for: .highlighted)

        filterButton.addTarget(self, action: #selector(self.didTapFilterButton), for: .touchUpInside)
        filterButton.frame = CGRect(x: 0, y: 0, width: 22, height: 22)
        
        let filterBarItem = UIBarButtonItem(customView: filterButton)
        
        if isFromCreateWorkout {
            navigationItem.rightBarButtonItems = [filterBarItem, navButtons.0!]
        } else
        {
            navigationItem.leftBarButtonItem = navButtons.0
            navigationItem.rightBarButtonItems = [filterBarItem]
        }
    }
    
    func addNavigationsDoneButton(){
        navigationItem.rightBarButtonItems = [GlobalUtility.getNavigationButtonItemWithExerciseLibrary(target: self, selector: #selector(self.saveTapped), title: "Done")]
    }
    
    @objc func saveTapped(){
        workoutDetailsVC.saveTapped()
    }
    
    func addTitleWithImage(){
        
//        let logo = UIImage(named: "logo_topbar")
//        let imageView = UIImageView(image:logo)
//        self.navigationItem.titleView = imageView
        
        // create workout header btn creation
        let logo = UIImage(named: "Icon_CreateWorkoutNew")
        let createWorkout:UIButton = UIButton(frame: CGRect(x: 24, y: 0, width: 40, height: 40))
        createWorkout.setImage(logo, for: .normal)
        createWorkout.addTarget(self, action:#selector(self.CreateWorkoutTapped), for: .touchUpInside)
        self.navigationItem.titleView = createWorkout
     //   self.navigationController?.navigationBar.barTintColor = UIColor.appNavColor()

    }
    
    
    @objc func CreateWorkoutTapped() {
        
        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        {
            
            let CreateWorkout = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "CreateWorkoutAdminVC")as! CreateWorkoutAdminVC
            CreateWorkoutAdminVC.arrExercise = [Any]()
            CreateWorkout.IsEditedByAdmin = false
        self.navigationController?.pushViewController(CreateWorkout, animated: false)
            
        }
        else
        {
            let CreateWorkout = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "CreateWorkoutVC")as! CreateWorkoutVC
            CreateWorkoutVC.arrExercise = [Any]()
            CreateWorkout.isFromMyProfileCreate = true
        self.navigationController?.pushViewController(CreateWorkout, animated: false)
        }
    }
    
    
    @objc func didTapFilterButton(sender: AnyObject){
        let Filter = self.storyboard?.instantiateViewController(withIdentifier: "FilterVC")as! FilterVC
        Filter.isFromCreateWorkout = self.isFromCreateWorkout
        Filter.isFromWorkoutExeVC = self.isFromWorkoutExeVC
        self.navigationController?.pushViewController(Filter, animated: false)
    }
    
    @objc func didTapSearchButton(sender: AnyObject){
        
        arrSeachedFilter = [FilterDataModel]()
        tblFilter.reloadData()
        navigationItem.leftBarButtonItem = nil;
        navigationItem.rightBarButtonItem = nil;
        navigationItem.titleView?.isHidden = true
        searchBar = UISearchBar(frame:CGRect(origin: CGPoint(x: 40,y :0), size: CGSize(width: self.view.frame.size.width - 40, height: 44)))
        searchBar.backgroundColor = UIColor.clear
        searchBar.placeholder = "Search"
        searchBar.showsCancelButton = true
        searchBar.delegate = self
        
        if let txfSearchField = searchBar.value(forKey: "_searchField") as? UITextField {
         //   txfSearchField.backgroundColor = UIColor.colorWithRGB(r: 222, g: 222, b: 222)
             txfSearchField.backgroundColor = UIColor.colorWithRGB(r: 38, g: 38, b: 38)
            txfSearchField.textColor = UIColor.white


        }
        let leftNavBarButton = UIBarButtonItem(customView:searchBar)
        self.navigationItem.rightBarButtonItem = leftNavBarButton
        self.navigationItem.leftBarButtonItem = nil;
        
        searchBar.tintColor = UIColor.colorWithRGB(r: 142, g: 142, b: 147)

        searchBar.becomeFirstResponder()
        if isFromCreateWorkout {
            self.navigationItem.hidesBackButton = true
        }
        view.bringSubview(toFront: self.lblNoRecord)
        
    }
    
    //MARK: -  Searchbar Delegate Methods
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        // Do some search stuff
        DispatchQueue.main.async {
            self.arrSeachedFilter = self.filterSearchData.filter({$0.gmDisplayName.lowercased().contains(searchText.lowercased())})
            self.tblFilter.reloadData()
        }
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        tblFilter.isHidden = false
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
       tblFilter.isHidden = true
       searchBar.resignFirstResponder()
        lblNoRecord.isHidden = true
        let workoutDetailsVC = (Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "WorkoutDetailsVC")) as! WorkoutDetailsVC
        workoutDetailsVC.searchString  = searchBar.text
        workoutDetailsVC.isFromCreateWorkout = self.isFromCreateWorkout
    self.navigationController?.pushViewController(workoutDetailsVC, animated: false)
        
    }
    
    internal func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        // Stop doing the search stuff
        // and clear the text in the search bar
        searchBar.isHidden = true
        tblFilter.isHidden = true
        searchBar.text = ""
        // Hide the cancel button
        searchBar.showsCancelButton = false
        if isFromCreateWorkout {
            self.addNavigationWhiteBackButton()
        } else {
            
            // add side menut button dovies 03-05-19
         // self.navigationController?.addSideMenuButton()
            
            self.addTitleWithImage()
        }
        self.addRightNavigationButton()
        self.lblNoRecord.isHidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}


extension WorkoutExeVC : UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard  indexPath.row < arrSeachedFilter.count else{return}
        let workoutDetailsVC = (Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "WorkoutDetailsVC")) as! WorkoutDetailsVC
        workoutDetailsVC.searchFilterData  = arrSeachedFilter[indexPath.row]
        lblNoRecord.isHidden = true
        workoutDetailsVC.isFromCreateWorkout = self.isFromCreateWorkout
      self.navigationController?.pushViewController(workoutDetailsVC, animated: false)
    }
}

extension WorkoutExeVC: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.arrSeachedFilter.count == 0,searchBar != nil,let text = searchBar.text, !text.isEmpty {
//            self.lblNoRecord.isHidden = false
			self.lblNoRecord.isHidden = true
        }else{
            self.lblNoRecord.isHidden = true
        }
        return arrSeachedFilter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchTableCell") as! SearchTableCell
        if indexPath.row < arrSeachedFilter.count{
            cell.lblTitle.text = arrSeachedFilter[indexPath.row].gmDisplayName
            cell.lblGroupName.text = arrSeachedFilter[indexPath.row].groupName
        }
        return cell
    }
}
