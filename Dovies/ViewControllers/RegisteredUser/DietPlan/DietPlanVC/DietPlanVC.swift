//
//  DietPlanVC.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

//This view controller used to show different diet plans list
class DietPlanVC: BaseViewController {
    
    @IBOutlet weak var IBTableview : UITableView!

    var pullToRefreshCtrl:UIRefreshControl!
    
    static let cellIndentifier = "DietPlanCollectionCell"
    static let cellPadding : CGFloat = 5.0
   // let cellSizeValue : CGFloat  = (ScreenSize.width - (ProgramsViewController.cellPadding))/2
     let cellSizeValue : CGFloat  = (ScreenSize.width - 1 )/2
    var placeholderCellsCount = 0
    var headerHeight : CGFloat = 75
    var dietPlanCategoies = [DietPlanCategory]()
    var selectedDietCategory : DietPlanCategory?
    var dietPlanStaticData : DietPlanStaticData?
    
    var programDietPlans : [ProgramDietPlan]?
    var programId : String?
    var isFromBuildPlan : Bool = false
    
    var shouldLoadMore = false
    var Ispulltorefresh = false

    var currentPageIndex = 1


    @IBOutlet weak var IBlblNoData: UILabel!

    //MARK: ViewLifeCycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        inititalUISetUp()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setNeedsStatusBarAppearanceUpdate()
        
        
        if self.navigationController?.isNavigationBarHidden == true
        {
            self.navigationController?.isNavigationBarHidden = false
        }
        //        self.navigationController?.navigationBar.barTintColor = UIColor.appNavColor()
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,
                                                                        NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]
        
        Ispulltorefresh = false
        if dietPlanCategoies.count == 0{
            DispatchQueue.main.async { [weak self] in
                guard let weakSelf = self else{return}
                
                // lokendra   weakSelf.placeholderCellsCount = 10
                weakSelf.placeholderCellsCount = 0
                weakSelf.IBTableview.reloadData()
                weakSelf.IBTableview.layoutIfNeeded()
                weakSelf.IBTableview.showLoader()
            }
            currentPageIndex = 1
            self.callGetDietPlanListAPI()
        }
    }
    
  
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    

    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        //UIApplication.shared.statusBarStyle = .default
  // Navigation--Color
//        self.navigationController?.navigationBar.barTintColor = UIColor.white
//        self.navigationController?.navigationBar.titleTextAttributes = [
//            NSAttributedStringKey.foregroundColor : UIColor.colorWithRGB(r: 34.0, g: 34.0, b: 34.0),
//            NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
       // self.IBTableview.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 20, right: 0)
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
        (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        self.IBTableview.contentInset = UIEdgeInsets(top: topBarHeight  , left: 0, bottom: self.tabBarController!.tabBar.frame.height , right: 0)

    }

    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
//        collectionV.collectionViewLayout.invalidateLayout()
    }
    
    //MARK: Instance methods
    override func inititalUISetUp() {

        

        self.view.backgroundColor = UIColor.appBlackColorNew()
        IBTableview.backgroundColor = UIColor.appBlackColorNew()
        IBTableview.register(UINib(nibName: "DietPlanCell", bundle: nil), forCellReuseIdentifier: "DietPlanCell")
        
       // collectionV.contentInset = UIEdgeInsets(top: 5, left: 0, bottom: 0, right: 0)
        
        self.title = "Diet Plan"
        self.navigationItem.title = "Diet Plan".uppercased()
        if isFromBuildPlan{
            
         self.addNavigationWhiteBackButton()
        
        }
        setPullToRefresh()

    }
    
    func setPullToRefresh(){
        pullToRefreshCtrl = UIRefreshControl()
        pullToRefreshCtrl.addTarget(self, action: #selector(self.pullToRefreshClick(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            IBTableview.refreshControl  = pullToRefreshCtrl
        } else {
        IBTableview.addSubview(pullToRefreshCtrl)
        }
    }
    
    @objc func pullToRefreshClick(sender:UIRefreshControl) {
        sender.endRefreshing()
        if Ispulltorefresh == false
        {
            currentPageIndex = 1
            shouldLoadMore = false
            callGetDietPlanListAPI()
            sender.endRefreshing()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dietPlanVc = segue.destination as? DietPlansListVC{
            dietPlanVc.selectedCategory = selectedDietCategory
            dietPlanVc.programId = programId
            dietPlanVc.programDietPlans = programDietPlans
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension DietPlanVC : UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return dietPlanCategoies.count > 0 ? dietPlanCategoies.count : placeholderCellsCount
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return ScreenSize.width - 40

   //      return ScreenSize.width
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
           guard let cell = tableView.dequeueReusableCell(withIdentifier: "DietPlanCell") as? DietPlanCell else
           {return UITableViewCell() }
        if indexPath.row < dietPlanCategoies.count{
            
            
            cell.setCellUIPlan(with: dietPlanCategoies[indexPath.row])
            
            for subview in cell.imgBackground.subviews {
                subview.removeFromSuperview()
            }
            
            let viewGradient =  UIView(frame: CGRect(x: 0, y: (ScreenSize.width - 40)/2, width:ScreenSize.width - 40, height: (ScreenSize.width - 40)/2))
            let gradientLayer:CAGradientLayer = CAGradientLayer()
            gradientLayer.frame.size =  viewGradient.frame.size
            gradientLayer.colors = [UIColor.clear.cgColor,UIColor.appBlackColorNew().withAlphaComponent(0.5).cgColor]
            viewGradient.layer.addSublayer(gradientLayer)
            cell.imgBackground.addSubview(viewGradient)
        }
        return cell

    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        if indexPath.row == (dietPlanCategoies.count - 1) && shouldLoadMore == true {
            shouldLoadMore = false
            callGetDietPlanListAPI()
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if dietPlanCategoies.count > indexPath.row{
            selectedDietCategory = dietPlanCategoies[indexPath.row]
            self.performSegue(withIdentifier: "gotoDietList", sender: nil)
        }
    }
}


//API Calls
extension DietPlanVC{
    
    /**
     This method is used to call the Diet Plan List api and fetch the list of Diets available for this particular user, and update the Collection View UI.
     */
    
    func callGetDietPlanListAPI(){
        self.Ispulltorefresh = true
        DoviesWSCalls.callGetDietPlanCategoryListAPI(dicParam: [WsParam.pageIndex : "\(currentPageIndex)"], onSuccess: {[weak self] (success, dietCategories, message, hasNextPage,dietPlanStaticContent) in
            guard let weakSelf = self else{return}
            weakSelf.shouldLoadMore = hasNextPage
            if success, let list = dietCategories{
                weakSelf.dietPlanStaticData = dietPlanStaticContent
                if weakSelf.currentPageIndex == 1{
                    weakSelf.dietPlanCategoies = list
                }else{
                   weakSelf.dietPlanCategoies += list
                }
                if hasNextPage{
                    weakSelf.currentPageIndex += 1
                }
                self?.view.sendSubview(toBack: (self?.IBlblNoData)!)
                self?.IBlblNoData.isHidden = true

            }else{
                weakSelf.dietPlanCategoies = dietCategories ?? []
                self?.view.bringSubview(toFront: (self?.IBlblNoData)!)
                self?.IBlblNoData.isHidden = false

            }
            
            weakSelf.placeholderCellsCount = 0
			DispatchQueue.main.async {
            weakSelf.IBTableview.hideLoader()
            weakSelf.loadTableview()
			}
            self?.Ispulltorefresh = false

        }) { (error) in
            self.Ispulltorefresh = false
        }
    }
    
    func loadTableview(){
        self.IBTableview.reloadData()
        self.IBTableview.layoutIfNeeded()
    }
    
}


