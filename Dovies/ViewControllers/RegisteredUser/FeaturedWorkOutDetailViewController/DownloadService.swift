/**
 * Copyright (c) 2017 Razeware LLC
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
 * distribute, sublicense, create a derivative work, and/or sell copies of the
 * Software in any work that is designed, intended, or marketed for pedagogical or
 * instructional purposes related to programming, coding, application development,
 * or information technology.  Permission for such use, copying, modification,
 * merger, publication, distribution, sublicensing, creation of derivative works,
 * or sale is expressly withheld.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import Foundation

// Downloads song snippets, and stores in local file.
// Allows cancel, pause, resume download.
class DownloadService {
	
	var activeWorkoutDownload: [String: Any] = [:]
	var showProgress:((CGFloat, String)->())!
	var updateLabel:((String, String)->())!
	var workoutDownloaded:((String)->())!
	
  // MARK: - Download methods called by TrackCell delegate methods
	
	func checkAllVideoDownloaded(model: ModelWorkoutDetail) -> [String]{
		var mArrVideoUrl = [String]()
		for list in model.workoutExerciseList{
			let theFileName = (list.workoutExerciseVideo as NSString).lastPathComponent
			if theFileName.isEmpty == false{
//				let videoThere = DoviesGlobalUtility.checkWorkOutDownloadedOrNot(folderName: theFileName)
//				if videoThere == false{
					mArrVideoUrl.append(list.workoutExerciseVideo)
//				}
			}
		}
		return mArrVideoUrl
	}
	
	func startWorkoutDownload(_ model: ModelWorkoutDetail)
	{
		let videoUrls = self.checkAllVideoDownloaded(model: model)
		if videoUrls.count > 0{
			downloadWorkout(model, arrExerciseList: videoUrls, index: 0)
		}
	}
	
	func downloadWorkout(_ model: ModelWorkoutDetail, arrExerciseList: [String], index: Int)
	{
		if index == arrExerciseList.count{
			activeWorkoutDownload[model.workoutId] = nil
            
			Workout.saveContactInDB(modelWorkoutDetail: model)
			self.workoutDownloaded?(model.workoutId)
            
			return
		}
        
		activeWorkoutDownload[model.workoutId] = model
		var index = index
		let theFileName = (arrExerciseList[index] as NSString).lastPathComponent
		model.sDownloadLabel = "\(index + 1)/\(arrExerciseList.count)"
		self.updateLabel?(model.sDownloadLabel, model.workoutId)
		if theFileName.isEmpty == false{
			let videoThere = DoviesGlobalUtility.checkWorkOutDownloadedOrNot(folderName: theFileName)
			if videoThere == true{
				index = index + 1
				self.downloadWorkout(model, arrExerciseList: arrExerciseList, index: index)
				return
			}
			ModelVideoListing.downloadVideoForWorkout(sVideoZipUrl: arrExerciseList[index], sFileName: theFileName, workoutId: model.workoutId!, currentProgress: { (progress, url, id) in
				model.progress = CGFloat(progress.fractionCompleted)
				self.showProgress?(model.progress, model.workoutId)
			}, response: { (downloadResponce, id) in
				model.progress = 0.0
				self.showProgress?(model.progress, model.workoutId)
				self.downloadWorkout(model, arrExerciseList: arrExerciseList, index: index)
			})
		}
	}
}
