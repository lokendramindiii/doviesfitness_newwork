//
//  CollectionLoadMoreView.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

class CollectionLoadMoreView: UICollectionReusableView {

    @IBOutlet weak var activityIndicator : UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        activityIndicator.startAnimating()
        // Initialization code
    }
    
}
