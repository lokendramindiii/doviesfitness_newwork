//
//  MedicalConditionsCell.swift
//  Dovies
//
//  Created by HB-PC on 02/05/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class MedicalConditionsCell: UITableViewCell {

    @IBOutlet weak var btnSelection : UIButton!
    @IBOutlet weak var lblTitle : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
