//
//  PlanOverviewVC.swift
//  Dovies
//
//  Created by hb on 16/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class PlanOverviewVC: BaseViewController {

    @IBOutlet weak var lblDescription : UILabel!
    
    //MARK: - View Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.hidesBackButton  = true
       // self.addNavigationBackButton()
        self.addNavigationWhiteBackButton()
        self.navigationItem.title = "My Plan".uppercased()
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white,
                                                                        NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]
   
        
		let attr: [NSAttributedStringKey: Any] = [NSAttributedStringKey.font :UIFont.tamilRegular(size: 14.0)]
        
        let attrStrComment1 = NSMutableAttributedString(string: "You can create your plan by selecting (create a plan), or you can choose from our workout plan menu.\n Creating your plan allows you to choose your workouts you have created or select a workout from our workout collection list. And, you get to choose the days of the week you want to workout and also select the day you want to rest. You have full access to everything. All you have to do is click on the days you want to do specific workout and select the workout and click add. After that, you can add all additional information like equipment, diet plan, and notes or other information in case you want to share your plan with your friends, family or clients. Creating a plan takes a few minutes, and it is effortless to do as well.\n You can also choose a plan from our workout plan menu and edit them according to your needs. Always remember, you can modify any workout plan on this app and even any workout from our workout collections. You have the freedom to customise a workout or a full workout plan according to your level. If you have any question or concern contact us right away. We are here to inspire and motivate you.", attributes: attr)

		
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 9.0
        attrStrComment1.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attrStrComment1.length))
        lblDescription.attributedText = attrStrComment1
        
    }
    
    func showSubscriptionScreen(){
        if let upgradeVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "UpgradeToPremiumViewController") as? UpgradeToPremiumViewController{
            upgradeVC.onPurchaseSuccess = {
                //self.callWorkOutDetailAPI()
            }
            upgradeVC.isPresent = true
            
            let subscriptionNav = UINavigationController(rootViewController: upgradeVC)
            
            self.present(subscriptionNav, animated: true, completion: nil)
        }
    }

    //MARK: - Button Actions
    @IBAction func btnCreatePlan(_ sender: Any) {
        
        if DoviesGlobalUtility.isSubscribed == true || DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName{
            
            let buildPlanVC = self.storyboard?.instantiateViewController(withIdentifier: "BuildPlanViewController")as! BuildPlanViewController
            BuildPlanViewController.isDataChanged = false
            buildPlanVC.planScreenType = .planAdd
            buildPlanVC.IsadminNewworkout = true
            self.navigationController?.pushViewController(buildPlanVC, animated: true)
            
        }else{
         
            self.showSubscriptionScreen()
        }        
    }
    
    @IBAction func btnBuildPlan(_ sender: Any) {
        self.redirectToTab(tabIndex: 3)
        self.navigationController?.popToRootViewController(animated: false)
    }
        
    //MARK: - didReceiveMemoryWarning
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

}
