//
//  FavoritePlanVC.swift
//  Dovies
//
//  Created by hb on 16/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class FavoritePlanVC: FavoriteBaseVC {

	@IBOutlet weak var collectionViewPlan : BaseCollectionView!
	@IBOutlet weak var IBviewExploreProgram: UIView!
	@IBOutlet weak var IBbottomCon: NSLayoutConstraint!
	
	//let cellSizeValue : CGFloat  = (ScreenSize.width - (ProgramsViewController.cellPadding))/2
    let cellSizeValue : CGFloat  = (ScreenSize.width - 1 )/2

    override func viewDidLoad() {
        super.viewDidLoad()
		if IS_IPHONE_X{
			IBbottomCon.constant = 74
		}
		collectionViewPlan.register(UINib(nibName: ProgramsViewController.cellIndentifier, bundle: nil), forCellWithReuseIdentifier: ProgramsViewController.cellIndentifier)
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		if FavoriteVC.currentSelectedSegment == 2{
			self.callGetCustomerFavouritesAPI(apiCallingType: FavouriteApiType.Program.rawValue ,completion: {[weak self] (success) in
				guard let weakSelf = self else { return }
				if success {
					weakSelf.collectionViewPlan.reloadData()
				}
				if weakSelf.favoriteLists?.program == nil || weakSelf.favoriteLists?.program.count == 0{
					weakSelf.IBviewExploreProgram.isHidden = false
				}
				else{
					weakSelf.IBviewExploreProgram.isHidden = true
				}
			})
		}
	}
	
	@IBAction func redirectToProgramm(){
		self.redirectToItsTab(tabIndex: 3)
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
	
    // MARK: Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let programDetailVc = segue.destination as? ProgramDetail_VC {
			programDetailVc.selectedProgram = sender as? Program
		}
    }

}

extension FavoritePlanVC : UICollectionViewDataSource, UICollectionViewDelegate {
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return self.favoriteLists?.program.count ?? 0
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ProgramsViewController.cellIndentifier, for: indexPath) as? ProgramsCollectionCell else{return UICollectionViewCell()}
		
		if let arrPlan = self.favoriteLists?.program {
			cell.setFavCellUI(with: arrPlan[indexPath.item])
		}
		
		return cell
	}

	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		if let arrProgram = self.favoriteLists?.program {
			if arrProgram[indexPath.row].programAccessLevel == "LOCK" {
				self.performSegue(withIdentifier: "favoritePlanToDetail", sender: arrProgram[indexPath.item])
			}
			else if arrProgram[indexPath.row].programAddedInMyDiet{
				let buildPlanVC = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "BuildPlanViewController") as! BuildPlanViewController
				buildPlanVC.planId = arrProgram[indexPath.item].programId
				buildPlanVC.planScreenType = .planView
                buildPlanVC.isFromfavrouitePlan = true
                
				BuildPlanViewController.isDataChanged = false
				self.navigationController?.pushViewController(buildPlanVC, animated: true)
			}
			else{
				self.performSegue(withIdentifier: "favoritePlanToDetail", sender: arrProgram[indexPath.item])
			}
		}
	}
}

extension FavoritePlanVC: UICollectionViewDelegateFlowLayout {
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		//return CGSize(width: cellSizeValue, height: cellSizeValue + 50)
        return CGSize(width: cellSizeValue, height: cellSizeValue)

	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
		return UIEdgeInsets(top: 0, left: 0, bottom: ProgramsViewController.cellPadding, right: 0)
	}
	
}
