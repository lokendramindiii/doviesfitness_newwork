//
//  UIViewController+Storyboard.swift
//  HeartFace
//
//  Created by HiddenBrains 
//  Copyright © 2018 hb. All rights reserved.
//

import UIKit

extension UIViewController {
    //Get Storyboard Identifier
    public static var storyboardIdentifier: String {
        return self.description().components(separatedBy: ".").dropFirst().joined(separator: ".")
    }
    
    func setBackgroundImageBasedOnDevice(imageName:String!, backgroundImage : UIImageView?)
    {
        guard let imageView = backgroundImage else{return}
        switch DoviesGlobalUtility.deviceType
        {
        case .iPhone5:
            imageView.image = UIImage(named: imageName+"-568h")
        case .iPhone6:
            imageView.image = UIImage(named: imageName+"-667h")
        case .iPhone6p:
            imageView.image = UIImage(named: imageName+"-736h")
        case .iPhoneX:
            imageView.image = UIImage(named: imageName+"-812h")
        default:
            imageView.image = UIImage(named: imageName)
        }
    }
}
