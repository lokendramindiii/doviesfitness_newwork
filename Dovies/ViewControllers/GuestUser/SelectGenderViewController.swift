//
//  SelectGenderViewController.swift
//  Dovies
//
//  Created by Mindiii on 9/13/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class SelectGenderViewController: BaseViewController {
    
    @IBOutlet weak var lblGender: UILabel!
    
    @IBOutlet weak var btnFemale: UIButton!
    
    @IBOutlet weak var btnMale: UIButton!
    
    var signupParams : [String:Any]?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        // Do any additional setup after loading the view.
    }
    
    func setupUI()  {
        self.btnMale.layer.cornerRadius = 5
        self.btnMale.layer.borderColor = UIColor.white.cgColor
        self.btnMale.layer.borderWidth = 1
        
        self.btnFemale.layer.cornerRadius = 5
        self.btnFemale.layer.borderColor = UIColor.white.cgColor
        self.btnFemale.layer.borderWidth = 1
        
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionFemale(_ sender: Any) {
        signupParams![WsParam.gender] = "Female"
        navigateToWeightSelectionScreen()
    }
    
    @IBAction func actionMale(_ sender: Any) {
        signupParams![WsParam.gender] = "Male"
        navigateToWeightSelectionScreen()
    }
    
    func navigateToWeightSelectionScreen()  {
        let selectGenderVC = Storyboard.Login.storyboard().instantiateViewController(withIdentifier: "WeightSelectionViewController")as! WeightSelectionViewController
        selectGenderVC.signupParams = self.signupParams
        
        self.navigationController?.pushViewController(selectGenderVC, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
