//
//  AboutYouVC.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

//This view controller used to edit user information
class AboutYouVC: BaseViewController {
    
    let ColorRed = UIColor(r: 228, g: 0, b: 0, alpha: 1)
    let ColorGreen = UIColor(r: 0, g: 196, b: 113, alpha: 1)

    
    @IBOutlet var btnAddPhoto       : UIButton!
    @IBOutlet var btnGender         : UIButton!
    @IBOutlet var btnHeight         : UIButton!
    @IBOutlet var btnWeight         : UIButton!
    @IBOutlet var btnUpdate         : UIButton!

    @IBOutlet var lblErrorUserame   : UILabel!

    @IBOutlet var profileView       : UIImageView!
    @IBOutlet var txtUserName       : DTTextField!
    @IBOutlet var txtDob            : DTTextField!

    @IBOutlet var txtfldName        : DTTextField!
   // @IBOutlet var txtfldNumber      : DTTextField!
    @IBOutlet var txtfldGender      : DTTextField!
    @IBOutlet var txtfldHeight      : DTTextField!
    @IBOutlet var txtfldWeight      : DTTextField!
    @IBOutlet var txtEmail          : DTTextField!
	//@IBOutlet var txtCode           : DTTextField!
    var datePicker = UIDatePicker()
    
    var Str_Dob : String = ""
    var imageUpdated                : Bool = false
    var gender                      : String = "Male"
    var heightFeet                  : String = ""
    var heightInches                : String = ""
    var heightInCentimeters         : String = ""
    var strUserNameText            : String = ""
    let validator                   : Validator = Validator()
	var countryObj                  : Country?
    
    var profileUpdated: (()->())?
    
    //MARK: - View Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "About You".uppercased()
        self.navigationItem.hidesBackButton  = true
       // self.addNavigationBackButton()
        self.addNavigationWhiteBackButton()
        self.setFormValidators()
        
        profileView.contentMode = .scaleAspectFill
        profileView.layer.cornerRadius =  profileView.frame.height / 2
        profileView.isUserInteractionEnabled = true
        profileView.backgroundColor = UIColor.clear
        profileView.setBorder(color: UIColor.appBlackColor(), size: 0.5)
        profileView.addTapGesture(target: self, action: #selector(self.UpdateUserButtonTapped))
        setDetail()
        getUserProfileWSCall()
    }
   

    //MARK: - UI setup
    func setDetail() {
        if let userDetail = DoviesGlobalUtility.currentUser {
            profileView.sd_setImage(with:  URL(string: (userDetail.customerProfileImage)!), placeholderImage: AppInfo.placeholderImage)
            txtUserName.text = userDetail.customerUserName
            txtfldName.text = userDetail.customerFullName
            // add loks code
            let formatter = DateFormatter()
            formatter.dateFormat = "MMM dd yyyy"
             let Str_Dob = formatter.date(from: userDetail.customerDob)
            if Str_Dob != nil
            {
                formatter.dateFormat = "dd MMM, yyyy"
                self.txtDob.text = formatter.string(from: Str_Dob!)

            }

           // txtDob.text = userDetail.cus
           // loks comment txtfldNumber.text = userDetail.customerMobileNumber
            txtfldGender.text = userDetail.customerGender
            txtEmail.text = userDetail.customerEmail
            txtfldHeight.text = getUserHeight(height: userDetail.customerHeight)
            txtfldWeight.text = getUserWeight(weight: userDetail.customerWeight)
			// loks comment txtCode.text = userDetail.customerIsdCode
			countryObj = SRCountryPickerController.getCountryInfo(countryCode: userDetail.customerCountryId)
        }
    }
    
    func getUserHeight(height : String)->String{
        if let units = DoviesGlobalUtility.currentUser?.customerUnits {
            if units.uppercased() == "Imperial".uppercased(){
                let heightunits = Conversions.getFeetAndInchesFromCentimeters(centimerters: height)
                
                if heightunits.0 == "0" && heightunits.1 == "0"
                {
                    return ""
                }
                else {
                    heightFeet = heightunits.0 + "'"
                    heightInches = heightunits.1 + "\""
                    return heightunits.0 + "' " + heightunits.1 + "\""
                }
                
            }else{
                if height.isEmpty == false {
                    heightInCentimeters = height + " cm"
                    return height + " cm"
                } else {
                    return ""
                }
            }
            
        }
        return ""
    }
    
    func getUserWeight(weight : String)->String{
        if let units = DoviesGlobalUtility.currentUser?.customerUnits {
            if units.uppercased() == "Imperial".uppercased(){
                let kgs = Conversions.getLbsFromKgs(kgs: weight)
                if kgs == 0
                {
                    return ""
                } else {
                    return "\(Int(kgs)) lbs"
                }
            } else {
				var yourString: String = ""
				if !weight.isEmpty
				{
					let w: Double = Double(weight)!
					yourString = String(format: "%.2f", w)
				}
                if yourString.isEmpty == false
                {
                    return yourString + " kgs"
                } else {
                    return ""
                }
				
            }
        }
        return ""
    }
    
    //MARK: - Actions
    @IBAction func btnGenderClicked(_ sender: Any) {
        self.view.endEditing(true)
        let mcPicker = McPicker(data: [["Male", "Female"]], selectedTitle: [gender])
        mcPicker.show { [weak self] (selections: [Int : String]) -> Void in
            guard let weakSelf = self else{return}
            if let gender = selections[0]{
                self?.txtfldGender.text = gender
                weakSelf.gender = gender
            }
        }
    }
    
    @IBAction func btnDobClicked(_ sender: Any) {
        self.view.endEditing(true)
        let mcPicker = McPicker(data: [["Male", "Female"]], selectedTitle: [gender])
        mcPicker.show { [weak self] (selections: [Int : String]) -> Void in
            guard let weakSelf = self else{return}
            if let gender = selections[0]{
                self?.txtfldGender.text = gender
                weakSelf.gender = gender
            }
        }
    }
    
    @IBAction func btnWeightClicked(_ sender: Any) {
        self.view.endEditing(true)
        var weight = [String]()
        if let units = DoviesGlobalUtility.currentUser?.customerUnits{
            if units.uppercased() == "imperial".uppercased(){
                for i in 30...500{
                    weight.append("\(i)" + " lbs")
                }
            }else{
                for i in 13...227{
                    weight.append("\(i)" + " kgs")
                }
            }
        }
        
        
        let mcPicker = McPicker(data: [weight], selectedTitle:[self.txtfldWeight.text ?? ""])
        mcPicker.show { [weak self](selections: [Int : String]) -> Void in
            guard self != nil else{return}
            if let weight = selections[0]{
                self?.txtfldWeight.text = weight
                
            }
        }
    }
    
    @IBAction func btnHeightClicked(_ sender: Any){
        self.view.endEditing(true)
        guard let user = DoviesGlobalUtility.currentUser else{return}
        var mcPicker : McPicker?
        if user.customerUnits.uppercased() == "imperial".uppercased(){
            mcPicker = McPicker(data: StaticPickerData.arrHeightInImperial, selectedTitle: [heightFeet,heightInches])
            
        }else{
            mcPicker = McPicker(data: [StaticPickerData.getArrHeightInMetrics()], selectedTitle: [heightInCentimeters])
            
        }
        
        mcPicker?.show {(selections: [Int : String]) -> Void in
            if user.customerUnits.uppercased() == "imperial".uppercased(){
                if let feet = selections[0], let inches = selections[1]{
                    self.txtfldHeight.text = "\(feet) \(inches)"
                    self.heightFeet = feet
                    self.heightInches = inches
                }
                
            }else{
                if let centimeters = selections[0]{
                    self.txtfldHeight.text = centimeters
                    self.heightInCentimeters = centimeters
                }
                
            }
        }
        
    }
    
    @IBAction func btnUpdate(_ sender: Any) {
        validator.validate(self)
    }
    
    @IBAction func btnAddPhotoClicked(_ sender: Any) {
        
        self.UpdateUserButtonTapped()
    }
    
    
    func setFormValidators(){
        validator.styleTransformers(success:{ (validationRule) -> Void in
            if let textField = validationRule.field as? DTTextField {
                textField.hideError()
            }
        }, error:{ (validationError) -> Void in
            if let textField = validationError.field as? DTTextField {
                textField.showError(message: validationError.errorMessage)
            }
        })
    }
    
    @objc func UpdateUserButtonTapped ()
    {
        HBImagePicker.sharedInstance().open(with: MediaTypeImage, actionSheetTitle: "Choose Image", cancelButtonTitle: "Cancel", cameraButtonTitle: "Use Camera", galleryButtonTitle: "Choose existing", canEdit: true) {[weak self] (success, dict) in
            
            guard let weakself = self else {return }
            if success {
                
                if let img = dict?["image"] as? UIImage {
                    weakself.imageUpdated = true
                    weakself.profileView.image = HBImagePicker.sharedInstance().compressImage(img)
                }
            }
        }
    }
    
    func isPhoneNumberValid()->Bool{
       //  loks comment
        
//        if let text = txtfldNumber.text, text.isEmpty{
//            return true
//        }else if let text = txtfldNumber.text{
//            if text.isValidPhoneNumber(){
//                return true
//            }else{
//                GlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.validMobileNumber)
//                return false
//            }
//
//        }else{
            GlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.validMobileNumber)
            return false
      //  }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


//=======For date picker ======
extension AboutYouVC{
    func openTimePicker(_ dateSelectionTextField: UITextField?) {
        datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.maximumDate = Date()
        dateSelectionTextField?.inputView = datePicker
        let toolBar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 44))
        toolBar.tintColor = UIColor.white
        toolBar.barTintColor = UIColor(r: 235, g: 235, b: 235, alpha: 1)
        let cancelBtn = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelTimePicker))
        let doneBtn = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneTimePicker))
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolBar.items = [cancelBtn, space, doneBtn]
        dateSelectionTextField?.inputAccessoryView = toolBar
    }
    
    @objc func cancelTimePicker() {
        self.txtDob.resignFirstResponder()
    }
    
    @objc func doneTimePicker() {
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM dd yyyy"
        Str_Dob = "\(formatter.string(from: datePicker.date))"
        formatter.dateFormat = "dd MMM, yyyy"
        self.txtDob.text = "\(formatter.string(from: datePicker.date))"
        self.txtDob.resignFirstResponder()
    }
    
}


//API Calls
extension AboutYouVC {
    /**
     This method is used to update user information to Dovies.
     
     - Parameter params: is used to for user information.
     */
    func updateUserInfoWSCall(with params:[String: Any]){
		if NetworkReachabilityManager()?.isReachable == true {
			GlobalUtility.showActivityIndi(viewContView: self.view)
		}
        DoviesWSCalls.callUpdateUserInfoAPI(dicParam: params, onSuccess: {[weak self] (isSuccess, message, response) in
            guard let weakSelf = self else{return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
            if isSuccess, let dic = response {
                UserDefaults.standard.set(dic, forKey: UserDefaultsKey.userData)
                DoviesGlobalUtility.currentUser = User.init(fromDictionary: dic)
                weakSelf.profileUpdated?()
                weakSelf.updateSidepanelProfileImage()
            weakSelf.navigationController?.popViewController(animated: true)
				GlobalUtility.showToastMessage(msg: "Done")
            }
			else{
				if let msg = message{
					DoviesGlobalUtility.showSimpleAlert(viewController: weakSelf, message: msg)
				}
			}
            
        }) { [weak self](error) in
            guard let weakSelf = self else{return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
        }
    }
    
    /**
     This method is used to get user information of particular user.
     */
    func getUserProfileWSCall(){
        DoviesWSCalls.callGetUserInfoAPI(dicParam: nil, onSuccess: {[weak self] (isSuccess, message, response) in
            guard let weakSelf = self else{return}
            if isSuccess, let dic = response {
                UserDefaults.standard.set(dic, forKey: UserDefaultsKey.userData)
                DoviesGlobalUtility.currentUser = User.init(fromDictionary: dic)
                weakSelf.profileUpdated?()
                weakSelf.updateSidepanelProfileImage()
            }
        }) { (error) in
        }
    }
    
    /**
     This function updates the profile image once we get user details.
     */
    func updateSidepanelProfileImage(){
        if let viewController = sideMenuController?.sideViewController as? SideMenuViewController{
            viewController.setUserPhotoAndName()
        }
    }
	
	@IBAction func btnCountrySelect(){
		SRCountryPickerHelper.shared.showCountryPicker { (country) in
			self.countryObj = country
			// loks comment self.txtCode.text = country.dial_code
		}
	}
  // api call checkuserAvailibility
    func checkUserAvailibility(name:String ,loginId: String ){
        var dic = [String: Any]()
        dic[WsParam.userAvailable] = name
        dic[WsParam.userId] = loginId
        
        DoviesWSCalls.callUsernameAvailabilityAPI(dicParam: dic, onSuccess: {[weak self]  (isSuccess, response, message) in
            guard let weakSelf = self else{return}
            if isSuccess, let userInfo = response{
                if (weakSelf.txtUserName.text?.count)! > 0{
                    let isAvailable = userInfo["is_available"] as? String ?? ""
                     weakSelf.btnUpdate.isEnabled = (isAvailable == "1") ? true : false
                    weakSelf.lblErrorUserame.text = userInfo["message"] as? String ?? ""
                    weakSelf.lblErrorUserame.textColor = (isAvailable == "1") ? self!.ColorGreen : self!.ColorRed
                    weakSelf.showErroeLabel()
                }
                else{
                    weakSelf.lblErrorUserame.textColor = self!.ColorRed
                    weakSelf.lblErrorUserame.text = AlertMessages.emptyUserName
                    weakSelf.showErroeLabel()
                }
            }
        }) { (error) in
            
        }
    }
}


extension AboutYouVC : ValidationDelegate{
    
    /**
     This method is used to convert
     - feet to centimeters
     - inches to centimeters.
     */
    func getHeight() -> String{
        guard let user = DoviesGlobalUtility.currentUser else{return ""}
        if let text = self.txtfldHeight.text{
            if user.customerUnits.uppercased() == "imperial".uppercased(){
                var height = text.replacingOccurrences(of: "'", with: "")
                height = height.replacingOccurrences(of: "\"", with: "")
                let heightValues = height.components(separatedBy: " ")
                if heightValues.count > 1{
                    let heightInCms = Conversions.feetToCentimeters(depthInFeet: heightValues[0])
                    let heightInCmsFromInches = Conversions.inchesToCentimeters(inches: heightValues[1])
                    let totalCms = heightInCms + heightInCmsFromInches
                    return "\(Int(totalCms.rounded()))"
                }
            }else{
                return text.replacingOccurrences(of: " cm", with: "")
            }
        }
        return ""
    }
    
    /**
     This method converts the weight from KGs to LBS.
     */
    func getWeight() -> String{
        guard let user = DoviesGlobalUtility.currentUser else{return ""}
        if let text = self.txtfldWeight.text{
            if user.customerUnits.uppercased() == "imperial".uppercased(){
                let kgs = Conversions.getKgsFromLBS(lbs: text.replacingOccurrences(of: " lbs", with: ""))
                return "\(kgs)"
            }else{
                return text.replacingOccurrences(of: " kgs", with: "")
            }
        }
        return ""
    }
    
    /**
     This method validates user personal information and update on server.
     */
    func validationSuccessful() {
        
        var dic = [String: Any]()
        dic[WsParam.gender] = txtfldGender.text
        dic[WsParam.name] = txtfldName.text
        dic[WsParam.dob] = Str_Dob
        dic[WsParam.userName] = txtUserName.text

    // loks comment    dic[WsParam.mobileNumber] = self.txtfldNumber.text
	// loks comment dic[WsParam.isdCode] = self.txtCode.text
        dic[WsParam.countryId] = countryObj?.country_code ?? ""
        dic[WsParam.email] = self.txtEmail.text
        dic[WsParam.height] = getHeight()
        dic[WsParam.weight] = getWeight()
        if imageUpdated {
            dic[WsParam.profile_pic] = self.profileView.image
        }
       // loks comment
       // if isPhoneNumberValid(){
      //  }
        
        if let text = txtUserName.text , text.isEmpty
        {
              self.lblErrorUserame.textColor = self.ColorRed
              self.lblErrorUserame.text = "Please enter your name"
        }
            
        strUserNameText =  self.txtUserName.text ?? ""
        let num = Int(strUserNameText);
        if strUserNameText == "" || strUserNameText.count == 1 || strUserNameText.count == 2
        {
        }
        else if num != nil
        {
        }
        else
        {
           self.updateUserInfoWSCall(with: dic)
        }
        
//        else
//        {
//          self.updateUserInfoWSCall(with: dic)
//        }
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        
    }
    func hideErroeLabel()  {
        self.lblErrorUserame.isHidden = true
        self.layout()
    }
    
    func showErroeLabel()  {
        self.lblErrorUserame.isHidden = false
        self.layout()
    }
    func layout()  {
        DispatchQueue.main.async {
            self.view.layoutIfNeeded()
            self.view.updateConstraintsIfNeeded()
        }
    }
}

extension AboutYouVC: UITextFieldDelegate{
    
   
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtDob {
            self.openTimePicker(textField)
        }
        
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
       
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
      
    }
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == txtUserName{
            
            if let text = textField.text, let swtRange = Range(range, in: text) {
                let fullString = text.replacingCharacters(in: swtRange, with: string)
                if fullString.count < 3{
                   // self.lblErrorUserame.textColor = ColorRed
                    //self.lblErrorUserame.text = AlertMessages.emptyUserName
                }
                else{
                    
                    if fullString.isAlphanumeric == true
                    {
                        let num = Int(fullString);
                        if num != nil {
                            print("Valid Integer")
                            self.hideErroeLabel()
                        }
                        else {
                            self.checkUserAvailibility(name: fullString, loginId:(DoviesGlobalUtility.currentUser?.customerId)!)
                        }
                    }

                   // self.checkUserAvailibility(name: fullString, loginId:(DoviesGlobalUtility.currentUser?.customerId)!)
                }
            }
            
            if string.isEmpty {
                return true
            }
            let isAllowed = isCharacterAllowed(char: string)
            return isAllowed
        }else{
            return true
        }
        
    }
    
    func isCharacterAllowed(char: String) -> Bool {
        let regex = "[a-z0-9_.]"
        let allowedCharacter = NSPredicate(format: "SELF MATCHES %@", regex).evaluate(with: char)
        return allowedCharacter
    }
    
}
