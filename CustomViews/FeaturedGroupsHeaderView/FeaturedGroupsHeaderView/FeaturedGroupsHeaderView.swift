//
//  FeaturedGroupsHeaderView.swift
//  Dovies
//
//  Created by HB-PC on 13/06/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class FeaturedGroupsHeaderView: UICollectionReusableView {

    @IBOutlet weak var lblTItle : UILabel!
    @IBOutlet weak var btnViewAll : UIButton!
    var viewAllTapped:((IndexPath)->())?
    var indexPath : IndexPath!
    
    @IBAction func btnViewAllTapped(_ sender : UIButton){
        viewAllTapped?(indexPath)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    
        // Initialization code
    }
    
}
