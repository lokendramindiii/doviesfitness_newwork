//
//  PurchaseExeCell.swift
//  Dovies
//
//  Created by hb on 21/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class PurchaseExeCell: UITableViewCell {
	
	@IBOutlet weak var IBlblName: UILabel!
	@IBOutlet weak var IBlblType: UILabel!
	@IBOutlet weak var IBlblDate: UILabel!
	@IBOutlet weak var IBimgPurchase: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
	
	func setPurchaseHistoryData(model: ModelPurchaseHistory){
		IBlblName.text = model.displayTitle
		IBlblDate.text = model.purchaseDate
		IBlblType.text = model.purchaseType
		//IBimgPurchase.sd_setImage(with: URL(string: model.displayImage), placeholderImage: AppInfo.placeholderImage)
        
        IBimgPurchase.sd_setImage(with: URL(string: model.displayImage), placeholderImage: nil)

	}

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
