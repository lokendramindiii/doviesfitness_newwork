//
//  GlobalUtility.swift
//
//  Created by HiddenBrains on 14/07/16.
//
//

import UIKit
import MediaPlayer
import CoreTelephony
import NVActivityIndicatorView

@objc class GlobalUtility: NSObject {
    var activityIndicator : NVActivityIndicatorView!

	class func performBlock(_ block: @escaping @convention(block) () -> Void, afterDelay delay: TimeInterval) {
		let when = DispatchTime.now() + delay
		DispatchQueue.main.asyncAfter(deadline: when , execute: block)
	}
	
	class func getCountryCallingCode(countryRegionCode:String)->String{
		
		let prefixCodes = ["AF": "93", "AE": "971", "AL": "355", "AN": "599", "AS":"1", "AD": "376", "AO": "244", "AI": "1", "AG":"1", "AR": "54","AM": "374", "AW": "297", "AU":"61", "AT": "43","AZ": "994", "BS": "1", "BH":"973", "BF": "226","BI": "257", "BD": "880", "BB": "1", "BY": "375", "BE":"32","BZ": "501", "BJ": "229", "BM": "1", "BT":"975", "BA": "387", "BW": "267", "BR": "55", "BG": "359", "BO": "591", "BL": "590", "BN": "673", "CC": "61", "CD":"243","CI": "225", "KH":"855", "CM": "237", "CA": "1", "CV": "238", "KY":"345", "CF":"236", "CH": "41", "CL": "56", "CN":"86","CX": "61", "CO": "57", "KM": "269", "CG":"242", "CK": "682", "CR": "506", "CU":"53", "CY":"537","CZ": "420", "DE": "49", "DK": "45", "DJ":"253", "DM": "1", "DO": "1", "DZ": "213", "EC": "593", "EG":"20", "ER": "291", "EE":"372","ES": "34", "ET": "251", "FM": "691", "FK": "500", "FO": "298", "FJ": "679", "FI":"358", "FR": "33", "GB":"44", "GF": "594", "GA":"241", "GS": "500", "GM":"220", "GE":"995","GH":"233", "GI": "350", "GQ": "240", "GR": "30", "GG": "44", "GL": "299", "GD":"1", "GP": "590", "GU": "1", "GT": "502", "GN":"224","GW": "245", "GY": "595", "HT": "509", "HR": "385", "HN":"504", "HU": "36", "HK": "852", "IR": "98", "IM": "44", "IL": "972", "IO":"246", "IS": "354", "IN": "91", "ID":"62", "IQ":"964", "IE": "353","IT":"39", "JM":"1", "JP": "81", "JO": "962", "JE":"44", "KP": "850", "KR": "82","KZ":"77", "KE": "254", "KI": "686", "KW": "965", "KG":"996","KN":"1", "LC": "1", "LV": "371", "LB": "961", "LK":"94", "LS": "266", "LR":"231", "LI": "423", "LT": "370", "LU": "352", "LA": "856", "LY":"218", "MO": "853", "MK": "389", "MG":"261", "MW": "265", "MY": "60","MV": "960", "ML":"223", "MT": "356", "MH": "692", "MQ": "596", "MR":"222", "MU": "230", "MX": "52","MC": "377", "MN": "976", "ME": "382", "MP": "1", "MS": "1", "MA":"212", "MM": "95", "MF": "590", "MD":"373", "MZ": "258", "NA":"264", "NR":"674", "NP":"977", "NL": "31","NC": "687", "NZ":"64", "NI": "505", "NE": "227", "NG": "234", "NU":"683", "NF": "672", "NO": "47","OM": "968", "PK": "92", "PM": "508", "PW": "680", "PF": "689", "PA": "507", "PG":"675", "PY": "595", "PE": "51", "PH": "63", "PL":"48", "PN": "872","PT": "351", "PR": "1","PS": "970", "QA": "974", "RO":"40", "RE":"262", "RS": "381", "RU": "7", "RW": "250", "SM": "378", "SA":"966", "SN": "221", "SC": "248", "SL":"232","SG": "65", "SK": "421", "SI": "386", "SB":"677", "SH": "290", "SD": "249", "SR": "597","SZ": "268", "SE":"46", "SV": "503", "ST": "239","SO": "252", "SJ": "47", "SY":"963", "TW": "886", "TZ": "255", "TL": "670", "TD": "235", "TJ": "992", "TH": "66", "TG":"228", "TK": "690", "TO": "676", "TT": "1", "TN":"216","TR": "90", "TM": "993", "TC": "1", "TV":"688", "UG": "256", "UA": "380", "US": "1", "UY": "598","UZ": "998", "VA":"379", "VE":"58", "VN": "84", "VG": "1", "VI": "1","VC":"1", "VU":"678", "WS": "685", "WF": "681", "YE": "967", "YT": "262","ZA": "27" , "ZM": "260", "ZW":"263"]
		let countryDialingCode = prefixCodes[countryRegionCode]
		return countryDialingCode!
		
	}
	
	class var strTimestamp: String {
		let currentDate = Date()
		let since1970 = currentDate.timeIntervalSince1970
		return "\(Int(since1970))"
	}
	
	// Functions to set/get UserDefault values
	
	class func setPref(value: Any?, key: String) {
		UserDefaults.standard.set(value, forKey: key)
		UserDefaults.standard.synchronize()
	}
	
	static func getBoolPref(key: String) -> Bool {
		return UserDefaults.standard.bool(forKey: key)
	}
	
	static func getIntPref(key: String) -> Int {
		return UserDefaults.standard.integer(forKey: key)
	}
	
	static func getPref(key: String) -> Any? {
		return UserDefaults.standard.object(forKey: key)
	}
	
	static func removePref(key: String) {
		UserDefaults.standard.removeObject(forKey: key)
		UserDefaults.standard.synchronize()
	}
	
	//showActivityIndi function is used to show Activity indicator
	static func showActivityIndi(viewContView: UIView){
        DispatchQueue.main.async(execute: { () -> Void in
            JHUD.show(at: APP_DELEGATE.window, message: nil)
           //            MBProgressHUD.showAdded(to: viewContView, animated: true)
        })
	}
    
    //showActivityIndi function is used to show Activity indicator
    static func showActivityIndiWindow(viewContView: UIWindow){
        DispatchQueue.main.async(execute: { () -> Void in
            JHUD.show(at: APP_DELEGATE.window, message: nil)
            //            MBProgressHUD.showAdded(to: viewContView, animated: true)
        })
    }

    //hideActivityIndi function is used to hide Activity indicator
    static func hideActivityIndiWindow(viewContView: UIWindow){
        DispatchQueue.main.async(execute: { () -> Void in
            JHUD.hide(for: APP_DELEGATE.window)
            //            MBProgressHUD.hide(for: viewContView, animated: true)
        })
    }
    
    
	//hideActivityIndi function is used to hide Activity indicator
	static func hideActivityIndi(viewContView: UIView){
        DispatchQueue.main.async(execute: { () -> Void in
            JHUD.hide(for: APP_DELEGATE.window)
//            MBProgressHUD.hide(for: viewContView, animated: true)
        })
	}
	
	static func showToastMessage(msg: String){
		DispatchQueue.main.async (execute: { () -> Void in
			let hud = MBProgressHUD.showAdded(to: APP_DELEGATE.window, animated: true)
			hud?.mode = .text
			hud?.detailsLabelText = msg
            hud?.detailsLabelFont = UIFont.robotoMedium(size: 18.0);
			hud?.removeFromSuperViewOnHide = true
			hud?.margin = 15.0
			//		hud?.yOffset = Float((UIScreen.main.bounds.height / 2) - 120)
			hud?.hide(true, afterDelay: 2)
		})
	}
    
	class func heightForLabel(_ text:String?, font:UIFont?, width:CGFloat) -> CGFloat{
		if text == nil || font == nil {
			return 0
		}
		let label:UILabel = UILabel(frame: CGRect(x: 0,y: 0,width: width,height: CGFloat.greatestFiniteMagnitude))
		label.numberOfLines = 0
		label.lineBreakMode = NSLineBreakMode.byWordWrapping
		label.font = font
		label.text = text
		
		label.sizeToFit()
		return label.frame.height
	}
    
//    func globalActivityIndicator(){
//        
//        let frame = CGRect(x: APP_DELEGATE.window-22, y: self.view.center.y-22, width: 45, height: 45)
//        activityIndicatorNew = NVActivityIndicatorView(frame: frame)
//        activityIndicatorNew.center = self.view.center
//        activityIndicatorNew.type = . circleStrokeSpin // add your type
//        activityIndicatorNew.color = UIColor.lightGray // add your color
//        APP_DELEGATE.window?.addSubview(activityIndicatorNew) // or use  webView.addSubview(activityIndicator)
//    }
	
	//openUrlFromApp function helps to open any url on browser
	class func openUrlFromApp(url: String)
    {
		let finalUrl = URL(string: url)
		if finalUrl != nil
        {
			if #available(iOS 10.0, *)
            {
				UIApplication.shared.open(finalUrl!, options: [:], completionHandler: nil)
			}
            else
            {
				UIApplication.shared.openURL(finalUrl!)
			}
		}
	}
    
    class func openCallApp(mobileNumber : String?) -> String
    {
        if let url = URL(string: "telprompt:\(mobileNumber!)") {
            if UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *)
                {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                }
                else
                {
                    UIApplication.shared.openURL(url)
                }
                return("");
            }
            else
            {
                return("Device not supported to make a call")
            }
        }
        else
        {}
        return("Invalid phone number")
    }
//
	class func showSimpleAlert(viewController: UIViewController, message: String, completion: (()->())? = nil) {
		UIAlertController.showAlert(in: viewController, withTitle: AlertTitle.appName, message: message, cancelButtonTitle: AlertButtonTitle.ok, destructiveButtonTitle: nil, otherButtonTitles: nil) { (alertController, action, buttonIndex) in
			completion?()
		}
	}
	
	//topViewController function helps to find top most controller
	class func topViewController(withRootViewController rootViewController: UIViewController) -> UIViewController {
		if (rootViewController is UITabBarController) {
			let tabBarController = (rootViewController as! UITabBarController)
			return self.topViewController(withRootViewController: tabBarController.selectedViewController!)
		}
		else if (rootViewController is UINavigationController) {
			let navigationController = (rootViewController as! UINavigationController)
			return self.topViewController(withRootViewController: navigationController.visibleViewController!)
		}
		else if (rootViewController.presentedViewController != nil) {
			let presentedViewController = rootViewController.presentedViewController
			return self.topViewController(withRootViewController: presentedViewController!)
		}
		else {
			return rootViewController
		}
	}
	
	class func stringFromTimeInterval(interval: TimeInterval) -> String {
		print("interval: \(interval)")
		let interval = Int(interval)
		let seconds = interval % 60
		let minutes = (interval / 60) % 60
		//let hours = (interval / 3600)
		return String(format: "%d:%02d", minutes, seconds)
	}
	
	class func applyPlainShadow(view: UIView) {
		let layer = view.layer
		layer.masksToBounds = false
		layer.shadowColor = UIColor.black.cgColor
		layer.shadowOffset = CGSize(width: 0, height: 0)
		layer.shadowOpacity = 0.4
		layer.shadowRadius = 2
	}
	
	class func getCurrentTimeStamp() -> Int64{
		let timeInterval = NSDate().timeIntervalSince1970
		
		return Int64(timeInterval)
	}
    

	//getVisibleViewController function is used to get current visible view controller
	class func getVisibleViewController(_ rootViewController: UIViewController?) -> UIViewController? {
		
		var rootVC = rootViewController
		if rootVC == nil {
			rootVC = UIApplication.shared.keyWindow?.rootViewController
		}
		
		if rootVC?.presentedViewController == nil {
			return rootVC
		}
		
		if let presented = rootVC?.presentedViewController {
			if presented.isKind(of: UINavigationController.self) {
				let navigationController = presented as! UINavigationController
				return navigationController.viewControllers.last!
			}
			
			if presented.isKind(of: UITabBarController.self) {
				let tabBarController = presented as! UITabBarController
				return tabBarController.selectedViewController!
			}
			
			return getVisibleViewController(presented)
		}
		return nil
	}
	
	func convertToDictionary(text: String) -> [String: Any]? {
		if let data = text.data(using: .utf8) {
			do {
				return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
			} catch {
				print(error.localizedDescription)
			}
		}
		return nil
	}
	
	class func verifyUrl(urlString: String?) -> Bool {
		//Check for nil
		if let urlString = urlString {
			// create NSURL instance
			if let url = NSURL(string: urlString) {
				// check if your application can open the NSURL instance
				return UIApplication.shared.canOpenURL(url as URL)
			}
		}
		return false
	}
	
	class func setShadowTo(_ view: UIView) {
		let layer: CALayer? = view.layer
		layer?.shadowOffset = CGSize(width: CGFloat(1), height: CGFloat(1))
		layer?.shadowColor = UIColor.black.cgColor
		layer?.shadowRadius = 3.0
		layer?.shadowOpacity = 0.50
		layer?.rasterizationScale = UIScreen.main.scale
		layer?.shouldRasterize = true
	}
	
	class func getImageWithColor(color: UIColor, size: CGSize) -> UIImage {
		let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
		UIGraphicsBeginImageContextWithOptions(size, false, 0)
		color.setFill()
		UIRectFill(rect)
		let image: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
		UIGraphicsEndImageContext()
		return image
	}
	
	class func loadFromNibNamed(nibNamed: String, bundle : Bundle? = nil) -> UIView? {
		return UINib(nibName: nibNamed, bundle: bundle).instantiate(withOwner: nil, options: nil)[0] as? UIView
	}
	
	class func getNavigationButtonItems(target: AnyObject, selector: Selector, imageName: String) -> [UIBarButtonItem]? {
		if let image = UIImage(named: imageName) {
			let itemWidth = 30
			let viewMain: UIView = UIView(frame: CGRect(x: 0, y: 0, width: itemWidth, height: itemWidth))
			//			viewMain.backgroundColor = UIColor.red
			let btnLeft: UIButton = UIButton(type: .custom)
			btnLeft.frame = CGRect(x: 0, y: 0, width: itemWidth, height: itemWidth)
			btnLeft.setImage(image, for: .normal)
			btnLeft.addTarget(target, action: selector, for: .touchUpInside)
			//			btnLeft.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0)
			viewMain.addSubview(btnLeft)
			let barBtnItem: UIBarButtonItem = UIBarButtonItem(customView: viewMain)
			let frontSpacer: UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
			frontSpacer.width = -10
			let arrBarButton = [frontSpacer, barBtnItem]
			return arrBarButton
		}
		return nil
	}
    
    class func getNavigationRightButtonItem(target: AnyObject, selector: Selector, imageName: String) -> (UIBarButtonItem? , UIButton?) {
        if let image = UIImage(named: imageName) {
            let itemWidth = 30
            let viewMain: UIView = UIView(frame: CGRect(x: 0, y: 0, width: itemWidth, height: itemWidth))
            let btnLeft: UIButton = UIButton(type: .custom)
            btnLeft.frame = CGRect(x: 0, y: 0, width: itemWidth, height: itemWidth)
            btnLeft.setImage(image, for: .normal)
            if let selectedImage = UIImage(named: "\(imageName)_h") {
                btnLeft.setImage(selectedImage, for: .selected)
                btnLeft.setImage(selectedImage, for: .highlighted)
            }
            btnLeft.addTarget(target, action: selector, for: .touchUpInside)
            viewMain.addSubview(btnLeft)
            let barBtnItem: UIBarButtonItem = UIBarButtonItem(customView: viewMain)
            return (barBtnItem,btnLeft)
        }
        return (nil,nil)
    }
    
    class func getNavigationButtonItemWith(target: AnyObject, selector: Selector, title: String) -> UIBarButtonItem {
        let itemWidth = 70
        let viewMain: UIView = UIView(frame: CGRect(x: 0, y: 0, width: itemWidth, height: 30))
        let btnLeft: UIButton = UIButton(type: .custom)
        btnLeft.frame = CGRect(x: 0, y: 2, width: itemWidth, height: 30)
        btnLeft.titleLabel?.font  = UIFont.tamilBold(size: 16)
        btnLeft.contentMode = .right
        btnLeft.setTitle(title.uppercased(), for: .normal)
        if title == "Cancel" {
            btnLeft.setTitleColor(#colorLiteral(red: 0.9882352941, green: 0.3215686275, blue: 0.1607843137, alpha: 1), for: .normal)

        } else {
            btnLeft.setTitleColor(UIColor.appBlackColor(), for: .normal)
        }
        
        btnLeft.addTarget(target, action: selector, for: .touchUpInside)
        viewMain.addSubview(btnLeft)
        let barBtnItem: UIBarButtonItem = UIBarButtonItem(customView: viewMain)
        
        return barBtnItem
    }
    
    class func getNavigationButtonItemWhiteAndRedWith(target: AnyObject, selector: Selector, title: String) -> UIBarButtonItem {
        let itemWidth = 70
        let viewMain: UIView = UIView(frame: CGRect(x: 0, y: 0, width: itemWidth, height: 30))
        let btnLeft: UIButton = UIButton(type: .custom)
        btnLeft.frame = CGRect(x: 0, y: 2, width: itemWidth, height: 30)
        btnLeft.titleLabel?.font  = UIFont.tamilBold(size: 16)
        btnLeft.contentMode = .right
        btnLeft.setTitle(title.uppercased(), for: .normal)
        if title == "Cancel" {
            btnLeft.setTitleColor(#colorLiteral(red: 0.9882352941, green: 0.3215686275, blue: 0.1607843137, alpha: 1), for: .normal)
            
        } else {
            btnLeft.setTitleColor(UIColor.white, for: .normal)
        }
        
        btnLeft.addTarget(target, action: selector, for: .touchUpInside)
        viewMain.addSubview(btnLeft)
        let barBtnItem: UIBarButtonItem = UIBarButtonItem(customView: viewMain)
        
        return barBtnItem
    }

    
    class func getNavigationButtonItemWithExerciseLibrary(target: AnyObject, selector: Selector, title: String) -> UIBarButtonItem {
        let itemWidth = 70
        let viewMain: UIView = UIView(frame: CGRect(x: 0, y: 0, width: itemWidth, height: 30))
        let btnLeft: UIButton = UIButton(type: .custom)
        btnLeft.frame = CGRect(x: 0, y: 2, width: itemWidth, height: 30)
        btnLeft.titleLabel?.font  = UIFont.tamilBold(size: 16)
        btnLeft.contentMode = .right
        btnLeft.setTitle(title.uppercased(), for: .normal)
        if title == "Cancel" {
            btnLeft.setTitleColor(#colorLiteral(red: 0.9882352941, green: 0.3215686275, blue: 0.1607843137, alpha: 1), for: .normal)
            
        } else {
            btnLeft.setTitleColor(UIColor.white, for: .normal)
        }
        
        btnLeft.addTarget(target, action: selector, for: .touchUpInside)
        viewMain.addSubview(btnLeft)
        let barBtnItem: UIBarButtonItem = UIBarButtonItem(customView: viewMain)
        
        return barBtnItem
        
    }

    
	class func getToolbar(doneTitle: String = "Done", target: Any, action: Selector, toolBarColor: UIColor = .black) -> UIToolbar {
		let toolBar = UIToolbar()
		toolBar.barStyle = .default
		toolBar.barTintColor = toolBarColor
		toolBar.tintColor       = UIColor.white
		toolBar.items = [UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
		                 UIBarButtonItem(title: doneTitle, style: .plain, target: target, action: action)]
		toolBar.sizeToFit()
		return toolBar
	}
	
	class func openApplicationSettings() {
		guard let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) else {
			return
		}
		if UIApplication.shared.canOpenURL(settingsUrl) {
			UIApplication.shared.openURL(settingsUrl)
		}
	}
}

struct ScreenSize {
    static let width = UIScreen.main.bounds.size.width
    static let height = UIScreen.main.bounds.size.height
	static let SCREEN_WIDTH         = UIScreen.main.bounds.size.width
	static let SCREEN_HEIGHT        = UIScreen.main.bounds.size.height
	static let SCREEN_MAX_LENGTH    = max(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
	static let SCREEN_MIN_LENGTH    = min(ScreenSize.SCREEN_WIDTH, ScreenSize.SCREEN_HEIGHT)
}

struct DeviceType {
	static let IS_IPHONE_4_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH < 568.0
	static let IS_IPHONE_5_OR_LESS  = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH <= 568.0
	static let IS_IPHONE_5          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 568.0
	static let IS_IPHONE_6          = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 667.0
	static let IS_IPHONE_6P         = UIDevice.current.userInterfaceIdiom == .phone && ScreenSize.SCREEN_MAX_LENGTH == 736.0
	static let IS_IPAD              = UIDevice.current.userInterfaceIdiom == .pad && ScreenSize.SCREEN_MAX_LENGTH == 1024.0
}

struct iOSVersion {
	static let SYS_VERSION_FLOAT = (UIDevice.current.systemVersion as NSString).floatValue
	static let iOS7 = (iOSVersion.SYS_VERSION_FLOAT < 8.0 && iOSVersion.SYS_VERSION_FLOAT >= 7.0)
	static let iOS8 = (iOSVersion.SYS_VERSION_FLOAT >= 8.0 && iOSVersion.SYS_VERSION_FLOAT < 9.0)
	static let iOS9 = (iOSVersion.SYS_VERSION_FLOAT >= 9.0 && iOSVersion.SYS_VERSION_FLOAT < 10.0)
	static let iOS10 = iOSVersion.SYS_VERSION_FLOAT >= 10.0
}

extension UIButton
{
	func Rounded(corners:UIRectCorner, radius: CGFloat)
	{
		let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
		let mask = CAShapeLayer()
		mask.path = path.cgPath
		self.layer.mask = mask
	}
}
