//
//  UpgradeToPremiumCollCell.swift
//  Dovies
//
//  Created by hb on 18/04/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class UpgradeToPremiumCollCell: UICollectionViewCell {
    
    @IBOutlet weak var lblPackageName: UILabel!
    @IBOutlet weak var lblAmount: UILabel!
    @IBOutlet weak var lblPackageType: UILabel!
    @IBOutlet weak var lblPackageDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func setData(PackageList:ModelGetAllPackage){
         lblPackageName.text = PackageList.packageName.uppercased()
         lblAmount.text = "$" + PackageList.packageAmount 
         lblPackageType.text = PackageList.packageText
         lblPackageDescription.text = PackageList.packageDescription
    }
    
}
