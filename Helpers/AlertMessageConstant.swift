//
//  AlertMessageConstant.swift
//

import Foundation
import UIKit



//let ALERT_MSG_

struct AlertTitles {
   static let appTitle = "DOVIESFITNESS"
    static let ok = "Ok"
    static let cancel = "Cancel"
    
}

let ALERT_BUTTON_CANCEL		=	"Cancel"
let ALERT_BUTTON_OK		=	"Ok"

//Alert Messages showing in applications
struct AlertMessages {
   
    static let emptyUserName = "Please enter your username."
    static let emptyUserFullName = "Please enter your full name."
    static let emptyDOB = "Please select your date of birth."
    static let emptyCountry = "Please select your country."
    static let emptyMobile = "Please enter your mobile number."

    static let emptyEmail = "Please enter your email address."
    static let invalidEmail = "Please enter a valid email address."
    static let emptyPassword = "Please enter a password."
    static let validatePassword = "Password should be minimum 6 characters."
    
	static let emptyRePassword = "Re-enter password."

	static let invalidatePasswordCheck = "Password does not match the confirm password."
    static let emptyWeight = "Please select weight."
    static let emptyGender = "Please select gender."
    static let passwordUpdate = "Your password has been changed successfully!"
    
    //About You Screen
    static let Name = "Please enter name"
    static let MobileNo = "Please enter 10-15 digit mobile number"
    
    //Change password
    static let OldPassword = "Please enter your old password."
    static let NewPassword = "Please enter your new password."
    static let NewPasswordAgain = "Please re-enter  your new password."
    
    //plan
    static let emptyPlanName = "Please enter workout plan name"
    static let emptyPlanAmount = "Please enter amount"

    static let emptyLevel = "Please select level"
    static let emptyNumberOfWeeks = "Please select No. of Weeks"
    
    static let workoutDeleteWarning = "Are you sure you want to delete this workout?"
	static let dietDeleteWarning = "Are you sure you want to delete this diet from program?"
    static let updatePlanSuccess = "Plan updated successfully"
    static let emptyImageForPlan = "Please select image for plan"
    static let emptyExercises = "No exercises found in this workout"
    
    //Add workout
    static let emptyWorkoutTitle = "Please enter workout name"
    static let emptyWorkoutLevel = "Please select workout level"
    static let emptyWorkoutGoodFor = "Please select workout good for"
    static let emptyWorkoutGroup = "Please select workout group"
    static let emptyWorkoutCreatedby = "Please select Created by"

    static let emptyAccessLevel = "Please select access level"
    static let emptyDisplayInNewfeed = "Please select display in Newsfeed"
    static let emptyDisplayInNewTag = "Please select display New Tag"

    static let emptyAllowNotification = "Please select allow notification"

    static let emptyWorkoutEquipments = "Please select workout equipments"
    static let emptyWorkoutPhoto = "Please select workout image"
    static let emptyWorkoutExercise = "Please select exercise"
    static let emptyTime = "Please select time"
    static let emptyTotalTime = "Please select total time"
    static let emptyReps = "Please select reps"
	
	static let cancleCreateWorkout = "All changes done to the workout will be lost. Are you sure  you want to cancel?"
    static let cancleCreatePlan = "All changes done to the plan will be lost. Are you sure  you want to cancel?"
    static let validMobileNumber = "Please enter valid mobile number"
    
    static let emptyPackageId = "Package id is empty"
    static let deleteConfirmation = "Are you sure you want to delete?"
    static let logoutConfirmation = "Are you sure you want to logout?"
    static let cancelLoggingWorkoutConfirmation = "Are you sure you want to cancel logging your workout?"
    static let deleteWorkoutConfirmation = "Are you sure you want to delete this Workout?"
    static let noNetwrok = "No network connection"
    static let deleteDownloadedWorkout = "You can't play your workout offline if you delete this workout. you will have to re-download this again to be able to play it in an offline mode.  Are you sure you want to delete your downloaded workout?"
    
    static let deleteNotification = "Are you sure you want to delete this Message?"

	static let deleteWorkoutLog = "Are you sure you want to delete this Workout Log?"
}

struct AlertButtonTitle {
	static let ok = "Ok"
	static let cancel = "Cancel"
	static let yes = "Yes"
	static let no = "No"
	static let setting = "Setting"
}

struct AlertTitle {
    static let signOut = "Sign Out"
    static let appName = ""
}
