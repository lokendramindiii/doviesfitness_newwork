//
//  WorkoutPlanVC.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit


class WorkoutPlanVC: BaseViewController {
    var selectedLevel : String = "Level"
    var planImage : UIImage?
    var IsAmount = false

    let validator = Validator()
    var equipmentList : EquipmentListVC?
    var goodForListing:GoodForListingVC?
    @IBOutlet var lblLevel: UILabel!
    @IBOutlet var btnlevel: UIButton!

    @IBOutlet weak var txtName : UITextField!
    @IBOutlet weak var btnAddImage : UIButton!
    @IBOutlet weak var btnAddPhoto: UIButton!
    
    @IBOutlet var img_StarPhoto: UIImageView!

    @IBOutlet var btnGoodFor: UIButton!
    @IBOutlet var btnGoodForImage: UIButton!
    @IBOutlet var lblGoodFor: UILabel!
    var arrSelectedGoodfor = [String]()
    var arrGoodforId = [String]()

    @IBOutlet var lblEquipments: UILabel!
    @IBOutlet var btnEquipments: UIButton!
    @IBOutlet var btnEquipImage: UIButton!
    
    // for admin add programe plan
    @IBOutlet var view_bgAdmin: UIView!

    @IBOutlet var btnAccesslevel: UIButton!
    @IBOutlet var btnAccesslevelImage: UIButton!
    @IBOutlet var lblAccesslevel: UILabel!
    
    @IBOutlet var txtfldAmount: UITextField!
    @IBOutlet var View_BgTfAmount: UIView!

    @IBOutlet var btnAllowedUser: UIButton!
    @IBOutlet var btnAllowedUserImage: UIButton!
    @IBOutlet var lblAlowedUser: UILabel!

    @IBOutlet var btnDisplayNewsfeed: UIButton!
    @IBOutlet var btnDisplayNewsfeedImage: UIButton!
    @IBOutlet var lblDisplayNewsfeed: UILabel!
    
    
    @IBOutlet var btnAllowNotification: UIButton!
    @IBOutlet var btnAllowNotificationImage: UIButton!
    @IBOutlet var lblAllowNotification: UILabel!
    
    // Mindiii add userlist
    var userlist : UserListVC?
    var arrSelecteduser = [String]()
    var arrUserId = [String]()
    var arrUserListing = [ModelUserlist]()
    
    //
    var arrSelectedequips = [String]()
    var arrEquipsId = [String]()
    
    @IBOutlet weak var imgPlan : UIImageView!
    
    var arrLevelListing = [FilterDataModel]()
    var  arrFilterData: [FilterGroup] = DoviesGlobalUtility.arrFilterData
    var buildPlan : BuildPlanModel?
    var workoutPlanModel  = WorkoutPlanModel()

    //MARK: - View controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
       btnAddImage.layer.cornerRadius = 20
       btnAddImage.clipsToBounds = true

       imgPlan.layer.borderWidth = 1.0
       imgPlan.layer.borderColor = UIColor(red:231/255, green:231/255, blue:231/255, alpha: 1).cgColor
        
        txtName.attributedPlaceholder = fun_attributedstring_forcolor(main_string: "Name *", string_to_color: "*")
        
        
        txtfldAmount.attributedPlaceholder = fun_attributedstring_forcolor(main_string: "Amount *", string_to_color: "*")

        btnlevel.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "Level ", String2: "*"), for: .normal)

        btnGoodFor.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "Good For ", String2: "*"), for: .normal)
        btnEquipments.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "Equipments ", String2: "*"), for: .normal)
        
        
        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        {
          //  self.callGetcustomerUserListingWS()
            
            self.callGetcustomerUserListingNewWS()
            
            view_bgAdmin.isHidden = false
            
            txtfldAmount.isUserInteractionEnabled  = false
            btnAllowedUser.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "Allowed users ", String2: ""), for: .normal)
            
            btnAccesslevel.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "Access Level ", String2: "*"), for: .normal)
            btnDisplayNewsfeed.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "Display in Newsfeed? ", String2: "*"), for: .normal)
            btnAllowNotification.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "Allow Notification? ", String2: "*"), for: .normal)
        }
        else {
            view_bgAdmin.isHidden = true
        }
        
         inititalUISetUp()
         setFormValidators()

    }
    
    

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    //MARK: - Custom methods
    override func inititalUISetUp() {
        self.navigationItem.title = "Workout Plan".uppercased()
        self.navigationItem.hidesBackButton  = true
        self.addNavigationBackButton()
        getArrayDetails()
        fillWithBuildPlan()
    }
    
    func fillWithBuildPlan(){
        guard let plan = buildPlan else {return}
        txtName.text = plan.customerProgramDetail[0].programName
        lblLevel.text = plan.customerProgramDetail[0].programLevel
        
        self.workoutPlanModel.lbl_level = plan.customerProgramDetail[0].programLevel
       
      //  imgPlan.sd_setImage(with: URL(string: plan.customerProgramDetail[0].programImage), placeholderImage: AppInfo.placeholderImage)
        
        imgPlan.sd_setImage(with: URL(string: plan.customerProgramDetail[0].programImage), placeholderImage: nil)

        planImage = imgPlan.image
        
        btnAddImage.isHidden = true
        img_StarPhoto.image = nil
        
        lblGoodFor.text = plan.customerProgramDetail[0].programGoodforMasterName
        lblEquipments.text = plan.customerProgramDetail[0].programEquipmentMasterName
        lblDisplayNewsfeed.text = plan.customerProgramDetail[0].programNewsFeed
        self.workoutPlanModel.lblDisplayInNewsfeed = plan.customerProgramDetail[0].programNewsFeed

        lblAccesslevel.text = plan.customerProgramDetail[0].programAccessLevelSelect
        self.workoutPlanModel.lbl_AccessLevel = plan.customerProgramDetail[0].programAccessLevelSelect

        lblAllowNotification.text = plan.customerProgramDetail[0].programNotification
        self.workoutPlanModel.lblallowNotification = plan.customerProgramDetail[0].programNotification

        lblAlowedUser.text = plan.customerProgramDetail[0].programAllowedUsersName
        if var textlevel = self.lblLevel.text, !textlevel.isEmpty{
            if textlevel == "All"
            {
                textlevel = "All Levels"
            }
            self.lblLevel.attributedText = self.fun_attributedstringNextLine(StringTitle: " Level", SelectString: textlevel)
        }
        if let text = self.lblGoodFor.text, !text.isEmpty{
            self.lblGoodFor.attributedText = self.fun_attributedstringNextLine(StringTitle: " Good for", SelectString: text)
        }
        if let text = self.lblEquipments.text, !text.isEmpty{
            
            self.lblEquipments.attributedText = self.fun_attributedstringNextLine(StringTitle: " Equipments", SelectString: text)
        }
        if let text = self.lblDisplayNewsfeed.text, !text.isEmpty{
            
            self.lblDisplayNewsfeed.attributedText = self.fun_attributedstringNextLine(StringTitle: " Display in Newsfeed", SelectString: text)
        }
        if let text = self.lblAccesslevel.text, !text.isEmpty{
            
            self.lblAccesslevel.attributedText = self.fun_attributedstringNextLine(StringTitle: " Access level", SelectString: text)
        }
        if let text = self.lblAllowNotification.text, !text.isEmpty{
            
            self.lblAllowNotification.attributedText = self.fun_attributedstringNextLine(StringTitle: " Allow Notification", SelectString: text)
        }
        if let text = self.lblAlowedUser.text, !text.isEmpty{
            
            self.lblAlowedUser.attributedText = self.fun_attributedstringNextLine(StringTitle: " Allowed users", SelectString: text)
        }
        if plan.customerProgramDetail[0].programAccessLevelSelect == "Paid"
        {
            txtfldAmount.isUserInteractionEnabled = true
            IsAmount = true
            txtfldAmount.text = plan.customerProgramDetail[0].programAmount
            View_BgTfAmount.backgroundColor = UIColor.clear
        }
        else
        {
             IsAmount = false
             txtfldAmount.isUserInteractionEnabled = false
             View_BgTfAmount.backgroundColor = UIColor.colorWithRGB(r: 248, g: 248, b: 248, alpha: 1.0)
        }
      
        arrSelectedGoodfor = plan.customerProgramDetail[0].programGoodforMasterName.components(separatedBy: " | ")
        arrGoodforId =  plan.customerProgramDetail[0].programGoodFor.components(separatedBy: ",")
        
        arrSelectedequips = plan.customerProgramDetail[0].programEquipmentMasterName.components(separatedBy: " | ")
        arrEquipsId = plan.customerProgramDetail[0].programEquipments.components(separatedBy:",")
        
        if  let text = plan.customerProgramDetail[0].programAllowedUserId , text.isEmpty
        {
        }
        else
        {
        arrSelecteduser = plan.customerProgramDetail[0].programAllowedUsersName.components(separatedBy: " | ")
        arrUserId = plan.customerProgramDetail[0].programAllowedUserId.components(separatedBy:",")
        }
        btnlevel.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "", String2: ""), for: .normal)
        btnGoodFor.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "", String2: ""), for: .normal)
        btnEquipments.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "", String2: ""), for: .normal)
        
        btnAllowNotification.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "", String2: ""), for: .normal)
        btnDisplayNewsfeed.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "", String2: ""), for: .normal)
        btnAccesslevel.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "", String2: ""), for: .normal)
        
        if let text = lblAlowedUser.text , text.isEmpty
        {
         btnAllowedUser.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "Allowed users ", String2: ""), for: .normal)
        }
        else
        {
        btnAllowedUser.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "", String2: ""), for: .normal)
        }
    }
    
    func getArrayDetails(){
        let arrValue = arrFilterData.filter {$0.groupName == "Level"}
        if arrValue.count > 0{
            self.arrLevelListing = arrValue[0].list
        }
        
    }
    
    //MARK: - Actions
    @IBAction func btnLevelClicked(_ sender: Any) {
        self.view.endEditing(true)
        let arrList = arrLevelListing.filter {!$0.gmGoogforMasterId.isEmpty}
        let mcPicker = McPicker(data: [(arrList.map{$0.gmDisplayName})], selectedTitle: [selectedLevel])
        mcPicker.show { [weak self] (selections: [Int : String]) -> Void in
            guard let weakSelf = self else{return}
            if let Level = selections[0]{
                BuildPlanViewController.isDataChanged = true
                weakSelf.btnlevel.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "", String2: ""), for: .normal)
                if Level == "All Level" || Level == "All Levels"
                {
                    self?.workoutPlanModel.lbl_level = "All"
                }
                  else
                {
                    self?.workoutPlanModel.lbl_level = Level
                }
                weakSelf.lblLevel.attributedText = self?.fun_attributedstringNextLine(StringTitle: " Level", SelectString: Level)
            }
        }
    }
    
    @IBAction func btnAddPhotoClicked(_ sender: Any) {
        HBImagePicker.sharedInstance().open(with: MediaTypeImage, actionSheetTitle: "Choose Image", cancelButtonTitle: "Cancel", cameraButtonTitle: "Use Camera", galleryButtonTitle: "Choose existing", canEdit: true) { [weak self] (success, dict) in
            guard let weakSelf = self else{return}
            if success {
                BuildPlanViewController.isDataChanged = true
                weakSelf.btnAddImage.backgroundColor = UIColor.clear
                if let img = dict?["image"] as? UIImage {
                    weakSelf.planImage = img
                    weakSelf.imgPlan.image = HBImagePicker.sharedInstance().compressImage(img)
                     weakSelf.btnAddImage.isHidden = true
                     weakSelf.img_StarPhoto.image = nil
                }
            }
        }
    }

    @IBAction func btnAddImageTapped(_ sender : UIButton){
        HBImagePicker.sharedInstance().open(with: MediaTypeImage, actionSheetTitle: "Choose Image", cancelButtonTitle: "Cancel", cameraButtonTitle: "Use Camera", galleryButtonTitle: "Choose existing", canEdit: true) { [weak self] (success, dict) in
            guard let weakSelf = self else{return}
            if success {
                BuildPlanViewController.isDataChanged = true
                weakSelf.btnAddImage.backgroundColor = UIColor.clear
                if let img = dict?["image"] as? UIImage {
                    weakSelf.planImage = img
                    weakSelf.imgPlan.image = HBImagePicker.sharedInstance().compressImage(img)
                    weakSelf.btnAddImage.isHidden = true
                    weakSelf.img_StarPhoto.image = nil
                }
            }
        }
    }
    
    
    @IBAction func  didTapGoodforButton() {
        goodForListing = self.storyboard?.instantiateViewController(withIdentifier: "GoodForListingVC")as? GoodForListingVC
        goodForListing?.selected = arrSelectedGoodfor
        goodForListing?.arrIdData = arrGoodforId
        goodForListing?.completion = { (selected,arrID) in
            if selected != nil{
                self.arrSelectedGoodfor = selected!
                self.arrGoodforId = arrID!
                self.lblGoodFor.text = selected!.joined(separator: " | ")
                if let text = self.lblGoodFor.text, !text.isEmpty{
                    self.btnGoodFor.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "", String2: ""), for: .normal)
                    self.lblGoodFor.attributedText = self.fun_attributedstringNextLine(StringTitle: " Good for", SelectString: selected!.joined(separator: " | "))

                }
                else if self.lblGoodFor.text == ""{
                    self.btnGoodFor.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "Good For ", String2: "*"), for: .normal)
                }
                else{
                    
                }
            }
        }
        self.navigationController?.pushViewController(goodForListing!, animated: true)
    }
    
  @IBAction func didTapEquipmentsButton(_ sender : UIButton){
        
        equipmentList = self.storyboard?.instantiateViewController(withIdentifier: "EquipmentListVC")as? EquipmentListVC
         equipmentList?.selected = arrSelectedequips
         equipmentList?.arrIdData = arrEquipsId
         equipmentList?.completion = { (selected,arrEquipId) in
            if selected != nil{
                 self.arrSelectedequips = selected!
                 self.arrEquipsId = arrEquipId!
                self.lblEquipments.text = selected!.joined(separator: " | ")
                if let text = self.lblEquipments.text, !text.isEmpty{
                    
                 self.lblEquipments.attributedText = self.fun_attributedstringNextLine(StringTitle: " Equipments", SelectString: selected!.joined(separator: " | "))
                    self.btnEquipments.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "", String2: ""), for: .normal)
                    
                }
                else if self.lblEquipments.text == ""{
                    
                    self.btnEquipments.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "Equipments ", String2: "*"), for: .normal)
                }
                else{
                }
            }
        }
        self.navigationController?.pushViewController(equipmentList! , animated: true)
        
    }
    
    
    
    
   // for admin
    @IBAction func  didTapAllowUserButton() {
        
        userlist = self.storyboard?.instantiateViewController(withIdentifier: "UserListVC")as? UserListVC
        userlist?.selected = arrSelecteduser
        userlist?.arrIdData = arrUserId
        userlist?.arrUserListing = self.arrUserListing
        userlist?.Str_Title = "Allow User"
        userlist?.completion = { (selected,arrUserID) in
            if selected != nil{
                self.arrSelecteduser = selected!
                self.arrUserId = arrUserID!
                self.lblAlowedUser.text = selected!.joined(separator: " | ")
                if let text = self.lblAlowedUser.text, !text.isEmpty
                {
                self.lblAlowedUser.attributedText = self.fun_attributedstringNextLine(StringTitle: " Allowed users", SelectString: selected!.joined(separator: " | "))

                self.btnAllowedUser.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "", String2: ""), for: .normal)
                }
                else if self.lblAlowedUser.text == ""{
                    self.btnAllowedUser.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "Allowed users ", String2: ""), for: .normal)
                }
                else{
                }
            }
        }
        self.navigationController?.pushViewController(userlist! , animated: true)
    }
    
    
    
    
    @IBAction func didTapAllowNotificationButton(){
        self.view.endEditing(true)
        let arrList = ["Yes","NO"]
        let mcPicker = McPicker(data: [(arrList)], selectedTitle: [""])
        mcPicker.show { [weak self] (selections: [Int : String]) -> Void in
            guard let weakSelf = self else{return}
            if let level = selections[0]{
            weakSelf.btnAllowNotification.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "", String2: ""), for: .normal)
                
                self?.workoutPlanModel.lblallowNotification = level
                weakSelf.lblAllowNotification.attributedText = self?.fun_attributedstringNextLine(StringTitle: " Allow Notification", SelectString: level)
            }
        }
    }
    
    @IBAction func didTapAccesslevelButton(){
        self.view.endEditing(true)
        let arrList = ["Free","Subscribers","Paid"]
        let mcPicker = McPicker(data: [(arrList)], selectedTitle: [""])
        mcPicker.show { [weak self] (selections: [Int : String]) -> Void in
            guard let weakSelf = self else{return}
            if let level = selections[0]{
            weakSelf.btnAccesslevel.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "", String2: ""), for: .normal)
                
                self?.workoutPlanModel.lbl_AccessLevel = level
                weakSelf.lblAccesslevel.attributedText = self?.fun_attributedstringNextLine(StringTitle: " Access level", SelectString: level)
                
                if level == "Paid"
                {
                    self?.IsAmount = true
                    self?.txtfldAmount.isUserInteractionEnabled  = true
                    self?.View_BgTfAmount.backgroundColor = UIColor.clear
                }
                else
                {
                    self?.IsAmount = false
                    self?.txtfldAmount.text = ""

                    self?.View_BgTfAmount.backgroundColor = UIColor.colorWithRGB(r: 248, g: 248, b: 248, alpha: 1.0)
                    self?.txtfldAmount.isUserInteractionEnabled  = false
                }
            }
        }
    }
    @IBAction func didTapDisplayNewsFeedButton(){
        self.view.endEditing(true)
        let arrList = ["Yes","NO"]
        let mcPicker = McPicker(data: [(arrList)], selectedTitle: [""])
        mcPicker.show { [weak self] (selections: [Int : String]) -> Void in
            guard let weakSelf = self else{return}
            if let level = selections[0]{
            weakSelf.btnDisplayNewsfeed.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "", String2: ""), for: .normal)
                self?.workoutPlanModel.lblDisplayInNewsfeed = level
                weakSelf.lblDisplayNewsfeed.attributedText = self?.fun_attributedstringNextLine(StringTitle: " Display in Newsfeed", SelectString: level)
                
            }
        }
    }
    @IBAction func didTapDisplayNewTagButton(){
        self.view.endEditing(true)
        let arrList = ["Yes","NO"]
        let mcPicker = McPicker(data: [(arrList)], selectedTitle: [""])
        mcPicker.show { [weak self] (selections: [Int : String]) -> Void in
            guard let weakSelf = self else{return}
            if let level = selections[0]{
      
            }
        }
    }

    
  
    func fun_attributedstringNextLine(StringTitle:String ,SelectString:String ) -> NSMutableAttributedString {
        
        let Str_MutableString = NSMutableAttributedString(string: StringTitle, attributes: [NSAttributedStringKey.font:UIFont.tamilBold(size: 13.0),NSAttributedString.Key.foregroundColor:UIColor.colorWithRGB(r: 34/255.0, g: 34/255.0, b: 34/255.0)])
        
        let SelectLevel = NSMutableAttributedString(string: "\n \(SelectString)", attributes: [NSAttributedStringKey.font:UIFont.tamilRegular(size: 12.0),NSAttributedString.Key.foregroundColor:UIColor.lightGray])
        Str_MutableString.append(SelectLevel)
        
        return Str_MutableString
        
    }
    
    
    //MARK: - Validations
     func setFormValidators() {
        
//        if let text =  txtName.text, text.isEmpty{
//            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyPlanName, completion: nil)
//        }
//         if (imgPlan.image == nil){
//
//            img_StarPhoto.image = UIImage(named: "img_star")
//            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyImageForPlan, completion: nil)
//        }
//         if  txtfldLevel.text == "Level *"{
//            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyLevel, completion: nil)
//        }
//         if let text = lblGoodFor.text, text.isEmpty{
//            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyWorkoutGoodFor, completion: nil)
//        }
//         if let text = lblEquipments.text, text.isEmpty{
//            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyWorkoutEquipments, completion: nil)
//        }
       
        
//        validator.styleTransformers(success:{ (validationRule) -> Void in
//            if let textField = validationRule.field as? DTTextField {
//                textField.hideError()
//            }
//        }, error:{ (validationError) -> Void in
//            if let textField = validationError.field as? DTTextField {
//                textField.showError(message: validationError.errorMessage)
//            }
//        })
//
//        validator.registerField(txtName, errorLabel: nil , rules: [RequiredRule(message: AlertMessages.emptyPlanName)])
//
//        validator.registerField(txtfldLevel, errorLabel: nil, rules: [RequiredRule(message: AlertMessages.emptyLevel)])
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

extension WorkoutPlanVC: UITextFieldDelegate{
    // MARK: Validate single field
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let dtTextField = textField as? DTTextField else{return}
        
        dtTextField.hideError()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        BuildPlanViewController.isDataChanged = true
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        validator.validateField(textField){ error in
            if error == nil {
                // Field validation was successful
            } else {
                // Validation error occurred
            }
        }
        return true
    }
    
}

extension WorkoutPlanVC {
    
    /**
     This method is used to call feed listing web service.
     */
    
    func callGetcustomerUserListingWS(){
        
        let dic = [String: Any]()
        DoviesWSCalls.callGetModelGetuserListDataAPI(dicParam: dic, onSuccess: { (sucess, arrgetuserlist,arrCreatedBy,arrWorkoutGroup, strMessage) in
            GlobalUtility.hideActivityIndi(viewContView: self.view)
            if sucess{
                if arrgetuserlist != nil{
                    self.arrUserListing = arrgetuserlist!
                }
                if arrCreatedBy != nil{
                  //  self.arrCreatedByListing = arrCreatedBy!
                }
                if arrWorkoutGroup != nil{
                  //  self.arrWorkoutListing = arrWorkoutGroup!
                }
            }
        }) { (error) in
            
        }
   }
    
    func callGetcustomerUserListingNewWS(){
        
        var dic = [String: Any]()
        
        dic[WsParam.searchQuery] = ""
        dic[WsParam.pageIndex] = "1"
        dic[WsParam.allowedUserId] = self.arrUserId.joined(separator:  ",")
        DoviesWSCalls.callGetModelGetuserListDataSearchAPI(dicParam: dic, onSuccess: {  (sucess, arrgetuserlist,arrCreatedBy,arrWorkoutGroup, strMessage ,hasNextPage) in
            
            GlobalUtility.hideActivityIndi(viewContView: self.view)
            
            if sucess{
                if arrgetuserlist != nil{
                    self.arrUserListing = arrgetuserlist!
                }
            }
        }) { (error) in
            
        }
    }

}

class WorkoutPlanModel: NSObject {
    
    var lbl_level: String?
    var lbl_AccessLevel: String?
    var lblDisplayInNewsfeed: String?
    var lblallowNotification: String?
    
}
