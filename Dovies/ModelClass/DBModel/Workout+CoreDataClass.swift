//
//  Workout+CoreDataClass.swift
//  
//
//  Created by Neel Shah on 01/05/18.
//
//

import Foundation
import CoreData


public class Workout: NSManagedObject {
    
	class func getWorkoutFromContext(params:NSPredicate?,sortOn:String? = nil ,isAsending:Bool? = nil) -> [AnyObject]? {
		var sortAry = [NSSortDescriptor]()
		if let sortKey = sortOn {
			let sortDesc = NSSortDescriptor(key: sortKey, ascending: isAsending!, selector:#selector(NSString.localizedCaseInsensitiveCompare(_:)))
			sortAry = [sortDesc]
		}
		return self.getWorkoutSortArrayFromContext(params: params, sortDescAry: sortAry as NSArray?)
	}
    
    
    class func getExerciseFromContext(params:NSPredicate?,sortOn:String? = nil ,isAsending:Bool? = nil) -> [AnyObject]? {
        var sortAry = [NSSortDescriptor]()
        if let sortKey = sortOn {
            let sortDesc = NSSortDescriptor(key: sortKey, ascending: isAsending!, selector:#selector(NSString.localizedCaseInsensitiveCompare(_:)))
            sortAry = [sortDesc]
        }
        return self.getWorkoutSortArrayFromContext(params: params, sortDescAry: sortAry as NSArray?)
    }
    
	
	class func getWorkoutSortArrayFromContext(params:NSPredicate?,sortDescAry:NSArray? = nil) -> [AnyObject]? {
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Workout")
		if let predict = params {
			fetchRequest.predicate = predict
		}
		
		let context = APP_DELEGATE.managedObjectContext
		
		let entity = NSEntityDescription.entity(forEntityName: "Workout", in: (context))
		fetchRequest.entity = entity
        
        
		if let sortKey = sortDescAry {
			fetchRequest.sortDescriptors = sortKey as? [NSSortDescriptor]
		}
		do {
			let fetchedDreams = try context.fetch(fetchRequest)
			if fetchedDreams.count > 0 {
				return fetchedDreams as [AnyObject]?
			} else {
				return nil
			}
		} catch {
			return nil
		}
	}
	
	class func deleteAllDataOfWorkout()
	{
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Workout")
		let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
		
		do {
			try APP_DELEGATE.managedObjectContext.execute(deleteRequest)
		} catch {
			// Error Handling
		}
	}
	
	class func deleteWorkoutWhichIsEdit(workoutId: String){
		let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Workout")
		fetchRequest.predicate = NSPredicate(format: "workoutId == '\(workoutId)'")
		
		let context = APP_DELEGATE.managedObjectContext
		
		let entity = NSEntityDescription.entity(forEntityName: "Workout", in: (context))
		fetchRequest.entity = entity
		if let result = try? context.fetch(fetchRequest){
			for object in result {
				context.delete(object as! NSManagedObject)
			}
        }
        do {
            try context.save() // <- remember to put this :)
        } catch {
            // Do something... fatalerror
        }
	}
	
	class func addNewRecoredInDB(modelWorkoutDetail:ModelWorkoutDetail, workout: Workout){
		
		workout.creatorName = modelWorkoutDetail.creatorName
		workout.creatorProfileImage = modelWorkoutDetail.creatorProfileImage
		workout.workoutLevel = modelWorkoutDetail.workoutAccessLevel
		workout.workoutDescription = modelWorkoutDetail.workoutDescription
		workout.workoutEquipment = modelWorkoutDetail.workoutEquipment
		workout.workoutExerciseCount = modelWorkoutDetail.workoutExerciseCount
		workout.workoutFullImage = modelWorkoutDetail.workoutFullImage
		workout.workoutGoodFor = modelWorkoutDetail.workoutGoodFor
		workout.workoutImage = modelWorkoutDetail.workoutImage
		workout.workoutLevel = modelWorkoutDetail.workoutLevel
		workout.workoutName = modelWorkoutDetail.workoutName
		workout.workoutShareUrl = modelWorkoutDetail.workoutShareUrl
		workout.workoutTotalTime = modelWorkoutDetail.workoutTotalTime
		workout.creatorId = modelWorkoutDetail.creatorId
		workout.workoutZipFile = modelWorkoutDetail.workoutZipFile
		workout.workoutCreatedBy = modelWorkoutDetail.workoutCreatedBy
		workout.workoutId = modelWorkoutDetail.workoutId
        
		for data in modelWorkoutDetail.workoutExerciseList{
           workout.addToExercise(Exercise.saveContactInDB(modelWorkoutExerciseList: data, parentId: modelWorkoutDetail.workoutId))
            
		}
       
		APP_DELEGATE.saveContext()
	}
	
	class func saveContactInDB(modelWorkoutDetail:ModelWorkoutDetail, isForEdit: Bool = false){
        
        print("call method  Coredata db")
        
		let predicate = NSPredicate(format: "workoutId == '\(modelWorkoutDetail.workoutId!)'")
        
        if someEntityExists(Workoutid: "\(modelWorkoutDetail.workoutId ?? "")") == true
        {
            print("exist")
            return
        }else
        {
            print("Not exist")
            
        }
        
		var workout: Workout!
		if let arrMusicTrack = Workout.getWorkoutFromContext(params: predicate) , arrMusicTrack.count > 0 {
			workout = arrMusicTrack.first as? Workout
		}
		else
		{
			if isForEdit == false{
				let context = APP_DELEGATE.managedObjectContext
				let entity = NSEntityDescription.entity(forEntityName: "Workout", in: (context))
				workout = Workout(entity: entity!, insertInto: (context))
                workout.workoutCreatedDate = Date()
			}
		}
		
		if workout != nil{
			Workout.addNewRecoredInDB(modelWorkoutDetail: modelWorkoutDetail,workout: workout)
		}
	}
    
    
    
 class  func someEntityExists(Workoutid: String) -> Bool {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "Workout")
        fetchRequest.predicate = NSPredicate(format: "workoutId = %@", Workoutid)
    
        var results: [NSManagedObject] = []
        
        do {
            results = try APP_DELEGATE.managedObjectContext.fetch(fetchRequest)
            
        }
        catch {
            print("error executing fetch request: \(error)")
        }
        
        return results.count > 0
    }
}
