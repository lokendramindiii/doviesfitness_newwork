//
//  MyWorkoutLogTableCell.swift
//  Dovies
//
//  Created by Neel Shah on 16/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class MyWorkoutLogTableCell: UITableViewCell {
	
	@IBOutlet weak var IBimgWorkout: UIImageView!
	@IBOutlet weak var IBlblWight: UILabel!
	@IBOutlet weak var IBlblDuration: UILabel!
	@IBOutlet weak var IBlblDate: UILabel!
	@IBOutlet weak var IBlblWorkOutName: UILabel!
	@IBOutlet weak var IBbtnDetail: UIButton!
    @IBOutlet weak var IBbtnReview: UIButton!
    @IBOutlet weak var IBbtnViewImages: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
       IBbtnReview.titleLabel?.numberOfLines = 1
       IBbtnReview.titleLabel?.adjustsFontSizeToFitWidth = true
        IBbtnReview.titleLabel?.lineBreakMode = .byClipping
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
	func setWorkOutLogData(modelWorkOutLogs: ModelWorkoutList) {
		
		let duration = "Duration:"
		let originalStr = "\(duration) \(modelWorkOutLogs.workoutTotalTime!)"

		
		IBlblDuration.text = modelWorkOutLogs.workoutTotalTime!
		
	//	IBimgWorkout.sd_setImage(with: URL(string: modelWorkOutLogs.workoutImage), placeholderImage: AppInfo.placeholderImage)
        
            IBimgWorkout.sd_setImage(with: URL(string: modelWorkOutLogs.workoutImage), placeholderImage: nil)
        
		IBlblDate.text = modelWorkOutLogs.logDate
		IBlblWight.text = modelWorkOutLogs.customerWeight
		IBlblWorkOutName.text = modelWorkOutLogs.workoutName
    IBbtnReview.setTitle(modelWorkOutLogs.workoutFeedbackStatus, for: .normal)
        
        
        if modelWorkOutLogs.workoutNote == "" || modelWorkOutLogs.workoutNote.isEmpty
        {
            IBbtnDetail.isUserInteractionEnabled = false
            IBbtnDetail.setTitleColor(UIColor(r: 199, g: 199, b: 199, alpha: 1), for: .normal)
        }
        else
        {
            IBbtnDetail.isUserInteractionEnabled = true
            IBbtnDetail.setTitleColor(UIColor.black, for: .normal)
        }
        
        
        let titleColor = modelWorkOutLogs.arrWorrkoutLogImages.count>0 ? UIColor.black : UIColor(r: 199, g: 199, b: 199, alpha: 1)
        IBbtnViewImages.setTitleColor(titleColor, for: .normal)
	}
    
}
