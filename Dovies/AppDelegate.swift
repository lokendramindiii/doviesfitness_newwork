//
//  AppDelegate.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

import IQKeyboardManagerSwift
import FBSDKCoreKit
import TwitterKit
import UserNotifications
import Fabric
import Crashlytics
import CoreData
import Branch

import Firebase
import FirebaseCore
import FirebaseMessaging
import StoreKit

//In-App Account
//doviesTestUser@inapp.com/Dovies@123
//
// http://doviesworkout.projectspreview.net/NS/check_in_app_purchase_subscription?
// http://doviesworkout.projectspreview.net/NS/update_subscriptions

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    let notificationDelegate = SampleNotificationDelegate()

	var window: UIWindow?
	var isRotateAllow = false
    var profileNavController : UINavigationController?
	let downloadService = DownloadService()
    var kDeviceToken = ""
    
    var isFromNotification = false
    var notificationType = ""
    var notificationId = ""
    var selectedTabbarIndex : Int = 0

    
	func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        UIApplication.shared.statusBarStyle = .lightContent

    // Override point for customization after application launch.
        
        //sharedobjec.loadSubscriptionOptions()
        
        //sharedobjec.restorePurchases()

      UIApplication.shared.isIdleTimerDisabled = false

      let MyprofileNav = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "MyProfileVC") as! MyProfileVC
        profileNavController = MyprofileNav.navigationController.self
       print(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])

		Fabric.with([Crashlytics.self])
		
	 	setAppearanceMethod()
        
        inititaliseTirdParty(with : launchOptions)
        setStoryBoardBasedOnUserLogin()
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        TWTRTwitter.sharedInstance().start(withConsumerKey:AppInfo.twitterConsumerKey, consumerSecret:AppInfo.twitterConsumerSecret)
        
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
        }
		LocalNotification.registerForLocalNotification(on: application)
        
        // For remote Notification
        
        FirebaseApp.configure()
        self.registerForRemoteNotification()

        if UserDefaults.standard.string(forKey: "isReminderTime") == nil{
            let formatter = DateFormatter()
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "yyyy-MM-dd"
            let dateString = formatter.string(from: Date())
            let strTime = "07:00 AM"
            let strDateTime = dateString + " " + strTime
            let selectedDate = NSDate.stringToDate(strDate: strDateTime, format: "yyyy-MM-dd h:mm a")
            LocalNotification.dispatchlocalNotification(with: "", body: "It's time for your workout. Be inspired Always", at: selectedDate)
        }
        
        DoviesGlobalUtility.checkNetworkStatus()
        
		try? AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback, with: .mixWithOthers)
		try? AVAudioSession.sharedInstance().setActive(true)
		
		return true
	}
    
    
    //    // Respond to URI scheme links
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        // pass the url to the handle deep link call
        /*
         let branchHandled = Branch.getInstance().application(application,
         open: url,
         sourceApplication: sourceApplication,
         annotation: annotation
         )
         if (!branchHandled) {
         // If not handled by Branch, do other deep link routing for the Facebook SDK, Pinterest SDK, etc
         }
         // do other deep link routing for the Facebook SDK, Pinterest SDK, etc
         */
        return true
    }
    //
    // Respond to Universal Links
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        // pass the url to the handle deep link call
        //Branch.getInstance().continue(userActivity)
        
        print("Continue User Activity called: ")
        
        if userActivity.activityType == NSUserActivityTypeBrowsingWeb {
            if let url = userActivity.webpageURL {
                
                
                print(url.absoluteString)
                if let dict = url.queryParameters{
                    
                    print("qParams = \(dict)")
                    
                    let decodedData = Data(base64Encoded:dict["share"] ?? "")
                    
                    let decodeUrl = String(data: decodedData!, encoding: .utf8)!
                    let paramsArray = decodeUrl.components(separatedBy: "&")
                    
                    if paramsArray.count == 0 || paramsArray.count == 1
                    {
                        
                        if url.absoluteString.contains("reset-password") {
                            GlobalUtility.openUrlFromApp(url:url.absoluteString)
                        }
                        else
                        {
                            print("Other url ")
                        }
                    }
                    else
                    {
                    let moduleNameSplit    = paramsArray[0]
                    let  mIdSplit = paramsArray[1]
                    
                    let ModuleNameArray = moduleNameSplit.components(separatedBy: "/")
                    let MouduleIdArray = mIdSplit.components(separatedBy: "/")

                    let moduleName    = ModuleNameArray[1]
                    let  mId = MouduleIdArray[1]

                    self.redirectBasedOnModuleName(moduleName, mId)
                    }
                    /*
                     if let moduleName = dict[WsParam.moduleName] {
                     if let mId = dict[WsParam.moduleId]{
                     
                     let decodedData = Data(base64Encoded: moduleName)
                     let decodedDataId = Data(base64Encoded: mId)
                     if decodedData != nil && decodedDataId != nil
                     {
                     
                     let decodeModelName = String(data: decodedData!, encoding: .utf8)!
                     print(decodeModelName)
                     let decodeModelId = String(data: decodedDataId!, encoding: .utf8)!
                     print(decodeModelId)
                     self.redirectBasedOnModuleName(decodeModelName, decodeModelId)
                     }
                     else
                     {
                     self.redirectBasedOnModuleName(moduleName, mId)
                     }
                     }
                     }
                     */
                }
            }
        }
        return true
    }
  
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool {
        /*
         let branchHandled = Branch.getInstance().application(app, open: url, options: options)
         if (!branchHandled) {
         if FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options){
         return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
         }
         if  TWTRTwitter.sharedInstance().application(app, open: url, options: options) {
         return  TWTRTwitter.sharedInstance().application(app, open: url, options: options)
         }
         }
         */
        
        if FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options){
            return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        }
        if  TWTRTwitter.sharedInstance().application(app, open: url, options: options) {
            return  TWTRTwitter.sharedInstance().application(app, open: url, options: options)
        }
        return true
    }
    
	
	func applicationWillResignActive(_ application: UIApplication) {
		// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
		// Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
	}
	
    
	func applicationDidEnterBackground(_ application: UIApplication) {
		// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
		// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        

	}
	
	func applicationWillEnterForeground(_ application: UIApplication) {
		// Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        

	}
	
	func applicationDidBecomeActive(_ application: UIApplication) {
        
		// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	}
	
	func applicationWillTerminate(_ application: UIApplication) {
		// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        

	}
	
//    @available(iOS 10.0, *)
//    private func userNotificationCenter( center: UNUserNotificationCenter,didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
//        // do something with the notification
//        print(response.notification.request.content.userInfo)
//        // the docs say you should execute this asap
//        return completionHandler()
//    }
//
//    // called if app is running in foreground
//    @available(iOS 10.0, *)
//    private func userNotificationCenter( center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler:@escaping (UNNotificationPresentationOptions) -> Void) {
//        // show alert while app is running in foreground
//        return completionHandler(UNNotificationPresentationOptions.alert)
//    }
//
    
    //    MARK:- Helper Methods -
    func inititaliseTirdParty(with options: [UIApplicationLaunchOptionsKey: Any]?){
        
        //Keyboard manager setup
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.previousNextDisplayMode = .alwaysShow
        IQKeyboardManager.shared.disabledToolbarClasses = [CommentsVC.self,NewsFeedMediaDetailsVC.self,CommentNotificationListVC.self,NotificationDetailVC.self]
        
//        Branch.getInstance().initSession(launchOptions: options) { params , error in
//            if let dic = params as? [String: AnyObject] {
//                if let moduleName = dic[WsParam.moduleName] as? String {
//                    if let mId = dic[WsParam.moduleId] as? String{
//                        self.redirectBasedOnModuleName(moduleName, mId)
//                    }
//                }
//            }
//        }
    }
    
    public func redirectBasedOnModuleName(_ moduleName : String , _ moduleId : String){

         guard let vc = UIApplication.shared.keyWindow?.getVisibleViewController(),let centerVC = vc.sideMenuController?.centerViewController as? TabBarViewController else{ return}
        
        if let finalVC = (centerVC.selectedViewController as? UINavigationController)?.viewControllers.last{
            switch moduleName {
            case ModuleName.dietPlan:
                let respectiveVC = Storyboard.DietPlan.instantiate(DietPlanDetailsVC.self) as DietPlanDetailsVC
                respectiveVC.planId = moduleId
                finalVC.navigationController?.pushViewController(respectiveVC, animated: true)
                
            case ModuleName.exercise:
                let wVC = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "WorkoutDetailsVC") as! WorkoutDetailsVC
                wVC.exerciseId = moduleId
                wVC.screenType = .workOutDetailList
                finalVC.navigationController?.pushViewController(wVC, animated: true)
                
            case ModuleName.newsFeed:
                let newsFeedDetailsVC = Storyboard.NewsFeed.storyboard().instantiateViewController(withIdentifier: "NewsFeedMediaDetailsVC") as! NewsFeedMediaDetailsVC
                newsFeedDetailsVC.newsFeedId = moduleId
                newsFeedDetailsVC.isFromShare = true
                finalVC.navigationController?.pushViewController(newsFeedDetailsVC, animated: true)
                
            case ModuleName.program:
                let programDetailsVC = Storyboard.Programs.instantiate(ProgramDetail_VC.self)
                programDetailsVC.programId = moduleId
                programDetailsVC.isFromShared = true
                finalVC.navigationController?.pushViewController(programDetailsVC, animated: true)
                
            case ModuleName.workout:
                let featuredWorkOutDetailViewControlle = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "FeaturedWorkOutDetailViewController") as! FeaturedWorkOutDetailViewController
                featuredWorkOutDetailViewControlle.workoutId = moduleId
               // featuredWorkOutDetailViewControlle.isFromShare = true

                featuredWorkOutDetailViewControlle.workoutScreenType = .sharedWorkout
                featuredWorkOutDetailViewControlle.navigationItem.title = ""
                finalVC.navigationController?.pushViewController(featuredWorkOutDetailViewControlle, animated: true)
                
            default:
                break
            }
        }
        
        
    }
    
    
    private func redirectionForAPNS(_ moduleName : String , _ moduleId : String,_ exName:String){
        
        guard let vc = UIApplication.shared.keyWindow?.getVisibleViewController(),let centerVC = vc.sideMenuController?.centerViewController as? TabBarViewController else{ return}
        
         var repsectiveNavRootCV = (centerVC.selectedViewController as? UINavigationController)?.viewControllers.last
        
            switch moduleName
            {
//            case ModuleName.dietPlan:
//                centerVC.selectedIndex = 2
//                repsectiveNavRootCV = (centerVC.selectedViewController as? UINavigationController)?.viewControllers.last
//                let respectiveVC = Storyboard.DietPlan.instantiate(DietPlanDetailsVC.self) as DietPlanDetailsVC
//                respectiveVC.planId = moduleId
//                repsectiveNavRootCV?.navigationController?.pushViewController(respectiveVC, animated: true)
//
            case ModuleName.exercise:
                centerVC.selectedIndex = 1
                repsectiveNavRootCV = (centerVC.selectedViewController as? UINavigationController)?.viewControllers.last
                let wVC = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "WorkoutDetailsVC") as! WorkoutDetailsVC
                wVC.exerciseId = moduleId
               // wVC.screenType = .workOutList // Ais change by code 05-03
                
                wVC.screenType = .workOutDetailList
                wVC.navigationItem.title = exName;
            repsectiveNavRootCV?.navigationController?.pushViewController(wVC, animated: true)
                
            case ModuleName.newsFeed:
                centerVC.selectedIndex = 0
                repsectiveNavRootCV = (centerVC.selectedViewController as? UINavigationController)?.viewControllers.last
                let newsFeedDetailsVC = Storyboard.NewsFeed.storyboard().instantiateViewController(withIdentifier: "NewsFeedMediaDetailsVC") as! NewsFeedMediaDetailsVC
                newsFeedDetailsVC.newsFeedId = moduleId
                //newsFeedDetailsVC.isFromShare = true
                repsectiveNavRootCV?.navigationController?.pushViewController(newsFeedDetailsVC, animated: true)
                
            case ModuleName.program:
                centerVC.selectedIndex = 3
                repsectiveNavRootCV = (centerVC.selectedViewController as? UINavigationController)?.viewControllers.last
                let programDetailsVC = Storyboard.Programs.instantiate(ProgramDetail_VC.self)
                programDetailsVC.programId = moduleId
                //programDetailsVC.isFromShared = true
                repsectiveNavRootCV?.navigationController?.pushViewController(programDetailsVC, animated: true)
                
            case ModuleName.custom:
                centerVC.selectedIndex = 4
                repsectiveNavRootCV = (centerVC.selectedViewController as? UINavigationController)?.viewControllers.last
                let notificationList = Storyboard.MyPlan.instantiate(NotificationVC.self)
               // programDetailsVC.programId = moduleId
            repsectiveNavRootCV?.navigationController?.pushViewController(notificationList, animated: true)
                
            case ModuleName.workout:
                centerVC.selectedIndex = 1
                repsectiveNavRootCV = (centerVC.selectedViewController as? UINavigationController)?.viewControllers.last
                let featuredWorkOutDetailViewControlle = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "FeaturedWorkOutDetailViewController") as! FeaturedWorkOutDetailViewController
                featuredWorkOutDetailViewControlle.workoutId = moduleId
                featuredWorkOutDetailViewControlle.workoutScreenType = .sharedWorkout
                featuredWorkOutDetailViewControlle.navigationItem.title = ""
            repsectiveNavRootCV?.navigationController?.pushViewController(featuredWorkOutDetailViewControlle, animated: true)
                
            default:
                break
            }
        
    }
    
    //redirecting based on user login
    func setStoryBoardBasedOnUserLogin(){
        if let userDic = UserDefaults.standard.value(forKey: UserDefaultsKey.userData) as? [String : Any]{
            DoviesGlobalUtility.currentUser = User(fromDictionary: userDic)
            DoviesGlobalUtility.setFilterData()
            
            
            guard let window = UIApplication.shared.windows.first else { return }
            window.rootViewController = Storyboard.Main.storyboard().instantiateInitialViewController()

        }else{
            guard let window = UIApplication.shared.windows.first else { return }
            window.rootViewController = Storyboard.Login.storyboard().instantiateInitialViewController()
        }
    }
    
    //redirecting based on user login
    func setStoryBoardBasedMyprofile(){
        if let userDic = UserDefaults.standard.value(forKey: UserDefaultsKey.userData) as? [String : Any]{
            DoviesGlobalUtility.currentUser = User(fromDictionary: userDic)
            
            
            guard let window = UIApplication.shared.windows.first else { return }
            window.rootViewController = Storyboard.Main.storyboard().instantiateInitialViewController()
            
        }else{
            guard let window = UIApplication.shared.windows.first else { return }
            window.rootViewController = Storyboard.Login.storyboard().instantiateInitialViewController()
        }
    }

    

    /// Setting up appearances methods for NavigationBar and Bar buttons
    func setAppearanceMethod(){
        
        UINavigationBar.appearance().layer.masksToBounds = false
        UINavigationBar.appearance().isTranslucent = true
        
        
        UINavigationBar.appearance().shadowImage = UIColor.colorWithRGB(r: 165, g: 165, b: 165, alpha: 0.7).as1ptImage()

     //   UINavigationBar.appearance().tintColor = kAppNavigationColor
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedStringKey.foregroundColor : UIColor.colorWithRGB(r: 34.0, g: 34.0, b: 34.0),
            NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)
        ]
        
        
        
      //  UINavigationBar.appearance().barTintColor = kAppNavigationColor
        
       // UINavigationBar.appearance().barTintColor = UIColor (r: 0, g: 0, b: 0, alpha: 1.0)


        UIBarButtonItem.appearance().tintColor = UIColor.black
        UITabBarItem.appearance().titlePositionAdjustment = UIOffset(horizontal: 0, vertical: 0)
        
       // UINavigationBar.appearance().backgroundColor = UIColor.clear
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font: UIFont.tamilBold(size: 11.0)], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.font: UIFont.tamilBold(size: 11.0)], for: .selected)
        

        
        let selectedColor   = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
        //    let unselectedColor = UIColor(red: 162/255.0, green: 162/255.0, blue: 162/255.0, alpha: 1.0)
        let unselectedColor = UIColor(red: 162/255.0, green: 162/255.0, blue: 162/255.0, alpha: 1.0)
        
        
             UITabBar.appearance().isTranslucent = true
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: unselectedColor], for: .normal)
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: selectedColor], for: .selected)
        
        UITabBar.appearance().tintColor = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)
        //*** narendra ** change tabbar background color
        //  UITabBar.appearance().barTintColor = UIColor.red

      //  UITabBar.appearance().backgroundImage = image
       // UITabBar.appearance().barTintColor = UIColor (r: 0, g: 0, b: 0, alpha: 0.6)
        
        
        self.window?.backgroundColor = UIColor.white
        //        let navBarAppearance = UINavigationBar.appearance()
        //        var image = UIImage()
        //
        //        image = imageFromColor(color: UIColor(r: 0, g: 0, b: 0, alpha: 1.0), frame: CGRect(x: 0, y: 0, width: self.window!.frame.size.width, height: 88))
        //
        //        navBarAppearance.setBackgroundImage(image, for: .default)

    }
    
    func imageFromColor(color: UIColor, frame: CGRect) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(frame.size, false, 0)
        color.setFill()
        UIRectFill(frame)
        let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
        UIGraphicsEndImageContext()
        return image
    }



	func application(_ application: UIApplication, supportedInterfaceOrientationsFor window: UIWindow?) -> UIInterfaceOrientationMask {
		if isRotateAllow == true{
			return [.landscapeRight, .portrait]
		}
		return .portrait
	}
	
	// MARK: - Core Data stack
	lazy var applicationDocumentsDirectory: URL = {
		// The directory the application uses to store the Core Data store file. This code uses a directory named "com.mbj.DreamChaser" in the application's documents Application Support directory.
		let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
		print(urls[urls.count-1])
		return urls[urls.count-1]
	}()
	
	lazy var managedObjectModel: NSManagedObjectModel = {
		// The managed object model for the application. This property is not optional. It is a fatal error for the application not to be able to find and load its model.
		let modelURL = Bundle.main.url(forResource: "Dovies", withExtension: "momd")!
		return NSManagedObjectModel(contentsOf: modelURL)!
	}()
	
	lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
		// The persistent store coordinator for the application. This implementation creates and returns a coordinator, having added the store for the application to it. This property is optional since there are legitimate error conditions that could cause the creation of the store to fail.
		// Create the coordinator and store
		let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
		let url = self.applicationDocumentsDirectory.appendingPathComponent("Dovies.sqlite")
		var failureReason = "There was an error creating or loading the application's saved data."
		do {
			let mOptions = [NSMigratePersistentStoresAutomaticallyOption: true,
							NSInferMappingModelAutomaticallyOption: true]
			
			try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: mOptions)
			//            try coordinator.addP
		} catch let error as NSError {
			// Report any error we got.
			var dict = [String: AnyObject]()
			dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
			dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
			
			dict[NSUnderlyingErrorKey] = error
			let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
			// Replace this with code to handle the error appropriately.
			// abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
			NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
			abort()
		} catch {
			// dummy
		}
		
		return coordinator
	}()
	
	lazy var managedObjectContext: NSManagedObjectContext = {
		// Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
		let coordinator = self.persistentStoreCoordinator
		var managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
		managedObjectContext.persistentStoreCoordinator = coordinator
		return managedObjectContext
	}()
	
	lazy var managedObjectContextMainQueueContact: NSManagedObjectContext = {
		// Returns the managed object context for the application (which is already bound to the persistent store coordinator for the application.) This property is optional since there are legitimate error conditions that could cause the creation of the context to fail.
		let coordinator = self.persistentStoreCoordinator
		var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
		managedObjectContext.persistentStoreCoordinator = coordinator
		return managedObjectContext
	}()
	
	// MARK: - Core Data Saving support
	
	func saveContext () {
		if self.managedObjectContext.hasChanges {
			do {
				try self.managedObjectContext.save()
			} catch {
				// Replace this implementation with code to handle the error appropriately.
				// abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
				let nserror = error as NSError
				NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
				//abort()
			}
		}
	}
   
    
}


extension AppDelegate : MessagingDelegate,UNUserNotificationCenterDelegate{
    func registerForRemoteNotification() {
        // iOS 10 support
        if #available(iOS 10, *) {
            let authOptions : UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options:authOptions){ (granted, error) in
                UNUserNotificationCenter.current().delegate = self as UNUserNotificationCenterDelegate
                Messaging.messaging().delegate = self
                
                
                let deafultCategory = UNNotificationCategory(identifier: "CustomSamplePush", actions: [], intentIdentifiers: [], options: [])
                 let center = UNUserNotificationCenter.current()
                center.setNotificationCategories(Set([deafultCategory]))

            }
        }else {
            
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }
        UIApplication.shared.registerForRemoteNotifications()
        
        NotificationCenter.default.addObserver(self, selector:
            #selector(tokenRefreshNotification), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
    }
    
    func messaging(_ messaging: Messaging, didRefreshRegistrationToken fcmToken: String) {
        print("FCM registration token: \(fcmToken)")
        kDeviceToken = fcmToken
            UserDefaults.standard.set(kDeviceToken, forKey: UserDefaultsKey.deviceToken)
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        kDeviceToken = fcmToken
        UserDefaults.standard.set(kDeviceToken, forKey: UserDefaultsKey.deviceToken)
    }
    
    //MARK: Notification
    @objc func tokenRefreshNotification(_ notification: Notification) {
        if let refreshedToken = InstanceID.instanceID().token() {
            print("InstanceID token: \(refreshedToken)")
            kDeviceToken = refreshedToken

            UserDefaults.standard.set(kDeviceToken, forKey: UserDefaultsKey.deviceToken)
            
        }
    }

    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo as? [String : Any] ?? [:]
        
        print("willPresent notification = \(userInfo)")
        completionHandler([.alert,.sound,.badge])
        
    }
    
    //called When you tap on the notification
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo as? [String : Any] ?? [:]
        print("didReceive notification = \(userInfo)")
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        notificationVC.readUnreadStatus = "Unread"
        handleNotificationWith(userInfo: userInfo)
    }
    
    func handleNotificationWith(userInfo:[AnyHashable : Any]){
        isFromNotification = true
        let  msgBody = userInfo["body"] as? String ?? ""
        notificationType = userInfo[WsParam.moduleName] as? String ?? ""
        notificationId = userInfo[WsParam.moduleId] as? String ?? ""
        let exName = userInfo["name"] as? String ?? ""
        print(notificationType)
        print(notificationId)
        self.redirectionForAPNS(notificationType, notificationId,exName)
    }
    
}


extension UINavigationController {
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
extension UIColor {
    
    /// Converts this `UIColor` instance to a 1x1 `UIImage` instance and returns it.
    ///
    /// - Returns: `self` as a 1x1 `UIImage`.
    func as1ptImage() -> UIImage {
        UIGraphicsBeginImageContext(CGSize(width: 1, height: 0.3))
        setFill()
        UIGraphicsGetCurrentContext()?.fill(CGRect(x: 0, y: 0, width: 1, height: 0.3))
        let image = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
        UIGraphicsEndImageContext()
        return image
    }
}



/* Dovies 1.5 lounch after  issue fixed
 
 1. Remove filcker line on Newsfeed.
 2. Featured workout detail - Top white background color show  When scrolling Fixed.
 3. Reset passwrod  deep linking issue fixed.
 4.iOS-Changing notification time is crushing the app
 
 */
