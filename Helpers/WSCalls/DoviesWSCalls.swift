//
//  DoviesWSCalls.swift
//  Dovies
//
//  Created by hb on 01/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class DoviesWSCalls: NSObject {
    
    //MARK: - Authentication APIs
    class func callLoginApi(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ userData: [String: Any]?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.login, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                var dicData: [String: Any]?
                let result = WsCalls.processResponse(dict: response.result.value as? [String: Any])
                if let arr = result.data as? [[String: Any]],arr.count > 0 {
                    dicData = arr[0]
                }
                onSuccess?(result.success, result.message, dicData)
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    class func callSingupWS(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ userData: [String: Any]?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.signUp, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                var dicData: [String: Any]?
                let result = WsCalls.processResponse(dict: response.result.value as? [String: Any])
                if let arr = result.data as? [[String: Any]],arr.count > 0{
                    dicData = arr[0]
                }
                onSuccess?(result.success, result.message, dicData)
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    
    class func callSingupPostWS(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ userData: [String: Any]?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        
        WSHelper.sharedInstance.getResponseFromURL(url: WsUrl.signUp, requestType: .post, requestParameters: dicParam, uploadProgress: { (progress) in
            print(progress.fractionCompleted)
        }) { (dicResponse, error) in
            if error == nil {
                var dicData: [String: Any]?
                let result = WsCalls.processResponse(dict: dicResponse as? [String: Any])
                if let arr = result.data as? [[String: Any]],arr.count > 0{
                    dicData = arr[0]
                }
                onSuccess?(result.success, result.message, dicData)
            } else {
                onFailure?(error)
            }
        }
    }
    
    class func callUsernameAvailabilityAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ response: [String : Any]?, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.userAvailability, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                onSuccess?(result.success, result.data as? [String : Any], result.message)
                
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    //----- workout publish api
    class func callGetpublishWorkoutAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ response: [String : Any]?, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.Getpublishworkout, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                onSuccess?(result.success, result.data as? [String : Any], result.message)
                
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    class func callEmailAvailabilityAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ response: [String : Any]?, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.emailAvailability, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                onSuccess?(result.success, result.data as? [String : Any], result.message)
                
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    class func callUpdateUserInfoAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ userData: [String: Any]?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
      WSHelper.sharedInstance.getResponseFromURL(url: WsUrl.updateCustomerDetail, requestType: .post, requestParameters: dicParam, uploadProgress: { (progress) in
			print(progress.fractionCompleted)
		}) { (dicResponse, error) in
			if error == nil {
				var dicData: [String: Any]?
				let result = WsCalls.processResponse(dict: dicResponse as? [String: Any])
				if let arr = result.data as? [[String: Any]],arr.count > 0{
					dicData = arr[0]
				}
				onSuccess?(result.success, result.message, dicData)
			} else {
				onFailure?(error)
			}
		}
    }
    
    
    class func callForgotPasswordAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ userData: [String: Any]?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.forgotPassword, method: .post, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                var dicData: [String: Any]?
                let result = WsCalls.processResponse(dict: response.result.value as? [String: Any])
                if let arr = result.data as? [[String: Any]],arr.count > 0{
                    dicData = arr[0]
                }
                onSuccess?(result.success, result.message, dicData)
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    class func callGetUserInfoAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ userData: [String: Any]?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sharedInstance.getResponseFromURL(url: WsUrl.getCustomerDetail, requestType: .get, requestParameters: dicParam, uploadProgress: { (progress) in
            print(progress.fractionCompleted)
        }) { (dicResponse, error) in
            if error == nil {
                var dicData: [String: Any]?
                let result = WsCalls.processResponse(dict: dicResponse as? [String: Any])
                if let arr = result.data as? [[String: Any]],arr.count > 0{
                    dicData = arr[0]
                }
                onSuccess?(result.success, result.message, dicData)
            } else {
                onFailure?(error)
            }
        }
    }
	
    
    class func callGetNotificationStatusAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ userData: [String: Any]?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sharedInstance.getResponseFromURL(url: WsUrl.getNotificationStatus, requestType: .get, requestParameters: dicParam, uploadProgress: { (progress) in
            print(progress.fractionCompleted)
        }) { (dicResponse, error) in
            if error == nil {
                var dicData: [String: Any]?
                let result = WsCalls.processResponse(dict: dicResponse as? [String: Any])
                if let arr = result.data as? [[String: Any]],arr.count > 0{
                    dicData = arr[0]
                }
                onSuccess?(result.success, result.message, dicData)
            } else {
                onFailure?(error)
            }
        }
    }
    
	class func callChangePasswordAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ userData: [String: Any]?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
		WSHelper.sharedInstance.getResponseFromURL(url: WsUrl.changePassword, requestType: .get, requestParameters: dicParam, uploadProgress: { (progress) in
			print(progress.fractionCompleted)
		}) { (dicResponse, error) in
			if error == nil {
				var dicData: [String: Any]?
				let result = WsCalls.processResponse(dict: dicResponse as? [String: Any])
				if let arr = result.data as? [[String: Any]],arr.count > 0{
					dicData = arr[0]
				}
				onSuccess?(result.success, result.message, dicData)
			} else {
				onFailure?(error)
			}
		}
	}
    
    class func callCreateWorkoutAPI(isForEdit: Bool  , dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ userData: [String: Any]?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
       
        var url = ""
        if isForEdit == true{
            url = WsUrl.editWorkout
        }
        else{
            url = WsUrl.addWorkout
        }
        
        
        WSHelper.sharedInstance.getResponseFromURL(url: url, requestType: .post, requestParameters: dicParam, uploadProgress: { (progress) in
            print(progress.fractionCompleted)
        }) { (dicResponse, error) in
            if error == nil {
                var dicData: [String: Any]?
                let result = WsCalls.processResponse(dict: dicResponse as? [String: Any])
                if let arr = result.data as? [[String: Any]],arr.count > 0{
                    dicData = arr[0]
                }
                onSuccess?(result.success, result.message, dicData)
            } else {
                onFailure?(error)
            }
        }
    }
    class func callNewsFeedAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ userData: [String: Any]?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.newsFeed, method: .post, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                var dicData: [String: Any]?
                let result = WsCalls.processResponse(dict: response.result.value as? [String: Any])
                if let arr = result.data as? [[String: Any]],arr.count > 0{
                    dicData = arr[0]
                }
                onSuccess?(result.success, result.message, dicData)
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    
     //MARK: - Diet plan APIs
    /// This method calls 'get diet plan category list' web service.
    ///
    /// - Parameters:
    ///   - dicParam: parameter to pass
    ///   - onSuccess: success block
    ///   - onFailure: failure block
    class func callGetDietPlanCategoryListAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ dietPlans: [DietPlanCategory]?, _ message: String?,_ hasNextPage : Bool,_ dietPlanStaticData : DietPlanStaticData?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.dietPlanCategories, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                var arrCategories: [DietPlanCategory]?
                var dietPlanInfo : DietPlanStaticData?
                let result = WsCalls.processResponseWithPagination(dict: response.result.value as? [String : Any])
                if let dictdata = result.data as? [String: Any], result.success {
                    if let arrDeitPlans = dictdata["diet_plan_categories"] as? [[String: Any]]{
                        arrCategories = [DietPlanCategory]()
                        for dicCategory in arrDeitPlans {
                            arrCategories?.append(DietPlanCategory(fromDictionary: dicCategory))
                        }
                    }
                    if let arrInfo = dictdata[WsParam.getStaticContent] as? [[String: Any]]{
                        dietPlanInfo = DietPlanStaticData(fromDictionary: arrInfo[0])
                    }
                }
                onSuccess?(result.success, arrCategories, result.message, result.nextPageStatus, dietPlanInfo)
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    /// This method calls 'get diet plan list' web service.
    ///
    /// - Parameters:
    ///   - dicParam: parameter to pass
    ///   - onSuccess: success block
    ///   - onFailure: failure block
    class func callGetDietPlanListAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ dietPlans: [DietPlan]?,_ dietPlanData : DietPlanStaticData?, _ message: String?, _ isNextPageAvailable: Bool) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.dietPlans, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                var arrDietList: [DietPlan]?
                var dietPlanInfo : DietPlanStaticData?
                let result = WsCalls.processResponseWithPagination(dict: response.result.value as? [String : Any])
                if let dictResponse = result.data as? [String: Any], result.success {
                    arrDietList = [DietPlan]()
                    
                    if let arr = dictResponse[WsParam.dietListing] as? [[String: Any]]{
                        for dicDiet in arr {
                            arrDietList?.append(DietPlan(fromDictionary: dicDiet))
                        }
                    }
                    if let arrInfo = dictResponse[WsParam.dietStaticContent] as? [[String: Any]]{
                        dietPlanInfo = DietPlanStaticData(fromDictionary: arrInfo[0])
                    }
                    
                }
                onSuccess?(result.success, arrDietList, dietPlanInfo, result.message,result.nextPageStatus)
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    /// This method calls 'get diet plan deatils' web service.
    ///
    /// - Parameters:
    ///   - dicParam: parameter to pass
    ///   - onSuccess: success block
    ///   - onFailure: failure block
    class func callGetDietPlanDetailsAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ dietPlanDetails: DietPlanDetails?, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.dietPlanDetails, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                var plan : DietPlanDetails?
                if let dictResponse = result.data as? [[String: Any]], result.success {
                    plan = DietPlanDetails(fromDictionary: dictResponse[0])
                    
                }
                onSuccess?(result.success, plan, result.message)
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    
    
     //MARK: - Program APIs
    /// This method calls 'get programs list' web service.
    ///
    /// - Parameters:
    ///   - dicParam: parameter to pass
    ///   - onSuccess: success block
    ///   - onFailure: failure block
    class func callGetPrograListAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ dietPlans: [Program]?,_ dietPlanData : ProgramStaticInfo?, _ message: String?,_ hasNextPage : Bool) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.programs, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                var arrProgramsList: [Program]?
                var programInfo : ProgramStaticInfo?
                let result = WsCalls.processResponseWithPagination(dict: response.result.value as? [String : Any])
                if let dictResponse = result.data as? [String: Any], result.success {
                    arrProgramsList = [Program]()
                    
                    if let arr = dictResponse[WsParam.getAllPrograms] as? [[String: Any]]{
                        for dicProgram in arr {
                                arrProgramsList?.append(Program(fromDictionary: dicProgram))
                            
                        }
                    }
                    if let arrInfo = dictResponse[WsParam.getProgramStaticContent] as? [[String: Any]]{
                        programInfo = ProgramStaticInfo(fromDictionary: arrInfo[0])
                    }
                    
                }
                onSuccess?(result.success, arrProgramsList, programInfo, result.message, result.nextPageStatus )
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    /// This method calls 'get program deatils' web service.
    ///
    /// - Parameters:
    ///   - dicParam: parameter to pass
    ///   - onSuccess: success block
    ///   - onFailure: failure block
    class func callGetProgramDetailsAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ programDetails: ProgramDetails?, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.programDetails, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                var program : ProgramDetails?
                if let dictResponse = result.data as? [[String: Any]], result.success {
                    program = ProgramDetails(fromDictionary: dictResponse[0])
                    
                }
                onSuccess?(result.success, program, result.message)
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    //MARK: - News Feed APIs
    /// This method calls 'get News Feed deatils' web service.
    ///
    /// - Parameters:
    ///   - dicParam: parameter to pass
    ///   - onSuccess: success block
    ///   - onFailure: failure block
    class func callGetNewsFeedsAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ featuredNews: [NewsFeedModel]?,_ otherNews : [NewsFeedModel]?, _ message: String? ,_ receiptStatus:String?,_ iosPackageId:String?,_ pakageMasterId:String? , _ nextPageStatus : Bool) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.newsFeed, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                var arrFeaturedNews: [NewsFeedModel]?
                var arrOtherNews: [NewsFeedModel]?
                var receiptReceived:String = ""
                var packageMasterId:String = ""
                var iosPackageId:String = ""

                let result = WsCalls.processResponseWithPagination(dict: response.result.value as? [String : Any])
                if let dictResponse = result.data as? [String: Any], result.success {
                    arrFeaturedNews = [NewsFeedModel]()
                    arrOtherNews = [NewsFeedModel]()
                    if let arr = dictResponse[WsParam.featuredNews] as? [[String: Any]]{
                        for dic in arr {
                            arrFeaturedNews?.append(NewsFeedModel(fromDictionary: dic))
                        }
                    }
                    if let arrInfo = dictResponse[WsParam.allOtherFeatured] as? [[String: Any]]{
                        for dic in arrInfo {
                            arrOtherNews?.append(NewsFeedModel(fromDictionary: dic))
                        }
                    }
                    
                    receiptReceived = dictResponse["isReceiptReceived"] as? String  ??  ""
                    
                    iosPackageId = dictResponse["ios_package_id"] as? String  ??  ""

                    packageMasterId = dictResponse["package_master_id"] as? String  ??  ""

                }
             // loks comment   print(arrFeaturedNews ?? "")
                onSuccess?(result.success, arrFeaturedNews, arrOtherNews, result.message,receiptReceived,iosPackageId,packageMasterId,result.nextPageStatus)
                
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    /// This method calls 'get News Feed Details' web service.
    ///
    /// - Parameters:
    ///   - dicParam: parameter to pass
    ///   - onSuccess: success block
    ///   - onFailure: failure block
    class func callGetNewsFeedDetailsAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ newsFeedDetail: NewsFeedDetailsModel?,_ newsFeedCommnets : [NewsFeedComment]?, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.newsFeedDetails, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                var feedDetail: NewsFeedDetailsModel?
                var comments: [NewsFeedComment]?
                
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                if let dictResponse = result.data as? [String: Any], result.success {
                    
                    if let arr = dictResponse[WsParam.getNewsFeedDetail] as? [[String: Any]]{
                        for dic in arr {
                            feedDetail = NewsFeedDetailsModel(fromDictionary: dic)
                        }
                    }
                    if let arrInfo = dictResponse[WsParam.newsFeedComments] as? [[String: Any]]{
                        comments = [NewsFeedComment]()
                        for dic in arrInfo {
                            comments?.append(NewsFeedComment(fromDictionary: dic))
                        }
                    }
                    
                }
                
                onSuccess?(result.success, feedDetail, comments, result.message)
                
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    
    /// this method used to change like status.
    ///
    /// - Parameters:
    ///   - dicParam: api parameters
    ///   - onSuccess: success block
    ///   - onFailure: failure block
    class func callLikeDislikeAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ userData: [String: Any]?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.likeUnlike, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                var dicData: [String: Any]?
                let result = WsCalls.processResponse(dict: response.result.value as? [String: Any])
                if let rDict = result.data as?[String: Any]{
                    dicData = rDict
                }
                onSuccess?(result.success, result.message, dicData)
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    /// this method used to change favourite status.
    ///
    /// - Parameters:
    ///   - dicParam: api parameters
    ///   - onSuccess: success block
    ///   - onFailure: failure block
    class func callFavouriteAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ userData: [String: Any]?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.makeFavourite, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                var dicData: [String: Any]?
                let result = WsCalls.processResponse(dict: response.result.value as? [String: Any])
                if let rDict = result.data as?[String: Any]{
                    dicData = rDict
                }
                onSuccess?(result.success, result.message, dicData)
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    
    
    class func callCustomNotificationLikeCommentAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String? , _ userData: [String: Any]?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.likeUnlikeCommentCustom, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                var dicData: [String: Any]?

                let result = WsCalls.processResponse(dict: response.result.value as? [String: Any])
                if let rDict = result.data as?[String: Any]{
                    dicData = rDict
                }
                onSuccess?(result.success, result.message , dicData )
            case .failure(let error):
                onFailure?(error)
            }
        }
       
    }
    
    
    
    class func callCustomNotificationPostCommentAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ comment: CommentlistModel? ) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.likeUnlikeCommentCustom, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                
                var theComment: CommentlistModel?
                let result = WsCalls.processResponse(dict: response.result.value as? [String: Any])
                    if let rArr = result.data as? [[String: Any]], rArr.count > 0 {
                    
                    theComment = CommentlistModel(fromDictionary: [String: Any]())
                    theComment?.name = DoviesGlobalUtility.currentUser?.customerFullName ?? ""
                    theComment?.profileImage = DoviesGlobalUtility.currentUser?.customerProfileImage ?? ""
                    theComment?.comment = (dicParam?[WsParam.comment] as? String) ?? ""
                    theComment?.addedDate = "1 sec ago"
                    theComment?.notificationAgo = "1 sec ago"
                    theComment?.customerId = DoviesGlobalUtility.currentUser?.customerId ?? ""
                     theComment?.userId = DoviesGlobalUtility.currentUser?.customerId ?? ""
                    theComment?.customNotificationId = (dicParam?[WsParam.commentId] as? String) ?? ""
                    theComment?.isLike = "NO"
                    theComment?.sysRecDeleted = "0"
                        
                    theComment?.customNotificationLikeCommentId = (rArr[0][WsParam.comment_Id] as? String) ?? ""
                }
                onSuccess?(result.success, result.message , theComment )
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    
    
    
    
    class func callActiveInActiveAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ userData: [String: Any]?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.makeActiveInactive, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                var dicData: [String: Any]?
                let result = WsCalls.processResponse(dict: response.result.value as? [String: Any])
                if let rDict = result.data as?[String: Any]{
                    dicData = rDict
                }
                onSuccess?(result.success, result.message, dicData)
            case .failure(let error):
                onFailure?(error)
            }
        }
    }

    class func callExerciseLibrary(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ modelExerciseLibrary:[ModelExerciseLibrary],_ nextPageStatus : Bool) -> ())?, onFailure:((_ error: Error?) -> ())?) {
		
		var param: [String: Any]!
		if dicParam != nil{
			param = dicParam
		}
		else{
			param = [String: Any]()
		}
		
        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        {
        param["added_by_type"] = "Admin"
        }
		param[WsParam.authCustomerId] =  DoviesGlobalUtility.currentUser?.customerAuthToken
		
		WSHelper.sendRequestWithURL(wsUrl: WsUrl.exerciseLibrary, method: .get, param: param, header: nil) { (response) in
			switch response.result {
			case .success:
				var modelExerciseLibrary = [ModelExerciseLibrary]()
				
				let result = WsCalls.processResponseWithPagination(dict: response.result.value as? [String: Any])
				if let rDict = result.data as? [[String: Any]]{
					for dic in rDict{
						modelExerciseLibrary.append(ModelExerciseLibrary(fromDictionary: dic))
					}
				}
                onSuccess!(result.success, result.message, modelExerciseLibrary, result.nextPageStatus)
			case .failure(let error):
				onFailure?(error)
			}
		}
	}
	
	//MARK: - News Feed comments APIs
	
	/// This method calls 'get News Feed comments' web service.
	///
	/// - Parameters:
	///   - dicParam: parameter to pass
	///   - onSuccess: success block
	///   - onFailure: failure block
	class func callGetNewsFeedCommentsAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ comments: [NewsFeedComment]?, _ message: String?, _ nextPageStatus : Bool) -> ())?, onFailure:((_ error: Error?) -> ())?) {
		WSHelper.sendRequestWithURL(wsUrl: WsUrl.getNewsFeedComments, method: .get, param: dicParam, header: nil) { (response) in
			switch response.result {
			case .success:
				var arrComments: [NewsFeedComment]?
				
				let result = WsCalls.processResponseWithPagination(dict: response.result.value as? [String : Any])
				if let arr = result.data as? [[String: Any]], result.success {
					arrComments = [NewsFeedComment]()
					for dic in arr {
						arrComments?.append(NewsFeedComment(fromDictionary: dic))
					}
				}
				onSuccess?(result.success, arrComments, result.message, result.nextPageStatus)
				
			case .failure(let error):
				onFailure?(error)
			}
		}
	}
	
	/// This method used to post news comment.
	///
	/// - Parameters:
	///   - dicParam: api parameters
	///   - onSuccess: success block
	///   - onFailure: failure block
	class func callPostNewsCommentAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ comment: NewsFeedComment?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
		WSHelper.sendRequestWithURL(wsUrl: WsUrl.newsCommentPost, method: .post, param: dicParam, header: nil) { (response) in
			switch response.result {
			case .success:
				var theComment: NewsFeedComment?
				let result = WsCalls.processResponse(dict: response.result.value as? [String: Any])
				if let rArr = result.data as? [[String: Any]], rArr.count > 0 {
					theComment = NewsFeedComment(fromDictionary: [String: Any]())
					theComment?.customerName = DoviesGlobalUtility.currentUser?.customerFullName ?? ""
					theComment?.customerProfileImage = DoviesGlobalUtility.currentUser?.customerProfileImage ?? ""
					theComment?.newsComment = (dicParam?[WsParam.commentText] as? String) ?? ""
					theComment?.newsCommentDeleteAccess = "1"
					theComment?.newsCommentPostedDays = "1 sec ago"
					theComment?.customerId = DoviesGlobalUtility.currentUser?.customerId ?? ""
					theComment?.newsCommentsId = (rArr[0][WsParam.commentId] as? String) ?? ""
				}
				onSuccess?(result.success, result.message, theComment)
			case .failure(let error):
				onFailure?(error)
			}
		}
	}
    //MARK: - Filter Data APIs
    class func callGetModelFilterDataAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ filterData: [FilterGroup]?, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.filterData, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                var arrFilterData: [FilterGroup]?
                
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                if let arr = result.data as? [[String: Any]], result.success {
                    arrFilterData = [FilterGroup]()
              // loks comment      print(arr)
                    GlobalUtility.setPref(value: arr, key: UserDefaultsKey.filterData)
                    
                    for dic in arr{
                        arrFilterData?.append(FilterGroup(fromDictionary: dic))
                    }
                }
                onSuccess?(result.success, arrFilterData, result.message)
                
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    // Mark: - Getcustomer user list  api - mindiii
    class func callGetModelGetuserListDataAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool,_ filterData: [ModelUserlist]?,_ Doviesguest: [ModelCreatedBy]?,_ WorkoutGroup: [ModelWorkoutCollection]?, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.Getcustomerlist, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                var arrgetuserlist = [ModelUserlist]()
                var arrCreatedBy = [ModelCreatedBy]()
                var arrWorkoutGroup = [ModelWorkoutCollection]()

                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
         // loks comment       print("result = \(result)")
                
                if let dict = result.data as? [String: Any], result.success {
                   // print("dict = \(dict)")

                    if let arrDoviesGuest = dict["doviesGuest"] as? [[String : Any]]{
                        for dic in arrDoviesGuest {
                            arrCreatedBy.append(ModelCreatedBy(fromDictionary: dic))
                        }
                    }
                    
                    if let arrUsers = dict["users"] as? [[String : Any]]{
                        for dic in arrUsers {
                            arrgetuserlist.append(ModelUserlist(fromDictionary: dic))
                        }
                    }
                    
                    if let arrworkoutGroup = dict["workoutGroup"] as? [[String : Any]]{
                        for dic in arrworkoutGroup {
                            arrWorkoutGroup.append(ModelWorkoutCollection(fromDictionary: dic))
                        }
                    }
                    
                    //print("arrworkoutGroup = \(arrworkoutGroup)")
                }
                onSuccess?(result.success, arrgetuserlist,arrCreatedBy,arrWorkoutGroup, result.message)
                
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    // Mark: - Getcustomer user list  api - mindiii
    class func callGetModelGetuserListDataSearchAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool,_ filterData: [ModelUserlist]?,_ Doviesguest: [ModelCreatedBy]?,_ WorkoutGroup: [ModelWorkoutCollection]?, _ message: String?,_ hasNextPage : Bool) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.GetcustomerlistNew, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
                
            case .success:
                var arrgetuserlist = [ModelUserlist]()
                var arrCreatedBy = [ModelCreatedBy]()
                var arrWorkoutGroup = [ModelWorkoutCollection]()
                
                let result = WsCalls.processResponseWithPagination(dict: response.result.value as? [String : Any])
                
                
                if let dict = result.data as? [String: Any], result.success {
                    
                    
                    if let arrDoviesGuest = dict["doviesGuest"] as? [[String : Any]]{
                        for dic in arrDoviesGuest {
                            arrCreatedBy.append(ModelCreatedBy(fromDictionary: dic))
                        }
                    }
                    
                    if let arrUsers = dict["users"] as? [[String : Any]]{
                        for dic in arrUsers {
                            arrgetuserlist.append(ModelUserlist(fromDictionary: dic))
                        }
                    }
                   
                    if let arrworkoutGroup = dict["workoutGroup"] as? [[String : Any]]{
                        for dic in arrworkoutGroup {
                            arrWorkoutGroup.append(ModelWorkoutCollection(fromDictionary: dic))
                        }
                    }
                    
                }
                onSuccess?(result.success, arrgetuserlist,arrCreatedBy,arrWorkoutGroup, result.message , result.nextPageStatus)
                
            case .failure(let error):
                onFailure?(error)
            }
        }
    }

    
    
	
	class func callFeaturedWorkoutsAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ modelFeaturedWorkouts: [ModelFeaturedWorkouts]) -> ())?, onFailure:((_ error: Error?) -> ())?) {
		
		var param: [String: Any]!
		if dicParam != nil{
			param = dicParam
		}
		else{
			param = [String: Any]()
		}
		
		param[WsParam.authCustomerId] =  DoviesGlobalUtility.currentUser?.customerAuthToken
		
		WSHelper.sendRequestWithURL(wsUrl: WsUrl.featuredWorkouts, method: .get, param: param, header: nil) { (response) in
			switch response.result {
			case .success:
				var arrModelFeaturedWorkouts = [ModelFeaturedWorkouts]()
				let result = WsCalls.processResponse(dict: response.result.value as? [String: Any])
				if let arr = result.data as? [[String: Any]], result.success {
					for dic in arr {
						arrModelFeaturedWorkouts.append(ModelFeaturedWorkouts(fromDictionary: dic))
					}
				}
				onSuccess?(result.success, result.message, arrModelFeaturedWorkouts)
			case .failure(let error):
				onFailure?(error)
			}
		}
	}
    
    class func callFeaturedWorkoutsListAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ modelFeaturedWorkouts: [ModelWorkoutList]) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        
        var param: [String: Any]!
        if dicParam != nil{
            param = dicParam
        }
        else{
            param = [String: Any]()
        }
        
        param[WsParam.authCustomerId] =  DoviesGlobalUtility.currentUser?.customerAuthToken
        
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.featuredWorkouts, method: .get, param: param, header: nil) { (response) in
            switch response.result {
            case .success:
                var arrModelFeaturedWorkouts = [ModelWorkoutList]()
                let result = WsCalls.processResponse(dict: response.result.value as? [String: Any])
                if let arr = result.data as? [[String: Any]], result.success {
                    for dic in arr {
                        arrModelFeaturedWorkouts.append(ModelWorkoutList(fromDictionary: dic))
                    }
                }
                onSuccess?(result.success, result.message, arrModelFeaturedWorkouts)
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
	
	class func callWorkOutDetailAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ modelWorkoutDetail: ModelWorkoutDetail?) -> ())?, onFailure:((_ error: Error?) -> ())?){
		
		var param: [String: Any]!
		if dicParam != nil{
			param = dicParam
		}
		else{
			param = [String: Any]()
		}
		
		param[WsParam.authCustomerId] =  DoviesGlobalUtility.currentUser?.customerAuthToken
		WSHelper.sendRequestWithURL(wsUrl: WsUrl.workout_detail, method: .get, param: dicParam, header: nil) { (response) in
			switch response.result {
			case .success:
				var modelWorkoutDetail: ModelWorkoutDetail?
				let result = WsCalls.processResponse(dict: response.result.value as? [String: Any])
				if let dic = result.data as? [String: Any], result.success {
					
					if let detailArr = dic[WsParam.workoutDetail] as? [[String: Any]]
					{
						let dicDetail = detailArr[0]
						modelWorkoutDetail = ModelWorkoutDetail(fromDictionary: dicDetail)
                        
						if let data = dic[WsParam.workoutExerciseList] as? [[String: Any]]{
							for dicWorkoutExercise in data{
                                modelWorkoutDetail?.workoutExerciseList.append(ModelWorkoutExerciseList(fromDictionary: dicWorkoutExercise))
							}
						}
					}
				}
				
				onSuccess?(result.success, result.message, modelWorkoutDetail)
			case .failure(let error):
				onFailure?(error)
			}
		}
	}
    //MARK: - Exercise Details APIs
    class func callGetExerciseDetailsAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ filterData: [ExerciseDetails]?, _ message: String?, _ hasNextpage : Bool) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.exerciseDetail, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                var arrExeDetailData: [ExerciseDetails]?
                
                let result = WsCalls.processResponseWithPagination(dict: response.result.value as? [String : Any])
                if let arr = result.data as? [[String: Any]], result.success {
                    arrExeDetailData = [ExerciseDetails]()
                    print(arr)
                    for dic in arr{
                        arrExeDetailData?.append(ExerciseDetails(fromDictionary: dic))
                    }
                }
                onSuccess?(result.success, arrExeDetailData, result.message, result.nextPageStatus)
                
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
	
    class func callExerciseListingAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ filterData: [ExerciseDetails]?, _ message: String?,_ hasNextPage : Bool) -> ())?, onFailure:((_ error: Error?) -> ())?)
	{
		
		var param: [String: Any]!
		if dicParam != nil{
			param = dicParam
		}
		else{
			param = [String: Any]()
		}
		
		param[WsParam.authCustomerId] =  DoviesGlobalUtility.currentUser?.customerAuthToken
		
		WSHelper.sendRequestWithURL(wsUrl: WsUrl.exerciseListing, method: .get, param: param, header: nil) { (response) in
			switch response.result {
			case .success:
				var arrExeDetailData: [ExerciseDetails]?
				
				let result = WsCalls.processResponseWithPagination(dict: response.result.value as? [String : Any])
                
				if let arr = result.data as? [[String: Any]], result.success {
					arrExeDetailData = [ExerciseDetails]()
					for dic in arr{
						arrExeDetailData?.append(ExerciseDetails(fromDictionary: dic))
					}
				}
				onSuccess?(result.success, arrExeDetailData, result.message, result.nextPageStatus)
				
			case .failure(let error):
				onFailure?(error)
			}
		}
	}
	
	//MARK: - Get favorite API
	
	/// This method calls 'get Customer Favourites' web service.
	///
	/// - Parameters:
	///   - dicParam: parameter to pass
	///   - onSuccess: success block
	///   - onFailure: failure block
	class func callGetCustomerFavouritesAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ comments: ModelFavorite?, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
		
		WSHelper.sendRequestWithURL(wsUrl: WsUrl.getCustomerFavourites, method: .get, param: dicParam, header: nil) { (response) in
			switch response.result {
			case .success:
				var favoriteList: ModelFavorite?
				
				let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
				if let dic = result.data as? [[String: Any]], result.success {
					favoriteList = ModelFavorite(fromDictionary: dic, dataType: dicParam![WsParam.moduleType] as! String)
				}
				onSuccess?(result.success, favoriteList, result.message)
				
			case .failure(let error):
				onFailure?(error)
			}
		}
	}
    
    /// this method used to add program
    ///
    /// - Parameters:
    ///   - dicParam: api parameters
    ///   - onSuccess: success block
    ///   - onFailure: failure block
    class func callAddprogramAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ userData: [String: Any]?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sharedInstance.getResponseFromURL(url: WsUrl.addProgram, requestType: .post, requestParameters: dicParam, uploadProgress: { (progress) in
            print(progress.fractionCompleted)
        }) { (dicResponse, error) in
            if error == nil {
                var dicData: [String: Any]?
                let result = WsCalls.processResponse(dict: dicResponse as? [String: Any])
                if let arr = result.data as? [[String: Any]],arr.count > 0{
                    dicData = arr[0]
                }
                onSuccess?(result.success, result.message, dicData)
            } else {
                onFailure?(error)
            }
        }
    }
	
    class func callGetCustomerWorkoutsAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ arrModelMyWorkouts: [ModelWorkoutList]?, _ message: String?, _ hasNextPage : Bool) -> ())?, onFailure:((_ error: Error?) -> ())?)
	{
		
		var param: [String: Any]!
		if dicParam != nil{
			param = dicParam
		}
		else{
			param = [String: Any]()
		}
		
		param[WsParam.authCustomerId] =  DoviesGlobalUtility.currentUser?.customerAuthToken
		
		WSHelper.sendRequestWithURL(wsUrl: WsUrl.getCustomerWorkouts, method: .get, param: param, header: nil) { (response) in
			switch response.result {
			case .success:
				var arrModelMyWorkouts: [ModelWorkoutList]?
				
				let result = WsCalls.processResponseWithPagination(dict: response.result.value as? [String : Any])
				if let arr = result.data as? [[String: Any]], result.success {
					arrModelMyWorkouts = [ModelWorkoutList]()
					for dic in arr{
						arrModelMyWorkouts?.append(ModelWorkoutList(fromDictionary: dic))
					}
				}
				onSuccess?(result.success, arrModelMyWorkouts, result.message, result.nextPageStatus)
				
			case .failure(let error):
				onFailure?(error)
			}
		}
	}
    
    
    class func callGetAdminWorkoutsAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ arrModelMyWorkouts: [ModelWorkoutList]?, _ message: String?, _ hasNextPage : Bool) -> ())?, onFailure:((_ error: Error?) -> ())?)
    {
        
        var param: [String: Any]!
        if dicParam != nil{
            param = dicParam
        }
        else{
            param = [String: Any]()
        }
        
        param[WsParam.authCustomerId] =  DoviesGlobalUtility.currentUser?.customerAuthToken
        
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.Getadminworkouts, method: .get, param: param, header: nil) { (response) in
            switch response.result {
            case .success:
                var arrModelMyWorkouts: [ModelWorkoutList]?
                
                let result = WsCalls.processResponseWithPagination(dict: response.result.value as? [String : Any])
                if let arr = result.data as? [[String: Any]], result.success {
                    arrModelMyWorkouts = [ModelWorkoutList]()
                    for dic in arr{
                        arrModelMyWorkouts?.append(ModelWorkoutList(fromDictionary: dic))
                    }
                }
                onSuccess?(result.success, arrModelMyWorkouts, result.message, result.nextPageStatus)
                
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
	
    class func callGetCustomerPlanAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ arrModelMyPlan: [ModelMyPlan]?, _ message: String?,_ hasNextPage : Bool) -> ())?, onFailure:((_ error: Error?) -> ())?)
	{
		
		var param: [String: Any]!
		if dicParam != nil{
			param = dicParam
		}
		else{
			param = [String: Any]()
		}
		
		param[WsParam.authCustomerId] =  DoviesGlobalUtility.currentUser?.customerAuthToken
		
		WSHelper.sendRequestWithURL(wsUrl: WsUrl.getCustomerPlans, method: .get, param: param, header: nil) { (response) in
			switch response.result {
			case .success:
				var arrModelMyPlan: [ModelMyPlan]?
				
				let result = WsCalls.processResponseWithPagination(dict: response.result.value as? [String : Any])
				if let arr = result.data as? [[String: Any]], result.success {
					arrModelMyPlan = [ModelMyPlan]()
					for dic in arr
                    {
                      arrModelMyPlan?.append(ModelMyPlan(fromDictionary: dic))
					}
				}
				onSuccess?(result.success, arrModelMyPlan, result.message, result.nextPageStatus)
				
			case .failure(let error):
				onFailure?(error)
			}
		}
	}
    
    //MARK: - Get ProgramsDetails
    
    /// This method calls 'get Customer Favourites' web service.
    ///
    /// - Parameters:
    ///   - dicParam: parameter to pass
    ///   - onSuccess: success block
    ///   - onFailure: failure block
    class func callGetPlanDetailsAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ comments: BuildPlanModel?, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.planDetail, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                var favoriteList: BuildPlanModel?
            // loks comment    print(response)
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                if let dic = result.data as? [String: Any], result.success {
                    favoriteList = BuildPlanModel(fromDictionary: dic)
                }
                onSuccess?(result.success, favoriteList, result.message)
                
            case .failure(let error):
                onFailure?(error)
            }
        }
    }

	
	class func callContactAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ modelDoviesContact: ModelDoviesContact?, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?)
	{
		
		var param: [String: Any]!
		if dicParam != nil{
			param = dicParam
		}
		else{
			param = [String: Any]()
		}
		
		param[WsParam.authCustomerId] =  DoviesGlobalUtility.currentUser?.customerAuthToken
		
		WSHelper.sendRequestWithURL(wsUrl: WsUrl.contactDetail, method: .get, param: param, header: nil) { (response) in
			switch response.result {
			case .success:
				var modelDoviesContact: ModelDoviesContact?
				
				let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
				if let arr = result.data as? [[String: Any]], result.success {
					for dic in arr{
						modelDoviesContact = ModelDoviesContact(fromDictionary: dic)
					}
				}
				onSuccess?(result.success, modelDoviesContact, result.message)
				
			case .failure(let error):
				onFailure?(error)
			}
		}
	}
    
    
    class func callContactUsAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?)
    {
        
        var param: [String: Any]!
        if dicParam != nil{
            param = dicParam
        }
        else{
            param = [String: Any]()
        }
        
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.contactUs, method: .get, param: param, header: nil) { (response) in
            switch response.result {
            case .success:
                
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                if let arr = result.data as? [[String: Any]], result.success {
           // loks comment        print(arr)
                }
                onSuccess?(result.success, result.message)
                
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
	
	class func callStaticAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ modelStaticPage: ModelStaticPage?, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?)
	{
		
		var param: [String: Any]!
		if dicParam != nil{
			param = dicParam
		}
		else{
			param = [String: Any]()
		}
		
		param[WsParam.authCustomerId] =  DoviesGlobalUtility.currentUser?.customerAuthToken
		
		WSHelper.sendRequestWithURL(wsUrl: WsUrl.staticPage, method: .get, param: param, header: nil) { (response) in
			switch response.result {
			case .success:
				var modelStaticPage: ModelStaticPage?
				
				let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
				if let arr = result.data as? [[String: Any]], result.success {
					for dic in arr{
						modelStaticPage = ModelStaticPage(fromDictionary: dic)
					}
				}
				onSuccess?(result.success, modelStaticPage, result.message)
				
			case .failure(let error):
				onFailure?(error)
			}
		}
	}
	
    class func callCustomerWorkoutLogAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ arrModelWorkOutLogs: [ModelWorkoutList], _ message: String?,_ hasNextpage : Bool) -> ())?, onFailure:((_ error: Error?) -> ())?)
	{
		
		var param: [String: Any]!
		if dicParam != nil{
			param = dicParam
		}
		else{
			param = [String: Any]()
		}
		
		param[WsParam.authCustomerId] =  DoviesGlobalUtility.currentUser?.customerAuthToken
		
        
		WSHelper.sendRequestWithURL(wsUrl: WsUrl.customerWorkoutLog, method: .get, param: param, header: nil) { (response) in
			switch response.result {
			case .success:
				var arrModelWorkOutLogs = [ModelWorkoutList]()
				
				let result = WsCalls.processResponseWithPagination(dict: response.result.value as? [String : Any])
				if let arr = result.data as? [[String: Any]], result.success {
					for dic in arr{
						arrModelWorkOutLogs.append(ModelWorkoutList(fromDictionary: dic))
					}
				}
				onSuccess?(result.success, arrModelWorkOutLogs, result.message, result.nextPageStatus)
				
			case .failure(let error):
				onFailure?(error)
			}
		}
	}
    
    
    //MARK: - Add Diet plan to program
    
    /// This method calls 'add diet plan' web service.
    ///
    /// - Parameters:
    ///   - dicParam: parameter to pass
    ///   - onSuccess: success block
    ///   - onFailure: failure block
    class func callEditProgramDietPlanAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ comments: ProgramDietPlan?, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.editProgramDietPlan, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                var dietPlan: ProgramDietPlan?
             // loks comment   print(response)
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                if let arrDic = result.data as? [[String: Any]], result.success {
                    dietPlan = ProgramDietPlan(fromDictionary: arrDic[0])
                }
                onSuccess?(result.success, dietPlan, result.message)
                
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    /// this method used to update plan description.
    ///
    /// - Parameters:
    ///   - dicParam: api parameters
    ///   - onSuccess: success block
    ///   - onFailure: failure block
    
    class func callEditProgramDetailsAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ userData: [String: Any]?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.editProgramDetail, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                var dicData: [String: Any]?
                let result = WsCalls.processResponse(dict: response.result.value as? [String: Any])
                if let rDict = result.data as?[String: Any]{
                    dicData = rDict
                }
                onSuccess?(result.success, result.message, dicData)
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
	
	//purchaseHistory
	
	class func callpurchaseHistoryAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ arrModelPurchaseHistory: [ModelPurchaseHistory], _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?)
	{
		
		var param: [String: Any]!
		if dicParam != nil{
			param = dicParam
		}
		else{
			param = [String: Any]()
		}
		
		param[WsParam.authCustomerId] =  DoviesGlobalUtility.currentUser?.customerAuthToken
		
		WSHelper.sendRequestWithURL(wsUrl: WsUrl.purchaseHistory, method: .get, param: param, header: nil) { (response) in
			switch response.result {
			case .success:
				var arrModelPurchaseHistory = [ModelPurchaseHistory]()
				
				let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
				if let arr = result.data as? [[String: Any]], result.success {
					for dic in arr{
						arrModelPurchaseHistory.append(ModelPurchaseHistory(fromDictionary: dic))
					}
				}
				onSuccess?(result.success, arrModelPurchaseHistory, result.message)
				
			case .failure(let error):
				onFailure?(error)
			}
		}
	}
	
	class func callSubscriptionHistoryAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ arrModelSubscriptionHistory: [ModelSubscriptionHistory], _ message: String?, _ isNextPageAvailable: Bool) -> ())?, onFailure:((_ error: Error?) -> ())?)
	{
		
		var param: [String: Any]!
		if dicParam != nil{
			param = dicParam
		}
		else{
			param = [String: Any]()
		}
		
		param[WsParam.authCustomerId] =  DoviesGlobalUtility.currentUser?.customerAuthToken
		
		WSHelper.sendRequestWithURL(wsUrl: WsUrl.subscriptionHistory, method: .get, param: param, header: nil) { (response) in
			switch response.result {
			case .success:
				var arrModelSubscriptionHistory = [ModelSubscriptionHistory]()
				
				let result = WsCalls.processResponseWithPagination(dict: response.result.value as? [String : Any])
				if let arr = result.data as? [[String: Any]], result.success {
					for dic in arr{
						arrModelSubscriptionHistory.append(ModelSubscriptionHistory(fromDictionary: dic))
					}
				}
				onSuccess?(result.success, arrModelSubscriptionHistory, result.message, result.nextPageStatus)
				
			case .failure(let error):
				onFailure?(error)
			}
		}
	}
	
	class func cellDeleteWorkoutAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?){
		
		var param: [String: Any]!
		if dicParam != nil{
			param = dicParam
		}
		else{
			param = [String: Any]()
		}
		
		param[WsParam.authCustomerId] =  DoviesGlobalUtility.currentUser?.customerAuthToken
		
		WSHelper.sendRequestWithURL(wsUrl: WsUrl.deleteWorkout, method: .get, param: param, header: nil) { (response) in
			switch response.result {
			case .success:
				
				let result = WsCalls.processResponseWithPagination(dict: response.result.value as? [String : Any])
				
				onSuccess?(result.success, result.message)
				
			case .failure(let error):
				onFailure?(error)
			}
		}
	}
	
	class func cellDeleteWorkoutLogAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?){
		
		var param: [String: Any]!
		if dicParam != nil{
			param = dicParam
		}
		else{
			param = [String: Any]()
		}
		
		param[WsParam.authCustomerId] =  DoviesGlobalUtility.currentUser?.customerAuthToken
		
		WSHelper.sendRequestWithURL(wsUrl: WsUrl.deleteWorkoutLog, method: .get, param: param, header: nil) { (response) in
			switch response.result {
			case .success:
				
				let result = WsCalls.processResponseWithPagination(dict: response.result.value as? [String : Any])
				
				onSuccess?(result.success, result.message)
				
			case .failure(let error):
				onFailure?(error)
			}
		}
	}
    
    //MARK: - Add workout to program
    
    /// This method calls 'edit program workout' web service.
    ///
    /// - Parameters:
    ///   - dicParam: parameter to pass
    ///   - onSuccess: success block
    ///   - onFailure: failure block
    class func callEditProgramWorkoutAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ workoutDay: WorkoutDayModel?, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.editProgramWorkout, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                var workout: WorkoutDayModel?
                print(response)
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                if let arrDic = result.data as? [[String: Any]], result.success {
                    workout = WorkoutDayModel(fromDictionary: arrDic[0])
                }
                onSuccess?(result.success, workout, result.message)
                
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
	
    //MARK: - Update program workout status
    
    /// This method calls 'update program workout status' web service.
    ///
    /// - Parameters:
    ///   - dicParam: parameter to pass
    ///   - onSuccess: success block
    ///   - onFailure: failure block
    class func callUpdateProgramWorkoutStatusAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ response: [String : Any]?, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.updateProgramWorkoutStatus, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                print(response)
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                onSuccess?(result.success, result.data as? [String : Any], result.message)
                
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    //MARK: - Delete Plan
    
    /// This method calls 'Delete Plan' web service.
    ///
    /// - Parameters:
    ///   - dicParam: parameter to pass
    ///   - onSuccess: success block
    ///   - onFailure: failure block
    class func callDeletePlanAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ response: [String : Any]?, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.deletePlan, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                print(response)
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                onSuccess?(result.success, result.data as? [String : Any], result.message)
                
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    
    class func callGetCustomerDiet(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ arrDietPlan: [DietPlan], _ message: String?, _ hasNextPage : Bool) -> ())?, onFailure:((_ error: Error?) -> ())?){
		
		var param: [String: Any]!
		if dicParam != nil{
			param = dicParam
		}
		else{
			param = [String: Any]()
		}
		
		param[WsParam.authCustomerId] =  DoviesGlobalUtility.currentUser?.customerAuthToken
		
		WSHelper.sendRequestWithURL(wsUrl: WsUrl.getCustomerDiet, method: .get, param: param, header: nil) { (response) in
			switch response.result {
			case .success:
				
				var arrDietPlan = [DietPlan]()
				
				let result = WsCalls.processResponseWithPagination(dict: response.result.value as? [String : Any])
				if let arr = result.data as? [[String: Any]], result.success {
					for dic in arr{
						arrDietPlan.append(DietPlan(fromDictionary: dic))
					}
				}
				onSuccess?(result.success, arrDietPlan, result.message, result.nextPageStatus)
				
			case .failure(let error):
				onFailure?(error)
			}
		}
	}

    //MARK: - Add Diet plan to my diet plans list
    
    /// This method calls 'add_to_my_diet' web service.
    ///
    /// - Parameters:
    ///   - dicParam: parameter to pass
    ///   - onSuccess: success block
    ///   - onFailure: failure block
    class func callAddToMyDietPlanAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ response: [String : Any]?, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.addToMyDiet, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                print(response)
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                onSuccess?(result.success, result.data as? [String : Any], result.message)
                
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    //MARK: - Add plan to my plans list
    
    /// This method calls 'add_to_my_plan' web service.
    ///
    /// - Parameters:
    ///   - dicParam: parameter to pass
    ///   - onSuccess: success block
    ///   - onFailure: failure block
    class func callAddToMyPlanAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ response: [String : Any]?, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.addToMyPlan, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                print(response)
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                onSuccess?(result.success, result.data as? [String : Any], result.message)
                
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    //MARK: - Add workout feedback
    
    /// This method calls 'add_workout_feedback' web service.
    ///
    /// - Parameters:
    ///   - dicParam: parameter to pass
    ///   - onSuccess: success block
    ///   - onFailure: failure block
    class func callAddWorkoutFeedbackAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ response: [String : Any]?, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.addWorkoutFeedback, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                print(response)
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                onSuccess?(result.success, result.data as? [String : Any], result.message)
                
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    class func callAddWorkoutFeedbackPostAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ response: [String : Any]?, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        
        WSHelper.sharedInstance.postMultipartDataWithImageArray(url: WsUrl.addWorkoutFeedback, requestType: .post, requestParameters: dicParam, uploadProgress: { (progress) in
            print(progress.fractionCompleted)
        }) { (dicResponse, error) in
            if error == nil {
                var dicData: [String: Any]?
                let result = WsCalls.processResponse(dict: dicResponse as? [String: Any])
                if let arr = result.data as? [[String: Any]],arr.count > 0{
                    dicData = arr[0]
                }
                
                onSuccess?(result.success, dicData, result.message)
            } else {
                onFailure?(error)
            }
        }
    }
    
    //MARK: - Exercise Details APIs
    class func callGetNotificationsAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ notificationList: [NotificationModel]?, _ message: String?,_ hasNextpage : Bool) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.notificationList, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                var arrNotifications: [NotificationModel]?
                
                let result = WsCalls.processResponseWithPagination(dict: response.result.value as? [String : Any])
                if let arr = result.data as? [[String: Any]], result.success {
                    arrNotifications = [NotificationModel]()
                    print(arr)
                    for dic in arr{
                    arrNotifications?.append(NotificationModel(fromDictionary: dic))
                    }
                }
                onSuccess?(result.success, arrNotifications, result.message,result.nextPageStatus)
                
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
	
	//MARK: - PaymentList
    class func callGetPackageListAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ packageList: ModelUpgradePremium?, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
		WSHelper.sendRequestWithURL(wsUrl: WsUrl.packageList, method: .get, param: dicParam, header: nil) { (response) in
			switch response.result {
			case .success:
                var arrlUpgradePremium: ModelUpgradePremium!
				
				let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
				if let arr = result.data as? [String: Any], result.success {
					print(arr)
                   // let arGetAllPackages = ([[result.data! as Any][0] as Any][0] as! [String:Any])["get_all_packages"] as! Any
                     arrlUpgradePremium = (ModelUpgradePremium(fromDictionary: (result.data as! [String : Any])))
//                    let ar1 = arr["get_all_packages"] as! [[String:Any]]
//
////                    arrlUpgradePremium?.append(ModelUpgradePremium(fromDictionary: dic))
//                    for dic in ar1{
//                        arrlUpgradePremium?.append(ModelUpgradePremium(fromDictionary: dic))
//                    }
//                    let ar2 = arr["get_static_content"] as! [[String:Any]]
//                    for dic in ar2{
//                        arrlUpgradePremium?.append(ModelUpgradePremium(fromDictionary: dic))
//                    }
				}
				onSuccess?(result.success, arrlUpgradePremium, result.message)
				
			case .failure(let error):
				onFailure?(error)
			}
		}
	}
	
	class func callCompletePaymentAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
		WSHelper.sendRequestWithURL(wsUrl: WsUrl.completePayment, method: .post, param: dicParam, header: nil) { (response) in
			switch response.result {
			case .success:
				let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
				onSuccess?(result.success, result.message)
			case .failure(let error):
				onFailure?(error)
			}
		}
	}
    
    
    //MARK: - Get Subscription status
    
    /// This method calls 'subscription_status' web service.
    ///
    /// - Parameters:
    ///   - dicParam: parameter to pass
    ///   - onSuccess: success block
    ///   - onFailure: failure block
    
    class func callSubscriptionStatusAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ response: [String : Any]?, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.subscriptionStatus, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                print(response)
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                onSuccess?(result.success, result.data as? [String : Any], result.message)
                
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    class func callProductPurchaseSuccessAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.productPurchaseSuccess, method: .post, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                onSuccess?(result.success, result.message)
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    class func callDeleteCommentAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.deleteComment, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                onSuccess?(result.success, result.message)
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    
    class func callDeleteNotificationCommentAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.deleteNotificationComment, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                onSuccess?(result.success, result.message)
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    class func callDeleteNotificationAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.deleteNotification, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                onSuccess?(result.success, result.message)
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    
    class func callReportCommentAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.reportComment, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                onSuccess?(result.success, result.message)
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    class func callReportNotificationCommentAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.reportCustomComment, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                onSuccess?(result.success, result.message)
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    class func callDeleteDietPlanAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.deleteDietPlan, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                onSuccess?(result.success, result.message)
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    //MARK: - Exercise Details APIs
    class func callGetFaqsAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ faqList: [FaqModel]?, _ message: String?,_ hasNextpage : Bool) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.getFaq, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                var arrFaq: [FaqModel]?
                
                let result = WsCalls.processResponseWithPagination(dict: response.result.value as? [String : Any])
                if let arr = result.data as? [[String: Any]], result.success {
                    arrFaq = [FaqModel]()
                    print(arr)
                    for dic in arr{
                        arrFaq?.append(FaqModel(fromDictionary: dic))
                    }
                }
                onSuccess?(result.success, arrFaq, result.message,result.nextPageStatus)
                
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    class func callUpdateUserUnitsAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ response: [[String : Any]]?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.editCustomerUnits, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                onSuccess?(result.success, result.message, result.data as? [[String : Any]])
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    class func callAddToMyWorkoutsAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.addToMyWorkout, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
                onSuccess?(result.success, result.message)
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    class func callUpdateUserCountryAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ response: [String : Any]?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.updateCustomerCountry, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                let result = WsCalls.processResponse(dict: response.result.value as? [String : Any])
				
                if let dataArr = result.data as? [[String : Any]]{
					if result.success == true{
                    	onSuccess?(result.success, result.message, dataArr[0])
					}
					else{
						onSuccess?(result.success, result.message, result.data as? [String : Any])
					}
                }else{
                    onSuccess?(result.success, result.message, result.data as? [String : Any])
                }
                
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
    
    class func callCreatePlanAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ message: String?, _ userData: [String: Any]?) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sharedInstance.getResponseFromURL(url: WsUrl.addCompleteProgram, requestType: .post, requestParameters: dicParam, uploadProgress: { (progress) in
            print(progress.fractionCompleted)
        }) { (dicResponse, error) in
            if error == nil {
                var dicData: [String: Any]?
                let result = WsCalls.processResponse(dict: dicResponse as? [String: Any])
                if let arr = result.data as? [[String: Any]],arr.count > 0{
                    dicData = arr[0]
                }
                onSuccess?(result.success, result.message, dicData)
            } else {
                onFailure?(error)
            }
        }
    }
    
    
    
    class func callGroupDetailsAPI(dicParam: [String : Any]?, onSuccess:((_ success: Bool, _ workoutcount: String?, _ message: String?, _ modelFeaturedWorkouts: [GroupDetailsModel]) -> ())?, onFailure:((_ error: Error?) -> ())?) {
        WSHelper.sendRequestWithURL(wsUrl: WsUrl.groupWorkoutList, method: .get, param: dicParam, header: nil) { (response) in
            switch response.result {
            case .success:
                var arrModelFeaturedWorkouts = [GroupDetailsModel]()
                let result = WsCalls.ProcessResponse(dict: response.result.value as? [String: Any])
                
                if let arr = result.data as? [[String: Any]], result.success {
                    for dic in arr {
                        arrModelFeaturedWorkouts.append(GroupDetailsModel(fromDictionary: dic))
                    }
                }
                
                onSuccess?(result.success ,result.workoutcount , result.message, arrModelFeaturedWorkouts)
                
            case .failure(let error):
                onFailure?(error)
            }
        }
    }
    
}




