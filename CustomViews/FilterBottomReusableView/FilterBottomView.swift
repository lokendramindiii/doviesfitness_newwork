//
//  FilterBottomView.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

class FilterBottomView: UICollectionReusableView {
    @IBOutlet var btnApply: UIButton!
    var filterTapped:(()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    /// This method call when filter button tapped
    ///
    /// - Parameter sender: Filter button
    @IBAction private func filterButtonTapped(sender : UIButton){
        filterTapped?()
        
    }
    
}
