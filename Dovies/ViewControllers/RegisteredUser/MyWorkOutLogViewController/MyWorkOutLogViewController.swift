//
//  MyWorkOutLogViewController.swift
//  Dovies
//
//  Created by Neel Shah on 16/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class MyWorkOutLogViewController: BaseViewController {
	
	@IBOutlet weak var IBtblWorkOutLog: UITableView!
	
    @IBOutlet weak var IBViewEmptyWorkoutLog : UIView!
    
	var arrModelWorkOutLogs = [ModelWorkoutList]()
    
    var cellCount = 0
    
    // loks cell count 10 remove for grey place holder
	//var cellCount = 10
    var isFromMYworkoutlog = true
    
//    var arrImages = [UIImage(named: "2ND_PAGE_OF_APP")!,UIImage(named: "FIRST_PAGE_DESIGN_FOR_APP")!,UIImage(named: "profile_placeHolder")!]

    var currentPageIndex = 1
    var shouldLoadMore = true {
        didSet {
            if shouldLoadMore == false {
                IBtblWorkOutLog.tableFooterView = UIView()
            } else {
                IBtblWorkOutLog.tableFooterView = DoviesGlobalUtility.loadingFooter()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: ViewLifeCycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
		self.navigationItem.title = "My Workout Log".uppercased()
		super.addNavigationWhiteBackButton()
		
		IBtblWorkOutLog.register(UINib(nibName: "MyWorkoutLogTableCell", bundle: nil), forCellReuseIdentifier: "MyWorkoutLogTableCell")
		IBtblWorkOutLog.separatorColor = UIColor.colorWithRGB(r: 230, g: 230, b: 230)
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewDidLoad()
		if arrModelWorkOutLogs.count == 0{
			DispatchQueue.main.async { [weak self] in
				guard let weakSelf = self else{return}
				weakSelf.IBtblWorkOutLog.reloadData()
				weakSelf.IBtblWorkOutLog.layoutIfNeeded()
				weakSelf.IBtblWorkOutLog.showLoader()
			}
			self.callWorkoutLogAPI()
		}
	}
	
    //MARK: Instance methods
    
    override func btnBackTapped(sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK: IBAction methods
    
    @objc func buttonDetailTap(sender: UIButton) {
     
        let detailsVC = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "WorkOutExerciseDescription_Detail")as! WorkOutExerciseDescription_Detail
        detailsVC.workoutLogModel = arrModelWorkOutLogs[sender.tag]
        detailsVC.modalTransitionStyle = .crossDissolve
        detailsVC.modalPresentationStyle = .overCurrentContext
        self.present(detailsVC, animated: true, completion: nil)
    }
    
    
    @objc func buttonViewTap(sender: UIButton) {
        let objModel = self.arrModelWorkOutLogs[sender.tag]
        let arrImages = objModel.arrWorrkoutLogImages
        guard arrImages.count>0 else {return}
        let VC = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "GalleryViewController")as! GalleryViewController
        //detailsVC.workoutLogModel = arrModelWorkOutLogs[sender.tag]
        print(arrImages)
        
        VC.arrLogImageModel = arrImages
        
        VC.modalTransitionStyle = .crossDissolve
        VC.modalPresentationStyle = .overCurrentContext
        if let tabbar = self.tabBarController{
           tabbar.present(VC, animated: true, completion: nil)
        }
        else{
            self.navigationController?.present(VC, animated: true, completion: nil)
        }
    }
    
    //MARK: WebService methods
    
	func callWorkoutLogAPI() {
        DoviesWSCalls.callCustomerWorkoutLogAPI(dicParam: [WsParam.pageIndex : "\(currentPageIndex)"], onSuccess: { (success, arrModelWorkOut, msg, hasNextPage) in
			if success{
                if self.currentPageIndex == 1{
                    self.arrModelWorkOutLogs = arrModelWorkOut
                }else {
                    self.arrModelWorkOutLogs += arrModelWorkOut
                }
                
			}
			self.shouldLoadMore = hasNextPage
            self.cellCount = 0
            
            if hasNextPage{
                self.currentPageIndex += 1
            }
            self.IBViewEmptyWorkoutLog.isHidden = self.arrModelWorkOutLogs.count > 0
			self.IBtblWorkOutLog.hideLoader()
			self.IBtblWorkOutLog.reloadData()
		}) { (error) in
			
		}
	}
}

extension MyWorkOutLogViewController: UITableViewDataSource{
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return arrModelWorkOutLogs.count > 0 ? arrModelWorkOutLogs.count : cellCount
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "MyWorkoutLogTableCell") as? MyWorkoutLogTableCell
		cell?.separatorInset = UIEdgeInsets.zero
        
        cell?.IBbtnDetail.tag = indexPath.row
        cell?.IBbtnDetail.addTarget(self, action: #selector(self.buttonDetailTap(sender:)), for: .touchUpInside)
        
		if arrModelWorkOutLogs.count > 0{
			cell?.setWorkOutLogData(modelWorkOutLogs: arrModelWorkOutLogs[indexPath.row])
		}
		cell?.IBbtnDetail.tag = indexPath.row
		cell?.IBbtnDetail.addTarget(self, action: #selector(buttonDetailTap(sender:)), for: .touchUpInside)
        
        cell?.IBbtnViewImages.tag = indexPath.row
        cell?.IBbtnViewImages.addTarget(self, action: #selector(buttonViewTap(sender:)), for: .touchUpInside)
		return cell!
	}
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (arrModelWorkOutLogs.count - 1) && shouldLoadMore == true {
            callWorkoutLogAPI()
        }
    }
	
	func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
		return true
	}
	
	func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
		if (editingStyle == UITableViewCellEditingStyle.delete) {
			UIAlertController.showAlert(in: self, withTitle: AlertTitle.appName, message: AlertMessages.deleteWorkoutLog, cancelButtonTitle: "No", destructiveButtonTitle: "Delete", otherButtonTitles: nil) { (alertControl, action, index) in
				if index == 1
				{
                    let workoutlogId = self.arrModelWorkOutLogs[indexPath.row].workoutLogId ?? ""
					DoviesWSCalls.cellDeleteWorkoutLogAPI(dicParam: ["workout_log_id": workoutlogId], onSuccess: { (success, msg) in
						if success{
							self.arrModelWorkOutLogs.remove(at: indexPath.row)
							self.IBtblWorkOutLog.reloadData()
							self.IBViewEmptyWorkoutLog.isHidden = self.arrModelWorkOutLogs.count > 0
						}
					}, onFailure: { (error) in
						
					})
				}
			}
		}
	}
}

extension MyWorkOutLogViewController: UITableViewDelegate{
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if arrModelWorkOutLogs.count > 0{
			let featuredWorkOutDetailViewControlle = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "FeaturedWorkOutDetailViewController") as! FeaturedWorkOutDetailViewController
			featuredWorkOutDetailViewControlle.navigationItem.title = arrModelWorkOutLogs[indexPath.row].workoutName
			featuredWorkOutDetailViewControlle.modelWorkoutList = arrModelWorkOutLogs[indexPath.row]
            featuredWorkOutDetailViewControlle.isFromMyworkoutlog = self.isFromMYworkoutlog
	self.navigationController?.pushViewController(featuredWorkOutDetailViewControlle, animated: true)
		}
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 85
	}
	
	func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		return 0.1
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 0.1
	}
}
