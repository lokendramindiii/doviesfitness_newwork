//
//  NewsFeedHorizontalCollectionCell.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

class NewsFeedHorizontalCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imgBackground : UIImageView!
    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var btnLock : UIButton!
    @IBOutlet weak var btnNew : UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        let path = UIBezierPath(roundedRect:btnNew.bounds,
                                byRoundingCorners:[.topLeft,.bottomRight],
                                cornerRadii: CGSize(width: 15.0, height: 15.0))

        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        btnNew.layer.mask = maskLayer
        
        let gradientView = UIView(frame: CGRect(x: 0, y: (NewsFeedViewController.cellWidth)/2, width: NewsFeedViewController.cellWidth, height: (NewsFeedViewController.cellWidth)/2))
        let gradientLayer:CAGradientLayer = CAGradientLayer()
        gradientLayer.frame.size =  gradientView.frame.size
        gradientLayer.colors = [UIColor.clear.cgColor,UIColor.appBlackColorNew().withAlphaComponent(0.7).cgColor]
        gradientView.layer.addSublayer(gradientLayer)
        self.imgBackground.addSubview(gradientView)

    }
 
    func setNewsFeedCellUI(newsFeed:NewsFeedModel){
        //lblTitle.text = newsFeed.newsTitle
        let attributedString = NSMutableAttributedString(string: newsFeed.newsTitle)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5.0
        paragraphStyle.alignment = .center
        
      attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
        
        lblTitle.attributedText = attributedString
        btnNew.isHidden = !newsFeed.isNew

       // imgBackground.sd_setImage(with:  URL(string: newsFeed.newsImage), placeholderImage: AppInfo.placeholderImage)
        
          imgBackground.sd_setImage(with:  URL(string: newsFeed.newsImage), placeholderImage: nil)
        
        btnLock.isHidden = (newsFeed.workoutAccessLevel.uppercased() == "OPEN".uppercased() || newsFeed.workoutAccessLevel.isEmpty) || DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
    }
    
}

