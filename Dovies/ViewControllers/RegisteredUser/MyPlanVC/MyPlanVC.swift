//
//  MyPlanVC.swift
//  
//
//  Created by hb on 16/03/18.
//

import UIKit

class MyPlanVC: BaseViewController {
 
    @IBOutlet var AddPlanView: UIView!
  //  @IBOutlet var myPlanCollView: BaseCollectionView!
    @IBOutlet var myPlanTableview: UITableView!

    static let cellIndentifier = "DietPlanCollectionCell"
    static let cellPadding : CGFloat = 5.0
 //    let cellSizeValue : CGFloat  = (ScreenSize.width - 1)/2
    @IBOutlet var btnAddPlan: UIButton!
    @IBOutlet var IBviewAddPlan: UIView!
    var loaderCellsCount = 16
    var arrModelMyPlan = [ModelMyPlan]()

    var pullToRefreshCtrl:UIRefreshControl!
    var shouldLoadMore = false
    var Ispulltorefresh = false

    var isReload = false
    var currentPageIndex = 1

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
        (self.navigationController?.navigationBar.frame.height ?? 0.0)
        
        self.myPlanTableview.contentInset = UIEdgeInsets(top: topBarHeight, left: 0, bottom: self.tabBarController!.tabBar.frame.height, right: 0)

        self.navigationItem.title = "My Plan".uppercased()
        self.navigationItem.hidesBackButton  = true
        self.addNavigationWhiteBackButton()

        self.inititalUISetUp()
        // Do any additional setup after loading the view.
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let topBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        self.myPlanTableview.contentInset = UIEdgeInsets(top: topBarHeight, left: 0, bottom: self.tabBarController!.tabBar.frame.height, right: 0)

        Ispulltorefresh = false
        if arrModelMyPlan.count == 0 || isReload{
            DispatchQueue.main.async { [weak self] in
                guard let weakSelf = self else{return}
                weakSelf.myPlanTableview.reloadData()
                weakSelf.myPlanTableview.layoutIfNeeded()
                 weakSelf.myPlanTableview.showLoaderBlack()
            }
            currentPageIndex = 1
            self.callMyPlanAPI()
        }
        
      //  self.navigationController?.navigationBar.barTintColor = UIColor.appNavColor()
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,
                                                                        NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //UIApplication.shared.statusBarStyle = .default
        
        //  self.navigationController?.navigationBar.barTintColor = UIColor.white
        //        self.navigationController?.navigationBar.titleTextAttributes = [
        //            NSAttributedStringKey.foregroundColor : UIColor.colorWithRGB(r: 34.0, g: 34.0, b: 34.0),
        //            NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]

}
    
    func showSubscriptionScreen(){
        if let upgradeVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "UpgradeToPremiumViewController") as? UpgradeToPremiumViewController{
            upgradeVC.onPurchaseSuccess = {
                //self.callWorkOutDetailAPI()
            }
            upgradeVC.isPresent = true
            
            let subscriptionNav = UINavigationController(rootViewController: upgradeVC)
            
            self.present(subscriptionNav, animated: true, completion: nil)
        }
    }
    
    override func btnBackTapped(sender: UIButton) {
        var isFromProgramDetails = false
        if let vcList = self.navigationController?.viewControllers{
            for vc in vcList{
                if vc is ProgramDetail_VC{
                    isFromProgramDetails = true
                }
            }
        }
        if isFromProgramDetails{
            self.redirectToTab(tabIndex: 4)
            self.navigationController?.popToRootViewController(animated: false)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        //myPlanCollView.collectionViewLayout.invalidateLayout()
    }
    
    override func inititalUISetUp() {
        //myPlanCollView.register(UINib(nibName: MyPlanVC.cellIndentifier, bundle: nil), forCellWithReuseIdentifier: MyPlanVC.cellIndentifier)
        
        myPlanTableview.backgroundColor = UIColor.appBlackColorNew()

        myPlanTableview.register(UINib(nibName: "DietPlanCell", bundle: nil), forCellReuseIdentifier: "DietPlanCell")
       // myPlanTableview.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        setPullToRefresh()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        

    }

    func setPullToRefresh(){
        pullToRefreshCtrl = UIRefreshControl()
        pullToRefreshCtrl.addTarget(self, action: #selector(self.pullToRefreshClick(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            myPlanTableview.refreshControl  = pullToRefreshCtrl
        } else {
            myPlanTableview.addSubview(pullToRefreshCtrl)
        }
    }
    
    @objc func pullToRefreshClick(sender:UIRefreshControl) {
        sender.endRefreshing()
        if Ispulltorefresh == false
        {
            currentPageIndex = 1
            shouldLoadMore = false
            callMyPlanAPI()
            sender.endRefreshing()
        }
    }
    
    func addRightNavigationButton(){
        let addButton = UIButton(type: .custom)
       // addButton.setImage(UIImage(named:"pluce_icon_s26_h"), for: .normal)
        addButton.setImage(UIImage(named:"pluce_icon_s26_white"), for: .normal)

        addButton.addTarget(self, action: #selector(self.btnAddCash(_:)), for: .touchUpInside)
        addButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        let addBarItem = UIBarButtonItem(customView: addButton)
        navigationItem.rightBarButtonItems = [addBarItem]
    }
    
    @IBAction func btnAddCash(_ sender: Any) {
        let PlanOverview = self.storyboard?.instantiateViewController(withIdentifier: "PlanOverviewVC")as! PlanOverviewVC
        self.navigationController?.pushViewController(PlanOverview, animated: true)
    }
    
    //MARK: WebService methods
    
    /**
     This method used to fetch current user plans and update the collection view
     */
    func callMyPlanAPI(){
        self.Ispulltorefresh = true
        DoviesWSCalls.callGetCustomerPlanAPI(dicParam: [WsParam.pageIndex : "\(currentPageIndex)"], onSuccess: { (success, addModelPlan, msg, hasNextPage) in
            self.shouldLoadMore = hasNextPage
           self.loaderCellsCount = 0
            if success{
                if self.currentPageIndex == 1
                {
                    self.arrModelMyPlan = addModelPlan!
                    
                }else{
                    self.arrModelMyPlan += addModelPlan!
                }
                if hasNextPage{
                    self.currentPageIndex += 1
                }
            }
            if self.arrModelMyPlan.count > 0 {
                self.IBviewAddPlan.isHidden = true
                self.addRightNavigationButton()
            }
            else{
                self.IBviewAddPlan.isHidden = false
            }
            DispatchQueue.main.async {
                self.myPlanTableview.hideLoader()
                self.myPlanTableview.reloadData()
                self.myPlanTableview.layoutIfNeeded()
            }
            self.Ispulltorefresh = false

        }) { (error) in
            self.Ispulltorefresh = false
        }
    }
  
    
    /**
     This method used to add program to favourite list
     */
    func callFavouriteAPI(with plan : ModelMyPlan){
        guard let cUser = DoviesGlobalUtility.currentUser else{return}
        var parameters = [String : Any]()
        parameters[WsParam.authCustomerId] =  cUser.customerAuthToken
        parameters[WsParam.moduleId] = plan.programId
        parameters[WsParam.moduleName] = ModuleName.program
        
        DoviesWSCalls.callFavouriteAPI(dicParam: parameters, onSuccess: {(success, message, response) in
            if !success{
                plan.programFavStatus = !plan.programFavStatus
            }
        }) { (error) in
            plan.programFavStatus = !plan.programFavStatus
        }
    }
    
    func callActiveInActiveAPI(with plan : ModelMyPlan){
        guard let cUser = DoviesGlobalUtility.currentUser else{return}
        var parameters = [String : Any]()
        parameters[WsParam.authCustomerId] =  cUser.customerAuthToken
        parameters[WsParam.programId] = plan.programId
        DoviesWSCalls.callActiveInActiveAPI(dicParam: parameters, onSuccess: {(success, message, response) in
            if !success{
                plan.programActive = !plan.programActive
            }
        }) { (error) in
             plan.programActive = !plan.programActive
        }
    }
}



extension MyPlanVC : UITableViewDelegate,UITableViewDataSource
{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrModelMyPlan.count > 0 ? arrModelMyPlan.count : loaderCellsCount
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        //  return ScreenSize.width
        return ScreenSize.width - 40
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DietPlanCell") as? DietPlanCell else
        {return UITableViewCell() }
        cell.btnMore.isHidden = false
        if arrModelMyPlan.count > 0{
            cell.setMyPlanUI(modelMyPlan: arrModelMyPlan[indexPath.row])
            
            for subview in cell.imgBackground.subviews {
                subview.removeFromSuperview()
            }
            
            let viewGradient =  UIView(frame: CGRect(x: 0, y: (ScreenSize.width - 40)/2, width:ScreenSize.width - 40, height: (ScreenSize.width - 40)/2))
            let gradientLayer:CAGradientLayer = CAGradientLayer()
            gradientLayer.frame.size =  viewGradient.frame.size
            gradientLayer.colors = [UIColor.clear.cgColor,UIColor.appBlackColorNew().withAlphaComponent(0.5).cgColor]
            viewGradient.layer.addSublayer(gradientLayer)
            cell.imgBackground.addSubview(viewGradient)


            cell.moreButtonMyPlanAction = { (myPlan) in
                if let plan = myPlan{
                    if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
                    {
                        self.showActionSheetAtIndex_admin(plan : plan )
                    }
                    else
                    {
                        self.showActionSheetAtIndex(plan : plan )
                    }
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        if indexPath.item == (arrModelMyPlan.count - 1) && shouldLoadMore == true {
            shouldLoadMore = false
            callMyPlanAPI()
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if arrModelMyPlan.count > 0{
            let model : ModelMyPlan = self.arrModelMyPlan[indexPath.item]
            if model.programAccessLevel.uppercased() == "OPEN".uppercased(){
                let buildPlanVC = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "BuildPlanViewController") as! BuildPlanViewController
                buildPlanVC.updateOnDeletePlan = {[weak self] (palnId) in
                    guard let weakSelf = self else{return}
                    if let plan = weakSelf.arrModelMyPlan.first(where: { $0.programId == palnId }) , let delIndex = weakSelf.arrModelMyPlan.index(of: plan){
                        weakSelf.arrModelMyPlan.remove(at: delIndex)
                    }
                    weakSelf.myPlanTableview.reloadData()
                    
                }
                buildPlanVC.updateOnFavStateChange = {[weak self] (palnId, status) in
                    guard let weakSelf = self else{return}
                    if let plan = weakSelf.arrModelMyPlan.first(where: { $0.programId == palnId }){
                        plan.programFavStatus = status
                    }
                    weakSelf.myPlanTableview.reloadData()
                }
                buildPlanVC.planId = arrModelMyPlan[indexPath.item].programId
                buildPlanVC.planScreenType = .planView
                BuildPlanViewController.isDataChanged = false
                buildPlanVC.isFromEditAdmin = true
                self.navigationController?.pushViewController(buildPlanVC, animated: true)
            }else{
                if let upgradeVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "UpgradeToPremiumViewController") as? UpgradeToPremiumViewController{
                    upgradeVC.onPurchaseSuccess = {
                        self.callMyPlanAPI()
                    }
                    self.navigationController?.pushViewController(upgradeVC, animated: true)
                }
            }
        }
    }
    
    
    func showActionSheetAtIndex(plan : ModelMyPlan){
        let arrData: [alertActionData]!
        if plan.programAccessLevel.uppercased() == "OPEN".uppercased(){
            arrData = [
                alertActionData(title: "Edit", imageName: "btn_edit_s21a", highlighedImageName : "btn_edit_s21a"),
                alertActionData(title: "Share", imageName: "btn_share", highlighedImageName : "btn_share"),
                alertActionData(title: plan.programFavStatus == false ? "Favourite" : "Unfavourite", imageName: plan.programFavStatus == false ? "btn_fav_s21a" : "favorite_icon_s34_h", highlighedImageName : "favorite_icon_s34_h"),
                alertActionData(title: "Delete", imageName: "delete_btn_s27b_h", highlighedImageName : "delete_btn_s27b_h"),
                
            ]
            
            DoviesGlobalUtility.showDefaultActionSheet(on:self,actionData: arrData,cancelTitle: "Cancel") { (clickedIndex:Int, isCancel:Bool) in
                switch clickedIndex{
                case 0:
                    let buildPlanVC = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "BuildPlanViewController") as! BuildPlanViewController
                    buildPlanVC.updateOnDeletePlan = {[weak self] (palnId) in
                        guard let weakSelf = self else{return}
                        if let plan = weakSelf.arrModelMyPlan.first(where: { $0.programId == palnId }) , let delIndex = weakSelf.arrModelMyPlan.index(of: plan){
                            weakSelf.arrModelMyPlan.remove(at: delIndex)
                        }
                        weakSelf.myPlanTableview.reloadData()
                    }
                    buildPlanVC.updateOnFavStateChange = {[weak self] (palnId, status) in
                        guard let weakSelf = self else{return}
                        if let plan = weakSelf.arrModelMyPlan.first(where: { $0.programId == palnId }){
                            plan.programFavStatus = status
                        }
                        weakSelf.myPlanTableview.reloadData()
                    }
                    
                    buildPlanVC.planId = plan.programId
                    buildPlanVC.planScreenType = .planEdit
                    
                    if DoviesGlobalUtility.isSubscribed == true || DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName{
                        
                        BuildPlanViewController.isDataChanged = false
                        self.navigationController?.pushViewController(buildPlanVC, animated: true)
                        
                    }else{
                        
                        self.showSubscriptionScreen()
                    }
                    
                case 1:
                    //                    DoviesGlobalUtility.generateBranchIoUrl(with: ModuleName.program, moduleId: plan.programId, completionHandler: { (urlString) in
                    //                        DoviesGlobalUtility.showShareSheet(on: self, shareText: urlString)
                    //                    })
                    
                    DoviesGlobalUtility.showShareSheet(on: self, shareText: plan.programShareUrl)
                    
                case 2:
                    plan.programFavStatus = !plan.programFavStatus
                    self.callFavouriteAPI(with: plan)
                    
                case 3:
                    self.deletePlanAction(of: plan)
                    
                default:
                    break
                }
                
            }
        }
        else{
            arrData = [
                alertActionData(title: "Delete", imageName: "delete_btn_s27b_h", highlighedImageName : "delete_btn_s27b_h")
            ]
            DoviesGlobalUtility.showDefaultActionSheet(on:self,actionData: arrData,cancelTitle: "Cancel") { (clickedIndex:Int, isCancel:Bool) in
                switch clickedIndex{
                case 0:
                    self.deletePlanAction(of: plan)
                default:
                    break
                }
            }
        }
    }
    
    
    func showActionSheetAtIndex_admin(plan : ModelMyPlan){
        let arrData: [alertActionData]!
        arrData = [
            alertActionData(title: "Edit", imageName: "btn_edit_s21a", highlighedImageName : "btn_edit_s21a"),
            alertActionData(title: "Publish to app", imageName: "icon_publish", highlighedImageName : "icon_publish"),
            alertActionData(title: plan.programActive == false ? "Active" : "Active", imageName: plan.programActive == false ? "icon_Inactive" : "ico_active", highlighedImageName : "ico_active"),
            alertActionData(title: "Delete", imageName: "delete_btn_s27b_h", highlighedImageName : "delete_btn_s27b_h"),
            
        ]
        
        DoviesGlobalUtility.showDefaultActionSheet(on:self,actionData: arrData,cancelTitle: "Cancel") { (clickedIndex:Int, isCancel:Bool) in
            switch clickedIndex{
            case 0:
                let buildPlanVC = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "BuildPlanViewController") as! BuildPlanViewController
                buildPlanVC.updateOnDeletePlan = {[weak self] (palnId) in
                    guard let weakSelf = self else{return}
                    if let plan = weakSelf.arrModelMyPlan.first(where: { $0.programId == palnId }) , let delIndex = weakSelf.arrModelMyPlan.index(of: plan){
                        weakSelf.arrModelMyPlan.remove(at: delIndex)
                    }
                    weakSelf.myPlanTableview.reloadData()
                }
                buildPlanVC.updateOnFavStateChange = {[weak self] (palnId, status) in
                    guard let weakSelf = self else{return}
                    if let plan = weakSelf.arrModelMyPlan.first(where: { $0.programId == palnId }){
                        plan.programFavStatus = status
                    }
                    weakSelf.myPlanTableview.reloadData()
                }
                
                buildPlanVC.planId = plan.programId
                buildPlanVC.planScreenType = .planEdit
                BuildPlanViewController.isDataChanged = false
                self.navigationController?.pushViewController(buildPlanVC, animated: true)
            case 1:
                self.callAddToMyPlansAPI_Publish(ProgramId: plan.programId)
                
            case 2:
                plan.programActive = !plan.programActive
                self.callActiveInActiveAPI(with: plan)
                
            case 3:
                self.deletePlanAction(of: plan)
                
            default:
                break
            }
        }
    }
    
    
    
    // for publish api
    func callAddToMyPlansAPI_Publish(ProgramId:String  )
    {
        guard let cUser = DoviesGlobalUtility.currentUser else{return}
        var parameters = [String : Any]()
        parameters[WsParam.authCustomerId] =  cUser.customerAuthToken
        parameters[WsParam.programId] = ProgramId
        parameters["added_by_type"] = "Admin"
        
        DoviesWSCalls.callAddToMyPlanAPI(dicParam: parameters, onSuccess: { (success, response, message) in
            if success{
                GlobalUtility.showToastMessage(msg: "Done")
            }else{
                DoviesGlobalUtility.showSimpleAlert(viewController: self, message: message, completion: nil)
            }
            
        }) { (error) in
            
        }
    }
    
    
    func deletePlanAction(of plan : ModelMyPlan){
        UIAlertController.showAlert(in: self, withTitle: AlertTitle.appName, message: AlertMessages.deleteConfirmation, cancelButtonTitle: "No", destructiveButtonTitle: "Delete", otherButtonTitles: nil) { (alertControl, action, index) in
            if index == 1{
                self.callDeletePlanAPI(of: plan)
            }
        }
    }
    
    func callDeletePlanAPI(of plan : ModelMyPlan){
        guard let user = DoviesGlobalUtility.currentUser else{return}
        var params = [String : Any]()
        params[WsParam.authCustomerId] = user.customerAuthToken
        params[WsParam.programId] = plan.programId
        
        if let index = arrModelMyPlan.index(where: {$0.programId == plan.programId}), arrModelMyPlan.count > index{
            arrModelMyPlan.remove(at: index)
            if #available(iOS 11.0, *) {
                myPlanTableview?.performBatchUpdates({() -> Void in
                    
                    myPlanTableview.deleteRows(at: [IndexPath(item: index, section: 0)], with: .automatic)
                })
                //   myPlanCollView.deleteItems(at: [IndexPath(item: index, section: 0)])}, completion: nil)
            } else {
                // Fallback on earlier versions
            }
            self.IBviewAddPlan.isHidden = !(self.arrModelMyPlan.count == 0)
        }
        DoviesWSCalls.callDeletePlanAPI(dicParam: params, onSuccess: {(success, response, message) in
        }) { (error) in
        }
    }
}



/*
extension MyPlanVC : UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return arrModelMyPlan.count > 0 ? arrModelMyPlan.count : loaderCellsCount
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MyPlanVC.cellIndentifier, for: indexPath) as? DietPlanCollectionCell else{return UICollectionViewCell()}
        cell.btnMore.isHidden = false
        if arrModelMyPlan.count > 0{
            cell.setMyPlanUI(modelMyPlan: arrModelMyPlan[indexPath.row])
            cell.moreButtonMyPlanAction = { (myPlan) in
                if let plan = myPlan{
                    if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
                    {
                        self.showActionSheetAtIndex_admin(plan : plan )
                    }
                    else
                    {
                        self.showActionSheetAtIndex(plan : plan )
                    }
                }
            }
        }
        return cell
    }
    
    
 
    func showActionSheetAtIndex_admin(plan : ModelMyPlan){
        let arrData: [alertActionData]!
            arrData = [
                alertActionData(title: "Edit", imageName: "btn_edit_s21a", highlighedImageName : "btn_edit_s21a"),
                alertActionData(title: "Publish to app", imageName: "icon_publish", highlighedImageName : "icon_publish"),
                alertActionData(title: plan.programActive == false ? "Active" : "Active", imageName: plan.programActive == false ? "icon_Inactive" : "ico_active", highlighedImageName : "ico_active"),
                alertActionData(title: "Delete", imageName: "delete_btn_s27b_h", highlighedImageName : "delete_btn_s27b_h"),
                
            ]
            
            DoviesGlobalUtility.showDefaultActionSheet(on:self,actionData: arrData,cancelTitle: "Cancel") { (clickedIndex:Int, isCancel:Bool) in
                switch clickedIndex{
                case 0:
                    let buildPlanVC = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "BuildPlanViewController") as! BuildPlanViewController
                    buildPlanVC.updateOnDeletePlan = {[weak self] (palnId) in
                        guard let weakSelf = self else{return}
                        if let plan = weakSelf.arrModelMyPlan.first(where: { $0.programId == palnId }) , let delIndex = weakSelf.arrModelMyPlan.index(of: plan){
                            weakSelf.arrModelMyPlan.remove(at: delIndex)
                        }
                        weakSelf.myPlanTableview.reloadData()
                    }
                    buildPlanVC.updateOnFavStateChange = {[weak self] (palnId, status) in
                        guard let weakSelf = self else{return}
                        if let plan = weakSelf.arrModelMyPlan.first(where: { $0.programId == palnId }){
                            plan.programFavStatus = status
                        }
                        weakSelf.myPlanTableview.reloadData()
                    }
                    
                    buildPlanVC.planId = plan.programId
                    buildPlanVC.planScreenType = .planEdit
                    BuildPlanViewController.isDataChanged = false
                    self.navigationController?.pushViewController(buildPlanVC, animated: true)
                case 1:
                    self.callAddToMyPlansAPI_Publish(ProgramId: plan.programId)
                   
                case 2:
                    plan.programActive = !plan.programActive
                    self.callActiveInActiveAPI(with: plan)

                case 3:
                    self.deletePlanAction(of: plan)
                    
                default:
                    break
                }
                
            }
       }
    
    
    
    // for publish api
    func callAddToMyPlansAPI_Publish(ProgramId:String  )
    {
        guard let cUser = DoviesGlobalUtility.currentUser else{return}
        var parameters = [String : Any]()
        parameters[WsParam.authCustomerId] =  cUser.customerAuthToken
        parameters[WsParam.programId] = ProgramId
        parameters["added_by_type"] = "Admin"

        DoviesWSCalls.callAddToMyPlanAPI(dicParam: parameters, onSuccess: { (success, response, message) in
            if success{
                GlobalUtility.showToastMessage(msg: "Done")
            }else{
                DoviesGlobalUtility.showSimpleAlert(viewController: self, message: message, completion: nil)
            }
            
        }) { (error) in
            
        }
    }
    
    
    func deletePlanAction(of plan : ModelMyPlan){
        UIAlertController.showAlert(in: self, withTitle: AlertTitle.appName, message: AlertMessages.deleteConfirmation, cancelButtonTitle: "No", destructiveButtonTitle: "Delete", otherButtonTitles: nil) { (alertControl, action, index) in
            if index == 1{
                self.callDeletePlanAPI(of: plan)
            }
        }
    }
    
    func callDeletePlanAPI(of plan : ModelMyPlan){
        guard let user = DoviesGlobalUtility.currentUser else{return}
        var params = [String : Any]()
        params[WsParam.authCustomerId] = user.customerAuthToken
        params[WsParam.programId] = plan.programId
    
        if let index = arrModelMyPlan.index(where: {$0.programId == plan.programId}), arrModelMyPlan.count > index{
            arrModelMyPlan.remove(at: index)
            myPlanCollView?.performBatchUpdates({() -> Void in myPlanCollView.deleteItems(at: [IndexPath(item: index, section: 0)])}, completion: nil)
            self.IBviewAddPlan.isHidden = !(self.arrModelMyPlan.count == 0)
        }
        DoviesWSCalls.callDeletePlanAPI(dicParam: params, onSuccess: {(success, response, message) in
        }) { (error) in
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == (arrModelMyPlan.count - 1) && shouldLoadMore == true {
            callMyPlanAPI()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionFooter:
            guard let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,withReuseIdentifier: BaseCollectionView.collectionViewLoadMoreIdentifier, for: indexPath) as? CollectionLoadMoreView else{return UICollectionReusableView()}
            return footerView
            
        default:
            return UIView() as! UICollectionReusableView
        }
    }
    
}



extension MyPlanVC : UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath){
        if arrModelMyPlan.count > 0{
            let model : ModelMyPlan = self.arrModelMyPlan[indexPath.item]
            if model.programAccessLevel.uppercased() == "OPEN".uppercased(){
				let buildPlanVC = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "BuildPlanViewController") as! BuildPlanViewController
				buildPlanVC.updateOnDeletePlan = {[weak self] (palnId) in
					guard let weakSelf = self else{return}
					if let plan = weakSelf.arrModelMyPlan.first(where: { $0.programId == palnId }) , let delIndex = weakSelf.arrModelMyPlan.index(of: plan){
						weakSelf.arrModelMyPlan.remove(at: delIndex)
					}
					weakSelf.myPlanTableview.reloadData()
					
				}
				buildPlanVC.updateOnFavStateChange = {[weak self] (palnId, status) in
					guard let weakSelf = self else{return}
					if let plan = weakSelf.arrModelMyPlan.first(where: { $0.programId == palnId }){
						plan.programFavStatus = status
					}
					weakSelf.myPlanTableview.reloadData()
				}
				buildPlanVC.planId = arrModelMyPlan[indexPath.item].programId
				buildPlanVC.planScreenType = .planView
				BuildPlanViewController.isDataChanged = false
                buildPlanVC.isFromEditAdmin = true
             self.navigationController?.pushViewController(buildPlanVC, animated: true)
            }else{
                if let upgradeVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "UpgradeToPremiumViewController") as? UpgradeToPremiumViewController{
                    upgradeVC.onPurchaseSuccess = {
                        self.callMyPlanAPI()
                    }
                    self.navigationController?.pushViewController(upgradeVC, animated: true)
                }
            }
        }
    }
 
}

extension MyPlanVC : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return shouldLoadMore ? CGSize(width: ScreenSize.width, height: HeightConstants.baseCollectionViewFooterHeight) : CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     //   return CGSize(width: cellSizeValue, height: (cellSizeValue + 50))
        return CGSize(width: cellSizeValue, height: (cellSizeValue ))
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: MyPlanVC.cellPadding, right: 0  )
    }
 
}
*/
