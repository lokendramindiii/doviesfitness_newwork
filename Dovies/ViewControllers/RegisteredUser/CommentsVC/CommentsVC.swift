//
//  CommentsVC.swift
//  Dovies
//
//  Created by hb on 22/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
class CommentsVC: BaseViewController,UITableViewDelegate,UITableViewDataSource,GrowingTextViewDelegate{
    
    @IBOutlet var inputToolbar                  : UIView!
    @IBOutlet var comTblView                    : UITableView!
    @IBOutlet weak var growingTextView          : GrowingTextView!
    @IBOutlet weak var textViewBottomConstraint : NSLayoutConstraint!
    @IBOutlet var btnCommentSend                : UIButton!
    @IBOutlet var btnUserProfile                : UIButton!
    
    var newsFeed                                : NewsFeedModel?
    var arrComments                             : [NewsFeedComment] = [NewsFeedComment]()
    var isAllowCommentsAdd                      : Bool = true
    var newsFeedId                              : String?
    var pullToRefreshCtrl                       :UIRefreshControl!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: ViewLifeCycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Comments".uppercased()
        self.navigationItem.hidesBackButton  = true
        self.addNavigationWhiteBackButton()
        self.comTblView.tableFooterView = UIView()
        self.textViewLayoutConfi()
        //self.setBottomViewTopLine()
        inputToolbar.isHidden = !isAllowCommentsAdd
        growingTextView.layer.cornerRadius = 4.0
        // *** Listen to keyboard show / hide ***
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillChangeFrame), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        

        // *** Hide keyboard when tapping outside ***
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureHandler))
        view.addGestureRecognizer(tapGesture)
        
        self.callGetNewsFeedCommentsAPI()
        
        comTblView.estimatedRowHeight = 76
        
        inputToolbar.layer.borderWidth = 0.4
        inputToolbar.layer.cornerRadius = 3
        inputToolbar.layer.borderColor =  UIColor(r: 34, g: 34, b: 34, alpha: 1.0).cgColor
        
        self.setPullToRefresh ()
        
    }
    
    
    
    func setPullToRefresh(){
        
        pullToRefreshCtrl = UIRefreshControl()
        
        pullToRefreshCtrl.addTarget(self, action: #selector(self.pullToRefreshClick(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            comTblView.refreshControl  = pullToRefreshCtrl
        } else {
            comTblView.addSubview(pullToRefreshCtrl)
        }
    }
    
    @objc func pullToRefreshClick(sender:UIRefreshControl) {
        self.pullToRefreshCtrl?.endRefreshing()
        
        self.callGetNewsFeedCommentsAPI()
        self.pullToRefreshCtrl?.endRefreshing()
        
    }
    
    //MARK: Instance methods
    
    @objc private func keyboardWillChangeFrame(_ notification: Notification) {
        if let endFrame = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue, let  height = self.navigationController?.navigationBar.frame.size.height {
            var keyboardHeight = view.bounds.height - endFrame.origin.y + height + 17
            keyboardHeight = keyboardHeight > 0 ? keyboardHeight : 0
            textViewBottomConstraint.constant = -keyboardHeight
            view.updateConstraintsIfNeeded()
            view.layoutIfNeeded()
        }
    }
    @objc func tapGestureHandler() {
        view.endEditing(true)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isTranslucent = false

        IQKeyboardManager.shared.enable = false
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isTranslucent = true
        IQKeyboardManager.shared.enable = true
    }
    
    // MARK: IBAction methods
    
    @IBAction func btnSendTapped(sender: UIButton) {
        if self.growingTextView.text.isEmptyAfterRemovingWhiteSpacesAndNewlines() == true {
            return
        }
        self.growingTextView.resignFirstResponder()
        self.callPostNewsCommentAPI()
    }
    
    // MARK: WebService mwethods
    
    /**
     This method is used to fetch the comments for each news feed and updates the UI.
     */
    
    func callGetNewsFeedCommentsAPI() {
        guard let cUser = DoviesGlobalUtility.currentUser  else {return}
        btnCommentSend.isUserInteractionEnabled = false
        
        var newsid = ""
        if let nId = newsFeed?.newsId{
            newsid = nId
        }else if let newId = newsFeedId{
            newsid = newId
        }
        
        var params = [String: Any]()
        params[WsParam.authCustomerId] = cUser.customerAuthToken
        params[WsParam.newsId] = newsid
        params[WsParam.recLimit] = ""
        
        DoviesWSCalls.callGetNewsFeedCommentsAPI(dicParam: params, onSuccess: { [weak self] (success, commentsList, message, nextPageStatus) in
            guard let weakSelf = self else {return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
            if let arr = commentsList, success {
                
                weakSelf.comTblView.backgroundView = nil
                weakSelf.arrComments = arr
                weakSelf.comTblView.reloadData()
                
                //  weakSelf.scrollToBottom(animated: false)
                // weakSelf.scrollToTop(animated: false)
                
            } else {
                
                //   DoviesGlobalUtility.setNoLabelData(tblView:    weakSelf.comTblView, text: "No comments found.")
                
            }
            weakSelf.btnCommentSend.isUserInteractionEnabled = true
        }) {[weak self] (error) in
            guard let weakSelf = self else{return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
            weakSelf.btnCommentSend.isUserInteractionEnabled = true
        }
    }
    
    /**
     This method is used to show the number of comments for each feed.
     - Parameter isAdd: is used to increase/decrease the count.
     */
    func updateCount(isAdd : Bool){
        if let feed = newsFeed{
            if let count = NSInteger(feed.feedCommentsCount){
                if isAdd{
                    feed.feedCommentsCount = "\(count + 1)"
                }else{
                    feed.feedCommentsCount = "\(count - 1)"
                }
                
            }else{
                feed.feedCommentsCount = "1"
            }
            if let newsFeedVC = self.navigationController?.viewControllers[0] as? NewsFeedViewController{
                newsFeedVC.updateTableCells?()
            }
            
        }
    }
    
    func scrollToBottom(animated: Bool){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.arrComments.count-1, section: 0)
            self.comTblView.scrollToRow(at: indexPath, at: .bottom, animated: animated)
            
        }
    }
    
    func scrollToTop(animated: Bool){
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.arrComments.count-1, section: 0)
            self.comTblView.scrollToRow(at: indexPath, at: .top, animated: animated)
            
        }
    }
    
    /**
     This method is used to post a comment for a paritcular feed.
     */
    func callPostNewsCommentAPI() {
        guard let cUser = DoviesGlobalUtility.currentUser else {return}
        var newsid = ""
        if let nId = newsFeed?.newsId{
            newsid = nId
        }else if let newId = newsFeedId{
            newsid = newId
        }
        var dic = [String: Any]()
        dic[WsParam.authCustomerId] = cUser.customerAuthToken
        dic[WsParam.newsId] = newsid
        dic[WsParam.commentText] = self.growingTextView.text.removeWhiteSpace()
        
        DoviesWSCalls.callPostNewsCommentAPI(dicParam: dic, onSuccess: { [weak self](success, message, commentObj) in
            guard let weakSelf = self else{return}
            if let comment = commentObj, success {
                weakSelf.growingTextView.text = ""
                //   weakSelf.arrComments.append(comment)
                weakSelf.arrComments.insert(comment, at: 0)
                weakSelf.comTblView.reloadData()
                weakSelf.comTblView.backgroundView = nil
                weakSelf.btnCommentSend.isSelected = false
                weakSelf.updateCount(isAdd : true)
                
            }
        }) { (error) in
            
        }
    }
    
    // MARK: UITableViewDataSource methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrComments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentsCell") as! CommentsCell
        let comment = self.arrComments[indexPath.row]
        
        cell.topSepratorLine.isHidden = true

        cell.imgViewProfile.sd_setImage(with: URL(string: comment.customerProfileImage), placeholderImage: AppInfo.userPlaceHolder)
        
        cell.lblName.text = comment.customerName.isEmpty ? "Unknown" : comment.customerName.capitalized
        
        cell.lblTime.text = comment.newsCommentPostedDays
        //  cell.cellIndex = indexPath
        cell.btnMore.tag = indexPath.row
        
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 1.5
        let att = [NSAttributedStringKey.font : UIFont.tamilRegular(size: 14.0), NSAttributedStringKey.foregroundColor : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1), NSAttributedStringKey.paragraphStyle : style]
        let attStr = NSMutableAttributedString(string: comment.newsComment, attributes: att)
        
        
        cell.lblComment.attributedText = attStr
        cell.moreButtonAction = { (cellIndex) in
            if let indexOfCell = cellIndex{
                self.showActionSheet(at : indexOfCell)
            }
        }
        return cell
    }
    
    /**
     This method is used to show actionsheet for particular comment to delete or report comment.
     - Parameter indexPath: is used to get customerId and if the customerId is same for logged user it will show *'Delete'* otherwise *'Report Comment'*
     */
    func showActionSheet(at indexPath : IndexPath){
        let arrData: [alertActionData]!
        
        print(indexPath.row)
        
        if let cUser = DoviesGlobalUtility.currentUser, arrComments[indexPath.row].customerId == cUser.customerId{
            arrData = [
                alertActionData(title: "Delete", imageName: "btn_del_s21a", highlighedImageName : "btn_del_s21a ")
            ]
        }else{
            arrData = [
                alertActionData(title: "Report Comment", imageName: "abouts_icon_42", highlighedImageName : "abouts_icon_42_h")
            ]
        }
        
        DoviesGlobalUtility.showDefaultActionSheet(on:self,actionData: arrData,cancelTitle: "Cancel") { (clickedIndex:Int, isCancel:Bool) in
            if clickedIndex == 0{
                if let cUser = DoviesGlobalUtility.currentUser, self.arrComments[indexPath.row].customerId == cUser.customerId{
                    self.callDeleteComment(at: indexPath)
                }
                else{
                    self.callReportComment(at: indexPath)
                }
            }
        }
    }
    
    /**
     This method is used to call the delete comment api to delete the comment and after successful deletion, the comment count will update in UI.
     - Parameter indexPath: is used to get commentId for particular index to delete the comment
     */
    
    func callDeleteComment(at indexPath : IndexPath){
        guard let cUser = DoviesGlobalUtility.currentUser, arrComments.count > indexPath.row else {return}
        var params = [String: Any]()
        params[WsParam.authCustomerId] = cUser.customerAuthToken
        params[WsParam.commentId] = arrComments[indexPath.row].newsCommentsId
        self.arrComments.remove(at: indexPath.row)
        // self.comTblView.deleteRows(at: [indexPath], with: .none)
        DispatchQueue.main.async {
            self.comTblView.reloadData()
        }
        DoviesWSCalls.callDeleteCommentAPI(dicParam: params, onSuccess: { (success, message) in
            if success{
                self.updateCount(isAdd : false)
            }
        }) { (error) in
        }
    }
    
    /**
     This method is used to call the delete comment api to report the comment and after successful reporting.
     - Parameter indexPath: is used to get commentId for particular index to report the comment
     */
    func callReportComment(at indexPath : IndexPath){
        guard let cUser = DoviesGlobalUtility.currentUser, arrComments.count > indexPath.row  else {return}
        
        var newsid = ""
        if let nId = newsFeed?.newsId{
            newsid = nId
        }else if let newId = newsFeedId{
            newsid = newId
        }
        
        var params = [String: Any]()
        params[WsParam.authCustomerId] = cUser.customerAuthToken
        params[WsParam.commentId] = arrComments[indexPath.row].newsCommentsId
        params[WsParam.newsId] = newsid
        DoviesWSCalls.callReportCommentAPI(dicParam: params, onSuccess: { (success, message) in
            GlobalUtility.showToastMessage(msg: "Done")
        }) { (error) in
        }
    }
    
    // MARK: UITableViewDelegate Methods
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.7, options: [.curveLinear], animations: { () -> Void in
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func textViewLayoutConfi () {
        automaticallyAdjustsScrollViewInsets = false
        growingTextView.layer.cornerRadius = 4.0
        growingTextView.maxLength = 500
        growingTextView.trimWhiteSpaceWhenEndEditing = false
        growingTextView.placeholder = "Write a Comment..."
        growingTextView.placeholderColor = UIColor(white: 0.8, alpha: 1.0)
        growingTextView.minHeight = 25.0
        growingTextView.maxHeight = 100.0
        growingTextView.backgroundColor = UIColor.black
    }
    
    func setBottomViewTopLine()
    {
        let topBorder = UIView()
        topBorder.backgroundColor = UIColor.separatorGrayColor()
        topBorder.frame = CGRect(x: 0, y: 0, width: 500, height: 1)
        inputToolbar.addSubview(topBorder)
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        btnCommentSend.isSelected = !textView.text.isEmpty
    }
}

