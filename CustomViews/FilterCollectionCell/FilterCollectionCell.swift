//
//  FilterCollectionCell.swift
//  
//
//  Created by CompanyName.
//

import UIKit

protocol FilterCollectionCellDelegate {
    func buttonTogglePressed(obj:FilterDataModel?, selected:Bool, indexPath: IndexPath)
}

class FilterCollectionCell: UICollectionViewCell {
    var delegate : FilterCollectionCellDelegate?
    var obj : FilterDataModel?
    var indexPath : IndexPath!
    
    @IBOutlet var btnFilter: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setData(_ obj:FilterDataModel?, at indexPath : IndexPath, isSelected : Bool){
        self.obj = obj
        if self.obj?.gmDisplayName == "All Levels"
        {
            btnFilter.setTitle("All Level", for: .normal)
        }
        else
        {
            btnFilter.setTitle(obj?.gmDisplayName.capitalized, for: .normal)
        }
        btnFilter.isSelected = isSelected
        self.indexPath = indexPath
    }
    
    @IBAction func toggleFilterOption(_ sender:UIButton){
        sender.isSelected = !sender.isSelected;
        if (self.delegate != nil){
            self.delegate?.buttonTogglePressed(obj: self.obj, selected: sender.isSelected, indexPath: self.indexPath)
        }
    }
    
}
