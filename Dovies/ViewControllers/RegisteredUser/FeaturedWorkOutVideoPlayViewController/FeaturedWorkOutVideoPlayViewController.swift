//
//  FeaturedWorkOutVideoPlayViewController.swift
//  Dovies
//
//  Created by Neel Shah on 17/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

let kMaxIdleTimeSeconds: Float = 3.0

class FeaturedWorkOutVideoPlayViewController: BaseViewController {
    
    var IBtblVideoList: UITableView!
    var IBplayerView: VideoContainerView!
    
    var currentPlayerPlayedCount = 0
    
    var currentIndexPlay = 0{
        didSet{
            self.setNextPreviousButtonStat()
        }
    }
    
    var playerItem: AVPlayerItem!
    var player: AVPlayer!
    var playerLayer: AVPlayerLayer!
    
    var mySound: AVAudioPlayer?
    
    var arrModelVideoList = [ModelVideoListing]()
    
    var modelWorkoutDetail: ModelWorkoutDetail!
    var workOutId = ""
    
    var playerHeight = 179.5 * CGRect.widthRatio
    
    var movingViewTimer: Timer!
    var waitingTimerBetweenVideo: Timer!
    var timeObserverToken: Timer!
    var totalTimeTime: Timer!
    
    var movingViewSpeed:CGFloat = 1.0
    var forwardX:CGFloat = 0.0
    var previousTimeScal: CGFloat = 0.0
    var videoTotalTime : Float64 = 0.0
    var iWaitingLabelText = 3
    var yPos: CGFloat = 65.0
    
    var videoPlayer: VideoContainerView!
    var progressBar: YLProgressBar!
    var lblTimeLandScape : UILabel!
    //narendra
    var viewForLandScapeTimeAndName : UIView!
    var lblLandScapeTime : UILabel!
    var lblLandScapeExName : UILabel!
    var lblLandScapeExType : UILabel!
    var idleTimer: Timer?
    
    var imageBottomShade : UIImageView!
    
    var isimageBottomShadeStatus = false
    
    @IBOutlet weak var consTopEndWorkout: NSLayoutConstraint!
    @IBOutlet weak var consLeadEndWorkout: NSLayoutConstraint!
    @IBOutlet weak var consTrailEndWorkout: NSLayoutConstraint!
    @IBOutlet weak var consBottomPlayerOption: NSLayoutConstraint!
    
    
    @IBOutlet weak var consLeadPlayerOption: NSLayoutConstraint!
    @IBOutlet weak var consTrailPlayerOption: NSLayoutConstraint!
    
    @IBOutlet weak var ConsLeadEndworkoutMusinc: NSLayoutConstraint!
    @IBOutlet weak var ConsTrailEndworkoutScreen: NSLayoutConstraint!
    @IBOutlet weak var ConsTopEndworkoutMusinc: NSLayoutConstraint!
    @IBOutlet weak var ConsTopEndworkoutScreen: NSLayoutConstraint!
    
    @IBOutlet weak var btnPauseWorkout: UIButton!
    
    @IBOutlet weak var consHeightPlayerOption: NSLayoutConstraint!
    
    @IBOutlet weak var View_workoutcomplteBg: UIView!
    
    var topShadeView: UIView!
    //narendra
    var lblCountDownWaiting: UILabel!
    var islandscapeRestTime = false
    
    @IBOutlet var lblTotalTimeOfWorkout: UILabel!
    var viewWaitingBlack: UIView!
    
    @IBOutlet var IBbtnPause: UIButton!
    @IBOutlet var IBbtnNext: UIButton!
    @IBOutlet var IBbtnPrevious: UIButton!
    @IBOutlet var IBbtnFav: UIButton!
    @IBOutlet var IBbtn_Fullexit: UIButton!
    
    @IBOutlet var IBbtnFavWorkoutEnd: UIButton!
    @IBOutlet var IBbtnInfo: UIButton!
    @IBOutlet var IBbtnSound: UIButton!
    
    @IBOutlet var IBbtnEndworkout: UIButton!
    @IBOutlet var IBbtnPlayEndWorkout: UIButton!
    
    @IBOutlet var IBviewEndWorkout: UIView!
    @IBOutlet var IBviewVideoPreview: UIButton!
    @IBOutlet weak var IblblWorkoutName: UILabel!
    @IBOutlet weak var IblblWorkoutTotalTime: UILabel!
    @IBOutlet weak var IblblWorkoutReps: UILabel!
    @IBOutlet weak var constrailing_lblworkoutReps: NSLayoutConstraint!
    @IBOutlet var lblRest: UILabel!
    @IBOutlet var ConsRestTopSpacing: NSLayoutConstraint!
    
    @IBOutlet weak var lblResumeWorkout: UILabel!
    @IBOutlet weak var IblTimer: UILabel!
    @IBOutlet weak var IBviewPlayerBottom: UIView!
    @IBOutlet weak var IBviewBottomSuperview: UIView!
    
    @IBOutlet weak var IBviewResumBottom: UIView!
    @IBOutlet weak var IBviewResumeLine: UILabel!
    
    @IBOutlet weak var IBconHeight: NSLayoutConstraint!
    @IBOutlet weak var IBconXTime: NSLayoutConstraint!
    @IBOutlet weak var IBbottomHeight: NSLayoutConstraint!
    
    @IBOutlet var View_WorkoutComleteBottom: UIView!
    
    var isFirstVideoDone = false
    var isbreaksound = false
    var isFirstSound = true
    
    var isVideoStartAgain = false
    var isFromWorkoutLog = false
    
    var isPreViewPlaying = false
    var playerCurrentSeekValue = kCMTimeZero
    var cellProgressViewHeight: CGFloat = 5.0
    
    var totalTimeOfWorkOut = 0
    var totalTimeExercisePlay = 0
    
    
    var isWaitingTimeGoingOn = false
    var isWorkoutDetail = false
    
    var isForDownload = false
    var topSpace : CGFloat = 0
    var bottomSpace : CGFloat = 0
    var likeTapped:((_ index: Int) -> ())?
    var onFeedBackSuccess:(() -> ())?
    var onNaviationColor:(() -> ())?
    
    var musicPlayer: MPMusicPlayerController!
    
    var shouldHideHomeIndicator = false
    
    var currentOngoingCell: ExerciseListingTableCell?{
        didSet {
            currentOngoingCell?.IBviewProgress.isHidden = false
            currentOngoingCell?.IBviewProgress.frame = CGRect(x: 0, y: yPos, width: 0, height: cellProgressViewHeight)
            currentOngoingCell?.IBviewProgress.backgroundColor =   UIColor.colorWithRGB(r: 188, g: 212, b: 136, alpha: 0.8)
        }
    }
    
    //MARK:- ViewLifeCycle mehtods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.appBlackColorNew()
        
        IBviewPlayerBottom.backgroundColor = UIColor.appBlackColorNew()
        
        // 14 screen
      //  IBviewBottomSuperview.backgroundColor = UIColor.appBlackColorSplash()
        
        
        self.view.sendSubview(toBack: lblRest)
        View_WorkoutComleteBottom.layer.cornerRadius = 25
        self.navigationController?.isNavigationBarHidden = true
        
        if #available(iOS 11.0, *) {
            let window = UIApplication.shared.keyWindow
            if let tPadding = window?.safeAreaInsets.top{
                topSpace = tPadding
            }
            if let bPadding = window?.safeAreaInsets.bottom{
                bottomSpace = bPadding
            }
        }
        
        
        musicPlayer = MPMusicPlayerController.systemMusicPlayer
        IBconHeight.constant = playerHeight
        
        // lokendra mindiii 10 0ct 2019 IS_IPHONE_X
        if IS_IPHONE_X
        {
            IBplayerView = VideoContainerView(frame: CGRect(x: 0, y: topSpace, width: UIScreen.main.bounds.width, height: playerHeight))
            self.view.addSubview(IBplayerView)
            
            IBtblVideoList = UITableView(frame: CGRect(x: 0, y: playerHeight + topSpace, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - (playerHeight + 48 + bottomSpace + topSpace)))
        }
            
        else
        {
            IBplayerView = VideoContainerView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: playerHeight))
            self.view.addSubview(IBplayerView)
            // Mindiii 13 sep
            
            IBtblVideoList = UITableView(frame: CGRect(x: 0, y: playerHeight , width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - (playerHeight + 48 + bottomSpace + topSpace)))
            
            //  IBtblVideoList = UITableView(frame: CGRect(x: 0, y: playerHeight + topSpace , width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - (playerHeight + 48 + bottomSpace + topSpace)))
            
        }
        
        //  IBtblVideoList = UITableView(frame: CGRect(x: 0, y: playerHeight + topSpace, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - (playerHeight + 48  + topSpace)))
        
        
        IBtblVideoList.separatorStyle = .none
        IBtblVideoList.delegate = self
        IBtblVideoList.dataSource = self
        self.view.addSubview(IBtblVideoList)
        IblTimer.circle()
        IBtblVideoList.register(UINib(nibName: "ExerciseListingTableCell", bundle: nil), forCellReuseIdentifier: "ExerciseListingTableCell")
        self.getVideoUrlFromPathAndPlayInitialVideo()
        
        IBbtnPlayEndWorkout.layer.cornerRadius = IBbtnPlayEndWorkout.bounds.width / 2
        
        if isForDownload == true{
            IBbtnFav.isHidden = true
            IBbtnFavWorkoutEnd.isHidden = true
        }
        
        if let sound = self.setupAudioPlayerWithFile(file: "boxingbell", type: "mp3") {
            self.mySound = sound
        }
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: Notification.Name.UIApplicationWillResignActive, object: nil)
        
        IBbtnEndworkout.addTarget(self, action: #selector(buttonDown), for: .touchDown)
        IBbtnEndworkout.addTarget(self, action: #selector(buttonUp), for: [.touchUpInside, .touchUpOutside])
        //narendra
        // *** Show Video options when tapping screen ***
        let tapOnVideo = UITapGestureRecognizer(target: self, action: #selector(actionTapOnVideoPlayer))
        IBplayerView.addGestureRecognizer(tapOnVideo)
        //resetIdleTimer()
        self.lblTotalTimeOfWorkout.font = UIFont.futuraBold(size: 30)
        self.IblblWorkoutTotalTime.font = UIFont.futuraBold(size: 28)
        self.IblblWorkoutReps.font = UIFont.futuraBold(size: 25)
        
        //narendra
        setRegualarTextOnEndTime()
        IBtblVideoList.backgroundColor = UIColor.black
    }
    
    
    @objc func appMovedToBackground() {
        if IBviewEndWorkout.isHidden == true{
            self.makePlayerPause(sender: IBbtnPause)
        }
        else{
            IBviewVideoPreview.alpha = 1.0
            IBviewVideoPreview.setTitle("Review Movement", for: .normal)
            IBviewVideoPreview.setImage(UIImage(named: "review_icon_s_46a"), for: .normal)
        }
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        //  UIApplication.shared.statusBarStyle = .lightContent
        
        self.navigationController?.isNavigationBarHidden = true
        APP_DELEGATE.isRotateAllow = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //        if IS_IPHONE_X
        //        {
        UIApplication.shared.isStatusBarHidden = true
        //   }
        
        UIApplication.shared.isIdleTimerDisabled = true
        
        if isFromWorkoutLog{
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //        if IS_IPHONE_X
        //        {
        UIApplication.shared.isStatusBarHidden = false
        //  }
        
        
        UIApplication.shared.isIdleTimerDisabled = false
    }
    
    
    override func prefersHomeIndicatorAutoHidden() -> Bool
    {
        return shouldHideHomeIndicator
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK:- Get Video and setUp Player
    func getVideoUrlFromPathAndPlayInitialVideo(){
        
        arrModelVideoList = ModelVideoListing.createStaticJson(arrModelWorkoutExerciseList: modelWorkoutDetail.workoutExerciseList)
        if arrModelVideoList.count > 0
        {
            IBbtnPrevious.isEnabled = false
            if arrModelVideoList.count == 1
            {
                IBbtnNext.isEnabled = false
            }
            IBtblVideoList.reloadData()
        }
        if arrModelVideoList.count > 0{
            
            self.view.layoutSubviews()
            self.view.layoutIfNeeded()
            self.setNewVideoInPlayerForFirstTime(bounds: IBplayerView.bounds)
            
        }
        else{
            self.dismiss(animated: true, completion: {
            })
        }
    }
    
    func setupAudioPlayerWithFile(file: NSString, type: NSString) -> AVAudioPlayer? {
        
        let path = Bundle.main.path(forResource: file as String, ofType: type as String)
        let url = NSURL.fileURL(withPath: path!)
        var audioPlayer: AVAudioPlayer?
        do {
            try audioPlayer = AVAudioPlayer(contentsOf: url)
        } catch {
            print("Player not available")
        }
        
        return audioPlayer
    }
    
    func setNewVideoInPlayerForFirstTime(bounds: CGRect) {
        self.view.isUserInteractionEnabled = false
        
        isVideoStartAgain = false
        player = AVPlayer()
        player.isMuted = IBbtnSound.isSelected
        playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = .resizeAspect;
        playerLayer.backgroundColor = UIColor.clear.cgColor
        if (playerLayer.superlayer != nil){
            playerLayer.removeFromSuperlayer()
        }
        
        playerLayer.frame =  CGRect(x: 0, y: 0, width: bounds.width, height: bounds.height);
        
        videoPlayer = VideoContainerView(frame: bounds)
        IBplayerView.addSubview(videoPlayer)
        
        videoPlayer.layer.addSublayer(playerLayer);
        player.actionAtItemEnd = .pause
        videoPlayer.playerLayer = playerLayer;
        if arrModelVideoList[currentIndexPlay].isWorkoutFav != nil{
            IBbtnFav.isSelected = arrModelVideoList[currentIndexPlay].isWorkoutFav
        }
        IBbtnFavWorkoutEnd.isSelected = IBbtnFav.isSelected
        IblblWorkoutName.text = arrModelVideoList[currentIndexPlay].sVideoName
        
        if arrModelVideoList[currentIndexPlay].isSetOfReps == false
        {
            
            IblblWorkoutReps.text =  ""
            IblblWorkoutReps.isHidden = true
        }
        else
        {
            IblblWorkoutReps.isHidden = false
            
            if arrModelVideoList[currentIndexPlay].workoutRepeatText == "01"
            {
                IblblWorkoutReps.text = arrModelVideoList[currentIndexPlay].workoutRepeatText + " Rep"
            }
            else
            {
                IblblWorkoutReps.text = arrModelVideoList[currentIndexPlay].workoutRepeatText + " Reps"
            }
            
        }
        
        
        let asset = AVURLAsset(url: arrModelVideoList[currentIndexPlay].urlVideoPath)
        
        playerItem = AVPlayerItem(asset: asset)
        playerItem.addObserver(self, forKeyPath: "status", options: .new, context: nil)
        self.player.replaceCurrentItem(with: self.playerItem)
        
        self.currentPlayerPlayedCount = 0
        self.playerLayer.player?.play()
        
        if DoviesGlobalUtility.isMusicAdded == true{
            self.musicPlayer.play()
        }
        else{
            print("Music is Not Added......")
        }
        
        
        if movingViewTimer != nil && movingViewTimer.isValid{
            movingViewTimer.invalidate()
            movingViewTimer = nil
        }
        
        if (self.IBtblVideoList.indexPathsForVisibleRows?.contains(IndexPath(row: self.currentIndexPlay, section: 0)))!{
            currentOngoingCell = IBtblVideoList.cellForRow(at: IndexPath(row: self.currentIndexPlay, section: 0)) as? ExerciseListingTableCell
            currentOngoingCell?.IBviewProgress.isHidden = false
            currentOngoingCell?.IBlblRepeatMode.textColor = UIColor.white
            currentOngoingCell?.IBlblExerciseName.textColor = UIColor.white
            
            //      currentOngoingCell?.contentView.backgroundColor = UIColor.appBlackColorNew()
            currentOngoingCell?.IBviewBg.backgroundColor = UIColor.appBlackColorSplash()
            //000   currentOngoingCell?.IBviewBreakTime.backgroundColor = UIColor.appBlackColorNew()
            //    currentOngoingCell?.IBviewBreakTime.backgroundColor = UIColor.green
            
            
        }
        isWaitingTimeGoingOn = false
        
        let ViewBlackheight = UIApplication.shared.statusBarOrientation.isLandscape ? UIScreen.main.bounds.height : UIScreen.main.bounds.height-100
        viewWaitingBlack = UIView(frame: CGRect(x: 0, y: 0, width: 1000, height:  ViewBlackheight))
        
        viewWaitingBlack.backgroundColor = UIColor.black
        viewWaitingBlack.alpha = 1.0
        viewWaitingBlack.isHidden = true
        videoPlayer.addSubview(viewWaitingBlack)
        
        let xProgress = IS_IPHONE_X ? topSpace + 29 : 0
        //        let yProgress =  UIDevice.current.orientation.isLandscape ? self.view.bounds.height - 30 : self.view.bounds.width - 30
        //        print(yProgress)
        
        //     let yProgress = UIApplication.shared.statusBarOrientation.isLandscape ? self.view.bounds.height - 15 : self.view.bounds.width - 15
        
        
        let yProgress = UIApplication.shared.statusBarOrientation.isLandscape ? self.view.bounds.height : self.view.bounds.width
        
        print(yProgress)
        
        let widthProgress = UIApplication.shared.statusBarOrientation.isLandscape ? IS_IPHONE_X ? UIScreen.main.bounds.width - (bottomSpace * 4 + 10) : UIScreen.main.bounds.width : IS_IPHONE_X ? UIScreen.main.bounds.height - (bottomSpace * 4 + 10) : UIScreen.main.bounds.height
        
        print(widthProgress)
        
        progressBar = YLProgressBar(frame: CGRect(x:xProgress , y: yProgress, width: widthProgress, height: 15))
        
        print("frame = \(progressBar.frame)")
        
        progressBar.type = .flat
        progressBar.setProgress(0.0, animated: false)
        progressBar.hideStripes = true
        progressBar.behavior = .default
        progressBar.alpha = 0.9
        
        if UIDevice.current.orientation == .landscapeLeft
        {
            progressBar.progressTintColors = [UIColor.clear, UIColor.clear]
            progressBar.trackTintColor = UIColor.clear
        }
        else
        {
            progressBar.progressTintColors = [UIColor.colorWithRGB(r: 188, g: 212, b: 136, alpha: 0.8), UIColor.colorWithRGB(r: 188, g: 212, b: 136, alpha: 0.8)]
            progressBar.trackTintColor = UIColor.colorWithRGB(r: 168, g: 164, b: 163, alpha: 0.8)
        }
        
        videoPlayer.addSubview(progressBar)
        
        let isH = self.arrModelVideoList[self.currentIndexPlay].isSetOfReps
        
        progressBar.isHidden = false
        
        ///narendra
        addViewForTopShadeOnVideo()
        addViewForVideoInfo()
        ///narendra
        
        movingViewSpeed = (arrModelVideoList[self.currentIndexPlay].fTotalSeconds/IBtblVideoList.bounds.width)
        
        self.previousTimeScal = 0.0
        
        self.view.bringSubview(toFront: IblblWorkoutReps)
        self.view.bringSubview(toFront: lblTotalTimeOfWorkout)
        self.view.bringSubview(toFront: IBviewPlayerBottom)
        
        playerEndObserver()
    }
    
    
    //MARK:- Transaction/Amination between video switch
    func setNewVideoInPlayerForSecondView(){
        self.makeRunningTimeNull()
        self.view.isUserInteractionEnabled = false
        if mySound != nil{
            mySound?.play()
        }
        self.setNewVideoInPlayerForFirstTime(bounds: CGRect(x: 0, y: IBplayerView.frame.size.height, width: IBplayerView.frame.size.width, height: IBplayerView.frame.size.height))
    }
    
    func setUpNextVideoToPlay(){
        
        if lblCountDownWaiting != nil{
            lblCountDownWaiting.isHidden = true
            self.view.sendSubview(toBack: lblRest)
        }
        
        let currentOldIndex = self.currentIndexPlay
        self.currentIndexPlay += 1
        
        self.arrModelVideoList[currentOldIndex].sVideoInSeconds = self.arrModelVideoList[currentOldIndex].sOriginalVideoInSeconds
        IBtblVideoList.reloadRows(at: [IndexPath(row: currentOldIndex, section: 0)], with: .none)
        
        if self.currentIndexPlay < self.arrModelVideoList.count{
            if (self.IBtblVideoList.indexPathsForVisibleRows?.contains(IndexPath(row: self.currentIndexPlay, section: 0)))!{
                self.IBtblVideoList.scrollToRow(at: IndexPath(row: self.currentIndexPlay, section: 0), at: .top, animated: true)
            }
            self.setNewVideoInPlayerForSecondView()
        }
        else{
            playerLayer.player?.pause()
            self.makeRunningTimeNull()
        }
    }
    
    func doPlayerAnimationTransaction(){
        if isFirstVideoDone == true{
            var viewVideoPlayer1: VideoContainerView!
            var viewVideoPlayer2: VideoContainerView!
            let arrVideoViews = NSMutableArray()
            
            for view in IBplayerView.subviews{
                if view.isKind(of: VideoContainerView.self){
                    arrVideoViews.add(view)
                }
            }
            
            if arrVideoViews.count > 0{
                viewVideoPlayer1 = arrVideoViews[0] as? VideoContainerView
            }
            if arrVideoViews.count > 1{
                viewVideoPlayer2 = arrVideoViews[1] as? VideoContainerView
            }
            
            if viewVideoPlayer1 != nil && viewVideoPlayer2 != nil{
                UIView.animate(withDuration: 0.7, animations: {
                    viewVideoPlayer1.frame = CGRect(x: viewVideoPlayer1.frame.origin.x, y: viewVideoPlayer1.frame.origin.y - self.IBplayerView.bounds.height, width: self.IBplayerView.bounds.width, height: self.IBplayerView.bounds.height)
                    viewVideoPlayer2.frame = CGRect(x: viewVideoPlayer2.frame.origin.x, y: viewVideoPlayer2.frame.origin.y - self.IBplayerView.bounds.height, width: self.IBplayerView.bounds.width, height: self.IBplayerView.bounds.height)
                }, completion: { (success) in
                    if success{
                        viewVideoPlayer1.removeFromSuperview()
                        self.view.isUserInteractionEnabled = true
                    }
                })
            }
        }
        else
        {
            self.view.isUserInteractionEnabled = true
        }
    }
    
    //MARK:- Start/End Timer, updating Progress view and updating time for exercise
    func startProgressTimerAndTimeValue () {
        if timeObserverToken == nil{
            timeObserverToken = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateTableCellValue), userInfo: nil, repeats: true)
            RunLoop.current.add(timeObserverToken!, forMode: .commonModes)
        }
        
        if self.movingViewTimer == nil {
            self.movingViewTimer =  Timer.scheduledTimer(
                timeInterval: TimeInterval(movingViewSpeed),
                target      : self,
                selector    : #selector(self.startAnimating),
                userInfo    : nil,
                repeats     : true)
            RunLoop.current.add(self.movingViewTimer!, forMode: .commonModes)
        }
        
        
        if isFirstSound == true
        {
            isFirstSound = false
            if mySound != nil{
                mySound?.play()
            }
        }
        
        self.startTotalTimeTimer()
    }
    
    @objc func startAnimating() {
        UIView.animate(withDuration: 0.3) {
            self.forwardX += 1.0
            if (self.IBtblVideoList.indexPathsForVisibleRows?.contains(IndexPath(row: self.currentIndexPlay, section: 0)))!{
                if self.currentOngoingCell?.index.row != self.currentIndexPlay{
                    self.currentOngoingCell = self.IBtblVideoList.cellForRow(at: IndexPath(row: self.currentIndexPlay, section: 0)) as? ExerciseListingTableCell
                }
                
                self.currentOngoingCell?.IBviewProgress.frame = CGRect(x: 0, y: self.yPos, width: self.forwardX, height: self.cellProgressViewHeight)
            }
        }
        var newtime = CMTimeGetSeconds(self.player!.currentTime());
        if self.isVideoStartAgain{
            newtime = newtime + (videoTotalTime * Double(currentPlayerPlayedCount))
        }
        let progressProgressing = CGFloat(newtime)/self.arrModelVideoList[self.currentIndexPlay].fTotalSeconds
        self.progressBar.setProgress(progressProgressing, animated: false)
        
    }
    
    @objc func updateTableCellValue(){
        
        self.totalTimeExercisePlay += 1
        self.previousTimeScal = CGFloat(CMTimeGetSeconds(CMTime(value: 10000, timescale: 10000))) + self.previousTimeScal
        let totalTime = self.arrModelVideoList[self.currentIndexPlay].fTotalSeconds - self.previousTimeScal
        let dMinutes: Int = Int(totalTime) % 3600 / 60
        let dSeconds: Int = Int(totalTime) % 3600 % 60
        if dSeconds < 0 && self.arrModelVideoList[self.currentIndexPlay].isTimeRepeat == true{
            playerLayer.player?.pause()
            totalTimeOfWorkOut -= 1 // Mindiii decrease -1 second time every next exercse play
            
            self.totalTimeExercisePlay -= 1
            self.makeRunningTimeNull()
            self.startWaitingTimer()
            return
        }
        
        self.arrModelVideoList[self.currentIndexPlay].sVideoInSeconds = "\(dMinutes):\(String(format: "%02d", dSeconds))"
        self.lblTimeLandScape.text = self.arrModelVideoList[self.currentIndexPlay].sVideoInSeconds
        
        //self.lblTimeLandScape.makeOutLine()
        if (self.IBtblVideoList.indexPathsForVisibleRows?.contains(IndexPath(row: self.currentIndexPlay, section: 0)))!{
            if currentOngoingCell?.index.row != self.currentIndexPlay{
                currentOngoingCell = IBtblVideoList.cellForRow(at: IndexPath(row: self.currentIndexPlay, section: 0)) as? ExerciseListingTableCell
            }
            if self.currentIndexPlay == (currentOngoingCell?.IBlblTime.tag)! % 1000{
                
                currentOngoingCell?.IBlblTime.text = self.arrModelVideoList[self.currentIndexPlay].sVideoInSeconds
            }
        }
    }
    
    @objc func updateTotalTimeOfWorOut(){
        
        // mindiii comment below two line
        //totalTimeOfWorkOut += 1
        //lblTotalTimeOfWorkout.text = secondsToHoursMinutesSeconds(seconds: totalTimeOfWorkOut)
        
        lblTotalTimeOfWorkout.text = secondsToHoursMinutesSeconds(seconds: totalTimeExercisePlay)
        
        IblblWorkoutTotalTime.text = lblTotalTimeOfWorkout.text
        lblTotalTimeOfWorkout.makeOutLine()
        IblblWorkoutReps.makeOutLine()
    }
    
    @objc func waitingPeriod(){
        IBbtnPause.isHidden = false
        self.forwardX = 0.0
        //        self.playerItem.removeObserver(self, forKeyPath: "status")
        self.setUpNextVideoToPlay()
    }
    
    func secondsToHoursMinutesSeconds (seconds : Int) -> (String) {
        let hour = seconds / 3600
        let minutes = (seconds % 3600) / 60
        let seconds = (seconds % 3600) % 60
        if hour > 0
        {
            return (String(format: "%02d", hour) + ":" + String(format: "%02d", minutes) + ":" + String(format: "%02d", seconds))
        }
        else
        {
            return (String(format: "%02d", minutes) + ":" + String(format: "%02d", seconds))
        }
    }
    
    func makeRunningTimeNull(){
        if waitingTimerBetweenVideo != nil{
            if waitingTimerBetweenVideo.isValid{
                waitingTimerBetweenVideo.invalidate()
                waitingTimerBetweenVideo = nil
            }
        }
        
        if timeObserverToken != nil{
            if timeObserverToken.isValid{
                timeObserverToken.invalidate()
                timeObserverToken = nil
            }
        }
        
        if movingViewTimer != nil{
            if movingViewTimer.isValid{
                movingViewTimer.invalidate()
                movingViewTimer = nil
            }
        }
    }
    
    func startTotalTimeTimer(){
        
        if self.totalTimeTime == nil{
            
            self.self.totalTimeTime = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateTotalTimeOfWorOut), userInfo: nil, repeats: true)
            RunLoop.current.add(self.totalTimeTime!, forMode: .commonModes)
        }
    }
    
    //MARK:- Break Timer and it's update
    func startWaitingTimer(){
        if mySound != nil{
            mySound?.play()
        }
        if self.currentIndexPlay == self.arrModelVideoList.count - 1{
            playerLayer.player?.pause()
            
            self.makeRunningTimeNull()
            if totalTimeTime != nil{
                if totalTimeTime.isValid{
                    totalTimeTime.invalidate()
                    totalTimeTime = nil
                }
            }
            IBbtnPause.isHidden = true
            // loks
            //self.endWorkoutTap()
            self.view.bringSubview(toFront: View_workoutcomplteBg)
            View_workoutcomplteBg.isHidden = false
            
        }
        else{
            
            print("isBreakTimeThere == \(self.arrModelVideoList[self.currentIndexPlay].isBreakTimeThere)")
            print("fBreakTime == \(self.arrModelVideoList[self.currentIndexPlay].fBreakTime)")
            self.playerItem.removeObserver(self, forKeyPath: "status")
            if self.arrModelVideoList[self.currentIndexPlay].isBreakTimeThere == true{
                self.progressBar.isHidden = true
                isWaitingTimeGoingOn = true
                totalTimeOfWorkOut -= 1 // mindiii time -1 second when break time start
                self.view.sendSubview(toBack: IblblWorkoutReps)
                
                viewWaitingBlack.isHidden = false
                // lokendra
                currentOngoingCell?.IBviewBg.backgroundColor = UIColor.black
                currentOngoingCell?.IBviewProgress.isHidden = true
                // lokendra
                
                currentOngoingCell?.IBviewBreakTime.isHidden = false
                currentOngoingCell?.IBbtnWorkOutDone.isSelected = true
                iWaitingLabelText = self.arrModelVideoList[self.currentIndexPlay].fBreakTime
                self.view.bringSubview(toFront: IBviewPlayerBottom)
                if lblCountDownWaiting == nil{
                    lblCountDownWaiting = UILabel(frame: CGRect(x: 50, y: 50 , width: 250, height: 65))
                }
                lblCountDownWaiting.isHidden = false
                self.view.bringSubview(toFront: lblRest)
                lblCountDownWaiting.center = IBplayerView.center
                
                self.waitingLabelUpdate(text: lblCountDownWaiting)
                
                self.waitingTimerBetweenVideo = Timer.scheduledTimer(timeInterval: TimeInterval(iWaitingLabelText + 1), target: self, selector: #selector(self.waitingPeriod), userInfo: nil, repeats: false)
            }
            else{
                IBbtnPause.isHidden = false
                self.forwardX = 0.0
                self.setUpNextVideoToPlay()
            }
        }
    }
    
    func waitingLabelUpdate(text: UILabel) {
        text.text = secondsToHoursMinutesSeconds(seconds: iWaitingLabelText)
        
        text.textAlignment = .center
        text.textColor = UIColor.white
        text.transform = CGAffineTransform(scaleX: 0.998, y:0.999)
        self.view.addSubview(text)
        self.islandscapeRestTime = true
        if UIDevice.current.orientation == .landscapeLeft {
            if UIApplication.shared.statusBarOrientation.isPortrait
            {
                text.font = UIFont.futuraBold(size: 35)
                lblRest.font = UIFont.futuraBold(size: 25)
            }
            else{
                text.font = UIFont.futuraBold(size: 70)
                lblRest.font = UIFont.futuraBold(size: 50)
            }
        }
        else if UIDevice.current.orientation == .portrait
        {
            text.font = UIFont.futuraBold(size: 35)
            lblRest.font = UIFont.futuraBold(size: 25)
        }
        UIView.animate(withDuration: 1.0, animations: {
            text.transform = CGAffineTransform(scaleX: 0.999, y: 0.998)
            
        }) { (success) in
            self.iWaitingLabelText -= 1
            self.totalTimeExercisePlay += 1
            if self.iWaitingLabelText != 0{
                if self.player.isPlaying == false{
                    if(self.IBviewEndWorkout.isHidden == true){
                        self.waitingLabelUpdate(text: text )
                    }
                    else{
                        text.isHidden = true
                    }
                }
                else
                {
                    text.isHidden = true
                }
            }
            else
            {
                text.isHidden = true
            }
        }
    }
    
    //MARK:- Player Oberver
    func playerEndObserver(){
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: .main) { [weak self] _ in
            if self?.isPreViewPlaying == true{
                self?.player.seek(to: kCMTimeZero)
                self?.playerLayer.player?.play()
                return
            }
            
            self?.currentPlayerPlayedCount += 1
            //            if self?.arrModelVideoList[(self?.currentIndexPlay)!].isSetOfReps == false
            //            {
            if self?.arrModelVideoList[(self?.currentIndexPlay)!].isTimeRepeat == false{
                self?.makeRunningTimeNull()
                self?.startWaitingTimer()
            }
            else{
                self?.isVideoStartAgain = true
                self?.videoTotalTime = CMTimeGetSeconds((self?.player!.currentTime())!)
                self?.player.seek(to: kCMTimeZero)
                self?.playerLayer.player?.play()
            }
            //            }
            //            else{
            //                self?.isVideoStartAgain = true
            //                self?.videoTotalTime = CMTimeGetSeconds((self?.player!.currentTime())!)
            //                self?.player.seek(to: kCMTimeZero)
            //                self?.playerLayer.player?.play()
            //            }
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        
        if keyPath == "status"{
            if player.currentItem?.status == .readyToPlay{
                if IBviewEndWorkout.isHidden == true{
                    self.doPlayerAnimationTransaction()
                    isFirstVideoDone = true
                    
                    //                    if self.arrModelVideoList[self.currentIndexPlay].isSetOfReps == false
                    //                    {
                    self.startProgressTimerAndTimeValue()
                    //                    }
                    //                    else{
                    //                        self.startTotalTimeTimer()
                    //                    }
                }
            }
        }
    }
    
    @objc func makeWorkOutEnd(){
        if musicPlayer.playbackState == .playing{
            musicPlayer.stop()
        }
        
        DoviesGlobalUtility.isMusicAdded = false
        
        APP_DELEGATE.isRotateAllow = false
        NotificationCenter.default.removeObserver(self)
        if self.isWaitingTimeGoingOn == false || self.currentIndexPlay == (self.arrModelVideoList.count - 1){
            self.playerItem.removeObserver(self, forKeyPath: "status")
        }
    }
    
    func didSelectAndChangeVideo(tableView: UITableView, indexPath: IndexPath){
        
        self.IBbtnPause.isSelected = false
        
        self.makeRunningTimeNull()
        NotificationCenter.default.removeObserver(self, name: .AVPlayerItemDidPlayToEndTime, object: player.currentItem)
        
        if self.isWaitingTimeGoingOn == false{
            self.playerItem.removeObserver(self, forKeyPath: "status")
        }
        
        self.forwardX = 0.0
        self.arrModelVideoList[self.currentIndexPlay].sVideoInSeconds = self.arrModelVideoList[self.currentIndexPlay].sOriginalVideoInSeconds
        
        currentIndexPlay = indexPath.row
        
        self.setNewVideoInPlayerForSecondView()
        for tableCell in tableView.visibleCells{
            let cell = tableCell as! ExerciseListingTableCell
            cell.setUpView(indexPath: cell.index, currentIndexPlay: currentIndexPlay, modelVideoList: arrModelVideoList[cell.index.row])
        }
        tableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    
    //MARK:- All Button action and it's related methods
    
    var timer: Timer?
    var speedAmmo = 0
    var isEndActionCompleted = false
    @objc func buttonDown(_ sender: UIButton) {
        speedAmmo = 0
        isEndActionCompleted = false
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(rapidFire), userInfo: nil, repeats: true)
    }
    
    func setRegualarTextOnEndTime(){
        // let myString = "End Workout\n"
        let myString = "End Workout"
        
      // 13 screen
     //   let myAttribute = [NSAttributedStringKey.foregroundColor: UIColor.black]
    
        let myAttribute = [NSAttributedStringKey.foregroundColor: UIColor.white ,NSAttributedStringKey.font : UIFont.tamilBold(size: 18.0)]

        
        // loks comment
        // let holdString = "Hold for 2 Seconds"
        //  let holdAttribute = [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font : UIFont.tamilRegular(size: 12.0)]
        //  let holdAttrString = NSAttributedString(string: holdString, attributes: holdAttribute)
        
        let myAttrString = NSAttributedString(string: myString, attributes: myAttribute)
        
        let combination = NSMutableAttributedString()
        
        combination.append(myAttrString)
        
        // loks comment
        //  combination.append(holdAttrString)
        IBbtnEndworkout.setAttributedTitle(combination, for: .normal)
        IblTimer.isHidden = true
    }
    
    @objc func buttonUp(_ sender: UIButton) {
        if isEndActionCompleted{return}
        setRegualarTextOnEndTime()
        timer?.invalidate()
        endUpAction()
        
    }
    
    @IBAction func actionWorkoutLogYes(_ sender: UIButton){
        View_workoutcomplteBg.isHidden = true
        self.endWorkoutTap()
    }
    
    
    @IBAction func actionWorkoutLogNo(_ sender: UIButton)
    {
        View_workoutcomplteBg.isHidden = true
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIApplicationWillResignActive, object: nil)
        self.makeWorkOutEnd()
        self.onFeedBackSuccess?()
        self.dismiss(animated: true, completion: nil)
    }
    
    func endUpAction(){
        //        if speedAmmo >= 2{
        //          //  self.endWorkoutTap()
        //          self.view.bringSubview(toFront: View_workoutcomplteBg)
        //          View_workoutcomplteBg.isHidden = false
        //        }
        
        
        if speedAmmo >= 0{
            //  self.endWorkoutTap()
            self.view.bringSubview(toFront: View_workoutcomplteBg)
            View_workoutcomplteBg.isHidden = false
        }
    }
    
    @objc func rapidFire() {
        //loks comment
        //   if speedAmmo < 2 {
        if speedAmmo < 0 {
            speedAmmo += 1
            IblTimer.isHidden = false
            IblTimer.text = "\(speedAmmo)"
            let myString = "\(speedAmmo)"
            let myAttribute = [NSAttributedStringKey.foregroundColor: UIColor.black]
            let myAttrString = NSAttributedString(string: myString, attributes: myAttribute)
            IBbtnEndworkout.setAttributedTitle(myAttrString, for: .normal)
            
        } else {
            isEndActionCompleted = true
            endUpAction()
            timer?.invalidate()
        }
    }
    
    
    @IBAction func endWorkoutTap(){
        self.makeWorkOutEnd()
        if isForDownload == true && NetworkReachabilityManager()?.isReachable == false{
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        print(secondsToHoursMinutesSeconds(seconds: totalTimeExercisePlay))
        
        lblTotalTimeOfWorkout.text = secondsToHoursMinutesSeconds(seconds: totalTimeExercisePlay)
        
        if let feedbackVc = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "FeedbackFormVC") as? FeedbackFormVC{
            NotificationCenter.default.removeObserver(self, name: Notification.Name.UIApplicationWillResignActive, object: nil)
            let navigation = UINavigationController(rootViewController: feedbackVc)
            

            feedbackVc.workoutName = modelWorkoutDetail.workoutName
            feedbackVc.workOutImage = modelWorkoutDetail.workoutImage
            feedbackVc.workoutId = workOutId
            feedbackVc.workoutTime = lblTotalTimeOfWorkout.text ?? ""
            isFromWorkoutLog = true
            feedbackVc.disMissView = { (isSubmitted) in
                if isSubmitted{
                    self.onFeedBackSuccess?()
                }
                else
                {
                    self.onNaviationColor?()
                }
                
            }
            self.present(navigation, animated: true, completion: {
                UIDevice.current.setValue( UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
            })
            
        }
    }
    
    @IBAction func btnFavTap(sender: UIButton){
        sender.isSelected = !sender.isSelected
        if sender == IBbtnFav
        {
            IBbtnFavWorkoutEnd.isSelected = sender.isSelected
        }
        else
        {
            IBbtnFav.isSelected = sender.isSelected
        }
        self.likeTapped!(currentIndexPlay)
        self.callToMakeExcerciseFavourite()
    }
    
    @IBAction func btnInfoTap(sender: UIButton){
        var value = UIInterfaceOrientation.landscapeRight.rawValue
        if UIDevice.current.orientation.isLandscape {
            value = UIInterfaceOrientation.portrait.rawValue
        }
        else{
            value = UIInterfaceOrientation.landscapeRight.rawValue
        }
        UIDevice.current.setValue(value, forKey: "orientation")
        
        return;
        
        APP_DELEGATE.isRotateAllow = false
        // loks comment code
        
        //        let detailsVC = self.storyboard?.instantiateViewController(withIdentifier: "WorkOutExerciseDescriptionDetail")as! WorkOutExerciseDescriptionDetail
        
        let detailsVC = self.storyboard?.instantiateViewController(withIdentifier: "WorkOutExerciseDescription_Detail")as! WorkOutExerciseDescription_Detail
        
        detailsVC.workoutExerciseList = modelWorkoutDetail.workoutExerciseList[currentIndexPlay]
        self.isWorkoutDetail = false
        if IBviewEndWorkout.isHidden == true{
            self.isWorkoutDetail = true
            self.makePlayerPause(sender: IBbtnPause)
        }
        detailsVC.isForWorkOut = { () in
            if self.IBviewEndWorkout.isHidden == true || self.isWorkoutDetail == true{
                self.makePlayerPause(sender: self.IBbtnPause)
            }
            self.isWorkoutDetail = false
        }
        
        detailsVC.modalTransitionStyle = .crossDissolve
        detailsVC.modalPresentationStyle = .overCurrentContext
        self.present(detailsVC, animated: true, completion: nil)
        
    }
    
    @IBAction func mute(sender: UIButton){
        player.isMuted = !sender.isSelected
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func btnNextPreviousTap(sender: UIButton){
        var indexPath: IndexPath!
        if sender == IBbtnPrevious{
            self.view.sendSubview(toBack: lblRest)
            currentIndexPlay = currentIndexPlay - 1
            indexPath = IndexPath(row: currentIndexPlay, section: 0)
        }
        else if sender == IBbtnNext{
            self.view.sendSubview(toBack: lblRest)
            currentIndexPlay = currentIndexPlay + 1
            indexPath = IndexPath(row: currentIndexPlay, section: 0)
        }
        
        self.didSelectAndChangeVideo(tableView: IBtblVideoList, indexPath: indexPath)
    }
    
    func setNextPreviousButtonStat(){
        if currentIndexPlay == 0{
            IBbtnPrevious.isEnabled = false
            IBbtnNext.isEnabled = true
        }
        else if currentIndexPlay == arrModelVideoList.count - 1{
            IBbtnPrevious.isEnabled = true
            IBbtnNext.isEnabled = false
        }
        else{
            IBbtnPrevious.isEnabled = true
            IBbtnNext.isEnabled = true
        }
    }
    
    @IBAction func playVideoPreviewWhilePause(){
        if player.isPlaying == true{
            isPreViewPlaying = false
            playerLayer.player?.pause()
            if musicPlayer.playbackState == .playing{
                musicPlayer.pause()
            }
            IBviewVideoPreview.alpha = 1.0
            IBviewVideoPreview.setTitle("Review Movement", for: .normal)
            IBviewVideoPreview.setImage(UIImage(named: "review_icon_s_46a"), for: .normal)
        }
        else
        {
            IBviewVideoPreview.alpha = 0.1
            isPreViewPlaying = true
            self.playerLayer.player?.play()
            if DoviesGlobalUtility.isMusicAdded == true{
                self.musicPlayer.play()
            }
            else{
                print("Music is Not Added......")
            }
            IBviewVideoPreview.setTitle("", for: .normal)
            IBviewVideoPreview.setImage(nil, for: .normal)
        }
    }
    
    @IBAction func openMusicPlayList(){
        MPMediaLibrary.requestAuthorization { (status) in
            if status == .authorized {
                DispatchQueue.main.async {
                    let mpMediaPickerController = MPMediaPickerController()
                    mpMediaPickerController.allowsPickingMultipleItems = true
                    mpMediaPickerController.delegate = self
                    self.present(mpMediaPickerController, animated: true) {
                        if self.IBviewEndWorkout.isHidden == true{
                            self.makePlayerPause(sender: self.IBbtnPause)
                        }
                    }
                }
            }
            else
            {
                self.displayMediaLibraryError()
            }
        }
    }
    
    func displayMediaLibraryError() {
        var error: String
        switch MPMediaLibrary.authorizationStatus() {
        case .restricted:
            error = "Media library access restricted by corporate or parental settings"
        case .denied:
            error = "Media library access denied by user"
        default:
            error = "Unknown error"
        }
        
        let controller = UIAlertController(title: "Error", message: error, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(controller, animated: true, completion: nil)
    }
    
    @objc @IBAction func makePlayerPause(sender: UIButton){
        if sender.isSelected == false
        {
            if IS_IPHONE_X{
                
                if UIApplication.shared.statusBarOrientation.isLandscape
                {
                    IBbottomHeight.constant =  57
                    ConsLeadEndworkoutMusinc.constant = 29.5
                    ConsTrailEndworkoutScreen.constant = 29.5
                    ConsTopEndworkoutMusinc.constant = 4.5
                    ConsTopEndworkoutScreen.constant = 4.5
                }
                else
                {
                    IBbottomHeight.constant = 81.82
                    ConsLeadEndworkoutMusinc.constant = 0
                    ConsTrailEndworkoutScreen.constant = 0
                    ConsTopEndworkoutMusinc.constant = 0
                    ConsTopEndworkoutScreen.constant = 0
                }
                // IBbottomHeight.constant = 80
            }
            else
            {
                if UIApplication.shared.statusBarOrientation.isLandscape
                {
                    ConsLeadEndworkoutMusinc.constant = 29
                    ConsTrailEndworkoutScreen.constant = 29
                    ConsTopEndworkoutMusinc.constant = 0.5
                    ConsTopEndworkoutScreen.constant = 0.5
                    
                }
                else
                {
                    ConsLeadEndworkoutMusinc.constant = 0
                    ConsTrailEndworkoutScreen.constant = 0
                    ConsTopEndworkoutMusinc.constant = 0
                    ConsTopEndworkoutScreen.constant = 0
                }
                
            }
            playerLayer.player?.pause()
            
            if musicPlayer.playbackState == .playing{
                musicPlayer.pause()
            }
            
            lblTotalTimeOfWorkout.isHidden = true
            IBviewVideoPreview.setTitle("Review Movement", for: .normal)
            IBviewVideoPreview.setImage(UIImage(named: "review_icon_s_46a"), for: .normal)
            progressBar.isHidden = true
            IBviewVideoPreview.alpha = 1.0
            playerCurrentSeekValue = (player.currentItem?.currentTime())!
            self.makeRunningTimeNull()
            
            self.view.bringSubview(toFront: IblblWorkoutReps)
            
            viewWaitingBlack.isHidden = true
            if totalTimeTime != nil{
                if totalTimeTime.isValid{
                    totalTimeTime.invalidate()
                    totalTimeTime = nil
                }
            }
            //narendra
            //self.IBviewVideoPreview.isHidden =  UIDevice.current.orientation.isLandscape ? true : false
            self.IBviewVideoPreview.isHidden = true
            //narendra
            
            self.view.bringSubview(toFront: IBplayerView)
            self.view.bringSubview(toFront: self.IBviewVideoPreview)
            
            self.view.bringSubview(toFront: IBviewEndWorkout)
            IBviewEndWorkout.isHidden = false
        }
        else
        {
            lblTotalTimeOfWorkout.isHidden = false
            isPreViewPlaying = false
            playerLayer.player?.pause()
            if DoviesGlobalUtility.isMusicAdded == true{
                self.musicPlayer.play()
            }
            else{
                print("Music is Not Added......")
            }
            self.IBviewVideoPreview.isHidden = true
            
            self.view.bringSubview(toFront: lblTotalTimeOfWorkout)
            
            self.view.bringSubview(toFront: IblblWorkoutReps)
            
            self.view.bringSubview(toFront: IBtblVideoList)
            
            //narendra
            self.view.bringSubview(toFront: lblTotalTimeOfWorkout)
            self.view.bringSubview(toFront: IBviewPlayerBottom)
            //narendra
            
            UIView.animate(withDuration: 0.5, animations: {
                
            }, completion: { (success) in
                
                self.IBviewEndWorkout.isHidden = true
                
                if self.isWaitingTimeGoingOn == true{
                    self.IblblWorkoutReps.isHidden = true
                    self.view.sendSubview(toBack: self.IblblWorkoutReps)
                    self.viewWaitingBlack.isHidden = false
                    self.lblCountDownWaiting.isHidden = false
                    
                    self.view.bringSubview(toFront: self.lblRest)
                    self.waitingLabelUpdate(text: self.lblCountDownWaiting)
                    
                    self.waitingTimerBetweenVideo = Timer.scheduledTimer(timeInterval: TimeInterval(self.iWaitingLabelText + 1), target: self, selector: #selector(self.waitingPeriod), userInfo: nil, repeats: false)
                    
                    self.startTotalTimeTimer()
                }
                else{
                    //narendra
                    //                    self.progressBar.isHidden = self.arrModelVideoList[self.currentIndexPlay].isSetOfReps
                    self.progressBar.isHidden = false
                    //narendra
                    self.player.seek(to: self.playerCurrentSeekValue)
                    self.playerLayer.player?.play()
                    
                    self.startProgressTimerAndTimeValue()
                }
            })
        }
        
        IBbtnPause.isSelected = !sender.isSelected
        IBbtnPlayEndWorkout.isSelected = IBbtnPause.isSelected
    }
    
    //MARK:- WebService methods
    
    /**
     This method is used to make excercise favourite.
     */
    func callToMakeExcerciseFavourite(){
        
        arrModelVideoList[currentIndexPlay].isWorkoutFav = !arrModelVideoList[currentIndexPlay].isWorkoutFav
        
        guard let cUser = DoviesGlobalUtility.currentUser else {return}
        var dic = [String: Any]()
        dic[WsParam.authCustomerId] = cUser.customerAuthToken
        dic[WsParam.moduleId] = arrModelVideoList[currentIndexPlay].workoutExercisesId
        dic[WsParam.moduleName] = ModuleName.exercise
        
        DoviesWSCalls.callFavouriteAPI(dicParam: dic, onSuccess: {[weak self] (success, message, responseDict) in
            guard let weakSelf = self else {return}
            if !success{
                weakSelf.arrModelVideoList[weakSelf.currentIndexPlay].isWorkoutFav = !weakSelf.arrModelVideoList[weakSelf.currentIndexPlay].isWorkoutFav
                weakSelf.IBbtnFav.isSelected = !weakSelf.IBbtnFav.isSelected
            }
        }) { (error) in
        }
    }
}

extension FeaturedWorkOutVideoPlayViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrModelVideoList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ExerciseListingTableCell") as? ExerciseListingTableCell
        cell?.setUpView(indexPath: indexPath, currentIndexPlay: currentIndexPlay, modelVideoList: arrModelVideoList[indexPath.row])
        cell?.selectionStyle = .none
        if indexPath.row == self.currentIndexPlay{
            currentOngoingCell = cell
            currentOngoingCell?.IBlblTime.text = self.arrModelVideoList[self.currentIndexPlay].sVideoInSeconds
            currentOngoingCell?.IBviewProgress.frame = CGRect(x: 0, y: yPos, width: self.forwardX, height: cellProgressViewHeight)
            if isWaitingTimeGoingOn == true{
                currentOngoingCell?.IBviewBreakTime.isHidden = false
            }
            
        }
        cell?.exerciseDoneTapped = { [weak self] in
            if self?.arrModelVideoList[indexPath.row].isSetOfReps == true{
                if self?.currentIndexPlay == (self?.arrModelVideoList.count)! - 1{
                    self?.startWaitingTimer()
                }
                else{
                    self?.didSelectAndChangeVideo(tableView: tableView, indexPath: IndexPath(row: indexPath.row + 1, section: indexPath.section))
                }
            }
        }
        return cell!
    }
}

extension FeaturedWorkOutVideoPlayViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.view.sendSubview(toBack: lblRest)
        
        if currentIndexPlay == indexPath.row{
            return
        }
        self.didSelectAndChangeVideo(tableView: tableView, indexPath: indexPath)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let videoModel = arrModelVideoList[indexPath.row]
        return (videoModel.isBreakTimeThere && indexPath.row < arrModelVideoList.count-1) ? 109 : 70
    }
}

extension FeaturedWorkOutVideoPlayViewController: MPMediaPickerControllerDelegate{
    func mediaPicker(_ mediaPicker: MPMediaPickerController, didPickMediaItems mediaItemCollection: MPMediaItemCollection) {
        if mediaItemCollection.items.count > 0 {
            musicPlayer.setQueue(with: mediaItemCollection)
            DoviesGlobalUtility.isMusicAdded = true
            print("Music added to Queue successfully..")
        }
        mediaPicker.dismiss(animated: true) {
            
        }
    }
    
    func mediaPickerDidCancel(_ mediaPicker: MPMediaPickerController) {
        mediaPicker.dismiss(animated: true) {
            
        }
    }
}

extension FeaturedWorkOutVideoPlayViewController {
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation == .landscapeLeft
        {
            if islandscapeRestTime == true
            {
                lblCountDownWaiting.font = UIFont.futuraBold(size: 70)
            }
            lblRest.font = UIFont.futuraBold(size: 50)
            
            imageBottomShade.isHidden = false
            // 14 screen
            topShadeView.isHidden = false

            progressBar.progressTintColors = [UIColor.clear, UIColor.clear]
            progressBar.trackTintColor = UIColor.clear
            
        }
        else if UIDevice.current.orientation == .portrait
        {
            if islandscapeRestTime == true
            {
                lblCountDownWaiting.font = UIFont.futuraBold(size: 35)
            }
            lblRest.font = UIFont.futuraBold(size: 25)
            // 14 screen
            topShadeView.isHidden = true

        }
        
        if UIDevice.current.orientation.isLandscape {
            ConsRestTopSpacing.constant = (ScreenSize.width / 2) - 95
            // loks
            if IS_IPHONE_X  || IS_IPHONE_XSMAX_XR {
                IBbottomHeight.constant =  57
                ConsLeadEndworkoutMusinc.constant = 29.5
                ConsTrailEndworkoutScreen.constant = 29.5
                ConsTopEndworkoutMusinc.constant = 4.5
                ConsTopEndworkoutScreen.constant = 4.5
                if IS_IPHONE_XSMAX_XR
                {
                    constrailing_lblworkoutReps.constant = 42
                }
                else
                {
                    constrailing_lblworkoutReps.constant = 35
                }
            }
            else
            {
                ConsLeadEndworkoutMusinc.constant = 29
                ConsTrailEndworkoutScreen.constant = 29
                ConsTopEndworkoutMusinc.constant = 0.5
                ConsTopEndworkoutScreen.constant = 0.5
            }
            
            //narendra
            self.IBviewVideoPreview.isHidden =  true
            self.consTopEndWorkout.constant = -(self.view.frame.size.width)
            //self.IBviewPlayerBottom.backgroundColor = UIColor.clear
            self.IBviewPlayerBottom.isHidden =  true
            //narendra
            IBbtn_Fullexit.setImage(UIImage(named: "exitfull_screenNewWhite"), for: .normal)
            IBbtnInfo.setImage(UIImage(named: "exitfull_screenNewWhite"), for: .normal)
            print("Landscape")
            videoPlayer.bringSubview(toFront: progressBar)
            IBplayerView.frame = CGRect(x: 0, y: 0, width: size.width , height: UIScreen.main.bounds.width)
            IBtblVideoList.isHidden = true
            //IBviewPlayerBottom.isHidden = false
            IBviewResumBottom.isHidden = false
            IBviewResumeLine.isHidden = false
            //lblTotalTimeOfWorkout.font = UIFont.futuraBold(size: 30.0)
            
            if IS_IPHONE_X ||  IS_IPHONE_XSMAX_XR{
                
                // 13 screen
              //  IBviewEndWorkout.backgroundColor = UIColor.white
                IBviewEndWorkout.backgroundColor = UIColor.black
                IBplayerView.backgroundColor = UIColor.black
                if IS_IPHONE_XSMAX_XR
                {
                    IBconXTime.constant = 48
                    consLeadEndWorkout.constant = 36
                    consTrailEndWorkout.constant = -36
                    
                    consLeadPlayerOption.constant = 36
                    consTrailPlayerOption.constant = -36
                    
                    consBottomPlayerOption.constant = 20
                    consHeightPlayerOption.constant = 66
                    
                }
                else
                {
                    IBconXTime.constant = 40
                    consLeadEndWorkout.constant = 28
                    consTrailEndWorkout.constant = -28
                    
                    consLeadPlayerOption.constant = 28
                    consTrailPlayerOption.constant = -28
                    
                    consBottomPlayerOption.constant = 20
                    consHeightPlayerOption.constant = 56
                    
                }
            }
            
            // 13 screen
        //    IBviewEndWorkout.backgroundColor = UIColor.colorWithRGB(r: 255/255.0, g: 255/255.0, b: 255/255.0, alpha: 0.2)
            
            IBviewEndWorkout.backgroundColor = UIColor.colorWithRGB(r: 0/255.0, g: 0/255.0, b: 0/255.0, alpha: 0.15)

            IblblWorkoutTotalTime.textColor = UIColor.white
            IblblWorkoutName.textColor = UIColor.white
          
            // loks add code 24 dec 2019
            if IS_IPHONE_XSMAX_XR
            {
                self.topShadeView.frame.origin.x = IS_IPHONE_X ? topSpace + 35 : 0
            }
            else
            {
                self.topShadeView.frame.origin.x = IS_IPHONE_X ? topSpace + 29 : 0
            }
            
            
            self.btnPauseWorkout.setBackgroundImage(UIImage(named: "resume_icon_s_46a"), for: .normal)
            
            IBplayerView.clipsToBounds = true
            self.videoPlayer.frame = IBplayerView.frame
            
            IBconHeight.constant = UIScreen.main.bounds.width
            
            self.shouldHideHomeIndicator = true
            if #available(iOS 11.0, *) {
                self.setNeedsUpdateOfHomeIndicatorAutoHidden()
            } else {
            }
            

            
        }
        else
        {
            topShadeView.isHidden = true
            ConsRestTopSpacing.constant = 45
            if IS_IPHONE_X{
                IBbottomHeight.constant = 81.8
                constrailing_lblworkoutReps.constant = 10
            }
            
            ConsLeadEndworkoutMusinc.constant = 0
            ConsTrailEndworkoutScreen.constant = 0
            ConsTopEndworkoutMusinc.constant = 0
            ConsTopEndworkoutScreen.constant = 0
            
            IBbtnInfo.setImage(UIImage(named: "full_screenNewWhite"), for: .normal)
            IBbtn_Fullexit.setImage(UIImage(named: "full_screenNewWhite"), for: .normal)
            
            //narendra
            self.consTopEndWorkout.constant = 0
            //self.IBviewPlayerBottom.backgroundColor = UIColor.white
            //narendra
            
            self.shouldHideHomeIndicator = false
            if #available(iOS 11.0, *) {
                self.setNeedsUpdateOfHomeIndicatorAutoHidden()
            } else {
                // Fallback on earlier versions
            }
            print("Portrait")
            
            // lokendra minddiii 10 0ct 2019
            if IS_IPHONE_X
            {
                IBplayerView.frame = CGRect(x: 0, y: topSpace, width: size.width, height: playerHeight)
            }
            else
            {
                IBplayerView.frame = CGRect(x: 0, y: 0, width: size.width, height: playerHeight)
            }
            IBtblVideoList.isHidden = false
            IBviewPlayerBottom.isHidden = false
            IBviewResumBottom.isHidden = false
            IBviewResumeLine.isHidden = false
            
            IBconHeight.constant = playerHeight
            if IS_IPHONE_X{
            // 13 screen
             //   IBviewEndWorkout.backgroundColor = UIColor.white
                IBviewEndWorkout.backgroundColor = UIColor.black
                IBplayerView.backgroundColor = UIColor.clear
                IBconXTime.constant = 10
                consLeadEndWorkout.constant = 0
                consTrailEndWorkout.constant = 0
                consLeadPlayerOption.constant = 0
                consTrailPlayerOption.constant = 0
                consBottomPlayerOption.constant = 0
                consHeightPlayerOption.constant = 48
            }
            // 13 screen
          //  IBviewEndWorkout.backgroundColor = UIColor.white
            IBviewEndWorkout.backgroundColor = UIColor.black

            IBviewEndWorkout.alpha = 1.0
            
         // 13 screen
//            IblblWorkoutTotalTime.textColor = UIColor.black
//            IblblWorkoutName.textColor = UIColor.gray
            
            IblblWorkoutTotalTime.textColor = UIColor.white
            IblblWorkoutName.textColor = UIColor.white

            self.topShadeView.frame.origin.x = 0
            
            self.btnPauseWorkout.setBackgroundImage(UIImage(named: "resume_icon_s_46a"), for: .normal)
            
            if IBviewVideoPreview.isHidden == false{
                videoPlayer.frame = CGRect(x: IBplayerView.frame.origin.x, y: 0, width: IBplayerView.frame.size.width, height: IBplayerView.frame.size.height)
            }
        }
        
        for view in IBplayerView.subviews{
            if view.isKind(of: VideoContainerView.self){
                let w =  UIDevice.current.orientation.isLandscape ? IBplayerView.frame.size.width  : IBplayerView.frame.size.width
                
                view.frame = CGRect(x: IBplayerView.frame.origin.x  , y: 0, width: w , height: IBplayerView.frame.size.height)
            }
        }
        if lblCountDownWaiting != nil{
            lblCountDownWaiting.center = IBplayerView.center
        }
        playerLayer.frame =  IBplayerView.frame
        
        // 14 screen
        //self.topShadeView.frame.size.height = playerHeight
        
        self.topShadeView.frame.size.height = 80

    }
}

//**** narendra -added to hide bottom bar if sceeen is idle
extension FeaturedWorkOutVideoPlayViewController{
    
    @objc func actionTapOnVideoPlayer() {
        
        guard UIApplication.shared.statusBarOrientation.isLandscape else {
            return
        }
        // 13 screen
        IBviewPlayerBottom.backgroundColor = UIColor.clear
        IBviewPlayerBottom.isHidden = false
        hideBottomView()
        //resetIdleTimer()
    }
    
    func hideBottomView() {
        
        //        UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
        //            self.IBviewPlayerBottom.isHidden = true
        //        })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            UIView.transition(with: self.view, duration: 0.5, options: .transitionCrossDissolve, animations: {
                if UIApplication.shared.statusBarOrientation.isLandscape{
                    self.IBviewPlayerBottom.isHidden = true
                }
            })
        }
    }
    
    func resetIdleTimer() {
        if idleTimer == nil{
            idleTimer = Timer.scheduledTimer(timeInterval: TimeInterval(kMaxIdleTimeSeconds), target: self, selector: #selector(self.idleTimerExceeded), userInfo: nil, repeats: false)
        } else {
            if fabs(Float((idleTimer?.fireDate.timeIntervalSinceNow)!)) < kMaxIdleTimeSeconds - 1.0 {
                idleTimer?.fireDate = Date(timeIntervalSinceNow: TimeInterval(kMaxIdleTimeSeconds))
            }
        }
    }
    
    @objc func idleTimerExceeded() {
        idleTimer = nil
        hideBottomView()
        resetIdleTimer()
    }
    
    override var next: UIResponder?{
        ///resetIdleTimer()
        return super.next
    }
    //**** narendra
    
    func addViewForVideoInfo() {
        
        
        // 14 screen
        //  let hViewTimeAndName: CGFloat = 134

        let hViewTimeAndName: CGFloat = 145

        let wViewTimeAndName: CGFloat = progressBar.frame.size.width
        let xViewTimeAndName: CGFloat = 0
        let yViewTimeAndName: CGFloat = hViewTimeAndName
        
        let wLabelTime: CGFloat = 130
        let hLabelTime: CGFloat = hViewTimeAndName
        let xLabelTime: CGFloat = 10
        let yLabelTime: CGFloat = -10
        
        viewForLandScapeTimeAndName = UIView(frame: CGRect(x: xViewTimeAndName , y: -yViewTimeAndName+2 , width: wViewTimeAndName, height: hViewTimeAndName))
        viewForLandScapeTimeAndName.backgroundColor = UIColor.clear
        
        if IS_IPHONE_XSMAX_XR
        {
            imageBottomShade = UIImageView(frame: CGRect(x: 6.5 , y: 0 , width: viewForLandScapeTimeAndName.frame.size.width - 13, height: viewForLandScapeTimeAndName.frame.size.height))

        }
        else
        {
            imageBottomShade = UIImageView(frame: CGRect(x: 0 , y: 0 , width: viewForLandScapeTimeAndName.frame.size.width, height: viewForLandScapeTimeAndName.frame.size.height))
        }
        
        // 14 screen
         // imageBottomShade.image = UIImage(named: "bottom-shade2.png")
       //24 dec imageBottomShade.image = UIImage(named: "video_overlay_new.png")

        
       // let viewGradient =  UIView(frame: CGRect(x: 0, y: (ScreenSize.width - 40)/2, width:ScreenSize.width - 40, height: (ScreenSize.width - 40)/2))
        
        let viewGradient =  UIView(frame: CGRect(x: 0, y: 30, width:ScreenSize.height, height: 150))

        let gradientLayer:CAGradientLayer = CAGradientLayer()
        gradientLayer.frame.size =  viewGradient.frame.size
        gradientLayer.colors = [UIColor.clear.cgColor,UIColor.appBlackColorNew().withAlphaComponent(0.3).cgColor]
        viewGradient.layer.addSublayer(gradientLayer)
       imageBottomShade.addSubview(viewGradient)

        
        imageBottomShade.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        imageBottomShade.isHidden = false
        imageBottomShade.contentMode = .scaleToFill
        isimageBottomShadeStatus = true
        viewForLandScapeTimeAndName.addSubview(imageBottomShade)
        progressBar.addSubview(viewForLandScapeTimeAndName)
        
        if IS_IPHONE_XSMAX_XR
        {
            lblTimeLandScape = UILabel(frame: CGRect(x: xLabelTime + 8, y: yLabelTime , width: wLabelTime, height: hLabelTime))
            
        }
        else
        {
            lblTimeLandScape = UILabel(frame: CGRect(x: xLabelTime, y: yLabelTime, width: wLabelTime, height: hLabelTime))
        }
        
        lblTimeLandScape.textAlignment = .left
        lblTimeLandScape.font = UIFont.futuraBold(size: 50.0)
        lblTimeLandScape.adjustsFontSizeToFitWidth = true
        lblTimeLandScape.text = self.arrModelVideoList[self.currentIndexPlay].sVideoInSeconds
        
        lblTimeLandScape.textColor = UIColor.white
        
        viewForLandScapeTimeAndName.addSubview(lblTimeLandScape)
        
        
        // 14 screen
        //    lblLandScapeExName = UILabel(frame: CGRect(x: wLabelTime + 10, y: 0, width: wViewTimeAndName - wLabelTime, height: hLabelTime))

        
        lblLandScapeExName = UILabel(frame: CGRect(x: wLabelTime + 10, y: -4, width: wViewTimeAndName - wLabelTime, height: hLabelTime))

        
        lblLandScapeExName.textAlignment = .left
        lblLandScapeExName.font = UIFont.tamilBold(size: 22.0)
        lblLandScapeExName.text = arrModelVideoList[currentIndexPlay].sVideoName
        //lblLandScapeExName.textColor = UIColor.colorWithRGB(r: 34, g: 34, b: 34)
        lblLandScapeExName.textColor = UIColor.white
        viewForLandScapeTimeAndName.addSubview(lblLandScapeExName)
        
    }
    
    
    func addViewForTopShadeOnVideo() {
        let hViewTimeAndName: CGFloat = IBplayerView.frame.size.height
        let wViewTimeAndName: CGFloat = progressBar.frame.size.width
        
        //  14 screen      let yViewTimeAndName: CGFloat = -2
        
        let yViewTimeAndName: CGFloat = -6

        var xTop =  UIDevice.current.orientation.isLandscape ? IS_IPHONE_X ? topSpace + 29 : 0 : 0
        
        // loks add code below  24 dec 2019
        if self.view.frame.size.width > 414
        {
            if IS_IPHONE_XSMAX_XR
            {
                xTop = topSpace + 36
                
            }
            else if IS_IPHONE_X
            {
                xTop = topSpace + 29
            }
        }
        
        
        // 14 screen
        //self.topShadeView = UIView(frame: CGRect(x: xTop , y: yViewTimeAndName , width: wViewTimeAndName, height: hViewTimeAndName))
        
        
        if IS_IPHONE_XSMAX_XR
        {
            self.topShadeView = UIView(frame: CGRect(x: xTop , y: yViewTimeAndName , width: wViewTimeAndName - 13, height: 80))

        }
        else
        {
            self.topShadeView = UIView(frame: CGRect(x: xTop , y: yViewTimeAndName , width: wViewTimeAndName, height: 80))
        }
        

        topShadeView.backgroundColor = UIColor.clear
        
        let imageBottomShade = UIImageView(frame: CGRect(x: 0 , y: 0 , width: topShadeView.frame.size.width, height: topShadeView.frame.size.height  ))
        
        
        // 14 screen  imageBottomShade.image = UIImage(named: "bottom-shade_top.png")
      
        let viewGradient =  UIView(frame: CGRect(x: 0, y: 0, width:topShadeView.frame.size.width , height: 300))

        let gradientLayer:CAGradientLayer = CAGradientLayer()
        gradientLayer.frame.size =  viewGradient.frame.size
        gradientLayer.colors = [UIColor.clear.cgColor,UIColor.black.withAlphaComponent(0.3).cgColor]

        gradientLayer.startPoint = CGPoint(x: 1.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.0)

        viewGradient.layer.addSublayer(gradientLayer)

        imageBottomShade.removeFromSuperview()
        imageBottomShade.addSubview(viewGradient)

  // 23 dec      imageBottomShade.image = UIImage(named: "bottom-shade_top.png")
        
     //   imageBottomShade.image = UIImage(named: "video_overlay_TopNew.png")

        imageBottomShade.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        imageBottomShade.contentMode = .scaleToFill
        
        topShadeView.addSubview(imageBottomShade)
        // 14 screen
        videoPlayer.addSubview(topShadeView)
        
        print("UIDevice.current.orientationggg00    \(UIDevice.current.orientation)")
        
        
        if self.view.frame.size.width > 414
        {
            topShadeView.isHidden = false
        }
        else
        {
            topShadeView.isHidden = true
        }
        
//        if UIDevice.current.orientation == .landscapeLeft
//        {
//
//            if self.view.frame.size.width > 414
//            {
//                topShadeView.isHidden = false
//            }
//            else
//            {
//                topShadeView.isHidden = true
//            }
//        }
//        else if UIDevice.current.orientation == .portrait
//        {
//            topShadeView.isHidden = true
//        }
        //videoPlayer.addSubview(topShadeView)
    }
}

class VideoContainerView: UIView {
    var playerLayer: CALayer?
    override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
        playerLayer?.frame = self.bounds
    }
}

extension AVPlayer {
    
    var isPlaying: Bool {
        return ((rate != 0) && (error == nil))
    }
}


