//
//  MusicPlayListViewController.swift
//  Dovies
//
//  Created by Neel Shah on 09/06/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import MediaPlayer

class MusicPlayListViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
		self.getPlayListFromDevice()
        // Do any additional setup after loading the view.
    }
	
	func getPlayListFromDevice()
	{
		let arrPlayListDevice = NSMutableArray()
		
		let authorizationStatus = MPMediaLibrary.authorizationStatus()
		switch authorizationStatus {
		case .notDetermined:
			// Show the permission prompt.
			MPMediaLibrary.requestAuthorization({[weak self] (newAuthorizationStatus: MPMediaLibraryAuthorizationStatus) in
				// Try again after the prompt is dismissed whether to check Media permission
				self?.getPlayListFromDevice()
			})
			return
		case .denied, .restricted:
			// If used has denied or restricted the Media permission and calling to sync Off line created playlist.
			return
		default:
			let allPlaylistArray = MPMediaQuery.playlists().collections!
			for playlist in allPlaylistArray {
				arrPlayListDevice.add(playlist)
			}
			
			let allArtist = MPMediaQuery.artists().collections!
			for playlist in allArtist {
				print(playlist.representativeItem?.value(forProperty: MPMediaItemPropertyArtist) ?? "")
				
				
			}
			
			let allAlbums = MPMediaQuery.albums().collections!
			for collection in allAlbums {
				print(collection.representativeItem?.value(forProperty: MPMediaItemPropertyAlbumTitle) ?? "")
				
				let item: MPMediaItem? = collection.representativeItem
				
				let albumName = item?.value(forKey: MPMediaItemPropertyAlbumTitle) as? String ?? "<Unknown>"
				let albumId = item!.value(forProperty: MPMediaItemPropertyAlbumPersistentID) as! NSNumber
				let artistName = item?.value(forKey: MPMediaItemPropertyArtist) as? String ?? "<Unknown>"
				
				print("Album name: \(albumName)")
				print("Artist name: \(artistName)")
				
				// Get all songs in this album
				let mediaQuery = MPMediaQuery.songs()
				let predicate = MPMediaPropertyPredicate.init(value: albumId, forProperty: MPMediaItemPropertyAlbumPersistentID)
				mediaQuery.addFilterPredicate(predicate)
				let song = mediaQuery.items
				
				if let allSongs = song {
					
					for item in allSongs {
						let pathURL: URL? = item.value(forProperty: MPMediaItemPropertyAssetURL) as? URL
						if pathURL == nil {
							print("@Warning!!! Track : \(item) is not playable.")
						} else {
							let title = item.value(forProperty: MPMediaItemPropertyTitle) as? String ?? "<Unknown>"
							let artistName = item.value(forProperty: MPMediaItemPropertyArtist) as? String ?? "<Unknown>"
							print("Song title: \(title)")
							print("Song artistName: \(artistName)")
						}
					}
				}
			}
			break
		}
		
		if arrPlayListDevice.count > 0
		{
			for playlist in arrPlayListDevice {
				
				let playListNew = playlist as! MPMediaPlaylist
				let songs = playListNew.items
				for song in songs {
					let songTitle = song.value(forProperty: MPMediaItemPropertyTitle)
					print("\t\t", songTitle!)
				}
			}
		}
		else{
			let lblEmptyPlaceholder = UILabel(frame: CGRect(x: 0, y: 0, width: Int(self.view.frame.size.width), height: Int(self.view.frame.size.height)))
			lblEmptyPlaceholder.text = "No playlist found."
			lblEmptyPlaceholder.textColor = UIColor.gray
			lblEmptyPlaceholder.textAlignment = .center
		}
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
