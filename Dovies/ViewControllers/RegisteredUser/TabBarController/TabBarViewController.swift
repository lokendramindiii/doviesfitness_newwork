//
//  TabBarViewController.swift
//  Dovies
//
//  Created by Neel Shah on 16/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class TabBarViewController: UITabBarController, UITabBarControllerDelegate {

    
    override func viewDidLoad(){
        super.viewDidLoad()
        // Do any additional setup after loading the view.
       
        var lblTopTabbarLine = UILabel()
        lblTopTabbarLine = UILabel(frame: CGRect(x: 0 , y: -0.3 , width: self.tabBar.frame.size.width, height: 0.4))
        
        lblTopTabbarLine.backgroundColor = UIColor.colorWithRGB(r: 33, g: 33, b: 33, alpha: 0.5)
        self.tabBar.addSubview(lblTopTabbarLine)


//        let tabBar = self.tabBar
//        tabBar.backgroundImage = UIImage(named: "")
//        tabBar.clipsToBounds = true
        
        

//        let blurredView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
//        blurredView.frame = self.view.bounds
//        tabBar.addSubview(blurredView)

        
        
//
//        let backView = UIView(frame: self.view.bounds)
//        backView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4)
//        tabBar.addSubview(backView)
        
        
        // Create a blur effect
//        let blurEffect = UIBlurEffect(style: .dark)
//        let blurEffectView = UIVisualEffectView(effect: blurEffect)
//        blurEffectView.frame = self.view.bounds
//        tabBar.addSubview(blurEffectView)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if APP_DELEGATE.selectedTabbarIndex == 4
        {
            self.selectedIndex = 4
            APP_DELEGATE.selectedTabbarIndex  = 0
        }

    }
    
    
    override   func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        NotificationCenter.default.post(name: Notification.Name("Notification_TabbaritemClick"), object: nil, userInfo: nil)

    }
	// UITabBarControllerDelegate method
	func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
		
	}
}

class SafeAreaFixTabBar: UITabBar {
	
	var oldSafeAreaInsets = UIEdgeInsets.zero
	
	@available(iOS 11.0, *)
	override func safeAreaInsetsDidChange() {
		super.safeAreaInsetsDidChange()
		
		if oldSafeAreaInsets != safeAreaInsets {
			oldSafeAreaInsets = safeAreaInsets
			
			invalidateIntrinsicContentSize()
			superview?.setNeedsLayout()
			superview?.layoutSubviews()
		}
	}
	
	override func sizeThatFits(_ size: CGSize) -> CGSize {
		var size = super.sizeThatFits(size)
		if #available(iOS 11.0, *) {
			let bottomInset = safeAreaInsets.bottom
			if bottomInset > 0 && size.height < 50 && (size.height + bottomInset < 90) {
				size.height += bottomInset
			}
		}
		return size
	}
	
	override var frame: CGRect {
		get {
			return super.frame
		}
		set {
			var tmp = newValue
			if let superview = superview, tmp.maxY !=
				superview.frame.height {
				tmp.origin.y = superview.frame.height - tmp.height
			}
			super.frame = tmp
		}
	}
}
