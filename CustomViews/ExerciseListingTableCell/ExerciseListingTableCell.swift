//
//  ExerciseListingTableCell.swift
//  Dovies
//
//  Created by Neel Shah on 19/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class ExerciseListingTableCell: UITableViewCell {
	
	@IBOutlet weak var IBviewBg: UIView!
	@IBOutlet weak var IBviewProgress: UIView!
	@IBOutlet weak var IBlblTime: UILabel!
	@IBOutlet weak var IBlblExerciseName: UILabel!
	var index: IndexPath!
	@IBOutlet weak var IBlblRepeatMode: UILabel!
	@IBOutlet weak var IBbtnWorkOutDone: UIButton!
	@IBOutlet weak var lblBreakTime: UILabel!
    
    @IBOutlet weak var constBreakTimeViewHeight: NSLayoutConstraint!
    @IBOutlet weak var breakTimeStack: UIStackView!
    
	@IBOutlet weak var IBviewBreakTime: UIView!
    @IBOutlet weak var viewMainBg: UIView!

    
	var exerciseDoneTapped:(()->())?

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
    func setUpView(indexPath: IndexPath, currentIndexPlay: Int, modelVideoList: ModelVideoListing)
    {
        
        IBviewProgress.isHidden = true
        IBviewBreakTime.isHidden = true
        IBbtnWorkOutDone.tag = indexPath.row
        
        self.contentView.backgroundColor = UIColor.black
        IBviewBg.backgroundColor = UIColor.black
    //0000    IBviewBreakTime.backgroundColor = UIColor.black
        IBlblExerciseName.textColor = UIColor.colorWithRGB(r: 162, g: 162, b: 162)
        IBlblTime.textColor = UIColor.white
        if indexPath.row < currentIndexPlay
        {
            IBlblRepeatMode.textColor = UIColor.colorWithRGB(r: 162, g: 162, b: 162)
            IBbtnWorkOutDone.isSelected = true
        }
        else
        {
            IBbtnWorkOutDone.isSelected = false
            if indexPath.row == currentIndexPlay
            {
                IBviewProgress.isHidden = false
                IBlblExerciseName.textColor = UIColor.white

                IBlblRepeatMode.textColor = UIColor.white
              //000  self.contentView.backgroundColor = UIColor.appBlackColorNew()
               IBviewBg.backgroundColor = UIColor.appBlackColorSplash()
                
              //000  IBviewBreakTime.backgroundColor = UIColor.appBlackColorNew()
             //   IBviewBreakTime.backgroundColor = UIColor.green

            }
            else
            {
                 IBlblRepeatMode.textColor = UIColor.colorWithRGB(r: 162, g: 162, b: 162)
         //ooo       IBviewBreakTime.backgroundColor = UIColor.black
                
            //     IBviewBreakTime.backgroundColor = UIColor.green
            }
        }
        IBlblExerciseName.text = modelVideoList.sVideoName
        hmsFrom(seconds: modelVideoList.fBreakTime) { hours, minutes, seconds in
            
            let sHours = self.getStringFrom(seconds: hours)
            let sMinutes = self.getStringFrom(seconds: minutes)
            let sSeconds = self.getStringFrom(seconds: seconds)
            if hours > 0
            {
                self.lblBreakTime.text = "\(sHours):\(sMinutes):\(sSeconds)" + " Hour"
            } else if minutes > 0 {
                self.lblBreakTime.text = "\(sMinutes):\(sSeconds)" + " Minutes"
            }else if seconds > 0{
                self.lblBreakTime.text = "\(sSeconds)" + " Seconds"
            }
            if hours > 0 || minutes > 0 || seconds > 0{
                self.constBreakTimeViewHeight.constant = 38
                self.breakTimeStack.isHidden = false
            }else{
                self.constBreakTimeViewHeight.constant = 0
                self.breakTimeStack.isHidden = true
            }
        }
        
        if modelVideoList.workoutRepeatText == "01"
        {
            IBlblRepeatMode.text = modelVideoList.workoutRepeatText  + " Rep"
        }
        else
        {
            IBlblRepeatMode.text = modelVideoList.workoutRepeatText  + " Reps"
        }
        
        if modelVideoList.isSetOfReps == true{
            IBlblRepeatMode.isHidden = false
        }
        else{
            IBlblRepeatMode.isHidden = true
        }
        IBlblRepeatMode.font = UIFont.tamilRegular(size: 12.0)
        IBlblExerciseName.font = UIFont.tamilBold(size: 14.0)
        
        IBlblTime.text = modelVideoList.sVideoInSeconds
        IBlblTime.tag = 1000 + indexPath.row
        index = indexPath
    }
	
	@IBAction func btnExerciseDoneTap(){
		if exerciseDoneTapped != nil{
			exerciseDoneTapped?()
		}
	}
	
	func hmsFrom(seconds: Int, completion: @escaping (_ hours: Int, _ minutes: Int, _ seconds: Int)->()) {
		
		completion(seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
	}
	
	func getStringFrom(seconds: Int) -> String {
		return seconds < 10 ? "0\(seconds)" : "\(seconds)"
	}
}
