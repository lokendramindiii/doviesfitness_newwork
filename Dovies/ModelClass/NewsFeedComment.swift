//
//  NewsFeedComment.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © 2018 CompanyName. All rights reserved.
//

import UIKit

//This class is used to store news feed comments list
class NewsFeedComment: NSObject, NSCoding {
    
    var customerId : String!
    var customerName : String!
    var customerProfileImage : String!
    var newsComment : String!
    var newsCommentDeleteAccess : String!
    var newsCommentPostedDays : String!
    var newsCommentsId : String!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        customerId = dictionary["customer_id"] as? String ?? ""
        customerName = dictionary["customer_name"] as? String ?? ""
        customerProfileImage = dictionary["customer_profile_image"] as? String ?? ""
        newsComment = dictionary["news_comment"] as? String ?? ""
        newsCommentDeleteAccess = dictionary["news_comment_delete_access"] as? String ?? ""
        newsCommentPostedDays = dictionary["news_comment_posted_days"] as? String ?? ""
        newsCommentsId = dictionary["news_comments_id"] as? String ?? ""
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if customerId != nil{
            dictionary["customer_id"] = customerId
        }
        if customerName != nil{
            dictionary["customer_name"] = customerName
        }
        if customerProfileImage != nil{
            dictionary["customer_profile_image"] = customerProfileImage
        }
        if newsComment != nil{
            dictionary["news_comment"] = newsComment
        }
        if newsCommentDeleteAccess != nil{
            dictionary["news_comment_delete_access"] = newsCommentDeleteAccess
        }
        if newsCommentPostedDays != nil{
            dictionary["news_comment_posted_days"] = newsCommentPostedDays
        }
        if newsCommentsId != nil{
            dictionary["news_comments_id"] = newsCommentsId
        }
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        customerId = aDecoder.decodeObject(forKey: "customer_id") as? String
        customerName = aDecoder.decodeObject(forKey: "customer_name") as? String
        customerProfileImage = aDecoder.decodeObject(forKey: "customer_profile_image") as? String
        newsComment = aDecoder.decodeObject(forKey: "news_comment") as? String
        newsCommentDeleteAccess = aDecoder.decodeObject(forKey: "news_comment_delete_access") as? String
        newsCommentPostedDays = aDecoder.decodeObject(forKey: "news_comment_posted_days") as? String
        newsCommentsId = aDecoder.decodeObject(forKey: "news_comments_id") as? String
        
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if customerId != nil{
            aCoder.encode(customerId, forKey: "customer_id")
        }
        if customerName != nil{
            aCoder.encode(customerName, forKey: "customer_name")
        }
        if customerProfileImage != nil{
            aCoder.encode(customerProfileImage, forKey: "customer_profile_image")
        }
        if newsComment != nil{
            aCoder.encode(newsComment, forKey: "news_comment")
        }
        if newsCommentDeleteAccess != nil{
            aCoder.encode(newsCommentDeleteAccess, forKey: "news_comment_delete_access")
        }
        if newsCommentPostedDays != nil{
            aCoder.encode(newsCommentPostedDays, forKey: "news_comment_posted_days")
        }
        if newsCommentsId != nil{
            aCoder.encode(newsCommentsId, forKey: "news_comments_id")
        }
        
    }
    
    
}
