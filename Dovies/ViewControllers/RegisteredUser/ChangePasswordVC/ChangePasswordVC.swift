//
//  ChangePasswordVC.swift
//  Dovies
//
//  Created by hb on 19/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit


class ChangePasswordVC: BaseViewController {

    @IBOutlet var oldTxtfld     : DTTextField!
    @IBOutlet var newTxtfld     : DTTextField!
    @IBOutlet var reTypeTxtfld  : DTTextField!
    
    let validator               : Validator = Validator()

    //MARK: ViewLifeCycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Change Password".uppercased()
        self.navigationItem.hidesBackButton  = true
     //   self.addNavigationBackButton()
        self.addNavigationWhiteBackButton()

        //self.addNavigationRightButton()
        self.setFormValidators()
    }
    override func inititalUISetUp() {
        setFormValidators()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //MARK: IBAction methods
    
    @IBAction func btnUpdate(_ sender: Any) {
        
        let string = newTxtfld.text ?? ""
        print("psw --\(string)")
        if string == " "
        {
            newTxtfld.text = ""
        }
        else
        {
            newTxtfld.text = string.trimmingCharacters(in: .whitespaces)
        }
        print("finalpsd --\(newTxtfld.text)")

        validator.validate(self)
    }
    
    //MARK: - Instance methods
    
    func setFormValidators() {
        validator.styleTransformers(success:{ (validationRule) -> Void in
            if let textField = validationRule.field as? DTTextField {
                textField.hideError()
            }
        }, error:{ (validationError) -> Void in
            if let textField = validationError.field as? DTTextField {
                textField.showError(message: validationError.errorMessage)
            }
        })
        
//        validator.registerField(oldTxtfld, errorLabel: nil, rules: [RequiredRule(message: AlertMessages.OldPassword),MinLengthRule(length: 6, message: AlertMessages.validatePassword)])
//        validator.registerField(newTxtfld, errorLabel: nil, rules: [RequiredRule(message: AlertMessages.NewPassword),MinLengthRule(length: 6, message: AlertMessages.validatePassword)])
//
        
        validator.registerField(oldTxtfld, errorLabel: nil, rules: [RequiredRule(message: AlertMessages.OldPassword)])
        
        validator.registerField(newTxtfld, errorLabel: nil, rules: [RequiredRule(message: AlertMessages.NewPassword)])

        validator.registerField(reTypeTxtfld, errorLabel: nil, rules: [RequiredRule(message: AlertMessages.NewPasswordAgain), PasswordRule()])
        
        validator.registerField(reTypeTxtfld, errorLabel: nil , rules: [RequiredRule(message: AlertMessages.emptyRePassword), ConfirmationRule(confirmField: newTxtfld, message: AlertMessages.invalidatePasswordCheck)])
    }
    
    @objc func addNavigationRightButton(){
        let button: UIButton = UIButton(type: UIButtonType.custom)
        button.setImage(UIImage(named: "btn_twitter"), for: UIControlState.normal)
        let barButton = UIBarButtonItem(customView: button)
        //assign button to navigationbar
        self.navigationItem.rightBarButtonItem = barButton
    }
}
extension ChangePasswordVC : ValidationDelegate{
    /**
     This method is used to validate the old and new passwords and updates the new password to server.
     */
    func validationSuccessful() {
		GlobalUtility.showActivityIndi(viewContView: self.view)
		DoviesWSCalls.callChangePasswordAPI(dicParam: ["old_password": oldTxtfld.text!, "new_password" : newTxtfld.text!, "new_confirm_password" : reTypeTxtfld.text!], onSuccess: { (success, msg, dic) in
			GlobalUtility.hideActivityIndi(viewContView: self.view)
			if success{
				DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.passwordUpdate) {
					self.navigationController?.popViewController(animated:true)
				}
			}
			else{
				DoviesGlobalUtility.showSimpleAlert(viewController: self, message: msg) {
					
				}
			}
		}) { (error) in
			GlobalUtility.hideActivityIndi(viewContView: self.view)
		}
    }
    
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        
    }
}
