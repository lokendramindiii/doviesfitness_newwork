//
//  UserListCell.swift
//  Dovies
//
//  Created by Mindiii on 12/5/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class UserListCell: UITableViewCell {

    @IBOutlet var userList: UILabel!
    @IBOutlet var btnSelection: UIButton!
    
    var model:String?
    var indexPath : IndexPath!
    var obj : ModelUserlist?
    var obj_modelCreatedBy : ModelCreatedBy?
    var obj_ModelWorkoutCollection : ModelWorkoutCollection?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setDataUserlist(_ obj:ModelUserlist?,at indexPath : IndexPath){
        

        self.obj = obj
        userList.text = obj?.name.capitalizingFirstLetter()
        self.indexPath = indexPath
    }
    func setDataModelCreatedBy(_ obj:ModelCreatedBy?,at indexPath : IndexPath){
        self.obj_modelCreatedBy = obj
        userList.text = obj?.guestName.capitalizingFirstLetter()
        self.indexPath = indexPath
    }
    func setDataModelWorkoutCollection(_ obj:ModelWorkoutCollection?,at indexPath : IndexPath){
        self.obj_ModelWorkoutCollection = obj
        userList.text = obj?.groupName.capitalizingFirstLetter()
        self.indexPath = indexPath
    }

}
