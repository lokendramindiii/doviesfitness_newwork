//
//  FavoriteDietPlanVC.swift
//  Dovies
//
//  Created by Ananda on 19/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class FavoriteDietPlanVC: FavoriteBaseVC {
    
    @IBOutlet weak var collectionView : BaseCollectionView!
    @IBOutlet weak var IBviewExploreDiet: UIView!
    @IBOutlet weak var IBbottomCon: NSLayoutConstraint!
    
    //let cellSizeValue : CGFloat  = (ScreenSize.width - (ProgramsViewController.cellPadding))/2
    
    let cellSizeValue : CGFloat  = (ScreenSize.width - 1 )/2
    override func viewDidLoad() {
        super.viewDidLoad()
        if IS_IPHONE_X{
            IBbottomCon.constant = 74
        }
        self.collectionView.register(UINib(nibName: DietPlansListVC.cellIndentifier, bundle: nil), forCellWithReuseIdentifier: DietPlansListVC.cellIndentifier)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if FavoriteVC.currentSelectedSegment == 3{
            self.callGetCustomerFavouritesAPI(apiCallingType: FavouriteApiType.Diet.rawValue ,completion: {[weak self] (success) in
                guard let weakSelf = self else { return }
                if success {
                    weakSelf.collectionView.reloadData()
                }
                if weakSelf.favoriteLists?.dietFavourite == nil || weakSelf.favoriteLists?.dietFavourite.count == 0{
                    weakSelf.IBviewExploreDiet.isHidden = false
                }
                else{
                    weakSelf.IBviewExploreDiet.isHidden = true
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func redirectToDiet(){
        self.redirectToItsTab(tabIndex: 2)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dietPlanVc = segue.destination as? DietPlanDetailsVC {
            dietPlanVc.selectedPlan = sender as? DietPlan
        }
    }
}

extension FavoriteDietPlanVC : UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.favoriteLists?.dietFavourite.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DietPlansListVC.cellIndentifier, for: indexPath) as? DietPlansListCell  else{return UICollectionViewCell()}
        
        if let arrDietPlan = self.favoriteLists?.dietFavourite {
            cell.setFavCellUI(with: arrDietPlan[indexPath.item])
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let arrDietPlan = self.favoriteLists?.dietFavourite {
            if arrDietPlan[indexPath.row].dietPlanAccessLevel == "LOCK" {
                self.performSegue(withIdentifier: "favoriteDietPlanToDetail", sender: arrDietPlan[indexPath.item])
            }
            else if arrDietPlan[indexPath.row].dietAddedInMyDiet{
                let pdfViewer = Storyboard.DietPlan.storyboard().instantiateViewController(withIdentifier: "DietPdfViewController") as! DietPdfViewController
                pdfViewer.strTitle = arrDietPlan[indexPath.item].dietPlanTitle
                pdfViewer.strUrl = arrDietPlan[indexPath.item].dietPlanPdf
                self.navigationController?.pushViewController(pdfViewer, animated: true)
            }
            else{
                self.performSegue(withIdentifier: "favoriteDietPlanToDetail", sender: arrDietPlan[indexPath.item])
            }
        }
    }
}

extension FavoriteDietPlanVC: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellSizeValue, height: cellSizeValue)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: DietPlansListVC.cellPadding, right: 0)
    }
    
}


