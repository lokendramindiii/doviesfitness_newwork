//
//  GroupDetailsCollectionCell.swift
//  Dovies
//
//  Created by HB-PC on 13/06/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class GroupDetailsCollectionCell: UICollectionViewCell {

    @IBOutlet weak var imgWorkout : UIImageView!

    @IBOutlet weak var lblName : UILabel!
    @IBOutlet weak var lblDesc : UILabel!
    @IBOutlet var btnLock: UIButton!
    @IBOutlet weak var btnNew : UIButton!
    
    @IBOutlet weak var viewBackground : UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        let path = UIBezierPath(roundedRect:btnNew.bounds,
                                byRoundingCorners:[.topLeft,.bottomRight],
                                cornerRadii: CGSize(width: 15.0, height:  15.0))
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        btnNew.layer.mask = maskLayer
                
        let gradientView = UIView(frame: CGRect(x: 0, y: (NewsFeedViewController.cellWidth)/2, width: NewsFeedViewController.cellWidth, height: (NewsFeedViewController.cellWidth)/2))
        let gradientLayer:CAGradientLayer = CAGradientLayer()
        gradientLayer.frame.size =  gradientView.frame.size
        gradientLayer.colors = [UIColor.clear.cgColor,UIColor.appBlackColorNew().withAlphaComponent(0.7).cgColor]
        gradientView.layer.addSublayer(gradientLayer)
        self.imgWorkout.addSubview(gradientView)


    }
    
    func setCell(with model :  WorkoutList){
      //  imgWorkout.sd_setImage(with: URL(string: model.workoutImage), placeholderImage: AppInfo.placeholderImage)
        
        imgWorkout.sd_setImage(with: URL(string: model.workoutImage), placeholderImage: nil)
        
        let attributedString = NSMutableAttributedString(string: model.workoutName)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5.0
        paragraphStyle.alignment = .center
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
        lblName.attributedText = attributedString
        
        var description = ""
        if model.workoutLevel == "All"
        {
             description = model.workoutTotalWorkTime + " All Levels"
        }
        else
        {
             description = model.workoutTotalWorkTime + " " + model.workoutLevel;
        }
        
        let attributedString2 = NSMutableAttributedString(string: description)
        let paragraphStyle2 = NSMutableParagraphStyle()
        paragraphStyle2.lineSpacing = 5.0
        paragraphStyle2.alignment = .center
        attributedString2.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle2, range: NSRange(location: 0, length: attributedString2.length))
        lblDesc.attributedText = attributedString2
        
        btnLock.isHidden = (model.workoutAccessLevel.uppercased() == "OPEN".uppercased() || model.workoutAccessLevel.isEmpty)
        
        btnNew.isHidden = !model.isNew
    }
	
	func setCollectionCell(with model :  ModelFeaturedWorkouts){
		//imgWorkout.sd_setImage(with: URL(string: model.workoutGroupImage), placeholderImage: AppInfo.placeholderImage)
        
        imgWorkout.sd_setImage(with: URL(string: model.workoutGroupImage), placeholderImage: nil)
        
		lblName.text = model.workoutGroupName
		lblDesc.text = "\(model.groupWorkoutCount!) Workouts"
	}
	
	func setCellForFav(with model :  ModelWorkoutList){
		//imgWorkout.sd_setImage(with: URL(string: model.workoutImage), placeholderImage: AppInfo.placeholderImage)
        
        imgWorkout.sd_setImage(with: URL(string: model.workoutImage), placeholderImage: nil)
		
		let attributedString = NSMutableAttributedString(string: model.workoutName)
		let paragraphStyle = NSMutableParagraphStyle()
    //    paragraphStyle.lineSpacing = 5.0

		paragraphStyle.lineSpacing = 2.0
        paragraphStyle.alignment = .center
		attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
		lblName.attributedText = attributedString
        
        var description = ""
        if model.workoutLevel == "All"
        {
            description = model.workoutTotalWorkTime + " All Levels"
        }
        else
        {
            description = model.workoutTotalWorkTime + " " + model.workoutLevel;
        }

		let attributedString2 = NSMutableAttributedString(string: description)
		let paragraphStyle2 = NSMutableParagraphStyle()
		paragraphStyle2.lineSpacing = 5.0
        paragraphStyle2.alignment = .center
	attributedString2.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle2, range: NSRange(location: 0, length: attributedString2.length))
		lblDesc.attributedText = attributedString2
        
        btnLock.isHidden = (model.workoutAccessLevel.uppercased() == "OPEN".uppercased() || model.workoutAccessLevel.isEmpty)
	}
}
