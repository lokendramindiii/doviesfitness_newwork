//
//  Notification_Cell.swift
//  Dovies
//
//  Created by Mindiii on 3/1/19.
//  Copyright © 2019 Hidden Brains. All rights reserved.
//

import UIKit

class Notification_Cell: UITableViewCell {

    @IBOutlet weak var IBlblTitle : UILabel!
    @IBOutlet weak var IBlblDate : UILabel!
    @IBOutlet weak var IBImgview : UIImageView!
    @IBOutlet weak var IBImgviewReadStatus : UIImageView!
    @IBOutlet weak var IBlblDescription : UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCellWith(notification : NotificationModel){
        
        IBlblDate.text = notification.notificationDate
        if notification.notificationStatus == "Unread"
        {
            IBImgviewReadStatus.isHidden = false
        }
        else
        {
            IBImgviewReadStatus.isHidden = true
        }
        
        //-----------------

        IBlblTitle.isHidden = false
        if notification.notificationCode.uppercased() == "EXERCISE"
        {
            IBlblTitle.text = "Exercise"
        }
        else if notification.notificationCode.uppercased() == "NEWS_FEED" {
            IBlblTitle.text = "Feed"
        }
        else if notification.notificationCode.uppercased() == "PROGRAM_PLAN" {
            IBlblTitle.text = "Workout Plan"
        }
        else if notification.notificationCode.uppercased() == "WORKOUT" {
            IBlblTitle.text = "Workout"
        }
        
        else if notification.notificationCode.uppercased() == "WELCOME" {
            IBlblTitle.text = notification.notificationTitle
        }
            
        else if notification.notificationCode.uppercased() == "CUSTOM"
        {
            let HtmlconvertDescription = notification.notificationTitle
            do {
                let finalstring    =  try NSAttributedString(data: HtmlconvertDescription!.data(using: String.Encoding.utf8)!, options: [.documentType: NSAttributedString.DocumentType.html,.characterEncoding: String.Encoding.utf8.rawValue],documentAttributes: nil)
                IBlblTitle.text = finalstring.string
                
            } catch {
                print("Cannot convert html string to attributed string: \(error)")
            }
        }
        IBlblTitle.makeSpacingWithLeftAlignment(lineSpace: 4.0, font: UIFont.tamilBold(size: 14), color: UIColor.colorWithRGB(r: 22, g: 22, b: 2, alpha: 1.0))

        //-----------------

        if notification.notificationImage.isEmpty
        {
            IBImgview.isHidden = true
        }
        else
        {
            IBImgview.isHidden = false
           // IBImgview.sd_setImage(with: URL(string: notification.notificationImage), placeholderImage: AppInfo.placeholderImage)
            IBImgview.sd_setImage(with: URL(string: notification.notificationImage), placeholderImage: nil)

            
            //   cell.imgProfile.af_setImage(withURL: URL(string: notification.notificationImage), placeholderImage: placehAppInfo.AppInfo.placeholderImage, filter: nil)
            
//            IBImgview.af_setImage(withURL: URL(string: notification.notificationImage)!, placeholderImage: UIImage(named: "img_placeholder"), filter: nil, progress: nil, progressQueue: .main, imageTransition: .noTransition, runImageTransitionIfCached: true) { (response) in
//                DispatchQueue.main.async {
//                    if let image = response.result.value {
//                        self.IBImgview.image = image
//                    }
//                    else{
//                        self.IBImgview.image = UIImage(named: "img_placeholder")
//                    }
//                }
//            }
       }
        IBlblDescription.text = notification.attrubutedMessage
        
        //---------
//        let HtmlconvertDescription = notification.notificationMessage
//        do {
//            let finalstring    =  try NSAttributedString(data: HtmlconvertDescription!.data(using: String.Encoding.utf8)!, options: [.documentType: NSAttributedString.DocumentType.html,.characterEncoding: String.Encoding.utf8.rawValue],documentAttributes: nil)
//
//              IBlblDescription.text = finalstring.string
//
//        } catch {
//            print("Cannot convert html string to attributed string: \(error)")
//        }
       IBlblDescription.makeSpacingWithLeftAlignment(lineSpace: 4.0, font: UIFont.tamilRegular(size: 14), color: UIColor.colorWithRGB(r: 122, g: 122, b: 122, alpha: 1.0))
        
    }
}
