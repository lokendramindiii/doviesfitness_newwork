//
//  MyWorkoutsVC.swift
//  Dovies
//
//  Created by hb on 16/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import NVActivityIndicatorView

class MyWorkoutsVC: BaseViewController {

	@IBOutlet weak var tblViewWorkout: UITableView!
	@IBOutlet weak var IBviewAddWorkOut: UIView!
     @IBOutlet weak var IBlbl_Content: UILabel!
    var activityIndicatorNew : NVActivityIndicatorView!

	var arrModelMyWorkouts = [ModelWorkoutList]()
	var cellCount = 0
	var removePreviousCont = false
	var isFromCreateWorkout = false
    var isPublishEnable = true
    var isRefresh = true
    var LoadMore = true

	var isFromBuildPlan = false
    var previousSelectedIndex : IndexPath?
    var workoutSelected:((ModelWorkoutList, Bool)->())?
    var userlist : UserListVC?
    var arrWorkoutListing = [ModelWorkoutCollection]()

    var currentPageIndex = 1

    var shouldLoadMore = true {
        didSet {
            if shouldLoadMore == false {
                tblViewWorkout.tableFooterView = UIView()
            } else {
              tblViewWorkout.tableFooterView = DoviesGlobalUtility.loadingFooter()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DoviesGlobalUtility.setFilterData()

	   self.title = "My Workouts".uppercased()
       self.IBlbl_Content.text =  "Create your own workout here for easy access. You can select the level of the workout. You can write what the workout is good for and also add additional information in the overview section if you choose to share the workout with friends and family or clients."
        
      IBlbl_Content.makeSpacingWithCenterAlignment(lineSpace: 9.0, font: UIFont.tamilRegular(size: 14), color: UIColor.colorWithRGB(r: 136, g: 136, b: 136, alpha: 1.0))
        
		self.addNavigationWhiteBackButton()
        
		tblViewWorkout.register(UINib(nibName: "MyWorkoutsCell", bundle: nil), forCellReuseIdentifier: "MyWorkoutsCell")
        
        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        {
           // self.callGetcustomerUserListingWS()
            self.callGetcustomerUserListingNewWS()
        }
    }

    override func btnBackTapped(sender: UIButton) {
        
        if let profileNav = APP_DELEGATE.profileNavController{
            
            //profileNav.popToRootViewController(animated: false)
            APP_DELEGATE.selectedTabbarIndex = 4
            APP_DELEGATE.setStoryBoardBasedMyprofile()
        }
        else
        {
            self.navigationController?.popToRootViewController(animated: false)
        }
    }
    
    
	override func viewWillAppear(_ animated: Bool) {
        
		super.viewWillAppear(animated)
       
        if isRefresh == false
        {
            return
        }
        tblViewWorkout.tableFooterView?.isHidden = true
        self.arrModelMyWorkouts.removeAll()
        self.setupActivityIndicator()
        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        {
            if removePreviousCont{
                removePreviousCont = false
                var contArry : [UIViewController] = (self.navigationController?.viewControllers)!
                var i = 0
                for cont in contArry{
                    if cont.isKind(of: CreateWorkoutAdminVC.self){
                        contArry.remove(at: i)
                    }
                    i = i + 1
                }
                self.navigationController?.viewControllers = contArry
            }
            if arrModelMyWorkouts.count == 0{
                DispatchQueue.main.async { [weak self] in
                    guard let weakSelf = self else{return}
                    weakSelf.tblViewWorkout.reloadData()
                  weakSelf.tblViewWorkout.layoutIfNeeded()
                }
            }
            self.currentPageIndex = 1
            self.callWorkoutAPI()
        }
        else
        {
            if removePreviousCont{
                removePreviousCont = false
                var contArry : [UIViewController] = (self.navigationController?.viewControllers)!
                var i = 0
                for cont in contArry{
                    if cont.isKind(of: CreateWorkoutVC.self){
                        contArry.remove(at: i)
                    }
                    i = i + 1
                }
                self.navigationController?.viewControllers = contArry
            }
            if arrModelMyWorkouts.count == 0{
                DispatchQueue.main.async { [weak self] in
                    guard let weakSelf = self else{return}
                    weakSelf.tblViewWorkout.reloadData()
                   weakSelf.tblViewWorkout.layoutIfNeeded()
                }
            }
            self.currentPageIndex = 1
            self.callWorkoutAPI()
        }
	}
    
    override func viewDidDisappear(_ animated: Bool) {
        activityIndicatorNew.stopAnimating()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func setupActivityIndicator() {
        let frame = CGRect(x: self.view.center.x-22, y: self.view.center.y-22, width: 45, height: 45)
        activityIndicatorNew = NVActivityIndicatorView(frame: frame)
        activityIndicatorNew.center = self.view.center
        activityIndicatorNew.type = . circleStrokeSpin // add your type
        activityIndicatorNew.color = UIColor.lightGray // add your color
        APP_DELEGATE.window?.addSubview(activityIndicatorNew)
        activityIndicatorNew.startAnimating()
    }
	// MARK: IBAction methods
	
    @objc @IBAction func btnAddTapped(sender: UIButton) {
        
        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        {
            let CreateWorkoutadmin = self.storyboard?.instantiateViewController(withIdentifier: "CreateWorkoutAdminVC")as! CreateWorkoutAdminVC
            CreateWorkoutadmin.IsEditedByAdmin = false
            CreateWorkoutAdminVC.arrExercise = [Any]()
            CreateWorkoutadmin.updateUiFromCreateWorkoutAdmin = {
                (isStatus) in
                self.isRefresh = isStatus
            }
            self.navigationController?.pushViewController(CreateWorkoutadmin, animated: true)
        }
        else
        {
            let CreateWorkout = self.storyboard?.instantiateViewController(withIdentifier: "CreateWorkoutVC")as! CreateWorkoutVC
            CreateWorkoutVC.arrExercise = [Any]()
            CreateWorkout.updateUiFromCreateWorkout = {
                (isStatus) in
                self.isRefresh = isStatus
            }
            self.navigationController?.pushViewController(CreateWorkout, animated: true)
        }
    }
    
    // MARK: WebService methods
    
    /**
     This method used to get user workout list and update the tableview.
     */
	
	func callWorkoutAPI(){
        DoviesWSCalls.callGetCustomerWorkoutsAPI(dicParam: [WsParam.pageIndex : "\(currentPageIndex)"], onSuccess: { (success, modelMyWorkouts, msg, hasNextPage) in
            self.shouldLoadMore = hasNextPage
            self.LoadMore = hasNextPage
            self.activityIndicatorNew.stopAnimating()
			if success{
                if self.currentPageIndex == 1, let workouts = modelMyWorkouts{
                    print(self.currentPageIndex)
                    self.arrModelMyWorkouts = workouts
                }else if   let workouts = modelMyWorkouts{
                    print(self.currentPageIndex)
                    self.arrModelMyWorkouts += workouts
                }
			}
			else
            {
				if self.currentPageIndex == 1{
					if self.arrModelMyWorkouts.count > 0{
						self.arrModelMyWorkouts.removeAll()
					}
				}
			}
            self.cellCount = 0
            if hasNextPage{
                self.currentPageIndex += 1
            }
			self.tblViewWorkout.reloadData()
			self.tblViewWorkout.hideLoader()
			if self.isFromCreateWorkout == true || self.isFromBuildPlan == true{
				return
			}
			if self.arrModelMyWorkouts.count == 0{
				self.IBviewAddWorkOut.isHidden = false
			}
			else{
                self.IBviewAddWorkOut.isHidden = true
                if self.navigationItem.rightBarButtonItems == nil{
                    self.navigationItem.rightBarButtonItems = GlobalUtility.getNavigationButtonItems(target: self, selector: #selector(self.btnAddTapped(sender:)), imageName: "pluce_icon_s26_white")
                }
			}
		}) { (error) in
            self.activityIndicatorNew.stopAnimating()
		}
	}
    
    
    func callWorkoutAdminAPI(){
        DoviesWSCalls.callGetAdminWorkoutsAPI(dicParam: [WsParam.pageIndex : "\(currentPageIndex)"], onSuccess: { (success, modelMyWorkouts, msg, hasNextPage) in
            self.shouldLoadMore = hasNextPage
            if success{
                if self.currentPageIndex == 1, let workouts = modelMyWorkouts{
                    self.arrModelMyWorkouts = workouts
                }else if let workouts = modelMyWorkouts{
                    self.arrModelMyWorkouts += workouts
                }
            }
            else{
                if self.currentPageIndex == 1{
                    if self.arrModelMyWorkouts.count > 0{
                        self.arrModelMyWorkouts.removeAll()
                    }
                }
            }
            self.cellCount = 0
            if hasNextPage{
                self.currentPageIndex += 1
            }
            self.tblViewWorkout.reloadData()
            self.tblViewWorkout.hideLoader()
            if self.isFromCreateWorkout == true || self.isFromBuildPlan == true{
                return
            }
            if self.arrModelMyWorkouts.count == 0{
                self.IBviewAddWorkOut.isHidden = false
            }
            else{
                self.IBviewAddWorkOut.isHidden = true
                if self.navigationItem.rightBarButtonItems == nil{
                    self.navigationItem.rightBarButtonItems = GlobalUtility.getNavigationButtonItems(target: self, selector: #selector(self.btnAddTapped(sender:)), imageName: "pluce_icon_s26_h")
                }
            }
        }) { (error) in
            
        }
    }
    
    @objc func didTapWorkoutGroupButton(sender : UIButton!){
        showActionSheetAtIndex(SelectIndex: sender.tag)
    }
    
    func showActionSheetAtIndex(SelectIndex : Int){
        let arrData: [alertActionData]!
            arrData = [
                alertActionData(title: "Publish to app", imageName: "icon_publish", highlighedImageName : "icon_publish"),
                alertActionData(title: "Delete", imageName: "delete_btn_s27b_h", highlighedImageName : "delete_btn_s27b_h"),
            ]
            
            DoviesGlobalUtility.showDefaultActionSheet(on:self,actionData: arrData,cancelTitle: "Cancel") { (clickedIndex:Int, isCancel:Bool) in
                switch clickedIndex{
                case 0:
                    print(self.arrModelMyWorkouts[SelectIndex].workoutId)
                    self.userlist = self.storyboard?.instantiateViewController(withIdentifier: "UserListVC")as? UserListVC
                    self.userlist?.arrWorkoutGroupListing = self.arrWorkoutListing
                    self.userlist?.SelectWorkoutCollectionId = self.arrModelMyWorkouts[SelectIndex].workoutGroupId
                    self.userlist?.WorkoutId = self.arrModelMyWorkouts[SelectIndex].workoutId
                    self.userlist?.Str_Title = "Workout Group"
                    self.navigationController?.pushViewController(self.userlist! , animated: true)
                case 1:

                    // change api for admin remaining
                    UIAlertController.showAlert(in: self, withTitle: AlertTitle.appName, message: AlertMessages.deleteWorkoutLog, cancelButtonTitle: "No", destructiveButtonTitle: "Delete", otherButtonTitles: nil) { (alertControl, action, index) in
                        if index == 1
                        {
                            let workoutlogId = self.arrModelMyWorkouts[SelectIndex].workoutId ?? ""
                            var dic = [String: Any]()
                            dic[WsParam.workoutId] = workoutlogId
                         //   dic["added_by_type"] = "Admin"
                            DoviesWSCalls.cellDeleteWorkoutAPI(dicParam: dic, onSuccess: { (success, msg) in
                                if success{
                                    self.arrModelMyWorkouts.remove(at: SelectIndex)
                                    self.tblViewWorkout.reloadData()
                                    self.IBviewAddWorkOut.isHidden = self.arrModelMyWorkouts.count > 0
                                }
                            }, onFailure: { (error) in
                                
                            })
                        }
                    }
                default:
                    break
                }
                
            }
        }
}

extension MyWorkoutsVC: UITableViewDelegate, UITableViewDataSource {
	
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        {
            return false
        }
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        {
        }
        else
        {
        if (editingStyle == UITableViewCellEditingStyle.delete) {
            UIAlertController.showAlert(in: self, withTitle: AlertTitle.appName, message: AlertMessages.deleteWorkoutLog, cancelButtonTitle: "No", destructiveButtonTitle: "Delete", otherButtonTitles: nil) { (alertControl, action, index) in
                if index == 1
                {
                    let workoutlogId = self.arrModelMyWorkouts[indexPath.row].workoutId ?? ""
                    var dic = [String: Any]()
                    dic[WsParam.workoutId] = workoutlogId
                    DoviesWSCalls.cellDeleteWorkoutAPI(dicParam: dic, onSuccess: { (success, msg) in
                        if success{
                            self.arrModelMyWorkouts.remove(at: indexPath.row)
                            self.tblViewWorkout.reloadData()
                            self.IBviewAddWorkOut.isHidden = self.arrModelMyWorkouts.count > 0
                        }
                    }, onFailure: { (error) in
                        
                })
            }
         }
       }
    }
}

    
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return arrModelMyWorkouts.count > 0 ? arrModelMyWorkouts.count : cellCount
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let cell = tableView.dequeueReusableCell(withIdentifier: "MyWorkoutsCell") as! MyWorkoutsCell
		if arrModelMyWorkouts.count > 0{
			cell.setWorkoutsData(modelWorkOuts: arrModelMyWorkouts[indexPath.row])
		}
        if isFromBuildPlan{
            if let iPath = previousSelectedIndex, iPath == indexPath{
                cell.isCellSelected = true
            }else{
                cell.isCellSelected = false
            }
        }
        cell.imgRightArrow.isHidden = isFromBuildPlan
        cell.btnSelect.isHidden = !isFromBuildPlan
        cell.btnWorkoutGroup.tag = indexPath.row
        cell.btnWorkoutGroup.addTarget(self, action: #selector(self.didTapWorkoutGroupButton), for: .touchUpInside)
        
        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        {
            if isPublishEnable == false
            {
                cell.btnWorkoutGroup.isHidden = true
            }
            else
            {
            cell.btnWorkoutGroup.isHidden = false
            }
        }
        else
        {
            cell.btnWorkoutGroup.isHidden = true
        }
		return cell
	}
	
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (arrModelMyWorkouts.count - 1) && shouldLoadMore == true {
           // self.shouldLoadMore = false
            
            if LoadMore == true
            {
             LoadMore = false
            self.callWorkoutAPI()
            }
        }
    }
    
    
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		if arrModelMyWorkouts.count > 0{
            let workout = arrModelMyWorkouts[indexPath.row]
            if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
            {
                if isFromBuildPlan{
                    let cell = tableView.cellForRow(at: indexPath) as! MyWorkoutsCell
                    if previousSelectedIndex == indexPath{
                        cell.isCellSelected = false
                    }
                    else{
                        cell.isCellSelected = true
                    }
                    self.workoutSelected?(arrModelMyWorkouts[indexPath.row], cell.isCellSelected)
                    if let iPath = previousSelectedIndex, iPath != indexPath{
                        let cell = tableView.cellForRow(at: iPath) as! MyWorkoutsCell
                        cell.isCellSelected = false
                    }
                    if cell.isCellSelected == false{
                        previousSelectedIndex = nil
                    }
                    else{
                        previousSelectedIndex = indexPath
                    }
                }
                else
                {
                    let featuredWorkOutDetailViewController = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "FeaturedWorkOutDetailViewController")as! FeaturedWorkOutDetailViewController
                    featuredWorkOutDetailViewController.navigationItem.title = arrModelMyWorkouts[indexPath.row].workoutName
                    featuredWorkOutDetailViewController.modelWorkoutList = arrModelMyWorkouts[indexPath.row]
                    featuredWorkOutDetailViewController.isFromCreateWorkout = self.isFromCreateWorkout
                    
                    featuredWorkOutDetailViewController.updateUiCloser = {
                       [weak self] (isupdate) in
                        guard let weakself = self else { return }
                        weakself.isRefresh = isupdate
                    }
                self.navigationController?.pushViewController(featuredWorkOutDetailViewController, animated: true)
                }
            }
            else{
                
            if workout.workoutAccessLevel.uppercased() == "OPEN".uppercased(){
                if isFromBuildPlan{
                    let cell = tableView.cellForRow(at: indexPath) as! MyWorkoutsCell
					if previousSelectedIndex == indexPath{
						cell.isCellSelected = false
					}
					else{
                    	cell.isCellSelected = true
					}
                    self.workoutSelected?(arrModelMyWorkouts[indexPath.row], cell.isCellSelected)
                    if let iPath = previousSelectedIndex, iPath != indexPath{
                        let cell = tableView.cellForRow(at: iPath) as! MyWorkoutsCell
                        cell.isCellSelected = false
                    }
					if cell.isCellSelected == false{
						previousSelectedIndex = nil
					}
					else{
						previousSelectedIndex = indexPath
					}
                }
				else
				{
                    let featuredWorkOutDetailViewController = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "FeaturedWorkOutDetailViewController")as! FeaturedWorkOutDetailViewController
                    featuredWorkOutDetailViewController.navigationItem.title = arrModelMyWorkouts[indexPath.row].workoutName
                    featuredWorkOutDetailViewController.modelWorkoutList = arrModelMyWorkouts[indexPath.row]
                    featuredWorkOutDetailViewController.isFromCreateWorkout = self.isFromCreateWorkout
                    featuredWorkOutDetailViewController.updateUiCloser = {
                       [weak self] (isupdate) in
                        guard let weakself = self else {  return }
                        weakself.isRefresh = isupdate
                    }
                self.navigationController?.pushViewController(featuredWorkOutDetailViewController, animated: true)
                }
            }else{
                if let upgradeVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "UpgradeToPremiumViewController") as? UpgradeToPremiumViewController{
                    upgradeVC.onPurchaseSuccess = {
                        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
                        {
                            self.callWorkoutAPI()
                        }
                        else
                        {
                            self.callWorkoutAPI()
                       }
                    }
                    self.navigationController?.pushViewController(upgradeVC, animated: true)
                }
             }
          }
       }
	}
}
extension MyWorkoutsVC {
    
    /**
     This method is used to call feed listing web service.
     */
    
    func callGetcustomerUserListingWS(){
        
        let dic = [String: Any]()
        DoviesWSCalls.callGetModelGetuserListDataAPI(dicParam: dic, onSuccess: { (sucess, arrgetuserlist,arrCreatedBy,arrWorkoutGroup, strMessage) in
            GlobalUtility.hideActivityIndi(viewContView: self.view)
            if sucess{
                
                if arrWorkoutGroup != nil{
                    self.arrWorkoutListing = arrWorkoutGroup!
                }
            }
        }) { (error) in
            
        }
    }
    
    
    func callGetcustomerUserListingNewWS(){
        
        var dic = [String: Any]()
        
        dic[WsParam.searchQuery] = ""
        dic[WsParam.pageIndex] = "1"
        dic[WsParam.allowedUserId] = ""
        
        DoviesWSCalls.callGetModelGetuserListDataSearchAPI(dicParam: dic, onSuccess: {  [weak self](sucess, arrgetuserlist,arrCreatedBy,arrWorkoutGroup, strMessage ,hasNextPage) in
            
            GlobalUtility.hideActivityIndi(viewContView: self!.view)
            
            guard let weakSelf = self else{return}
            weakSelf.shouldLoadMore = hasNextPage
            
            if sucess{
             
                if arrWorkoutGroup != nil{
                    self?.arrWorkoutListing = arrWorkoutGroup!
                }
            }
        }) { (error) in
            
        }
    }

}

