//
//  FeaturesHorizontalCollCell.swift
//  Dovies
//
//  Created by hb on 08/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class FeaturesHorizontalCollCell: UICollectionViewCell {

    @IBOutlet var img_BackImage: UIImageView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblTime: UILabel!
    @IBOutlet var btnLock: UIButton!
    @IBOutlet weak var btnNew : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let path = UIBezierPath(roundedRect:btnNew.bounds,
                                byRoundingCorners:[.topLeft,.bottomRight],
                                cornerRadii: CGSize(width: 15.0, height:  15.0))
        
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        btnNew.layer.mask = maskLayer
        
        let gradientView = UIView(frame: CGRect(x: 0, y: (NewsFeedViewController.cellWidth  )/2, width: NewsFeedViewController.cellWidth, height: (NewsFeedViewController.cellWidth)/2))
        let gradientLayer:CAGradientLayer = CAGradientLayer()
        gradientLayer.frame.size =  gradientView.frame.size
        gradientLayer.colors = [UIColor.clear.cgColor,UIColor.appBlackColorNew().withAlphaComponent(0.7).cgColor]
        gradientView.layer.addSublayer(gradientLayer)
        self.img_BackImage.addSubview(gradientView)
     
    }
    
	func setFeaturedCell(modelWorkoutList: ModelWorkoutList){
        
        btnNew.isHidden = !modelWorkoutList.isNew
        img_BackImage.sd_setImage(with: URL(string: modelWorkoutList.workoutImage), placeholderImage: nil)
		lblTime.text = modelWorkoutList.workoutTotalWorkTime + " - " + modelWorkoutList.workoutLevel
		lblTitle.text = modelWorkoutList.workoutName
        btnLock.isHidden = (modelWorkoutList.workoutAccessLevel.uppercased() == "OPEN".uppercased() || modelWorkoutList.workoutAccessLevel.isEmpty)
	}
}
