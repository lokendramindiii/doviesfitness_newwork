//
//  WeightSelectionViewController.swift
//  Dovies
//
//  Created by Mindiii on 9/13/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

enum HeightUnit : Int{
    case Imperial = 1
    case Metric = 2
}

enum WeightUnit: Int {
     case Imperial = 1
     case Metric = 2
}


class WeightSelectionViewController: BaseViewController{
    
    var signupParams : [String:Any]?
    
    @IBOutlet weak var lblHeightValue1: UILabel!
    @IBOutlet weak var lblHeightValue2: UILabel!
    
    @IBOutlet weak var lblHeightUnit1: UILabel!
    @IBOutlet weak var lblHeightUnit2: UILabel!
    
    @IBOutlet weak var lblWeightValue: UILabel!
    @IBOutlet weak var lblWeightUnit: UILabel!
    
    @IBOutlet weak var stackHeightUnits: UIStackView!
  
    @IBOutlet weak var viewSegmentH: UIView!
    @IBOutlet weak var viewSegmentW: UIView!

    
    @IBOutlet weak var viewHUnit1: UIView!
    @IBOutlet weak var viewHUnit2: UIView!
    
    
    @IBOutlet weak var btnHeightUnit1: UIButton!
    @IBOutlet weak var btnHeightUnit2: UIButton!
    @IBOutlet weak var viewHeightUnit1Bg: UIView!
    @IBOutlet weak var viewHeightUnit2Bg: UIView!
    
    @IBOutlet weak var btnWeightUnit1: UIButton!
    @IBOutlet weak var btnWeightUnit2: UIButton!
    @IBOutlet weak var viewWeightUnit1Bg: UIView!
    @IBOutlet weak var viewWeightUnit2Bg: UIView!
    
    var selectedHeightUnit = HeightUnit.Imperial
    var selectedWeightUnit = WeightUnit.Imperial
    

    var strHeightUnit = ""
    var strWeightUnit = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupUI()
        strHeightUnit = "FT"
        strWeightUnit = "LBS"

    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    func setupUI() {
        self.btnHeightUnit1.layer.cornerRadius = 5
        self.btnHeightUnit2.layer.cornerRadius = 5

        self.btnHeightUnit1.clipsToBounds = true
        self.btnHeightUnit2.clipsToBounds = true
        
        self.btnWeightUnit1.layer.cornerRadius = 5
        self.btnWeightUnit2.layer.cornerRadius = 5
        
        self.btnWeightUnit1.clipsToBounds = true
        self.btnWeightUnit2.clipsToBounds = true
        

        self.viewSegmentH.layer.cornerRadius = 5
        self.viewSegmentH.layer.borderWidth = 1
        self.viewSegmentH.layer.borderColor = UIColor.white.cgColor
        
        
        self.viewSegmentW.layer.cornerRadius = 5
        self.viewSegmentW.layer.borderWidth = 1
        self.viewSegmentW.layer.borderColor = UIColor.white.cgColor
        
        self.selectSegmentHeightUnit1()
        self.selectSegmentWeightUnit1()
    }
    
    func selectSegmentHeightUnit1()  {
        self.lblHeightUnit1.text = "FT"
        self.lblHeightUnit2.text = "IN"
        
        self.lblHeightValue1.text = "0"
        self.lblHeightValue2.text = "0"
        
        self.btnHeightUnit1.backgroundColor = UIColor.white
        self.viewHeightUnit1Bg.backgroundColor = UIColor.white
        self.btnHeightUnit1.setTitleColor(UIColor.black, for: .normal)
        
        self.btnHeightUnit2.backgroundColor = UIColor.clear
        self.viewHeightUnit2Bg.backgroundColor = UIColor.clear

        self.btnHeightUnit2.setTitleColor(UIColor.white, for: .normal)
    }
    
    func selectSegmentHeightUnit2()  {
        self.lblHeightUnit1.text = "CM"
        self.lblHeightUnit2.text = "CM"
        
        self.lblHeightValue1.text = "0"
        self.lblHeightValue2.text = "0"
        
        self.btnHeightUnit2.backgroundColor = UIColor.white
        self.viewHeightUnit2Bg.backgroundColor = UIColor.white

        self.btnHeightUnit2.setTitleColor(UIColor.black, for: .normal)
        
        self.btnHeightUnit1.backgroundColor = UIColor.clear
        self.viewHeightUnit1Bg.backgroundColor = UIColor.clear

        self.btnHeightUnit1.setTitleColor(UIColor.white, for: .normal)
    }
    
    
    func selectSegmentWeightUnit1()  {
        self.lblWeightUnit.text = "LBS"
        self.lblWeightValue.text = "0"

        self.btnWeightUnit1.backgroundColor = UIColor.white
        self.viewWeightUnit1Bg.backgroundColor = UIColor.white

        self.btnWeightUnit1.setTitleColor(UIColor.black, for: .normal)
        
        self.btnWeightUnit2.backgroundColor = UIColor.clear
        self.viewWeightUnit2Bg.backgroundColor = UIColor.clear

        self.btnWeightUnit2.setTitleColor(UIColor.white, for: .normal)
    }
    
    func selectSegmentWeightUnit2()  {
        self.lblWeightUnit.text = "KG"
        self.lblWeightValue.text = "0"
        self.btnWeightUnit2.backgroundColor = UIColor.white
        self.viewWeightUnit2Bg.backgroundColor = UIColor.white
        self.btnWeightUnit2.setTitleColor(UIColor.black, for: .normal)
        
        self.btnWeightUnit1.backgroundColor = UIColor.clear
        self.viewWeightUnit1Bg.backgroundColor = UIColor.clear

        self.btnWeightUnit1.setTitleColor(UIColor.white, for: .normal)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionHeightPicker1(_ sender: Any) {
        
        var mcPicker : McPicker?
        if selectedHeightUnit == .Imperial{
            mcPicker = McPicker(data: [StaticPickerData.getArrHeightInFeet()], selectedTitle: [""])
            
        }else{
            mcPicker = McPicker(data: [StaticPickerData.getArrHeightInMetrics()], selectedTitle: [""])
            
        }
        
        mcPicker?.show {(selections: [Int : String]) -> Void in
            if self.selectedHeightUnit == .Imperial{
                if var feet = selections[0]{
                    feet = feet.replacingOccurrences(of: " ft", with: "")
                    self.lblHeightValue1.text = feet
                }
                
            }else{
                if var centimeters = selections[0]{
                    centimeters = centimeters.replacingOccurrences(of: " cm", with: "")
                    self.lblHeightValue1.text = centimeters
                }
                
            }
        }
    }
    
    @IBAction func actionHeightPicker2(_ sender: Any) {
        var mcPicker : McPicker?
        if selectedHeightUnit == .Imperial{
            mcPicker = McPicker(data: [StaticPickerData.getArrHeightInInches()], selectedTitle: [""])
            
        }else{
            mcPicker = McPicker(data: [StaticPickerData.getArrHeightInCms()], selectedTitle: [""])
            
        }
        
        mcPicker?.show {(selections: [Int : String]) -> Void in
            if self.selectedHeightUnit == .Imperial{
                if var inches = selections[0]{
                    inches = inches.replacingOccurrences(of: " in", with: "")
                    self.lblHeightValue2.text = inches

                }
                
            }else{
                if var centimeters = selections[0]{
                    centimeters = centimeters.replacingOccurrences(of: " cm", with: "")
                    self.lblHeightValue2.text = centimeters
                }
                
            }
        }
    }
    

    
    @IBAction func actionWeightPicker(_ sender: Any) {
        
        self.view.endEditing(true)
        var weight = [String]()
        
            if self.selectedWeightUnit == .Imperial{
                for i in 30...500{
                    weight.append("\(i)")
                }
            }else{
                for i in 13...227{
                    weight.append("\(i)")
                }
            }
        
        
        let mcPicker = McPicker(data: [weight], selectedTitle:[""])
//        mcPicker.label?.font = UIFont.tamilBold(size: 20)
//        mcPicker.toolbarBarTintColor = UIColor(r: 235, g: 235, b: 235, alpha: 1)
//        mcPicker.pickerBackgroundColor = UIColor(r: 205, g: 209, b: 214, alpha: 1)
        mcPicker.show { [weak self](selections: [Int : String]) -> Void in
            guard self != nil else{return}
            if var weight = selections[0]{
                weight = weight.replacingOccurrences(of: " kgs", with: "")
                weight = weight.replacingOccurrences(of: " lbs", with: "")
                self?.lblWeightValue.text = weight
                
            }
        }

    }
    
    @IBAction func actionSkip(_ sender: Any) {
        signupParams![WsParam.weight] =  ""
        signupParams![WsParam.height] = ""
        signupParams![WsParam.weightUnit] = strWeightUnit
        signupParams![WsParam.heightUnit] = strHeightUnit
        
        print(self.signupParams)
        self.navigateToUsernameScreen(params: signupParams!)

    }
    
    @IBAction func actionNext(_ sender: Any) {
        
        signupParams![WsParam.weight] = self.getWeight()
        signupParams![WsParam.height] = self.getHeight()
        
        signupParams![WsParam.weightUnit] = strWeightUnit
        signupParams![WsParam.heightUnit] = strHeightUnit

        print(self.signupParams)
         self.navigateToUsernameScreen(params: signupParams!)

    }
    
    func navigateToUsernameScreen(params: [String: Any])  {
        let usernameVC = Storyboard.Login.storyboard().instantiateViewController(withIdentifier: "CreateUsernameViewController")as! CreateUsernameViewController
        usernameVC.signupParams = params
        
        self.navigationController?.pushViewController(usernameVC, animated: true)
    }
    
    
    @IBAction func actionHeightUnitSelection(_ sender: UIButton) {
        if sender.tag == 0{
            self.viewHUnit1.isHidden = false
            self.selectSegmentHeightUnit1()
            self.selectedHeightUnit = .Imperial
            strHeightUnit = "FT"
            print("ft")


        }
        else{
            self.selectSegmentHeightUnit2()
            self.viewHUnit1.isHidden = true
            self.selectedHeightUnit = .Metric
            strHeightUnit = "CM"
            print("CM")


        }
    }
    
    
    @IBAction func actionWeightUnitSelection(_ sender: UIButton) {
        if sender.tag == 0{
            self.selectSegmentWeightUnit1()
            self.selectedWeightUnit = .Imperial
            strWeightUnit = "LBS"
            print("LBS")

        }
        else{
            self.selectSegmentWeightUnit2()
            self.selectedWeightUnit = .Metric
            strWeightUnit = "KG"
            print("KG")

        }
    }
    

    
}

extension WeightSelectionViewController{
    /**
     This method is used to convert
     - feet to centimeters
     - inches to centimeters.
     */
    func getHeight() -> String{
            if selectedHeightUnit == .Imperial {
                if let heightFt = lblHeightValue1.text, let heightIn = lblHeightValue2.text {
                    let heightInCms = Conversions.feetToCentimeters(depthInFeet: heightFt)
                    let heightInCmsFromInches = Conversions.inchesToCentimeters(inches: heightIn)
                    let totalCms = heightInCms + heightInCmsFromInches
                    return "\(Int(totalCms.rounded()))"
                }
            }else{
                let heightCms = lblHeightValue2.text ?? "0"
                return heightCms
            }
        return ""
    }
    
    /**
     This method converts the weight from KGs to LBS.
     */
    func getWeight() -> String{
        if let text = self.lblWeightValue.text{
            if selectedWeightUnit == .Imperial{
                let kgs = Conversions.getKgsFromLBS(lbs: text.replacingOccurrences(of: " lbs", with: ""))
                return "\(kgs)"
            }else{
                return text.replacingOccurrences(of: " kgs", with: "")
            }
        }
        return ""
        
    }
    
}
