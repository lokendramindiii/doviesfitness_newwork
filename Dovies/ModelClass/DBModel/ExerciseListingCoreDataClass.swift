//
//  ExerciseListingCoreDataClass.swift
//  Dovies
//
//  Created by Mindiii on 4/3/19.
//  Copyright © 2019 Hidden Brains. All rights reserved.
//
import Foundation
import CoreData

public class Exercise_List: NSManagedObject {
    
    class func saveContactInDBExerciseListEntity(ExerciseArray:[ExerciseDetails]){
        
       //  getImage()
        
        for obj in ExerciseArray {

            if someEntityExists(exerciseId: "\(obj.exerciseId ?? "")") == true
            {
                print("exist")
                return
            }
            else
            {
                saveImageDocumentDirectory(object: [obj])
                
                //                let url = URL(string:obj.exerciseImage)
                //                        if let data = try? Data(contentsOf: url!) {
                //                                DispatchQueue.main.async {
                //                                    saveImageToDB(exerciseId: obj.exerciseId, image: data, exerciseName: obj.exerciseName)
                //
                //                                    print("saveing mode")
                //                            }
                //                        }
                    }
             }
       
    }
    
    
    
    class  func saveImageDocumentDirectory(object: [ExerciseDetails]){
        
        let fileManager = FileManager.default
        var paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("Mindiii")
        
        let url = URL(string:object[0].exerciseImage)
        let url12 = URL(fileURLWithPath: paths)
        
        print(url12)

        
        
        
        if let dData = try? Data(contentsOf: url!) {
            
            let image = UIImage(data: dData)
            
            print(paths)
            
            saveImageToDB(exerciseId: object[0].exerciseId, image: paths, exerciseName: object[0].exerciseName)
                
            print("saveing mode")
            
            }
        else
        {
            print(" No saveing mode")
        }
    }
    
    
    class func getImage(){
        
        
        let fileManager = FileManager.default
        let imagePAth = (self.getDirectoryPath() as NSString).appendingPathComponent("Mindiii")
        
        if fileManager.fileExists(atPath: imagePAth){
            
            print(imagePAth)
            
        }else{
            print("No Image")
        }
        
    }
    
    
  class  func getDirectoryPath() -> String {
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        
        let documentsDirectory = paths[0]
        return documentsDirectory
        
    }
    

    
    
 class   func createDirectorya(){
        
        let fileManager = FileManager.default
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("customDirectory")
        
        if !fileManager.fileExists(atPath: paths){
            
            try! fileManager.createDirectory(atPath: paths, withIntermediateDirectories: true, attributes: nil)
        }else{
            
            print("Already dictionary created.")
            
        }
        
    }
    
   class func deleteDirectory(){
        
       let fileManager = FileManager.default
        
        let paths = (NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as NSString).appendingPathComponent("customDirectory")
        
        if fileManager.fileExists(atPath: paths){
            
            try! fileManager.removeItem(atPath: paths)
            
        }else{
            
            print("Something wronge.")
            
        }
        
    }
    
    class  func someEntityExists(exerciseId: String) -> Bool {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "ExerciseList")
        fetchRequest.predicate = NSPredicate(format: "exerciseId = %@", exerciseId)
        var results: [NSManagedObject] = []
        
        do {
            results = try APP_DELEGATE.managedObjectContext.fetch(fetchRequest)
        }
        catch {
            print("error executing fetch request: \(error)")
        }
        return results.count > 0
    }
    
    
    
    
    class func fetchFromCoredata(exerciseId:String) -> String
    {
        let string = ""
        
        let context = APP_DELEGATE.managedObjectContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ExerciseList")
        request.predicate = NSPredicate(format: "exerciseId = %@", exerciseId)
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                
                let image = data.value(forKey: "exerciseImage") as! String
                
                getImage()
                
                return image
                
            }
        } catch {
            return string

            print("Failed")
        }
        return string

    }
    
    
//
//   class func fetchFromCoredata(exerciseId:String) ->  Data
//    {
//        let data = Data()
//        let context = APP_DELEGATE.managedObjectContext
//        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "ExerciseList")
//        request.predicate = NSPredicate(format: "exerciseId = %@", exerciseId)
//        do {
//            let result = try context.fetch(request)
//            for data in result as! [NSManagedObject] {
//                let image = data.value(forKey: "exerciseImage") as! NSData
//                return image as Data
//            }
//        } catch {
//            print("Failed")
//            return  data
//        }
//        return  data
//    }
//
    
    class  func saveImageToDB(exerciseId: String, image: String , exerciseName:String) {
        
        let context = APP_DELEGATE.managedObjectContext
        let entity = NSEntityDescription.entity(forEntityName: "ExerciseList", in: context)!
        let ExerciseListEntity = NSManagedObject(entity: entity, insertInto: context)
        
        ExerciseListEntity.setValue(image, forKey: "exerciseImage")
        ExerciseListEntity.setValue(exerciseId, forKey: "exerciseId")
        ExerciseListEntity.setValue(exerciseName, forKey: "exerciseName")
        
        do {
            try context.save()
            print("saved!")
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    class func deleteAllDataOfExerciseLibrary()
    {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ExerciseList")
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try APP_DELEGATE.managedObjectContext.execute(deleteRequest)
        } catch {
        }
    }
    
}
