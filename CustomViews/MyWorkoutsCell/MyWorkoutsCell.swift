//
//  WorkoutCell.swift
//  Dovies
//
//  Created by Ananda on 16/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class MyWorkoutsCell: UITableViewCell {
	
	@IBOutlet weak var imgView: UIImageView!
	@IBOutlet weak var lblTime: UILabel!
	@IBOutlet weak var lblTitle: UILabel!
	@IBOutlet weak var lblSubtitile: UILabel!

    @IBOutlet weak var btnSelect : UIButton!
    @IBOutlet weak var btnWorkoutGroup : UIButton!

    @IBOutlet weak var imgRightArrow : UIImageView!
    @IBOutlet weak var imgLock : UIImageView!
    
    var isCellSelected : Bool = false{
        didSet{
            btnSelect.isSelected = isCellSelected
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.lblSubtitile.numberOfLines = 2;
        self.lblTitle.numberOfLines = 2;
        // Initialization code
    }
	
	func setWorkoutsData(modelWorkOuts: ModelWorkoutList){
        
        let attributedString = NSMutableAttributedString(string: modelWorkOuts.workoutName)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 3.0
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
        lblTitle.attributedText = attributedString
        
        let attributedString2 = NSMutableAttributedString(string: modelWorkOuts.workoutCategory)
        let paragraphStyle2 = NSMutableParagraphStyle()
        paragraphStyle2.lineSpacing = 3.0
        attributedString2.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle2, range: NSRange(location: 0, length: attributedString2.length))
        lblSubtitile.attributedText = attributedString2
        
        
		//lblTitle.text = modelWorkOuts.workoutName
        //lblSubtitile.text = modelWorkOuts.workoutCategory
		lblTime.text = modelWorkOuts.workoutTime
		//imgView.sd_setImage(with: URL(string: modelWorkOuts.workoutImage), placeholderImage: AppInfo.placeholderImage)
        
        imgView.sd_setImage(with: URL(string: modelWorkOuts.workoutImage), placeholderImage: nil)

        
        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        {
            imgLock.isHidden = true
        }
        else
        {
            imgLock.isHidden = modelWorkOuts.workoutAccessLevel.uppercased() == "OPEN".uppercased()
        }
	}
	
	func setWorkoutsDownLoadData(modelWorkOuts: Workout){
		lblTitle.text = modelWorkOuts.workoutName
		lblTime.text = modelWorkOuts.workoutTotalTime
		//imgView.sd_setImage(with: URL(string: modelWorkOuts.workoutImage!), placeholderImage: AppInfo.placeholderImage)
        
        imgView.sd_setImage(with: URL(string: modelWorkOuts.workoutImage!), placeholderImage: nil)

        let attributedString2 = NSMutableAttributedString(string: modelWorkOuts.workoutGoodFor ?? "")
        let paragraphStyle2 = NSMutableParagraphStyle()
        paragraphStyle2.lineSpacing = 3.0
        attributedString2.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle2, range: NSRange(location: 0, length: attributedString2.length))
        lblSubtitile.attributedText = attributedString2
        

        
		imgLock.isHidden = true
	}
    
    func setFeaturedWorkoutsData(modelWorkOuts: ModelWorkoutList){
        lblTitle.text = modelWorkOuts.workoutName
        lblTime.text = modelWorkOuts.workoutTotalWorkTime
      //  imgView.sd_setImage(with: URL(string: modelWorkOuts.workoutImage), placeholderImage: AppInfo.placeholderImage)
        
        imgView.sd_setImage(with: URL(string: modelWorkOuts.workoutImage), placeholderImage: nil)

        
        lblSubtitile.text = modelWorkOuts.workoutCategory
        imgLock.isHidden = modelWorkOuts.workoutAccessLevel.uppercased() == "OPEN".uppercased()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
