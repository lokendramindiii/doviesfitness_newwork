//
//  GoodForListingVC.swift
//  Dovies
//
//  Created by hb on 23/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class GoodForListingVC: BaseViewController,UITableViewDelegate,UITableViewDataSource {
    
    var selected: [String] = [String]()
    var completion : ((_ selected: [String]?,_ arrID: [String]?) -> ())?
    
    var arrListing = [FilterDataModel]()
    var arrIdData = [String]()
    var  arrFilterData: [FilterGroup] = DoviesGlobalUtility.arrFilterData
    @IBOutlet var listingTblView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Good For".uppercased()
        self.navigationItem.hidesBackButton  = true
      //  self.addNavigationBackButton()
        self.addNavigationWhiteBackButton()
        
        self.listingTblView.tableFooterView = UIView()
        self.getArrayDataListing()
        // self.callFilterDataListingWS()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getArrayDataListing(){
        let arrValue = arrFilterData.filter {$0.groupName == "Good For"}
        if arrValue.count > 0{
            self.arrListing = arrValue[0].list
            
        }
    }
    // MARK: UITableview datasorce Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrListing.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListingTableCell") as! ListingTableCell
        cell.btnSelection.tag = indexPath.row
        cell.btnSelection.isSelected = arrIdData.contains(arrListing[indexPath.row].gmGoogforMasterId)
        cell.setData(arrListing[indexPath.row], at: indexPath)
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        return cell
    }
    
    // MARK: UITableview Delegate Methods
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if arrIdData.contains(arrListing[indexPath.row].gmGoogforMasterId){
            if let index = self.arrIdData.index(where: {$0 == arrListing[indexPath.row].gmGoogforMasterId}){
                self.arrIdData.remove(at: index)
                self.selected.remove(at: index)
            }
        }else{
            self.arrIdData.append(arrListing[indexPath.row].gmGoogforMasterId)
            self.selected.append(arrListing[indexPath.row].gmDisplayName)
        }
        listingTblView.reloadRows(at: [indexPath], with: .none)
    }
    
    @IBAction func btnDoneClicked(_ sender: Any) {
        self.completion?(self.selected, self.arrIdData)
        self.navigationController?.popViewController(animated: true)
    }
}


extension GoodForListingVC{
    /**
     This method used to call feed listing WS
     */

    func callFilterDataListingWS(){
        guard let cUser = DoviesGlobalUtility.currentUser else {return}
        var dic = [String: Any]()
        dic[WsParam.authCustomerId] = cUser.customerAuthToken
        dic["module_type"] = ""
        DoviesWSCalls.callGetModelFilterDataAPI(dicParam: dic, onSuccess: { (sucess, filterArr, strMessage) in
            GlobalUtility.hideActivityIndi(viewContView: self.view)
            if sucess{
                if filterArr != nil{
                    let arrValue = filterArr!.filter {$0.groupName == "Good For"}
                    if arrValue.count > 0{
                        self.arrListing = arrValue[0].list
                        self.listingTblView.reloadData()
                    }
                    
                }
            }
        }) { (error) in
            
        }
    }
}
