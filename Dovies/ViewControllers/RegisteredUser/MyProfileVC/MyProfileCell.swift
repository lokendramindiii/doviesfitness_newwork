//
//  MyProfileCell.swift
//  Dovies
//
//  Created by hb on 19/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class MyProfileCell: UITableViewCell {

    @IBOutlet var lblTime: UILabel!
    @IBOutlet var lblListing: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
