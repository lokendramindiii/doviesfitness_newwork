//
//  GalleryViewController.swift
//  CollectionViewPhotoGellery
//
//  Created by Narendra Goojer on 30/09/18.
//  Copyright © 2018 Narendra Goojer. All rights reserved.
//

import UIKit

class GalleryViewController: UIViewController {
    @IBOutlet weak var collectionFull: UICollectionView!
    
    @IBOutlet weak var collectionThumb: UICollectionView!
    
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnShare: UIButton!

    
    var arrLogImageModel = [ModelWorkoutLogListImage]()
    var currentPage  = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionFull.dataSource = self
        self.collectionFull.delegate = self
        self.collectionThumb.delegate = self

        self.collectionThumb.dataSource = self
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionClose(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    // loks add share button in galleryvc
    @IBAction func actionShare(_ sender: Any) {
        print(self.currentPage)
        let iPath = IndexPath(item: self.currentPage, section: 0)
        
        let cell = self.collectionFull.cellForItem(at: iPath) as! CollectionFullImageCell
        UIGraphicsBeginImageContextWithOptions(cell.ContentView.frame.size, true, 0)
        cell.ContentView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()        // set up activity view controller
        let imageToShare = [ image! ]
        
        let activitySheet = UIActivityViewController(activityItems:imageToShare, applicationActivities: nil)
        self.present(activitySheet, animated: true, completion: nil)

    }
    
    
    @IBAction func actionDownload(_ sender: UIButton) {
        print(self.currentPage)
        let iPath = IndexPath(item: self.currentPage, section: 0)
        
        let cell = self.collectionFull.cellForItem(at: iPath) as! CollectionFullImageCell
        
//        if let imageObj = cell.image.image{
//           UIImageWriteToSavedPhotosAlbum(imageObj, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)
//        }
      //  UIGraphicsBeginImageContext(cell.ContentView.frame.size)
        UIGraphicsBeginImageContextWithOptions(cell.ContentView.frame.size, true, 0)
        cell.ContentView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        UIImageWriteToSavedPhotosAlbum(image!, self, #selector(image(_:didFinishSavingWithError:contextInfo:)), nil)

    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!

        } else {
            GlobalUtility.showToastMessage(msg: "Saved")
        }
    }
    
}

extension GalleryViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrLogImageModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let imageModel = self.arrLogImageModel[indexPath.item]
        if (collectionView == self.collectionFull){
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionFullImageCell", for: indexPath) as! CollectionFullImageCell
            cell.loadCellUI(model: imageModel)
            return cell

        }
        else{
           let  cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionThumbCell", for: indexPath) as! CollectionThumbCell
            cell.loadCellUI(model: imageModel)
            if indexPath.item == self.currentPage{
                        cell.image.layer.borderColor = UIColor.white.cgColor
                        cell.image.layer.borderWidth = 1.0
                        cell.layer.masksToBounds = true
            }
            else{
                cell.image.layer.borderColor = UIColor.clear.cgColor
                cell.image.layer.borderWidth = 0.0
                cell.layer.masksToBounds = true
            }
            return cell

        }
        
    }
    

    
}

extension GalleryViewController : UIScrollViewDelegate{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        guard scrollView == self.collectionFull else {
            return
        }
        let center = CGPoint(x: scrollView.contentOffset.x + (scrollView.frame.width / 2), y: (scrollView.frame.height / 2))
        if let ip = self.collectionFull.indexPathForItem(at: center){
            self.currentPage = ip.item
            print(ip.item)
            self.collectionThumb.reloadData()
        }
        
    }

}


extension GalleryViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        guard collectionView == self.collectionFull else {
            return CGSize(width: 75, height: 75)
        }
        
      //  return CGSize(width: self.view.frame.size.width, height: 312 * SCREEN_HEIGHT_RATIO)
       return CGSize(width: self.view.frame.size.width, height: self.view.frame.size.width)
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if collectionView == self.collectionThumb{
            //print("selected thumb = \(indexPath.item)")
            self.currentPage = indexPath.item
            self.collectionThumb.reloadData()
            self.collectionFull.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: false)
        }


    }
}


