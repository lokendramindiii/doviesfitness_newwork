//
//	GetAllPackage.swift
//
//	Create by hb on 19/4/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ModelGetAllPackage : NSObject, NSCoding{

	var iosPackageId : String!
	var packageAmount : String!
	var packageMasterId : String!
    var packageDescription : String!
	var packageName : String!
	var packageText : String!
    var colorcode : String!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		iosPackageId = dictionary["ios_package_id"] as? String ?? ""
		packageAmount = dictionary["package_amount"] as? String ?? ""
		packageMasterId = dictionary["package_master_id"] as? String ?? ""
        colorcode = dictionary["color_code"] as? String ?? ""

		packageName = dictionary["package_name"] as? String ?? ""
		packageText = dictionary["package_text"] as? String ?? ""
        packageDescription = dictionary["description"] as? String ?? ""
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if iosPackageId != nil{
			dictionary["ios_package_id"] = iosPackageId
		}
		if packageAmount != nil{
			dictionary["package_amount"] = packageAmount
		}
		if packageMasterId != nil{
			dictionary["package_master_id"] = packageMasterId
		}
        if colorcode != nil{
            dictionary["color_code"] = colorcode
        }

		if packageName != nil{
			dictionary["package_name"] = packageName
		}
		if packageText != nil{
			dictionary["package_text"] = packageText
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         iosPackageId = aDecoder.decodeObject(forKey: "ios_package_id") as? String
         packageAmount = aDecoder.decodeObject(forKey: "package_amount") as? String
         packageMasterId = aDecoder.decodeObject(forKey: "package_master_id") as? String
         packageName = aDecoder.decodeObject(forKey: "package_name") as? String
         packageText = aDecoder.decodeObject(forKey: "package_text") as? String
        colorcode = aDecoder.decodeObject(forKey: "color_code") as? String
        
	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if iosPackageId != nil{
			aCoder.encode(iosPackageId, forKey: "ios_package_id")
		}
		if packageAmount != nil{
			aCoder.encode(packageAmount, forKey: "package_amount")
		}
		if packageMasterId != nil{
			aCoder.encode(packageMasterId, forKey: "package_master_id")
		}
		if packageName != nil{
			aCoder.encode(packageName, forKey: "package_name")
		}
		if packageText != nil{
			aCoder.encode(packageText, forKey: "package_text")
		}
        if colorcode != nil{
            aCoder.encode(colorcode, forKey: "color_code")
        }
	}

}
