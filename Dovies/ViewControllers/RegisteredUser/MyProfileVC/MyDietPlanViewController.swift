//
//  MyDietPlanViewController.swift
//  Dovies
//
//  Created by Neel Shah on 26/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class MyDietPlanViewController: BaseViewController {
	
	@IBOutlet weak var IBcollectionDiet: BaseCollectionView!
	@IBOutlet weak var IBviewAddDiet: UIView!
	
    var arrDietPlans : [DietPlan] = [DietPlan]()
	
	static let cellIndentifier = "DietPlanCollectionCell"
	static let cellPadding : CGFloat = 5.0
	//let cellSizeValue : CGFloat  = (ScreenSize.width - (ProgramsViewController.cellPadding))/2
   // let cellSizeValue : CGFloat  = (ScreenSize.width - 1)/2  - 30-11-2019
    let cellSizeValue : CGFloat  = (ScreenSize.width - 62)/2

    
	var placeholderCellsCount = 0

    var shouldLoadMore = false
    var currentPageIndex = 1
    
    //MARK: ViewLifeCycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.minimumInteritemSpacing = 20
        layout.minimumLineSpacing = 20
        IBcollectionDiet.collectionViewLayout = layout

		self.navigationItem.title = "My Diet Plan".uppercased()
		self.addNavigationWhiteBackButton()
		self.inititalUISetUp()
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
        
   //     self.navigationController?.navigationBar.barTintColor = UIColor.appNavColor()
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white ,
                                                                        NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]
        
		self.IBviewAddDiet.isHidden = true
		if arrDietPlans.count == 0{
			//shimmer loader setup
			DispatchQueue.main.async { [weak self] in
				guard let weakSelf = self else{return}
				weakSelf.placeholderCellsCount = 0
				weakSelf.IBcollectionDiet.reloadData()
				weakSelf.IBcollectionDiet.layoutIfNeeded()
				weakSelf.IBcollectionDiet.showLoader()
			}
		}
        currentPageIndex = 1
		self.callMyDietPlanWs()
	}
	
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        //UIApplication.shared.statusBarStyle = .default
      //  self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [
            NSAttributedStringKey.foregroundColor : UIColor.colorWithRGB(r: 34.0, g: 34.0, b: 34.0),
            NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]
    }
    
    
	deinit{
		NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "AddDietPlan"), object: nil)
	}
	
	override func inititalUISetUp() {
        
        IBcollectionDiet.backgroundColor = UIColor.appBlackColorNew()
		IBcollectionDiet.register(UINib(nibName: MyDietPlanViewController.cellIndentifier, bundle: nil), forCellWithReuseIdentifier: MyDietPlanViewController.cellIndentifier)
		
		IBcollectionDiet.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
	}
	
    /**
     This method used to fetch user diet plans and update the collection view
     */
	@objc func callMyDietPlanWs(){
        DoviesWSCalls.callGetCustomerDiet(dicParam: [WsParam.pageIndex : "\(self.currentPageIndex)"], onSuccess: { (success, arrDietPlan, msg, hasNextPage) in
            self.shouldLoadMore = hasNextPage
			if success{
                if self.currentPageIndex == 1{
                    self.arrDietPlans = arrDietPlan
                }else{
                    self.arrDietPlans += arrDietPlan
                }
                if hasNextPage{
                    self.currentPageIndex += 1
                }
			}
			if self.arrDietPlans.count > 0{
				self.IBviewAddDiet.isHidden = true
				self.addRightNavigationButton()
			}
			else{
				self.IBviewAddDiet.isHidden = false
			}
			self.placeholderCellsCount = 0
			self.IBcollectionDiet.hideLoader()
			self.IBcollectionDiet.reloadData()
		}) { (error) in
			
		}
	}
	
	func addRightNavigationButton(){
		let addButton = UIButton(type: .custom)
      //  addButton.setImage(UIImage(named:"pluce_icon_s26_h"), for: .normal)
		addButton.setImage(UIImage(named:"pluce_icon_s26_white"), for: .normal)
        
		addButton.addTarget(self, action: #selector(self.didAddButton), for: .touchUpInside)
		addButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
		let addBarItem = UIBarButtonItem(customView: addButton)
		navigationItem.rightBarButtonItems = [addBarItem]
	}
	
	@IBAction func didAddButton(sender: AnyObject){
		
		NotificationCenter.default.addObserver(self, selector: #selector(callMyDietPlanWs), name: NSNotification.Name(rawValue: "AddDietPlan"), object: nil)
		
		self.redirectToTab(tabIndex: 2)
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let dietPlanVc = segue.destination as? DietPlanDetailsVC {
			dietPlanVc.selectedPlan = sender as? DietPlan
		}
    }
}

extension MyDietPlanViewController : UICollectionViewDataSource{
	
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
		return arrDietPlans.count > 0 ? arrDietPlans.count : placeholderCellsCount
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MyDietPlanViewController.cellIndentifier, for: indexPath) as? DietPlanCollectionCell else{return UICollectionViewCell()}
		if arrDietPlans.count > 0{
            cell.btnMore.isHidden = false
            cell.setCellUI(with: arrDietPlans[indexPath.item])
            cell.moreButtonAction = { (dPlan) in
                if let plan = dPlan{
                    self.showActionSheet(at : plan)
                }
                
            }
		}
		return cell
	}
    
    func showActionSheet(at dietPlanObj : DietPlan){
//        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
//        {

            let arrData: [alertActionData]!
            arrData = [
                alertActionData(title: "Delete", imageName: "btn_del_s21a", highlighedImageName : "btn_del_s21a ")
            ]
            DoviesGlobalUtility.showDefaultActionSheet(on:self,actionData: arrData,cancelTitle: "Cancel") { (clickedIndex:Int, isCancel:Bool) in
                if clickedIndex == 0{
                    self.callDeleteDietPlanApi(at : dietPlanObj)
                }
            }
      //  }
        /*
        else{
        
            let arrData: [alertActionData]!
            arrData = [
                alertActionData(title: dietPlanObj.dietPlanFavStatus == false ? "Favourite" : "Unfavourite", imageName: dietPlanObj.dietPlanFavStatus == false ? "btn_fav_s21a" : "favorite_icon_s34_h", highlighedImageName : "favorite_icon_s34_h"),
                alertActionData(title: "Delete", imageName: "btn_del_s21a", highlighedImageName : "btn_del_s21a ")
            ]
            DoviesGlobalUtility.showDefaultActionSheet(on:self,actionData: arrData,cancelTitle: "Cancel") { (clickedIndex:Int, isCancel:Bool) in
                if clickedIndex == 0{
                    self.callAddToFavouriteAPI(at : dietPlanObj)
                }else if clickedIndex == 1{
                    self.callDeleteDietPlanApi(at : dietPlanObj)
                }
            }
        } */
    }
    
    func callDeleteDietPlanApi(at dietPlanObj : DietPlan){
        
        UIAlertController.showAlert(in: self, withTitle: AlertTitle.appName, message: AlertMessages.deleteConfirmation, cancelButtonTitle: "No", destructiveButtonTitle: "Delete", otherButtonTitles: nil) { (alertControl, action, index) in
            if index == 1{
                
                guard let cUser = DoviesGlobalUtility.currentUser else{return}
                var parameters = [String : Any]()
                parameters[WsParam.authCustomerId] =  cUser.customerAuthToken
                parameters[WsParam.dietPlanId] = dietPlanObj.dietPlanId
                
                if let index = self.arrDietPlans.index(where: {$0.dietPlanId == dietPlanObj.dietPlanId}), self.arrDietPlans.count > index{
                    self.arrDietPlans.remove(at: index)
                    self.IBviewAddDiet.isHidden = !(self.arrDietPlans.count == 0)
                    self.IBcollectionDiet?.performBatchUpdates({() -> Void in self.IBcollectionDiet.deleteItems(at: [IndexPath(item: index, section: 0)])}, completion: nil)
                    
                }
                
                DoviesWSCalls.callDeleteDietPlanAPI(dicParam: parameters, onSuccess: { (success, message) in
                }) {  (error) in
                }
            }
        }
    }
    
    func callAddToFavouriteAPI(at dietPlanObj : DietPlan){
        guard let cUser = DoviesGlobalUtility.currentUser else{return}
        var parameters = [String : Any]()
        parameters[WsParam.authCustomerId] =  cUser.customerAuthToken
        parameters[WsParam.moduleId] = dietPlanObj.dietPlanId
        parameters[WsParam.moduleName] = ModuleName.dietPlan
        dietPlanObj.dietPlanFavStatus = !dietPlanObj.dietPlanFavStatus
        DoviesWSCalls.callFavouriteAPI(dicParam: parameters, onSuccess: { (success, message, response) in
            if !success{
                dietPlanObj.dietPlanFavStatus = !dietPlanObj.dietPlanFavStatus
            }
        }) {  (error) in
            dietPlanObj.dietPlanFavStatus = !dietPlanObj.dietPlanFavStatus
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == (arrDietPlans.count - 1) && shouldLoadMore == true {
            shouldLoadMore = false
            callMyDietPlanWs()
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionFooter:
            guard let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,withReuseIdentifier: BaseCollectionView.collectionViewLoadMoreIdentifier, for: indexPath) as? CollectionLoadMoreView else{return UICollectionReusableView()}
            return footerView
            
        default:
            return UIView() as! UICollectionReusableView
        }
    }
    
}
extension MyDietPlanViewController : UICollectionViewDelegate{
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
	{
		if arrDietPlans.count > 0{
            let model = self.arrDietPlans[indexPath.item]
            if model.dietPlanAccessLevel.uppercased() == "OPEN".uppercased() || DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName{
                let pdfViewer = Storyboard.DietPlan.storyboard().instantiateViewController(withIdentifier: "DietPdfViewController") as! DietPdfViewController
                pdfViewer.strTitle = arrDietPlans[indexPath.item].dietPlanTitle
                pdfViewer.strUrl = arrDietPlans[indexPath.item].dietPlanPdf
                self.navigationController?.pushViewController(pdfViewer, animated: true)
            }else{
                if let upgradeVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "UpgradeToPremiumViewController") as? UpgradeToPremiumViewController{
                    upgradeVC.onPurchaseSuccess = {
                        self.callMyDietPlanWs()
                    }
                    self.navigationController?.pushViewController(upgradeVC, animated: true)
                }
            }
		}
	}
	
}

extension MyDietPlanViewController : UICollectionViewDelegateFlowLayout{
	
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return shouldLoadMore ? CGSize(width: ScreenSize.width, height: HeightConstants.baseCollectionViewFooterHeight) : CGSize.zero
    }
    
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
		return CGSize(width: cellSizeValue, height: cellSizeValue)
	}
	
    
    // 30-11-2019
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsets(top: 0, left: 0, bottom: MyDietPlanViewController.cellPadding, right: 0)
//    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
    }

}
