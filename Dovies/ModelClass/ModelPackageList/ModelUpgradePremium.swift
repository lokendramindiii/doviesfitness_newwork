//
//	Data.swift
//
//	Create by hb on 19/4/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation

class ModelUpgradePremium : NSObject, NSCoding{

	var getAllPackages : [ModelGetAllPackage]!
	var getStaticContent : [ModelGetStaticContent]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		getAllPackages = [ModelGetAllPackage]()
		if let getAllPackagesArray = dictionary["get_all_packages"] as? [[String:Any]]{
			for dic in getAllPackagesArray{
				let value = ModelGetAllPackage(fromDictionary: dic)
				getAllPackages.append(value)
			}
		}
		getStaticContent = [ModelGetStaticContent]()
		if let getStaticContentArray = dictionary["get_static_content"] as? [[String:Any]]{
			for dic in getStaticContentArray{
				let value = ModelGetStaticContent(fromDictionary: dic)
				getStaticContent.append(value)
			}
		}
        
        if let getSuccessStoryArray = dictionary["get_success_story"] as? [[String:Any]]{
            let value = ModelGetStaticContent(fromStoryImages: getSuccessStoryArray)
            getStaticContent.append(value)
        }
        
        if let getSubscriptionArray = dictionary["get_subscription_terms"] as? [[String:Any]]{
            let value = ModelGetStaticContent(fromSubscription: getSubscriptionArray)
            getStaticContent.append(value)
        }
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if getAllPackages != nil{
			var dictionaryElements = [[String:Any]]()
			for getAllPackagesElement in getAllPackages {
				dictionaryElements.append(getAllPackagesElement.toDictionary())
			}
			dictionary["get_all_packages"] = dictionaryElements
		}
		if getStaticContent != nil{
			var dictionaryElements = [[String:Any]]()
			for getStaticContentElement in getStaticContent {
				dictionaryElements.append(getStaticContentElement.toDictionary())
			}
			dictionary["get_static_content"] = dictionaryElements
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         getAllPackages = aDecoder.decodeObject(forKey :"get_all_packages") as? [ModelGetAllPackage]
         getStaticContent = aDecoder.decodeObject(forKey :"get_static_content") as? [ModelGetStaticContent]

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if getAllPackages != nil{
			aCoder.encode(getAllPackages, forKey: "get_all_packages")
		}
		if getStaticContent != nil{
			aCoder.encode(getStaticContent, forKey: "get_static_content")
		}

	}

}
