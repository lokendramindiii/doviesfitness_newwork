//
//  CommentsCell.swift
//  Dovies
//
//  Created by hb on 22/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class CommentsCell: UITableViewCell {
	
	@IBOutlet weak var imgViewProfile: UIImageView!
	@IBOutlet weak var lblName: UILabel!
	@IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var topSepratorLine: UIImageView!

	@IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var btnMore : UIButton!
    
 //   var cellIndex : IndexPath?
     var moreButtonAction:((IndexPath?)->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnMoreTapped(_ sender: UIButton){
       // moreButtonAction?(cellIndex)
        let idxPath : IndexPath = IndexPath(row: sender.tag , section: 0)
        moreButtonAction?(idxPath)

    }

}
