//
//	WorkoutList.swift
//
//	Create by hb on 12/3/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ModelWorkoutList : NSObject, NSCoding{

	var workoutGroupDescription : String!
	var workoutGroupId : String!
	var workoutGroupName : String!
	var workoutId : String!
	var workoutImage : String!
	var workoutLevel : String!
	var workoutName : String!
	var workoutTotalWorkTime : String!
	var workoutExerciseCount: String!
	var workoutFavStatus: Bool!
	var workoutShareUrl: String!

	var workoutCategory : String!
	var workoutTime : String!
	var workoutAccessLevel : String!
	var customerWeight : String!
	var logDate : String!
	var workoutNote : String!
	var workoutTotalTime : String!
	var workoutDescription: String!
	var workoutLogId: String!
    var workoutFeedbackStatus: String!
    var isNew : Bool!

    var arrWorrkoutLogImages = [ModelWorkoutLogListImage]()
	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
        
        if let new = dictionary["is_new"] as? String , new == "1"{
            isNew = true
        }else if  let newStatus = dictionary["is_new"] as? Bool{
            isNew = newStatus
        }else{
            isNew = false
        }
		workoutGroupDescription = dictionary["workout_group_description"] as? String ?? ""
		workoutDescription = dictionary["workout_description"] as? String ?? ""
		workoutGroupId = dictionary["workout_group_id"] as? String ?? ""
		workoutGroupName = dictionary["workout_group_name"] as? String ?? ""
		workoutId = dictionary["workout_id"] as? String ?? ""
		workoutImage = dictionary["workout_image"] as? String ?? ""
		workoutLevel = dictionary["workout_level"] as? String ?? ""
		workoutName = dictionary["workout_name"] as? String ?? ""
		workoutTotalWorkTime = dictionary["workout_total_work_time"] as? String ?? ""
		workoutExerciseCount = dictionary["workout_exercise_count"] as? String ?? ""
		workoutShareUrl = dictionary["workout_share_url"] as? String ?? ""
        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        {
            workoutAccessLevel = "OPEN"
        }
        else
        {
            workoutAccessLevel = dictionary["workout_access_level"] as? String ?? ""
        }
		workoutCategory = dictionary["workout_category"] as? String ?? ""
		workoutTime = dictionary["workout_time"] as? String ?? ""
		workoutLogId = dictionary["workout_log_id"] as? String ?? ""
		customerWeight = dictionary["customer_weight"] as? String ?? ""
		if customerWeight.lowercased() == "0 kg"{
			customerWeight = " NA"
		}
		else if customerWeight.lowercased() == "0 lbs"{
			customerWeight = " NA"
		}
		logDate = dictionary["log_date"] as? String ?? ""
		workoutNote = dictionary["workout_note"] as? String ?? ""
		workoutTotalTime = dictionary["workout_total_time"] as? String ?? ""
        workoutFeedbackStatus = dictionary["feedback_status"] as? String ?? ""
		if let likeStatus = dictionary["workout_fav_status"] as? String , likeStatus == "1"{
			workoutFavStatus = true
		}else if  let bLikeStatus = dictionary["workout_fav_status"] as? Bool{
			workoutFavStatus = bLikeStatus
		}else{
			workoutFavStatus = false
		}
        
        if let logImages = dictionary["workout_log_images"] as? [Dictionary<String, Any>]{
            for dict in logImages {
                let obj =  ModelWorkoutLogListImage(imageURL:dict["log_image"] as? String ?? "",  dateStr: dict["log_date"] as? String ?? "" ,CustomerWeightStr: customerWeight)
                self.arrWorrkoutLogImages.append(obj)
            }
        }
        
	}

	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if workoutGroupDescription != nil{
			dictionary["workout_group_description"] = workoutGroupDescription
		}
		if workoutGroupId != nil{
			dictionary["workout_group_id"] = workoutGroupId
		}
		if workoutGroupName != nil{
			dictionary["workout_group_name"] = workoutGroupName
		}
		if workoutId != nil{
			dictionary["workout_id"] = workoutId
		}
		if workoutImage != nil{
			dictionary["workout_image"] = workoutImage
		}
		if workoutLevel != nil{
			dictionary["workout_level"] = workoutLevel
		}
		if workoutName != nil{
			dictionary["workout_name"] = workoutName
		}
		if workoutTotalWorkTime != nil{
			dictionary["workout_total_work_time"] = workoutTotalWorkTime
		}
        if workoutFeedbackStatus != nil{
            dictionary["feedback_status"] = workoutFeedbackStatus
        }
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         workoutGroupDescription = aDecoder.decodeObject(forKey: "workout_group_description") as? String
         workoutGroupId = aDecoder.decodeObject(forKey: "workout_group_id") as? String
         workoutGroupName = aDecoder.decodeObject(forKey: "workout_group_name") as? String
         workoutId = aDecoder.decodeObject(forKey: "workout_id") as? String
         workoutImage = aDecoder.decodeObject(forKey: "workout_image") as? String
         workoutLevel = aDecoder.decodeObject(forKey: "workout_level") as? String
         workoutName = aDecoder.decodeObject(forKey: "workout_name") as? String
         workoutTotalWorkTime = aDecoder.decodeObject(forKey: "workout_total_work_time") as? String
        workoutFeedbackStatus = aDecoder.decodeObject(forKey: "feedback_status") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if workoutGroupDescription != nil{
			aCoder.encode(workoutGroupDescription, forKey: "workout_group_description")
		}
		if workoutGroupId != nil{
			aCoder.encode(workoutGroupId, forKey: "workout_group_id")
		}
		if workoutGroupName != nil{
			aCoder.encode(workoutGroupName, forKey: "workout_group_name")
		}
		if workoutId != nil{
			aCoder.encode(workoutId, forKey: "workout_id")
		}
		if workoutImage != nil{
			aCoder.encode(workoutImage, forKey: "workout_image")
		}
		if workoutLevel != nil{
			aCoder.encode(workoutLevel, forKey: "workout_level")
		}
		if workoutName != nil{
			aCoder.encode(workoutName, forKey: "workout_name")
		}
		if workoutTotalWorkTime != nil{
			aCoder.encode(workoutTotalWorkTime, forKey: "workout_total_work_time")
		}
        
        if workoutFeedbackStatus != nil{
            aCoder.encode(workoutFeedbackStatus, forKey: "feedback_status")
        }

	}

}
