//
//  UpgradeToPremiumCell.swift
//  Dovies
//
//  Created by hb on 18/04/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import TTTAttributedLabel

protocol UpgradeToPremium_RedirectPrivacyPolicy {
    
    func RedirectToPrivacyPolicy(_ PrivacyPolicy: String)
}

class UpgradeToPremiumCell: UITableViewCell , UITextViewDelegate {
    
    @IBOutlet weak var lblPrivacyPolicy : TTTAttributedLabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var txtContent: UITextView!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var storyImageScollview: UIScrollView!
    
    var arrStoryImages = [ModelStoryImage]()
    
    var delegate: UpgradeToPremium_RedirectPrivacyPolicy?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func setData(getStaticContent:ModelGetStaticContent){
        
        lblPrivacyPolicy.isUserInteractionEnabled = true
        lblTitle.text = getStaticContent.title
        lblTitle.font = UIFont.tamilBold(size: 16)
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 7.0
        
        let attr: [NSAttributedStringKey: Any] = [NSAttributedStringKey.font :UIFont.tamilRegular(size: 14.0)]//, NSAttributedStringKey.kern : 1.5
        let attrStrComment1 = NSMutableAttributedString(string: getStaticContent.content, attributes: attr)
        attrStrComment1.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attrStrComment1.length))
        // txtContent.attributedText = attrStrComment1
        lblPrivacyPolicy.attributedText = attrStrComment1
        
        // Image  ModelGetStaticContent in   ModelStoryImage class
        if getStaticContent.arrStoryImage.count > 0
        {
            self.storyImageScollview.isHidden = false
            
            var Xaxis = 0
            for i in 0...getStaticContent.arrStoryImage.count - 1
            {
                print(i)
                var imageView : UIImageView
                imageView  = UIImageView(frame:CGRect(x:Xaxis, y:0, width:112, height:112));
                Xaxis = Int(imageView.frame.size.width) +  Xaxis + 5
              //  imageView.sd_setImage(with: URL(string:getStaticContent.arrStoryImage[i].imageUrl), placeholderImage: AppInfo.placeholderImage)
                
                imageView.sd_setImage(with: URL(string:getStaticContent.arrStoryImage[i].imageUrl), placeholderImage: nil)

                self.storyImageScollview.addSubview(imageView)
            }
            
            var btnRight : UIButton
            btnRight  = UIButton(frame:CGRect(x:ScreenSize.width - 40 , y:(storyImageScollview.frame.size.height + 27) / 2 , width:20, height:54));
            btnRight.setImage(UIImage (named: "btn_rightarw_s20"), for: .normal)
            self.contentView.addSubview(btnRight)
            storyImageScollview.contentSize = CGSize(width: Xaxis, height: 112)

        }
        else
        {
            self.storyImageScollview.isHidden = true
        }
        
        if getStaticContent.arrSubscriptionTerms.count > 0
        {
            
            lblTitle.isHidden = false
            lblTitle.text = getStaticContent.arrSubscriptionTerms[0].subscriptionTitle
            lblTitle.font = UIFont.tamilBold(size: 13)
            txtContent.isHidden = true
            
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 5.0
            
            let yourOtherAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.gray , .font: UIFont.tamilRegular(size: 10.0)]
            
            var subscriptionContent = getStaticContent.arrSubscriptionTerms[0].subscriptionContent ?? ""
            
            subscriptionContent = subscriptionContent.replacingOccurrences(of: "&apos;", with: "'")
            
            let partOne = NSMutableAttributedString(string: subscriptionContent + "\n Privacy Policy and Terms And Conditions", attributes: yourOtherAttributes)
            
            partOne.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: partOne.length))
            
            let text = subscriptionContent + "  Privacy Policy and Terms And Conditions "
                        
            
            self.lblPrivacyPolicy.delegate = self
            self.lblPrivacyPolicy.setText(partOne)
            
            let subscriptionNoticeLinkAttributes = [
                NSAttributedStringKey.font: UIFont.tamilBold(size: 12.0),
                NSAttributedStringKey.underlineStyle: NSNumber(value:true),
                ]
            lblPrivacyPolicy.linkAttributes = subscriptionNoticeLinkAttributes
            
            let subscriptionNoticeActiveLinkAttributes = [
                NSAttributedStringKey.backgroundColor: UIColor.lightGray,
                NSAttributedStringKey.underlineStyle: NSNumber(value:false),
                ]
            
            lblPrivacyPolicy.activeLinkAttributes = subscriptionNoticeActiveLinkAttributes
            
            let range1 = (text as NSString).range(of: "Terms And Conditions")
            self.lblPrivacyPolicy.addLink(to: NSURL(string: "URL_TERMS_AND_CONDITIONS") as URL?, with:range1)
            
            let range2 = (text as NSString).range(of: "Privacy Policy")
            self.lblPrivacyPolicy.addLink(to: NSURL(string: "URL_PRIVACY_POLICY") as URL?, with:range2)
            
        }
        else
        {
            self.lblTitle.isHidden = false
        }
        
    }
    
    @IBAction func btnPrivacyPolicyTapped(){
        
        print("Privacy Policy")
        self.delegate?.RedirectToPrivacyPolicy("Privacy Policy")
    }
    
    @IBAction func btnTermsTapped(){
        self.delegate?.RedirectToPrivacyPolicy("Terms And Conditions")
    }

}

extension UpgradeToPremiumCell: TTTAttributedLabelDelegate {
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        if url.absoluteString == "URL_TERMS_AND_CONDITIONS"{
            self.btnTermsTapped()
        }
        else if url.absoluteString == "URL_PRIVACY_POLICY"{
            self.btnPrivacyPolicyTapped()
        }
    }
}


