//
//  CreateWorkoutAdminVC.swift
//  Dovies
//
//  Created by hb on 23/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

let cell_adminTag = 100000
let timeTxtfld_adminTag = 10000
let repsTxtfld_adminTag = 1000
let totalTimeTxtfld_adminTag = 100
let breakTimeTxtfld_adminTag = 10


class CreateWorkoutAdminVC: BaseViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UITextViewDelegate , UIScrollViewDelegate {
    

    var Level : String = "Level"

    var isEditSelected : Bool = false
    var isCreatedByYou: Bool = false
    var IsEditedByAdmin: Bool = false

    var timeMinute : String = "Min"
    var timeSecond : String = "Sec"
    var parentExeID : String!
    var  arrLevel: [FilterGroup]!
    var btnSelected : UIButton = UIButton()
    var btnTimeSelected = true
    static var arrExercise = [Any]()
    static var arrTempExercises = [ExerciseDetails]()
    var exeTime : String = ""
    var exeReps : String = ""
    var exeBreakTime : String = ""
    @IBOutlet var workoutTblView: UITableView!
    
    var shouldLoadMore = false

    var headerViewAdmin :AddworkoutHeaderAdminView!
    var dragger: TableViewDragger!
    var imageUpdated = false
    let validator = Validator()
    var arrLevelListing = [FilterDataModel]()
    var goodForListing:GoodForListingVC?
    var equipmentList : EquipmentListVC?
    var  arrFilterData: [FilterGroup] = DoviesGlobalUtility.arrFilterData
    var modelWorkoutDetailData: ModelWorkoutDetail!
    var ExerciseDetailsData : ExerciseDetails?
    var arExerciseDetailsData : [ModelWorkoutExerciseList]?
    var arrSelectedGoodfor = [String]()
    var arrGoodforId = [String]()
    var arrSelectedequips = [String]()
    var arrEquipsId = [String]()
    var arrSelectedEquipsID = [String]()
    var arrSelectedEquipsList = [String]()
    
    var isForEditWork:(() -> ())?
    var updateUiFromCreateWorkoutAdmin :((_ isupdate:Bool) ->())?

    
    // Mindiii add userlist
    var userlist : UserListVC?
    var arrSelecteduser = [String]()
    var arrUserId = [String]()
    var arrUserListing = [ModelUserlist]()

    // Mindiii add workout group
    var strSelectedWorkout = String()
    var strWorkoutId = String()
    var arrWorkoutListing = [ModelWorkoutCollection]()

    // Mindiii add Created by
    var strGuestName = String()
    var strGuestId = String()
    var arrCreatedByListing = [ModelCreatedBy]()
    
    //Mindiii Video section add
    var ValidationRow = -1
    var IsVideoOpen = ""

    var selectedRow = -1
    var visibleCellsDuringAnimation = [UITableViewCell]()
    var playerHeight = 179.5 * CGRect.widthRatio
    let cellVideoHeight:CGFloat = 120 + (185 * CGRect.widthRatio)
    
    var createWrokoutAdminModel = CreateWorkoutAdminModel()
    var defaultsetAdmin: UserDefaults!

    var isNewworkout = false

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
      
        defaultsetAdmin = UserDefaults.standard
        
        if  defaultsetAdmin.object(forKey: "allTimerExcercises") != nil || defaultsetAdmin.object(forKey: "numberOfRepas") != nil || defaultsetAdmin.object(forKey: "timeFinishEachExcercise") != nil
        {
        }
        else{
            defaultsetAdmin.set("00:30", forKey: "allTimerExcercises")
            defaultsetAdmin.set("10", forKey: "numberOfRepas")
            defaultsetAdmin.set("00:30", forKey: "timeFinishEachExcercise")
        }
        if self.navigationController?.navigationBar.barTintColor == UIColor.white
        {
        }
        else
        {
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white,
                                                                            NSAttributedStringKey.font: UIFont.tamilBold(size: 16.0)]
        }
        DoviesGlobalUtility.setFilterData()
        
       // self.callGetcustomerUserListingWS()
        
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(appMovedToBackground), name: Notification.Name.UIApplicationWillResignActive, object: nil)
        
        self.navigationItem.title = "Create workout".uppercased()
        self.navigationItem.leftBarButtonItem = GlobalUtility.getNavigationButtonItemWith(target: self, selector: #selector(self.cancelButtonTapped), title: "Cancel")
        self.setDetail()
        self.getArrayDetails()
        self.workoutTblView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 5, right: 0)
        self.mapExerciseData()
        
        self.callGetcustomerUserListingNewWS()

    }
    
    @objc func appMovedToBackground() {
        if selectedRow > -1{
            let idxPath : IndexPath = IndexPath(row: selectedRow, section: 0)
            closeVideoPlayForCell(indexPath: idxPath)
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        if selectedRow > -1{
            let selectedCell = workoutTblView.cellForRow(at: IndexPath(row: selectedRow, section: 0)) as! CreateWorkoutCell
            selectedCell.stopVideo()
        }
    }
    
    deinit{
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIApplicationWillResignActive, object: nil)
        if selectedRow > -1{
            let selectedCell = workoutTblView.cellForRow(at: IndexPath(row: selectedRow, section: 0)) as! CreateWorkoutCell
            selectedCell.removePlayerFromView()
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.headerViewAdmin.btnGoodFor.addTarget(self, action: #selector(self.didTapGoodforButton), for: .touchUpInside)
        print("callExerciseListingAPI\(CreateWorkoutAdminVC.arrExercise)")
        workoutTblView.reloadData()
        workoutTblView.layoutIfNeeded()
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        sizeHeaderToFit()
        if IS_IPHONE_X
        {
            self.workoutTblView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 40, right: 0)
        }
        else{
            self.workoutTblView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 75, right: 0)
        }
    }
    
    @objc func cancelButtonTapped(_ sender: UIButton){
        self.setCancelButtonValidation()
    }
    
    func mapExerciseData(){
        if modelWorkoutDetailData != nil {
            let objModelWorkoutExercise: [ModelWorkoutExerciseList] = modelWorkoutDetailData.workoutExerciseList
            if objModelWorkoutExercise.count > 0 {
                for i in 0...objModelWorkoutExercise.count - 1 {
                    let objExerInfo = ExerciseDetails(fromDictionary: [String : Any]())
                    // loks add below line
                    objExerInfo.exerciseVideo = objModelWorkoutExercise[i].workoutExerciseVideo
                    objExerInfo.exerciseAccessLevel = objModelWorkoutExercise[i].workoutAccessLevel
                    
                    objExerInfo.exerciseId = objModelWorkoutExercise[i].workoutExercisesId
                    objExerInfo.exerciseImage = objModelWorkoutExercise[i].workoutExerciseImage
                    objExerInfo.exerciseName = objModelWorkoutExercise[i].workoutExerciseName
                    objExerInfo.breakTimeValue = String(objModelWorkoutExercise[i].workoutExerciseBreakTime.dropFirst(3))
                    objExerInfo.timeValue = String(objModelWorkoutExercise[i].workoutExercisesTime.dropFirst(3))
                    objExerInfo.excerciseOption = objModelWorkoutExercise[i].workoutExerciseType
                    objExerInfo.repsValue = objModelWorkoutExercise[i].workoutRepeatText
                    objExerInfo.totalTimeValue = String(objModelWorkoutExercise[i].workoutExercisesTime.dropFirst(3))
                    CreateWorkoutAdminVC.arrExercise.append(objExerInfo)
                }
                
            }
            print(CreateWorkoutAdminVC.arrExercise)
        }
        if isEditSelected == true {
            
            arrGoodforId = modelWorkoutDetailData.workoutGoodForIds.components(separatedBy: ",")
            arrSelectedGoodfor = modelWorkoutDetailData.workoutGoodFor.components(separatedBy:" | ")
            
            arrEquipsId = modelWorkoutDetailData.workoutEquipmentIds.components(separatedBy: ",")
            
            arrSelectedequips = modelWorkoutDetailData.workoutEquipment.components(separatedBy:" | ")
            

            if  let text = modelWorkoutDetailData.workoutAllowedUsernameID , text.isEmpty
            {
            }
            else
            {
                arrSelecteduser = modelWorkoutDetailData.workoutAllowedUsername.components(separatedBy:" | ")
                arrUserId = modelWorkoutDetailData.workoutAllowedUsernameID.components(separatedBy: ",")
            }
            self.strGuestId = modelWorkoutDetailData.creatorId
        }
    }
    
    func sizeHeaderToFit(){
        let headerView = workoutTblView.tableHeaderView!
        
        headerView.setNeedsLayout()
        headerView.layoutIfNeeded()
        
        let height = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
        var frame = headerView.frame
        frame.size.height = height
        headerView.frame = frame
        
        workoutTblView.tableHeaderView = headerView
    }
    
    func setDetail() {
        
        headerViewAdmin = MyClassadmin.instanceFromNib() as! AddworkoutHeaderAdminView
        
        headerViewAdmin.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 795 )
        
        headerViewAdmin.imgAddPhoto.layer.borderWidth = 1.0
        headerViewAdmin.imgAddPhoto.layer.borderColor = UIColor(red:231/255, green:231/255, blue:231/255, alpha: 1).cgColor
        
        headerViewAdmin.btn_allTimerExcercise.layer.borderWidth = 1.0
        headerViewAdmin.btn_allTimerExcercise.layer.borderColor =  UIColor(red:231/255, green:231/255, blue:231/255, alpha: 1).cgColor
       headerViewAdmin.btn_allTimerExcercise.setTitle(defaultsetAdmin.string(forKey: "allTimerExcercises") ?? "", for: .normal)

        
        headerViewAdmin.btn_TimeFinish_reps.layer.borderWidth = 1.0
        headerViewAdmin.btn_TimeFinish_reps.layer.borderColor =  UIColor(red:231/255, green:231/255, blue:231/255, alpha: 1).cgColor
        headerViewAdmin.btn_TimeFinish_reps.setTitle(defaultsetAdmin.string(forKey: "timeFinishEachExcercise") ?? "", for: .normal)

        headerViewAdmin.btnNoOfReps.layer.borderWidth = 1.0
        headerViewAdmin.btnNoOfReps.layer.borderColor =  UIColor(red:231/255, green:231/255, blue:231/255, alpha: 1).cgColor
        headerViewAdmin.btnNoOfReps.setTitle(defaultsetAdmin.string(forKey: "numberOfRepas") ?? "", for: .normal)

        
        workoutTblView.tableHeaderView = headerViewAdmin
        headerViewAdmin.isUserInteractionEnabled = true
        headerViewAdmin.btnClickPhoto.layer.cornerRadius = 20
        headerViewAdmin.btnClickPhoto.clipsToBounds = true
        headerViewAdmin.txtView.delegate = self
        headerViewAdmin.txtView.text = "Overview"
        let style = NSMutableParagraphStyle()
        style.lineSpacing = 1.5
        
        headerViewAdmin.txtView.typingAttributes = [NSAttributedStringKey.paragraphStyle.rawValue : style]
        headerViewAdmin.txtView.textColor = UIColor.init(r: 199.0, g: 199.0, b: 199.0, alpha: 1.0)
        headerViewAdmin.btnLevel.addTarget(self, action: #selector(self.didTapLevelButton), for: .touchUpInside)
        headerViewAdmin.btnLevelImage.addTarget(self, action: #selector(self.didTapLevelButton), for: .touchUpInside)

        headerViewAdmin.btnGoodFor.addTarget(self, action: #selector(self.didTapGoodforButton), for: .touchUpInside)
        headerViewAdmin.btnGoodForImage.addTarget(self, action: #selector(self.didTapGoodforButton), for: .touchUpInside)

        headerViewAdmin.btnEquipments.addTarget(self, action: #selector(self.didTapEquipmentsButton), for: .touchUpInside)
        headerViewAdmin.btnEquipImage.addTarget(self, action: #selector(self.didTapEquipmentsButton), for: .touchUpInside)

        headerViewAdmin.btnAllowedUser.addTarget(self, action: #selector(self.didTapAllowUserButton), for: .touchUpInside)
        headerViewAdmin.btnAllowedUserImage.addTarget(self, action: #selector(self.didTapAllowUserButton), for: .touchUpInside)

        headerViewAdmin.btnAccesslevel.addTarget(self, action: #selector(self.didTapAccesslevelButton), for: .touchUpInside)
        headerViewAdmin.btnAccesslevelImage.addTarget(self, action: #selector(self.didTapAccesslevelButton), for: .touchUpInside)

        headerViewAdmin.btnDisplayNewsfeed.addTarget(self, action: #selector(self.didTapDisplayNewsFeedButton), for: .touchUpInside)
        headerViewAdmin.btnDisplayNewsfeedImage.addTarget(self, action: #selector(self.didTapDisplayNewsFeedButton), for: .touchUpInside)

        headerViewAdmin.btnCreatedBy.addTarget(self, action: #selector(self.didTapCreatedByButton), for: .touchUpInside)
        headerViewAdmin.btnCreatedByImage.addTarget(self, action: #selector(self.didTapCreatedByButton), for: .touchUpInside)
        
        headerViewAdmin.btnAllowNotification.addTarget(self, action: #selector(self.didTapAllowNotificationButton), for: .touchUpInside)
        headerViewAdmin.btnAllowNotificationImage.addTarget(self, action: #selector(self.didTapAllowNotificationButton), for: .touchUpInside)

        headerViewAdmin.btnDefaultViewHideShow.addTarget(self, action: #selector(self.didTapDefaultViewHideshow), for: .touchUpInside)
        headerViewAdmin.btn_TimeFinish_reps.addTarget(self, action: #selector(self.didTapTimeSelection_Button(sender:)), for: .touchUpInside)
        headerViewAdmin.btn_allTimerExcercise.addTarget(self, action: #selector(self.didTapTimeSelection_Button(sender:)), for: .touchUpInside)
        headerViewAdmin.btnNoOfReps.addTarget(self, action: #selector(self.didTapNoofRepsSelection_Button(sender:)), for: .touchUpInside)
        
           headerViewAdmin.txtfldTitle.attributedPlaceholder = fun_attributedstring_forcoloradmin(main_string: "Workout Name *", string_to_color: "*")
     headerViewAdmin.btnLevel.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "Level  ", String2: "*"), for: .normal)
        headerViewAdmin.btnGoodFor.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "Good For ", String2: "*"), for: .normal)
        headerViewAdmin.btnEquipments.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "Equipments ", String2: "*"), for: .normal)
        headerViewAdmin.btnCreatedBy.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "Created By ", String2: "*"), for: .normal)
        headerViewAdmin.btnAllowedUser.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "Allowed users ", String2: ""), for: .normal)
        headerViewAdmin.btnAccesslevel.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "Access Level ", String2: "*"), for: .normal)
        headerViewAdmin.btnDisplayNewsfeed.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "Display in Newsfeed? ", String2: "*"), for: .normal)
        headerViewAdmin.btnAllowNotification.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "Allow Notification? ", String2: "*"), for: .normal)

        
        
        if isEditSelected == true{
            headerViewAdmin.btnClickPhoto.isHidden = true
            //headerViewAdmin.imgAddPhoto.sd_setImage(with:  URL(string: modelWorkoutDetailData.workoutImage), placeholderImage: AppInfo.placeholderImage)
            
            headerViewAdmin.imgAddPhoto.sd_setImage(with:  URL(string: modelWorkoutDetailData.workoutImage), placeholderImage: nil)
            
            headerViewAdmin.img_StarPhoto.image = nil
            headerViewAdmin.txtfldTitle.text = modelWorkoutDetailData.workoutName
            Level = modelWorkoutDetailData.workoutLevel
            self.headerViewAdmin.btnLevel.setImage(nil, for: .normal)
            headerViewAdmin.txtView.text = modelWorkoutDetailData.workoutDescription
            headerViewAdmin.txtView.textColor = UIColor.colorWithRGB(r: 40.0, g: 40.0, b: 40.0)
            headerViewAdmin.lblLevel.text = modelWorkoutDetailData.workoutLevel
            
            self.createWrokoutAdminModel.lbl_SelectLevel = modelWorkoutDetailData.workoutLevel
            
            if var textlevel = headerViewAdmin.lblLevel.text, !textlevel.isEmpty{
                if textlevel == "All"
                {
                 textlevel = "All Levels"
                }
                headerViewAdmin.lblLevel.attributedText = fun_attributedstringNextLine1(StringTitle: " Level", SelectString: textlevel)
            }
            
            headerViewAdmin.lblGoodFor.text = modelWorkoutDetailData.workoutGoodFor
            if let text = headerViewAdmin.lblGoodFor.text, !text.isEmpty{
                headerViewAdmin.lblGoodFor.attributedText = fun_attributedstringNextLine1(StringTitle: " Good for", SelectString: text)
            }
            headerViewAdmin.lblEquipments.text = modelWorkoutDetailData.workoutEquipment
            
            if let text =  headerViewAdmin.lblEquipments.text, !text.isEmpty{
                headerViewAdmin.lblEquipments.attributedText = fun_attributedstringNextLine1(StringTitle: " Equipments", SelectString: text)
            }
            
            headerViewAdmin.lblCreatedBy.text = modelWorkoutDetailData.creatorName
            if let text = headerViewAdmin.lblCreatedBy.text, !text.isEmpty{
                
                headerViewAdmin.lblCreatedBy.attributedText = fun_attributedstringNextLine1(StringTitle: " Created by", SelectString: text)
            }
            
            headerViewAdmin.lblAlowedUser.text = modelWorkoutDetailData.workoutAllowedUsername
            
            if modelWorkoutDetailData.workoutAllowedUsername == ""
            {
                headerViewAdmin.btnAllowedUser.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "Allowed users ", String2: ""), for: .normal)
            }
            else
            {
                headerViewAdmin.btnAllowedUser.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "", String2: ""), for: .normal)
            }
            
            
            if let text = headerViewAdmin.lblAlowedUser.text, !text.isEmpty{
                
                headerViewAdmin.lblAlowedUser.attributedText = fun_attributedstringNextLine1(StringTitle: " Allowed users", SelectString: text)
            }
            
            headerViewAdmin.lblAccesslevel.text = modelWorkoutDetailData.workoutSelectAccessLevel
            self.createWrokoutAdminModel.lbl_AccessLevel = modelWorkoutDetailData.workoutSelectAccessLevel
            if let text = headerViewAdmin.lblAccesslevel.text, !text.isEmpty{
                
                headerViewAdmin.lblAccesslevel.attributedText = fun_attributedstringNextLine1(StringTitle: " Access level", SelectString: text)
            }
         
            headerViewAdmin.lblDisplayNewsfeed.text = modelWorkoutDetailData.isFeatured
            self.createWrokoutAdminModel.lbl_DisplayInNewsfeed = modelWorkoutDetailData.isFeatured
            
            if let text = headerViewAdmin.lblDisplayNewsfeed.text, !text.isEmpty{
                
                headerViewAdmin.lblDisplayNewsfeed.attributedText = fun_attributedstringNextLine1(StringTitle: " Display in Newsfeed", SelectString: text)
            }
            
            
            headerViewAdmin.lblAllowNotification.text = modelWorkoutDetailData.workoutAllowedNotification
            self.createWrokoutAdminModel.lbl_allowNotification = modelWorkoutDetailData.workoutAllowedNotification
            if let text = headerViewAdmin.lblAllowNotification.text, !text.isEmpty{
                
                headerViewAdmin.lblAllowNotification.attributedText = fun_attributedstringNextLine1(StringTitle: " Allow Notification", SelectString: text)
            }
            
            headerViewAdmin.btnLevel.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "", String2: ""), for: .normal)
            headerViewAdmin.btnGoodFor.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "", String2: ""), for: .normal)
            headerViewAdmin.btnEquipments.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "", String2: ""), for: .normal)
            headerViewAdmin.btnDisplayNewsfeed.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "", String2: ""), for: .normal)
            headerViewAdmin.btnCreatedBy.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "", String2: ""), for: .normal)
            
            headerViewAdmin.btnAllowNotification.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "", String2: ""), for: .normal)
            headerViewAdmin.btnAccesslevel.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "", String2: ""), for: .normal)

        }
        workoutTblView.register(UINib(nibName: "CreateWorkoutCell", bundle: nil), forCellReuseIdentifier: "CreateWorkoutCell")
        self.workoutTblView.tableFooterView = UIView()
        self.addRightNavigationButton()
        if #available(iOS 11.0, *) {
            self.workoutTblView.dragDelegate = true as? UITableViewDragDelegate
            self.workoutTblView.dragInteractionEnabled = true
            
        } else {
            // Fallback on earlier versions
        }
        dragger = TableViewDragger(tableView: workoutTblView)
        dragger.dataSource = self
        dragger.delegate = self
        dragger.alphaForCell = 1.0
        dragger.zoomScaleForCell = 1.0
        headerViewAdmin.btnProfile.addTarget(self, action: #selector(self.didAddworkoutPhoto), for: .touchUpInside)
        headerViewAdmin.btnClickPhoto.addTarget(self, action: #selector(self.didAddworkoutPhoto), for: .touchUpInside)
        
        headerViewAdmin.setNeedsLayout()
        headerViewAdmin.layoutIfNeeded()
        
    }
    
    func getArrayDetails(){
        
        let arrValue = arrFilterData.filter {$0.groupName == "Level"}
        if arrValue.count > 0{
            self.arrLevelListing = arrValue[0].list
        }
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.init(r: 199.0, g: 199.0, b: 199.0, alpha: 1.0) {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Overview"
            textView.textColor = UIColor.init(r: 199.0, g: 199.0, b: 199.0, alpha: 1.0)
        }
    }
    
    func setCancelButtonValidation(){
        if let text = headerViewAdmin.txtfldTitle.text?.removeWhiteSpace(), text.count > 0 || self.headerViewAdmin.imgAddPhoto.image != nil || headerViewAdmin.lblLevel.text?.count != 0 || headerViewAdmin.lblGoodFor.text?.count != 0 || headerViewAdmin.lblEquipments.text?.count != 0 || CreateWorkoutAdminVC.arrExercise.count != 0
        {
            UIAlertController.showAlert(in: (APP_DELEGATE.window?.rootViewController)!, withTitle: "", message: AlertMessages.cancleCreateWorkout, cancelButtonTitle: "No", destructiveButtonTitle: "Yes", otherButtonTitles: nil, tap: { (alertCont, action, index) in
                if index == 1{
                self.navigationController?.popViewController(animated: true)
                    CreateWorkoutAdminVC.arrExercise = [Any]()
                }
            })
        }
        else
        {
       
            
            self.updateUiFromCreateWorkoutAdmin?(false)
            self.navigationController?.popViewController(animated: true)
            
            CreateWorkoutAdminVC.arrExercise = [Any]()
        }
    }
    
    func setFormValidators()-> Bool{
        if let text = headerViewAdmin.txtfldTitle.text, text.isEmpty{
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyWorkoutTitle, completion: nil)
            return false
        }
        else if (self.headerViewAdmin.imgAddPhoto.image == nil){
            self.headerViewAdmin.img_StarPhoto.image = UIImage(named: "img_star")
            
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyWorkoutPhoto, completion: nil)
            return false
        }
        else if let text = headerViewAdmin.lblLevel.text, text.isEmpty || self.createWrokoutAdminModel.lbl_SelectLevel == ""{
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyWorkoutLevel, completion: nil)
            return false
        }
        else if let text = headerViewAdmin.lblGoodFor.text, text.isEmpty{
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyWorkoutGoodFor, completion: nil)
            return false
        }
        else if let text = headerViewAdmin.lblEquipments.text, text.isEmpty{
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyWorkoutEquipments, completion: nil)
            return false
        }
 
        else if let text = headerViewAdmin.lblCreatedBy.text, text.isEmpty{
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyWorkoutCreatedby, completion: nil)
            return false
        }
            
        else if let text = headerViewAdmin.lblAccesslevel.text, text.isEmpty || self.createWrokoutAdminModel.lbl_AccessLevel == ""{
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyAccessLevel, completion: nil)
            return false
        }
        else if let text = headerViewAdmin.lblDisplayNewsfeed.text, text.isEmpty || self.createWrokoutAdminModel.lbl_DisplayInNewsfeed == ""{
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyDisplayInNewfeed, completion: nil)
            return false
        }
        else if let text = headerViewAdmin.lblAllowNotification.text, text.isEmpty || self.createWrokoutAdminModel.lbl_allowNotification == ""{
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyAllowNotification, completion: nil)
            return false
        }
        else if CreateWorkoutAdminVC.arrExercise.count == 0 {
            DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyWorkoutExercise, completion: nil)
            return false
        }
        if (CreateWorkoutAdminVC.arrExercise.count > 0)
        {
            var iCanProceed:Bool = true
            if CreateWorkoutAdminVC.arrExercise.count > 0{
                for i in 0...CreateWorkoutAdminVC.arrExercise.count-1{
                    let model = CreateWorkoutAdminVC.arrExercise[i] as? ExerciseDetails
                    
                    if model?.excerciseOption == "Time"
                    {
                        if model?.breakTimeValue == "" || model?.breakTimeValue == "00:00"
                        {
                            if model?.timeValue == "" || model?.timeValue == "00:00"{
                                ValidationRow = i
                                self.workoutTblView.reloadData()
                                DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyTime, completion: nil)
                                iCanProceed = false
                                break
                            }
                        }
                            // loks add else part
                        else
                        {
                            if model?.timeValue == "" || model?.timeValue == "00:00"{
                                ValidationRow = i
                                DispatchQueue.main.async {
                                    self.workoutTblView.reloadData()
                                }
                                
                                DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyTime, completion: nil)
                                iCanProceed = false
                                break
                            }
                        }
                    }
                    else
                    {
                        if model?.breakTimeValue == "" || model?.breakTimeValue == "00:00"
                        {
                            if model?.totalTimeValue == "" || model?.totalTimeValue == "00:00"
                            {
                                ValidationRow = i
                                DispatchQueue.main.async {
                                    self.workoutTblView.reloadData()
                                }
                                DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyTotalTime, completion: nil)
                                iCanProceed = false
                                break
                            }
                            if model?.repsValue == "" || model?.repsValue == "00"
                            {
                                ValidationRow = i
                                DispatchQueue.main.async {
                                    self.workoutTblView.reloadData()
                                }
                                DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyReps, completion: nil)
                                iCanProceed = false
                                break
                            }
                        }
                            //       loks add else part
                        else
                        {
                            if model?.totalTimeValue == "" || model?.totalTimeValue == "00:00"
                            {
                                ValidationRow = i
                                DispatchQueue.main.async {
                                    self.workoutTblView.reloadData()
                                }
                                DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyTotalTime, completion: nil)
                                iCanProceed = false
                                break
                            }
                            if model?.repsValue == "" || model?.repsValue == "00"
                            {
                                ValidationRow = i
                                DispatchQueue.main.async {
                                    self.workoutTblView.reloadData()
                                }
                                DoviesGlobalUtility.showSimpleAlert(viewController: self, message: AlertMessages.emptyReps, completion: nil)
                                iCanProceed = false
                                break
                            }
                        }
                    }
                }
            }
            return iCanProceed
        } else {
            return false
        }
    }
    
    func addRightNavigationButton(){
        let saveButton = UIButton(type: .custom)
        saveButton.setTitle("DONE", for: .normal)
        saveButton.titleLabel?.font  = UIFont.tamilBold(size: 16)
        saveButton.setTitleColor(#colorLiteral(red: 0.9882352941, green: 0.3215686275, blue: 0.1607843137, alpha: 1), for: .normal)
        // saveButton.setImage(UIImage(named:"btn_save"), for: .normal)
        saveButton.addTarget(self, action: #selector(self.didSaveButton), for: .touchUpInside)
        saveButton.frame = CGRect(x: 0, y: 0, width: 30, height: 25)
        let saveBarItem = UIBarButtonItem(customView: saveButton)
        navigationItem.rightBarButtonItems = [saveBarItem]
    }
    
    @objc func didSaveButton(sender: AnyObject){
        //            Request to call an API begins here...
        
        self.view.endEditing(true)

       if IsEditedByAdmin == true
        {
            if self.setFormValidators() == true{
            }
            else
            {
                return
            }
 
            // Note:- self.strGuestId == DoviesGlobalUtility.currentUser?.customerId-  fix by time issue

            if  self.strGuestId == DoviesGlobalUtility.currentUser?.customerId
            {
                let arrData:[alertActionData] =
                    [alertActionData(title: "Save as new workout", imageName: "ico_workout_tab_NewBlack", highlighedImageName : "ico_workout_tab_NewBlack"),
                     alertActionData(title: "Overwrite current workout", imageName: "btn_edit_s21a", highlighedImageName : "btn_edit_s21a")
                ]
                DoviesGlobalUtility.showDefaultActionSheet(on:self,actionData: arrData,cancelTitle: "CANCEL") { (clickedIndex:Int, isCancel:Bool) in
                    if clickedIndex == 0{
                        
                        self.isEditSelected = false
                        self.isCreatedByYou = true
                        if self.setFormValidators() == true{
                            self.createWorkoutWSCall()
                        }
                    }
                    else{
                        self.isEditSelected = true
                        if self.setFormValidators() == true{
                            self.createWorkoutWSCall()
                        }
                    }
                }
            }
            else
            {
                if self.setFormValidators() == true{
                   self.isNewworkout = true
                    self.createWorkoutWSCall()
                }
            }
            
            return

//           let arrData:[alertActionData] =
//                    [alertActionData(title: "Save as new workout", imageName: "ico_workout_tab_h", highlighedImageName : "ico_workout_tab_h"),
//                     alertActionData(title: "Overwrite current workout", imageName: "btn_edit_s21a", highlighedImageName : "btn_edit_s21a")
//                ]
//                DoviesGlobalUtility.showDefaultActionSheet(on:self,actionData: arrData,cancelTitle: "CANCEL") { (clickedIndex:Int, isCancel:Bool) in
//                    if clickedIndex == 0{
//
//                        self.isEditSelected = false
//                        self.isCreatedByYou = true
//                        if self.setFormValidators() == true{
//                            self.createWorkoutWSCall()
//                        }
//                    }
//                    else{
//                        self.isEditSelected = true
//                        if self.setFormValidators() == true{
//                            self.createWorkoutWSCall()
//                        }
//                    }
//                }
            
          }
        
        if self.setFormValidators() == true{
            self.createWorkoutWSCall()
        }
    }
    
    @objc func didAddworkoutPhoto(sender: AnyObject){
        self.UpdateUserButtonTapped()
        
    }
    
    @objc func UpdateUserButtonTapped ()
    {
        HBImagePicker.sharedInstance().open(with: MediaTypeImage, actionSheetTitle: "Choose Image", cancelButtonTitle: "Cancel", cameraButtonTitle: "Use Camera", galleryButtonTitle: "Choose existing", canEdit: true) {[weak self] (success, dict) in
            guard let weakself = self else { return }

            if success {
                if let img = dict?["image"] as? UIImage {
                    weakself.imageUpdated = true
                    weakself.headerViewAdmin.imgAddPhoto.image = HBImagePicker.sharedInstance().compressImage(img)
                    weakself.headerViewAdmin.btnClickPhoto.isHidden = true
                    weakself.headerViewAdmin.img_StarPhoto.image = nil
                }
            }
        }
    }
    
    @objc func didTapLevelButton(){
        self.view.endEditing(true)
        let arrList = arrLevelListing.filter {!$0.gmGoogforMasterId.isEmpty}
        let mcPicker = McPicker(data: [(arrList.map{$0.gmDisplayName})], selectedTitle: [Level])
        mcPicker.show { [weak self] (selections: [Int : String]) -> Void in
            guard let weakSelf = self else{return}
            if let level = selections[0]{
                let arrList = weakSelf.arrLevelListing.filter {$0.gmDisplayName == level}
            weakSelf.headerViewAdmin.btnLevel.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "", String2: ""), for: .normal)

                if level == "All Levels" || level == "All Level"
                {
                self?.createWrokoutAdminModel.lbl_SelectLevel = "All"
                }
                else
                {
                self?.createWrokoutAdminModel.lbl_SelectLevel = level
                }
                
                weakSelf.headerViewAdmin.lblLevel.attributedText = fun_attributedstringNextLine1(StringTitle: " Level", SelectString: level)
                
                self?.headerViewAdmin.btnLevel.setImage(nil, for: .normal)
                
                if arrList.count > 0{
                    weakSelf.Level = arrList[0].gmGoogforMasterId
                }
                
            }
        }
    }
    
    @objc func didTapAllowNotificationButton(){
        self.view.endEditing(true)
        let arrList = ["Yes","NO"]
        let mcPicker = McPicker(data: [(arrList)], selectedTitle: [""])
        mcPicker.show { [weak self] (selections: [Int : String]) -> Void in
            guard let weakSelf = self else{return}
            if let level = selections[0]{
            weakSelf.headerViewAdmin.btnAllowNotification.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "", String2: ""), for: .normal)
                
                self?.createWrokoutAdminModel.lbl_allowNotification = level
                weakSelf.headerViewAdmin.lblAllowNotification.attributedText = fun_attributedstringNextLine1(StringTitle: " Allow Notification", SelectString: level)
                
            }
        }
    }
    
    @objc func didTapAccesslevelButton(){
        self.view.endEditing(true)
        let arrList = ["Free","Subscribers"]
        let mcPicker = McPicker(data: [(arrList)], selectedTitle: [""])
        mcPicker.show { [weak self] (selections: [Int : String]) -> Void in
            guard let weakSelf = self else{return}
            if let level = selections[0]{
        weakSelf.headerViewAdmin.btnAccesslevel.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "", String2: ""), for: .normal)
                
            self?.createWrokoutAdminModel.lbl_AccessLevel = level
                weakSelf.headerViewAdmin.lblAccesslevel.attributedText = fun_attributedstringNextLine1(StringTitle: " Access Level", SelectString: level)


          //   weakSelf.headerView.lblAccesslevel.text = level
            }
        }
    }
    @objc func didTapDisplayNewsFeedButton(){
        self.view.endEditing(true)
        let arrList = ["Yes","NO"]
        let mcPicker = McPicker(data: [(arrList)], selectedTitle: [""])
        mcPicker.show { [weak self] (selections: [Int : String]) -> Void in
            guard let weakSelf = self else{return}
            if let level = selections[0]{
                weakSelf.headerViewAdmin.btnDisplayNewsfeed.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "", String2: ""), for: .normal)
                
                self?.createWrokoutAdminModel.lbl_DisplayInNewsfeed = level
                weakSelf.headerViewAdmin.lblDisplayNewsfeed.attributedText = fun_attributedstringNextLine1(StringTitle: " Display in Newsfeed", SelectString: level)

               // weakSelf.headerView.lblDisplayNewsfeed.text = level
            }
        }
    }
    
 
    
    @IBAction func ExerciseButton()
    {
        if selectedRow > -1{
            let idxPath : IndexPath = IndexPath(row: selectedRow, section: 0)
            self.closeVideoPlayForCell(indexPath:idxPath )
        }
        self.performSegue(withIdentifier: "createWorkoutToWorkoutExe", sender: nil)
    }
    
    @IBAction func WorkoutInfo()
    {
        let detailsVC = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "WorkOutExerciseDescription_Detail")as! WorkOutExerciseDescription_Detail
        detailsVC.Str_CreateWorkoutFlage = "CreateWorkout"
        detailsVC.modalTransitionStyle = .crossDissolve
        detailsVC.modalPresentationStyle = .overCurrentContext
        self.present(detailsVC, animated: true, completion: nil)
    }
    
    
    @IBAction func WorkoutTime()
        {
            if CreateWorkoutAdminVC.arrExercise.count == 0
            {
                let totalworkoutTime = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "TotalWorkouttimeVC")as! TotalWorkouttimeVC
                totalworkoutTime.TotalWorkoutMinutes = ""
                totalworkoutTime.modalTransitionStyle = .crossDissolve
                totalworkoutTime.modalPresentationStyle = .overCurrentContext
                self.present(totalworkoutTime, animated: true, completion: nil)
                return
            }
            else
            {
                
                var TotalMinutesofSecond:Int = 0
                var Totalsecond:Int = 0
                
                for i in 0...CreateWorkoutAdminVC.arrExercise.count-1{
                    if let model = CreateWorkoutAdminVC.arrExercise[i] as? ExerciseDetails {
                        
                        var Timevalue:String = "00:00"
                        var TotalTimeValue:String = "00:00"
                        
                        if model.excerciseOption == "Time"{
                            Timevalue  =  model.timeValue
                            let time = Timevalue.components(separatedBy: ":")
                            let min:Int    = Int(time[0]) ?? 0
                            let sec:Int =  Int(time[1]) ?? 0
                            TotalMinutesofSecond += (min * 60)
                            Totalsecond += sec
                        }
                        else {
                            TotalTimeValue = model.totalTimeValue
                            let totalTimeArr = TotalTimeValue.components(separatedBy: ":")
                            let min:Int    = Int(totalTimeArr[0]) ?? 0
                            let sec:Int =  Int(totalTimeArr[1]) ?? 0
                            TotalMinutesofSecond += (min * 60)
                            Totalsecond += sec
                        }
                        
                        
                        var  breakTime:String = "00:00"
                        if model.breakTimeValue == "00:00"{
                            breakTime = "00:00"
                        }else if model.breakTimeValue == ""{
                            breakTime = "00:00"
                        }else{
                            breakTime =   model.breakTimeValue
                        }
                        let  breakTimeArr = breakTime.components(separatedBy: ":")
                        let bmin:Int    = Int(breakTimeArr[0]) ?? 0
                        let bsec:Int =  Int(breakTimeArr[1]) ?? 0
                        TotalMinutesofSecond += (bmin * 60)
                        Totalsecond += bsec
                    }
                }
                
                var SendMinutes = ""
                var SendSecond = ""

                TotalMinutesofSecond = TotalMinutesofSecond + Totalsecond
                
                
                
              //  let minutes = Int(TotalMinutesofSecond) / 60 % 60
                
                
                let minutesa = Int(TotalMinutesofSecond) / 60
                
                print("\(minutesa)")
                
                SendMinutes = "\(minutesa)"
                let sec = Int((TotalMinutesofSecond % 3600) % 60)
                if sec >= 30
                {
                    SendMinutes = "\(minutesa + 1)"
                }
                if 59  >= TotalMinutesofSecond
                {
                    SendMinutes = "\(TotalMinutesofSecond)"
                    SendSecond = SendMinutes
                }
                let totalworkoutTime = Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "TotalWorkouttimeVC")as! TotalWorkouttimeVC
                totalworkoutTime.TotalWorkoutMinutes = SendMinutes
                totalworkoutTime.TotlaWorkoutSecond = SendSecond
                totalworkoutTime.modalTransitionStyle = .crossDissolve
                totalworkoutTime.modalPresentationStyle = .overCurrentContext
                self.present(totalworkoutTime, animated: true, completion: nil)
            }
      }
    
    @objc func didTapDefaultViewHideshow()
    {
        
        if headerViewAdmin.btnDefaultViewHideShow.isSelected {
            
            headerViewAdmin.btnDefaultViewHideShow.isSelected = false
            headerViewAdmin.View_AllRepsTimer.isHidden = true
            headerViewAdmin.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 795 - 101)
            headerViewAdmin.Image_updownarrow.image = UIImage(named: "icon_arrowUp")
            
        } else {
            headerViewAdmin.btnDefaultViewHideShow.isSelected = true
            headerViewAdmin.View_AllRepsTimer.isHidden = false
            headerViewAdmin.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 795 )
            headerViewAdmin.Image_updownarrow.image = UIImage(named: "icon_arrowDown")
        }
        headerViewAdmin.setNeedsLayout()
        headerViewAdmin.layoutIfNeeded()
        self.workoutTblView.reloadData()
    }
    
    @objc func didTapGoodforButton() {
        goodForListing = self.storyboard?.instantiateViewController(withIdentifier: "GoodForListingVC")as? GoodForListingVC
        goodForListing?.selected = arrSelectedGoodfor
        goodForListing?.arrIdData = arrGoodforId
        goodForListing?.completion = { (selected,arrID) in
            if selected != nil{
                self.arrSelectedGoodfor = selected!
                self.arrGoodforId = arrID!
                self.headerViewAdmin.lblGoodFor.text = selected!.joined(separator: " | ")
                if let text = self.headerViewAdmin.lblGoodFor.text, !text.isEmpty{
                    
                    self.headerViewAdmin.lblGoodFor.attributedText = fun_attributedstringNextLine1(StringTitle: " Good for", SelectString: selected!.joined(separator: " | "))
                    self.headerViewAdmin.btnGoodFor.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "", String2: ""), for: .normal)
                    
                }
                else if self.headerViewAdmin.lblGoodFor.text == ""{
                    
                    self.headerViewAdmin.btnGoodFor.setAttributedTitle(fun_attributedstring_forcolorBtn(String1: "Good For ", String2: "*"), for: .normal)
                    
                }
                else{
                    
                }
            }
        }
        self.navigationController?.pushViewController(goodForListing!, animated: true)
    }
    
    @objc func didTapEquipmentsButton() {
        
        equipmentList = self.storyboard?.instantiateViewController(withIdentifier: "EquipmentListVC")as? EquipmentListVC
        equipmentList?.selected = arrSelectedequips
        equipmentList?.arrIdData = arrEquipsId
        equipmentList?.completion = { (selected,arrEquipId) in
            if selected != nil{
                self.arrSelectedequips = selected!
                self.arrEquipsId = arrEquipId!
                self.headerViewAdmin.lblEquipments.text = selected!.joined(separator: " | ")
                if let text = self.headerViewAdmin.lblEquipments.text, !text.isEmpty{
                    self.headerViewAdmin.lblEquipments.attributedText = fun_attributedstringNextLine1(StringTitle: " Equipments", SelectString: selected!.joined(separator: " | "))
                    self.headerViewAdmin.btnEquipments.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "", String2: ""), for: .normal)
                    
                }
                else if self.headerViewAdmin.lblEquipments.text == ""{
                    self.headerViewAdmin.btnEquipments.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "Equipments ", String2: "*"), for: .normal)

                }
                else{
                }
            }
        }
        self.navigationController?.pushViewController(equipmentList! , animated: true)
        
    }
    
    
    @objc func didTapAllowUserButton() {
        
        userlist = self.storyboard?.instantiateViewController(withIdentifier: "UserListVC")as? UserListVC
        userlist?.selected = arrSelecteduser
        userlist?.arrIdData = arrUserId
        userlist?.arrUserListing = self.arrUserListing
        userlist?.Str_Title = "Allow User"
        userlist?.completion = { (selected,arrUserID) in
            if selected != nil{
                self.arrSelecteduser = selected!
                self.arrUserId = arrUserID!
                self.headerViewAdmin.lblAlowedUser.text = selected!.joined(separator: " | ")
                if let text = self.headerViewAdmin.lblAlowedUser.text, !text.isEmpty{
                    
                     self.headerViewAdmin.lblAlowedUser.attributedText = fun_attributedstringNextLine1(StringTitle: " Allowed users", SelectString: selected!.joined(separator: " | "))
                    self.headerViewAdmin.btnAllowedUser.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "", String2: ""), for: .normal)
                    
                }
                else if self.headerViewAdmin.lblAlowedUser.text == ""{
                    self.headerViewAdmin.btnAllowedUser.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "Allowed users ", String2: ""), for: .normal)
                }
                else{
                }
            }
        }
        self.navigationController?.pushViewController(userlist! , animated: true)
    }
    
    
    @objc func didTapCreatedByButton() {
        
        userlist = self.storyboard?.instantiateViewController(withIdentifier: "UserListVC")as? UserListVC
        userlist?.SelectGuestId = self.strGuestId
        userlist?.arrCreatedByListing = self.arrCreatedByListing
        userlist?.Str_Title = "Created By"
        userlist?.completionCreatedBy = { (selected,GuestId) in
            if selected != nil{
                
                self.strGuestName = selected!
                self.strGuestId = GuestId!
                self.headerViewAdmin.lblCreatedBy.text = selected
                if let text = self.headerViewAdmin.lblCreatedBy.text, !text.isEmpty{
                    
                    self.headerViewAdmin.lblCreatedBy.attributedText = fun_attributedstringNextLine1(StringTitle: " Created by", SelectString: selected ?? "")
                    self.headerViewAdmin.btnCreatedBy.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "", String2: ""), for: .normal)
                }
                else if self.headerViewAdmin.lblCreatedBy.text == ""{
                    self.headerViewAdmin.btnCreatedBy.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "Created By ", String2: "*"), for: .normal)
                }
                else{
                }
            }
        }
        self.navigationController?.pushViewController(userlist! , animated: true)
    }
    
    
    @objc func didTapWorkoutGroupButton() {
        
        userlist = self.storyboard?.instantiateViewController(withIdentifier: "UserListVC")as? UserListVC
        userlist?.SelectWorkoutCollectionId = self.strWorkoutId
        userlist?.arrWorkoutGroupListing = self.arrWorkoutListing
        userlist?.Str_Title = "Workout Group"
        userlist?.completionWorkoutGroupBy = { (selected,WorkoutId) in
            if selected != nil{
                
                self.strSelectedWorkout = selected!
                self.strWorkoutId = WorkoutId!
                self.headerViewAdmin.lblWorkoutGroup.text = selected
                if let text = self.headerViewAdmin.lblWorkoutGroup.text, !text.isEmpty{
                    self.headerViewAdmin.btnWorkoutGroup.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "", String2: ""), for: .normal)
                }
                else if self.headerViewAdmin.lblWorkoutGroup.text == ""{
                    self.headerViewAdmin.btnWorkoutGroup.setAttributedTitle(fun_attributedstring_forcolorBtnadmin(String1: "Workout Group? ", String2: "*"), for: .normal)
                }
                else{
                }
            }
        }
        self.navigationController?.pushViewController(userlist! , animated: true)
        
    }
    
    // loks add code no of reps
    @objc func didTapNoofRepsSelection_Button(sender : UIButton!) {
        
        if 400 ... 499 ~= sender.tag {
            if IsVideoOpen == "OPEN" {
                let idxPath : IndexPath = IndexPath(row: sender.tag - 400 , section: 0)
                ValidationRow = -1
                let objCell: CreateWorkoutCell = self.workoutTblView.cellForRow(at: idxPath) as! CreateWorkoutCell
                objCell.layer.borderWidth = 0
                objCell.layer.borderColor = UIColor.clear.cgColor
                self.closeVideoPlayForCell(indexPath: idxPath)
                return
            }
        }
        
        self.view.endEditing(true)
        var Reps = [String]()
        
        for i in 0...50{
            if i < 10
            {
                Reps.append("0\(i)")
            }
            else
            {
                Reps.append("\(i)")
            }
        }
        
        let mcPicker = McPicker(data: [Reps], selectedTitle:[""])
        mcPicker.label?.font = UIFont.tamilBold(size: 20)
        mcPicker.toolbarBarTintColor = UIColor(r: 235, g: 235, b: 235, alpha: 1)
        
        mcPicker.show { [weak self](selections: [Int : String]) -> Void in
            guard self != nil else{return}
            if let Reps = selections[0]{
                var objCellIdx: Int = 0
                
                if sender.tag == 1002  {
                    self?.headerViewAdmin.btnNoOfReps.setTitle(Reps, for: .normal)
                    
                    self!.defaultsetAdmin.set(Reps, forKey: "numberOfRepas")
                    
                    return
                }
                if 400 ... 499 ~= sender.tag {
                    print("100 - 199")
                    objCellIdx = sender.tag - 400
                }
                let idxPath : IndexPath = IndexPath(row: objCellIdx, section: 0)
                let objCell: CreateWorkoutCell = self?.workoutTblView.cellForRow(at: idxPath) as! CreateWorkoutCell
                if 400 ... 499 ~= sender.tag {
                    objCell.btnRepsSelection.setTitle(Reps, for: .normal)
                    
                    if Reps == "00"
                    {
                        objCell.lblSelectNoofReps.attributedText =      fun_attributedstring_forcoloradmin(main_string: "Select number of Reps *", string_to_color: "*")
                    }
                    else
                    {
                        objCell.lblSelectNoofReps.text = "Select number of Reps"
                        self?.ValidationRow = -1
                        objCell.layer.borderWidth = 0
                        objCell.layer.borderColor = UIColor.clear.cgColor
                        
                    }
                    
                    if CreateWorkoutAdminVC.arrExercise[objCellIdx] is ExerciseDetails{
                        (CreateWorkoutAdminVC.arrExercise[objCellIdx] as? ExerciseDetails)?.repsValue = Reps
                    }else{
                        (CreateWorkoutAdminVC.arrExercise[objCellIdx] as? ModelWorkoutExerciseList)?.workoutRepeatText = Reps
                    }
                }
            }
        }
    }
    
    
    // loks add code
    @objc func didTapTimeSelection_Button(sender : UIButton!) {
        self.view.endEditing(true)
        if let timeComponents = sender.titleLabel?.text?.components(separatedBy: ":") , timeComponents.count > 1{
            timeMinute = timeComponents[0]
            timeSecond = timeComponents[1]
        }
        
        let mcPicker = McPicker(data: [["00", "01","02","03","04","05", "06","07","08","09","10", "11","12","13","14","15", "16","17","18","19","20", "21","22","23","24","25", "26","27","28","29","30", "31","32","33","34","35", "36","37","38","39","40", "41","42","43","44","45", "46","47","48","49","50", "51","52","53","54","55", "56","57","58","59"],["00", "01","02","03","04","05", "06","07","08","09","10", "11","12","13","14","15", "16","17","18","19","20", "21","22","23","24","25", "26","27","28","29","30", "31","32","33","34","35", "36","37","38","39","40", "41","42","43","44","45", "46","47","48","49","50", "51","52","53","54","55", "56","57","58","59"]], selectedTitle: [timeMinute,timeSecond])
        let minLabel = UILabel(frame: CGRect(x: 0, y: ( 216 / 2) - 15, width: (UIScreen.main.bounds.width / 2) - 30, height: 30))
        minLabel.text = "Min"
        minLabel.textAlignment = .right
        mcPicker.picker.addSubview(minLabel)
        let secLabel = UILabel(frame: CGRect(x: UIScreen.main.bounds.width / 2, y: ( 216 / 2) - 15, width: (UIScreen.main.bounds.width / 2) - 30, height: 30))
        secLabel.text = "Sec"
        secLabel.textAlignment = .right
        mcPicker.picker.addSubview(secLabel)
        mcPicker.show {[weak self] (selections: [Int : String]) -> Void in
            guard self != nil else{return}
            if let minute = selections[0], let second = selections[1]{
                
                self?.timeMinute = minute
                self?.timeSecond = second
                
                let strTime: String = ("\(minute):\(second)")
                if sender.tag == 1001  {
                    self?.headerViewAdmin.btn_allTimerExcercise.setTitle(strTime, for: .normal)
                    self!.defaultsetAdmin.set(strTime, forKey: "allTimerExcercises")

                }  else if sender.tag == 1003{
                    self?.headerViewAdmin.btn_TimeFinish_reps.setTitle(strTime, for: .normal)
                   self!.defaultsetAdmin.set(strTime, forKey: "timeFinishEachExcercise")
                }
            }
        }
        mcPicker.bringSubview(toFront: minLabel)
        mcPicker.bringSubview(toFront: secLabel)
    }
  
    
    @objc func didTapTimeSelectionButton(sender : UIButton!) {
        
        
        var objIndex: Int = 0
        if 1000 ... 1999 ~= sender.tag {
            print("1000 - 1999")
            objIndex = sender.tag - 1000
        }else if 2000 ... 2999 ~= sender.tag {
            print("2000 - 2999")
            objIndex = sender.tag - 2000
        }else{
            objIndex = sender.tag - 3000
        }
        
        if IsVideoOpen == "OPEN" {
            let idxPath : IndexPath = IndexPath(row: objIndex, section: 0)
            ValidationRow = -1
            let objCell: CreateWorkoutCell = self.workoutTblView.cellForRow(at: idxPath) as! CreateWorkoutCell
            objCell.layer.borderWidth = 0
            objCell.layer.borderColor = UIColor.clear.cgColor
            self.closeVideoPlayForCell(indexPath: idxPath)
            return
        }
        
        self.view.endEditing(true)
        
        self.btnSelected = sender
        if let timeComponents = sender.titleLabel?.text?.components(separatedBy: ":") , timeComponents.count > 1{
            timeMinute = timeComponents[0]
            timeSecond = timeComponents[1]
        }
        
        let mcPicker = McPicker(data: [["00", "01","02","03","04","05", "06","07","08","09","10", "11","12","13","14","15", "16","17","18","19","20", "21","22","23","24","25", "26","27","28","29","30", "31","32","33","34","35", "36","37","38","39","40", "41","42","43","44","45", "46","47","48","49","50", "51","52","53","54","55", "56","57","58","59"],["00", "01","02","03","04","05", "06","07","08","09","10", "11","12","13","14","15", "16","17","18","19","20", "21","22","23","24","25", "26","27","28","29","30", "31","32","33","34","35", "36","37","38","39","40", "41","42","43","44","45", "46","47","48","49","50", "51","52","53","54","55", "56","57","58","59"]], selectedTitle: [timeMinute,timeSecond])
        let minLabel = UILabel(frame: CGRect(x: 0, y: ( 216 / 2) - 15, width: (UIScreen.main.bounds.width / 2) - 30, height: 30))
        minLabel.text = "Min"
        minLabel.textAlignment = .right
        mcPicker.picker.addSubview(minLabel)
        let secLabel = UILabel(frame: CGRect(x: UIScreen.main.bounds.width / 2, y: ( 216 / 2) - 15, width: (UIScreen.main.bounds.width / 2) - 30, height: 30))
        secLabel.text = "Sec"
        secLabel.textAlignment = .right
        mcPicker.picker.addSubview(secLabel)
        mcPicker.show {[weak self] (selections: [Int : String]) -> Void in
            guard self != nil else{return}
            if let minute = selections[0], let second = selections[1]{
                
                self?.timeMinute = minute
                self?.timeSecond = second
                var objCellIdx: Int = 0
                
                
                if 1000 ... 1999 ~= sender.tag {
                    print("1000 - 1999")
                    objCellIdx = sender.tag - 1000
                }else if 2000 ... 2999 ~= sender.tag {
                    print("2000 - 2999")
                    objCellIdx = sender.tag - 2000
                }else{
                    objCellIdx = sender.tag - 3000
                }
                let idxPath : IndexPath = IndexPath(row: objCellIdx, section: 0)
                let objCell: CreateWorkoutCell = self?.workoutTblView.cellForRow(at: idxPath) as! CreateWorkoutCell
                let strTime: String = ("\(minute):\(second)")
                
                if 1000 ... 1999 ~= sender.tag {
                    objCell.btnTimeSelection.setTitle(strTime, for: .normal)
                    if strTime == "00:00"
                    {
                        objCell.lblTimeSelection.attributedText = fun_attributedstring_forcoloradmin(main_string: "Select time *", string_to_color: "*")
                    }
                    else
                    {
                        objCell.lblTimeSelection.text = "Select time"
                        self?.ValidationRow = -1
                        objCell.layer.borderWidth = 0
                        objCell.layer.borderColor = UIColor.clear.cgColor
                        
                    }
                    
                    if CreateWorkoutAdminVC.arrExercise[objCellIdx] is ExerciseDetails{
                        (CreateWorkoutAdminVC.arrExercise[objCellIdx] as? ExerciseDetails)?.timeValue = strTime
                    }else{
                        (CreateWorkoutAdminVC.arrExercise[objCellIdx] as? ModelWorkoutExerciseList)?.workoutExercisesTime = strTime
                    }
                } else if 2000 ... 2999 ~= sender.tag{
                    objCell.btnTotalTimeSelection.setTitle(strTime, for: .normal)
                    
                    if strTime == "00:00"
                    {
                        objCell.lblTimetoFinsihReps.attributedText =      fun_attributedstring_forcoloradmin(main_string: "Time to finish Reps *", string_to_color: "*")
                    }
                    else
                    {
                        objCell.lblTimetoFinsihReps.text = "Time to finish Reps"
                        self?.ValidationRow = -1
                        objCell.layer.borderWidth = 0
                        objCell.layer.borderColor = UIColor.clear.cgColor
                        
                    }
                    
                    if CreateWorkoutAdminVC.arrExercise[objCellIdx] is ExerciseDetails{
                        (CreateWorkoutAdminVC.arrExercise[objCellIdx] as? ExerciseDetails)?.totalTimeValue = strTime
                    }else{
                        (CreateWorkoutAdminVC.arrExercise[objCellIdx] as? ModelWorkoutExerciseList)?.workoutTotalTime = strTime
                    }
                } else {
                    objCell.btnBreakTimeSelection.setTitle(strTime, for: .normal)
                    if CreateWorkoutAdminVC.arrExercise[objCellIdx] is ExerciseDetails{
                        (CreateWorkoutAdminVC.arrExercise[objCellIdx] as? ExerciseDetails)?.breakTimeValue = strTime
                    }else{
                        (CreateWorkoutAdminVC.arrExercise[objCellIdx] as? ModelWorkoutExerciseList)?.workoutBreakTime = strTime
                    }
                }
            }
        }
        mcPicker.bringSubview(toFront: minLabel)
        mcPicker.bringSubview(toFront: secLabel)
    }
    
    
    @objc func didTapVideoPlayButton(sender : UIButton!) {
        
        let idxPath : IndexPath = IndexPath(row: sender.tag - 500, section: 0)
        ValidationRow = -1
        let objCell: CreateWorkoutCell = self.workoutTblView.cellForRow(at: idxPath) as! CreateWorkoutCell
        objCell.layer.borderWidth = 0
        objCell.layer.borderColor = UIColor.clear.cgColor
        
        self.openVideoUIForTableCell(indexPath: idxPath )
    }
    
    
    @objc func didTapDuplicateButton(sender : UIButton!) {
        
        if IsVideoOpen == "OPEN" {
            let idxPath : IndexPath = IndexPath(row: sender.tag - 50, section: 0)
            ValidationRow = -1
            let objCell: CreateWorkoutCell = self.workoutTblView.cellForRow(at: idxPath) as! CreateWorkoutCell
            objCell.layer.borderWidth = 0
            objCell.layer.borderColor = UIColor.clear.cgColor
            self.closeVideoPlayForCell(indexPath: idxPath)
            return
        }
        ValidationRow = -1
        
        
        let objNN = CreateWorkoutAdminVC.arrExercise[sender.tag - 50] as? ExerciseDetails
        let newObj = objNN?.copy() as? ExerciseDetails
        newObj?.excerciseOption = objNN?.excerciseOption
        newObj?.exerciseName = objNN?.exerciseName
        newObj?.exerciseAccessLevel = objNN?.exerciseAccessLevel
        newObj?.exerciseAmount = objNN?.exerciseAmount
        newObj?.exerciseAmountDisplay = objNN?.exerciseAmountDisplay
        newObj?.exerciseCategory = objNN?.exerciseCategory
        newObj?.exerciseDescription = objNN?.exerciseDescription
        newObj?.exerciseImage = objNN?.exerciseImage
        newObj?.exerciseIsFavourite = objNN?.exerciseIsFavourite
        newObj?.exerciseIsFeatured = objNN?.exerciseIsFeatured
        newObj?.exerciseLevel = objNN?.exerciseLevel
        newObj?.exerciseShareUrl = objNN?.exerciseShareUrl
        newObj?.exerciseVideo = objNN?.exerciseVideo
        newObj?.exerciseVideoDuration = objNN?.exerciseVideoDuration
        newObj?.exerciseBodyParts = objNN?.exerciseBodyParts
        newObj?.exerciseEquipments = objNN?.exerciseEquipments
        newObj?.exerciseId = objNN?.exerciseId
        newObj?.exerciseTags = objNN?.exerciseTags
        newObj?.isLiked = objNN?.isLiked
        newObj?.timeValue = objNN?.timeValue
        newObj?.repsValue = objNN?.repsValue
        newObj?.breakTimeValue = objNN?.breakTimeValue
        newObj?.totalTimeValue = objNN?.totalTimeValue
        
        CreateWorkoutAdminVC.arrExercise.insert(newObj as! ExerciseDetails, at: sender.tag - 50 + 1)
        
          self.workoutTblView.reloadData()
           self.workoutTblView.layoutIfNeeded()
          self.workoutTblView.setNeedsLayout()
        
    }
    
    @objc func didTapDeleteButton(sender : UIButton!) {
        
        
        if IsVideoOpen == "OPEN" {
            let idxPath : IndexPath = IndexPath(row: sender.tag , section: 0)
            ValidationRow = -1
            let objCell: CreateWorkoutCell = self.workoutTblView.cellForRow(at: idxPath) as! CreateWorkoutCell
            objCell.layer.borderWidth = 0
            objCell.layer.borderColor = UIColor.clear.cgColor
            self.closeVideoPlayForCell(indexPath: idxPath)
            return
        }
        
        
        UIAlertController.showAlert(in: self, withTitle: "", message: AlertMessages.deleteConfirmation, cancelButtonTitle: "No", destructiveButtonTitle: "Delete", otherButtonTitles: nil, tap: { (alertController, action, buttonIndex) in
            if buttonIndex == 1 {
                CreateWorkoutAdminVC.arrExercise.remove(at: sender.tag) //Remove your element from
                self.workoutTblView.deleteRows(at: [IndexPath(row: sender.tag, section: 0)], with: .automatic)
                //   self.workoutTblView.beginUpdates()
                self.ValidationRow = -1
                self.workoutTblView.reloadData()
                self.workoutTblView.layoutIfNeeded()
                
            }
        }
        )
    }
    // MARK: UITableview video play and close
    func openVideoUIForTableCell(indexPath: IndexPath)
    {
        if selectedRow == -1 {
            selectedRow = indexPath.row
            self.workoutTblView.isScrollEnabled = false
            IsVideoOpen  = "OPEN"
            let selectedCell = workoutTblView.cellForRow(at: indexPath) as! CreateWorkoutCell
            selectedCell.layer.borderWidth = 0
            selectedCell.layer.borderColor = UIColor.clear.cgColor
            ValidationRow = -1
            
            //    selectedCell.contentView.backgroundColor =         UIColor.colorWithRGB(r: 244, g: 244, b: 244)
            //  selectedCell.contentView.backgroundColor = UIColor.white
            
            selectedCell.btnVideo.setImage(UIImage(named: "ImgUparrow"), for: .normal)
            self.visibleCellsDuringAnimation = workoutTblView.visibleCells
            selectedCell.setUpPlayerInCell(urlString: (CreateWorkoutAdminVC.arrExercise [indexPath.row] as! ExerciseDetails).exerciseVideo)
            
            for cell in self.self.visibleCellsDuringAnimation {
                if let theCell = cell as? CreateWorkoutCell {
                    // theCell.viewUpperLayer.isHidden = true
                    // theCell.contentView.backgroundColor = UIColor.colorWithRGB(r: 244, g: 244, b: 244)
                    //  theCell.contentView.backgroundColor = UIColor.red
                }
            }
            
            selectedCell.constPlayerHeight.constant =  playerHeight
            self.workoutTblView.beginUpdates()
            self.workoutTblView.endUpdates()

                DispatchQueue.main.async {

                    self.workoutTblView.scrollToRow(at: indexPath, at: .none, animated: true)
                    self.workoutTblView.layoutIfNeeded()
                }
        }
        else
        {
            self.closeVideoPlayForCell(indexPath: indexPath)
        }
    }
    
    func closeVideoPlayForCell(indexPath: IndexPath){
        
        guard let selectedCell = workoutTblView.cellForRow(at: IndexPath(row: selectedRow, section: 0)) as? CreateWorkoutCell else{
            DispatchQueue.main.async {
                self.workoutTblView.reloadData()
            }
            return
        }
        selectedCell.btnVideo.setImage(UIImage(named: "arrow_dropdown_New"), for: .normal)
        selectedCell.layer.borderWidth = 0
        selectedCell.layer.borderColor = UIColor.clear.cgColor
        ValidationRow = -1
        selectedRow = -1
        self.workoutTblView.beginUpdates()
        self.workoutTblView.endUpdates()
        self.workoutTblView.layoutIfNeeded()
        
        for cell in self.self.visibleCellsDuringAnimation {
            if let theCell = cell as? CreateWorkoutCell {
                // theCell.contentView.backgroundColor = UIColor.white
            }
        }
        selectedCell.constPlayerHeight.constant = 0
        selectedCell.stopVideo()
        selectedCell.removePlayerFromView()
        self.workoutTblView.scrollToRow(at:indexPath, at: .none, animated: true)
        self.workoutTblView.isScrollEnabled = true
        IsVideoOpen  = "CLOSED"
        
    }
    
    // MARK: UITableview DataSource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CreateWorkoutAdminVC.arrExercise.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CreateWorkoutCell") as! CreateWorkoutCell
        
        if ValidationRow == indexPath.row
        {
            cell.layer.borderWidth = 1.0
            cell.layer.borderColor = UIColor.red.cgColor
        }
        else
        {
            cell.layer.borderWidth = 0
            cell.layer.borderColor = UIColor.clear.cgColor
        }
        
        cell.tag = indexPath.row
        cell.txtfldTime.tag = timeTxtfld_adminTag + cell.tag
        cell.txtfldReps.tag = repsTxtfld_adminTag + cell.tag
        cell.totalTimeView.tag = totalTimeTxtfld_adminTag + cell.tag
        cell.txtfldBreakTime.tag = breakTimeTxtfld_adminTag + cell.tag
        cell.breakView.isHidden = false
        cell.setLayoutofTextField()
        cell.delegate = self
        cell.btnTime.setImage(UIImage(named: "btn_selexc")?.withRenderingMode(.alwaysOriginal), for: .normal)
        cell.totalTimeView.isHidden = true
        cell.lblbreaktime.backgroundColor = UIColor.colorWithRGB(r: 231.0, g: 231.0, b: 231.0)

        cell.btnDelete.tag = indexPath.row
        cell.btnDuplicate.tag = indexPath.row + 50
        cell.btnTimeSelection.tag = indexPath.row + 1000
        cell.btnTotalTimeSelection.tag = indexPath.row + 2000
        cell.btnBreakTimeSelection.tag = indexPath.row + 3000
        cell.btnRepsSelection.tag = indexPath.row + 400
        cell.btnVideo.tag = indexPath.row + 500
        
        cell.btnDelete.addTarget(self, action: #selector(self.didTapDeleteButton), for: .touchUpInside)
        cell.btnVideo.addTarget(self, action: #selector(self.didTapVideoPlayButton), for: .touchUpInside)
        cell.btnDuplicate.addTarget(self, action: #selector(self.didTapDuplicateButton), for: .touchUpInside)
        
        cell.btnRepsSelection.addTarget(self, action: #selector(self.didTapNoofRepsSelection_Button(sender:)), for: .touchUpInside)
        
        cell.btnTimeSelection.addTarget(self, action: #selector(self.didTapTimeSelectionButton(sender:)), for: .touchUpInside)
        cell.btnTotalTimeSelection.addTarget(self, action: #selector(self.didTapTimeSelectionButton(sender:)), for: .touchUpInside)
        cell.btnBreakTimeSelection.addTarget(self, action: #selector(self.didTapTimeSelectionButton(sender:)), for: .touchUpInside)
        if indexPath.row == CreateWorkoutAdminVC.arrExercise.count - 1 {
            cell.breakView.isHidden = true
            cell.lblbreaktime.backgroundColor = UIColor.clear
        }
    
        
        if let detail = CreateWorkoutAdminVC.arrExercise[indexPath.row] as? ExerciseDetails{
            cell.setCellContent(detail)
        }
        return cell
    }
    
    
    // MARK: UITableview Delegate Methods
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if self.selectedRow == indexPath.row{
            if indexPath.row == CreateWorkoutAdminVC.arrExercise.count - 1{
                return cellVideoHeight - 14
            }
            // return 362
            return cellVideoHeight + 25
        }else{
            if indexPath.row == CreateWorkoutAdminVC.arrExercise.count - 1{
                return (CreateWorkoutAdminVC.arrExercise[indexPath.row] as? ExerciseDetails)?.excerciseOption == "Time" ? 113.0 : 113.0
            }
            if CreateWorkoutAdminVC.arrExercise[indexPath.row] is ExerciseDetails{
                return (CreateWorkoutAdminVC.arrExercise[indexPath.row] as? ExerciseDetails)?.excerciseOption == "Time" ? 153.0 : 153.0
            }
        }
        
        return 0
      
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        //return 175.0
        return 150.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if indexPath.row < CreateWorkoutAdminVC.arrExercise.count{
            let exeDetails = CreateWorkoutAdminVC.arrExercise[indexPath.row]as! ExerciseDetails
            if exeDetails.exerciseAccessLevel.uppercased() == "OPEN".uppercased(){
                if selectedRow == -1
                {
                    //  self.openVideoUIForTableCell(indexPath: indexPath)
                }
                else
                {
                    self.closeVideoPlayForCell(indexPath: indexPath)
                }
                self.workoutTblView.reloadData()
                
            }else{
                if let upgradeVC = Storyboard.SidePanel.storyboard().instantiateViewController(withIdentifier: "UpgradeToPremiumViewController") as? UpgradeToPremiumViewController{
                    upgradeVC.onPurchaseSuccess = {
                        //   self.updateUI()
                    }
                    self.navigationController?.pushViewController(upgradeVC, animated: true)
                }
            }
        }
    }
    
    public func textFieldDidBeginEditing(_ textField: UITextField){
        
    }
    
    static func createWorkoutArrayContainsExercise(exerciseObj: Any) -> Bool {
        var isExerciseDetailsType = true
        var exerciseId: String!
        if let exercise = exerciseObj as? ExerciseDetails {
            exerciseId = exercise.exerciseId
        } else if let exercise = exerciseObj as? ModelWorkoutExerciseList {
            exerciseId = exercise.workoutExercisesId
            isExerciseDetailsType = false
        }
        let arrContainsSelectedObject = CreateWorkoutAdminVC.arrExercise.contains(where: { (obj) -> Bool in
            if isExerciseDetailsType, let theObj = obj as? ExerciseDetails, theObj.exerciseId == exerciseId {
                return true
            } else if let theObj = obj as? ModelWorkoutExerciseList, theObj.workoutExercisesId == exerciseId {
                return true
            }
            return false
        })
        return arrContainsSelectedObject
    }
    
    static func indexOfObjectInCreateWorkoutArray(exerciseObj: Any) -> Int? {
        
        var isExerciseDetailsType = true
        var exerciseId: String!
        if let exercise = exerciseObj as? ExerciseDetails {
            exerciseId = exercise.exerciseId
        } else if let exercise = exerciseObj as? ModelWorkoutExerciseList {
            exerciseId = exercise.workoutExercisesId
            isExerciseDetailsType = false
        }
        if let indexOfObj = CreateWorkoutAdminVC.arrExercise.index(where: { (obj) -> Bool in
            if isExerciseDetailsType, let theObj = obj as? ExerciseDetails, theObj.exerciseId == exerciseId {
                return true
            } else if let theObj = obj as? ModelWorkoutExerciseList, theObj.workoutExercisesId == exerciseId {
                return true
            }
            return false
        }) {
            return indexOfObj
        }
        return nil
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "createToListAdmin"{
            let cont = segue.destination as! MyWorkoutsVC
            cont.removePreviousCont = true
        }
        else if let dest = segue.destination as? WorkoutExeVC {
            dest.isFromCreateWorkout = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
class MyClassadmin: AddworkoutHeaderAdminView {
    
    class func instanceFromNib() -> UIView {
        return UINib(nibName: "AddworkoutHeaderAdminView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
}

extension CreateWorkoutAdminVC: CreateWorkoutCellDelegate{
    
    
    func btnRepsSelectionValue(_ RepsNo: String, _ index: Int) {
        
        if CreateWorkoutAdminVC.arrExercise[index] is ExerciseDetails{
            (CreateWorkoutAdminVC.arrExercise[index] as? ExerciseDetails)?.repsValue = RepsNo
        }
        else{
            (CreateWorkoutAdminVC.arrExercise[index] as? ModelWorkoutExerciseList)?.workoutRepeatText = RepsNo
        }
    }
    
    func btnTimeSelectionValue(_ time: String,_ index: Int) {
        
        if CreateWorkoutAdminVC.arrExercise[index] is ExerciseDetails{
            (CreateWorkoutAdminVC.arrExercise[index] as? ExerciseDetails)?.timeValue = time
        }else{
            (CreateWorkoutAdminVC.arrExercise[index] as? ModelWorkoutExerciseList)?.workoutExercisesTime = time
        }
    }
    
    func btnTotalTimeSelectionValue(_ totaltime: String,_ index: Int) {
        
        if CreateWorkoutAdminVC.arrExercise[index] is ExerciseDetails{
            (CreateWorkoutAdminVC.arrExercise[index] as? ExerciseDetails)?.totalTimeValue = totaltime
        }else{
            (CreateWorkoutAdminVC.arrExercise[index] as? ModelWorkoutExerciseList)?.workoutTotalTime = totaltime
        }
    }
    
    func btnBreakTimeSelectionValue(_ breaktime: String,_ index: Int) {
        
        
        if CreateWorkoutAdminVC.arrExercise[index] is ExerciseDetails{
            (CreateWorkoutAdminVC.arrExercise[index] as? ExerciseDetails)?.breakTimeValue = breaktime
        }else{
            (CreateWorkoutAdminVC.arrExercise[index] as? ModelWorkoutExerciseList)?.workoutBreakTime = breaktime
        }
    }
    
    func txtDidEndEditing(_ time: String, _ reps: String, _ breaktime: String, _ totaltime: String, _ index: Int) {
        if index < CreateWorkoutAdminVC.arrExercise.count{
            if CreateWorkoutAdminVC.arrExercise[index] is ExerciseDetails{
                (CreateWorkoutAdminVC.arrExercise[index] as? ExerciseDetails)?.repsValue = reps
            }
            else{
                (CreateWorkoutAdminVC.arrExercise[index] as? ModelWorkoutExerciseList)?.workoutRepeatText = reps
            }
        }
    }
    
    func txtDidBeginEditing(_ textField: UITextField) {
        print("textFieldDidBeginEditing")
    }
    
    func toggleOption(_ option: String, _ index: Int ) {
        
        if IsVideoOpen == "OPEN" {
            let idxPath : IndexPath = IndexPath(row: index , section: 0)
            ValidationRow = -1
            let objCell: CreateWorkoutCell = self.workoutTblView.cellForRow(at: idxPath) as! CreateWorkoutCell
            objCell.layer.borderWidth = 0
            objCell.layer.borderColor = UIColor.clear.cgColor
            self.closeVideoPlayForCell(indexPath: idxPath)
            return
        }
        
        if CreateWorkoutAdminVC.arrExercise[index] is ExerciseDetails{
            (CreateWorkoutAdminVC.arrExercise[index] as? ExerciseDetails)?.excerciseOption = option
            if option == "Time"{
                
                // Clear Reps
                // lokendra comment below two line
                //   (CreateWorkoutVC.arrExercise[index] as? ExerciseDetails)?.repsValue = ""
                //   (CreateWorkoutVC.arrExercise[index] as? ExerciseDetails)?.totalTimeValue = "00:00"
                (CreateWorkoutAdminVC.arrExercise[index] as? ExerciseDetails)?.repsValue =  defaultsetAdmin.string(forKey: "numberOfRepas") ?? ""
                (CreateWorkoutAdminVC.arrExercise[index] as? ExerciseDetails)?.totalTimeValue = defaultsetAdmin.string(forKey: "timeFinishEachExcercise") ?? ""
                
            }else{
                
                // Clear Time
                // lokendra comment below one line
                // (CreateWorkoutVC.arrExercise[index] as? ExerciseDetails)?.timeValue = "00:00"
                (CreateWorkoutAdminVC.arrExercise[index] as? ExerciseDetails)?.timeValue =  defaultsetAdmin.string(forKey: "allTimerExcercises") ?? ""
                
                // Add line of code for uitableview * and validation manage
                (CreateWorkoutAdminVC.arrExercise[index] as? ExerciseDetails)?.repsValue =  defaultsetAdmin.string(forKey: "numberOfRepas") ?? ""
                (CreateWorkoutAdminVC.arrExercise[index] as? ExerciseDetails)?.totalTimeValue =  defaultsetAdmin.string(forKey: "timeFinishEachExcercise") ?? ""

            }
        }else{
            
            (CreateWorkoutAdminVC.arrExercise[index] as? ModelWorkoutExerciseList)?.workoutOption = option
            if option == "Time"{
                // Clear Reps
                btnTimeSelected = true
                (CreateWorkoutAdminVC.arrExercise[index] as? ModelWorkoutExerciseList)?.workoutRepeatText = ""
                (CreateWorkoutAdminVC.arrExercise[index] as? ModelWorkoutExerciseList)?.workoutTotalTime = "00:00"
            }else{
                // Clear Time
                btnTimeSelected = false
                
                (CreateWorkoutAdminVC.arrExercise[index] as? ModelWorkoutExerciseList)?.workoutExercisesTime = "00:00"
            }
        }
        //    self.workoutTblView.reloadRows(at: [IndexPath.init(row: index, section: 0)], with: .none)
        self.workoutTblView.beginUpdates()
        self.workoutTblView.endUpdates()
        
        let idxPath : IndexPath = IndexPath(row: index, section: 0)
        let objCell: CreateWorkoutCell = self.workoutTblView.cellForRow(at: idxPath) as! CreateWorkoutCell
        (CreateWorkoutAdminVC.arrExercise[index] as? ExerciseDetails)?.excerciseOption = option
        if option == "Time"{
            objCell.totalTimeView.isHidden = true
            objCell.btnTime.setImage(UIImage(named: "btn_selexc")?.withRenderingMode(.alwaysOriginal), for: .normal)
            objCell.btnReps.setImage(UIImage(named: "btn_unselexc")?.withRenderingMode(.alwaysOriginal), for: .normal)
            objCell.lbl_TimeCircle.textColor = UIColor(r: 34/255.0, g: 34/255.0, b: 34/255.0, alpha: 1.0)
            objCell.lbl_RepsCircle.text = "Reps"
            objCell.lbl_RepsCircle.textColor = UIColor.lightGray
            
            objCell.btnRepsSelection.isHidden = true
            objCell.btnTimeSelection.isHidden = false
            objCell.lblTimeSelection.isHidden = false
            objCell.btnTimeSelection.setTitle(defaultsetAdmin.string(forKey: "allTimerExcercises") ?? "", for: .normal)
            
            if defaultsetAdmin.string(forKey: "allTimerExcercises") == "00:00"
            {
                objCell.lblTimeSelection.attributedText = fun_attributedstring_forcoloradmin(main_string: "Select time *", string_to_color: "*")
            }
                
            else
            {
                objCell.lblTimeSelection.text = "Select time"
            }
            
            if option == "Time"{
                // Clear Reps
                // lokendra below line two comment
                //                (CreateWorkoutVC.arrExercise[index] as? ExerciseDetails)?.repsValue = ""
                //                (CreateWorkoutVC.arrExercise[index] as? ExerciseDetails)?.totalTimeValue = "00:00"
                
                (CreateWorkoutAdminVC.arrExercise[index] as? ExerciseDetails)?.repsValue = defaultsetAdmin.string(forKey: "numberOfRepas") ?? ""
                (CreateWorkoutAdminVC.arrExercise[index] as? ExerciseDetails)?.totalTimeValue = defaultsetAdmin.string(forKey: "timeFinishEachExcercise") ?? ""
                
                // code add for uitableview * manage when scrolling
                (CreateWorkoutAdminVC.arrExercise[index] as? ExerciseDetails)?.timeValue = defaultsetAdmin.string(forKey: "allTimerExcercises") ?? ""
            }else{
                // Clear Time
                // lokendra comment
                // (CreateWorkoutVC.arrExercise[index] as? ExerciseDetails)?.timeValue = "00:00"
                (CreateWorkoutAdminVC.arrExercise[index] as? ExerciseDetails)?.timeValue = defaultsetAdmin.string(forKey: "allTimerExcercises") ?? ""
            }
        }

        else{
            objCell.totalTimeView.isHidden = false
            //        bottomConstraint.constant = 15
            objCell.btnTime.setImage(UIImage(named: "btn_unselexc")?.withRenderingMode(.alwaysOriginal), for: .normal)
            objCell.lbl_TimeCircle.textColor = UIColor.lightGray
            objCell.btnReps.setImage(UIImage(named: "btn_selexc")?.withRenderingMode(.alwaysOriginal), for: .normal)
            objCell.lbl_RepsCircle.textColor = UIColor(r: 34/255.0, g: 34/255.0, b: 34/255.0, alpha: 1.0)
            objCell.lbl_RepsCircle.text = "Reps"
            objCell.btnRepsSelection.isHidden = false
            objCell.btnTimeSelection.isHidden = true
            objCell.lblTimeSelection.isHidden = true
            
            if defaultsetAdmin.string(forKey: "allTimerExcercises") == "00:00"
            {
                objCell.lblTimeSelection.attributedText = fun_attributedstring_forcoloradmin(main_string: "Select time *", string_to_color: "*")
            }
            else
            {
                objCell.lblTimeSelection.text = "Select time"
            }
            objCell.btnTotalTimeSelection.setTitle(defaultsetAdmin.string(forKey: "timeFinishEachExcercise") ?? "", for: .normal)
            objCell.btnRepsSelection.setTitle(defaultsetAdmin.string(forKey: "numberOfRepas") ?? "", for: .normal)
            
            objCell.txtfldReps.text = ""
            if defaultsetAdmin.string(forKey: "numberOfRepas") == "00"
            {
                objCell.lblSelectNoofReps.attributedText =      fun_attributedstring_forcoloradmin(main_string: "Select number of Reps *", string_to_color: "*")
            }
            else
            {
                objCell.lblSelectNoofReps.text =  "Select number of Reps"
            }
            
            if defaultsetAdmin.string(forKey: "timeFinishEachExcercise") == "00:00"
            {
                objCell.lblTimetoFinsihReps.attributedText =      fun_attributedstring_forcoloradmin(main_string: "Time to finish Reps *", string_to_color: "*")
            }
            else
            {
                objCell.lblTimetoFinsihReps.text =  "Time to finish Reps"
            }
            
        }
        // self.workoutTblView.beginUpdates()
        // self.workoutTblView.endUpdates()
        
    }
}


// loks add function for * set red color
func fun_attributedstring_forcoloradmin(main_string:NSString ,string_to_color:NSString ) -> NSMutableAttributedString {
    let range = (main_string as NSString).range(of: string_to_color as String)
    
    let attribute = NSMutableAttributedString.init(string: main_string as String)
    
    attribute.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red , range: range)
    return attribute
}

func fun_attributedstring_forcolorBtnadmin(String1:String ,String2:String ) -> NSMutableAttributedString {
    
    let att = NSMutableAttributedString(string: "\(String1)\(String2)");
    att.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.init(r: 199.0, g: 199.0, b: 199.0, alpha: 1.0), range: NSRange(location: 0, length: String1.count))
    att.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.red, range: NSRange(location: String1.count, length: String2.count))
    return att
    
}
// fun_attributedstringNextLine1  Doble line in a label
func fun_attributedstringNextLine1(StringTitle:String ,SelectString:String ) -> NSMutableAttributedString {
    
    let Str_MutableString = NSMutableAttributedString(string: StringTitle, attributes: [NSAttributedStringKey.font:UIFont.tamilBold(size: 13.0),NSAttributedString.Key.foregroundColor:UIColor.colorWithRGB(r: 34/255.0, g: 34/255.0, b: 34/255.0)])
    
    let SelectLevel = NSMutableAttributedString(string: "\n \(SelectString)", attributes: [NSAttributedStringKey.font:UIFont.tamilRegular(size: 12.0),NSAttributedString.Key.foregroundColor:UIColor.lightGray])
    Str_MutableString.append(SelectLevel)
    return Str_MutableString
    
}

extension CreateWorkoutAdminVC: TableViewDraggerDataSource, TableViewDraggerDelegate {
    func dragger(_ dragger: TableViewDragger, moveDraggingAt indexPath: IndexPath, newIndexPath: IndexPath) -> Bool {

        if selectedRow == -1
        {
            let item = CreateWorkoutAdminVC.arrExercise[indexPath.row]
            CreateWorkoutAdminVC.arrExercise.remove(at: indexPath.row)
            CreateWorkoutAdminVC.arrExercise.insert(item, at: newIndexPath.row)
            self.workoutTblView.moveRow(at: indexPath, to: newIndexPath)

            return true

        }
        return false
    }

    func dragger(_ dragger: TableViewDragger, didEndDraggingAt indexPath: IndexPath) {

        DispatchQueue.main.async {
            self.workoutTblView.reloadData()
        }
    }
}

// MARK: WebService methods
extension CreateWorkoutAdminVC {
    
    /**
     This method is used to call feed listing web service.
     */
    
    func callGetcustomerUserListingWS(){
        
        let dic = [String: Any]()
        
        DoviesWSCalls.callGetModelGetuserListDataAPI(dicParam: dic, onSuccess: { (sucess, arrgetuserlist,arrCreatedBy,arrWorkoutGroup, strMessage) in
            GlobalUtility.hideActivityIndi(viewContView: self.view)
            if sucess{
                if arrgetuserlist != nil{
                    self.arrUserListing = arrgetuserlist!
                }
                if arrCreatedBy != nil{
                    self.arrCreatedByListing = arrCreatedBy!
                }
                if arrWorkoutGroup != nil{
                    self.arrWorkoutListing = arrWorkoutGroup!
                }
            }
        }) { (error) in
            
        }
    }
    
    
    
    func callGetcustomerUserListingNewWS(){
        
        var dic = [String: Any]()
        
        dic[WsParam.searchQuery] = ""
        dic[WsParam.pageIndex] = "1"
        dic[WsParam.allowedUserId] = self.arrUserId.joined(separator:  ",")

        DoviesWSCalls.callGetModelGetuserListDataSearchAPI(dicParam: dic, onSuccess: {  [weak self](sucess, arrgetuserlist,arrCreatedBy,arrWorkoutGroup, strMessage ,hasNextPage) in
            
            GlobalUtility.hideActivityIndi(viewContView: self!.view)
            
            guard let weakSelf = self else{return}
            weakSelf.shouldLoadMore = hasNextPage

            if sucess{
                if arrgetuserlist != nil{
                    self?.arrUserListing = arrgetuserlist!
                }
                if arrCreatedBy != nil{
                    self?.arrCreatedByListing = arrCreatedBy!
                }
                if arrWorkoutGroup != nil{
                    self?.arrWorkoutListing = arrWorkoutGroup!
                }
            }
        }) { (error) in
            
        }
    }
    

    
    func callLevelDataListingWS(){
        guard let cUser = DoviesGlobalUtility.currentUser else {return}
        var dic = [String: Any]()
        dic[WsParam.authCustomerId] = cUser.customerAuthToken
        dic["module_type"] = ""
        
        DoviesWSCalls.callGetModelFilterDataAPI(dicParam: dic, onSuccess: { (sucess, filterArr, strMessage) in
            GlobalUtility.hideActivityIndi(viewContView: self.view)
            if sucess{
                if filterArr != nil{
                    let arrValue = filterArr!.filter {$0.groupName == "Level"}
                    if arrValue.count > 0{
                        self.arrLevelListing = arrValue[0].list
                    }
                }
            }
        }) { (error) in
            
        }
    }
    
    /**
     This method is used to call 'Create Workout' api to add the workout information on the server. After creating the workout, screen popouts.
     */
    
    func createWorkoutWSCall(){
        GlobalUtility.showActivityIndi(viewContView: self.view)
        var dic = [String: Any]()
        guard let cUser = DoviesGlobalUtility.currentUser else {return}
        dic[WsParam.authCustomerId] = cUser.customerAuthToken
        dic[WsParam.workout_name] = headerViewAdmin.txtfldTitle.text
        dic[WsParam.workout_image] = headerViewAdmin.imgAddPhoto.image
        dic[WsParam.workout_level] = self.createWrokoutAdminModel.lbl_SelectLevel
        dic[WsParam.workout_good_for] =  self.arrGoodforId.joined(separator:  ",")
        dic[WsParam.workout_equipment] = self.arrEquipsId.joined(separator: ",")
        dic[WsParam.workout_description] = headerViewAdmin.txtView.text ?? ""
        
        dic[WsParam.workout_addedBy] = "Admin"
        dic[WsParam.workout_workoutGroupId] = strWorkoutId
        dic[WsParam.workout_allowedUsers] = self.arrUserId.joined(separator:  ",")
        dic[WsParam.workout_accessLevel] = self.createWrokoutAdminModel.lbl_AccessLevel
        dic[WsParam.workout_allowNotification] = self.createWrokoutAdminModel.lbl_allowNotification
        dic[WsParam.workout_isFeatured] = self.createWrokoutAdminModel.lbl_DisplayInNewsfeed
        dic[WsParam.workout_addedById] = strGuestId

//lokendra 04-11-2019
//        if isEditSelected == true{
//            dic[WsParam.parent_workout_id] = self.parentExeID
//            dic["workout_id"] = self.parentExeID
//        }
        
   //     Note :- Isnewworkout  condition add therefore time issue fixed (When we Program plan  - workout detail - workout edit  Overwriet as workout issue fixed )
        
        if isNewworkout == false
        {
            if isEditSelected == true{
                dic[WsParam.parent_workout_id] = self.parentExeID
                dic["workout_id"] = self.parentExeID
            }
        }
        else
        {
            self.isEditSelected = false
        }
        
        var arrData : [[String : Any]] = [[String : Any]]()
        for i in 0...CreateWorkoutAdminVC.arrExercise.count-1{
            if let model = CreateWorkoutAdminVC.arrExercise[i] as? ExerciseDetails {
                var dic = [String: Any]()
                if model.excerciseOption == "Time"{
                    dic["exercise_time"] = "00:" + model.timeValue
                    dic["exercise_type"] = "Time"
                }else{
                    dic["exercise_time"] = "00:" + model.totalTimeValue
                    dic["exercise_type"] = "Repeat"
                }
                dic["exercise_id"] = model.exerciseId
                dic["exercise_repeat_text"] = model.repsValue
                if model.breakTimeValue == "00:00"{
                    dic["break_time"] = "00:00:00"
                }else if model.breakTimeValue == ""{
                    dic["break_time"] = "00:00:00"
                }else{
                    dic["break_time"] = "00:" + model.breakTimeValue
                }
                dic["sequence_number"] = "\(i + 1) "
                arrData.append(dic)
            }
        }
        let data = try! JSONSerialization.data(withJSONObject:arrData, options: [ ])
        let jsonString = String(data: data, encoding: .utf8)
        dic["exercise"] = jsonString!
        print(dic)
        DoviesWSCalls.callCreateWorkoutAPI(isForEdit: isEditSelected == true ? isCreatedByYou == true ? true : false : false  ,dicParam: dic, onSuccess: {[weak self] (isSuccess, message, response) in
        
            guard let weakSelf = self else{return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
            if isSuccess {
                
                if weakSelf.isForEditWork != nil{
                    weakSelf.isForEditWork!()
                }
                
                CreateWorkoutAdminVC.arrExercise = [Any]()
                
                weakSelf.performSegue(withIdentifier: "createToListAdmin", sender: nil)
                GlobalUtility.showToastMessage(msg: "Done")
            }
            else{
                DoviesGlobalUtility.showSimpleAlert(viewController: weakSelf, message: message, completion: nil)
            }
            
        }) { [weak self](error) in
            guard let weakSelf = self else{return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
        }
    }
}

class CreateWorkoutAdminModel: NSObject {
    var lbl_SelectLevel: String?
    var lbl_AccessLevel: String?
    var lbl_DisplayInNewsfeed: String?
    var lbl_allowNotification: String?

}
