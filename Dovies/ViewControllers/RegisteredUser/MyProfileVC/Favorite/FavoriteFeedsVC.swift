//
//  FavoriteFeedsVC.swift
//  Dovies
//
//  Created by hb on 16/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class FavoriteFeedsVC: FavoriteBaseVC {
	
	@IBOutlet weak var tblViewFeed: UITableView!
	@IBOutlet weak var IBviewExploreFeed: UIView!
	@IBOutlet weak var IBbottomCon: NSLayoutConstraint!
	
    let tableLandScapeCellHeight = 180*CGRect.widthRatio + NewsFeedViewController.tableElementsPadding
	
    override func viewDidLoad() {
        super.viewDidLoad()
		if IS_IPHONE_X{
			IBbottomCon.constant = 74
		}
		tblViewFeed.register(UINib(nibName: "NewsFeedVerticalTableCell", bundle: nil), forCellReuseIdentifier: "NewsFeedVerticalTableCell")
		tblViewFeed.contentInset = UIEdgeInsetsMake(0, 0, IS_IPHONE_X ? 74: 0, 0)
        // Do any additional setup after loading the view.
    }

	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
        
      // loks comment 09-04-19 because remove diet plan in fav list
        
     //   if FavoriteVC.currentSelectedSegment == 4{
        
		if FavoriteVC.currentSelectedSegment == 3{
			self.callGetCustomerFavouritesAPI(apiCallingType: FavouriteApiType.Newsfeed.rawValue ,completion: {[weak self] (success) in
				guard let weakSelf = self else { return }
				if success {
                    weakSelf.tblViewFeed.isHidden = false

					weakSelf.tblViewFeed.reloadData()
				}
				if weakSelf.favoriteLists?.newsFeed == nil || weakSelf.favoriteLists?.newsFeed.count == 0{
					weakSelf.IBviewExploreFeed.isHidden = false
                    weakSelf.tblViewFeed.isHidden = true
				}
				else{
                    weakSelf.tblViewFeed.isHidden = false
					weakSelf.IBviewExploreFeed.isHidden = true
				}
            })
            
		}
	}

	@IBAction func redirectToFeed(){
		self.redirectToItsTab(tabIndex: 0)
	}
	
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
	
	//MARK: Instance methods
	
	func redirectToRespectiveDetailsBasedOnSelectedFeed(selectedFeed: NewsFeedModel, selectedIndexPath: IndexPath) {
		switch selectedFeed.newsMediaType.uppercased() {
		case NewsFeedContentType.image.rawValue.uppercased(), NewsFeedContentType.text.rawValue.uppercased(), NewsFeedContentType.video.rawValue.uppercased():
			self.performSegue(withIdentifier: "gotoNewsFeedMedia", sender: selectedFeed)
			
		case NewsFeedContentType.dietPlan.rawValue.uppercased():
			let dietDetailsVC = Storyboard.DietPlan.instantiate(DietPlanDetailsVC.self)
			dietDetailsVC.planId = selectedFeed.newsModuleId
			self.navigationController?.pushViewController(dietDetailsVC, animated: true)
			
		case NewsFeedContentType.workout.rawValue.uppercased():
			let workoutDetailsVC = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "WorkoutDetailsVC") as! WorkoutDetailsVC
			workoutDetailsVC.exerciseId = selectedFeed.newsModuleId
			self.navigationController?.pushViewController(workoutDetailsVC, animated: true)
			
		case NewsFeedContentType.program.rawValue.uppercased():
			let programDetailsVC = Storyboard.Programs.instantiate(ProgramDetail_VC.self)
			programDetailsVC.programId = selectedFeed.newsModuleId
			self.navigationController?.pushViewController(programDetailsVC, animated: true)
			
		case NewsFeedContentType.Exercise.rawValue.uppercased():
			let workoutDetailsVC = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "WorkoutDetailsVC") as! WorkoutDetailsVC
			workoutDetailsVC.screenType = .workOutDetailList
			workoutDetailsVC.exerciseId = selectedFeed.newsModuleId
			self.navigationController?.pushViewController(workoutDetailsVC, animated: true)
		default:
			print("")
		}
	}
    
	// MARK: WebService methods
	
    /**
     This method used to like or dislike a news feed
     */
	func callNewsFeedLikeAPI(with newsFeed : NewsFeedModel, likeButton : UIButton){
		guard let cUser = DoviesGlobalUtility.currentUser else {return}
		var dic = [String: Any]()
		dic[WsParam.authCustomerId] = cUser.customerAuthToken
		dic[WsParam.moduleId] = newsFeed.newsId
		dic[WsParam.moduleName] = ModuleName.newsFeed
		DoviesWSCalls.callLikeDislikeAPI(dicParam: dic, onSuccess: { (success, message, responseDict) in
			likeButton.isUserInteractionEnabled = true
			
			if !success{
				likeButton.isSelected = !likeButton.isSelected
				newsFeed.newsLikeStatus = !newsFeed.newsLikeStatus
			}
		}) { (error) in
			likeButton.isUserInteractionEnabled = true
			likeButton.isSelected = !likeButton.isSelected
			newsFeed.newsLikeStatus = !newsFeed.newsLikeStatus
		}
		
	}
    
    /**
     This method used to favourite the news feed
     */
    func callNewsFeedFavouriteAPI(with newsFeed : NewsFeedModel, favButton : UIButton){
        favButton.isUserInteractionEnabled = false
        guard let cUser = DoviesGlobalUtility.currentUser else{return}
        var parameters = [String : Any]()
        parameters[WsParam.authCustomerId] =  cUser.customerAuthToken
        parameters[WsParam.moduleId] = newsFeed.newsId
        parameters[WsParam.moduleName] = ModuleName.newsFeed
        
        DoviesWSCalls.callFavouriteAPI(dicParam: parameters, onSuccess: { (success, message, response) in
            favButton.isUserInteractionEnabled = true
            
            if !success{
                favButton.isSelected = !favButton.isSelected
                newsFeed.newsFavStatus = !newsFeed.newsFavStatus
                
            }
        }) { (error) in
            favButton.isUserInteractionEnabled = true
            favButton.isSelected = !favButton.isSelected
            newsFeed.newsFavStatus = !newsFeed.newsFavStatus
        }
        
    }
	
    // MARK: Navigation

	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		if let newsFeedDetails = segue.destination as? NewsFeedMediaDetailsVC{
            newsFeedDetails.newsFeed = (sender as! NewsFeedModel)
		}
	}

}

extension FavoriteFeedsVC: UITableViewDelegate, UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
		if let arrProgram = self.favoriteLists?.newsFeed {
            
            if  arrProgram[indexPath.row].newsMediaType.uppercased() == "IMAGE"
            {
            }
            else
            {
    self.redirectToRespectiveDetailsBasedOnSelectedFeed(selectedFeed: arrProgram[indexPath.row], selectedIndexPath: indexPath)
            }
		}
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return UITableViewAutomaticDimension
	}
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
            return tableLandScapeCellHeight
    }
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.favoriteLists?.newsFeed.count ?? 0
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: "NewsFeedVerticalTableCell") as? NewsFeedVerticalTableCell else {return UITableViewCell()}
			if let arrNews = self.favoriteLists?.newsFeed {
                cell.setNewsFeedCellUI(newsFeed: arrNews[indexPath.row], indexPath: indexPath ,isFromNewsFeed : false)
				//Like button action
				cell.likeTapped = {  (newsFeedObj , likeButton, iPath) in
                    
                    let selectedCell = self.tblViewFeed.cellForRow(at: iPath) as! NewsFeedVerticalTableCell

					likeButton.isSelected = !likeButton.isSelected
					newsFeedObj.newsLikeStatus = !newsFeedObj.newsLikeStatus
					likeButton.isUserInteractionEnabled = false
                    
                    if let likesCount = Int(newsFeedObj.customerLikes){
                        var lCount = likesCount
                        if newsFeedObj.newsLikeStatus{
                            lCount += 1
                        }else{
                            lCount -= 1
                        }
                        newsFeedObj.customerLikes = "\(lCount)"
                        
                        if newsFeedObj.customerLikes == "0" || newsFeedObj.customerLikes == "1"
                        {
                            selectedCell.IBbtnNumberOfLikes.setTitle("\(lCount) Like", for: .normal)
                        }
                        else
                        {
                            selectedCell.IBbtnNumberOfLikes.setTitle("\(lCount) Likes", for: .normal)
                        }
                    }
                    
					self.callNewsFeedLikeAPI(with: newsFeedObj, likeButton: likeButton)
				}
                
                // More button
                cell.addMoreTapped = {  (newsFeedObj , moreButton,iPath) in
                    if let arrProgram = self.favoriteLists?.newsFeed {
                        
                        self.redirectToRespectiveDetailsBasedOnSelectedFeed(selectedFeed: arrProgram[iPath.row], selectedIndexPath: iPath)
                    }
                }
                
				//Comment button action
				cell.commentTapped = {  (newsFeedObj, iPath) in
					let comments = Storyboard.NewsFeed.storyboard().instantiateViewController(withIdentifier: "CommentsVC")as! CommentsVC
					comments.newsFeed = newsFeedObj
					self.navigationController?.pushViewController(comments, animated: true)
				}
				//share button action
				cell.shareTapped = {  (newsFeedObj) in

                    DoviesGlobalUtility.showShareSheet(on: self, shareText: newsFeedObj.newsShareUrl)
				}
                
                cell.favouriteTapped = { (newsFeedObj , favoriteButton,iPath) in
                    favoriteButton.isSelected = !favoriteButton.isSelected
                    newsFeedObj.newsFavStatus = !newsFeedObj.newsFavStatus
                    favoriteButton.isUserInteractionEnabled = false
                    self.callNewsFeedFavouriteAPI(with: newsFeedObj, favButton : favoriteButton)
                }
			}
		return cell
	}
}

