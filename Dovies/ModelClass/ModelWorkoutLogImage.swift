//
//  ModelWorkoutLogImage.swift
//  Dovies
//
//  Created by Mindiii on 10/3/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class ModelWorkoutLogImage: NSObject {
    var image: UIImage!
    var time: Date!
    
    init(image: UIImage, time: Date) {
        self.image = image
        self.time = time
        super.init()
    }
}

class ModelWorkoutLogListImage: NSObject {
    var logImageURL: String!
    var logDateStr: String!
    var logDate: Date!
    var CustomerWeight: String!
    init(imageURL: String, dateStr: String ,CustomerWeightStr:String) {
        self.logImageURL = imageURL
        self.logDateStr = dateStr
        self.CustomerWeight = CustomerWeightStr
        self.logDate = self.logDateStr.dateWithFormat(format: "yyyy-MM-dd HH:mm:ss")
        super.init()
    }
}
