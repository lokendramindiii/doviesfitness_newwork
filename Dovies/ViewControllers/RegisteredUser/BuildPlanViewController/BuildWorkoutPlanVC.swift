//
//  BuildWorkoutPlanVC.swift
//  Dovies
//
//  Created by HB-PC on 23/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import SJSegmentedScrollView
import NVActivityIndicatorView

class BuildWorkoutPlanVC: BaseViewController {

    var selectedSegment             : SJSegmentTab?
    let segmentController           : SJSegmentedViewController = SJSegmentedViewController()
    var weekCount                   : Int = 1
    var buildPlanModel              : BuildPlanModel?
    var isFromMyPlan                : Bool = false
    var isFromEditAdmin             : Bool = false

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    //vijay
    @IBOutlet weak var activityIndicatorView : NVActivityIndicatorView!{
        didSet{
            activityIndicatorView.padding = 20
            activityIndicatorView.color = UIColor.lightGray
            activityIndicatorView.type = .circleStrokeSpin
        }
    }
    
    //MARK: ViewLifeCycle methods
    
    override func viewDidLoad() {
                
    //    GlobalUtility.showActivityIndi(viewContView: self.view)
        super.viewDidLoad()
        inititalUISetUp()
    }

    //MARK: - Instance methods
    override func inititalUISetUp() {
        addNavigationBackButton()
        inititalSegmentSetUp()
    }
    
    func inititalSegmentSetUp(){
        guard let model = buildPlanModel else {return}
       var segControllers = [WeekPlanViewController]()
        weekCount = model.programWorkouts.weeksData.count
        segmentController.headerViewController = UIViewController()
        for i in 1...weekCount{
            let weekPlanView = (Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "WeekPlanViewController")) as! WeekPlanViewController
            weekPlanView.title = "Week \(i)"
            weekPlanView.weekIndex = i-1
            weekPlanView.reloadAfterEdit = { (arreditedWorkouts , weekIndex) in
                self.buildPlanModel?.programWorkouts.weeksData[weekIndex] = arreditedWorkouts
            }
            
            weekPlanView.buildPlanModel = buildPlanModel
            weekPlanView.isFromMyPlan =  isFromMyPlan
            weekPlanView.isFromEditAdmin =  isFromEditAdmin

            segControllers.append(weekPlanView)
        }
        segmentController.segmentControllers = segControllers
        segmentController.headerViewHeight = 0
        segmentController.segmentViewHeight = 40.0
        
        if  isFromEditAdmin {
            segmentController.segmentBackgroundColor = UIColor.black
            segmentController.selectedSegmentViewHeight = 2.0
            segmentController.segmentTitleColor = UIColor.white
            segmentController.selectedSegmentViewColor = UIColor.white
            segmentController.segmentTitleFont = UIFont.tamilBold(size: 13.0)
            segmentController.segmentShadow = SJShadow.dark()

        }
        else
        {
         segmentController.selectedSegmentViewHeight = 0.0
            segmentController.segmentTitleColor = UIColor.segmentDarkTitleColor()
            segmentController.selectedSegmentViewColor = UIColor.appBlackColor()
            segmentController.segmentTitleFont = UIFont.tamilRegular(size: 13.0)
            segmentController.segmentShadow = SJShadow.light()

        }
      
        segmentController.segmentBounces = false
        segmentController.delegate = self
        addChildViewController(segmentController)
        self.view.addSubview(segmentController.view)
        segmentController.didMove(toParentViewController: self)
    }
}

extension BuildWorkoutPlanVC : SJSegmentedViewControllerDelegate {
    
    func didMoveToPage(_ controller: UIViewController, segment: SJSegmentTab?, index: Int) {
        if selectedSegment != nil {
            selectedSegment?.titleColor(.lightGray)
        }
        if segmentController.segments.count > 0 {
            selectedSegment = segmentController.segments[index]
            if isFromEditAdmin
            {
                selectedSegment?.titleColor(.white)
            }
            else
            {
            selectedSegment?.titleColor(.black)
            }
        }
    }
}
