//
//	GetStaticContent.swift
//
//	Create by hb on 19/4/2018
//	Copyright © 2018. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ModelGetStaticContent : NSObject, NSCoding{

	var content : String!
	var image : String!
	var title : String!
    var arrStoryImage = [ModelStoryImage]()
    var arrSubscriptionTerms = [ModelSubscriptionTerms]()

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [String:Any]){
		content = dictionary["content"] as? String ?? ""
		image = dictionary["image"] as? String ?? ""
		title = dictionary["title"] as? String ?? ""
	}
    
    init(fromStoryImages arrImages: [[String:Any]]){
        for dictImage in arrImages{
            self.arrStoryImage.append(ModelStoryImage(fromDictionary: dictImage))
        }
        content =  ""
        image = ""
       // title = ""
        title = "Success Stories"
        // success stories show 1.3 version

    }
    
    init(fromSubscription arrTitleContent: [[String:Any]]){
        for dic in arrTitleContent{
            self.arrSubscriptionTerms.append(ModelSubscriptionTerms(fromDictionary: dic))
        }
        content =  ""
        image = ""
        title = ""
    }
    

    
	/**
	 * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if content != nil{
			dictionary["content"] = content
		}
		if image != nil{
			dictionary["image"] = image
		}
		if title != nil{
			dictionary["title"] = title
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         content = aDecoder.decodeObject(forKey: "content") as? String
         image = aDecoder.decodeObject(forKey: "image") as? String
         title = aDecoder.decodeObject(forKey: "title") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    @objc func encode(with aCoder: NSCoder)
	{
		if content != nil{
			aCoder.encode(content, forKey: "content")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		if title != nil{
			aCoder.encode(title, forKey: "title")
		}

	}

}

class ModelStoryImage : NSObject{
    var imageUrl : String!
    
    init(fromDictionary dictionary: [String:Any]){
       imageUrl = dictionary["image"] as? String ?? ""
    }
}

class ModelSubscriptionTerms : NSObject{
    var subscriptionContent : String!
    var subscriptionTitle : String!

    init(fromDictionary dictionary: [String:Any]){
        subscriptionContent = dictionary["content"] as? String ?? ""
        subscriptionTitle = dictionary["title"] as? String ?? ""

    }
}

