//
//  FavoriteVC.swift
//  Dovies
//
//  Created by hb on 16/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import SJSegmentedScrollView

//	Exercise,Diet,Workout,Program,Newsfeed

enum FavouriteApiType : String {
	case Exercise = "Exercise"
	case Diet = "Diet"
	case Workout = "Workout"
	case Program = "Program"
	case Newsfeed = "Newsfeed"
}

class FavoriteVC: BaseViewController, SJSegmentedViewControllerDelegate {
	
	var selectedSegment: SJSegmentTab?
	let segmentController = SJSegmentedViewController()
	static var currentSelectedSegment = 0

    //MARK: ViewLifeCycle methods
    
	override func viewDidLoad() {
		super.viewDidLoad()
	
		self.title = "Favorite".uppercased()
		self.addNavigationWhiteBackButton()
		self.inititalSegmentSetUp()
        
	}
	
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isTranslucent = true
    }
    
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
    
	func inititalSegmentSetUp(){
		if let storyboard = self.storyboard {
			
			let favoriteExerciseVC = (Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "WorkoutDetailsVC")) as! WorkoutDetailsVC
			favoriteExerciseVC.screenType = .favoriteWorkoutList
			favoriteExerciseVC.title = "Exercise"
			
			let favoriteWorkoutVC = (storyboard.instantiateViewController(withIdentifier: "FavoriteWorkoutVC"))
			favoriteWorkoutVC.title = "Workout"
			
			let favoritePlanVC = (storyboard.instantiateViewController(withIdentifier: "FavoritePlanVC"))
			favoritePlanVC.title = "Workout Plan"
            
// Loks Diet plan hide by dovies 08-04-2019
//            let favoriteDietPlanVC = (storyboard.instantiateViewController(withIdentifier: "FavoriteDietPlanVC"))
//            favoriteDietPlanVC.title = "Diet Plan"
			
			let favoriteFeedsVC = (storyboard.instantiateViewController(withIdentifier: "FavoriteFeedsVC"))
			favoriteFeedsVC.title = "Feeds"
			
			segmentController.headerViewController = UIViewController()
            segmentController.segmentControllers = [favoriteExerciseVC, favoriteWorkoutVC, favoritePlanVC, favoriteFeedsVC]
            segmentController.headerViewHeight = 0
            segmentController.selectedSegmentViewHeight = 2.0
            segmentController.headerViewOffsetHeight = 0
            segmentController.segmentTitleColor = UIColor.colorWithRGB(r: 153, g: 153, b: 153)
          //  segmentController.segmentBackgroundColor = UIColor.black
            segmentController.selectedSegmentViewColor = UIColor.colorWithRGB(r: 34, g: 34, b: 34)
            
            
            segmentController.segmentShadow = SJShadow.light()
            segmentController.segmentBounces = true
            segmentController.delegate = self
            segmentController.segmentTitleFont = UIFont.tamilRegular(size: 14.0)
            addChildViewController(segmentController)
            self.view.addSubview(segmentController.view)
            segmentController.didMove(toParentViewController: self)
            
		}
	}
	
	func didMoveToPage(_ controller: UIViewController, segment: SJSegmentTab?, index: Int) {
		if FavoriteVC.currentSelectedSegment != index{
			FavoriteVC.currentSelectedSegment = index
			controller.viewWillAppear(true)
		}
		FavoriteVC.currentSelectedSegment = index
		if selectedSegment != nil {
			selectedSegment?.titleColor(.lightGray)
		}
		
		if segmentController.segments.count > 0 {
            selectedSegment = segmentController.segments[index]
            selectedSegment?.titleColor(.black)
        }
	}
}

//MARK: WebService methods

class FavoriteBaseVC: BaseViewController {
	
	
	var favoriteLists: ModelFavorite?
	
    /**
     This method used to fetch user favourites
     */
	func callGetCustomerFavouritesAPI(apiCallingType: String = "",completion: ((_ success: Bool) -> ())?) {
		guard let cUser = DoviesGlobalUtility.currentUser else {return}
		var dic = [String: Any]()
		dic[WsParam.authCustomerId] = cUser.customerAuthToken
		dic[WsParam.moduleType] = apiCallingType
		
		DoviesWSCalls.callGetCustomerFavouritesAPI(dicParam: dic, onSuccess: {[weak self] (success, favoriteList, message) in
			guard let weakSelf = self else {return}
			GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
				weakSelf.favoriteLists = favoriteList
			completion?(success)
		}, onFailure: {[weak self] (error) in
			guard let weakSelf = self else {return}
			GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
		})
	}
	
	func redirectToItsTab(tabIndex: Int){
		self.redirectToTab(tabIndex: tabIndex)
	}
}
