//
//  ExcerciseLibrary.swift
//  Dovies
//
//  Created by Neel Shah on 08/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class ModelExerciseLibrary: NSObject, NSCoding{
	
	var displayName : String!
	var exerciseCategoryId : String!
	var image : String!
	
	
	/**
	* Instantiate the instance using the passed dictionary values to set the properties values
	*/  
	init(fromDictionary dictionary: [String:Any]){
		displayName = dictionary["display_name"] as? String ?? ""
		exerciseCategoryId = dictionary["exercise_category_id"] as? String ?? ""
		image = dictionary["image"] as? String ?? ""
	}
	
	/**
	* Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
	*/
	func toDictionary() -> [String:Any]
	{
		var dictionary = [String:Any]()
		if displayName != nil{
			dictionary["display_name"] = displayName
		}
		if exerciseCategoryId != nil{
			dictionary["exercise_category_id"] = exerciseCategoryId
		}
		if image != nil{
			dictionary["image"] = image
		}
		return dictionary
	}
	
	/**
	* NSCoding required initializer.
	* Fills the data from the passed decoder
	*/
	@objc required init(coder aDecoder: NSCoder)
	{
		displayName = aDecoder.decodeObject(forKey: "display_name") as? String
		exerciseCategoryId = aDecoder.decodeObject(forKey: "exercise_category_id") as? String
		image = aDecoder.decodeObject(forKey: "image") as? String
		
	}
	
	/**
	* NSCoding required method.
	* Encodes mode properties into the decoder
	*/
	@objc func encode(with aCoder: NSCoder)
	{
		if displayName != nil{
			aCoder.encode(displayName, forKey: "display_name")
		}
		if exerciseCategoryId != nil{
			aCoder.encode(exerciseCategoryId, forKey: "exercise_category_id")
		}
		if image != nil{
			aCoder.encode(image, forKey: "image")
		}
		
	}
}
