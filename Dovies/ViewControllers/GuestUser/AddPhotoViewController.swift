//
//  AddPhotoViewController.swift
//  Dovies
//
//  Created by Mindiii on 9/15/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import SideMenuController

class AddPhotoViewController: UIViewController {

    var signupParams : [String:Any]?

    @IBOutlet weak var imgUser: UIImageView!
    
    @IBOutlet weak var imgPlaceholder: UIImageView!
    
    @IBOutlet weak var btnTakePhoto: UIButton!
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        // Do any additional setup after loading the view.
    }

    
    func setupUI() {
        
        self.lblMessage.makeSpacingWithCenterAlignment(lineSpace: 9.0, font: UIFont.tamilRegular(size: 14), color: UIColor(r: 27, g: 27, b: 27, alpha: 1))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func actionSkip(_ sender: Any) {
        self.callRegistrationPostApi(with: signupParams!)
    }
    
    @IBAction func actionNext(_ sender: Any) {
        
        if let image = self.imgUser.image{
            signupParams![WsParam.profile_pic] = image
        }
        self.callRegistrationPostApi(with: signupParams!)
        
    }
    
    
    func navigateToWelcomeScreen()  {
        let vc = Storyboard.Login.storyboard().instantiateViewController(withIdentifier: "WelcomeUserViewController")as! WelcomeUserViewController
        vc.signupParams = self.signupParams
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func showHomeScreen(message: String) {
        guard let window = UIApplication.shared.windows.first else { return }
        window.rootViewController = Storyboard.Main.storyboard().instantiateInitialViewController()
        
        let centerView = window.rootViewController as! SideMenuController
        let viewCont = centerView.centerViewController
        if viewCont != nil{
            DoviesGlobalUtility.showSimpleAlert(viewController: viewCont!, message: message, completion: nil)
        }
    }
    
    @IBAction func actionTakePhoto(_ sender: Any) {
        HBImagePicker.sharedInstance().open(with: MediaTypeImage, actionSheetTitle: "Choose Image", cancelButtonTitle: "Cancel", cameraButtonTitle: "Use Camera", galleryButtonTitle: "Choose existing", canEdit: true) {[weak self] (success, dict) in
            guard let weakself = self else { return}
            
            if success {
                if let img = dict?["image"] as? UIImage {
                    weakself.imgUser.image = HBImagePicker.sharedInstance().compressImage(img)
                }
            }
        }
    }
    
}


extension AddPhotoViewController{
    /**
     This method is used to call registration api to register a user.
     - Parameter params: It is having user details to register with Dovies app.
     */
    func callRegistrationApi(with params: [String:Any]){
        print("Registration Params = \(params)")
        GlobalUtility.showActivityIndi(viewContView: self.view)
        DoviesWSCalls.callSingupWS(dicParam: params, onSuccess: {[weak self] (isSuccess, message, response) in
            guard let weakSelf = self else{return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
            if isSuccess, let userInfo = response{
                if let userID = UserDefaults.standard.value(forKey: UserDefaultsKey.logoutId) as? String,   userID != userInfo[WsParam.customerUserName] as? String{
                    GlobalUtility.removePref(key: UserDefaultsKey.logoutId)
                    DoviesGlobalUtility.deleteCasheForNewUser()
                }
                UserDefaults.standard.set(userInfo, forKey: UserDefaultsKey.userData)
                DoviesGlobalUtility.currentUser = User.init(fromDictionary: userInfo)
                DoviesGlobalUtility.setFilterData()
                weakSelf.navigateToWelcomeScreen()

            }else{
                DoviesGlobalUtility.showSimpleAlert(viewController: weakSelf, message: message, completion: nil)
            }
        }) { [weak self](error) in
            guard let weakSelf = self else{return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
        }
    }
    
    func callRegistrationPostApi(with params: [String:Any]){
        print("Registration Params = \(params)")
        GlobalUtility.showActivityIndi(viewContView: self.view)
        DoviesWSCalls.callSingupPostWS(dicParam: params, onSuccess: {[weak self] (isSuccess, message, response) in
            guard let weakSelf = self else{return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
            if isSuccess, let userInfo = response{
                if let userID = UserDefaults.standard.value(forKey: UserDefaultsKey.logoutId) as? String,   userID != userInfo[WsParam.customerUserName] as? String{
                    GlobalUtility.removePref(key: UserDefaultsKey.logoutId)
                    DoviesGlobalUtility.deleteCasheForNewUser()
                }
                UserDefaults.standard.set(userInfo, forKey: UserDefaultsKey.userData)
                DoviesGlobalUtility.currentUser = User.init(fromDictionary: userInfo)
                DoviesGlobalUtility.setFilterData()
                weakSelf.navigateToWelcomeScreen()
                
            }else{
                DoviesGlobalUtility.showSimpleAlert(viewController: weakSelf, message: message, completion: nil)
            }
        }) { [weak self](error) in
            guard let weakSelf = self else{return}
            GlobalUtility.hideActivityIndi(viewContView: weakSelf.view)
        }
    }
}
