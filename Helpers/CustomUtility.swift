//
//  CustomUtility
//
//  Copyright ©  Hidden Brains. All rights reserved.
//

import UIKit

//import FacebookCore
import FacebookLogin
import FacebookShare
import FBSDKLoginKit
import Social

class CustomUtility: NSObject,UIPickerViewDelegate,UIPickerViewDataSource {
    
    typealias SignInCompletionHandler = (_ sucess :Bool,_ userDict :NSDictionary?)-> Void
    typealias GetFBFriendsCompletionHandler = (_ sucess :Bool,_ friendsArray :NSDictionary?)-> Void
    var _completionHandler:SignInCompletionHandler?
    var _fbFriendsCompletionHandler:GetFBFriendsCompletionHandler?
    
    var actionSheetview:UIView?
    static let shared: CustomUtility = {
        let instance = CustomUtility()
        // setup code
        return instance
    }()
    
    var dataArray = [String]()
    var rowSelected: ((_ title: String,_ indexSelected:Int)->())?
    var dateSelected: (()->())?
    var completion: ((_ isDoneClicked: Bool)->())?
    
    //MARK:- NavigationBarButtons
    class  func getBarButton(image:String,selectedImage:String,action:Selector,target:AnyObject)->UIBarButtonItem{
        
        let btnBar:UIButton = UIButton(type: .custom)
        btnBar.contentHorizontalAlignment = .left
        let btnImage:UIImage = UIImage(named:image)!
        btnBar.setImage(btnImage, for: .normal)
        btnBar.setImage(UIImage(named:selectedImage), for: .highlighted)
        btnBar.addTarget(target, action: action, for: .touchUpInside)
        btnBar.frame = CGRect(x: 0, y: 0, width: btnImage.size.width, height: btnImage.size.height)
        
        return UIBarButtonItem(customView: btnBar)
        
    }
    
    
    //MARK:- Pickers
    
    func getActionSheet(parentController:AnyObject,delegate:AnyObject)->UIView{
        
        let tmpView:UIView = UIView(frame: UIScreen.main.bounds)
        tmpView.frame = CGRect(x: 0, y: tmpView.frame.size.height, width: tmpView.frame.size.width, height: tmpView.frame.size.height)
        tmpView.backgroundColor = UIColor.clear
        
        let transparentView:UIView = UIView(frame: CGRect(x: 0, y: 0, width: tmpView.frame.size.width, height: tmpView.frame.size.height))
        transparentView.backgroundColor = UIColor.black
        transparentView.alpha = 0.8
        tmpView.addSubview(transparentView)
        
        let gesture:UITapGestureRecognizer = UITapGestureRecognizer(target: delegate, action: #selector(self.cancel_clicked(_:)))
        transparentView.addGestureRecognizer(gesture)
        UIApplication.shared.keyWindow?.addSubview(tmpView)
        
        return tmpView
    }
    
    
    func removeActionSheetFromSuperView(){
        
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions(rawValue: 0), animations: {
            
            self.actionSheetview?.frame = CGRect(x: 0, y: UIScreen.main.bounds.size.height, width: (self.actionSheetview?.frame.size.width)!, height: (self.actionSheetview?.frame.size.height)!)
            
        }) { (finished:Bool) in
            self.actionSheetview?.removeFromSuperview()
        }
        
    }
    
    
    
    func createTitleLabel(title:String)->UILabel{
        var lWidth,lheight:CGFloat
        if(UIDevice.current.userInterfaceIdiom == .pad){
            lWidth = 4
            lheight = 36
            
        }else{
            if(UIApplication.shared.statusBarOrientation == .portrait || UIApplication.shared.statusBarOrientation == .portraitUpsideDown){
                lheight = 36
                lWidth = 4
            }else{
                lWidth = 2
                lheight = 28
            }
            
        }
        
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: lWidth, width: UIScreen.main.bounds.size.width, height: lheight))
        label.backgroundColor = UIColor.clear
        label.font = UIFont(name: "PT Sans", size: 15)
        label.text = title
        label.textAlignment = .center
        label.textColor = UIColor.white
        return label
        
        
    }
    
    func toolbarItemForPickerWithTarget(target:AnyObject)->[UIBarButtonItem]{
        var barItems:[UIBarButtonItem] =  [UIBarButtonItem]()
        
        let cancelTitle:String = "  Cancel"
        let doneTitle:String = "Done  "
        
        let cancelBtn:UIBarButtonItem = UIBarButtonItem(title: cancelTitle, style: .plain, target: target, action: #selector(self.cancel_clicked(_:)))
        barItems.append(cancelBtn)
        let flexSpace:UIBarButtonItem = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        barItems.append(flexSpace)
        let doneBtn:UIBarButtonItem = UIBarButtonItem(title: doneTitle, style: .done, target: target, action: #selector(self.done_clicked(_:)))
        barItems.append(doneBtn)
        
        return barItems
        
    }
    
    
    func createToolBarWithTitle(title:String,target:AnyObject)-> UIToolbar{
        
        let toolbar:UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: (UIApplication.shared.keyWindow?.bounds.size.width)!, height: 44))
        toolbar.barStyle = .default
        toolbar.setBackgroundImage(UIImage(named: "bg_top_bar"), forToolbarPosition: .any, barMetrics: .default)
        toolbar.isOpaque = false
        toolbar.isTranslucent = false
        toolbar.barTintColor = UIColor.white
        toolbar.setItems(self.toolbarItemForPickerWithTarget(target: target), animated: true)
        toolbar.addSubview(self.createTitleLabel(title: title))
        
        return toolbar
    }
    
    func getPicker(title:String,target:AnyObject,dataArray:[String],rowSelected: ((_ title:String,_ indexSelected:Int) -> Void)?,completion:((_ isDoneClicked:Bool) -> Void)?)->UIPickerView?{
        
        if(dataArray.count<1){
            return nil
        }
        self.resetHandlers()
        if let _ = rowSelected{
            self.rowSelected = rowSelected
        }
        if let _ = completion{
            self.completion = completion
        }
        self.dataArray = dataArray
        let toolBar:UIToolbar = self.createToolBarWithTitle(title: title, target: self)
        let picker:UIPickerView = UIPickerView()
        picker.frame = CGRect(x: 0, y: 44, width: (UIApplication.shared.keyWindow?.bounds.size.width)!, height: 216)
        picker.delegate = self
        picker.dataSource = self
        picker.showsSelectionIndicator = true
        
        let origin = CGFloat((UIScreen.main.bounds.size.height)-CGFloat(260.0))
        let actionSheetView:UIView = self.getActionSheet(parentController: target, delegate: self)
        self.actionSheetview = actionSheetView
        
        let objView:UIView = UIView()
        objView.frame = CGRect(x: 0, y: origin, width: actionSheetView.frame.size.width, height: actionSheetView.frame.size.height)
        objView.backgroundColor = UIColor.white
        objView.addSubview(toolBar)
        objView.addSubview(picker)
        actionSheetview?.addSubview(objView)
        self.actionSheetview?.alpha = 0
        
        UIView.animate(withDuration: 0.1, delay: 0.0, options: UIViewAnimationOptions(rawValue: 0), animations: {
            
            self.actionSheetview?.frame = CGRect(x: 0, y: 0, width: (self.actionSheetview?.frame.size.width)!, height: (self.actionSheetview?.frame.size.height)!)
            self.actionSheetview?.alpha = 1.0
        }) { (finished:Bool) in
            
        }
        
        return picker
        
    }
    
    func getDatePicker(title:String,target:AnyObject,dateSelected: (() -> Void)?,completion:((_ isDoneClicked:Bool) -> Void)?)->UIDatePicker{
        self.resetHandlers()
        
        if((self.actionSheetview) != nil){
            self.actionSheetview?.removeFromSuperview()
        }
        if let _ = dateSelected{
            self.dateSelected = dateSelected
        }
        if let _ = completion{
            self.completion = completion
        }
        
        let toolBar = self.createToolBarWithTitle(title: title, target: self)
        let picker:UIDatePicker = UIDatePicker()
        picker.datePickerMode = .date
        picker.addTarget(self, action: #selector(self.dateChanged(_:)), for: .valueChanged)
        picker.frame = CGRect(x: 0, y: 44, width: (UIApplication.shared.keyWindow?.bounds.size.width)!, height: 216)
        
        let origin = CGFloat((UIScreen.main.bounds.size.height)-CGFloat(260.0))
        let actionSheetView:UIView = self.getActionSheet(parentController: target, delegate: self)
        self.actionSheetview = actionSheetView
        
        let objView:UIView = UIView()
        objView.frame = CGRect(x: 0, y: origin, width: actionSheetView.frame.size.width, height: actionSheetView.frame.size.height)
        objView.backgroundColor = UIColor.white
        objView.addSubview(toolBar)
        objView.addSubview(picker)
        actionSheetview?.addSubview(objView)
        
        UIView.animate(withDuration: 0.1, delay: 0.0, options: UIViewAnimationOptions(rawValue: 0), animations: {
            
            self.actionSheetview?.frame = CGRect(x: 0, y: 0, width: (self.actionSheetview?.frame.size.width)!, height: (self.actionSheetview?.frame.size.height)!)
        }) { (finished:Bool) in
            
        }
        
        return picker
        
        
    }
    
    
    /*func getDatePicker(title:String,target:AnyObject,pickerMode:UIDatePickerMode)->UIDatePicker{
     
     if((self.actionSheetview) != nil){
     self.actionSheetview?.removeFromSuperview()
     }
     let toolBar = self.createToolBarWithTitle(title: title, target: target)
     let locale:Locale = Locale(identifier: "en_US")
     let picker:UIDatePicker = UIDatePicker()
     picker.locale = locale as Locale
     picker.datePickerMode = pickerMode
     picker.addTarget(target, action: #selector(self.timeChanged(_:)), for: .valueChanged)
     picker.frame = CGRect(x: 0, y: 44, width: (UIApplication.shared.keyWindow?.bounds.size.width)!, height: 216)
     
     let origin = CGFloat((UIScreen.main.bounds.size.height)-CGFloat(260.0))
     let actionSheetView:UIView = self.getActionSheet(parentController: target, delegate: target)
     self.actionSheetview = actionSheetView
     
     let objView:UIView = UIView()
     objView.frame = CGRect(x: 0, y: origin, width: actionSheetView.frame.size.width, height: actionSheetView.frame.size.height)
     objView.backgroundColor = UIColor.white
     objView.addSubview(toolBar)
     objView.addSubview(picker)
     actionSheetview?.addSubview(objView)
     
     UIView.animate(withDuration: 0.1, delay: 0.0, options: UIViewAnimationOptions(rawValue: 0), animations: {
     
     self.actionSheetview?.frame = CGRect(x: 0, y: 0, width: (self.actionSheetview?.frame.size.width)!, height: (self.actionSheetview?.frame.size.height)!)
     }) { (finished:Bool) in
     
     }
     
     return picker
     
     }*/
    
    
    @objc func dateChanged(_ sender:AnyObject){
        if(self.dateSelected != nil){
            self.dateSelected!()
        }
    }
    func timeChanged(_ sender:AnyObject){
        if(self.dateSelected != nil){
            self.dateSelected!()
        }
    }
    @objc func done_clicked(_ sender:AnyObject){
        self.removeActionSheetFromSuperView();
        if(self.completion != nil){
            self.completion!(true)
        }
    }
    @objc func cancel_clicked(_ sender:AnyObject){
        
        self.removeActionSheetFromSuperView();
        if(self.completion != nil){
            self.completion!(false)
        }
    }
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return self.dataArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        return self.dataArray[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        
        if(self.rowSelected != nil){
            self.rowSelected!(self.dataArray[row],row)
        }
        
    }
    /*func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView{
     if let titleLabel = view as? UILabel {
     titleLabel.text = "Your Text"
     return titleLabel
     } else {
     let titleLabel = UILabel()
     titleLabel.font = UIFont.boldSystemFont(ofSize: 16)//Font you want here
     titleLabel.textAlignment = NSTextAlignment.center
     titleLabel.text = "Your Text"
     return titleLabel
     }
     }*/
    
    func resetHandlers(){
        self.rowSelected = nil
        self.dateSelected = nil
        self.completion = nil
        self.dataArray = [String]()
    }
    
    //Taking User's basic info from Facebook
    class func getFacebookProfileInfo(completionHandler:@escaping SignInCompletionHandler) {
        let requestMe: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "picture.type(large), name, gender, email, first_name, last_name, cover"])
        let connection: FBSDKGraphRequestConnection = FBSDKGraphRequestConnection()
        connection.add(requestMe) { (FBSDKGraphRequestConnection, result, error) in
            if (result != nil) {
                let dicFbData: NSDictionary = result as! NSDictionary
                print("dicFbData \(dicFbData)")
                DispatchQueue.main.async(execute: {
                    completionHandler(true, dicFbData)
                })
            } else {
                DispatchQueue.main.async(execute: {
                    completionHandler(false,nil)
                })
            }
        };
        connection.start()
    }
    //MARK:- FaceBook Login
    
    /// getFacebookData function is used to do Fb signIn or signin user friends list
    ///
    /// - Parameters:
    ///   - sender: object of viewcontroller from where this function called.
    ///   - completionHandler: after getting response from request this block will execute
    ///   - isForFBFriends: it's taking true/false to perform singIn only or to get friends of user.
    ///   - completionHandlerFriends: it's taking block funtion of getting friends of user
    func getFacebookData(sender: UIViewController, completionHandler:SignInCompletionHandler?, isForFBFriends: Bool, completionHandlerFriends:GetFBFriendsCompletionHandler?)
    {
        if completionHandler != nil{
            _completionHandler = completionHandler
        }else{
            _fbFriendsCompletionHandler = completionHandlerFriends
        }
        
        if (FBSDKAccessToken.current() == nil) {
            let objFBSDKLoginManager : FBSDKLoginManager = FBSDKLoginManager()
            objFBSDKLoginManager.logIn(withReadPermissions: ["public_profile", "email"], from: sender)  { (result, error) -> Void in
                print("\(error as Optional))")
                if (error != nil) {
                    DispatchQueue.main.async(execute: {
                        if completionHandler != nil{
                            self._completionHandler!(false,nil)
                            
                        }else{
                            self._fbFriendsCompletionHandler!(false, nil)
                            
                        }
                    })
                }else if (result?.isCancelled)!{
                    DispatchQueue.main.async(execute: {
                        if completionHandler != nil{
                            self._completionHandler!(false,nil)
                            
                        }else{
                            self._fbFriendsCompletionHandler!(false, nil)
                            
                        }
                    })
                }else{
                    if isForFBFriends == false{
                        self.getFacebookProfileInfo()
                    }else{
                        self.getFaceBookFriends()
                    }
                }
            }
        }else{
            if isForFBFriends == false{
                self.getFacebookProfileInfo()
            }else{
                self.getFaceBookFriends()
            }
        }
    }
    
    //Taking User's basic info from Facebook
    func getFacebookProfileInfo()
    {
        let requestMe: FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "picture, name, gender, email, first_name, last_name"])
        let connection: FBSDKGraphRequestConnection = FBSDKGraphRequestConnection()
        connection.add(requestMe) { (FBSDKGraphRequestConnection, result, error) in
            if (result != nil){
                let dicFbData: NSDictionary = result as! NSDictionary
                print("dicFbData \(dicFbData)")
                DispatchQueue.main.async(execute: {
                    self._completionHandler!(true,dicFbData)
                })
            }else{
                DispatchQueue.main.async(execute: {
                    self._completionHandler!(false,nil)
                })
            }
        };
        connection.start()
    }
    
    //getFaceBookFriends function is used to take Fb user firnds list
    func getFaceBookFriends()
    {
        let params = ["fields": "id, name, picture"]
        
        let request = FBSDKGraphRequest(graphPath:"/me/friends?limit=5000", parameters: params)
        _ = request?.start(completionHandler: {(connection, result, error) -> Void in
            if error == nil {
                print("Friends are : \(result ?? "")")
                self._fbFriendsCompletionHandler!(true, result as? NSDictionary)
            } else {
                print("Error Getting Friends \(error as Optional))");
                self._fbFriendsCompletionHandler!(false, nil)
            }
        })
    }
    
    //MARK:- Twitter Login
    /**
     Taking access
     
     - parameter isForGetFollowers: pass true if you have requested for follow user of account
     - parameter completionHandler: after getting responce of request this block will call
     */
    func getTwitterAccount(isForGetFollowers: Bool, completionHandler:@escaping SignInCompletionHandler)
    {
        let accountStore  = ACAccountStore()
        let accountType = accountStore.accountType(withAccountTypeIdentifier: ACAccountTypeIdentifierTwitter)
        accountStore.requestAccessToAccounts(with: accountType, options: nil,
                                             completion: { (granted, error) in
                                                
                                                DispatchQueue.main.async {
                                                    self._completionHandler = completionHandler
                                                }
                                                if (granted) {
                                                    // invoke twitter API
                                                    
                                                    let accounts: [ACAccount] = accountStore.accounts(with: accountType) as! [ACAccount]
                                                    
                                                    if (accounts.count > 0){
                                                        let twitterAccountsAction  = UIAlertController(title: "Select Twitter Account", message: nil, preferredStyle: .actionSheet)
                                                        
                                                        for twAc in accounts {
                                                            twitterAccountsAction.addAction(UIAlertAction(title: twAc.username, style: .default, handler: { (action) in
                                                                if isForGetFollowers == false{
                                                                    self.getTwitterLoginUserData(twitterAccount: twAc)
                                                                }else{
                                                                    self.getTwitterFollowersData(twitterAccount: twAc)
                                                                }
                                                            }))
                                                        }
                                                        
                                                        guard let vc = UIApplication.shared.keyWindow?.getVisibleViewController() else{return}
                                                        vc.present(twitterAccountsAction, animated: true, completion: nil)
                                                    }else{
                                                        DispatchQueue.main.async(execute: {
                                                            self._completionHandler!(false,nil)
                                                        })
                                                    }
                                                }else{
                                                    DispatchQueue.main.async(execute: {
                                                        self._completionHandler!(false,nil)
                                                    })
                                                }
        })
    }
    
    //Taking User's Twitter profile info
    func getTwitterLoginUserData(twitterAccount : ACAccount)
    {
        let twitterInfoRequest = SLRequest.init(forServiceType: SLServiceTypeTwitter, requestMethod: SLRequestMethod.GET, url: (URL(string : "https://api.twitter.com/1.1/users/show.json")), parameters: NSDictionary(object: twitterAccount.username, forKey: "screen_name" as NSCopying) as! [AnyHashable: Any])
        twitterInfoRequest?.account = twitterAccount
        
        //https://api.twitter.com/1.1/followers/list.json
        
        twitterInfoRequest?.perform(handler: { (responseData, urlResponse, error)  -> Void in
            if ((error) != nil) {
                print("Error: %@", error?.localizedDescription ?? "")
                DispatchQueue.main.async(execute: {
                    self._completionHandler!(false,nil)
                })
            }else{
                if let response = responseData {
                    var dict = NSDictionary()
                    do {
                        dict = try! JSONSerialization.jsonObject(with: response, options: JSONSerialization.ReadingOptions.mutableLeaves) as! NSDictionary
                        DispatchQueue.main.async(execute: {
                            print(dict)
                            self._completionHandler!(true,dict)
                        })
                    }
                }
            }
        })
    }
    
    //Taking User's Twitter Followers user
    func getTwitterFollowersData(twitterAccount : ACAccount)
    {
        let twitterInfoRequest = SLRequest.init(forServiceType: SLServiceTypeTwitter, requestMethod: SLRequestMethod.GET, url: (URL(string : "https://api.twitter.com/1.1/followers/list.json")), parameters: NSDictionary(object: twitterAccount.username, forKey: "screen_name" as NSCopying) as! [AnyHashable: Any])
        twitterInfoRequest?.account = twitterAccount
        twitterInfoRequest?.perform(handler: { (responseData, urlResponse, error) -> Void in
            if ((error) != nil) {
                print("Error: %@", error?.localizedDescription ?? "")
                DispatchQueue.main.async(execute: {
                    self._completionHandler!(false,nil)
                })
            }else{
                if let response = responseData {
                    var dict = NSDictionary()
                    do {
                        dict = try! JSONSerialization.jsonObject(with: response, options: JSONSerialization.ReadingOptions.mutableLeaves) as! NSDictionary
                        DispatchQueue.main.async(execute: {
                            self._completionHandler!(true,dict)
                        })
                    }
                }
            }
            
        })
    }
    
    
}
