//
//  SideMenuViewController.swift
//  Dovies
//
//  Created by Neel Shah on 16/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import SideMenuController
import FBSDKLoginKit
//let sideMenuTextColor = UIColor.colorWithRGB(r: 153.0, g: 153.0, b: 153.0)
let sideMenuTextColor = UIColor.black
class SideMenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
	
    @IBOutlet var tblMenuView: UITableView!
	@IBOutlet var IBimgUser: UIImageView!
	@IBOutlet var IBlblName: UILabel!
    @IBOutlet var IBlblUserName: UILabel!

    @IBOutlet var IBBtnSubscribe: UIButton!
    @IBOutlet var IBImgSubscribe: UIImageView!

    @IBOutlet var IBBtnFB: UIButton!
	@IBOutlet var IBBtnInstagram: UIButton!
    @IBOutlet var IBBtnHiddenBrains: UIButton!
    @IBOutlet var IBBtnDovies: UIButton!

    
 //   Hide My Download
    
//    var arrMenuItems: [String] = ["My Downloads", "Rate Us", "Invite Friends","Contact Us", "App FAQ", "Settings", "Logout"]
  var arrMenuItems: [String] = ["Rate Us", "Invite Friends","Contact Us", "App FAQ", "Settings", "Logout"]
    
//   var arrMenuIcons: [UIImage] = [UIImage(named: "ico_download")!, UIImage(named: "ico_rating")!,UIImage(named: "ico_invite_frnd_menu")!,UIImage(named: "ico_contact_us_menu")!,UIImage(named: "ico_faq_menu")!,UIImage(named: "ico_setting_menu")!,UIImage(named: "ico_logout_menu")!]
    
    var arrMenuIcons: [UIImage] = [UIImage(named: "ico_rating")!,UIImage(named: "ico_invite_frnd_menu")!,UIImage(named: "ico_contact_us_menu")!,UIImage(named: "ico_faq_menu")!,UIImage(named: "ico_setting_menu")!,UIImage(named: "ico_logout_menu")!]
    
 //   var arrHighlightedIcons: [UIImage] = [UIImage(named: "download_s42_h")!, UIImage(named: "rate_the_app_icon_s_42_h")!,UIImage(named: "invite_friends_icon_s_42_h")!,UIImage(named: "contacts_icon_s_42_h")!,UIImage(named: "abouts_icon_42_h")!]
    
   var arrHighlightedIcons: [UIImage] = [ UIImage(named: "rate_the_app_icon_s_42_h")!,UIImage(named: "invite_friends_icon_s_42_h")!,UIImage(named: "contacts_icon_s_42_h")!,UIImage(named: "abouts_icon_42_h")!]
    
    //MARK: - View Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        

        self.tblMenuView.separatorStyle = .none
        //self.tblMenuView.tableFooterView = UIView(
        
        IBimgUser.addTapGesture(target: self, action: #selector(self.profileImageTapped))
		self.setUserPhotoAndName()
        setSubscriptionStatus()
        
        IBBtnSubscribe.layer.cornerRadius = self.IBBtnSubscribe.frame.size.height/2
        IBBtnSubscribe.titleLabel?.font = UIFont.tamilBold(size: 12.7*CGRect.widthRatio)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let clearView = UIView()
        tblMenuView.backgroundColor = UIColor.clear// Whatever color you like
        UITableViewCell.appearance().selectedBackgroundView = clearView
        
    }
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		IBimgUser.setRoundBorder(radius: IBimgUser.frame.size.width / 2, color: UIColor.darkGray, size: 1.0)
	}
    
    @objc func profileImageTapped(){
        if let tab = sideMenuController?.centerViewController as? TabBarViewController{
            tab.selectedIndex = 4
        }
        sideMenuController?.toggle()
    }
	
	func setUserPhotoAndName(){
		guard let userData = DoviesGlobalUtility.currentUser else {
			return
		}
		IBlblName.text = userData.customerFullName
        if let name = userData.customerUserName{
            IBlblUserName.text = "@\(name)"
        }
        
        IBimgUser.sd_setImage(with: URL(string: userData.customerProfileImage), placeholderImage: AppInfo.placeholderImage)

	}
    
    func setSubscriptionStatus(){
        
        guard let cUser = DoviesGlobalUtility.currentUser else {return}
        var dic = [String: Any]()
        dic[WsParam.authCustomerId] = cUser.customerAuthToken
        DoviesWSCalls.callSubscriptionStatusAPI(dicParam: dic, onSuccess: { (success, response, message) in
            if success, let subscriptionDict = response{
                if let subStatus = (subscriptionDict[WsParam.isSubscribed] as? String)?.toBool(){
                    DoviesGlobalUtility.isSubscribed = subStatus
                    UserDefaults.standard.set(subStatus, forKey: WsParam.isSubscribed)
                    
                }
                if let daysLeft = (subscriptionDict[WsParam.title] as? String){
                    DoviesGlobalUtility.subscriptionDaysLeft = daysLeft
                }
                self.IBBtnSubscribe.isUserInteractionEnabled = !DoviesGlobalUtility.isSubscribed
                
                let font =  DoviesGlobalUtility.isSubscribed ? UIFont.tamilBold(size: 13.0*CGRect.widthRatio) : UIFont.tamilBold(size: 13.0*CGRect.widthRatio)
                self.IBBtnSubscribe.titleLabel?.font = font
                
                self.IBImgSubscribe.image = DoviesGlobalUtility.isSubscribed ? UIImage(named: "ico_upgrade") : UIImage(named: "ico_lock_subsc")
                let color = DoviesGlobalUtility.isSubscribed ? UIColor(r: 207, g: 94, b: 64, alpha: 1.0) : UIColor(r: 91, g: 171, b: 79, alpha: 1.0)
                self.IBBtnSubscribe.backgroundColor = color
           
                
                if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
                {
                    self.IBBtnSubscribe.setTitle("PREMIUM ACCOUNT", for: .normal)
                    self.IBBtnSubscribe.isUserInteractionEnabled = false
                    self.IBImgSubscribe.image = nil
                }
                else
                {
                if DoviesGlobalUtility.subscriptionDaysLeft == "UPGRADE TO PREMIUM"
                {
                    self.IBBtnSubscribe.setTitle("Upgrade To Premium", for: .normal)
                }
                else
                {
                    self.IBBtnSubscribe.setTitle(DoviesGlobalUtility.subscriptionDaysLeft, for: .normal)
                }
            }
                
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpgradeToPrimiumStatus"), object: nil, userInfo: nil)
                
               NotificationCenter.default.removeObserver("UpgradeToPrimiumStatus")

            }
        }) { (error) in
            
        }
        
    }
    
    
    
	
	@IBAction func btnSubscribe(_ sender: Any) {
		let sidePanel = Storyboard.SidePanel.instantiate(UpgradeToPremiumViewController.self)
		if let tab = sideMenuController?.centerViewController as? TabBarViewController
		{
			let selectedTab = tab.viewControllers![tab.selectedIndex] as! UINavigationController
			sidePanel.hidesBottomBarWhenPushed = true
			selectedTab.pushViewController(sidePanel, animated: true)
			sideMenuController?.toggle()
		}
	}
    
    @IBAction func socialButtonTapped(_ sender : UIButton){
    
        switch sender {
        case IBBtnFB:
            GlobalUtility.openUrlFromApp(url: StaticUrls.doviesFacebook)
            
        case IBBtnInstagram:
            GlobalUtility.openUrlFromApp(url: StaticUrls.doviesInstagram)
            
        case IBBtnHiddenBrains:
            GlobalUtility.openUrlFromApp(url: StaticUrls.doviesTwitter)
            return            
        case IBBtnDovies:
            GlobalUtility.openUrlFromApp(url: StaticUrls.doviesFitness)
            return
//            let sidePanel = Storyboard.SidePanel.instantiate(StaticPagesViewController.self)
//            sidePanel.pageType = .aboutUs
//            if let tab = sideMenuController?.centerViewController as? TabBarViewController, let selectedTab = tab.viewControllers![tab.selectedIndex] as? UINavigationController{
//                sidePanel.hidesBottomBarWhenPushed = true
//                selectedTab.pushViewController(sidePanel, animated: true)
//                sideMenuController?.toggle()
//            }
            
        default:
            return
        }
        
    }
    //Tableview DataSorce methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrMenuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTableCell") as! SideMenuTableCell
        cell.lblMenuItem.text = arrMenuItems [indexPath.row]
        cell.lblMenuItem.font = UIFont.tamilRegular(size: 14.0*CGRect.widthRatio)
        if arrMenuItems [indexPath.row] == "Logout"
        {
            cell.lblMenuItem.textColor = #colorLiteral(red: 0.9882352941, green: 0.3215686275, blue: 0.1607843137, alpha: 1)
        }
        else
        {
            cell.lblMenuItem.textColor = sideMenuTextColor
        }
        
        cell.imgMenuIcon.image = arrMenuIcons[indexPath.row]
        cell.selectionStyle = UITableViewCellSelectionStyle.gray
        
        if DoviesGlobalUtility.isNetworkAvailable || indexPath.row == 0{
            cell.isUserInteractionEnabled = true
            cell.contentView.alpha = 1.0
        }else{
            cell.contentView.alpha = 0.5
            cell.isUserInteractionEnabled = false
        }
        
        if indexPath.row == arrMenuItems.count-1{
           cell.separatorFull.isHidden = false
        }
        else{
            cell.separatorFull.isHidden = true
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 43.67 * CGRect.widthRatio
    }
      /*
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let myCell = tblMenuView.cellForRow(at: indexPath) as! SideMenuTableCell
        myCell.lblMenuItem.textColor = UIColor.colorWithRGB(r: 34.0, g: 34.0, b: 34.0)
        myCell.imgMenuIcon.image = arrHighlightedIcons[indexPath.row]
        myCell.backgroundColor = UIColor.colorWithRGB(r: 229.0, g: 229.0, b: 229.0)
    }
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let myCell = tblMenuView.cellForRow(at: indexPath) as! SideMenuTableCell
        myCell.lblMenuItem.textColor = UIColor.darkGray
        myCell.lblMenuItem.textColor = sideMenuTextColor
        myCell.imgMenuIcon.image = arrMenuIcons[indexPath.row]
        myCell.backgroundColor = UIColor.white
    }
    
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let sidePanel = Storyboard.SidePanel.instantiate(MyDownloadViewController.self)
            if let tab = sideMenuController?.centerViewController as? TabBarViewController
            {
                let selectedTab = tab.viewControllers![tab.selectedIndex] as! UINavigationController
                sidePanel.hidesBottomBarWhenPushed = true
                selectedTab.pushViewController(sidePanel, animated: true)
                sideMenuController?.toggle()
            }
        }
        else if (indexPath.row == 1) {
            GlobalUtility.openUrlFromApp(url: StaticUrls.iTunesRedirectLink)
        }
        else if (indexPath.row == 2)
        {
            let sidePanel = Storyboard.SidePanel.instantiate(InviteFriendsViewController.self)
            if let tab = sideMenuController?.centerViewController as? TabBarViewController
            {
                sideMenuController?.toggle()
                sidePanel.hidesBottomBarWhenPushed = true
                let selectedTab = tab.viewControllers![tab.selectedIndex] as! UINavigationController
                selectedTab.pushViewController(sidePanel, animated: true)
            }
        }

        else if (indexPath.row == 3)
        {
            let sidePanel = Storyboard.SidePanel.instantiate(ContactUsVC.self)
            if let tab = sideMenuController?.centerViewController as? TabBarViewController
            {
                let selectedTab = tab.viewControllers![tab.selectedIndex] as! UINavigationController
                sidePanel.hidesBottomBarWhenPushed = true
                selectedTab.pushViewController(sidePanel, animated: true)
                sideMenuController?.toggle()
            }
        }

        else if (indexPath.row == 4) //FAQ
        {
            let sidePanel = Storyboard.SidePanel.instantiate(FaqViewController.self)
            if let tab = sideMenuController?.centerViewController as? TabBarViewController
            {
                let selectedTab = tab.viewControllers![tab.selectedIndex] as! UINavigationController
                sidePanel.hidesBottomBarWhenPushed = true
                selectedTab.pushViewController(sidePanel, animated: true)
                sideMenuController?.toggle()
            }
        }
        else if (indexPath.row == 5) //Settings
        {
            let sidePanel = Storyboard.MyPlan.instantiate(SettingViewController.self)

            if let tab = sideMenuController?.centerViewController as? TabBarViewController
            {
                sideMenuController?.toggle()
                sidePanel.hidesBottomBarWhenPushed = true
                let selectedTab = tab.viewControllers![tab.selectedIndex] as! UINavigationController
                selectedTab.pushViewController(sidePanel, animated: true)
            }
        }
        else if (indexPath.row == 6)// Logout
        {
            self.logout()
        }
        self.tblMenuView.reloadData()
    }
*/
    
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         if (indexPath.row == 0) {
            GlobalUtility.openUrlFromApp(url: StaticUrls.iTunesRedirectLink)
        }
        else if (indexPath.row == 1)
        {
            let sidePanel = Storyboard.SidePanel.instantiate(InviteFriendsViewController.self)
            if let tab = sideMenuController?.centerViewController as? TabBarViewController
            {
                sideMenuController?.toggle()
                sidePanel.hidesBottomBarWhenPushed = true
                let selectedTab = tab.viewControllers![tab.selectedIndex] as! UINavigationController
                selectedTab.pushViewController(sidePanel, animated: true)
            }
        }
            
        else if (indexPath.row == 2)
        {
            let sidePanel = Storyboard.SidePanel.instantiate(ContactUsVC.self)
            if let tab = sideMenuController?.centerViewController as? TabBarViewController
            {
                let selectedTab = tab.viewControllers![tab.selectedIndex] as! UINavigationController
                sidePanel.hidesBottomBarWhenPushed = true
                selectedTab.pushViewController(sidePanel, animated: true)
                sideMenuController?.toggle()
            }
        }
            
        else if (indexPath.row == 3) //FAQ
        {
            let sidePanel = Storyboard.SidePanel.instantiate(FaqViewController.self)
            if let tab = sideMenuController?.centerViewController as? TabBarViewController
            {
                let selectedTab = tab.viewControllers![tab.selectedIndex] as! UINavigationController
                sidePanel.hidesBottomBarWhenPushed = true
                selectedTab.pushViewController(sidePanel, animated: true)
                sideMenuController?.toggle()
            }
        }
        else if (indexPath.row == 4) //Settings
        {
            let sidePanel = Storyboard.MyPlan.instantiate(SettingViewController.self)
            
            if let tab = sideMenuController?.centerViewController as? TabBarViewController
            {
                sideMenuController?.toggle()
                sidePanel.hidesBottomBarWhenPushed = true
                let selectedTab = tab.viewControllers![tab.selectedIndex] as! UINavigationController
                selectedTab.pushViewController(sidePanel, animated: true)
            }
        }
        else if (indexPath.row == 5)// Logout
        {
            self.logout()
        }
        self.tblMenuView.reloadData()
    }
    

    
    func logout()  {
        UIAlertController.showAlert(in: (APP_DELEGATE.window?.rootViewController)!, withTitle: "", message: AlertMessages.logoutConfirmation, cancelButtonTitle: "Cancel", destructiveButtonTitle: "Logout", otherButtonTitles: nil, tap: { (alertCont, action, index) in
            if index == 1{
                if let dict = GlobalUtility.getPref(key: UserDefaultsKey.userData) as? [String : Any], let userId = dict[WsParam.customerUserName] as? String{
                    GlobalUtility.setPref(value: userId, key: UserDefaultsKey.logoutId)
                }
                let objFBSDKLoginManager : FBSDKLoginManager = FBSDKLoginManager()
                objFBSDKLoginManager.logOut()
                
                UserDefaults.standard.removeObject(forKey: WsParam.isSubscribed)
                UserDefaults.standard.removeObject(forKey: UserDefaultsKey.userData)
                APP_DELEGATE.setStoryBoardBasedOnUserLogin()
            }
        })
    }
}

