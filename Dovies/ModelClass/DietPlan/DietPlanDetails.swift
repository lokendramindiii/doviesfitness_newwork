//
//  DietPlanDetails.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

struct DietPlanDetails {
    
    var dietPlanAccessLevel : String!
    var dietAccessLevel : String!
    var dietPlanDescription : String!
    var dietPlanFullImage : String!
    var dietPlanImage : String!
    var dietPlanName : String!
    var dietPlanPackageCode : String!
    var dietPlanPdf : String!
    var dietPlanShareUrl : String!
    var dietPlanFavStatus : Bool!
	var isAdded: Bool!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        
        if DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        {
            dietPlanAccessLevel = "OPEN"
            dietAccessLevel = "FREE"
        }
        else
        {
            dietPlanAccessLevel = dictionary["diet_plan_access_level"] as? String ?? ""
            dietAccessLevel = dictionary["diet_access_level"] as? String ?? ""

        }

        dietPlanDescription = dictionary["diet_plan_description"] as? String ?? ""
        dietPlanFullImage = dictionary["diet_plan_full_image"] as? String ?? ""
        dietPlanImage = dictionary["diet_plan_image"] as? String ?? ""
        dietPlanName = dictionary["diet_plan_name"] as? String ?? ""
        dietPlanPackageCode = dictionary["diet_plan_package_code"] as? String ?? ""
        dietPlanPdf = dictionary["diet_plan_pdf"] as? String ?? ""
        dietPlanShareUrl = dictionary["diet_plan_share_url"] as? String ?? ""
        if let favStatus = dictionary["diet_plan_fav_status"] as? String , favStatus == "1"{
            dietPlanFavStatus = true
        }else if  let dietFavStat = dictionary["diet_plan_fav_status"] as? Bool{
            dietPlanFavStatus = dietFavStat
        }else{
            dietPlanFavStatus = false
        }
		
		if let favStatus = dictionary["is_added"] as? String , favStatus == "1"{
			isAdded = true
		}else if  let dietFavStat = dictionary["is_added"] as? Bool{
			isAdded = dietFavStat
		}else{
			isAdded = false
		}
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if dietAccessLevel != nil{
            dictionary["diet_access_level"] = dietAccessLevel
        }
        if dietPlanAccessLevel != nil{
            dictionary["diet_plan_access_level"] = dietPlanAccessLevel
        }
        if dietPlanDescription != nil{
            dictionary["diet_plan_description"] = dietPlanDescription
        }
        if dietPlanFavStatus != nil{
            dictionary["diet_plan_fav_status"] = dietPlanFavStatus
        }
        if dietPlanFullImage != nil{
            dictionary["diet_plan_full_image"] = dietPlanFullImage
        }
        if dietPlanImage != nil{
            dictionary["diet_plan_image"] = dietPlanImage
        }
        if dietPlanName != nil{
            dictionary["diet_plan_name"] = dietPlanName
        }
        if dietPlanPackageCode != nil{
            dictionary["diet_plan_package_code"] = dietPlanPackageCode
        }
        if dietPlanPdf != nil{
            dictionary["diet_plan_pdf"] = dietPlanPdf
        }
        if dietPlanShareUrl != nil{
            dictionary["diet_plan_share_url"] = dietPlanShareUrl
        }
        return dictionary
    }

}
