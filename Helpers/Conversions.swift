//
//  Conversions.swift
//

import Foundation

class Conversions {
    
    class func getFeetAndInchesFromCentimeters(centimerters : String)->(String,String){
        guard let doubleVal = Double(centimerters) else{return ("0","0")}
        let heightCentimeters = Measurement(value:doubleVal, unit: UnitLength.centimeters)
        let feets = heightCentimeters.converted(to: UnitLength.feet).value
        let inchesInFeet = feets.truncatingRemainder(dividingBy: 1)
        let inches = Conversions.feetToInches(feetValue: inchesInFeet).rounded()
        if feets < 3{
            return ("\(Int(3))", "\(Int(0))")
        }
        return ("\(Int(feets))", "\(Int(inches))")
    }
    
    // Convert from cm to inches
    class func centimetersToFeet(centimerters: String) -> Double {
        guard let doubleVal = Double(centimerters) else{return 0}
        let heightCentimeters = Measurement(value: doubleVal, unit: UnitLength.centimeters)
        return heightCentimeters.converted(to: UnitLength.feet).value
    }
    
    // Convert from feet to inches
    class func feetToInches(feetValue: Double) -> Double {
        
        let heightCentimeters = Measurement(value: feetValue, unit: UnitLength.feet)
        return heightCentimeters.converted(to: UnitLength.inches).value
    }
    
    // Convert from feet to Centimeters
    class func feetToCentimeters(depthInFeet: String) -> Double {
        guard let doubleVal = Double(depthInFeet) else{return 0}
        let heightCentimeters = Measurement(value: doubleVal, unit: UnitLength.feet)
        return heightCentimeters.converted(to: UnitLength.centimeters).value
    }
    
    // Convert from inches to Centimeters
    class func inchesToCentimeters(inches: String) -> Double {
        guard let doubleVal = Double(inches) else{return 0}
        let heightCentimeters = Measurement(value: doubleVal, unit: UnitLength.inches)
        return heightCentimeters.converted(to: UnitLength.centimeters).value
    }
    
    class func getKgsFromLBS(lbs : String) -> Double{
        guard let doubleVal = Double(lbs) else{return 0}
        let weight = Measurement(value: doubleVal, unit: UnitMass.pounds)
        return weight.converted(to: UnitMass.kilograms).value
    }

    class func getLbsFromKgs(kgs : String) -> Double{
        guard let doubleVal = Double(kgs) else{return 0}
        let weight = Measurement(value: doubleVal, unit: UnitMass.kilograms)
        return weight.converted(to: UnitMass.pounds).value
    }
    
}
