//
//  FeaturedWorkOutVC.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

//This view controller used to show and select workout to build user plan
class FeaturedWorkOutVC: BaseViewController {
	
	@IBOutlet weak var IBcollectionCategory     : UICollectionView!
    var arrModelFeature                         : [ModelFeaturedWorkouts] = [ModelFeaturedWorkouts]()
   //	let cellSizeValue                           : CGFloat  = (ScreenSize.width - (ProgramsViewController.cellPadding))/2
    
  //  let cellSizeValue                           : CGFloat  = (ScreenSize.width)/2

    let cellSizeValue : CGFloat  = (ScreenSize.width - 62 )/2

    
    var cellCount                               : Int = 10
    var previousSelectedIndex                   : IndexPath?
    
    var workoutSelected:((ModelWorkoutList, Bool)->())?
    var doneTap:(()->())?

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    //MARK: ViewLifeCycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        inititalUISetUp()
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if arrModelFeature.count == 0{
            DispatchQueue.main.async { [weak self] in
                guard let weakSelf = self else{return}
                weakSelf.IBcollectionCategory.reloadData()
                weakSelf.IBcollectionCategory.layoutIfNeeded()
                weakSelf.IBcollectionCategory.showLoader()
            }
            self.callFeaturedWorkoutAPI()
        }
    }
    
    //MARK: Instance methods
    
    override func inititalUISetUp() {
		IBcollectionCategory.register(UINib(nibName: "GroupDetailsCollectionCell", bundle: nil), forCellWithReuseIdentifier: "GroupDetailsCollectionCell")
    }
}

//API calls
extension FeaturedWorkOutVC {
    
    /**
     This method fetch the feature workouts list and update the UI
     */
    func callFeaturedWorkoutAPI(){
        DoviesWSCalls.callFeaturedWorkoutsAPI(dicParam: [WsParam.type : "Featured"], onSuccess: { (success, msg, arrModel) in
			self.IBcollectionCategory.hideLoader()
			if success{
				self.arrModelFeature = arrModel
				self.cellCount = 0
			}
            self.IBcollectionCategory.reloadData()
        }) { (error) in
            
        }
    }
}

extension FeaturedWorkOutVC: UICollectionViewDelegate, UICollectionViewDataSource{
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		return arrModelFeature.count > 0 ? arrModelFeature.count : cellCount
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GroupDetailsCollectionCell", for: indexPath) as? GroupDetailsCollectionCell else{return UICollectionViewCell()}
		if arrModelFeature.count > 0{
			let group = arrModelFeature[indexPath.row]
			cell.setCollectionCell(with: group)
		}
		return cell
	}
	
	func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
		let featuredWorkoutCollectionVC = (Storyboard.MyPlan.storyboard().instantiateViewController(withIdentifier: "FeaturedWorkoutCollectionVC")) as! FeaturedWorkoutCollectionVC
		featuredWorkoutCollectionVC.workoutGroupId = arrModelFeature[indexPath.row].workoutGroupId
		featuredWorkoutCollectionVC.navigationItem.title = arrModelFeature[indexPath.row].workoutGroupName.uppercased()
		featuredWorkoutCollectionVC.workoutSelected = { (workoutModel, isCellSelected) in
			self.workoutSelected?(workoutModel, isCellSelected)
		}
		featuredWorkoutCollectionVC.doneTap = { () in
			self.doneTap?()
		}
		self.workoutSelected?(ModelWorkoutList(fromDictionary: ["":""]), false)
		self.navigationController?.pushViewController(featuredWorkoutCollectionVC, animated: true)
	}
}

extension FeaturedWorkOutVC: UICollectionViewDelegateFlowLayout{
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: cellSizeValue, height: cellSizeValue)


	}
	
	func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
		return UIEdgeInsets(top: 0, left: 0, bottom: DietPlansListVC.cellPadding, right: 0)
	}
}
