//
//  MyDownloadViewController.swift
//  Dovies
//
//  Created by Neel Shah on 01/05/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import CoreData

class MyDownloadViewController: BaseViewController {
	
	@IBOutlet weak var IBtblDownloads: UITableView!
    @IBOutlet weak var IBviewEmptyDownload: UIView!
	var arrModelWorkOutData: [Workout]  = [Workout]()

    override func viewDidLoad() {
        super.viewDidLoad()

		self.navigationItem.title = "My Downloads".uppercased()
		self.addNavigationBackButton()
      
         if let workouts =  Workout.getWorkoutFromContext(params: nil, sortOn: "workoutCreatedDate", isAsending: false) as? [Workout]{

            arrModelWorkOutData = workouts

            let arr = convertToJSONArray(moArray: arrModelWorkOutData)
           // print("arr = \(arr)")

            self.modalTransitionStyle = .crossDissolve
            self.modalPresentationStyle = .overCurrentContext
            }
        
		IBtblDownloads.register(UINib(nibName: "MyWorkoutsCell", bundle: nil), forCellReuseIdentifier: "MyWorkoutsCell")
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func convertToJSONArray(moArray: [Workout]) -> [NSDictionary] {
        var jsonArray: [[String: Any]] = []
        for item in moArray {
            var dict: [String: Any] = [:]
            for attribute in item.entity.attributesByName {
                //check if value is present, then add key to dictionary so as to avoid the nil value crash
                if let value = item.value(forKey: attribute.key) {
                    dict[attribute.key] = value
                }
            }
            jsonArray.append(dict)
        }
        return jsonArray as! [NSDictionary]
    }
    
    func convertToDictionary(obj: Workout) -> NSDictionary {
        var dict: [String: Any] = [:]
        for attribute in obj.entity.attributesByName {
            //check if value is present, then add key to dictionary so as to avoid the nil value crash
            if let value = obj.value(forKey: attribute.key) {
                dict[attribute.key] = value
            }
        }
        return dict as! NSDictionary
    }
	
	func getDetailDataForDetailView(model: Workout) -> ModelWorkoutDetail{
		let modelDetail = ModelWorkoutDetail(modelWorkout: model)
		return modelDetail
	}
}

extension MyDownloadViewController: UITableViewDelegate{
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let featuredWorkOutDetailViewControlle = Storyboard.WorkOut.storyboard().instantiateViewController(withIdentifier: "FeaturedWorkOutDetailViewController") as! FeaturedWorkOutDetailViewController
		featuredWorkOutDetailViewControlle.navigationItem.title = arrModelWorkOutData[indexPath.row].workoutName
		featuredWorkOutDetailViewControlle.workoutId = arrModelWorkOutData[indexPath.row].workoutId
		featuredWorkOutDetailViewControlle.isForDownload = true
        featuredWorkOutDetailViewControlle.isFromMyDownload = true
		featuredWorkOutDetailViewControlle.modelWorkoutDetail = self.getDetailDataForDetailView(model: arrModelWorkOutData[indexPath.row])
		self.navigationController?.pushViewController(featuredWorkOutDetailViewControlle, animated: true)
	}
	
	func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
		return 0.001
	}
	
	func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
		return 0.001
	}
	
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 74
	}
}

extension MyDownloadViewController: UITableViewDataSource{
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.IBviewEmptyDownload.isHidden = arrModelWorkOutData.count > 0
		return arrModelWorkOutData.count
	}
	
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyWorkoutsCell") as! MyWorkoutsCell
		cell.setWorkoutsDownLoadData(modelWorkOuts: arrModelWorkOutData[indexPath.row])
        return cell
    }
	
	func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
		return true
	}
	
	func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
		if (editingStyle == UITableViewCellEditingStyle.delete) {
			if let wId = arrModelWorkOutData[indexPath.row].workoutId{
				UIAlertController.showAlert(in: self, withTitle: AlertTitle.appName, message: AlertMessages.deleteDownloadedWorkout, cancelButtonTitle: "No", destructiveButtonTitle: "Delete", otherButtonTitles: nil) { (alertControl, action, index) in
					if index == 1{
						Workout.deleteWorkoutWhichIsEdit(workoutId: wId)
						self.arrModelWorkOutData.remove(at: indexPath.row)
						self.IBtblDownloads.reloadData()
					}
				}
			}
		}
	}
}
