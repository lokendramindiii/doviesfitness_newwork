//
//  SubHistoryVC.swift
//  Dovies
//
//  Created by hb on 20/02/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class SubHistoryVC: BaseViewController,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet var SubTblView: UITableView!

    @IBOutlet weak var emptyDataView: UIView!
    var arrSubscriptionHistory = [ModelSubscriptionHistory]()
	var cellCount = 10
	
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Subscription History"
        self.addNavigationBackButton()
        self.SubTblView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
		if arrSubscriptionHistory.count == 0{
			DispatchQueue.main.async { [weak self] in
				guard let weakSelf = self else{return}
				weakSelf.SubTblView.reloadData()
				weakSelf.SubTblView.layoutIfNeeded()
				weakSelf.SubTblView.showLoader()
			}
			self.callSubHistoryWs()
		}
	}
	
	@objc func addNavigationRightButton(){
		let button: UIButton = UIButton(type: UIButtonType.custom)
		button.setImage(UIImage(named: "btn_twitter"), for: UIControlState.normal)
		let barButton = UIBarButtonItem(customView: button)
		//assign button to navigationbar
		self.navigationItem.rightBarButtonItem = barButton
	}
	
	func callSubHistoryWs(){
		DoviesWSCalls.callSubscriptionHistoryAPI(dicParam: nil, onSuccess: { (success, arrModelSubscriptionHistory, msg, isNextThere) in
			if success{
				self.arrSubscriptionHistory = arrModelSubscriptionHistory
			}
			else{
				self.cellCount = 0
			}
            self.emptyDataView.isHidden = self.arrSubscriptionHistory.count > 0
			self.SubTblView.reloadData()
			self.SubTblView.hideLoader()			
		}) { (error) in
			
		}
	}

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
    // MARK: UITableview DataSource Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return arrSubscriptionHistory.count > 0 ? arrSubscriptionHistory.count : cellCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubHistoryCell") as! SubHistoryCell
		if arrSubscriptionHistory.count > 0{
			cell.setSubHistoryData(model: arrSubscriptionHistory[indexPath.row])
		}
        return cell
    }
    
     // MARK: UITableview Delegate Methods
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    
}
