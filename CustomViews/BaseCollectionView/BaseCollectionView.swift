//
//  BaseCollectionView.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit



class BaseCollectionView: UICollectionView {

    static let collectionViewLoadMoreIdentifier  = "CollectionLoadMoreView"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        register(UINib(nibName: BaseCollectionView.collectionViewLoadMoreIdentifier, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: BaseCollectionView.collectionViewLoadMoreIdentifier)
		
    }

}
