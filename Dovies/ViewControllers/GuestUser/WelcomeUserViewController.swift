//
//  WelcomeUserViewController.swift
//  Dovies
//
//  Created by Mindiii on 9/15/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class WelcomeUserViewController: UIViewController {

    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var lblWemcome: UILabel!
    
    @IBOutlet weak var lblMessage: UILabel!
    
    @IBOutlet weak var lblBottomMessage: UILabel!

    var signupParams : [String:Any]?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupUI()
        // Do any additional setup after loading the view.
    }

    func setupUI() {
        
        let userName = self.signupParams![WsParam.userName] as? String ?? ""
        
        self.lblWemcome.text = "WELCOME TO DOVIESFITNESS, \(userName.uppercased())"
        
        self.imgView.makeCircular()
        self.lblWemcome.makeSpacingWithCenterAlignment(lineSpace: 9.0, font: UIFont.futuraBold(size: 15 ),color: UIColor.black)
        self.lblMessage.makeSpacingWithCenterAlignment(lineSpace: 5.0, font: UIFont.tamilRegular(size: 14),color: UIColor(r: 27, g: 27, b: 27, alpha: 1))
        
        self.lblBottomMessage.makeSpacingWithCenterAlignment(lineSpace: 1.0, font: UIFont.futuraBold(size: 8),color: UIColor(r: 181, g: 181, b: 181, alpha: 1))
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @IBAction func actionNext(_ sender: Any) {
        self.navigateToHomeScreen()
    }
    
    func navigateToHomeScreen() {
        DoviesGlobalUtility.showSubscriptionPopupOnRegistration = true
        guard let window = UIApplication.shared.windows.first else { return }
        window.rootViewController = Storyboard.Main.storyboard().instantiateInitialViewController()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
