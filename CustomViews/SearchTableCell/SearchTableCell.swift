//
//  SearchTableCell.swift
//  Dovies
//
//  Created by HB-PC on 09/04/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class SearchTableCell: UITableViewCell {

    @IBOutlet weak var lblTitle : UILabel!
    @IBOutlet weak var lblGroupName : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle  = .none
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
