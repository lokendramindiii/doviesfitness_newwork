//
//  GetUserList.swift
//  Dovies
//
//  Created by Mindiii on 12/4/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//


class ModelUserlist: NSObject, NSCoding{
    
    var userId : String!
    var username : String!
    var name : String!
    
       /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        userId = dictionary["iCustomerId"] as? String ?? ""
        username = dictionary["vUserName"] as? String ?? ""
        name = dictionary["vName"] as? String ?? ""
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if userId != nil{
            dictionary["iCustomerId"] = userId
        }
        if username != nil{
            dictionary["vUserName"] = username
        }
        if name != nil{
            dictionary["vName"] = name
        }
        
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        userId = aDecoder.decodeObject(forKey: "iCustomerId") as? String
        username = aDecoder.decodeObject(forKey: "vUserName") as? String
        name = aDecoder.decodeObject(forKey: "vName") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if userId != nil{
            aCoder.encode(userId, forKey: "iCustomerId")
        }
        if username != nil{
            aCoder.encode(username, forKey: "vUserName")
        }
        if name != nil{
            aCoder.encode(name, forKey: "vName")
        }
    }
    
}



//

class ModelCreatedBy: NSObject, NSCoding{
    
    var customerId : String!
    var guestId : String!
    var guestName : String!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        customerId = dictionary["iCustomerId"] as? String ?? ""
        guestId = dictionary["iDeviosGuestId"] as? String ?? ""
        guestName = dictionary["vGuestName"] as? String ?? ""
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if customerId != nil{
            dictionary["iCustomerId"] = customerId
        }
        if guestId != nil{
            dictionary["iDeviosGuestId"] = guestId
        }
        if guestName != nil{
            dictionary["vGuestName"] = guestName
        }
        
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        customerId = aDecoder.decodeObject(forKey: "iCustomerId") as? String
        guestId = aDecoder.decodeObject(forKey: "iDeviosGuestId") as? String
        guestName = aDecoder.decodeObject(forKey: "vGuestName") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if guestId != nil{
            aCoder.encode(guestId, forKey: "iDeviosGuestId")
        }
        if customerId != nil{
            aCoder.encode(customerId, forKey: "iCustomerId")
        }
        if guestName != nil{
            aCoder.encode(guestName, forKey: "vGuestName")
        }
    }
    
}


class ModelWorkoutCollection: NSObject, NSCoding{
    
    var groupId : String!
    var groupName : String!
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        groupId = dictionary["iWorkoutGroupMasterId"] as? String ?? ""
        groupName = dictionary["vGroupName"] as? String ?? ""
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if groupId != nil{
            dictionary["iWorkoutGroupMasterId"] = groupId
        }
        if groupName != nil{
            dictionary["vGroupName"] = groupName
        }
        
        return dictionary
    }
    
    /**
     * NSCoding required initializer.
     * Fills the data from the passed decoder
     */
    @objc required init(coder aDecoder: NSCoder)
    {
        groupName = aDecoder.decodeObject(forKey: "vGroupName") as? String
        groupId = aDecoder.decodeObject(forKey: "iWorkoutGroupMasterId") as? String
    }
    
    /**
     * NSCoding required method.
     * Encodes mode properties into the decoder
     */
    @objc func encode(with aCoder: NSCoder)
    {
        if groupId != nil{
            aCoder.encode(groupId, forKey: "iWorkoutGroupMasterId")
        }
        if groupName != nil{
            aCoder.encode(groupName, forKey: "vGroupName")
        }
        
    }
    
}
