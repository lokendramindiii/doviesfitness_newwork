//
//	Data.swift
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class ModelFavorite : NSObject/*, NSCoding*/{

	var dietFavourite : [DietPlan]!
	var exercise : [ExerciseDetails]!
	var newsFeed : [NewsFeedModel]!
	var program : [Program]!
	var workoutFavourites : [ModelWorkoutList]!


	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	init(fromDictionary dictionary: [[String:Any]], dataType: String){
		dietFavourite = [DietPlan]()
		if dataType == FavouriteApiType.Diet.rawValue{
			for dic in dictionary{
				let value = DietPlan(fromDictionary: dic)
				dietFavourite.append(value)
			}
		}
		
		exercise = [ExerciseDetails]()
		if dataType == FavouriteApiType.Exercise.rawValue{
			for dic in dictionary{
				let value = ExerciseDetails(fromDictionary: dic)
				exercise.append(value)
			}
		}
		
		newsFeed = [NewsFeedModel]()
		if dataType == FavouriteApiType.Newsfeed.rawValue{
			for dic in dictionary{
				let value = NewsFeedModel(fromDictionary: dic)
				newsFeed.append(value)
			}
		}
		
		program = [Program]()
		if dataType == FavouriteApiType.Program.rawValue{
			for dic in dictionary{
				let value = Program(fromDictionary: dic)
				program.append(value)
			}
		}
		
		workoutFavourites = [ModelWorkoutList]()
		if dataType == FavouriteApiType.Workout.rawValue{
			for dic in dictionary{
				let value = ModelWorkoutList(fromDictionary: dic)
				workoutFavourites.append(value)
			}
		}
	}
	
}
