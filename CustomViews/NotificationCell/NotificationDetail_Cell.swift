//
//  NotificationDetail_Cell.swift
//  Dovies
//
//  Created by Mindiii on 2/6/19.
//  Copyright © 2019 Hidden Brains. All rights reserved.
//

import UIKit

class NotificationDetail_Cell: UITableViewCell {

    
    @IBOutlet weak var lblTitle: CustomLabel!
  //  @IBOutlet weak var lblDescription : CustomLabel!
    @IBOutlet weak var txt_View : UITextView!
    @IBOutlet weak var txt_contentHeightCons : NSLayoutConstraint!

    @IBOutlet weak var btnTitle : UIButton!
    @IBOutlet weak var viewBgTitle : UIView!

    @IBOutlet weak var lblLikeCount: CustomLabel!
    @IBOutlet weak var lblCommentCount: CustomLabel!
    @IBOutlet weak var btnComments : UIButton!
    @IBOutlet weak var btnLike : UIButton!

    
    var commentTapped:(() -> ())?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func btnCommentTapped(_ sender: UIButton){
        commentTapped?()
    }
    
}
