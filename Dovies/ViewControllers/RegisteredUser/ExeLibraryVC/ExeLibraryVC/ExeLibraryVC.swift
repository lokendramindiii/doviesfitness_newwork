//
//  ExeLibraryVC.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

class ExeLibraryVC: BaseViewController,UICollectionViewDelegate,UICollectionViewDataSource {
    
    //IBOutlets
    @IBOutlet var LibraryCollView: BaseCollectionView!
    @IBOutlet weak var emptyView: UIView!
    
    var pullToRefreshCtrl:UIRefreshControl!
    
    //Constants
    static let cellIndentifier = "ExerciseLibararyCollectionCell"
    static let cellPadding : CGFloat = 5.0
  //  let cellSizeValue : CGFloat  =  (ScreenSize.width - (ProgramsViewController.cellPadding))/2
       let cellSizeValue : CGFloat  =  (ScreenSize.width - 1)/2
    //variables
    var arrModelExerciseLibrary = [ModelExerciseLibrary]()
    var placeholderCellsCount = 0
    var isFromCreateWorkout = false
    
    var shouldLoadMore = false
    var currentPageIndex = 1
    
    //MARK: - View controller life cycle
    override func viewDidLoad() {
        
        super.viewDidLoad()

        LibraryCollView.backgroundColor = UIColor.appBlackColorNew()
        self.inititalUISetUp()
        CreateWorkoutVC.arrTempExercises = [ExerciseDetails]()
    }
    
  
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        
        if arrModelExerciseLibrary.count == 0{
            //shimmer loader setup
            DispatchQueue.main.async { [weak self] in
                guard let weakSelf = self else{return}
                
                weakSelf.placeholderCellsCount = 0
              //  weakSelf.placeholderCellsCount = 10
                weakSelf.LibraryCollView.reloadData()
                weakSelf.LibraryCollView.layoutIfNeeded()
                weakSelf.LibraryCollView.showLoader()
                
            }
            self.callExerciseLibraryListing()
        }
        
        if APP_DELEGATE.isFromNotification{
            APP_DELEGATE.isFromNotification = false
            self.callExerciseLibraryListing()
        }
        
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        LibraryCollView.collectionViewLayout.invalidateLayout()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
  
    
//MARK: - Custom methods
    
    override func inititalUISetUp() {
        LibraryCollView.register(UINib(nibName: ExeLibraryVC.cellIndentifier, bundle: nil), forCellWithReuseIdentifier: ExeLibraryVC.cellIndentifier)
        
         LibraryCollView.contentInset = UIEdgeInsets(top: 1.0, left: 0, bottom: self.tabBarController!.tabBar.frame.height, right: 0)
        setPullToRefresh()
    }
    
    func setPullToRefresh(){
        pullToRefreshCtrl = UIRefreshControl()
        pullToRefreshCtrl.addTarget(self, action: #selector(self.pullToRefreshClick(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            LibraryCollView.refreshControl  = pullToRefreshCtrl
        } else {
            LibraryCollView.addSubview(pullToRefreshCtrl)
        }
    }
    
    @objc func pullToRefreshClick(sender:UIRefreshControl) {
        currentPageIndex = 1
        shouldLoadMore = false
        
        callExerciseLibraryListing()
        sender.endRefreshing()
    }
    
    //MARK: - Collection view datasource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int{
        return arrModelExerciseLibrary.count > 0 ? arrModelExerciseLibrary.count : placeholderCellsCount
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell{
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ExeLibraryVC.cellIndentifier, for: indexPath) as? ExerciseLibararyCollectionCell else{return UICollectionViewCell()}
        if arrModelExerciseLibrary.count > 0{
            cell.setCellForExerciseLibraryUI(modelExerciseLibrary: arrModelExerciseLibrary[indexPath.row])
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
			case UICollectionElementKindSectionFooter:
				guard let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,withReuseIdentifier: BaseCollectionView.collectionViewLoadMoreIdentifier, for: indexPath) as? CollectionLoadMoreView else{return UICollectionReusableView()}
				return footerView
				
			default:
				return UIView() as! UICollectionReusableView
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.item == (arrModelExerciseLibrary.count - 1) && shouldLoadMore == true {
            callExerciseLibraryListing()
        }
        
    }
    
    //MARK: - Collection view delegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if arrModelExerciseLibrary.count > indexPath.row{
            let workoutDetailsVC = self.storyboard?.instantiateViewController(withIdentifier: "WorkoutDetailsVC")as! WorkoutDetailsVC
            workoutDetailsVC.navigationItem.title = arrModelExerciseLibrary[indexPath.row].displayName
            workoutDetailsVC.modelExerciseLibrary = arrModelExerciseLibrary[indexPath.row]
            workoutDetailsVC.isFromCreateWorkout = self.isFromCreateWorkout
            self.navigationController?.pushViewController(workoutDetailsVC, animated: false)
        }
    }
    
    //MARK:- WS Call
    
    /**
     This method is used fetch the list of exercise library and updates the UI.
     */
    func callExerciseLibraryListing(){
        DoviesWSCalls.callExerciseLibrary(dicParam: [WsParam.pageIndex : "\(currentPageIndex)"], onSuccess: { (success, msg, responce, hasNextPage) in

            if success{
                if self.currentPageIndex == 1{
                    self.arrModelExerciseLibrary = responce
                    
                //Exercise_library.saveContactInDBExerciseLibraryEntity(ExerciseLibraryArray:responce)

                self.emptyView.isHidden = self.arrModelExerciseLibrary.count > 0
                }else{
                    self.arrModelExerciseLibrary += responce
                //Exercise_library.saveContactInDBExerciseLibraryEntity(ExerciseLibraryArray:responce)
                    
                }

                if hasNextPage{
                    self.currentPageIndex += 1
                }
                self.shouldLoadMore = hasNextPage
                self.placeholderCellsCount = 0
                self.LibraryCollView.hideLoader()
                self.LibraryCollView.reloadData()
            }
            else{
                if self.currentPageIndex == 1{
                    self.emptyView.isHidden = false
                }
            }
        }) { (error) in
            
        }
    }
    
}
extension ExeLibraryVC: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
        return shouldLoadMore ? CGSize(width: ScreenSize.width, height: HeightConstants.baseCollectionViewFooterHeight) : CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: cellSizeValue, height: cellSizeValue)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0  )
    }
    
}
