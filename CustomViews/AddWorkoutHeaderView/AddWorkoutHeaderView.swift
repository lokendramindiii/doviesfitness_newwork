//
//  AddWorkoutHeaderView.swift
//  Dovies
//
//  Created by hb on 26/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit

class AddWorkoutHeaderView: UIView {
    
    @IBOutlet var btnProfile: UIButton!
    @IBOutlet var txtfldTitle: UITextField!

    @IBOutlet var btnLevel: UIButton!
    @IBOutlet var lblLevel: UILabel!

    
    @IBOutlet var btnLevelImage: UIButton!
    @IBOutlet var txtView: UITextView!
    @IBOutlet var btnGoodFor: UIButton!
    @IBOutlet var btnGoodForImage: UIButton!
    @IBOutlet var lblGoodFor: UILabel!
    @IBOutlet var lblEquipments: UILabel!
    @IBOutlet var btnEquipments: UIButton!
    @IBOutlet var btnEquipImage: UIButton!
 
    
    @IBOutlet var btnExercise: UIButton!
    @IBOutlet var btnExerciseImage: UIButton!
    
    @IBAction func btnClickPhoto(_ sender: Any) {
    }
    @IBOutlet var btnClickPhoto: UIButton!
    @IBAction func btnProfileClicked(_ sender: Any) {
    }
    @IBOutlet var imgAddPhoto: UIImageView!
    @IBOutlet var img_StarPhoto: UIImageView!

    // loks add functionality new desgine
    @IBOutlet var btnDefaultViewHideShow: UIButton!
    @IBOutlet var Image_updownarrow: UIImageView!

    @IBOutlet var btn_allTimerExcercise: UIButton!
    @IBOutlet var btn_TimeFinish_reps: UIButton!
    @IBOutlet var btnNoOfReps: UIButton!

    @IBOutlet var ViewTimerHightConstraint: NSLayoutConstraint!
    @IBOutlet var View_AllRepsTimer: UIView!

}
