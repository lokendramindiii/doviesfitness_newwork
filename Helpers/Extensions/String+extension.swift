//
//  String+extension.swift
//
//  Copyright © 2017 hb. All rights reserved.
//

import Foundation
import UIKit

extension String{
	
	func inserting(separator: String, every n: Int) -> String {
		var result: String = ""
		let characters = Array(self)
		stride(from: 0, to: 1, by: n).forEach {
			result += String(characters[$0..<min($0+n, count)])
			result += separator
			result += String(characters[n..<count])
		}
		return result
	}
	
	var MD5: String? {
		let length = Int(CC_MD5_DIGEST_LENGTH)
		
		guard let data = self.data(using: String.Encoding.utf8) else { return nil }
		
		let hash = data.withUnsafeBytes { (bytes: UnsafePointer<Data>) -> [UInt8] in
			var hash: [UInt8] = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
			CC_MD5(bytes, CC_LONG(data.count), &hash)
			return hash
		}
		
		return (0..<length).map { String(format: "%02x", hash[$0]) }.joined()
	}
	
	func base64Encoded() -> String? {
		if let data = self.data(using: .utf8) {
			return data.base64EncodedString()
		}
		return ""
	}
	
	func base64Decoded() -> String? {
		if let data = Data(base64Encoded: self, options: Data.Base64DecodingOptions.ignoreUnknownCharacters) {
			return String(data: data, encoding: .utf8)
		}
		return ""
	}
	
	//	func nsRange(from range: Range<String.Index>) -> NSRange {
	//		let from = range.lowerBound.samePosition(in: utf16)
	//		let to = range.upperBound.samePosition(in: utf16)
	//		return NSRange(location: utf16.distance(from: utf16.startIndex, to: from), length: utf16.distance(from: from, to: to))
	//	}
	
	func range(from nsRange: NSRange) -> Range<String.Index>? {
		guard
			let from16 = utf16.index(utf16.startIndex, offsetBy: nsRange.location, limitedBy: utf16.endIndex),
			let to16 = utf16.index(from16, offsetBy: nsRange.length, limitedBy: utf16.endIndex),
			let from = String.Index(from16, within: self),
			let to = String.Index(to16, within: self)
			else { return nil }
		return from ..< to
	}
	
	
	func toBool() -> Bool? {
		switch self.lowercased() {
		case "true", "yes", "1":
			return true
		case "false", "no", "0":
			return false
		default:
			return nil
		}
	}
	
	var html2AttributedString: NSAttributedString? {
		guard let data = data(using: .utf8) else { return nil }
		do {
			return try NSAttributedString(data: data, options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html, NSAttributedString.DocumentReadingOptionKey.characterEncoding: String.Encoding.utf8.rawValue], documentAttributes: nil)
		} catch let error as NSError {
			print(error.localizedDescription)
			return  nil
		}
	}
	
	var html2String: String {
		return html2AttributedString?.string ?? ""
	}
	
	func rangeOfLastWord() -> Range<Int>? {
		let arr = self.characters.split(separator: " ").map(String.init)
		var spaceAndNewLineCount = 0
		var characCount = 0
		for charac in self.characters {
			characCount += 1
			if charac == " " || charac == "\n" {
				spaceAndNewLineCount += 1
			}
		}
		let fromIndex = self.utf16.count - arr[arr.count-1].utf16.count
		return fromIndex..<self.utf16.count
	}
	
	public func replacing(range: Range<Int>, with replacementString: String) -> String {
		let start = characters.index(characters.startIndex, offsetBy: range.lowerBound)
		let end   = characters.index(start, offsetBy: range.count)
		return self.replacingCharacters(in: start ..< end, with: replacementString)
	}
	
	func first(charsCount: Int) -> String {
		let toIndex = self.characters.index(self.startIndex, offsetBy: charsCount)
		return self.substring(to: toIndex)
	}
	
    func isValidEmailCharacters() -> Bool {
        if self.count >= 6 && self.count <= 100
        {
            return true
        }
        else
        {
            return false
        }
    }

    
	func isValidEmail() -> Bool {
		let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
		let range = self.range(of: emailRegEx, options:.regularExpression)
		return range != nil ? true : false
	}
	
	func isValidText() -> Bool {
		let str:String = self.replacingOccurrences(of: " ", with: "")
		return str.characters.count>0 ? true:false
	}
	
	func isValidPassword() -> Bool {
        //8 to 16
		let str:String = self.replacingOccurrences(of: " ", with: "")
		return str.count>=8 && str.count<=16 ? true:false
	}
	
	func isValidPhoneNumber() -> Bool {
		let str:String = self.replacingOccurrences(of: " ", with: "")
		return str.count >= 8 && str.count <= 15 ? true : false
	}
	
	func isEmptyAfterRemovingWhiteSpacesAndNewlines() -> Bool {
		return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).isEmpty
	}
	
	func removeWhiteSpace() -> String {
		return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
	}
	
	func charSpacing(letterSpacing: CGFloat = 0) -> NSAttributedString {
		let attrString = NSMutableAttributedString(string:self)
		attrString.addAttribute(NSAttributedStringKey.kern, value: CGRect.widthRatio * letterSpacing, range: NSMakeRange(0, attrString.length))
		return attrString
	}
	
	subscript (i: Int) -> Character {
		//        return self[self.index(after: self.startIndex)]
		return self[self.index(self.startIndex, offsetBy:i)]
		
	}
	
	func setUnderLine(font:UIFont, textColor:UIColor) -> NSMutableAttributedString{
		let attrs = [
			NSAttributedStringKey.font.rawValue : font,
			NSAttributedStringKey.foregroundColor : textColor,
			NSAttributedStringKey.underlineStyle : 1
			] as! [NSAttributedStringKey : Any]
		return NSMutableAttributedString(string: self, attributes: attrs)
	}
	
	func updateImageUrl(cgSize: CGSize) -> String{
		let strImgUrl = self as NSString
		let lastComponent = strImgUrl.lastPathComponent
		let removeLastComponent = strImgUrl.deletingLastPathComponent
		var updateLastComponent = ""
		if lastComponent.contains("170x170"){
			updateLastComponent = lastComponent.replacingOccurrences(of: "170x170", with: "\(Int(cgSize.width) * 2)x\(Int(cgSize.height) * 2)")
		}
		else{
			updateLastComponent = lastComponent.replacingOccurrences(of: "70x70", with: "\(Int(cgSize.width) * 2)x\(Int(cgSize.height) * 2)")
		}
		
		updateLastComponent = updateLastComponent.replacingOccurrences(of: "bb-85", with: "bb-170")
		print(updateLastComponent)
		
		let sUrl = removeLastComponent + "/" + updateLastComponent
		
		return sUrl
	}
	
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont, lineSpace : CGFloat? = 1.0) -> CGFloat {
		let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        var boundingBox = CGRect.zero;
        if lineSpace == 1.0
        {
            boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        }
        else
        {
            let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = lineSpace!
            boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font, NSAttributedStringKey.paragraphStyle:paragraphStyle], context: nil)
        }
		return boundingBox.height
	}
    
    // loks add heightWithConstrainedWidthLineSpacing
    func heightWithConstrainedWidthLineSpacing(width:CGFloat, lineSpace: CGFloat, font:UIFont , Str_String:String)-> CGFloat{
        let style = NSMutableParagraphStyle()
        style.lineSpacing = lineSpace
        style.alignment = .center
        let att = [NSAttributedStringKey.font : font,  NSAttributedStringKey.paragraphStyle : style, NSAttributedStringKey.kern : 0.5]
            as [NSAttributedStringKey : Any]
        
        let attStr = NSMutableAttributedString(string: Str_String, attributes: att
        )
        let rect: CGRect = attStr.boundingRect(with: CGSize(width: width, height: 10000), options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil)
        
        return rect.size.height
        
    }

	
	func heightForWithFont(font: UIFont, width: CGFloat, insets: UIEdgeInsets) -> CGFloat {
	 	let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width + insets.left + insets.right, height: CGFloat.greatestFiniteMagnitude))

		label.numberOfLines = 0
		label.lineBreakMode = NSLineBreakMode.byWordWrapping
		label.font = font
		label.text = self
		
		label.sizeToFit()
        return label.frame.height + insets.top + insets.bottom

	}
	
	func substring(from: Int?, to: Int?) -> String {
		if let start = from {
			guard start < self.count else {
				return ""
			}
		}
		
		if let end = to {
			guard end >= 0 else {
				return ""
			}
		}
		
		if let start = from, let end = to {
			guard end - start >= 0 else {
				return ""
			}
		}
		
		let startIndex: String.Index
		if let start = from, start >= 0 {
			startIndex = self.index(self.startIndex, offsetBy: start)
		} else {
			startIndex = self.startIndex
		}
		
		let endIndex: String.Index
		if let end = to, end >= 0, end < self.characters.count {
			endIndex = self.index(self.startIndex, offsetBy: end + 1)
		} else {
			endIndex = self.endIndex
		}
		
		return String(self[startIndex ..< endIndex])
	}
	
	var stringByRemovingWhitespaceAndNewlineCharacterSet: String {
		return components(separatedBy: NSCharacterSet.whitespacesAndNewlines).joined(separator: " ")
	}
    
    func widthOfString(usingFont font: UIFont) -> CGFloat {
        let fontAttributes = [NSAttributedStringKey.font: font]
        let size = self.size(withAttributes: fontAttributes)
        return size.width
    }
	
	func capitalizingFirstLetter() -> String {
		return prefix(1).uppercased() + dropFirst()
	}
	
	mutating func capitalizeFirstLetter() {
		self = self.capitalizingFirstLetter()
	}
}

