//
//  ViewController.swift
//  InstaGramDemo
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

let instagram_clientID = "03c8bf5f28df46c7b4631d855303d8a0"
let instagram_redirectURI = "http://www.doviesworkout.com"
let instagram_clientSecret = "e9d72c95d6e04370b58a3fe68352bc95"

class InstaGramViewController: BaseViewController,UIWebViewDelegate {
    var viewInstagram : UIView!
   
    var webView : UIWebView!
    var btnBAck : UIButton!
    
    var onTokenSuccess:((String)->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
        self.addViewInstagram()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
	
    //MARK:- Private Methods
	
    func addViewInstagram() {
        viewInstagram = UIView(frame: CGRect(x: 0,y: 0,width : SCREEN_WIDTH, height : SCREEN_HEIGHT))
        viewInstagram.backgroundColor = UIColor(red: 65/255.0, green: 111/255.0, blue: 150/255.0, alpha: 1.0)
        self.view.addSubview(viewInstagram)
        
        btnBAck = UIButton(type:.custom)
        btnBAck.frame = CGRect(x: 10, y: 20, width: 45, height: 44)
        btnBAck.backgroundColor = UIColor.clear
        btnBAck.setTitle("Back", for: .normal)
        
        btnBAck.setTitle("Back", for: .highlighted)
        btnBAck.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        btnBAck.setTitleColor(UIColor.white, for: .normal)
        btnBAck.setTitleColor(UIColor.gray, for: .highlighted)
        btnBAck.addTarget(self, action: #selector(self.backClicked), for: .touchUpInside)
        btnBAck.contentHorizontalAlignment = UIControlContentHorizontalAlignment.center
        viewInstagram.addSubview(btnBAck)
        
        webView = UIWebView(frame: CGRect(x: 0,y: 64,width: SCREEN_WIDTH,height: SCREEN_HEIGHT-64))
        webView.delegate = self
        viewInstagram.addSubview(webView)
        self.authorization()
    }

	//Firing Insta API in webview
    func authorization() {
        //Logging out befor login
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.logut()
        let str = "https://api.instagram.com/oauth/authorize/?client_id=\(instagram_clientID)&redirect_uri=\(instagram_redirectURI)&response_type=code"
        webView.loadRequest(NSURLRequest(url: (NSURL(string: str)!) as URL) as URLRequest)
    }
    
    @objc func backClicked(){
        self.dismiss(animated: true, completion: nil)
    }
	
	func logut(){
		//Clearing the coockie
		let cookieJar : HTTPCookieStorage = HTTPCookieStorage.shared
		for cookie in cookieJar.cookies! as [HTTPCookie]{
			NSLog("cookie.domain = %@", cookie.domain)
			
			if cookie.domain == "www.instagram.com" ||
				cookie.domain == "api.instagram.com"{
                
				
				cookieJar.deleteCookie(cookie)
			}
		}
	}
	
	//MARK:- UIWebViewDelegate
	
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
    }
	
	func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool
    {
        let html = request.url!.absoluteString
        
        //Checking web view returning redirect URL
        if ((html.range(of: instagram_redirectURI)) != nil) {
            print(extractCode(urlString: html) ?? "")
            let code = extractCode(urlString: html)
            if let token = code  {
                onTokenSuccess?(token)
                self.dismiss(animated: true, completion: {})
                return false
            }
        }
        return true
    }
	
    func webViewDidFinishLoad(_ webView: UIWebView) {
        MBProgressHUD.hideAllHUDs(for: self.view, animated: true)
    }
	
	
	func extractCode(urlString: String) -> String? {
		var code: String? = nil
		let url = NSURL(string: urlString)
		let urlQuery = (url?.query != nil) ? url?.query : urlString
		let components = urlQuery?.components(separatedBy: "&")
		for comp in components! {
            if (comp.range(of: "code=") != nil) {
                code = comp.components(separatedBy: "=").last
            }
		}
		return code
	}
}

