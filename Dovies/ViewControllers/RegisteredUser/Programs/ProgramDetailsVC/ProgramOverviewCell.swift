//
//  ProgramOverviewCell.swift
//  Dovies
//
//  Created by IOS-macbook on 03/10/19.
//  Copyright © 2019 Hidden Brains. All rights reserved.
//

import UIKit

class ProgramOverviewCell: UITableViewCell {

    @IBOutlet weak var lblDescription : CustomLabel!
    @IBOutlet weak var IBbtnPurchase: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
