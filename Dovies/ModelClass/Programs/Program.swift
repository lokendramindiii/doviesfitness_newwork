//
//  Program.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

struct Program {
    var programId : String!
    var programImage : String!
    var programName : String!
    var programShareUrl : String!
    var programAccessLevel : String!
    var programWeekCount : String!
    var accessLevel : String!
    var programAddedInMyDiet = false

    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        programId = dictionary["program_id"] as? String ?? ""
        programImage = dictionary["program_image"] as? String ?? ""
        programName = dictionary["program_name"] as? String ?? ""
        programShareUrl = dictionary["program_share_url"] as? String ?? ""
        programAccessLevel = dictionary["program_access_level"] as? String ?? ""
        programWeekCount = dictionary["program_week_count"] as? String ?? ""

        accessLevel = dictionary["access_level"] as? String ?? ""
		
		if let favStatus = dictionary["is_customer_added"] as? String , favStatus == "1"{
			programAddedInMyDiet = true
		}else if  let dietFavStat = dictionary["is_customer_added"] as? Bool{
			programAddedInMyDiet = dietFavStat
		}else{
			programAddedInMyDiet = false
		}
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if programId != nil{
            dictionary["program_id"] = programId
        }
        if programImage != nil{
            dictionary["program_image"] = programImage
        }
        if programName != nil{
            dictionary["program_name"] = programName
        }
        if programShareUrl != nil{
            dictionary["program_share_url"] = programShareUrl
        }
        if programAccessLevel != nil{
            dictionary["program_access_level"] = programShareUrl
        }
        if programWeekCount != nil{
            dictionary["program_week_count"] = programWeekCount
        }
        
        if accessLevel != nil{
            dictionary["access_level"] = accessLevel
        }
        return dictionary
    }
}

struct ProgramStaticInfo{
    var content : String!
    var title : String!
    
    
    /**
     * Instantiate the instance using the passed dictionary values to set the properties values
     */
    init(fromDictionary dictionary: [String:Any]){
        content = dictionary["content"] as? String ?? ""
        title = dictionary["title"] as? String ?? ""
    }
    
    /**
     * Returns all the available property values in the form of [String:Any] object where the key is the approperiate json key and the value is the value of the corresponding property
     */
    func toDictionary() -> [String:Any]
    {
        var dictionary = [String:Any]()
        if content != nil{
            dictionary["content"] = content
        }
        if title != nil{
            dictionary["title"] = title
        }
        return dictionary
    }
}
