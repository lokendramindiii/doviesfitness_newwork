//
//  FeaturedDetailListTableCell.swift
//  Dovies
//
//  Created by Neel Shah on 14/03/18.
//  Copyright © 2018 Hidden Brains. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

class FeaturedDetailListTableCell: UITableViewCell {
	
	@IBOutlet weak var constBreakTimeHeight: NSLayoutConstraint!
    @IBOutlet weak var constPlayerHeight: NSLayoutConstraint!
    @IBOutlet weak var constTopViewHeight: NSLayoutConstraint!
	@IBOutlet weak var stackViewBreakTime: UIStackView!
	@IBOutlet weak var lblBreakTime: UILabel!
	@IBOutlet weak var btn_Detail: UIButton!
	@IBOutlet weak var lblEquipment: UILabel!
	@IBOutlet weak var lblLevel: UILabel!
	@IBOutlet weak var lblName: UILabel!
	@IBOutlet weak var btnLike: UIButton!
	@IBOutlet weak var btnCheck: UIButton!
    @IBOutlet weak var btnvideo: UIButton!
    @IBOutlet weak var IBbtnShare: UIButton!

	@IBOutlet weak var imgViewExercise: UIImageView!
	//@IBOutlet weak var tagListViewBodyParts: CustomTagListView!
    @IBOutlet weak var tagListlbl_BodyParts: UILabel!

   //	@IBOutlet weak var IBbtnDetail: UIButton!
    @IBOutlet weak var btnLock: UIButton!
	
	@IBOutlet weak var IBbtnVideoClose: UIButton!
	@IBOutlet weak var IBbtnVideoPlay: UIButton!
	//@IBOutlet weak var viewUpperLayer: UIView!
	
	@IBOutlet weak var viewVideoPlayer: VideoContainerView!
	
	@IBOutlet weak var IBacitivtyIndicatorVideo: UIActivityIndicatorView!
	
	var downloadRequest: DownloadRequest?

	var playerItem: AVPlayerItem!
	var player: AVPlayer!
	var playerLayer: AVPlayerLayer!
	
	var favouriteTap:(() -> ())?
	var btnDetailTap:(() -> ())?
	var imageTapped:(() -> ())?
	var videoCloseBtnTap:(() -> ())?
    var shareTappedbtn:(() -> ())?

    var videoUpperLayerTapped:(()->())?
	var videoDownloaded:(()->())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
	
    func setUpFeaturedDetailData(workoutExercise: ModelWorkoutExerciseList, isLastCell : Bool){
		
	//	imgViewExercise.sd_setImage(with: URL(string: workoutExercise.workoutExerciseImage), placeholderImage: AppInfo.placeholderImage)
        
        imgViewExercise.sd_setImage(with: URL(string: workoutExercise.workoutExerciseImage), placeholderImage: nil)

        
        let attributedString = NSMutableAttributedString(string: workoutExercise.workoutExerciseName)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 5.0
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
        lblName.attributedText = attributedString
        
		//lblName.text = workoutExercise.workoutExerciseName
        
        // loks add workoutExerciseType for check time or repeats
        if workoutExercise.workoutExerciseType == "Time"
        {
            if workoutExercise.workoutExerciseDetail == "01 mins"{
                btn_Detail.setTitle("01 min", for: .normal)
            }
            else if workoutExercise.workoutExerciseDetail == "00 secs"{
                btn_Detail.setTitle("00 sec", for: .normal)
            }
            else if workoutExercise.workoutExerciseDetail == "01 secs"{
                btn_Detail.setTitle("01 sec", for: .normal)
            }
            else
            {
            btn_Detail.setTitle(workoutExercise.workoutExerciseDetail, for: .normal)
            }
        }
        else
        {
            if workoutExercise.workoutExerciseDetail == "01"{
            btn_Detail.setTitle(workoutExercise.workoutExerciseDetail + " Rep", for: .normal)
            }
            else if workoutExercise.workoutExerciseDetail == "00"
          {
            btn_Detail.setTitle(workoutExercise.workoutExerciseDetail + " Rep", for: .normal)
            }
            else
            {
            btn_Detail.setTitle(workoutExercise.workoutExerciseDetail + " Reps", for: .normal)
            }
        }
        if workoutExercise.workoutExerciseLevel == "All" 
        {
            lblLevel.text = "All Level"
        }
        else
        {
            lblLevel.text = workoutExercise.workoutExerciseLevel
        }
        btnLock.isHidden = workoutExercise.workoutAccessLevel.uppercased() == "OPEN".uppercased() || DoviesGlobalUtility.currentUser?.customerUserName == DoviesGlobalUtility.isAdminUserName
        
        if btnLock.isHidden == true {
            btnvideo.isHidden = false
        }
        else
        {
            btnvideo.isHidden = true
        }
        
        if workoutExercise.fBreakTime != nil{
            
            hmsFrom(seconds: workoutExercise.fBreakTime) { hours, minutes, seconds in
                
                let sHours = self.getStringFrom(seconds: hours)
                
                let sMinutes = self.getStringFrom(seconds: minutes)
                
                let sSeconds = self.getStringFrom(seconds: seconds)
                
                if hours > 0{
                    self.lblBreakTime.text = "\(sHours):\(sMinutes):\(sSeconds)" + " Hour"
                }
                else if minutes > 0 {
                    
                    if minutes > 1{
                        self.lblBreakTime.text = "\(sMinutes):\(sSeconds)" + " Minutes"
                    }
                    else{
                        self.lblBreakTime.text = "\(sMinutes):\(sSeconds)" + " Minute"
                    }
                    
                }else if seconds > 0{
                    
                    if seconds > 1{
                    self.lblBreakTime.text = "\(sSeconds)" + " Seconds"
                    }
                    else{
                        self.lblBreakTime.text = "\(sSeconds)" + " Second"
                    }
                }
                if isLastCell || !(hours > 0 || minutes > 0 || seconds > 0){
                    self.constBreakTimeHeight.constant = 0
                    self.stackViewBreakTime.isHidden = true
                }else {
                    self.constBreakTimeHeight.constant = 38
                    self.stackViewBreakTime.isHidden = false
                }
                
            }
        }
    
    
		if workoutExercise.workoutExerciseIsFavourite != nil{
			btnLike.isSelected = workoutExercise.workoutExerciseIsFavourite
		}
		let arrEquipments = workoutExercise.workoutExerciseEquipments.components(separatedBy: " | ")
		if arrEquipments.count > 0 {
			lblEquipment.text = arrEquipments[0]
		}
		
//        tagListViewBodyParts.removeAllTags()
//  tagListViewBodyParts.addTags(workoutExercise.workoutExerciseBodyParts.components(separatedBy: " | "))
//
//
//        tagListViewBodyParts.textFont = UIFont.tamilRegular(size: 12.0)


        tagListlbl_BodyParts.text = workoutExercise.workoutExerciseBodyParts.replacingOccurrences(of: " | ", with: ", ")
		//let tapGes = UITapGestureRecognizer(target: self, action: #selector(tapOnImage))
	//	imgViewExercise.addGestureRecognizer(tapGes)
//        viewUpperLayer.addTapGesture(target: self, action: #selector(tapOnVideoUpperLayer))
//        viewUpperLayer.isHidden = true
	}
	
    func hmsFrom(seconds: Int, completion: @escaping (_ hours: Int, _ minutes: Int, _ seconds: Int)->()) {
        completion(seconds / 3600, (seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    func getStringFrom(seconds: Int) -> String {
        return seconds < 10 ? "0\(seconds)" : "\(seconds)"
    }
    
	@objc func tapOnImage(){
		imageTapped?()
	}
	
	@IBAction func btnFavouriteTap(){
		favouriteTap?()
	}
	
	@IBAction func btnDetailTapped(){
		btnDetailTap?()
	}
    @IBAction func btnSahreTap(sender: UIButton){
        shareTappedbtn?()
    }
    
	@IBAction func btnVideoCloseTap(sender: UIButton){
		videoCloseBtnTap?()
	}
	
    @objc func tapOnVideoUpperLayer(){
        videoUpperLayerTapped?()
    }
    
	func setUpPlayerInCell(urlString: String)
	{
		
		let theFileName = (urlString as NSString).lastPathComponent
		
		if theFileName.isEmpty == true{
			return
		}
		IBacitivtyIndicatorVideo.isHidden = true
        //viewUpperLayer.isHidden = false
		let videoThere = DoviesGlobalUtility.checkWorkOutDownloadedOrNot(folderName: theFileName)
		if videoThere == false{
			IBbtnVideoPlay.isHidden = true
			IBacitivtyIndicatorVideo.isHidden = false
			IBacitivtyIndicatorVideo.startAnimating()
			downloadRequest = ModelVideoListing.downloadVideoForExercise(sVideoZipUrl: urlString, sFileName: theFileName, currentProgress: { (progress, url, id) in
				
			}, response: { (downloadResponce, id) in
				if downloadResponce.error == nil{
					self.playVideoFromDocumentDirectory(videoLastPathComponent: theFileName)
					if self.videoDownloaded != nil{
						self.videoDownloaded!()
					}
				}
				else{
					
				}
				self.IBacitivtyIndicatorVideo.isHidden = true
                self.IBacitivtyIndicatorVideo.stopAnimating()
			})
		}
		else{
			self.playVideoFromDocumentDirectory(videoLastPathComponent: theFileName)
		}
	}
	
	func playVideoFromDocumentDirectory(videoLastPathComponent: String){
		
		let urlString = DoviesGlobalUtility.pathForVideoFile(videoFileName: videoLastPathComponent)
		
		player = AVPlayer()
		
		playerLayer = AVPlayerLayer(player: player)
		playerLayer.videoGravity = .resizeAspect;
		playerLayer.backgroundColor = UIColor.clear.cgColor
		if (playerLayer.superlayer != nil){
			playerLayer.removeFromSuperlayer()
		}
		
		playerLayer.frame =  CGRect(x: 0, y: 0, width: viewVideoPlayer.bounds.width, height: 200);
		
		viewVideoPlayer.layer.addSublayer(playerLayer);
		player.actionAtItemEnd = .pause
		viewVideoPlayer.playerLayer = playerLayer;
		
		let asset = AVURLAsset(url: URL(string: urlString.absoluteString)!)
		
		playerItem = AVPlayerItem(asset: asset)
		self.player.replaceCurrentItem(with: self.playerItem)
		
		self.playVideoForCell()
		self.playerEndObserver()
	}
	
	@IBAction func playVideoForCell(){
		self.playerLayer.player?.play()
		IBbtnVideoPlay.isHidden = true
	}
	
	func playerEndObserver(){
		NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: .main) { [weak self] _ in
			self?.player.seek(to: kCMTimeZero)
			self?.playerLayer.player?.play()
		}
	}
	
    
	func stopVideo(){
		downloadRequest?.cancel()
		if (player != nil){
			if player.isPlaying{
				self.player.pause()
				IBbtnVideoPlay.isHidden = false
			}
		}
	}
	
	func removePlayerFromView(){
		if (player != nil){
			playerLayer.removeFromSuperlayer()
		}
	}
}

