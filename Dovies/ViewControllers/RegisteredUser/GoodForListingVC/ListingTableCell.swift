//
//  ListingTableCell.swift
//  Dovies
//
//  Created by CompanyName.
//  Copyright © CompanyName. All rights reserved.
//

import UIKit

class ListingTableCell: UITableViewCell {
    
    @IBOutlet var lblList: UILabel!
    @IBOutlet var btnSelection: UIButton!
    
    var indexPath : IndexPath!
    var obj : FilterDataModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setData(_ obj:FilterDataModel?,at indexPath : IndexPath){
        self.obj = obj
        lblList.text = obj?.gmDisplayName.capitalized
        self.indexPath = indexPath
        
    }

}
